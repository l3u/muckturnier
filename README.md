# Muckturnier

## Das Programm für die Turnierleitung

Muckturnier ist die Komplettlösung für die Vorbereitung, Durchführung und Auswertung eines Muckturniers. Damit spart man sich als Turnierleitung viel Arbeit und Zeit, sowohl vor als auch während des Turniers. Das Programm ist für alle gängigen Desktop-Betriebssysteme verfügbar und – dank Open Source – vollkommen kostenlos.

## Homepage

Die offizielle Projekthomepage ist <https://muckturnier.org/>. Dort gibt es neben der Dokumentation und Quelltext-Tarballs der Releases auch Installer für Windows, [AppImage](https://appimage.org/)s für Linux und App-Pakete für macOS.

## Dokumentation

Die Dokumentation des aktuellen Releases steht unter <https://muckturnier.org/readme/> online. Der aktuelle Stand der Dokumentation kann direkt im [GitLab-Repository](https://gitlab.com/l3u/muckturnier/-/blob/master/doc/Readme.rst) angesehen werden.

## Aktueller Entwicklungsstand

Die aktuelle Liste bisher unveröffentlichter Änderungen kann direkt im [GitLab-Repository](https://gitlab.com/l3u/muckturnier/-/blob/master/doc/CHANGELOG.rst) angesehen werden.
