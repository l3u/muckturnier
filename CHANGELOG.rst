.. SPDX-FileCopyrightText: 2012-2025 Tobias Leupold <tl@stonemx.de>

   SPDX-License-Identifier: CC-BY-SA-4.0

   Das Format dieser Datei ist inspiriert von keepachangelog.com, nutzt aber ReStructuredText
   anstatt MarkDown. Die Zeilenlänge sollte 100 Zeichen nicht überschreiten (mit der
   offensichtlichen Ausnahme der Überschriften-Vorlage unten, die um drei Leerzeichen eingerückt
   sein muss).

   Hier ist die Überschriften-Vorlage, zum Kopieren und Einfügen nach einem neuen Release:

   ====================================================================================================
   [unveröffentlicht]
   ====================================================================================================

   Neu
   ===

   * für neue Features.

   Geändert
   ========

   * für Änderungen an bestehender Funktionalität.

   Veraltet
   ========

   * für Features, die bald entfernt werden.

   Entfernt
   ========

   * für entfernte Features.

   Korrigiert
   ==========

   * Für Bugfixes.

   Sicherheit
   ==========

   * für Sicherheitslücken.

====================================================================================================
[unveröffentlicht]
====================================================================================================

Neu
===

Geändert
========

* Wenn nur ein Bobbl pro Runde gespielt wird, dann wird jetzt die Bobbl-Nummer sowohl auf der
  Spielstandseite als auch beim Exportieren der Spielstände in eine HTML- oder TSV-Datei
  weggelassen.

* Es gibt jetzt auch Spielstand-Zettel-Vorlagen für nur einen Bobbl pro Runde, entsprechend den
  Vorlagen für zwei und drei Bobbl in einer „horizontalen“ und „vertikalen“ Variante.

Veraltet
========

Entfernt
========

Korrigiert
==========

* Es wurde ein Workaround für den Breeze-Style unter KDE Plasma 6 hinzugefügt, damit wieder überall
  Rahmen gezeichnet werden, wo welche sein sollten (vgl.
  `KDE-Bug #488195 <https://bugs.kde.org/show_bug.cgi?id=488195>`_,
  `KDE-Bug #499131 <https://bugs.kde.org/show_bug.cgi?id=499131>`_ sowie
  `Breeze-Merge-Request #520 <https://invent.kde.org/plasma/breeze/-/merge_requests/520>`_).

* Wenn man im Zeitplan-Display jetzt für eine Seite ein Hintergrundbild lädt, und die Seite hatte
  schon vorher eines, dann wird dieses jetzt sofort angezeigt, und nicht erst, nachdem die Größe des
  Displays verändert wird.

* Bei den Binärpaketen können jetzt auch JPEG-Dateien (insbesondere als Hintergrundbild für das
  Zeitanzeige-Display) genutzt werden, nicht nur PNG-Dateien. Das Fehlen der JPEG-Unterstützung war
  gar nicht am Code gelegen, sondern an den Qt-Builds, die bisher zum Bauen der jeweiligen Pakete
  genutzt wurden. Egal wie – das Problem ist jetzt behoben.

* Ein kleiner Hack (das Hinzufügen eines „Hair Space“-Zeichens zum „Ergebnisse“-Tab) umgeht nun
  `Qt-Bug #126859 <https://bugreports.qt.io/browse/QTBUG-126859>`_ für den Moment. Sollte der Bug
  irgendwann behoben werden, kann das wieder entfernt werden.

* Wenn eine Datenbank als „Abgeschlossen“ markiert ist und der Turnierstatus wieder auf „Offen“
  gesetzt wird, die „Auslosung“-Spalte auf der Anmeldungsseite eingeblendet ist, die „Übersicht
  Auslosung“-Seite aber nicht, dann wird jetzt nicht mehr fälschlicherweise nachgefragt, ob auch die
  „Übersicht Auslosung“-Seite eingeblendet werden soll.

Sicherheit
==========

====================================================================================================
Release von Muckturnier 3.9.0 (16.02.2025)
====================================================================================================

Neu
===

* Das Programm kann jetzt mit cmake nicht nur unter Linux, sondern auch unter Windows und macOS
  gebaut werden (wenngleich ein komplettes Build incl. Icons etc. nach wie vor nur unter Linux
  funktioniert; für die beiden anderen Systeme empfehle ich nach wie vor ein Binärpaket).
  Weiterhin kann das Programm jetzt auch (via cmake) korrekt auf FreeBSD gebaut werden.

* Es gibt jetzt den „Code-Scanner testen“-Dialog, mit dem man überprüfen kann, ob ein
  Hardware-QR-Code-Scanner korrekt konfiguriert ist: Man scannt einen Test-QR-Code, der alle Zeichen
  enthält, die in einem Anmeldungscode vorkommen können. Dann wird überprüft, ob die Eingabe des
  Scanners exakt mit dem Datensatz übereinstimmt.

* Es gibt jetzt ein Auslosungs-Display. Das ist, genau wie das Turnier-Zeitplan-Display, dafür
  gedacht, auf einem zweiten Monitor, Fernseher, Beamer etc. angezeigt zu werden. Das Display zeigt
  alle Tische samt der zugelosten Paare und aktualisiert sich, sobald etwas an der Auslosung
  geändert wird. Die Schriftgröße kann so angepasst werden, dass alle Tische gleichzeitig sichtbar
  sind.

* Im Turnier-Zeitplan-Display kann jetzt auch für die Zeit-Anzeige-Seite ein Hintergrundbild
  angezeigt werden.

* Für das Turnier-Zeitplan-Display können jetzt mehrere benannte Einstellungen (für verschiedene
  Turniere) gespeichert werden.

* Wenn für eine Voranmeldung bereits ein Anmeldungscode erstellt wurde, und nach einer Änderung wird
  ein neuer Code exportiert oder die Voranmeldung wird gelöscht, dann wird der vorher exportierte
  Code jetzt als widerrufen hinterlegt. Wird ein widerrufener Code gescannt, wird eine entsprechende
  Warnung angezeigt.

* Die Suchfunktion wurde weiter verbessert: Jetzt werden typische Paar-Trenner („/“, „+“ und „&“)
  berücksichtigt. Wenn man z. B. „Müller Alexander / Schmidt Heinrich“ angemeldet hat, und will
  „alexander Mueller/Heinrich schmitt“ anmelden, dann wurde das trotz aktivierter phonetischer
  Suche bisher nicht als mögliches Duplikat gemeldet – sofern die automatische Groß- und
  Kleinschreibung deaktiviert war und/oder ein anderer als der eingestellte Paartrenner eingegeben
  wurde.
  „Mueller/Heinrich“ wurde – mangels Leerzeichen vor bzw. nach dem Schrägstrich – als ein Wort
  angesehen, und deswegen konnte die phonetische Suche keine Übereinstimmung finden. Jetzt werden
  bei solchen Eingaben, sofern einer der o. g. Trenner benutzt wird, die Namen einzeln verarbeitet
  und es wird korrekt eine „Potenzielles Duplikat“-Warnmeldung angezeigt.

Geändert
========

* Das Serialisierungsprotokoll für Anmeldungscodes wurde geändert und liegt jetzt in Version 2 vor.
  Damit werden keine Leerzeichen mehr in den Codes gespeichert. Das behebt Kompatibilitätsprobleme
  mit Hardware-Barcode-Scannern, die Leerzeichen einfach ignorieren und bei der Eingabe auslassen.

* Das Hauptmenü wurde teils neu geordnet: Optional einblendbare Seiten sind jetzt unter
  „Zusatzfunktionen“ zu finden, der WLAN-Code-Scanner-Server unter „Netzwerk“ etc.

* Eine Stoppuhr kann jetzt nicht mehr durch das Drücken der ESC-Taste geschlossen werden, sondern
  nur noch explizit durch das Klicken auf das X der Fenstertitelleiste. Das behebt gleichzeitig
  einen Crash, der beim Beenden des Programms auftrat, wenn eine Stoppuhr mittels ESC geschlossen
  wurde (da in diesem Fall der gestartete Timer im Hintergrund weiterlief).

* Das Interface für die Auswahl der Markierungen für Voranmeldungen und allein gekommene Spieler
  wurde vereinheitlicht: Für beide Features kann jetzt eine Group-Box (de)aktiviert werden.

* Die Farben der Standardmarkierungen wurden leicht angepasst, so dass sie sowohl für helle als auch
  für dunkle Farbschemata („Light/Dark Mode“) einen hinreichenden Kontrast haben und somit gut
  lesbar sind.

* Die Einstellungen für die Voranmeldung werden jetzt nicht mehr als bereits serialisierter String,
  sondern als JSON-Objekt gespeichert – so wie alle anderen Unter-Einstellungen auch. Die Markierung
  für Voranmeldungen wird jetzt auch intern (in Funktions- und Variablennamen sowie beim Speichern)
  nicht mehr mit „abwesend“, sondern mit „vorangemeldet“ referenziert.

* Die Datenbankrevision wurde auf Version 14 aktualisiert. Ein Update alter Datenbanken wird, wie
  immer, beim Öffnen angeboten.

* Auslosungszettel werden jetzt für vereinfachtes Schneiden in Stapel zusammengefasst.

* Der Statustext der Anmeldungsseite wurde überarbeitet. Die Ausgabe ist jetzt strukturierter und
  gibt insbesondere aus, wie viele Tische besetzt wären und wie viele Paare bzw. Spieler noch fehlen
  würden, wenn alle Voranmeldungen kommen.

Entfernt
========

* Der Dialog „Turnierinformationen“ wurde entfernt, da er keinen Mehrwert bringt.

* Das Menü „Angezeigte Spalten der Anmeldungsliste“ wurde aus dem „Ansicht“-Menü des Hauptfensters
  entfernt. Die Spalten können über das Kontextmenü der Anmeldungslisten-Kopfzeile sowie über das
  „Einstellungen“-Knopf-Menü der Anmeldungsseite konfiguriert werden. Ein drittes Menü, auf das man
  auch dann zugreifen kann, wenn die Liste gerade gar nicht sichtbar ist, bringt keinen echten
  Mehrwert und verkompliziert den Code deswegen nur unnötig.

Korrigiert
==========

* Die Voranmeldungsseite wird jetzt korrekt aktualisiert, wenn man zwischen verschiedenen
  Datenbanken wechselt bzw. ein neues Turnier startet, nachdem schon eine Datenbank offen war.

* Die Voranmeldungsseite wird jetzt auch dann korrekt aktualisiert, wenn das Berücksichtigen von
  Voranmeldungen über ein Netzwerk geändert wird und/oder die Seite bereits geöffnet ist, bevor
  eine Verbindung hergestellt wird.

* Es wird jetzt auf allen Plattformen, die das zulassen, für das Turnier-Zeitplan-Display (und
  jetzt auch für das Auslosungs-Display) ein „Maxmimieren“- und „Schließen“-Button abgezeigt.

* Beim Erstellen neuer Markierungen bzw. beim Editieren vorhandener werden jetzt die Warnungen zu
  Namenskollisionen (wieder) korrekt angezeigt.

* Ein Wechsel des Farbschemas („Light/Dark Mode“) wird jetzt korrekt angewandt, ohne dass ein
  Neustart des Programms dafür nötig ist.

* Die Turniereinstellungen werden jetzt auch dann korrekt gespeichert, wenn nicht das Programm
  beendet oder die Datenbank explizit geschlossen wird – sondern auch dann, wenn man eine andere
  Datenbank öffnet oder eine neue anlegt, und es ist bereits eine Datenbank offen.
  Bemerkenswerterweise war das bisher nicht der Fall.

* Beim Drucken von Spielstandzetteln wird jetzt auch dann die korrekte Anzahl an Zetteln ausgegeben,
  wenn die Anzahl der Tische nicht durch die Anzahl der Zettel pro Blatt teilbar ist.

* Die Einstellungen für die verschiedenen Zetteltypen werden jetzt korrekt (in der Gruppe
  ``notes_settings``) gespeichert.

* Wenn jetzt die Frage-Box offen ist, ob ein Paar/Spieler gelöscht werden soll, und der
  entsprechende Eintrag wird von einem anderen Netzwerkteilnehmer umbenannt oder gelöscht, dann wird
  die Box jetzt automatisch geschlossen (und mit „Abbrechen“ beantwortet). Danach wird, wie bisher
  auch, die Meldung angezeigt, dass das Löschen nicht mehr möglich ist.

* Wenn der Name einer markierten Anmeldung im Netzwerkbetrieb geändert wird, dann schlägt daraufhin
  die Synchronisation nicht mehr fehl. Dieser Fehler hatte sich tatsächlich schon von Version 3.7.0
  eingeschlichen: Fälschlicherweise wurde nicht nur der Name geändert, sondern auch die Markierung
  entfernt. Das ist jetzt behoben.

* Der „Über Muckturnier“-Menüeintrag wird jetzt auch unter macOS korrekt angezeigt, egal ob eine
  Datenbank offen ist, oder nicht. Bisher wurde der korrekte „Über Muckturnier“-Eintrag nur dann
  angezeigt, wenn keine Datenbank offen war. Dafür aber nicht der „Übersicht Auslosung“-Eintrag.
  Der wiederum wurde – unter falschem Namen – als „Über Muckturnier“-Eintrag angezeigt, sobald eine
  Datenbank geöffnet war. Höchst suspekt, passend zu macOS ;-) Vermutlich verursacht durch wegen der
  deutschen Namen der Aktionen verwirrter Qt-Heuristik. Egal – jetzt funktioniert's.

====================================================================================================
Release von Muckturnier 3.8.0 (11.09.2024)
====================================================================================================

Neu
===

* Es können jetzt Tischnummernaufkleber, Auslosungszettel und Spielstandblöcke direkt aus dem
  Programm heraus als PDF generiert und ausgedruckt werden.

* Es kann jetzt eine extra Auslosung für jede Runde eingegeben werden. Damit ist es jetzt möglich,
  dass man die Paare bzw. Spieler eines Tisches, dessen Ergebnis man eingibt, direkt für die
  nächste Runde auslost. Eine Auslosung ab Runde 2 ist immer vollkommen zufällig über alle Tische,
  die Regeln für die Auslosung der 1. Runde (bei der ja die Anzahl der Tische noch nicht bekannt
  ist) bleiben unverändert. Dafür war ein Update des Datenbankschemas notwendig. Alte Datenbanken
  werden wie gewohnt aktualisiert.

* Man kann jetzt, passend zu der Möglichkeit der Auslosung pro Runde, alle Anmeldungen nach einem
  Tisch-Spielstand filtern. Dann werden nur die Paare bzw. Spieler angezeigt, die an diesem Tisch
  gesessen waren. So kann man dann direkt die jeweiligen Paare bzw. Spieler für die nächste Runde
  auslosen. Optional können auch direkt nach der Eingabe eines Spielstands alle Anmeldungen nach dem
  eben eingegeben Spielstand gefiltert werden („Einstellungen“ → „Tisch automatisch für Auslosung
  auswählen“). Dann werden die passenden Paare bzw. Spieler immer gleich auf der Anmeldungsseite
  angezeigt.

* Es wird jetzt (optional) beim Auslosen ab Runde 2 ein Hinweis angezeigt, dass ein Spieler im
  Verlauf des Turniers bereits einem anderen als Partner zugelost wurde und/oder dass der bzw. die
  Gegner schon einmal zugelost waren (in Runde 1 kann es ja noch keine „Kollisionen“ geben).

* Es kann jetzt ausgewählt werden, dass – wenn möglich – bei einer Auslosung pro Runde vermieden
  wird, dass dieselben Partner und/oder dieselben Gegner im Verlauf des Turniers mehrfach zugelost
  werden. Wenn es noch Plätze gibt, die „kollisionsfrei“ sind, dann wird die Auslosung auf diese
  beschränkt. Wenn es keine entsprechenden Plätze mehr gibt, dann wird komplett zufällig ausgelost.
  Es sind auch dann, wenn die freien Plätze gefiltert werden, immer alle noch nicht ausgelosten
  Plätze manuell auswählbar. Es wird nur (sofern aktiviert) ein Hinweis auf die „Kollision“
  angezeigt.

Geändert
========

* Die Optionen für die automatische Paar- bzw. Spielerauswahl sind jetzt nicht mehr in „Auswahl
  laut Auslosung“ und weiter in „Paar 2 rückt pro Runde“ aufgeteilt, sondern in die drei sinnvollen
  Kombinationen daraus: „Paar 1 bleibt sitzen, Paar 2 rückt weiter“, „Automatische Auswahl laut der
  Auslosung für die Runde“ und „Keine automatische Auswahl“. Bei Option 1 wird wie gehabt nur die
  Auslosung für die 1. Runde berücksichtigt, und alle weiteren Plätze ergeben sich aus den
  Spielständen der vorherigen Runde. Bei Option 2 wird die Auslosung der jeweiligen Runde für die
  Paar- bzw. Tischauswahl herangezogen. Bei Option 3 wird nichts automatisch ausgewählt.

* Die Einstellungen für die Auslosung bzw. die automatische Paar- oder Tischauswahl sind jetzt
  sowohl auf der Anmeldungsseite als auch auf der Ergebnisse-Seite einstellbar, einmal aus Sicht
  der Auslosung und einmal aus Sicht der Ergebniseingabe. Weiterhin wird die Einstellung jetzt auch
  auf der Startseite (ohne das Einblenden aller Optionen) des „Neues Turnier starten“-Dialogs
  angezeigt.

* Wenn man das Turnier-Zeitplan-Info-Display schließt und es wurden Änderungen an den Einstellungen
  vorgenommen, dann wird jetzt nachgefragt, ob die Einstellungen gespeichert werden sollen. Bisher
  wurden in diesem Fall die Änderungen stillschweigend verworfen.

* Als Bobbl-Symbol kann jetzt nur noch ein einzelnes Zeichen eingegeben werden.

Entfernt
========

* Die Option „Eingabe ohne Tischnummern“ wurde entfernt. Ergebnisse ohne Tischnummern eingeben ist
  ungeachtet dessen nach wie vor theoretisch möglich: Hierfür muss bei der Ergebniseingabe lediglich
  immer die erste freie Tischnummer ausgewählt werden (das ist das, was intern automatisch
  passierte, wenn diese Option ausgewählt war)

Korrigiert
==========

* Wenn die Einstellungen für die Auslosung der 1. Runde im laufenden Betrieb geändert werden, dann
  werden jetzt vom Auslosungs-Popup die nach den neuen Regeln verfügbaren auslosbaren Plätze
  korrekt aktualisiert.

* Wenn das Auslosungs-Popup gerade offen ist und ein anderer Netzwerkteilnehmer ändert die
  Auslosung, dann werden jetzt die verbliebenen ziehbaren Plätze und der erste freie Platz korrekt
  aktualisiert.

* Wenn das Turnier bereits gestartet wurde (also entweder die Anmeldung im Netzwerkbetrieb beendet
  oder bereits mindestens ein Ergebnis eingegeben wurde) können Anmeldungen jetzt nicht mehr als
  „Vorangemeldet“ (also abwesend) markiert werden.
  Das verhindert weiterhin, dass eine eingegebene Auslosung dann auch nicht mehr durch das (ggf.
  versehentliche) Setzen der entsprechenden Makierung automatisch gelöscht wird.

* Die letzte Position der Tab-Leiste wird jetzt auch unter Qt 6 korrekt wiederhergestellt.

* Es blitzen jetzt bei einem Qt-6-Build keine extra Fenster mehr kurz auf, wenn man
  Markierungsdefintionen ändert, die Turnier-Zeitplan-Seite öffnet, den Einstellungen-Dialog oder
  die Auslosungs-Übersicht aufruft.

* Wenn zwischen einem Qt-5- und einem Qt-6-Build hin- und hergewechselt wird, wurde statt des
  Bobbl-Symbols mitunter Zweichensalat angezeigt (da Qt 6 Unicode-Strings in den Einstellungen
  anders speichert als Qt 5). Sollte eine nicht verarbeitbare Einstellung für das Bobbl-Symbol
  gelesen werden, dann wird jetzt als Fallback das Standard-Symbol angezeigt.

* Wenn Qt 6 benutzt wird, dann stürzt das Programm jetzt nicht mehr ab, wenn die Seite „Übersicht
  Auslosung“ eingeblendet wird, aber noch keine Auslosung eingegeben wurde.

====================================================================================================
Release von Muckturnier 3.7.1 (22.03.2024)
====================================================================================================

Geändert
========

* Der WLAN-Code-Scanner-Server akzeptiert jetzt neben ``GET``- auch ``POST``-Anfragen. Für ``POST``
  kann als Content-Type entweder ``application/x-www-form-urlencoded`` oder ``application/json``
  benutzt werden.

* Der Parametername für den Datensatz eines Anmeldungscodes, den eine Handy-App per HTTP überträgt,
  ist jetzt konfigurierbar (und nicht mehr hard-coded als ``content``).

Korrigiert
==========

* Der WLAN-Code-Scanner-Server verhält sich jetzt zumindest annähernd konform zu RFC 7230. Z. B.
  schickt er jetzt beim Antworten auf eine Anfrage einen ``Date``-Header mit dem Zeitpunkt der
  Antwort, lehnt ungültige Anfragen mit den passenden Statuscodes ab, verarbeitet Header-Feldnamen
  unabhängig von Groß- und Kleinschreibung etc.

====================================================================================================
Release von Muckturnier 3.7.0 (06.03.2024)
====================================================================================================

Neu
===

* Es gibt jetzt die neue optionale Seite „Voranmeldung“. Hier können neben dem eigentlichen
  Voranmelden von Paaren bzw. Spielern auch QR-Codes erzeugt werden. Diese kann man dann z. B. per
  E-Mail verschicken und später bei der Anmeldung (direkt von einem Smartphone-Display) scannen.
  Damit ist dann die Anmeldung mit einem „Pieps“ und einem Klick erledigt :-)

* Passend zu den neuen Anmeldungscodes gibt es jetzt einen WLAN-Code-Scanner-Server, mit dem sich
  eine Smartphone-Barcode-Scanner-App wie `Binary Eye <https://github.com/markusfisch/BinaryEye>`_
  unterhalten kann. Damit kann man dann ein Handy zum Scannen der QR-Codes benutzen, und muss
  keinen Hardware-Barcode-Scanner kaufen.

* Es kann jetzt eine (eindeutige) Paar- bzw. Spielernummer als zusätzliches Identifikationsmerkmal
  vergeben werden. Wenn nach einer Nummer gesucht wird, dann wird, sofern es die Nummer gibt, nur
  das passende Paar bzw. der passende Spieler gelistet. So kann z. B. bei Einzelspielerturnieren,
  bei denen in jeder Runde neu ausgelost wird, die Auswahl der Spieler bei der Ergebniseingabe
  allein über eine Spielernummer erfolgen.

* Auf der Anmeldeseite wird jetzt bei Einzelspielerturnieren die Spielernummer (1 oder 2) der
  Auslosung angezeigt.

* Bei Einzelspielerturnieren kann man jetzt auf der Ergebnisse-Seite (via Kontextmenü) für ein
  bereits eingegebenes Ergebnis die Spieler 1 und 2 eines Paares vertauschen, ohne das Ergebnis
  editieren und die Spieler manuell anders auswählen zu müssen.

* Auf der Ergebnisse-Liste, der Anmeldungen-Liste und der Markierungen-Liste wird jetzt die Zeile
  unter dem Mauszeiger hervorgehoben. Diese Hervorhebung bleibt auch während einer Interaktion
  (Kontextmenü, Popup) bestehen.

* Ein gerade auf der Ergebnisse-Seite eingegebenes Ergebnis wird jetzt in der Liste hervorgehoben.

* Der Disqualifikationsstatus eines Paars bzw. Spielers wird jetzt auch in den Auswahlboxen bei der
  Ergebniseingabe angezeigt (der Zusatz „(disqualifiziert)“ wird aber bei der Suche nicht
  berücksichtigt). Ebenso wird der Disqualifikationsstatus jetzt beim Netzwerk-Vergleich von
  Spielständen angezeigt.

* Beim Zuweisen eines Einzelspielers zu einem anderen (um ein Paar zu bilden) wird jetzt eine
  Warnung angezeigt, falls der Spieler eine Auslosung und/oder Paarnummer hat und diese durch das
  Zuweisen verworfen wird/werden.

* Das Info-Display des Turnierzeitplans hat jetzt neben der Zeitanzeige noch eine „Willkommen“- und
  eine „Auf wiedersehen“-Seite, auf der ein entsprechendes Bild angezeigt werden kann. Die Anzeige
  kann auch über den „Info-Display“-Knopf auf der Zeitplan-Seite umgeschaltet werden, so dass man
  den Mauszeiger nicht auf einen zweiten Bildschirm bewegen muss.

* Für das Info-Display kann man jetzt sowohl die Textfarbe als auch die Schriftart für jede
  Textzeile einzeln (und auch für alle gleichzeitig) einstellen. Eine zusätzliche Zeile mit einem
  (optional auch ausblendbaren) Muckturnier.org-Logo ist ebenfalls dazugekommen.

Geändert
========

* Die Revision der Datenbank wurde auf Version 12 aktualisiert. Die automatische Aktualisierung
  älterer Turnierdatenbanken wird wie immer beim Öffnen angeboten.

* Die Revision des Netzwerkprotokolls wurde auf Version 6 aktualisiert.

* Die Ergebnisse-Seite und die Übersicht-Auslosung-Seite haben jetzt auch je ein eigenes Datenmodell
  mit zugehöriger Anzeige bekommen. Die Vorteile sind dieselben wie bei der kürzlichen Einführung
  von Datenmodellen für die Anmeldung, die Markierungen und die Rangliste: Übersichtlicherer Code,
  weniger Datenduplikation und sauberere Implementierung.

* Wenn die Markierung für „Abwesend“ bzw. das Berücksichtigen allein gekommener Spieler aktiviert
  werden, dann wird jetzt jeweils automatisch die passende Markierung ausgewählt, sofern die
  voreingstellten Standard-Markierungen benutzt werden.

* Markierungen, die für Voranmeldungen bzw. allein gekommene Spieler ausgewählt sind, können jetzt
  nicht mehr gelöscht werden.

* Beim Sortieren (von z. B. Anmeldungen) wird jetzt bei Zahlen die „natürliche“ Sortierreihenfolge
  benutzt. Also z. B. 1, 2, 3, 10, 11 anstatt bisher 1, 10, 11, 2, 3.

* Das Einstellungen-Menü des Info-Displays kann jetzt einfach per Kontextmenü (Rechtsklick) geöffnet
  werden. Der Menü-Knopf wurde entfernt, somit gibt es jetzt keine sichtbaren Bedienelemente mehr.

Korrigiert
==========

* Die ausgeloste Spielernummer innerhalb eines Paars (immer 1 für feste Paare, für Einzelspieler
  entweder 1 oder 2) wird jetzt bei Verwendung eines Netzwerks korrekt synchronisiert.

* Beim Vergleich der eingegebenen Ergebnisse übers Netzwerk ist es jetzt egal, ob ein Paar als
  Paar 1 oder als Paar 2 eingegeben wurde, und auch, ob ein Paar bei einem Einzelspielerturnier aus
  „Spieler 1 / Spieler 2“ oder aus „Spieler 2 / Spieler 1“ besteht.
  In allen diesen Fällen sind die erreichten Punkte gleich, und auch die Rangliste ist dieselbe
  (und darum geht es ja). Folgerichtig werden solche Ergebnisse auch bei unterschiedlicher
  Paarkonfiguration jetzt als „identisch“ angezeigt.

* Markierungen, Disqualifikationen und Auslosungen wurden beim Verbinden mit einem Server nur dann
  übernommen, wenn sie am Server gesetzt waren – aber nicht entfernt, wenn sie zwar am Client
  gesetzt waren, aber nicht am Server. Das führte in dem Fall zu einem Fehlschlagen der
  Synchronisation.
  Jetzt werden alle Markierungen, Auslosungen und Disqualifikationen korrekt synchronisiert.

* Beim Wechseln von Datenbanken werden jetzt die angezeigten Spalten der Anmeldungsseite korrekt
  wiederhergestellt und alle Auswahlboxen zur Steuerung der Sichtbarkeit der Spalten entsprechend
  an- bzw. abgewählt.

* Beim Bereinigen der Auslosung wird jetzt die Spielernummer (korrekt) gesetzt.

* Wenn bei Einzelspielerturnieren eine Auslosung als Spieler 1 eines Paars gelöscht wird, dann wird
  der zugehörige Spieler 2 dieses Paars (sofern es einen gibt) automatisch zum Spieler 1.

* In der Auslosungsübersicht werden jetzt gelöschte Auslosungen korrekt entfernt.

* In der Auslosungsübersicht werden jetzt die Spieler in der korrekten Reihenfolge, entsprechend der
  zugelosten Spielernummer innerhalb des Paars, angezeigt.

* Es können jetzt nicht mehr dieselben Markierungen für „Vorangemeldet“ und „Allein da“ ausgewählt
  werden. In der Folge gibt es jetzt auch keine unsinnigen Statustexte auf der Anmeldeseite für
  diesen (faktisch ja gar nicht möglichen) Fall mehr.

* Wenn der Trenner für Paarnamen bei Einzelspielerturnieren geändert wird während eine
  Turnierdatenbank geöffnet ist und die Auslosungs-Übersicht-Seite offen ist, dann wird diese jetzt
  mit dem neuen Paartrenner korrekt aktualisiert.

* Wenn eine Auslosung über das Kontextmenü gelöscht wird (und nicht über das Auslosungs-Popup), dann
  wird jetzt ebenfalls überprüft, ob es offene Runden gibt und ggf. eine Warnung angezeigt.

* Das Scrollen zu gerade eingegebenen Auslosungen auf der Übersicht-Auslosung-Seite funktioniert
  jetzt auch bei Einzelspielerturnieren (mit zusammengesetzten Paarnamen) korrekt.

* Beim Update auf die Datenbankversion 12 werden jetzt für alle Auslosungen korrekte Spielernummern
  vergeben. Für aktualisierte alte Datenbanken bei Einzelspielern mit Auslosung wurde in Version
  3.6.0 fälschlicherweise immer die Spielernummer 0 angezeigt.

* Wenn jetzt ein Turnier als „abgeschlossen“ oder „offen“ markiert wird, dann wird nicht mehr
  gefragt, ob die „Übersicht Auslosung“-Seite angezeigt werden soll, wenn diese nicht offen ist.

====================================================================================================
Release von Muckturnier 3.6.0 (02.01.2024)
====================================================================================================

Neu
===

* Wenn ein Paar bzw. Spieler auf der Anmeldeseite ausgewählt ist, dann wird der entsprechende
  Eintrag jetzt mit einem Rahmen hervorgehoben. Der Rahmen bleibt auch nach einer Bearbeitung
  erhalten (natürlich nur, wenn der Eintrag nicht gelöscht wurde). Weiterhin wird der ausgewählte
  Eintrag jetzt auch dann angezeigt, wenn er durch eine Bearbeitung jetzt eigentlich ausgeblendet
  wäre.

* Es kann jetzt bei der Anmeldung direkt bei der Eingabe der Namen bzw. des Namens die Anmeldeliste
  durchsucht werden. Das ist insbesondere dann praktisch, wenn es Voranmeldungen gibt: Die
  Eingabe wird dann erst zum Suchen einer evtl. vorhandenen Voranmeldung benutzt, und wenn es keine
  passende gibt, gleich auch zum Anmelden.

* Der „Paar-/Spielerliste exportieren“-Dialog zeigt jetzt die Namen mit den entsprechenden
  Markierungsfarben an. Die Markierungen haben jetzt auch dieselbe Reihenfolge wie auf der
  Anmeldeseite (die Liste wird aber trotzdem weiterhin alphabetisch sortiert, entsprechend der
  Ausgabe).

* Für Feste-Paare- und Einzelspieler-Turniere wird jetzt die Vorlage für Markierungen jeweils
  separat gespeichert.

* Auf der Anmeldeseite kann jetzt eine automatische Groß- und Kleinschreibung der eingegebenen Namen
  aktiviert werden. Die Eingabe „karl-theodor von und zu guttenberg / valérie giscard d'estaing“
  würde damit z. B. automatisch zu „Karl-Theodor von und zu Guttenberg / Valérie Giscard d'Estaing“
  korrigiert werden.

* Es gibt jetzt eine neue Extra-Seite: Den Turnier-Zeitplan. Damit kann man den Ablauf des Turniers
  planen, und sieht jederzeit, wie man in der Zeit liegt. Außerdem gibt es die Möglichkeit, ein
  Info-Display anzuzeigen (auf einem zweiten Bildschirm, Beamer etc.), wo gut sichtbar die momentan
  laufende Runde, die verbleibende Zeit und der Start der nächsten Runde angezeigt werden.

* Wenn man mit Voranmeldungen arbeitet, kann man jetzt ein Paar bzw. einen Spieler direkt bei der
  Suche nach dem dem Namen als „gekommen“ markieren. Wenn die Suche während der Eingabe nur einen
  Treffer liefert, und dieser Treffer ist als „abwesend“ (also vorangemeldet) markiert, dann wird
  beim Anmelden der Eingabe gefragt, ob die Markierung für diesen Treffer auf „neue Anmeldung“
  geändert werden soll.

Geändert
========

* Sowohl für die Anmeldungen, als auch für die Markierungen und die Rangliste kommt jetzt je ein
  Datenmodell mit zugehöriger Anzeige zum Einsatz. Das macht zum einen die Codebasis deutlich
  sauberer und übersichtlicher, und zum anderen sorgt es für deutlich weniger Daten-Duplikation.

* Als Standardvorlage für Markierungen wird jetzt der Satz Markierungen benutzt, der sich im
  Realeinsatz für das Verwalten von Voranmeldungen, allein gekommener Spieler und der Auslosung
  bei der Anmeldung schon vielfach bewährt hat. Einzelspieler-Turniere bekommen eine abgewandelte
  Vorlage, da hier „allein da“ nicht sinnvoll anzuwenden ist.
  Ungeachtet dessen wird natürlich eine individualisierte Vorlage benutzt, sofern eine gepeichert
  wurde.

* Wenn ein anderer Netzwerkteilnehmer jetzt eine Auslosung für ein Paar bzw. einen Spieler eingibt,
  für das gerade eine Auslosung eingegeben werden soll, dann wird nicht mehr das Popup aktualisiert,
  sondern es wird eine Warnung angezeigt, und das Popup wird geschlossen (denn der andere
  Netzwerkteilnehmer hat ja schon ausgelost).

* Beim Verwenden von vertikalen Spielstandzetteln und mehr als zwei Bobbln wird jetzt versucht, alle
  Bobbl ohne Scrollbar anzuzeigen, sofern der Platz dafür ausreicht. Bisher wurde immer der Platz
  für zwei Bobbl angezeigt, und immer Scrollbars bei mehr als zwei.

Korrigiert
==========

* Beim Anlegen neuer Markierungen werden bei der Suche nach gleich benannten vorhandenen
  Markierungen jetzt auch Umlaute etc. korrekt berücksichtigt.

* Wenn das „Markieren“-Popup geöffnet ist, und ein anderer Netzwerkteilnehmer editiert Markierungen,
  dann wird jetzt das Popup korrekt aktualisiert.

* Beim Zuweisen einer Auslosung wurde bisher nicht kontrolliert, ob zwischenzeitlich ein anderer
  Netzwerkteilnehmer die angefragte Auslosung vergeben hat. Potenziell hätte also bisher dieselbe
  Auslosung an zwei Paare bzw. Spieler vergeben werden können. Das wird jetzt geprüft und im Falle
  einer Kollision wird das Setzen der Auslosung abgelehnt.

* Wenn das Zuweisen einer Auslosung angefragt wird, und gleichzeitig ein anderer Netzwerkteilnehmer
  (schneller) eine Auslosung für das gerade bearbeitete Paar bzw. den gerade bearbeiteten Spieler
  eingegeben hat, dann wird diese Auslosung nicht mehr einfach überschrieben, sondern die Änderung
  abgelehnt.

* Die Berechung der sichtbaren Zeilennummer, die für das Wiederherstellen der Anmeldeseite nach
  einer Netzwerkänderung benötigt wird, berücksichtigt jetzt auch durch eine Suche ausgeblendete
  Zeilen.

* Wenn das Auslosungs-Popup auf der Anmeldeliste über das Kontextmenü aufgerufen wird, dann wird es
  jetzt an der Position des urprünglichen Mausklicks angezeigt (auf Höhe der Zeile, die bearbeitet
  wird) und nicht mehr auf Höhe des entsprechenden Menüeintrags (außerhalb der Zeile).

* Beim Disqualifizieren bzw. Zurücknehmen einer Disqualifikation werden jetzt kollidierende
  Netzwerkänderung abgefangen (wenn das Paar bzw. der Spieler zwischenzeitlich gelöscht oder
  umbenannt wurde).

* Wenn Einstellungen geändert und gespeichert werden, aber keine Turnierdatenbank geöffnet ist,
  stürzt das Programm jetzt nicht mehr ab.

* Wenn im Einzelspielermodus ausgelost wird, werden jetzt bei der automatischen Tisch- bzw.
  Spielerauswahl die Spielernamen in derselben Reihenfolge innerhalb des Paars ausgewählt und
  angezeigt wie auf der Auslosungsübersicht.

* Wenn im Einzelspielermodus bei aktivierter automatischer Tisch- bzw. Spielerauswahl der Name eines
  Spielers Nr. 1 in der zweiten Auswahlbox eingegeben wurde (oder entsprechend der des zweiten
  Spielers in der ersten), der Tisch aber bereits ausgewählt war, dann blieb der eigentlich in die
  andere Auswahlbox gehörende Name stehen und wurde doppelt angezeigt. Das ist jetzt korrigiert.

Entfernt
========

* Die verbliebenen „Was ist das?“-Texte wurden in die vorhandenen Tool-Tips integriert und der
  „Was ist das?“-Eintrag aus dem Menü entfernt. Dieses Feature ist vermutlich den meisten Usern
  nicht geläufig, und wurde auch nur sporadisch genutzt.

* Der Prüfsummen-Dialog wird jetzt nur noch bei einem Debug-Build gebaut und im Menü angezeigt.

====================================================================================================
Release von Muckturnier 3.5.1 (13.02.2023)
====================================================================================================

Neu
===

* Es gibt jetzt die Möglichkeit, via ``Q_LOGGING_CATEGORY`` Warnungen bzw. Debugging-Meldungen auf
  der Konsole auszugeben. Warnungen werden immer ausgegeben, Debugging-Meldungen nur, wenn beim
  Bauen ``-DCMAKE_BUILD_TYPE=Debug`` gesetzt war (resp. ``#define DEBUG_MODE`` im Header
  ``debugMode.h`` definiert ist).

Geändert
========

* Der ChangeLog nutzt ab jetzt das von `keepachangelog.com <https://keepachangelog.com/>`_
  vorgeschlagene Format.

Entfernt
========

* Die Scripts ``changelog2web.pl`` und ``readme2web.pl`` wurden entfernt, da sie nichts mit einem
  Release von Muckturnier selbst zu tun haben, und somit in das Repository der Homepage gehören.

Korrigiert
==========

* Beim Erstellen eines Backups wurden alle seit dem Erstellen bzw. dem letzten Schließen der
  Datenbank zur Laufzeit festgelegten Parameter (automatische Markierungen, das Behandeln allein
  gekommener Spieler etc.) nicht gespeichert. Nach einem erneuten Öffnen der Datenbank wurden wieder
  die Voreinstellungswerte gesetzt. Das ist jetzt korrigiert.

* Wenn eine nicht-sequenzielle Auslosung eingestellt war, wurden die Einstellungen bei gewissen
  Konstellationen beim Öffnen der Datenbank (bzw. nach einem Backup) nicht richtig geladen, sondern
  die Standardeinstellungen für die Auslosung gesetzt. Jetzt sollten alle Einstellungen für die
  Auslosung in allen Fällen korrekt wiederhergestellt werden (sofern die Datenbank nicht manuell
  verändert wurde und unsinnige Werte enthält).

* Bisher war bei den Einstellungen für die Auslosung die Voreinstellung für die maximale Tischanzahl
  immer 1, solang diese Option nicht ausgewählt war. Wurde die maximale Tischanzahl aktiviert,
  nachdem man bereits andere Einstellungen vorgenommen hatte, wurden diese meistens verändert bzw.
  deaktiviert. Jetzt wird die maximale Tischanzahl automatisch auf einen sinnvollen Wert
  voreingestellt, solang die Option nicht aktiviert ist, damit beim Aktivieren die anderen
  Einstellungen nicht verändert werden.

====================================================================================================
Release von Muckturnier 3.5.0 (03.08.2022)
====================================================================================================

* Neu: Im Netzwerkbetrieb kann jetzt jeder Client eine Stoppuhr mit der des Servers abgleichen, so
  dass sie dieselben Einstellungen hat bzw. synchron läuft. Sind am Server mehrere Stoppuhren
  geöffnet, kann eine zum Synchronisieren ausgewählt werden.

* Änderung: Da jetzt der ständige Ergebnisvergleich auch am Server funktioniert, und auch vom Server
  aus die Unterschiede der Ergebnisse mit allen Clients angezeigt werden können, hat jetzt der
  Ranglisten-Vergleichs-Dialog endgültig ausgedient. Schließlich geht es ja um das Finden von
  Eigabefehlern, und das geht über den Ergebnisvergleich deutlich besser, als über die Rangliste.
  Folgerichtig wurde der Ranglisten-Vergleichs-Dialog entfernt.

* Neu: Es können jetzt auch vom Server aus Rundenergebnisse mit den Clients verglichen werden.

* Neu: Es wird jetzt auch am Server angezeigt, ob die an den Clients eingegebenen Ergebnisse mit den
  lokalen übereinstimmen.

* Bugfix: Beim Vergleichen von Ergebnissen werden jetzt auch die gewählten Tischnummern verglichen,
  nicht nur die Ergebnisse.

* Verbesserung: Der „Schließen“-Knopf nimmt jetzt auf der „Übersicht Auslosung“-Seite keine ganze
  Spalte mehr in Anspruch, was auf Displays mit geringerer Auflösung für deutlich mehr Platz sorgt.

* Verbesserung: Wenn schon Auslosungen eingegeben wurden, und angefragt wird, dass die „Auslosung“-
  Spalte ausgeblendet werden soll, wird jetzt darauf hingewiesen und gefragt, ob sie wirklich
  ausgeblendet werden soll.

* Verbesserung: Wenn eine laufende Stoppuhr geschlossen werden soll, wird jetzt nachgefragt, ob sie
  wirklich geschlossen werden soll.

* Neu: Die Vorlage für Markierungen, Markierungsauswahl (für neue Anmeldungen, abwesende
  Voranmeldungen etc.) und die Einstellungen für allein gekommene Spieler werden jetzt auch im
  „Neues Turnier starten“-Dialog angezeigt, mit dem Verweis auf die Möglichkeit des Erstellens
  einer entsprechenden Vorlage über die Einstellungen.

* Bugfix: Auslosungen sowie das Setzen oder Löschen der Auslosung für die komplette Anmeldeliste
  werden jetzt bei der Verwendung eines Netzwerks korrekt und in beide Richtungen zwischen Server
  und Clients synchronisiert.

* Bugfix: Die Ergebniseingabe funktioniert jetzt auch dann wieder richtig, wenn „Automatische
  Auswahl“ → „Auswahl laut Auslosung“ nicht ausgewählt ist. Triviale Ursache, aber fundamentale
  Funktionalität war kaputt: Ohne einen Haken da konnte man entweder gar keine Ergebnisse eingeben,
  oder alle – incl. ungültiger (z. B. zwei mal dasselbe Paar ausgewählt).

* Bugfix: Die Option „Automatische Auswahl“ → „Paar 2 rückt pro Runde weiter“ wird jetzt, abhängig
  von der Option „Auswahl laut Auslosung“, korrekt ausgegraut und freigeschaltet.

* Verbesserung: Ein HTML-Export enthält jetzt ein „@media print“-Style-Sheet, um beim Ausdrucken
  das Druckbild zu verbessern (Schriftgröße, Seitenränder etc.).

* Neu: Das Ein- und Ausblenden der Spalten der Anmeldungsliste sowie der Kopfzeile ist jetzt auch
  über das Hauptmenü möglich.

* Neu: Es ist jetzt möglich, die Rangliste direkt auszudrucken. Hierfür wird automatisch ein
  entsprechend formatierter HTML-Export in eine temporäre Datei angestoßen, die dann mit dem
  Systemwebbrowser geöffnet wird. Das Drucken selbst übernimmt der Browser. Die Exportdatei wird
  nach dem Schließen des entsprechenden Dialogs automatisch gelöscht.

* Bugfix: Wenn eine Datenbank geöffnet wird, in der es eine nicht abgeschlossene Runde gibt,
  funktionierte die automatische Paar- bzw. Spielerauswahl erst, nachdem eine andere Runde und dann
  wieder die offene Runde geöffnet wurde. Das ist jetzt behoben.

* Bugfix: Wenn versucht wird, eine Datei anzulegen, die nicht geschrieben werden kann, dann wird
  jetzt nicht mehr fälschlicherweise behauptet, dass die Datei eine Turnierdatenbank ist, die durch
  eine andere Instanz gesperrt ist. Außerdem wird jetzt auch der korrekte Dateiname für eine
  Lock-Datei im Root-Verzeichnis generiert.

* Änderung: Der Menüeintrag „Alle Plätze auslosen“ ist jetzt immer auswählbar. Wenn das Auslosen
  (noch) nicht möglich ist, dann wird dem Benutzer angezeigt, wieso.

* Änderung: Die „Auslosung“-Spalte wird auf der Anmelde-Seite beim Anlegen eines neuen Turniers
  jetzt standardmäßig eingeblendet (sofern die Vorlage für neue Turniere nichts anderes vorgibt).

* Änderung: Bei ausblendbaren Frage-Message-Boxen ist jetzt „Nicht mehr nachfragen“ nicht mehr
  per Voreinstellung angehakt. Man muss es also jetzt bewusst bestätigen, dass nicht mehr
  nachgefragt werden soll.

* Änderung: Es wird jetzt Qt 5.15 für das Kompilieren von Muckturnier benötigt.

* Bugfix: Der Startmenü-Eintrag für das Handbuch funktioniert unter Windows jetzt wieder.

* Neu: Muckturnier kann jetzt auch mit Qt 6 gebaut werden (ohne die Qt-5-Kompatibilität zu
  verlieren).

* Änderung: Der Prüfsummen-Dialog zeigt jetzt nur noch die Prüfsumme der Paar-/Spielerliste und die
  der aktuellen Rangliste. Das Berechnen von Teil-Prüfsummen für die Rangliste wurde entfernt. Der
  Abgleich der eingegebenen Ergebnisse ist mittlerweile durch den kontinuierlichen Vergleich der
  Server/Client-Funktionalität gegeben, und der Prüfsummen-Dialog ist jetzt eigentlich nur noch ein
  kleiner Helfer für die Entwicklung. Also weg mit dem alten Code ;-)

* Bugfix: Wenn ein Turnier neu angelegt, und vor dem ersten Schließen der Datenbank bzw. des
  Programms als „abgeschlossen“ markiert wurde, wurden geänderte Turniereinstellungen (incl. des
  „Abgeschlossen“-Status) nicht gespeichert. Nach einem erneuten Öffnen der Datenbank wurden die
  ursprünglich gesetzten Einstellungen wiederhergestellt, und das Turnier war wieder als „offen“
  markiert. Nun werden die Einstellungen in jedem Fall korrekt gespeichert.

* Änderung: Die Standard-Dateierweiterung für den HTML-Export ist jetzt „.html“ (statt bisher
  „.htm“).

* Neu: Es können jetzt Paare bzw. Spieler disqualifiziert werden. Die Eingabe von Ergebnissen ist,
  damit der normale Turnierablauf weitergehen kann, noch möglich (potenziell mit Dummy-Spielern von
  der Turnierleitung), aber das entsprechende Paar bzw. der entsprechende Spieler taucht ab der
  Runde der Disqualifikation nicht mehr in der Rangliste auf.
  Hierfür war ein Update der Datenbankrevision nötig (auf dbv10). Beim Öffnen älterer Datenbanken
  wird, wie gewohnt, ein automatisches Update angeboten.

* Änderung: Beim Export der Turnierdaten ins CSV-Format wird eine CSV-Datei mit Tabulatoren als
  Feldtrennern erzeugt. Das ist faktisch dann eine TSV-Datei. Die Datei wird jetzt auch so genannt,
  damit ein Tabellenkalkulationsprogramm gleich weiß, welche Feldtrenner genutzt werden.

====================================================================================================
Release von Muckturnier 3.4.1 (17.01.2021)
====================================================================================================

* Bugfix: Wenn auf der Ergebnisse-Seite im Einstellungen-Menü „Automatische Auswahl“ → „Auswahl
  laut Auslosung“ deaktiviert war, konnte man keine Runden mehr auswählen (ein äußerst suspekter
  Fehler!). Das ist jetzt behoben.

* Bugfix/Neu: Über spezielle Markierungen (für allein gekommene Spieler bzw. abwesende Anmeldungen)
  wird jetzt auch dann informiert, wenn sie von einem anderen Netzwerkteilnehmer aktiviert werden.

* Bugfix: Wenn die Markierung für abwesende Anmeldungen an einem Server geändert wurde, nachdem
  bereits Clients angemeldet waren, schlug dort die Synchronisation fehl. Das ist jetzt behoben.

* Bugfix: Die ausgewählte Markierung für abwesende Anmeldungen wird jetzt nur noch dann in die
  Prüfsumme mit einbezogen, wenn sie aktiviert ist. Wenn bei einem Server eine andere Markierung
  ausgewählt war, als bei einem Client, die Abwesende-Anmeldungen-Markierung aber nicht aktiviert
  war, schlug bisher die Synchronisation aufgrund eines Prüfsummenfehlers fehl.

* Bugfix/Neu: Unter Linux und macOS unterstützt Qt standardmäßig einen dunklen Style (der von
  Windows wird seitens Qt derzeit noch nicht offiziell unterstützt). Bedingt durch fehlende dafür
  ausgelegte Icons bzw. hart kodierte Farbwerte, die für eine helle Anzeige ausgelegt waren, waren
  mache Bereiche bzw. Dialoge kaum les- und nutzbar. Das ist jetzt behoben.

====================================================================================================
Release von Muckturnier 3.4.0 (01.09.2020)
====================================================================================================

* Verbesserung: Wenn ein Client jetzt eine Anmeldung anfordert, und diese aufgrund einer Änderung
  in der Zwischenzeit fehlschlägt, dann bleibt der eingegebene Name jetzt stehen, und muss für eine
  erneute Anfrage nicht nochmals eingegeben werden.

* Bugfix: Das Begrüßungs-Bild des Windows-Installers wird jetzt auch auf Windows 10 angezeigt.

* Bugfix: Ausgeblendete Anmeldungen werden jetzt bei der Suche auf der Anmeldeseite nicht mehr als
  Treffer aufgelistet. Stattdessen wird darauf hingewiesen, wenn es ausgeblendete Treffer gibt.

* Änderung: Der Quelltext ist jetzt nicht mehr unter „GPLv2“, sondern unter „GPLv3 or later“
  lizenziert.

* Neu: Wenn ein Server oder Client läuft, dann wird jetzt der Netzwerkstatus und die Kommunikation
  (Senden und Empfangen von Daten) in der Statusleiste angezeigt.

* Bugfix: Wenn bei einem Server oder Client die Bedingung für allein gekommene Spieler dadurch
  geändert wurde, dass der Trenner für Paarnamen in den Einstellungen editiert wurde, war danach
  keine Verbindung mehr möglich (es trat ein Prüfsummenfehler auf). Das ist jetzt behoben.

* Änderung: Die Prüfsummen-Funktion wurde überarbeitet, da es bei der bisherigen Implementierung
  evtl. Kollisionen geben könnte. Die im Prüfsummen-Dialog angezeigten Prüfsummen liefern daher
  jetzt bei denselben Daten andere Prüfsummen als die früherer Versionen.

* Neu: Wenn ein Netzwerk benutzt wird, dann werden die eingegebenen Ergebnisse jetzt ständig
  verglichen. Jeder Client zeigt den Zeitpunkt der letzten Änderung am Server an und überprüft nach
  jeder client- oder serverseitigen Änderung, ob die eingebenen Ergebnisse der momentan offenen
  Runde mit der des Servers übereinstimmen oder nicht. Welche Ergebnisse abweichen, kann jetzt
  direkt per Dialog angezeigt werden.

* Bugfix: Wenn während des Editierens eines Namens eine Warnung wegen eines ähnlichen Namens
  angezeigt wurde, und ein anderer Netzwerkteilnehmer währenddessen diesen Namen ebenfalls
  editierte, stürzte das Programm nach dem Schließen der Warnung ab (unabhängig davon, ob mit „Ja“
  oder „Nein“ geantwortet wurde). Das ist jetzt behoben.

* Optimierung: Die Prüfsumme der Anmeldeseite wird jetzt gecachet, und immer dann aktualisiert, wenn
  sich die Anmeldeliste ändert. Dadurch muss jetzt für das Anfragen eines Ranglistenvergleichs die
  Prüfsumme nicht immer wieder neu berechnet werden, obwohl durch diese Netzwerkanfrage gar keine
  Änderung stattfindet.

* Neu/Verbesserung: Wenn allein gekommene Spieler gesondert behandelt werden, dann werden jetzt die
  entsprechenden Markierungen auch beim manuellen Editieren des/der Namen/s gesetzt bzw. entfernt.

* Änderung: Die Spalten der Anmeldeseite werden jetzt nicht mehr in einem assoziativen Array,
  sondern in einer Liste gespeichert und verarbeitet. Im Zuge des Updates auf dbv9 werden die
  Einstellungen älterer Datenbanken konvertiert. Die Vorlage für neue Datenbanken wird beim ersten
  Speichern der Vorlage aktualisiert und die Liste dort hin verschoben.

* Änderung: Das Linux-Build-System erstellt die Datei version.h jetzt nicht mehr im Quellcode-
  Verzeichnis, sondern im Build-Verzeichnis. Somit wird beim Kompilieren der Git-Version nichts
  mehr außerhalb des Build-Verzeichnisses verändert.

* Änderung: Es werden jetzt generierte PNG-Dateien für die Icons und Bilder statt bisher SVG
  benutzt. Zum einen kann man die Dateien für die Bildschirmdarstellung optimieren, und zum anderen
  hängt Muckturnier jetzt nicht mehr von QtSvg ab.

* Bugfix: Beim Verbinden mit einem Server mit bereits eingegebenen Ergebnissen und unterschiedlichen
  Annmeldungslisten konnte unter gewissen Umständen eine Endlosschleife auftreten. Das ist jetzt
  behoben.

* Neu: Geöffnete optional anzuzeigende Seiten (momentan ist nur die Auslosungs-Übersichts-Seite)
  werden jetzt in der Datenbank gespeichert und beim Öffnen automatisch wiederhergestellt.

* Bugfix: Das Wiederherstellen des „Markieren“- und „Auslosung setzen“-Dialogs nach
  Netzwerkänderungen funktioniert jetzt richtig. Es sollte dabei keine Crashes mehr geben, und
  abgesehen davon wird jetzt auch der Mauszeiger an die richtige Stelle verschoben, wenn der
  bearbeitete Eintrag nach der Änderung eine andere Position in der Anmeldeliste hat.

* Bugfix: Es ist jetzt nicht mehr möglich, ungültige Ergebnisse mit undefinierten Paaren mittels
  der automatischen Paarauswahl und der Auslosung zu speichern. Wenn man ein Ergebnis gespeichert
  hatte, und danach eines der bereits eingegebenen Paare einem anderen Tisch zuloste, dann wurde
  per automatischer Auswahl beim Auswählen dieses Tisches eine ungültige Paarauswahl gesetzt.
  Beim Speichern dieses Ergebnisses stürzte das Programm dann ab.

* Änderung: Es wird jetzt mindestes Qt 5.11 benötigt. außerdem kommt ab jetzt der C++17-Standard
  zum Einsatz.

* Bugfix: Eine Netzwerkverbindung mit einem Server, der ein anderes Gebietsschema nutzt, als der
  Client, ist jetzt wieder möglich.

* Verbesserung: Durch korrektes Benutzen der C++11-for-Schleifen sollte es jetzt keine eigentlich
  unnötigen detach()-Aufrufe und Deep Copys für Qt-Container mehr geben.

* Verbesserung/Bugfix: Ein Lockfile wird nur noch dann angelegt, wenn eine Datenbank angelegt wird,
  oder eine soeben geöffnete Datenbank beschreibbar ist. Weiterhin wird das Lockfile jetzt im selben
  Verzeichnis angelegt, in dem auch die Datenbank gespeichert ist. Das stellt sicher, dass die
  Datenbank auch dann nicht von zwei Instanzen gleichzeitig schreibend geöffnet wird, wenn sie
  auf einem Netzwerklaufwerk liegt, und die Instanzen auf verschiedenen Rechnern laufen.

* Verbesserung: Es wird jetzt beim Neuanlegen und beim Öffnen einer nicht schreibgeschützten
  Datenbank geprüft, ob eine „Exclusive Transaction“ gestartet werden kann. Andernfalls ist die
  Datenbank gesperrt und kann schreibend nicht benutzt werden. Das passiert z. B. dann, wenn dies
  durch einen anderen Prozess, der auf dieselbe Datei zugreift, bedingt ist (was an sich nicht
  passieren sollte), aber auch dann, wenn man versucht, eine SQLite-Datenbank auf einer Windows-
  Netzwerk-Freigabe bzw. einem Samba-/CIFS-Share zu erstellen oder zu öffnen.

* Verbesserung: Die Implementierung der Match-Funktion der Suche wurde überarbeitet und kommt jetzt
  ohne reguläre Ausdrücke aus. Weiterhin müssen weniger Varianten verglichen werden, weil die Wörter
  nicht mehr sortiert werden müssen.
  Unterm Strich resultiert das in einer erheblichen Steigerung der Geschwindigkeit (ca. um den
  Faktor 30!), was sich insbesondere beim Import einer langen Paar- bzw. Spielerliste bemerkbar
  macht.

* Neu: Die Suchfunktion unterstützt jetzt optional auch eine phonetische Suche mittels des
  „Kölner Phonetik“-Algorithmus. Damit ist eine effektive Suche mit falsch geschriebenen aber
  ähnlich klingenden Suchbegriffen möglich.
  Beispielsweise kann man so den Namen „Raithel“ auch mit dem Suchbegriff „reidl“ finden. Optional
  zu aktivieren ist diese Suchart deswegen, weil sie – bedingt durch die gewollt unscharfe Suche –
  auch mehr falsch positive (also nicht gewollte/passende) Treffer liefert.

* Verbesserung: Die Suchfunktion berücksichtigt jetzt auch mehrere aufeinanderfolgende gleiche
  Buchstaben. So findet man Namen wie „Heerdegen“ jetzt z. B. auch bei der Suche nach „Herdeegen“.

* Änderung/Verbesserung: Der „Turnier starten“-Dialog wurde überarbeitet. Es können jetzt viel mehr
  Optionen vorausgewählt (alle, die auch in der Datenbankdatei gespeichert werden) und auch als
  Vorlage festgelegt werden.

* Neu: Es kann jetzt, wenn Ergebnisse vorliegen und alle Runden abgeschlossen sind, ein Turnier als
  „abgeschlossen“ deklariert werden. In dem Fall kann das Turnier, unabhängig davon, ob die
  Datenbankdatei beschreibbar ist oder nicht, nicht mehr (aus Versehen) verändert werden.

* Neu: Auf der Ranglistenseite können jetzt nicht entscheidende Tore grau dargestellt werden, damit
  auf den ersten Blick sichtbar ist, ob nur die Bobbl, oder auch die Tore bzw. die gegnerischen
  Tore für eine Platzierung entscheidend waren.

* Änderung: Die gegnerischen geschossenen Tore werden jetzt standardmäßig angezeigt. Diese Option
  ist nun auch Teil der Vorlage für Turniereinstellungen und kann entsprechend automatisch für neue
  Turniere an- oder abgewählt werden.

* Neu: Die Position der Tabs für Seiten (sofern diese als Tabs angezeigt werden) kann jetzt
  via „Ansicht“ → „Position der Tabs für Seiten“ angepasst werden. Zusätzlich zu der Position am
  unteren Rand des Fensters können die Tabs jetzt auch oben, links oder rechts angezeigt werden.

* Bugfix: Wenn eine Instanz von Muckturnier zunächst ein Netzwerk-Client war, die Client-Seite dann
  geschlossen wurde und die Instanz danach als Server benutzt wurde, dann stürzte das Programm bei
  Änderungen ab. Das ist jetzt behoben.

* Neu: Die Seitenanordnung kann jetzt gespeichert und beim Starten bzw. Öffnen einer
  Turnierdatenbank wiederhergestellt werden.

* Änderung: Die Anmelde-, Ergebnisse- und Ranglistenseite sind jetzt immer sichtbar. Das sorgt für
  ein konsistenteres Erscheinungsbild, da ja die Tableiste sowieso eingeblendet wurde, sobald man
  Ergebnisse eingeben konnte (und folgerichtig sowieso entsprechend vertikalen Platz benötigte).

* Bugfix: Das Programm stürzt jetzt beim Schließen nicht mehr ab, wenn eine Netzwerkseite offen war,
  die offene Datenbank geschlossen wurde, und das Programm danach beendet wurde.

* Verbesserung/Bugfix: Beim Update älterer Datenbanken und beim Öffnen werden jetzt fehlende Werte,
  die eigentlich aber vorhanden sein müssten, berücksichtigt: In diesem Fall wird jetzt eine
  Fehlermeldung angezeigt, anstatt das Fehlen einfach zu ignorieren.

* Verbesserung: Die Berechnung der Größe der Zeitanzeige der Stoppuhr ist jetzt deutlich schneller.

* Neu: Die Einstellungen und die Größe des Stoppuhr-Fensters, wenn die Stoppuhr läuft, können jetzt
  (optional) gespeichert und beim nächsten Start wiederhergestellt werden.

* Neu: Die Eingabemaske für Spielstände kann jetzt an verschiedene Varianten von Spielstandzetteln
  angepasst werden. Zur Auswahl steht jetzt zusätzlich eine vertikale Version, bei der Paar 1 links,
  Paar 2 rechts und die Bobbl von oben nach unten geschrieben werden.

* Bugfix: Scrollbars der Anzeige der Bobbl-Eingabemasken auf der Spielstandseite werden jetzt mit
  dem Breeze-KDE-Style korrekt angezeigt.

* Neu: Die Vorlage für neue Turniere kann jetzt direkt im „Neues Turnier starten“-Dialog gesetzt
  bzw. aktualisiert werden.

* Bugfix: Durch An- und Abwählen der Option „Eingabe ohne Tischnummern“ konnte im Einzelspielermodus
  die Option „Tischnummer bzw. Paare automatisch auswählen“ eingeblendet werden, obwohl diese für
  Einzelspielerturniere überhaupt nicht verfügbar ist. Das ist jetzt behoben.

====================================================================================================
Release von Muckturnier 3.3.1 (27.01.2020)
====================================================================================================

* Bugfix: Wenn eine Netzwerkverbindung bestand, und der Name eines Paars/Spielers editiert wurde,
  dann führte die Ranglistenseite eine Schleife mit einer undefinierten Variable als Grenze aus.
  Im ungünstigsten Fall führte das zu 2.147.483.647 Durchläufen. Je nach dem (zufälligen) Wert der
  Variable blieb das Programm hängen und reagierte nicht mehr. Das ganze war eine Folge des mit
  Version 3.3 eingeführten Cachens von Daten und ist nun behoben.

* Änderung: Der Maximalwert für Runden, Punkte etc. ist jetzt 9999. Damit sieht der Dialog immer
  noch gut aus, und niemand wird jemals so viel brauchen.

* Bugfix: Wenn beim Herstellen einer Serververbindung bereits die Socketverbindung fehlschlägt, dann
  wird jetzt zusätzlich zu dem Socketfehler kein Timeout-Fehler mehr angezeigt.

====================================================================================================
Release von Muckturnier 3.3 (01.01.2020)
====================================================================================================

* Änderung: Die Readme wird jetzt mit rst2html5.py (wie der Name schon sagt: als HTML5) gebaut.
  Außerdem werden jetzt Touch-Geräte berücksichtigt: Der Header hat jetzt einen „Viewport“-Meta-Tag
  und die Links im Inhaltsverzeichnis werden für Touch-Geräte mit mehr Platz dazwischen angezeigt.

* Workaround: Qt-Bug #44056 sorgt bei der Verwendung des Fusion-Styles dafür, dass QGroupBoxen ohne
  Titel zu viel Platz oben haben. Der gepostete Bugfix verschiebt bloß den Platz nach innen, löst
  also das Problem nicht. Jetzt gibt es einen Workaround, der das Problem einstweilen damit
  umschifft, dass Boxen mit und ohne Titel unterschieden werden, und der Rahmen nicht vom Style,
  sondern via CSS gemalt wird.

* Neu/Verbesserung: Wenn bei SQL-Querys (aus welchen Gründen auch immer) Fehler auftreten, dann
  werden diese jetzt abgefangen und vearbeitet: Zunächst wird versucht, den letzten konsistenten
  Zustand der Datenbank per Rollback wiederherzustellen, sofern eine Transaktion offen ist. Dann
  wird die momentan laufende Funktion abgebrochen, die Netzwerkverbindung (sofern vorhanden) bzw.
  ein etwa laufender Server und die Datenbank geschlossen; schließlich wird eine Fehlermeldung
  mit dem ausgeführten Query und der Fehlermeldung der Datenbank angezeigt. Ein SQL-Fehler sollte
  jetzt das Programm nicht mehr zum Absturz bringen können.

* Bugfix: Wenn das Anlegen einer neuen Datenbank fehlschlägt, dann wird die zuletzt geöffnete jetzt
  wieder geöffnet, so dass es beim Bearbeiten danach keinen Crash mehr gibt.

* Bugfix: Das Update von älteren Datenbanken, die Revision 5 oder früher benutzt haben, funktioniert
  jetzt wieder korrekt. Bei Einzelspielerturnieren wurde seit Version 0.7.7 fälschlicherweise die
  Option „Automatische Paar- bzw. Tischauswahl“ aktiviert, und beim Öffnen der Datenbank nicht mehr
  deaktiviert. Das führte zu falschen Warnungen bei der Spielerauswahl bzw. zur Unbenutzbarkeit der
  Spielstandeingabe, da die Tischnummer automatisch falsch gesetzt wurde.
  Beim Update auf dbv8 von dbv5 wird der Wert jetzt direkt korrekt gesetzt, beim Update von dbv6
  oder dbv7 wird auf einen potenziell falsch gesetzten Wert geprüft und dieser ggf. korrigiert.

* Bugfix: Wenn eine Datenbank geöffnet ist, und eine andere geöffnet werden soll, das aber
  fehlschlägt, dann gibt es jetzt beim Weiterbearbeiten der zuletzt geöffneten Datenbank keinen
  Crash mehr.

* Neu: Beim Anlegen einer Datenbank werden jetzt die application_id- und user_version-Header-Felder
  von SQLite benutzt. application_id wird auf 1299538795 gesetzt (hexadezimal 0x4d75636b, was die
  Repräsentation der Zeichenkette "Muck" ist), und user_version auf die aktuelle Datenbankrevision.
  Damit kann eine Muckturnier-Turnierdatenbank z. B. per file(1) identifiziert werden, und man kann
  die Datenbankrevision unabhängig vom Muckturnier-Programm bzw. dem SQLite-CLI sehen.

* Änderung: Ab jetzt werden die Turniereinstellungen nicht mehr als Schlüssel-Wert-Paare in die
  "config"-Tabelle geschrieben, sondern als JSON-Objekt unter dem Schlüssel "SETTINGS". Um das
  Verwalten der Einstellungen kümmert sich jetzt die neue Klasse TournamentSettings.
  Damit ist jetzt folgendes gegeben:

  - Das Handhaben der Einstellungen ist jetzt logisch von der Handhabung der Datenbank getrennt.

  - Es wird jetzt nicht mehr zwischen Einstellungen unterschieden, die zum Anlegen der Datenbank
    festgelegt werden müssen, und solchen, die erst beim Schließen gespeichert werden.

  - Das Serialisieren und Parsen der Einstellungen ist jetzt einfacher (außerdem kam ja mittlerweile
    sowieso schon JSON hierfür zum Einsatz, z. B. für das Management allein gekommener Spieler).

  - Das Hinzufügen eines neuen Konfigurationswertes erfordert jetzt nicht mehr zwingend ein Update
    der Datenbankrevision (wie z. B. bei Revision 6 geschehen).

* Neu: Es kann jetzt eine Markierung für „abwesend“ definiert werden. So markierte Anmeldungen
  werden ignoriert. Das ist z. B. nützlich, wenn es Voranmeldungen gibt: Es werden dann die Paare
  bzw. Spieler, die noch nicht gekommen sind, nicht mitgezählt.

* Neu/Verbesserung: Die Tischnummer kann jetzt bei der Ergebniseingabe auch direkt per Tastatur
  eingegeben werden.

* Bugfix: Wenn die Serverseite geschlossen wird, während der Server läuft, dann wird der Server
  jetzt korrekt geschlossen. Damit gibt es jetzt keinen Crash mehr, wenn man hinterher etwas an der
  geöffneten Datenbank verändert.

* Bugfix: Beim Verbinden mit einem Server gibt es jetzt auch dann einen Timeout samt Fehlermeldung,
  wenn keine Socketverbindung hergestellt werden konnte (also noch gar kein Handshake initialisiert
  werden konnte). Bisher wurde (unendlich) auf das „connected“-Signal gewartet.

* Neu: Wenn ein Discover-Server eine IPv6-Link-Local-Adresse ausliefert, dann wird jetzt die
  (wahrscheinlich) korrekte Scope-ID für den lokalen Rechner automatisch gesetzt.

* Neu: Beim Starten des Servers werden jetzt alle verfügbaren IP-Adressen (incl. aller
  IPv6-Adressen), die nicht die „Localhost“-Adressen 127.0.0.1 bzw. ::1 sind, zur Auswahl
  aufgelistet. Die erste benutzbare wird voreingestellt. Ob eine IPv4- oder eine IPv6-Adresse als
  Voreinstellung ausgewählt werden soll, kann auf der „Netzwerk“-Seite in den Einstellungen
  ausgewählt werden.

* Änderung: Der Muckturnier-Server benutzt jetzt standardmäßig Port 8810 TCP und der Discover-Server
  Port 8811 UDP. Die beiden sind Stand jetzt nicht bei der IANA registriert, und es wurde auch noch
  keine "Illegal Activity" darauf verzeichnet. Somit sollten diese beiden Ports tatsächlich „frei“
  sein.

* Neu: Die bisher hart kodierten Einstellungen für das Netzwerk (Timeouts, Port des
  Discover-Servers etc.) können ab jetzt unter „Netzwerk“ im Einstellungen-Dialog verändert werden.

* Neu: Das Muckturnier-Programm gibt es jetzt auch (offiziell) for macOS. War erstaunlich einfach!
  Nach ein paar Code-Ergänzungen und ein bisschen Einlesen in die Materie gibt es ab jetzt ein
  App-Paket, das man „einfach so“ in die Programme ziehen kann. Also zumindest soweit ich das als
  Archetyp des Nicht-Apple-Users überblicken kann :-P Unterstützt werden dürfte jede Version von
  macOS ab „Sierra“ (Version 10.12).

* Schönheitskorrektur: Es wird jetzt wieder NULL (also kein Wert) für den Namen der „unmarkiert“-
  Markierung anstatt einem leeren String in der Datenbank gespeichert, so wie das vor Einführung der
  Vorlage für Markierungen der Fall war. Somit sind wir wieder konsistent (der leere String sollte
  aber egal wie keine Probleme verursachen, sofern er in einer Datenbank benutzt wird).

* Neu/Verbesserung: Die gewählten Einstellungen für „Allein gekommene Spieler berücksichtigen“ und
  die automatische Standardmarkierung für neue Anmeldungen sind jetzt Bestandteil der Vorlage für
  Markierungen. Damit werden die dort gesetzten Einstellungen automatisch für neue Datenbanken
  voreingestellt.

* Neu: Die gesetzte Standardmarkierung und die Einstellungen für allein gekommene Spieler werden
  jetzt in der Datenbank gespeichert und beim Öffnen wiederhergestellt.

* Bugfix: Wenn eine nicht beschreibbare Datenbank geöffnet ist, dann wird jetzt nicht mehr der
  „Markieren“-Popup-Dialog auf der Anmeldeseite angezeigt (der beim Benutzen dann einen SQL-Fehler
  produziert, weil die Datenbank ja nicht beschreibbar ist).

* Bugfix: Wenn eine nicht beschreibbare Datenbank geöffnet wird, und die Server- oder Client-Seite
  ist offen (und wird automatisch geschlossen), dann stürzt das Programm jetzt nicht mehr ab, wenn
  man es hinterher schließt.

* Bugfix: Wenn ein Paar-/Spielername editiert wird, dann werden offene Suchergebnisse auf der
  Ergebnisse- bzw. Ranglistenseite jetzt auch (wieder?) lokal korrekt aktualisiert, und nicht nur
  bei anderen Netzwerkteilnehmern.

* Optimierung/Deduplication: Unnötigerweise gesondert implementierte Funktionen zusammengefasst:
  - setMarker() und removeMarker()
  - listSetMarker() und listRemoveAllMarkers()
  - moveMarkerUp() und moveMarkerDown()

* Neu: Es kann jetzt bereits vor dem Beginn des Turniers eine Auslosung eingegeben werden. Sofern
  die Auslosung schon vor der 1. Runde feststeht, kann somit gleich von Anfang an die automatische
  Tisch- bzw. Paarauswahl genutzt werden.
  Passend dazu gibt es jetzt eine neue Seite, die optional per „Extras“ → „Übersicht Auslosung“
  angezeigt werden kann und die nach Tischen sortierte Auslosung samt fehlender Tische und Paare
  enthält.

* Neu: Wenn „Allein gekommene Spieler berücksichtigen“ ausgewählt ist, dann können Spieler mit der
  entsprechenden Markierung jetzt per Kontextmenü mit einem Klick automatisch zu einem Paar
  zusammengeführt und ummarkiert werden.

* Neu: Die „Automatischen Markierungen“ wurden erweitert: Das automatische Setzen einer Markierung
  war dafür gedacht, allein gekommene Spieler bei einem Feste-Paare-Turnier zu identifizieren. Mit
  der neuen Option „Allein gekommene Spieler berücksichtigen“ zeigt die Anmeldeseite jetzt auch an,
  wie der viele Anmeldungen allein gekommene Spieler sind, und ob die Anmeldungen okay für den
  Turnierstart sind (fehlen noch einzelne Spieler und/oder ein Paar, damit es aufgeht).
  Die gewählten Einstellungen werden in der Datenbank gespeichert und gehen somit beim Schießen und
  wieder Öffnen nicht verloren.

* Bugfix: Beim Anzeigen der Server- bzw. Client-Seite ändert sich jetzt die Größe des Hauptfensters
  nicht mehr.

* Verbesserung/Bugfix: Wenn beim Anlegen einer neuen Datenbank eine Datei überschrieben werden soll,
  dann wird jetzt geprüft, ob es eine Datenbank ist, die momentan von einer anderen Instanz von
  Muckturnier geöffnet ist. In diesem Fall wird das Überschreiben dann abgelehnt.

* Neu: Neben der Anzahl der angemeldeten Paare/Spieler wird (sofern eine korrekt teilbare Anzahl an
  Anmeldungen vorliegt) jetzt auch auch Anzahl der besetzten Tische angezeigt. Anstatt nur
  anzuzeigen, dass die Anzahl der Paare/Spieler nicht teilbar ist, wird jetzt angezeigt, was noch
  fehlt.

* Optimierung: Die Anzahl der SQL-Querys wurde durch Cachen minimiert und die Querys an sich
  wo möglich optimiert. Damit werden jetzt auch endlich ein paar PHP-Altlasten über Bord geworfen:

  - Die Anzahl der angemeldeten Paare/Spieler wird jetzt, wenn sich etwas ändert, als Variable
    gespeichert, anstatt sie immer wieder per Query zu ermitteln (und das geschah erstaunlich oft!).

  - Es werden jetzt, wenn Markierungen und Namen gebraucht werden, nur die Markierungsliste per SQL
    geholt und die Namen aus dieser Liste generiert (die Liste enthält sowieso alle Paar-/
    Spielernamen). Außerdem ist das Query jetzt einfacher und sollte somit auch schneller sein.

  - Der Rundenstatus (abgeschlossen oder nicht) wird jetzt gecachet, wenn ein Spielstand eingegeben
    oder gelöscht wird. Bisher fragten immer sowohl die Ergebnisse- als auch die Ranglistenseite
    zwei mal hintereinander exakt dieselben Daten per SQL-Query an.

  - Die fehlenden Tische werden jetzt mit einem effektiveren Query ermittelt.

  Unterm Strich resultiert das z. B. bei der Eingabe des Feste-Paare-Testdatensatzes in einer
  Reduktion der ausgeführten Querys um 36 % (353 Anfragen bisher, 225 jetzt).

* Optimierung: Wo möglich bzw. sinnvoll kommen jetzt besser passende Qt-Container für Daten zum
  Einsatz (z. B. QVector anstatt QList oder QHash anstatt QMap). Das sollte den Speicherbedarf
  verringern und die Geschwindigkeit steigern (ob mess- bzw. fühlbar sei dahingestellt ;-).

====================================================================================================
Release von Muckturnier 3.2 (05.08.2019)
====================================================================================================

* Bugfix: Wenn jetzt auf der Anmeldeseite eine Netzwerkänderung verarbeitet wird, dann wird der
  Fokus (bzw. der Cursor) nicht mehr automatisch auf das „Namen“-Eingabefeld gesetzt und springt
  somit nicht mehr nach oben, wenn man gerade dabei war, einen Suchbegriff einzugeben.

* Änderung: Das Exportieren von Turnierdaten bei laufendem Netzwerk ist jetzt möglich. Weil
  eigentlich kann nichts schiefgehen: Das einzige, was ein anderer Netzwerkteilnehmer bei einem
  gestarteten Turnier machen kann, ist das Editieren eines Namens. Und entweder wird diese Änderung
  dann noch mit exportiert, oder eben nicht, je nach dem, wann sie verarbeitet wird.

* Änderung: Es werden jetzt bei laufendem Netzwerk die Menüeinträge für den Listenimport und -export
  nicht mehr einfach komplett deaktiviert. Stattdessen wird beim Aufrufen eine Meldung angezeigt,
  dass dafür die Netzwerkverbindung beendet bzw. der Server gestoppt werden muss.

* Bugfix: Beim Generieren der verschiedenen Suchvarianten werden jetzt zusätzlich zu den Varianten
  des nach Wörtern sortierten Suchbegriffs die Varianten des unveränderten Suchbegriffs
  berücksichtigt.
  Nur die sortierte Variante zu verwenden hatte z. B. zur Folge, dass bei der Suche nach „he h“ das
  Paar „Heidi / Horst“ nicht gelistet wurde, da der einzige resultierende reguläre Ausdruck „h.+he“
  war. Bei der Suche nach „he ho“ wurde dann „he.+ho“ generiert, und das Paar wurde gefunden. Jetzt
  werden z. B. für „he h“ sowohl „he.+h“ als auch „h.+he“ generiert, was für weniger unerwartete
  Ergebnisse sorgt.

* Bugfix: Die Anzahl der verbleibenden Tische bei der Spielstandeingabe wird jetzt wieder richtig
  angezeigt.

* Neu: Alle lokalen Einstellungen sind jetzt im neuen Einstellungen-Dialog („Extras“ →
  „Einstellungen“) zusammengefasst und nicht mehr über mehrere Menüpunkte verteilt.

* Neu/Verbesserung: Die Ein- bzw. ausgeblendeten Spalten der Anmeldeseite werden jetzt in der
  Datenbank gespeichert und die Einstellung beim Öffnen der Datenbank wiederhergestellt. Die per
  Voreinstellung ein- bzw. ausgeblendeten Spalten können in den Einstellungen auf der
  „Anmeldeseite“-Seite festgelegt werden.

* Änderung: Beim Editieren eines Namens auf der Anmeldeseite wird eine etwaige Änderung jetzt nicht
  mehr übernommen, wenn der Editor den Fokus durch einen Mausklick auf ein anderes Element verliert,
  sondern nur noch, dann wenn Enter bzw. Return gedrückt wird. Das behebt gleichzeitig das Problem,
  dass in diesem Fall bisher bei der Suche nach einem gleichlautenden bzw. ähnlichen Eintrag dann
  der falsche Name benutzt wurde.

* Verbesserung: Die automatische Suche nach einem Muckturnier-Server wird jetzt mehrfach versucht.
  Da UDP ein unzuverlässiges Protokoll ist, geht ab und an ein Broadcast-Paket verloren und der
  Server antwortet womöglich nicht auf den ersten Versuch.

* Bugfix: Nach dem Editieren eines Paar-/Spielernamens wird jetzt nur noch dann gescrollt, wenn sich
  durch die Änderung die Position des Eintrags in der Liste verändert hat.

* Bugfix: Wenn Netzwerkänderungen vorgenommen werden, während auf der Anmeldeseite für einen Eintrag
  das Kontextmenü geöffnet ist, dann sollte das jetzt nach dem Anwenden der Änderung in jedem Fall
  wieder an der Stelle rekonstruiert werden, an der der Eintrag dann angezeigt wird.

* Verbesserung: Wird jetzt eine neue Anmeldung eingegeben, oder eine bestehende bearbeitet und es
  verändert sich dadurch die Sortierung, dann blinkt der Eintrag kurz auf, damit man die neue
  Position in der Liste auf den ersten Blick sehen kann.

* Bugfix: Wenn ein Client eine Änderung anfragt, dann wird nach der Serverantwort wieder korrekt
  zu dem entsprechenden Eintrag gescrollt.

* Neu: Es gibt jetzt auf der Anmeldeseite einen „Markieren“-Hilfsdialog, der mittels einer ein- und
  ausblendbaren Spalte aufgerufen werden kann. Damit können Markierungen schneller und komfortabler
  bearbeitet werden.

* Verbesserung: Beim Setzen und Entfernen von Markierungen wird jetzt auf der Paar-/Spielerliste
  nur noch dann gescrollt, wenn der bearbeitete Eintrag hinterher nicht mehr sichtbar wäre.

* Änderung: Das passende „Paar/Spieler“-Kontextmenü wird jetzt auch dann angezeigt, wenn es per
  Klick auf die „Markierung“-Spalte angefragt wird.

* Bugfix: Die Auswahl von „Spalte ‚Markierung‘ anzeigen“ auf der Anmeldeseite wird jetzt nach
  Änderungen an Markierungen nicht mehr zurückgesetzt.

====================================================================================================
Release von Muckturnier 3.1 (01.07.2019)
====================================================================================================

* Verbesserung: Beim Öffnen und Starten einer Turnierdatenbank wird jetzt das zugeörige Verzeichnis
  gespeichert und beim nächsten Start als Startpunkt des Dateiauswahldialogs gesetzt.

* Verbesserung: Wenn mit Ninja gebaut wird, dann werden jetzt sowohl für GCC als auch für Clang
  standardmäßig Farben in der (Fehler-)Ausgabe aktiviert. Das kann jetzt auch mit der CMake-Option
  NINJA_NO_COLORS ausgeschaltet werden.

* Neu: Der Standarddateiname für neue Turnierdatenbanken ist jetzt einstellbar. Es können Teile des
  aktuellen Datums bzw. der Zeit automatisch eingefügt werden. Die Voreinstellung ist
  „Muckturnier [Jahreszahl].mtdb“.

* Neu: Die bisher hart codierten Zeichenketten für den Trenner zwischen den Namen der Spieler in den
  Paarnamen bei Einzelspielerturnieren und für das Bobbl-Symbol können jetzt angepasst werden.

* Änderung: Sofern „Eingabe ohne Tischnummern“ nicht aktiviert ist, muss man jetzt immer einen Tisch
  auswählen (es ist als Voreinstellung nicht mehr der erste freie Tisch gesetzt). So kann es nicht
  mehr passieren, dass man während der ersten Runde aus Versehen nur die Namen heraussucht, aber
  nicht die zugehörige Tischnummer auswählt.

* Änderung: Markierungen können jetzt unabhängig davon, ob es so markierte Anmeldungen gibt, ein-
  und ausgeblendet werden. So kann ein Netzwerkteilnehmer z. B. nur eine bestimmte Markierung
  anzeigen und darauf warten, dass diese von einem anderen gesetzt wird.

* Neu: Um auf kleinen Displays vertikalen Platz zu sparen, kann jetzt die Fenstertitelleiste und der
  Fensterrahmen sowie die Menüleiste ausgeblendet werden.

* Neu: Beim Aufrufen der Client-Seite wird jetzt automatisch per UDP-Broadcast nach einem
  Muckturnier-Server gesucht. Wenn einer antwortet, dann wird dessen Socket-Adresse voreingestellt,
  so dass man die IP und den Port nicht manuell eingeben muss.

* Bugfix: Es wird jetzt das Gebietsschema des Servers übernommen, sofern ein Client ein anderes
  benutzt (z. B. wenn die Oberfläche des Server-Rechners auf Deutsch und die des Clients auf
  Englisch eingestellt ist). So ist sichergestellt, dass Sortierung der Namen überall gleich ist,
  und keine Prüfsummenfehler trotz identischen Datenbestands auftreten.

* Neu: Es kann jetzt, abhängig vom eingegebenen Namen einer neuen Anmeldung, automatisch eine
  Markierung gesetzt werden (z. B. „Allein da“, wenn kein „/“ im Namen vorkommt).

* Bugfix: Es können jetzt auch Backups wiederhergestellt werden, wenn die Original-Datenbank nicht
  mehr existiert.

* Verbesserung: Wenn ein Paar/Spieler editiert und währenddessen eine Netzwerkänderung verarbeitet
  wird, dann bleiben jetzt eine etwaige Textauswahl, die Cursorposition und die Scrollbarposition
  (soweit möglich) erhalten.

* Bugfix: Wenn auf dem Server ein Paar/Spieler editiert wurde und währenddessen mehr als ein
  Paar/Spieler von einem anderen Netzwerkteilnehmer angemeldet wurde, dann stürzte das Programm ab.
  Das ist jetzt korrigiert.

* Bugfix: Wenn auf dem Server ein Paar/Spieler editiert wurde und der Name bereits geändert war,
  und eine Änderung an der Anmeldungsliste von einem Client verarbeitet wurde, dann wurde eine
  falsche Fehlermeldung angezeigt. Wurde die nicht schnell genug weggeklickt, dann bekam der Client
  einen Timeout und der Server stürzte ab. Das ist jetzt behoben.

* Neu/Verbesserung: Es können jetzt lokale Einstellungen gespeichert werden. Namentlich:
  - Die letzte Größe und Position des Programmfensters
  - Die aktuellen Turniereinstellungen als Vorlage für neue Turnierdatenbanken
  - Die aktuellen Markierungen als Vorlage für neue Turnierdatenbanken

* Bugfix: Wenn auf dem Server ein Name editiert wurde und ein Client fragte währenddessen die
  Änderung dieses Namens an, und auf dem Server wurde die „Kollidierende Netzwerkänderung“-
  Fehlermeldung nicht schnell genug weggeklickt, dann bekam der Client einen Timout und der Server
  stürzte ab. Das ist jetzt behoben.

* Änderung: Die Ranglistenplätze werden jetzt von der Datenbank, und nicht mehr von der
  Ranglistenseite berechnet. Der „Turnierdaten exportieren“-Dialog greift jetzt nicht mehr auf die
  Ranglistenseite zu.

* Neu: Bei Aktionen auf der Anmeldungsseite, bei denen potenziell mehrere Paare/Spieler gelöscht
  werden (z. B. „Alle unmarkierten Paare/Spieler löschen“) wird jetzt angeboten, vorher ein Backup
  anzulegen (per Voreinstellung aktiviert).

* Neu: Wenn jetzt eine Turnierdatenbank geöffnet wird, die potenziell ein Backup ist, wird
  angeboten, das Backup wiederherzustellen, anstatt es direkt zu öffnen.

* Bugfix: Wird eine ungültige oder beschädigte Datenbank geöffnet, dann werden beim anschließenden
  Schließen keine Schreibversuche mehr unternommen und entsprechend keine zusätzliche Fehlermeldung
  mehr angezeigt.

* Bugfix: Beim Update einer Datenbank auf eine neue Revision werden jetzt keine Schreibversuche mehr
  in nicht beschreibbare Datenbanken unternommen. Beim Abbrechen wird die zuletzt geöffnete
  Datenbank korrekt wieder geöffnet. Somit sollte es in keinem Fall mehr SQL-Fehler und/oder
  Speicherzugriffsfehler geben.

* Bugfix: Beim Zurücksetzen der Datenbank wird jetzt das Ergebnis des Anlegens eines Backups vorher
  korrekt interpretiert.

* Neu: Das Netzwerk unterstützt jetzt auch die Aktionen, die die ganze Anmeldungsliste betreffen:
  Alle Paare/Spieler markieren, alle Markierungen entfernen, markierte Paare/Spieler löschen und
  die ganze Liste löschen.

* Neu: Es können jetzt auch Markierungen bearbeitet werden, wenn das Netzwerk läuft. Alle Aktionen
  werden unterstützt: Neu anlegen, Bearbeiten, Verschieben und Löschen.

* Verbesserung: Beim Vergleich der Ranglisten per Netzwerk wird jetzt berücksichtigt, ob auf dem
  Server und dem Client für alle Paare/Spieler gleich viele Ergebnisse eingegeben wurden.

* Bugfix: Der Vergleich von Ranglisten per Netzwerk gibt jetzt auch dann sinnvolle Ergebnisse aus,
  wenn durch unterschiedliche Ergebnisse verschiedene Platzierungen entstehen. Bisher wurde der 1.
  mit dem 1., der 2. mit dem 2. Platz verglichen etc., so dass eine unterschiedliche Platzierung zu
  einer Rasterverschiebung führte. Deswegen wurden ab dem ersten unterschiedlichen Platz alle
  nachfolgenden als verschieden angezeigt, auch dann, wenn die Punkte eigentlich identisch waren.

* Verbesserung: Wenn jetzt ein Paar- bzw. Spielername editiert wird (egal ob lokal oder per
  Netzwerk), dann werden beim neu Laden der Ergebnisse-Seite vorher evtl. eingegebene Daten
  wiederhergestellt, anstatt die Ausgangssituation anzuzeigen.

* Bugfix: Wenn die „Abgebrochen“-Markierung von einem Bobbl entfernt wird, dann werden jetzt nur
  noch die Bobbl davor zurückgesetzt, die als abgebrochen markiert waren. Bei allen anderen bleiben
  die eingegebenen Daten erhalten.

* Bugfix: Beim Verbinden mit einem Server wird jetzt nicht mehr nur die IP-Adresse, sondern auch der
  Port zwischengespeichert und beim nächsten Verbinden voreingestellt.

* Bugfix: Es sollten jetzt, unabhängig davon, ob Server oder Client offen sind, keine
  Speicherzugriffsfehler mehr beim Beenden auftreten.

* Änderung: Das Netzwerkprotokoll wurde überarbeitet. Es gibt jetzt nicht mehr einen Anmeldungs-
  und einen Ranglistenserver, sondern nur noch einen Server, der sich um alles kümmert. Die
  Anmeldung am Netzwerk ist jetzt auch mit bereits eingegebenen Ergebnissen möglich (potenziell
  abweichende Namen werden dann abgeglichen). Änderungen an Namen werden jetzt also auch nach der
  Anmeldung verteilt und die Anmeldungslisten bleiben immer synchron.

* Änderung: Die Server- und Clientdialoge werden jetzt nicht mehr als extra Fenster, sondern wie
  die anderen Seiten (standardmäßig) als Tabs angezeigt.

* Bugfix: Nach einem Reset der Datenbank werden die Netzwerk-Menüeinträge jetzt korrekt aktiviert
  bzw. deaktiviert.

* Bugfix: Der Install-Prefix ("/usr") wird jetzt unter Linux auch dann korrekt gesetzt, wenn mit
  Ninja anstatt mit GNU make kompiliert wird.

====================================================================================================
Release von Muckturnier 3.0 (25.03.2019)
====================================================================================================

* Änderung: Das Versionierungsschema wird ab jetzt an das „Semantic Versioning“ angelehnt. Zum einen
  wird das Programm jetzt schon lang produktiv eingesetzt, zum anderen gab es bereits zwei
  grundsätzliche Änderungen der Codebasis (von PHP/HTML/JavaScript hin zu Python/PyQt/Qt und dann zu
  C++/Qt). Deswegen wird auf die Version 0.7.7 jetzt die Version 3.0 folgen und 1.0 und 2.0 werden
  einfach ausgelassen.

* Änderung: Der Slogan des Programms ist jetzt „Das Programm für die Turnierleitung“ und nicht mehr
  „Das Open-Source-Projekt zum Auswerten von Muckturnieren“, da es zum einen schon lang nicht mehr
  nur um die Auswertung geht, und zum anderen die meisten potenziellen Nutzer vermutlich gar nicht
  wissen, was „Open Source“ überhaupt ist ;-)

* Bugfix: Wenn markierte Anmeldungen ausgeblendet sind und dann die ganze Paar-/Spielerliste
  gelöscht wird, dann werden die entsprechenden Markierungen jetzt automatisch wieder eingeblendet,
  damit beim nächsten Benutzen die jeweilige Markierung nicht (unerwarteterweise) auf „ausgeblendet“
  gesetzt ist und der oder die markierte(n) Eintrag/Einträge „verschwinden“.

* Änderung: Unter Linux werden der MIME-Typ-Eintrag für die Turnierdatenbank und der
  Startmenüeintrag jetzt mittels der xdg-utils installiert (anstatt die Dateien einfach dorthin zu
  kopieren, wo sie dann landen).

* Verbesserung: Windows-Installer und Uninstaller mit eigenen Icons, Bildern und Willkommensseiten
  deutlich schicker gemacht :-)

* Verbesserung: Der Windows-Installer wählt jetzt automatisch das Installations-Zielverzeichnis der
  zuletzt installierten Version aus, sofern es eine alte Installation gibt.

* Änderung: Statt unter Windows eingebettete Ressourcen zu benutzen, werden jetzt einfach die
  entsprechenden Dateien in ein Unterverzeichnis installiert. NSIS kann die Dateien so besser
  komprimieren (was zu einem kleineren Installer führt), und da es ja jetzt schon länger einen
  "richtigen" Installer gibt, besteht keine Notwendigkeit mehr für das Einbetten.

* Verbesserung: Der Windows-Installer legt jetzt Registry-Einträge an, mit denen die Deinstallation
  über die Windows-Softwareverwaltung möglich wird. Bei einem Upgrade wird die vorher installierte
  Version jetzt automatisch deinstalliert, damit keine veralteten Dateien übrigbleiben. Weiterhin
  wird bei einem Downgrade eine Warnung angezeigt.

* Verbesserung: Wenn jetzt eine Spielstand-Spin-Box angeklickt wird, dann wird der bisher
  eingegebene Text (also der aktuelle Spielstand) komplett markiert, so dass eine Eingabe den
  bisherigen Spielstand überschreibt (und man den alten nicht erst löschen muss).

* Neu: Es kann jetzt eine Standardmarkierung („Automatische Markierung“) ausgewählt werden, mit der
  alle neuen Paare/Spieler nach dem Anmelden markiert werden. Standardmäßig ist keine Markierung,
  also „(unmarkiert)“ gesetzt.

* Bugfix: Das Programm konnte zum Absturz gebracht werden, wenn man in der „Markierung“-Spalte
  auf der Paare- bzw. Spielerseite den Namen einer Markierung editiert hat. Die ganze Spalte sollte
  natürlich gar nicht erst editierbar sein, und das ist jetzt der Fall.

* Neu: Es besteht jetzt die Möglichkeit, ein Anmeldungsnetzwerk aufzubauen. Ein Rechner fungiert
  dazu als Server, an dem sich mehrere Clients anmelden können. So können mehrere Rechner (eine
  Netzwerkverbindung natürlich vorausgesetzt) für die Anmeldung genutzt werden, und alle Änderungen
  an der Paar- bzw. Spielerliste werden immer automatisch an alle Rechner geschickt. Alle
  Beteiligten sehen immer den aktuellen Stand, und die Anmeldung mit mehreren Rechnern ist deutlich
  komfortabler möglich, als bisher.

* Neu: Passend zum Anmeldungsnetzwerk gibt es jetzt auch die Möglichkeit, Ranglisten per Netzwerk
  zu vergleichen. Ein Rechner ist der Server, mehrere Clients können die Server-Rangliste anfragen.
  Ein Client vergleicht dann die Server-Rangliste mit seiner eigenen und zeigt etwaige Unterschiede
  an. Wenn eine Netzwerkverbindung zur Verfügung steht, dann können Eingabefehler (die in
  verschiedenen Ranglisten resultieren) so direkt angezeigt werden, ohne dass man Prüfsummen oder
  die ganze Rangliste abgleichen muss.

* Bugfix: Wenn man ein Paar bzw. einen Spieler umbenennt oder löscht, dann wird ein evtl.
  angezeigtes Suchergebnis wieder korrekt wiederhergestellt und aktualisiert, anstatt die Suche zu
  beenden und wieder die komplette Liste anzuzeigen (war eine Regression aus der neuen
  Markierungsimplementierung).

* Neu/Bugfix: Es gibt unter Windows jetzt einen „Kontexthilfe“-Menüeintrag, da Windows es nicht
  zulässt, das Kontexthilfe-Fragezeichen neben Minimier- und Maximier-Buttons in der
  Fenstertitelleiste anzuzeigen. Damit kann die Kontexthilfe jetzt auch unter Windows aufgerufen
  werden.

====================================================================================================
Release von Muckturnier 0.7.7 (16.11.2018)
====================================================================================================

* Änderung: Es wird jetzt mindestens Qt 5.7 benötigt. Überladene Signale werden jetzt mit
  QOverload<T>::of verknüpft.

* Änderung: Es gibt jetzt eine „SharedObjects“-Klasse, die die Instanziierung der Datenbank und
  aller geteilten Hilfsklassen übernimmt. Diese sind jetzt keine Unterklassen oder -funktionen
  der Datenbank oder anderer Hilfsklassen mehr, stattdessen gibt es jetzt eine sinnvollere
  Aufteilung.

* Verbesserung/Bugfix: Für hohe Punktzahlen werden jetzt beim Punktestand-Dialog Scrollbars
  angezeigt, und der Dialog wird nicht mehr abgeschnitten.

* Änderung: Die alte Dateierweiterung „.db“ wird bei der Dateiauswahl beim Öffnen einer Datenbank
  nicht mehr standardmäßig angezeigt. Solche Datenbanken können aber natürlich immer noch zum Öffnen
  ausgewählt werden, wenn man als Filter „Alle Dateien“ auswählt.

* Dokumentation: Die Readme enthält jetzt auch „Tips aus der Praxis“, vor allem mit Vorgehensweisen
  für große Turniere, aber auch grundsätzlichen Hinweisen.

* Verbesserung: Es wird auf der Anmeldeseite jetzt auch zum jeweiligen Paar-/Spielernamen gescrollt,
  wenn ein Paar/Spieler umbenannt oder markiert bzw. de-markiert wurde und sich dadurch die Position
  in der Liste verändert hat.

* Änderung: Die Verarbeitung der angezeigten Zeichenketten überarbeitet. Jetzt wird die
  I18N-Infrastruktur von Qt genutzt, um z. B. Zeichenketten anzuzeigen, die sich für Singular und
  Plural unterscheiden.

* Änderung: Die Implementierung der Markierungen wurde neu geschrieben. Jetzt können beliebig viele
  Markierungen mit beliebigen Namen und Farben angelegt werden. Jede Markierung kann jetzt entweder
  einen neuen Block der so markierten Namen in der Paar-/Spielerliste erzeugen, oder den jeweiligen
  Namen im vorherigen Block fortlaufend mit einsortiert darstellen. Die Reihenfolge der Anzeige kann
  frei gewählt werden.
  Das für die neuen Markierungen nötige Update der Datenbankstruktur wird beim Öffnen einer älteren
  Datenbank wie gewohnt angeboten.

* Verbesserung: Wenn aufgrund der Änderung von Daten die Anzeige der Anmelde- und Ergebnisse-Seiten
  aufgefrischt werden, dann wird jetzt nur noch das geändert, was wirklich sein muss. Dass z. B.
  auf der Ergebnisse-Seite in diesem Fall die komplette Eingabemaske gelöscht und neu generiert
  wurde, war noch ein Überbleibsel aus PHP-Zeiten (da musste man ja immer alles neu erzeugen).

* Verbesserung: Wenn ein Paar/Spieler jetzt markiert wird, oder eine Markierung entfernt wird,
  dann wird (sofern sich dadurch die Position in der Liste ändert) in der Paar-/Spielerliste jetzt
  zum entsprechenden Namen gescrollt, damit er hinterher sichtbar ist.

* Neu/Verbesserung: Es gibt jetzt einen eigenen Dialog für den Import von Paar- bzw- Spielerlisten.
  Hier wird eine Vorschau des zu importierenden Datensatzes vor dem eigentlichen Import angezeigt.
  Etwaige Duplikate werden gesondert gelistet und können vom Import ausgeschlossen bzw. als
  potenzielles Duplikat markiert werden.

* Neu: Der Turnierdatenbank-Dateityp „.mtdb“ wird jetzt mit dem Muckturnier-Programm verknüpft.
  Damit kann eine Turnierdatenbank z. B. per (Doppel-)Klick aus einem Dateimanager heraus geöffnet
  werden.
  Unter Linux wird dafür jetzt eine „Freedesktop Shared Mime Info“-XML-Datei installiert, unter
  Windows legt der Installer die entsprechenden Registry-Einträge an.

* Bugfix: Beim Öffnen einer Datenbank werden jetzt – für den Fall, dass vorher eine andere Datenbank
  geöffnet war – potenziell eingegebene Suchbegriffe zurückgesetzt.

* Bugfix/Verbesserung: Wenn „Tischnummer bzw. Paare automatisch auswählen“ aktiviert wird, dann
  werden jetzt – sofern möglich – direkt danach die passenden Paare für den aktuell ausgewählten
  Tisch gesetzt.

* Neu: Für die Prüfsummen der Paar- bzw. Spieler- und Rangliste gibt es jetzt einen eigenen Dialog.
  Dort können jetzt auch Prüfsummen für Teile der Rangliste berechnet werden. Damit sollte der
  Abgleich von an mehreren Rechnern eingegebenen Spielständen deutlich schneller möglich sein.

* Bugfix: Die Anzeigeoptionen („Gegenerische Tore berücksichtigen“ etc.) werden jetzt wieder korrekt
  gespeichert (behebt eine Regression, die durch „Datenbank-Checks beim Öffnen einer Datei
  überarbeitet und zentralisiert“ entstanden ist).

* Neu/Verbesserung: Die Combo-Boxen zur Auswahl der Paare bzw. Spieler auf der Ergebnisse-Seite
  können jetzt genauso durchsucht werden wie die Paar- und Rangliste und die Rundenergebnisse.
  Das sollte bei der Eingabe der 1. Runde (oder aller Runden wenn ohne automatische Paarauswahl
  gespielt wird) – vor allem bei vielen Paaren/Spielern – eine erheblich schnellere Eingabe
  ermöglichen.

* Bugfix: Beim Editieren (Löschen und neu Eingeben) von Spielständen werden bei als abgebrochen
  eingegebenen Bobbeln die gespeicherten Punktwerte jetzt korrekt voreingestellt und der jeweilige
  Bobbl auch als abgebrochen markiert.

* Änderung: Die Eingabe von abgebrochenen Bobbeln überarbeitet: Es gibt jetzt keine Checkbox für
  die ganze Runde mehr, die diese als abgebrochen markiert und pro Bobbl den „Abgebrochen“-Knopf
  einblendet. Der ist jetzt stattdessen immer sichtbar und markiert nicht nur den jeweiligen,
  sondern auch alle folgenden Bobbl als abgebrochen.
  Die Spielstandzettel kommen sowieso nicht in der richtigen Reihenfolge, so dass man das eh für
  jeden einzeln einstellen muss.

* Verbesserung: Der Spielstand-Eingabedialog kann jetzt auch von den Spielstand-Eingabefeldern aus
  mittels eines Rechtsklicks aufgerufen werden (nicht mehr nur von den Bobbl-Radio-Knöpfen aus).
  Der Aufruf per Doppelklick auf einen Bobbl-Radio-Knopf wurde entfernt, damit die Eingabe
  einheitlich funktioniert (außerdem ist ein Rechtsklick eh komfortabler als ein Doppelklick).

* Neu: Bei der Spielstandeingabe wird jetzt angezeigt, wie viele Ergebnisse (also Tische) bereits
  eingegeben wurden, und wieviele noch fehlen.

* Verbesserung: Bei der Anmeldung neuer Paare/Spieler und beim Umbenennen wird jetzt die neue
  Suchfunktion benutzt, um Verwechslungen oder auch versehentliche doppelte Einträge zu verhindern.
  Ist z. B. ein Spieler namens „René“ angemeldet, und es soll ein anderer zu „Rene“ umbenannt
  werden, oder es bereits ein Paar namens „Meier Bernd / Müller Peter“ gibt, und man will
  „Peter Müller / Bernd Meier“ anmelden, wird eine Warnung angezeigt, dass der Name nicht eindeutig
  ist bzw. das Paar scheinbar bereits angemeldet ist.

* Verbesserung: Suchfunktion weiter verbessert: Die Reihenfolge der eingegeben Wörter spielt jetzt
  keine Rolle mehr. Wenn z. B. nach dem Paar „Meier Bernd / Müller Peter“ gesucht wird, kann man es
  mit der Suche nach „Peter Müller“ genauso finden, wie wenn nach „Müller Peter“ gesucht wird. Die
  Suche nach „Peter Bernd“ oder „Müller Meier“ führt nun auch zum gesuchten Paar.

* Verbesserung: Beim Im- bzw. Exportieren von Daten wird jetzt als Startpunkt der Ordner geöffnet,
  in dem auch die Datenbank liegt (anstatt immer den Standard-„Eigene Dateien“-Ordner auszuwählen).
  Ist eine Datenbank geöffnet und man öffnet eine andere, wird ebenfalls der Order der letzten
  offenen Datenbank als Startordner gesetzt.

====================================================================================================
Release von Muckturnier 0.7.6 (27.07.2018)
====================================================================================================

* Neu: Eine Funktion zum automatischen Erstellen eines Datenbankbackups hinzugefügt. Der „Datenbank
  zurücksetzen“-Dialog bietet jetzt auch ein Backup vor dem Zurücksetzen an.
  Weiterhin können über diese Funktion erstellte Backups jetzt auch automatisch rückgesichert
  werden: Der neue Dialog „Extras“ → „Backup wiederherstellen“ zeigt eine Liste aller verfügbaren
  Backups.

* Verbesserung: Die Suchfunktion behandelt jetzt diakritische Zeichen und Umlaute gesondert. Wenn
  man z. B. einen Namen wie „André Müller“ angemeldet hat, bekommt man das passende Ergebnis jetzt
  auch, wenn man z. B. nach „andre mueller“ sucht.

* Verbesserung/Bugfix: Datenbank-Checks beim Öffnen einer Datei überarbeitet und zentralisiert bzw.
  abstrahiert (wird jetzt z. B. auch beim Rücksichern eines Backups benutzt).
  Es sollten jetzt keine Schreibversuche in nicht benutzbare Dateien mehr unternommen werden und
  entsprechend auch keine merkwürdigen Fehlermeldungen bei dem Versuch, eine solche Datei zu öffnen,
  mehr erscheinen.

* Neu: Logo der Homepage hinzugefügt und die Startansicht sowie das „Über“-Fenster überarbeitet.

* Änderung: Für die Anzeige der Lizenz im Windows-Installer wird jetzt eine Schriftart mit festen
  Zeichenbreiten benutzt (damit der mit Leerzeichen „formatierte“ GPL-Text nicht mehr so komisch
  aussieht).

* Bugfix: Ein etwaiges Suchergebnis auf der Paare-/Spielerseite wird jetzt korrekt aktualisiert,
  wenn Markierungen geändert und/oder Paare/Spieler umbenannt oder gelöscht werden.

* Verbesserung/Bugfix: Export überarbeitet: Allgemeine Daten besser sortiert, fehlende Punkte pro
  Bobbl und Bobbl pro Runde hinzugefügt.

* Bugfix: Markup-Fehler im HTML-Export korrigiert.

* Änderung: Links auf die neue Homepage http://muckturnier.org/ aktualisiert.

* Änderung: Alte ChangeLog-Einträge ins neue Format übertragen.

====================================================================================================
Release von Muckturnier 0.7.5 (15.06.2018)
====================================================================================================

* Verbesserung: Der „Turnierinformationen“-Dialog enthält jetzt Prüfsummen für die Paar- bzw.
  Spielerliste und die aktuelle Rangliste, damit parallel an mehreren Rechnern eingegebene
  Ergebnisse schnell und einfach daraufhin geprüft werden können, dass alle Eingaben identisch
  waren.

* Änderung: Der Export von Turnierdaten als HTML wird jetzt als HTML5 und nicht mehr als XHTML 1.1
  strict ausgegeben.

* Verbesserung: Wenn mindestens eine Stoppuhr läuft, dann wird jetzt vor dem Schließen des Programms
  darauf hingewiesen und gefragt, ob es trotzdem beendet werden soll (und alle Stoppuhren damit
  ebenfalls geschlossen werden sollen).

* Verbesserung: Es werden jetzt alle SQL-Querys auf fehler beim Ausführen überprüft (was an sich
  nicht vorkommen sollte, nur, wenn z. B. die gerade geöffnete Datenbank gelöscht wird o. Ä.).
  Bestehende Fehler wurden behoben (z. B. der Versuch, Optionen in einer Datenbank ohne
  Schreibrechte zu speichern oder fehlgeschlagene Querys beim Überschreiben einer offenen Datenbank
  mit einer neuen).

* Verbesserung: Beim Anlegen oder Öffnen einer Datenbank wird jetzt eine Lock-Datei angelegt. So
  kann vermieden werden, dass versehentlich mehrere Instanzen von Muckturnier auf dieselbe
  Datenbank zugreifen und so Inkonsistenzen entstehen.

* Verbesserung: Es können jetzt auf der Paare- bzw. Spielerseite mehrere oder alle Paare bzw.
  Spieler gleichzeitig gelöscht werden (alle, alle markierten oder alle ohne Markierung).

* Verbesserung: Die Optionen „Keine Tischnummern“, „Paare automatisch auswählen“ und „Gegnerische
  Tore berücksichtigen“ werden jetzt in der Datenbank gespeichert und beim Öffnen passend gesetzt.

* Neu: Falls ohne Tischnummern gespielt wird, können diese jetzt auch ausgeblendet werden. Die
  Rundenergebnisse werden dann in der Reihenfolge der Eingabe angezeigt (intern werden die
  Tischnummern benötigt und einfach automatisch hochgezählt).

* Bugfix: Auf der Paar-/Spieler- und der Ergebnisseite werden jetzt auch Namen, die mit Umlauten
  oder Buchstaben mit Akzenten etc. beginnen, korrekt alphabetisch sortiert (anstatt der Liste
  hinten angestellt zu werden).

* Verbesserung: Es können jetzt auch schreibgeschützte Turnierdatenbanken (natürlich nur zum
  Betrachten) geöffnet werden.

* Allgemein: Code generell durchgesehen und überarbeitet:

  - Hoffentlich werden, via „-Wall -Wextra -pedantic“, jetzt alle Compilerwarnungen angezeigt.

  - In Funktionsaufrufen werden jetzt überall da, wo es sinnvoll und möglich ist, const references
    beim Aufruf verwendet.

  - Q_OBJECT-Makros entfernt, wo sie nicht gebraucht werden.

  - Klassen zu sinnvolleren Namen umbenannt (AboutWindow → AboutDialog, ExportDialog →
    ExportTournamentDialog, GridDelegate → TableDelegate, TournamentInfo → TournamentInfoDialog).

  - Funktionen, die virtuelle, geschützte Funktionen überschreiben, sind jetzt auch als solche
    deklariert (via „virtual“ und „override“).

  - SQL-Querys und Verarbeitung der Daten überarbeitet und optimiert.

* Neu: Eine „Dialog“-Basisklasse eingeführt, von der sich jetzt alle Dialoge ableiten. Das führt zu
  einem einheitlicheren Erscheinungsbild und weniger redundatem weil doppelt geschriebenen Code.

* Bugfix: Beim Zurücksetzen der Datenbank werden die Menüeinträge zum Im- bzw. Export der Paar- bzw.
  Spielerliste und dem Datenexport jetzt korrekt (de)aktiviert.

* Verbesserung: Die Stoppuhr kann jetzt (pro Instanz unabhängig voneinander) einen optionalen
  Titel anzeigen.

* Änderung: Der Export von Paar- bzw. Spielerlisten hat jetzt einen eigenen kleinen Dialog, wo man
  auswählen kann, ob die neuen Markierungen mit exportiert werden sollen, oder nicht. Beim Import
  wird ebenfalls gefragt, ob die Markierungen beachtet werden sollen.

* Neu: Es können jetzt auf der Paar- bzw. Spielerliste Markierungen gesetzt werden, etwa zum
  Kennzeichnen von vorangemeldeten, aber noch nicht erschienenen Paaren bzw. Spielern.

* Bugfix: Auch beim Editieren von Paar- bzw. Spielernamen werden jetzt überschüssige Leerzeichen
  vor dem Speichern entfernt.

* Verbesserung: Bei der Eingabe von Spielständen können die Punkte jetzt „zirkulär“ eingegeben
  werden, d. h. wenn man bei 0 Punkten anfängt und das Mausrad nach hinten dreht, oder die
  Pfeiltaste nach unten drückt, dann springt der Spielstand auf das Maxium (z. B. 20). Damit braucht
  man weniger Bewegungen bzw. Tastaturanschläge für die Eingabe hoher Punktezahlen.
  Weiterhin kann durch einen Doppel- oder Rechtsklick auf den Auswahl-Bobbl beim Eingeben der
  Spielstände ein Spielstand jetzt auch mit einem Klick über einen Dialog eingegeben werden – ganze
  ohne das Mausrad oder die Tastatur zu verwenden.

* Bugfix: Mehrfach vergebene Ranglistenplätze werden jetzt korrekt angezeigt (nicht mehr so oft, wie
  es sie gibt, sondern nur noch ein Mal).

====================================================================================================
Release von Muckturnier 0.7.4 (02.01.2018)
====================================================================================================

* Änderung: Strengere Buildsystem-Vorgaben:
  - Für Qt: "-DNO_URL_CAST_FROM_STRING -DNO_CAST_FROM_BYTEARRAY -DQT_DEPRECATED_WARNINGS"
  - Für den C++-Compiler "-Wall"

  Alle als veraltet markierten Codeteile aktualisiert und Compilerwarnungen behoben.

* Neu: Es können jetzt auch Paar- bzw. Spielerlisten importiert werden, und zwar aus einer UTF-8-
  kodierten Textdatei mit je einem Paar- bzw. Spielernamen pro Zeile.

* Änderung: Von Anfang an wurde der Terminus „Game“ anstatt „Booger“ (für Variablen, Klassennamen
  etc.) benutzt. Es dreht sich aber immer um Bobbl, weil ein Spiel innerhalb eines Bobbels gespielt
  wird, und dieser aus mehreren Spielen besteht. Das ist nun korrigiert.
  Hierfür nötig war ein Update der Datenbankversion (auf Version 5). Beim Laden einer älteren
  Datenbank wird ein Update angeboten.

* Bugfix: Update von Datenbankversion 3 auf 4 repariert: Es wurden alle Turniere wie Einzelspieler-
  Turniere behandelt. Das hatte keine sichtbaren Folgen, aber in der Datenbank wurden abwechselnd
  die Spielernummer 1 und 2 anstatt immer 1 gesetzt.

* Änderung: Der „Einstellungen“-Dialog wurde in einen „Neues Turnier starten“- und einen
  „Turnierinformationen“-Dialog aufgeteilt.
  Der bisherige Dialog war noch ein Überbleibsel aus den Zeiten, in denen Muckturnier noch eine
  Webanwendung war. In Version 0.7.2 wurde die ursprüngliche „Einstellungen“-Seite zunächst einfach
  nur in einen Dialog verschoben, aber nicht verändert.

* Verbesserung: Wenn es mehrfach vergebene Ranglistenplätze gibt, dann werden sie auf der
  Ranglistenseite jetzt aufgelistet.

* Verbesserung: Das Datum der Readme-Datei wird jetzt automatisch vom Build-System entsprechend der
  letzten Dateiänderung gesetzt.

* Neu: Es können jetzt auch Verlaufs-Ranglisten für bereits abgeschlossene Runden angezeigt werden.

* Bugfix: Potenzielle Probleme beim CMake-Build (Generieren der Versionsnummer) behoben. Hätte nicht
  funktioniert, wenn das Arbeitsverzeichnis Leerzeichen enthält.

====================================================================================================
Release von Muckturnier 0.7.3 (06.09.2017)
====================================================================================================

* Neu: Auf der Ergebnisse-Seite können jetzt Paar 1 und Paar 2 per Kontextmenü vertauscht werden,
  ohne dass der Spielstand „richtig“ editiert werden muss.

* Verbesserung: SQL-Querys und die nachgeschaltete Datenverarbeitung überarbeitet.

* Bugfix: Designschwäche im Datenbanklayout beseitigt: Bei einem Turnier mit einzelnen Spielern war
  bisher die Spielernummer innerhalb eines Paars nicht definiert. Theoretisch hätte also aus dem
  Paar "Spieler 1 / Spieler 2" beim Anzeigen bzw. Ändern "Spieler 2 / Spieler 1" werden können.
  Hierfür war ein Update der Datenbankversion (auf Version 4) nötig. Beim Laden einer Version-3-
  Datenbank wird ein Update angeboten.

* Neu: Die Spieler- bzw. Paarliste kann jetzt exportiert werden. Das ist z. B. nützlich, wenn man
  die Datenbank im Vorfeld mit Voranmeldungen füllt und dann eine Liste zum Abhaken erstellen will.

* Bugfix: Die Export- und Einstellungen-Menüeinträge werden jetzt beim Schließen einer Datenbank
  deaktiviert.

* Bugfix: Es sollten jetzt alle Dialoge und Menüeinträge übersetzt (also auf deutsch) angezeigt
  werden (sofern bei einer dynamisch gelinkten Version die Qt-Übersetzungen vorliegen. Das statisch
  gelinkte Windows-Build bringt die Übersetzungen als Resource mit).

* Änderung: GUI allgemein durchgesehen und überarbeitet (mehr Platz für das Wichtige, Tippfehler,
  Gitternetzlinien für die Listen zwecks besserer Sichtbarkeit etc.).

* Neu: Die Spielerliste, die Spielergebnisse und die Rangliste können jetzt durchsucht werden. Es
  werden dann nur die Treffer angezeigt, um z. B. schnell ein Spiel oder einen Namen zum Korrigieren
  oder einen Ranglistenplatz zu finden.

* Neu: Für die Rangliste können jetzt auch die gegnerischen geschossenen Tore (also die Punkte, die
  die Gegner in gewonnenen Bobbeln erreicht haben) herangezogen werden – je weniger, desto besser.

====================================================================================================
Release von Muckturnier 0.7.2 (06.08.2017)
====================================================================================================

* Neu: Runden können jetzt abgebrochen werden. Bei einem abgebrochenen Bobbl können zwei Spielstände
  eingegeben werden, die dann beide als geschossene Tore zählen.

* Verbesserung: Besserer Workflow bei der Eingabe eines verlorenen Bobbls mit dem Mausrad: Nach der
  Auswahl des Bobbls kann der Spielstand jetzt auch dann gesetzt werden, wenn sich der Mauszeiger
  über dem Bobbl-Radiobutton befindet.

* Bugfix: Wenn „Tischnummer bzw. Paare automatisch auswählen“ bei der Ergebniseingabe abgewählt
  wird, dann bleibt das jetzt auch nach dem Speichern eines Spielstandes so.

* Bugfix: Wenn bei der Eingabe von Spielständen von der Auslosung abgewichen wurde, konnte es
  passieren, dass per automatischer Auswahl kein Tisch ausgewählt wurde und dann die Tischnummer 0
  gespeichert wurde. Das ist jetzt behoben.

* Neu: Passend zu der Möglichkeit, eine Runde abzubrechen, bringt Muckturnier jetzt auch eine
  Stoppuhr mit. Diese kann z. B. groß gezogen und per Beamer angezeigt werden.

* Änderung: Es gibt jetzt ein neues GUI, das sich nicht mehr an der Webseitenoptik der ersten
  Versionen orientiert:

  - Die Einstellungen sind jetzt in einem extra Dialog untergebracht.

  - Die Spieler- Ergebnisse- und Rangliste-Seiten können jetzt nicht mehr nur als Tabs, sondern auch
    neben- bzw. übereinander angeordnet oder als extra Fenster angezeigt werden (so kann z. B. die
    Rangliste live per Beamer angezeigt werden).

  - Man wird jetzt von Muckturnier mit einem Splashscreen begrüßt, der das Starten und Öffnen eines
    Turniers mit je einem großen Knopf ermöglicht.

====================================================================================================
Release von Muckturnier 0.7.1.2 (15.04.2017)
====================================================================================================

* Verbesserung: Schöneres Layout und besserer Text für die "Einstellungen"-Seite.

* Neu: Änderungen am Datenbanklayout werden jetzt in der Datei "DbLayout" dokumentiert.

* Bugfix: Paar- bzw. Spielernamen werden jetzt – unabhängig von Groß- und Kleinschreibung – auf der
  Ergebnisseite richtig alphabetisch sortiert.

====================================================================================================
Release von Muckturnier 0.7.1.1 (25.02.2017)
====================================================================================================

* Bugfix: Der Titel des Kontextmenüs auf der Spieler-Seite wird jetzt nicht mehr abgeschnitten.

====================================================================================================
Release von Muckturnier 0.7.1 (05.02.2017)
====================================================================================================

* Bugfix: Die beim Export bei „Datenbank erstellt mit“ und „Protokoll erstellt mit“ angezeigten
  Versionen sind jetzt nicht mehr vertauscht.

* Verbesserung: Bei der Eingabe der Spielstände kann jetzt ein Spielstand nicht nur gelöscht,
  sondern auch korrigiert werden: Der bestehende Spielstand wird gelöscht, aber die bisher
  gespeicherten Daten werden als Auswahl gesetzt, und müssen somit nicht mehr manuell ausgewählt
  werden.

* Verbesserung: Die Spieler/Paare-Liste hat jetzt ein schöneres Kontextmenü, mit einem „Name(n)
  ändern“-Eintrag als Alternative zum Editieren per Doppelklick.

* Verbesserung: Es wird jetzt auch bei der Eingabe der Spieler bzw. Paare so gescrollt, dass der
  eingegebene Name immer sichtbar ist.

* Änderung: Die Dateierweiterung für Muckturnier-Datenbanken heißt jetzt „.mtdb“. Diese Erweiterung
  wird laut der Wikipedia-Liste noch von keinem Programm genutzt (Stand 17.01.2017), und ist nicht
  so aussagelos wie „.db“.

* Verbesserung: Es gibt jetzt einen Dialog für Datenexport. Man kann jetzt auswählen, welche
  Datensätze exportiert werden sollen, und wie formatiert werden soll. Außerdem können jetzt
  CSV-Dateien erzeugt werden, die z. B. in einem Tabellenkalkulationsprogramm weiterverarbeitet
  werden können.

====================================================================================================
Release von Muckturnier 0.7 (09.01.2017)
====================================================================================================

* Änderung: Die Codebasis wurde auf C++ portiert. Damit sind jetzt native Windows-Builds möglich.

* Bugfix: Die seit Qt 5.6 überall automatisch eingefügten Tastaturshortcuts werden nicht mehr als
  „&“ im Titel angezeigt.

* Bugfix: Wenn man bei automatischer Tischauswahl jetzt ein „Paar 1“ als „Paar 2“ (oder umgekehrt)
  auswählt, kommt die korrekte Warnung.

* Bugfix: Wenn eine neu angelegte aber unveränderte oder zurückgesetzte Datenbank geöffnet wird,
  dann können die Einstellungen noch verändert werden, wenn noch keine Paare oder Spieler angemeldet
  sind.

* Verbesserung: Der Bereich, in dem auf der „Ergebnisse eintragen“-Seite die Spiele angezeigt
  werden, wird jetzt so angepasst, dass man nur dann Scrollbars sieht, wenn es nicht anders geht.

* Verbesserung: Ein Spielstand kann jetzt auch gespeichert werden, wenn in einem der beiden
  Eingabefelder im letzten Spiel die Return- bzw. Enter-Taste gedrückt wird (ja, das sind zwei
  verschiedene: „Enter“ ist normalerweise im Nummernblock, während „Return“ die normalerweise als
  „Enter“ bekannte Taste ist).

* Verbesserung: Beim Erstellen des Protokolls wird jetzt der Turnierdatenbank-Dateiname als Titel
  gesetzt.

* Verbesserung: Es wird jetzt dafür gesorgt, dass bei der Ergebnisübersicht automatisch so gescrollt
  wird, dass der gerade eingegebene Spielstand sichtbar ist.

====================================================================================================
Release von muckturnier 0.6.1 (01.03.2015)
====================================================================================================

* Verbesserung: Es wird jetzt eine dynamische Versionsnummer generiert, falls eine git-Version
  benutzt wird. Ansonsten wird automatisch die Release-versionsnummer gesetzt.

* Bugfix: Die Spielstand-Seite wird jetzt deaktiviert, wenn eine Datenbank ohne Spielstände geladen
  wird.

* Verbesserung: Beim Neuanlegen bzw. Laden einer Datenbank wird jetzt das Home-Verzeichnis bzw. das
  "Eigene-Dateien"-Verzeichnis des Users als Startpunkt gesetzt.

* Generell: Tippfehler korrigiert, Dokumentation korrigiert, ChangeLog ab jetzt in einem
  vernünftigeren Format (Update 22.06.2018: Die alten Einträge ins neue Format konvertiert).

====================================================================================================
Release von muckturnier 0.5.2 (17.10.2014)
====================================================================================================

* Kleinigkeiten bereinigt.

* Noch ein Release, bevor der Qt-5-Port git master wird :-)

====================================================================================================
Release von muckturnier 0.6 (17.10.2014)
====================================================================================================

* Qt-5-Port von muckturnier.

====================================================================================================
Release von muckturnier 0.5.1 (26.09.2014)
====================================================================================================

* Kleinigkeiten bereinigt (Tippfehler korrigiert, HTML-Journal verschönert, Code aufgeräumt etc.).

* Die Spalte "pairs" (ein beim letzten Aufräumen der Datenbankinternas übersehenes Relikt aus
  Zeiten, als muckturnier nur mit Paaren umgehen konnte) in der Datenbank heißt jetzt – sinnvoller –
  "players". Die "pair"-Spalten heißen jetzt "pair_or_players".

* Es kann jetzt eine zu ladende Datenbank als Kommandozeilenparameter angegeben werden, damit z. B.
  ein "Öffnen mit …" in einem Dateimanager mit muckturnier zum erwarteten Ergebnis führt.

====================================================================================================
Release von muckturnier 0.5 (03.08.2014)
====================================================================================================

* muckturnier ist jetzt groß geworden: es wurde komplett neu geschrieben, und zwar in Python. Es
  gibt jetzt eine „richtige“ graphische Oberfläche, die in Qt implementiert ist (endlich!). Das
  Programm ist so (vorausgesetzt, dass Python, Qt 4 und PyQt4 installiert sind) „einfach so“
  sowohl unter Linux, als auch unter Windows lauffähig; vermutlich auch unter Mac OS X.

* Der Funktionsumfang ist nach wie vor derselbe, allerdings nutzt muckturnier jetzt die
  Möglichkeiten, die man mit einem „richtigen“ GUI hat, so dass jetzt vieles schöner
  implementiert ist.

====================================================================================================
Release von muckturnier 0.4.1 (26.01.2014)
====================================================================================================

* Die Version von muckturnier, die die Datenbank erstellt, wird jetzt mit in der Datenbank
  gespeichert. Außerdem wurde eine Datenbankversionsnummer eingeführt, damit man später mit
  etwaigen Änderungen des Datenbanklayouts einfacher umgehen kann.

* Verschiedene Datenbankchecks hinzugefügt, insbesondere eine Prüfung, ob die Datenbank denn
  überhaupt eine SQLite-3-Datenbank ist.

* Diverse HTML-Fehler behoben und das GUI stellenweise überarbeitet.

====================================================================================================
Release von muckturnier 0.4 (19.01.2014)
====================================================================================================

* Es können jetzt beliebig viele Bobbl pro Runde gespielt werden.

* Die Turniereinstellungen enthalten jetzt auch Spin-Boxen.

====================================================================================================
Release von muckturnier 0.3.2 (04.01.2014)
====================================================================================================

* muckturnier.bat sollte jetzt auch systemweite PHP-Installationen unterstützen.

* Dokumentation ergänzt: Damit PHP unter Windows läuft, muss ein „Microsoft Visual C++
  Redistributable Package“ installiert sein. Außerdem ist PHP 5.4 die letzte Version, die noch
  unter Windows XP bzw. 2003 läuft.

====================================================================================================
Release von muckturnier 0.3.1 (02.03.2013)
====================================================================================================

* Die Eingabefelder im Spielergebnisse-Dialog sind jetzt Spin-Boxen, die mit Mausklicks oder dem
  Mausrad bedient werden können. Vielen Dank an Stephen Morley (http://code.stephenmorley.org/)
  für die Implementierung!

* Fehlerhafte include-Statements korrigiert

* Problem mit POST-Targets unter Windows behoben

* date.timezone im Windows-Startscript gesetzt, damit beim Protokoll keine Warnung angezeigt wird

* GUI aufpoliert

====================================================================================================
Release von muckturnier 0.3 (28.10.2012)
====================================================================================================

* Es können jetzt auch Spiele mit pro Runde ausgelosten Paaren eingegeben werden, bzw. kann von der
  Auslosung abgewichen werden.

* Eingabemaske für Einzelspieler hinzugefügt bzw. angepasst.

* Einstellungen können jetzt graphisch vorgenommen werden.

====================================================================================================
Release von muckturnier 0.2 (21.03.2012)
====================================================================================================

* Es muss mindestens PHP 5.4.0 verwendet werden.

* PHP 5.4.0 bringt den PHP Development Server als eingebauten HTTP-Server mit. Somit ist nur noch
  PHP selbst nötig, um muckturnier auszuführen – man muss keinen extra Webserver mehr aufsetzen.

* Das Speicherbackend auf die neue SQLite3-Klasse von PHP 5.4.0 angepasst.

* Startscripts für Linux und Windows geschrieben, die den Server starten.

====================================================================================================
Release von muckturnier 0.1.1 (05.03.2012)
====================================================================================================

* Statt der Druckversionen der Rangliste und der Rundenübersicht ein Protokoll mit
  individualisierbarem Titel hinzugefügt, das beide Listen enthält. Die Rundenübersicht ist im
  Prinzip hinfällig, da die Runden sowieso bei der Eingabe der Ergebnisse eingesehen werden können.

* Die HTML-Ausgabe aufgeräumt. Die Listen enthalten jetzt nur noch das, was sie tatsächlich brauchen
  (das Protokoll hat keine ID-Tags mehr etc.).

* Probleme mit der HTML-Ausgabe und JavaScript beseitigt. Alle generierten Seiten sollten jetzt
  W3C-konformes XHMTL 1.1 sein, insbesondere das Protokoll.

====================================================================================================
Release von muckturnier 0.1 (04.03.2012)
====================================================================================================

* Erstes Release
