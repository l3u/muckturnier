# SPDX-FileCopyrightText: 2017-2025 Tobias Leupold <tl@stonemx.de>
#
# SPDX-License-Identifier: BSD-2-Clause

CONFIG += c++17
CONFIG += qt
CONFIG += static
CONFIG += object_parallel_to_source

QT += widgets
QT += sql
QT += network
QT += svg

DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x050F00

src=$$PWD/../src
INCLUDEPATH += $$src

TARGET = muckturnier
