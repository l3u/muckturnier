#!/bin/bash

# SPDX-FileCopyrightText: none
#
# SPDX-License-Identifier: CC0-1.0

find $1/src/ -name \*.h -printf 'HEADERS += $$PWD/../../src/%P\n'
find $1/src/ -name \*.cpp -printf 'SOURCES += $$PWD/../../src/%P\n'
