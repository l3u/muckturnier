#!/bin/bash

# SPDX-FileCopyrightText: none
#
# SPDX-License-Identifier: CC0-1.0

share_path="../../build/share/"
if [[ ! -e "$share_path" ]]; then
    echo
    echo "Please build first with cmake!"
    exit 1
fi

version=$(git describe --dirty)
version=${version##*v}

release_base=appimage_files/muckturnier-$version

mkdir appimage_files

tar -xf muckturnier-$version.tar.xz -C appimage_files/

cp -r $share_path $release_base/res/appimage_build/

sed -i "s/@VERSION@/$version/" $release_base/res/appimage_build/make_appimage_build.sh

# Generate debugMode.h
sed "s/#cmakedefine DEBUG_MODE/\/\* #undef DEBUG_MODE \*\//" \
    $release_base/src/debugMode.h.in > $release_base/src/debugMode.h
rm $release_base/src/debugMode.h.in

# Remove unneeded files
rm $release_base/src/ChecksumsDialog.*

# Compile sources.pri
./create_sources_list.sh $release_base > $release_base/res/appimage_build/sources.pri
