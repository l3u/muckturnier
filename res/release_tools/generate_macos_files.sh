#!/bin/bash

# SPDX-FileCopyrightText: none
#
# SPDX-License-Identifier: CC0-1.0

readme_path="../../build/share/muckturnier/Readme.html"
if [[ ! -e "$readme_path" ]]; then
    echo
    echo "Please build first to have the compiled Readme!"
    exit 1
fi

version=$(git describe --dirty)
version=${version##*v}
main_version=${version%%-*}
version_parts=(${main_version//./ })
version_major=${version_parts[0]}
version_minor=${version_parts[1]}
[[ "${version_parts[2]}" != "" ]] && version_bugfix=${version_parts[2]} || version_bugfix=0

mkdir macos_files
release_base=macos_files/muckturnier-$version

tar -xJf muckturnier-$version.tar.xz -C macos_files/

../release_tools/create_iconset.sh \
    $release_base/res/icons/muckturnier \
    macos_files/muckturnier.iconset

../release_tools/create_iconset.sh \
    $release_base/res/icons/application-x-muckturnier \
    macos_files/application-x-muckturnier.iconset

# Generate the DMG installer's background
inkscape --export-type="png" $release_base/res/macos_build/background.svgz \
    -o $release_base/res/macos_build/background-orig.png
# Adjust the DPI so that the size will match the pixels
magick -density 28.5 $release_base/res/macos_build/background-orig.png \
                     $release_base/res/macos_build/background.png
rm $release_base/res/macos_build/background-orig.png

cp $readme_path macos_files/

sed -i "s/@VERSION@/$version/" $release_base/res/macos_build/make_macos_build.sh

sed -i "s/@VERSION@/$version_major.$version_minor.$version_bugfix/" \
       $release_base/res/macos_build/Info.plist

copyrightYears=$(./get_copyright_years.sh "$release_base/src/releaseDate.h")
sed -i "s/@COPYRIGHT@/$copyrightYears/" $release_base/res/macos_build/Info.plist

# Generate debugMode.h
sed "s/#cmakedefine DEBUG_MODE/\/\* #undef DEBUG_MODE \*\//" \
    $release_base/src/debugMode.h.in > $release_base/src/debugMode.h
rm $release_base/src/debugMode.h.in

# Remove unneeded files
rm $release_base/src/ChecksumsDialog.*

# Compile sources.pri
./create_sources_list.sh $release_base > $release_base/res/macos_build/sources.pri
