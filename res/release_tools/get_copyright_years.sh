#!/bin/bash

# SPDX-FileCopyrightText: none
#
# SPDX-License-Identifier: CC0-1.0

if [[ "$1" == "" ]]; then
    echo "Usage: $0 <releaseDate.h path>"
    exit
fi

releaseDate=$(sed -nE "s/.+MT_RELEASE_DATE \"(.+)\"/\1/p" "$1")
releaseYear=$(date +%Y -d "$releaseDate")
echo "2010-$releaseYear"
