#!/bin/bash

# SPDX-FileCopyrightText: none
#
# SPDX-License-Identifier: CC0-1.0

# Fill in variables
git_root=$(git rev-parse --show-toplevel)
version=$(git describe --dirty)
version=${version##*v}
release_date=$(git --no-pager log -1 --format=%ad --date=iso-strict)
readme_date=$(git --no-pager log -1 --format=%ad --date=format:%d.%m.%Y -- \
              "$git_root/doc/Readme.rst")

# Create snapshot
mkdir muckturnier-$version
cd "$git_root"
git archive HEAD | tar -xC res/release_tools/muckturnier-$version

# >>> Get in the released sources
cd res/release_tools/muckturnier-$version

# Generate version.h
sed "s/@VERSION@/$version/" src/version.h.in > src/version.h
rm src/version.h.in

# Generate releaseDate.h
sed "s/@RELEASE_DATE@/$release_date/" src/releaseDate.h.in > src/releaseDate.h
rm src/releaseDate.h.in

# Update the readme's last changed date and version information
sed -i -e "s/@LASTCHANGE@/$readme_date/" doc/Readme.rst
sed -i -e "s/@VERSION@/$version/" doc/Readme.rst

# Remove the .gitignore file
rm .gitignore

# <<< Get out of the released sources
cd ..

# Create the release tarball
tar -cJf muckturnier-$version.tar.xz muckturnier-$version
rm -r muckturnier-$version
