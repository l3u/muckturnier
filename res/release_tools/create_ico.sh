#!/bin/bash

# SPDX-FileCopyrightText: none
#
# SPDX-License-Identifier: CC0-1.0

if [[ $# != 2 ]]; then
    echo "Usage: $0 <source_dir|source.svg(z)> <output.ico>"
    exit
fi

sizes=""

if [[ -f $1 ]]; then
    # We have to create pngs from the given svg
    mkdir /tmp/$$
    for size in 256 48 32 16; do
        output="/tmp/$$/$size-$$.png"
        inkscape --export-filename=$output --export-width=$size --export-height=$size $1
        sizes="$sizes $output"
    done
    magick $sizes $2
    rm -r "/tmp/$$"
else
    # We already have the pngs
    name=`basename $1`
    for size in 256 48 32 16; do
        sizes="$sizes $1/$size-$name.png"
    done
    magick $sizes $2
fi

echo "Created $2 file from $1"
