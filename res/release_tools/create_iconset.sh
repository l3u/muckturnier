#!/bin/bash

# SPDX-FileCopyrightText: none
#
# SPDX-License-Identifier: CC0-1.0

if [[ $# != 2 ]]; then
    echo "Usage: $0 <source_dir> <output.iconset>"
    exit
fi

mkdir "$2"

name=`basename $1`
for size in 16 32 64 128 256 512 1024; do
    cp $1/$size-$name.png $2/icon_${size}x${size}.png
done

cp "$2/icon_32x32.png" "$2/icon_16x16@2x.png"
mv "$2/icon_64x64.png" "$2/icon_32x32@2x.png"
cp "$2/icon_256x256.png" "$2/icon_128x128@2x.png"
cp "$2/icon_512x512.png" "$2/icon_256x256@2x.png"
mv "$2/icon_1024x1024.png" "$2/icon_512x512@2x.png"

echo "Created $2 from $1"
