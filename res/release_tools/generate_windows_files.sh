#!/bin/bash

# SPDX-FileCopyrightText: none
#
# SPDX-License-Identifier: CC0-1.0

readme_path="../../build/share/muckturnier/Readme.html"
if [[ ! -e "$readme_path" ]]; then
    echo
    echo "Please build first to have the compiled Readme!"
    exit 1
fi

version=$(git describe --dirty)
version=${version##*v}
main_version=${version%%-*}
version_parts=(${main_version//./ })
version_major=${version_parts[0]}
version_minor=${version_parts[1]}
[[ "${version_parts[2]}" != "" ]] && version_bugfix=${version_parts[2]} || version_bugfix=0
if [[ $version == *"-"* ]]; then
    version_build_no=${version#*-}
    version_build_no=${version_build_no%%-*}
    [[ "$version_build_no" == "" ]] && version_build_no=0
else
    version_build_no=0
fi

res_dir="windows_files/res"
mkdir -p $res_dir

tar -xJf muckturnier-$version.tar.xz -C windows_files/

release_base=windows_files/muckturnier-$version

# Generate debugMode.h
sed "s/#cmakedefine DEBUG_MODE/\/\* #undef DEBUG_MODE \*\//" \
    $release_base/src/debugMode.h.in > $release_base/src/debugMode.h
rm $release_base/src/debugMode.h.in

unix2dos -n $release_base/LICENSES/GPL-3.0-or-later.txt ./windows_files/COPYING.txt
unix2dos -n $release_base/CHANGELOG.rst ./windows_files/CHANGELOG.rst
unix2dos -n $readme_path $res_dir/Readme.html

cp $release_base/res/pixmaps/*.png $res_dir/
cp $release_base/res/notes_templates/*.svg $res_dir/

../release_tools/create_ico.sh $release_base/res/icons/muckturnier \
                               $release_base/res/windows_build/muckturnier.ico

../release_tools/create_ico.sh $release_base/res/icons/application-x-muckturnier \
                               windows_files/application-x-muckturnier.ico

../release_tools/create_ico.sh $release_base/res/windows_build/installer.svgz \
                               windows_files/installer.ico
../release_tools/create_ico.sh $release_base/res/windows_build/uninstaller.svgz \
                               windows_files/uninstaller.ico

cp $release_base/res/windows_build/welcomefinish.bmp windows_files/welcomefinish.bmp
cp $release_base/res/windows_build/unwelcomefinish.bmp windows_files/unwelcomefinish.bmp

mv $release_base/res/windows_build/muckturnier_setup.nsi windows_files/
sed -i "s/@VERSION@/$version/" windows_files/muckturnier_setup.nsi
sed -i "s/@VERSION_MAJOR@/$version_major/" windows_files/muckturnier_setup.nsi
sed -i "s/@VERSION_MINOR@/$version_minor/" windows_files/muckturnier_setup.nsi
sed -i "s/@VERSION_BUGFIX@/$version_bugfix/" windows_files/muckturnier_setup.nsi

sed -i "s/@VERSION@/$version_major.$version_minor.$version_bugfix.$version_build_no/"  \
       $release_base/res/windows_build/muckturnier.pro

copyrightYears=$(./get_copyright_years.sh "$release_base/src/releaseDate.h")
sed -i "s/@COPYRIGHT@/$copyrightYears/" $release_base/res/windows_build/muckturnier.pro

# Remove unneeded files
rm $release_base/src/ChecksumsDialog.*

# Compile sources.pri
./create_sources_list.sh $release_base > $release_base/res/windows_build/sources.pri
