<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>BookingPage</name>
    <message numerus="yes">
        <source>%n Voranmeldung(en) erfasst</source>
        <translation>
            <numerusform>Eine Voranmeldung erfasst</numerusform>
            <numerusform>%n Voranmeldungen erfasst</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>ClientPage</name>
    <message numerus="yes">
        <source>Suche nach einem Muckturnier-Server via UDP-Port %1 (%n Versuch(e) verbleiben) …</source>
        <translation>
            <numerusform>Suche nach einem Muckturnier-Server via UDP-Port %1 (ein Versuch verbleibt) …</numerusform>
            <numerusform>Suche nach einem Muckturnier-Server via UDP-Port %1 (%n Versuche verbleiben) …</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>ExportDatasetsPage</name>
    <message numerus="yes">
        <source>Einzelergebnisse der folgenden Runden
(es wurde(n) %n Runde(n) gespielt)</source>
        <translation>
            <numerusform>Einzelergebnisse der folgenden Runden
(es wurde eine Runde gespielt)</numerusform>
            <numerusform>Einzelergebnisse der folgenden Runden
(es wurden %n Runden gespielt)</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>ExportPlayersDialog</name>
    <message numerus="yes">
        <source>%n Paar(e) ausgewählt</source>
        <translation>
            <numerusform>Ein Paar ausgewählt</numerusform>
            <numerusform>%n Paare ausgewählt</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n Spieler ausgewählt</source>
        <translation>
            <numerusform>Ein Spieler ausgewählt</numerusform>
            <numerusform>%n Spieler ausgewählt</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>HtmlExporter</name>
    <message numerus="yes">
        <source>Rangliste nach %n Runde(n)</source>
        <translation>
            <numerusform>Rangliste nach einer Runde</numerusform>
            <numerusform>Rangliste nach %n Runden</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>ImportPlayersDialog</name>
    <message numerus="yes">
        <source>%n Paar(e) importieren</source>
        <translation>
            <numerusform>Ein Paar importieren</numerusform>
            <numerusform>%n Paare importieren</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n Spieler importieren</source>
        <translation>
            <numerusform>Einen Spieler importieren</numerusform>
            <numerusform>%n Spieler importieren</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n Paar(e) importiert!</source>
        <translation>
            <numerusform>Ein Paar importiert!</numerusform>
            <numerusform>%n Paare importiert!</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n Spieler importiert!</source>
        <translation>
            <numerusform>Einen Spieler importiert!</numerusform>
            <numerusform>%n Spieler importiert!</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>Es wurde(n) %n mögliche(s) Duplikat gefunden.</source>
        <translation>
            <numerusform>Es wurde ein mögliches Duplikat gefunden.</numerusform>
            <numerusform>Es wurden %n mögliche Duplikate gefunden.</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>NetworkStatusWidget</name>
    <message numerus="yes">
        <source>%n Client(s) angemeldet</source>
        <translation>
            <numerusform>Ein Client angemeldet</numerusform>
            <numerusform>%n Clients angemeldet</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>RegistrationPage</name>
    <message numerus="yes">
        <source>%n Paar(e)</source>
        <translation>
            <numerusform>Ein Paar</numerusform>
            <numerusform>%n Paare</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n Spieler</source>
        <translation>
            <numerusform>Ein Spieler</numerusform>
            <numerusform>%n Spieler</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>Angemeldete Paare (%n ausgeblendet)</source>
        <translation>
            <numerusform>Angemeldete Paare (eines ausgeblendet)</numerusform>
            <numerusform>Angemeldete Paare (%n ausgeblendet)</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>Angemeldete Spieler (%n ausgeblendet)</source>
        <translation>
            <numerusform>Angemeldete Spieler (einer ausgeblendet)</numerusform>
            <numerusform>Angemeldete Spieler (%n ausgeblendet)</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>&lt;p&gt;und %n weitere(s)&lt;/p&gt;</source>
        <comment>für Paare</comment>
        <translation>
            <numerusform>&lt;p&gt;und ein weiteres&lt;/p&gt;</numerusform>
            <numerusform>&lt;p&gt;und %n weitere&lt;/p&gt;</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>&lt;p&gt;und %n weitere(r)&lt;/p&gt;</source>
        <comment>für Spieler</comment>
        <translation>
            <numerusform>&lt;p&gt;und ein weiterer&lt;/p&gt;</numerusform>
            <numerusform>&lt;p&gt;und %n weitere&lt;/p&gt;</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n Tisch(e) besetzt</source>
        <translation>
            <numerusform>ein Tisch besetzt</numerusform>
            <numerusform>%n Tische besetzt</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%1/%n Tisch(en) besetzt</source>
        <translation>
            <numerusform>%1/%n Tisch besetzt</numerusform>
            <numerusform>%1/%n Tischen besetzt</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>es fehlt/fehlen noch %n Spieler!</source>
        <translation>
            <numerusform>es fehlt noch ein Spieler!</numerusform>
            <numerusform>es fehlen noch %n Spieler!</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n Voranmeldung(en) fehlt/fehlen noch</source>
        <translation>
            <numerusform>Eine Voranmeldung fehlt noch</numerusform>
            <numerusform>%n Voranmeldungen fehlen noch</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n allein gekommene(r) Spieler</source>
        <translation>
            <numerusform>ein allein gekommener Spieler</numerusform>
            <numerusform>%n allein gekommene Spieler</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>es würde(n) noch %n Spieler fehlen!</source>
        <translation>
            <numerusform>es würde noch ein Spieler fehlen!</numerusform>
            <numerusform>es würden noch %n Spieler fehlen!</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>damit wäre(n) </source>
        <translation>
            <numerusform>damit wäre </numerusform>
            <numerusform>damit wären </numerusform>
        </translation>
    </message>
</context>
<context>
    <name>SchedulePage</name>
    <message numerus="yes">
        <source>%n Minute(n)</source>
        <translation>
            <numerusform>%n Minute</numerusform>
            <numerusform>%n Minuten</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>ScorePage</name>
    <message numerus="yes">
        <source>Bisher %n Ergebnis(se) aus der %1. Runde</source>
        <translation>
            <numerusform>Bisher ein Ergebnis aus der %1. Runde</numerusform>
            <numerusform>Bisher %n Ergebnisse aus der %1. Runde</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>(%n fehlt/fehlen noch)</source>
        <translation>
            <numerusform>(eines fehlt noch)</numerusform>
            <numerusform>(%n fehlen noch)</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>&lt;p&gt;Tisch %1 wurde noch nicht komplett ausgelost. Folgende(r) Spieler wurde(n) nicht ausgewählt:&lt;/p&gt;&lt;p&gt;%2&lt;/p&gt;</source>
        <translation>
            <numerusform>&lt;p&gt;Tisch %1 wurde noch nicht komplett ausgelost. Folgender Spieler wurde nicht ausgewählt:&lt;/p&gt;&lt;p&gt;%2&lt;/p&gt;</numerusform>
            <numerusform>&lt;p&gt;Tisch %1 wurde noch nicht komplett ausgelost. Folgende Spieler wurden nicht ausgewählt:&lt;/p&gt;&lt;p&gt;%2&lt;/p&gt;</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>ServerPage</name>
    <message numerus="yes">
        <source>%n Client(s) angemeldet</source>
        <translation>
            <numerusform>Ein Client angemeldet</numerusform>
            <numerusform>%n Clients angemeldet</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>TournamentTemplatePage</name>
    <message numerus="yes">
        <source>innerhalb (von) %n Tisches/n</source>
        <translation>
            <numerusform>innerhalb eines Tisches</numerusform>
            <numerusform>innerhalb von %n Tischen</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>TsvExporter</name>
    <message numerus="yes">
        <source>Rangliste nach %n Runde(n)</source>
        <translation>
            <numerusform>Rangliste nach einer Runde</numerusform>
            <numerusform>Rangliste nach %n Runden</numerusform>
        </translation>
    </message>
</context>
</TS>
