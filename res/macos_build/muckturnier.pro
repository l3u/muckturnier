# SPDX-FileCopyrightText: 2020-2025 Tobias Leupold <tl@stonemx.de>
#
# SPDX-License-Identifier: BSD-2-Clause

include(../shared_config.pri)
include(sources.pri)

# Use an upper-case target on Mac
TARGET = Muckturnier

# Build a combined x86_64 and ARM ("universal") binary
#QMAKE_APPLE_DEVICE_ARCHS = x86_64 arm64

QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -Os

LIBS += -dead_strip

RESOURCES.files += ../pixmaps/banner.png
RESOURCES.files += ../pixmaps/notes.png
RESOURCES.files += ../pixmaps/notes_dark.png
RESOURCES.files += ../pixmaps/new.png
RESOURCES.files += ../pixmaps/new_dark.png
RESOURCES.files += ../pixmaps/open.png
RESOURCES.files += ../pixmaps/open_dark.png
RESOURCES.files += ../pixmaps/marker.png
RESOURCES.files += ../pixmaps/marker_dark.png
RESOURCES.files += ../pixmaps/draw.png
RESOURCES.files += ../pixmaps/draw_dark.png
RESOURCES.files += ../pixmaps/unused.png
RESOURCES.files += ../pixmaps/unused_dark.png
RESOURCES.files += ../pixmaps/visible_dark.png
RESOURCES.files += ../pixmaps/hidden_dark.png
RESOURCES.files += ../pixmaps/phonetic.png
RESOURCES.files += ../pixmaps/phonetic_dark.png
RESOURCES.files += ../pixmaps/noautoselect.png
RESOURCES.files += ../pixmaps/noautoselect_dark.png
RESOURCES.files += ../pixmaps/synchronize.png
RESOURCES.files += ../pixmaps/synchronize_dark.png
RESOURCES.files += ../pixmaps/capitalize.png
RESOURCES.files += ../pixmaps/capitalize_dark.png
RESOURCES.files += ../pixmaps/search_as_you_type.png
RESOURCES.files += ../pixmaps/search_as_you_type_dark.png
RESOURCES.files += ../pixmaps/display_logo.png

RESOURCES.files += ../notes_templates/table_number.svg
RESOURCES.files += ../notes_templates/draw_note.svg
RESOURCES.files += ../notes_templates/draw_note_round.svg
RESOURCES.files += ../notes_templates/score_note_1_booger.svg
RESOURCES.files += ../notes_templates/score_note_1_booger_vertical.svg
RESOURCES.files += ../notes_templates/score_note_2_boogers.svg
RESOURCES.files += ../notes_templates/score_note_2_boogers_vertical.svg
RESOURCES.files += ../notes_templates/score_note_3_boogers.svg
RESOURCES.files += ../notes_templates/score_note_3_boogers_vertical.svg

RESOURCES.files += ./qtbase_de.qm
RESOURCES.files += ./muckturnier_de.qm
RESOURCES.files += ./application-x-muckturnier.icns

RESOURCES.files += ../../../Readme.html

RESOURCES.path = Contents/Resources
QMAKE_BUNDLE_DATA += RESOURCES

ICON = muckturnier.icns
QMAKE_INFO_PLIST = Info.plist
