#!/bin/bash

# SPDX-FileCopyrightText: none

# SPDX-License-Identifier: CC0-1.0

qt="/Users/tobias/Qt/5.15.16_static"
#qt="/Users/tobias/Qt/6.7.3_static"

iconutil --convert icns ../../../muckturnier.iconset
mv ../../../muckturnier.icns ./

iconutil --convert icns ../../../application-x-muckturnier.iconset
mv ../../../application-x-muckturnier.icns ./

$qt/bin/lconvert -o qtbase_de.qm $qt/translations/qtbase_de.qm
$qt/bin/lrelease ../muckturnier_de.ts -qm muckturnier_de.qm

$qt/bin/qmake muckturnier.pro -config release

make -j3

# Create a DMG Drag-and-Drop Installer

volname="Muckturnier-@VERSION@-x86_64"

# Create a makeshift writable disk image
mkdir $volname
hdiutil create -size 100m -format UDRW -fs HFS+ -srcfolder $volname -volname $volname tmp.dmg
hdiutil attach -readwrite -noverify -noautoopen -nobrowse tmp.dmg

# Copy our stuff into the image
cp README.html /Volumes/$volname
cp -r Muckturnier.app /Volumes/$volname/
ln -s /Applications /Volumes/$volname/Programme
mkdir /Volumes/$volname/.background
cp background.png /Volumes/$volname/.background

# Format the Finder view
osascript format_finder.applescript $volname

# Finish the preparation
chmod -Rf go-w /Volumes/$volname
bless --folder /Volumes/$volname --openfolder /Volumes/$volname
rm -rf /Volumes/$volname/.fseventsd
hdiutil detach /Volumes/$volname

# Create the compressed read-only image
hdiutil convert tmp.dmg -format ULMO -o $volname.dmg
mv $volname.dmg ../../../
