-- SPDX-FileCopyrightText: 2008-2014 Andrey Tarantsov
-- SPDX-FileCopyrightText: 2020 Andrew Janke
-- SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
--
-- SPDX-License-Identifier: MIT

-- This script has been shamelessly stolen from https://github.com/create-dmg/create-dmg
-- and then de-generalized to make is usable as-is for creating a Muckturnier DMG installer

on run (volumeName)
    tell application "Finder"
        tell disk (volumeName as string)
            open

            set theXOrigin to 0
            set theYOrigin to 0
            set theWidth to 700
            set theHeight to 400

            set theBottomRightX to (theXOrigin + theWidth)
            set theBottomRightY to (theYOrigin + theHeight)
            set dsStore to "\"" & "/Volumes/" & volumeName & "/" & ".DS_Store\""

            tell container window
                set current view to icon view
                set toolbar visible to false
                set statusbar visible to false
                set the bounds to {theXOrigin, theYOrigin, theBottomRightX, theBottomRightY}
                set statusbar visible to false
            end tell

            set opts to the icon view options of container window
            tell opts
                set icon size to 96
                set text size to 16
                set arrangement to not arranged
            end tell
            set background picture of opts to file ".background:background.png"

            set position of item "README.html" to {130, 270}
            set position of item "Muckturnier.app" to {360, 270}
            set position of item "Programme" to {580, 270}

            close
            open
            delay 1

            tell container window
                set statusbar visible to false
                set the bounds to {theXOrigin, theYOrigin, theBottomRightX - 10, theBottomRightY - 10}
            end tell
        end tell

        delay 1

        tell disk (volumeName as string)
            tell container window
                set statusbar visible to false
                set the bounds to {theXOrigin, theYOrigin, theBottomRightX, theBottomRightY}
            end tell
        end tell

        delay 3

        set waitTime to 0
        set ejectMe to false
        repeat while ejectMe is false
            delay 1
            set waitTime to waitTime + 1
            if (do shell script "[ -f " & dsStore & " ]; echo $?") = "0" then set ejectMe to true
        end repeat
        log "waited " & waitTime & " seconds for .DS_Store to be created."
    end tell
end run
