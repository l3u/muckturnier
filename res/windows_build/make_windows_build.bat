@rem SPDX-FileCopyrightText: none
@rem
@rem SPDX-License-Identifier: CC0-1.0

set PATH=C:\Qt\mingw810_32\bin;%PATH%
set QT=C:\Qt\5.15.16_static

%QT%\bin\lconvert -o ..\..\..\res\qtbase_de.qm %QT%\translations\qtbase_de.qm
%QT%\bin\lrelease ..\..\res\muckturnier_de.ts -qm ..\..\..\res\muckturnier_de.qm
%QT%\bin\qmake muckturnier.pro
mingw32-make -j3 release
xcopy /Y release\muckturnier.exe .\..\..\..\
