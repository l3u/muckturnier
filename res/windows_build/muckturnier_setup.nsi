﻿; SPDX-FileCopyrightText: 2017-2025 Tobias Leupold <tl@stonemx.de>
;
; SPDX-License-Identifier: BSD-2-Clause

; -------------------- Includes

!include "MUI2.nsh"
!include "FileFunc.nsh"

; -------------------- General settings

Unicode true

!define MT_VERSION "@VERSION@"
!define MT_VERSION_MAJOR @VERSION_MAJOR@
!define MT_VERSION_MINOR @VERSION_MINOR@
!define MT_VERSION_BUGFIX @VERSION_BUGFIX@

!define MT_UNINSTALL_INFO "Software\Microsoft\Windows\CurrentVersion\Uninstall\Muckturnier"
!define MT_SM_ENTRY "$SMPROGRAMS\Muckturnier"
!define MT_MTDB_ENTRY "muckturnier.tournamentdatabase"

!define MUI_ICON "installer.ico"
!define MUI_UNICON "uninstaller.ico"
!define MUI_WELCOMEFINISHPAGE_BITMAP "welcomefinish.bmp"
!define MUI_UNWELCOMEFINISHPAGE_BITMAP "unwelcomefinish.bmp"

Name "Muckturnier"
OutFile "muckturnier-${MT_VERSION}-i386_setup.exe"
InstallDir "$PROGRAMFILES\Muckturnier\"
InstallDirRegKey HKLM "${MT_UNINSTALL_INFO}" "InstallLocation"
SetCompressor /SOLID lzma

; -------------------- Installer Pages

; Welcome page
!insertmacro MUI_PAGE_WELCOME

; License agreement
!define MUI_PAGE_CUSTOMFUNCTION_SHOW changeLicenseFont
!define MUI_PAGE_CUSTOMFUNCTION_LEAVE checkForInstalledMuckturnier
!insertmacro MUI_PAGE_LICENSE "COPYING.txt"

; Target directory
!insertmacro MUI_PAGE_DIRECTORY

; Installed files
!insertmacro MUI_PAGE_INSTFILES

; Finished
!define MUI_FINISHPAGE_RUN
!define MUI_FINISHPAGE_RUN_TEXT "Muckturnier starten"
!define MUI_FINISHPAGE_RUN_FUNCTION "startMuckturnier"
!insertmacro MUI_PAGE_FINISH

; -------------------- Uninstaller Pages

; Welcome page
!insertmacro MUI_UNPAGE_WELCOME

; Confirm uninstall
!insertmacro MUI_UNPAGE_CONFIRM

; Uninstalled files
!insertmacro MUI_UNPAGE_INSTFILES

; -------------------- l10n

!insertmacro MUI_LANGUAGE "German"

; -------------------- Functions

Function checkForInstalledMuckturnier
    ; Check uninstaller registry entry
    ReadRegStr $R0 HKLM "${MT_UNINSTALL_INFO}" "QuietUninstallString"
    StrCmp $R0 "" checkInstalledEnd

    ; Get the installed version
    ReadRegStr $R1 HKLM "${MT_UNINSTALL_INFO}" "DisplayVersion"
    ReadRegDWORD $R2 HKLM "${MT_UNINSTALL_INFO}" "VersionMajor"
    ReadRegDWORD $R3 HKLM "${MT_UNINSTALL_INFO}" "VersionMinor"
    ReadRegDWORD $R4 HKLM "${MT_UNINSTALL_INFO}" "VersionBugfix"

    ; Check the installed version
    IntCmp $R2 ${MT_VERSION_MAJOR} checkMinor informAboutUninstall versionOlder
    checkMinor:
    IntCmp $R3 ${MT_VERSION_MINOR} checkBugfix informAboutUninstall versionOlder
    checkBugfix:
    IntCmp $R4 ${MT_VERSION_BUGFIX} informAboutUninstall informAboutUninstall versionOlder

    versionOlder:
    MessageBox MB_YESNO|MB_DEFBUTTON2|MB_ICONEXCLAMATION \
               "Es ist bereits Muckturnier $R1 installiert. Diese Version ist neuer als die, die \
                jetzt installiert werden soll (${MT_VERSION})!$\n$\n\
                Soll wirklich ein Downgrade durchgeführt werden?" \
               IDNO abortInstallation

    informAboutUninstall:
    MessageBox MB_OKCANCEL|MB_ICONINFORMATION \
               "Es wurde eine bestehende Installation von Muckturnier gefunden (Version $R1).$\n$\n\
                Bei einem Update wird diese zunächst deinstalliert, damit keine veralteten Dateien \
                und/oder Verknüpfungen verbleiben." \
               IDOK uninstall

    abortInstallation:
    MessageBox MB_OK|MB_ICONINFORMATION "Die Installation wurde abgebrochen."
    Quit

    ; Uninstall the installed version
    uninstall:
    ClearErrors
    ExecWait $R0
    IfErrors uninstallFailed uninstallFinished

    ; Something went wrong
    uninstallFailed:
    ReadRegStr $R5 HKLM "${MT_UNINSTALL_INFO}" "InstallLocation"
    MessageBox MB_YESNO|MB_ICONEXCLAMATION \
               "Das Entfernen der bestehenden Installation von Muckturnier $R1 ist fehlgeschlagen! \
                $\n$\n\
                Wenn für die neue Installation das selbe Zielverzeichnis ($R5) gewählt wird, \
                verbleiben evtl. veraltete Dateien. Dies könnte zu Problemen führen.$\n$\n\
                Es wird empfohlen, die bestehende Installation manuell zu entfernen und dann \
                Muckturnier erneut zu installieren.$\n$\n\
                Soll trotzdem fortgefahren werden?" \
               IDNO abortInstallation IDYES checkInstalledEnd

    ; The old version has been successfully removed
    uninstallFinished:
    MessageBox MB_OK|MB_ICONINFORMATION "Muckturnier $R1 wurde erfolgreich deinstalliert!"

    checkInstalledEnd:
FunctionEnd

Function changeLicenseFont
    FindWindow $0 "#32770" "" $HWNDPARENT
    CreateFont $1 "Lucida Console" "7"
    GetDlgItem $0 $0 1000
    SendMessage $0 ${WM_SETFONT} $1 1
FunctionEnd

Function startMuckturnier
    ExecShell "" "${MT_SM_ENTRY}\Muckturnier.lnk"
FunctionEnd

; -------------------- Install section

Section "Muckturnier"
    SetOutPath "$INSTDIR"

    File "muckturnier.exe"
    File "COPYING.txt"
    File "CHANGELOG.rst"
    File "application-x-muckturnier.ico"

    SetOutPath "$INSTDIR\res"

    File "res\Readme.html"
    File "res\muckturnier_de.qm"
    File "res\qtbase_de.qm"

    File "res\banner.png"
    File "res\hidden.png"
    File "res\notes.png"
    File "res\open.png"
    File "res\new.png"
    File "res\noautoselect.png"
    File "res\unused.png"
    File "res\visible.png"
    File "res\marker.png"
    File "res\draw.png"
    File "res\phonetic.png"
    File "res\synchronize.png"
    File "res\capitalize.png"
    File "res\search_as_you_type.png"
    File "res\display_logo.png"

    File "res\table_number.svg"
    File "res\draw_note.svg"
    File "res\draw_note_round.svg"
    File "res\score_note_1_booger.svg"
    File "res\score_note_1_booger_vertical.svg"
    File "res\score_note_2_boogers.svg"
    File "res\score_note_2_boogers_vertical.svg"
    File "res\score_note_3_boogers.svg"
    File "res\score_note_3_boogers_vertical.svg"

    ; Create a start menu entry
    CreateDirectory "${MT_SM_ENTRY}"
    CreateShortcut "${MT_SM_ENTRY}\Muckturnier.lnk" "$INSTDIR\muckturnier.exe" "" ""
    CreateShortcut "${MT_SM_ENTRY}\Handbuch zu Muckturnier.lnk" "$INSTDIR\res\Readme.html" "" ""
    CreateShortcut "${MT_SM_ENTRY}\Muckturnier deinstallieren.lnk" "$INSTDIR\uninstall.exe" "" ""

    ; Check for an existing ".mtdb" entry and create a backup if needed
    ReadRegStr $1 HKCR ".mtdb" ""
    StrCmp "$1" "" SkipBackup ; it's empty
    StrCmp "$1" "Muckturnier-Turnierdatenbank" SkipBackup ; it's set by this installer
    WriteRegStr HKCR ".mtdb" "${MT_MTDB_ENTRY}_backup" "$1"
    SkipBackup:

    ; Create an ".mtdb" entry
    WriteRegStr HKCR ".mtdb" "" "${MT_MTDB_ENTRY}"
    WriteRegStr HKCR "${MT_MTDB_ENTRY}" "" "Muckturnier-Turnierdatenbank"
    WriteRegStr HKCR "${MT_MTDB_ENTRY}\DefaultIcon" "" "$INSTDIR\application-x-muckturnier.ico"
    WriteRegStr HKCR "${MT_MTDB_ENTRY}\shell" "" "open"
    WriteRegStr HKCR "${MT_MTDB_ENTRY}\shell\open" "" "Turnier öffnen"
    WriteRegStr HKCR "${MT_MTDB_ENTRY}\shell\open\command" "" "$INSTDIR\muckturnier.exe $\"%1$\""

    ; Write uninstall information
    WriteRegStr HKLM "${MT_UNINSTALL_INFO}" "DisplayName" "Muckturnier"
    WriteRegDWORD HKLM "${MT_UNINSTALL_INFO}" "VersionMajor" "${MT_VERSION_MAJOR}"
    WriteRegDWORD HKLM "${MT_UNINSTALL_INFO}" "VersionMinor" "${MT_VERSION_MINOR}"
    WriteRegDWORD HKLM "${MT_UNINSTALL_INFO}" "VersionBugfix" "${MT_VERSION_BUGFIX}"
    ${GetSize} "$INSTDIR" "/S=0K" $0 $1 $2
    IntFmt $0 "0x%08X" $0
    WriteRegDWORD HKLM "${MT_UNINSTALL_INFO}" "EstimatedSize" "$0"
    WriteRegStr HKLM "${MT_UNINSTALL_INFO}" "InstallLocation" "$\"$INSTDIR$\""
    WriteRegStr HKLM "${MT_UNINSTALL_INFO}" "UninstallString" "$\"$INSTDIR\uninstall.exe$\""
    WriteRegStr HKLM "${MT_UNINSTALL_INFO}" "QuietUninstallString" "$\"$INSTDIR\uninstall.exe$\" /S"
    WriteRegStr HKLM "${MT_UNINSTALL_INFO}" "DisplayIcon" "$\"$INSTDIR\muckturnier.exe$\""
    WriteRegStr HKLM "${MT_UNINSTALL_INFO}" "DisplayVersion" "${MT_VERSION}"
    WriteRegStr HKLM "${MT_UNINSTALL_INFO}" "Publisher" "Tobias Leupold"
    WriteRegStr HKLM "${MT_UNINSTALL_INFO}" "URLInfoAbout" "https://muckturnier.org/"

    ; Create the uninstaller
    WriteUninstaller "$INSTDIR\uninstall.exe"
SectionEnd

; -------------------- Uninstall section

Section "Uninstall"
    Delete "$INSTDIR\res\Readme.html"
    Delete "$INSTDIR\res\muckturnier_de.qm"
    Delete "$INSTDIR\res\qtbase_de.qm"

    Delete "$INSTDIR\res\banner.png"
    Delete "$INSTDIR\res\hidden.png"
    Delete "$INSTDIR\res\notes.png"
    Delete "$INSTDIR\res\open.png"
    Delete "$INSTDIR\res\new.png"
    Delete "$INSTDIR\res\noautoselect.png"
    Delete "$INSTDIR\res\unused.png"
    Delete "$INSTDIR\res\visible.png"
    Delete "$INSTDIR\res\marker.png"
    Delete "$INSTDIR\res\draw.png"
    Delete "$INSTDIR\res\phonetic.png"
    Delete "$INSTDIR\res\synchronize.png"
    Delete "$INSTDIR\res\capitalize.png"
    Delete "$INSTDIR\res\search_as_you_type.png"
    Delete "$INSTDIR\res\display_logo.png"

    Delete "$INSTDIR\res\table_number.svg"
    Delete "$INSTDIR\res\draw_note.svg"
    Delete "$INSTDIR\res\draw_note_round.svg"
    Delete "$INSTDIR\res\score_note.svg"
    Delete "$INSTDIR\res\score_note_vertical.svg"
    Delete "$INSTDIR\res\score_note_3_boogers.svg"
    Delete "$INSTDIR\res\score_note_3_boogers_vertical.svg"

    RMDir "$INSTDIR\res"

    Delete "$INSTDIR\muckturnier.exe"
    Delete "$INSTDIR\COPYING.txt"
    Delete "$INSTDIR\CHANGELOG.rst"
    Delete "$INSTDIR\application-x-muckturnier.ico"

    Delete "$INSTDIR\uninstall.exe"

    RMDir "$INSTDIR"

    ; Remove the start menu entry
    Delete "${MT_SM_ENTRY}\Muckturnier.lnk"
    Delete "${MT_SM_ENTRY}\Handbuch zu Muckturnier.lnk"
    Delete "${MT_SM_ENTRY}\Muckturnier deinstallieren.lnk"
    RMDir "${MT_SM_ENTRY}"

    ; Check for a saved existing ".mtdb" entry
    ReadRegStr $1 HKCR ".mtdb" ""
    StrCmp $1 "${MT_MTDB_ENTRY}" 0 SkipRestore ; The key wasn't set by this installer
    ReadRegStr $1 HKCR ".mtdb" "${MT_MTDB_ENTRY}_backup"

    ; Check if we don't have an ".mtdb" backup. If so, delete the whole key and finish
    StrCmp $1 "" 0 Restore
    DeleteRegKey HKCR ".mtdb"
    Goto SkipRestore

    ; Restore the backed up ".mtdb" entry
    Restore:
    WriteRegStr HKCR ".mtdb" "" $1
    DeleteRegValue HKCR ".mtdb" "${MT_MTDB_ENTRY}_backup"
    DeleteRegKey HKCR "Muckturnier-Turnierdatenbank" ; Delete key with association name settings
    SkipRestore:

    ; Remove the uninstall information
    DeleteRegKey HKLM "${MT_UNINSTALL_INFO}"

SectionEnd
