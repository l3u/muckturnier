# SPDX-FileCopyrightText: 2017-2024 Tobias Leupold <tl at stonemx dot de>
#
# SPDX-License-Identifier: BSD-2-Clause

include(../shared_config.pri)
include(sources.pri)

QMAKE_TARGET_COMPANY = Muckturnier.org
QMAKE_TARGET_DESCRIPTION = "Das Programm f�r die Turnierleitung"
QMAKE_TARGET_COPYRIGHT = "Copyright (c) @COPYRIGHT@ Tobias Leupold"
QMAKE_TARGET_PRODUCT = Muckturnier
VERSION = @VERSION@
RC_ICONS = muckturnier.ico
