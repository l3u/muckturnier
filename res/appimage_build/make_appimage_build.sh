#!/bin/bash

# SPDX-FileCopyrightText: none
#
# SPDX-License-Identifier: CC0-1.0

version="@VERSION@"
qt="/home/tobias/Qt/5.15.16_static"

# Prepare the translations
$qt/bin/lconvert -o qtbase_de.qm $qt/translations/qtbase_de.qm
$qt/bin/lrelease ../muckturnier_de.ts -qm muckturnier_de.qm

# Build Muckturnier
$qt/bin/qmake muckturnier.pro -config release
make -j$(nproc)
strip muckturnier

# Prepare an AppDir with our resources
mkdir -p AppDir/usr/
cp -r share AppDir/usr/
cp *.qm AppDir/usr/share/muckturnier/

# Maybe update the linuxdeploy binary from https://github.com/linuxdeploy/linuxdeploy/releases/

# Build the AppImage
linuxdeploy \
    --appdir AppDir \
    --executable muckturnier \
    --desktop-file ../org.muckturnier.muckturnier.desktop \
    --icon-file AppDir/usr/share/icons/hicolor/128x128/apps/muckturnier.png \
    --output appimage

cp Muckturnier-x86_64.AppImage ../../../Muckturnier-$version-x86_64.AppImage
