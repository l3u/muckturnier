# SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
#
# SPDX-License-Identifier: BSD-2-Clause

include(../shared_config.pri)
include(sources.pri)

QTPLUGIN += composeplatforminputcontextplugin

QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -Os
