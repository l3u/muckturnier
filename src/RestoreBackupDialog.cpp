// SPDX-FileCopyrightText: 2018-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "RestoreBackupDialog.h"

#include "Database/Database.h"

#include "shared/BackupEngine.h"

// Qt includes
#include <QDebug>
#include <QRegularExpression>
#include <QMessageBox>
#include <QDir>
#include <QVBoxLayout>
#include <QListWidget>
#include <QLabel>
#include <QApplication>
#include <QTimer>

// C++ includes
#include <functional>

RestoreBackupDialog::RestoreBackupDialog(QWidget *parent, Database *db, BackupEngine &backupEngine,
                                         const QString &dbToOverwrite, bool autoAccept)
    : TitleDialog(parent),
      m_db(db), m_dbToOverwrite(dbToOverwrite), m_autoAccept(autoAccept)
{
    setTitle(tr("Backup wiederherstellen"));

    QLabel *header = new QLabel(tr("Für die Turnierdatenbank<br/><b>%1.mtdb</b><br/>"
                                   "wurden Backups zu folgenden Zeitpunkten gefunden:").arg(
                                   backupEngine.baseName().toHtmlEscaped()));
    header->setWordWrap(true);
    layout()->addWidget(header);

    m_backups = new QListWidget;
    layout()->addWidget(m_backups);
    connect(m_backups, &QListWidget::itemSelectionChanged,
            this, std::bind(&TitleDialog::setOkButtonEnabled, this, true));

    addButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    setButtonText(QDialogButtonBox::Ok, tr("Wiederherstellen"));
    setOkButtonEnabled(false);

    // Search for Backups and fill in the data

    QDir dir(backupEngine.path());
    dir.setNameFilters( { backupEngine.baseName() + QLatin1String("_*.mtdb") } );
    QRegularExpression re(QStringLiteral("^")
                          + QRegularExpression::escape(backupEngine.baseName())
                          + QLatin1String("_(\\d{4}-\\d{2}-\\d{2}_"
                                          "\\d{2}-\\d{2}-\\d{2})_?(\\d+)?\\.mtdb$"));

    const auto entryList = dir.entryList(QDir::Files, QDir::Name);
    for (const QString &file : entryList) {
        QRegularExpressionMatch match = re.match(file);
        if (match.hasMatch()) {
            QString date = formatDateString(match.captured(1));
            if (! match.captured(2).isEmpty()) {
                date += QStringLiteral(" (%1)").arg(match.captured(2));
            }
            QListWidgetItem *item = new QListWidgetItem(date);
            item->setData(Qt::UserRole, QVariant(backupEngine.path() + QLatin1String("/") + file));
            m_backups->addItem(item);
        }
    }

    // Select the opened backup in case we have one and searched for the base db
    for (int i = 0; i < m_backups->count(); i++) {
        if (m_backups->item(i)->data(Qt::UserRole).toString() == m_db->dbFile()) {
            m_backups->item(i)->setText(tr("%1 (jetzt geöffnet)").arg(
                                           m_backups->item(i)->text()));
            m_backups->setCurrentItem(m_backups->item(i));
            break;
        }
    }

    if (m_backups->count() == 0) {
        m_backups->addItem(tr("Keine Backups gefunden"));
        m_backups->setEnabled(false);
    }

    if (m_autoAccept) {
        QTimer::singleShot(0, this, &RestoreBackupDialog::runAutoAccept);
    }
}

void RestoreBackupDialog::runAutoAccept()
{
    accept();
    TitleDialog::accept();
}

QString RestoreBackupDialog::formatDateString(const QString &backupDate) const
{
    const QDateTime backupDateTime = QDateTime::fromString(backupDate,
                                                           QStringLiteral("yyyy-MM-dd_hh-mm-ss"));

    if (QDateTime::currentDateTime().toString(QStringLiteral("dd.MM.yyyy"))
        != backupDateTime.toString(QStringLiteral("dd.MM.yyyy"))) {

        return backupDateTime.toString(QStringLiteral("dd.MM.yyyy hh:mm:ss"));
    } else {
        return backupDateTime.toString(QStringLiteral("hh:mm:ss"));
    }
}

void RestoreBackupDialog::accept()
{
    QFileInfo backupInfo(m_backups->currentItem()->data(Qt::UserRole).toString());
    QFileInfo dbToOverwriteInfo(m_dbToOverwrite);

    if (QMessageBox::question(this,
            tr("Backup wiederherstellen"),
            tr("<p><b>Soll das Backup wirklich wiederhergestellt werden?</b></p>"
               "<p>Die Turnierdatenbank „%1“ wird dabei mit dem Backup „%2“ überschrieben und "
               "alle Änderungen gehen verloren!</p>").arg(
               dbToOverwriteInfo.fileName().toHtmlEscaped(),
               backupInfo.fileName().toHtmlEscaped()),
            QMessageBox::Ok | QMessageBox::Cancel) == QMessageBox::Cancel) {

        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    const QString openedDb = m_db->dbFile();
    Q_EMIT closeTournament();

    if (m_db->checkDatabase(backupInfo.filePath()) != Database::Error::NoError) {
        QApplication::restoreOverrideCursor();
        Q_EMIT statusUpdate(tr("Wiederherstellung fehlgeschlagen!"));
        QMessageBox::critical(this,
            tr("Backup wiederherstellen"),
            tr("<p><b>Das Backup „%1“ kann nicht wiederhergestellt werden:</b></p>").arg(
               QDir::toNativeSeparators(backupInfo.filePath()).toHtmlEscaped())
            + m_db->errorMessage()
            + tr("<p>Die zuletzt geöffnete Datenbank wird jetzt neu geladen.</p>"));
        Q_EMIT openTournament(openedDb);
        return;

    } else {
        m_db->closeDatabaseConnection();
    }

    Q_EMIT statusUpdate(tr("Stelle Backup wieder her …"));

    const bool dbToOverwriteExists = QFileInfo::exists(m_dbToOverwrite);

    // To be sure, create a backup of the database to be overwritten
    BackupEngine oldDbEngine(m_dbToOverwrite);
    if (dbToOverwriteExists && ! oldDbEngine.createBackup(QStringLiteral("~"))) {
        QApplication::restoreOverrideCursor();
        Q_EMIT statusUpdate(tr("Wiederherstellung fehlgeschlagen!"));
        QMessageBox::critical(this,
            tr("Backup wiederherstellen"),
            tr("<p><b>Wiederherstellung fehlgeschlagen</b></p>"
               "<p>Es konnte keine Sicherungskopie der Datenbank „%1“ erstellt werden.</p>"
               "<p>Die zuletzt geöffnete Datenbank wird jetzt neu geladen.</p>").arg(
               dbToOverwriteInfo.fileName().toHtmlEscaped()));
        Q_EMIT openTournament(openedDb);
        return;
    }

    // Copy the backup to restore

    if (dbToOverwriteExists && ! QFile::remove(m_dbToOverwrite)) {
        // This shouldn't happen
        QApplication::restoreOverrideCursor();
        Q_EMIT statusUpdate(tr("Wiederherstellung fehlgeschlagen!"));
        QMessageBox::critical(this,
            tr("Backup wiederherstellen"),
            tr("<p><b>Wiederherstellung fehlgeschlagen</b></p>"
               "<p>Das Backup „%1“ konnte nicht wiederhergestellt werden, weil die zu "
               "überschreibende Datenbank „%2“ nicht gelöscht werden konnte.</p>"
               "<p>Die zuletzt geöffnete Datenbank wird jetzt neu geladen.</p>").arg(
               backupInfo.fileName().toHtmlEscaped(),
               dbToOverwriteInfo.fileName().toHtmlEscaped()));
        Q_EMIT openTournament(openedDb);
        return;
    }

    QString dbToOverwriteBackup;
    if (dbToOverwriteExists) {
        dbToOverwriteBackup = oldDbEngine.path() + QLatin1String("/") + oldDbEngine.backupFile();
    }

    if (! QFile::copy(backupInfo.filePath(), m_dbToOverwrite)) {
        // This shouldn't happen either
        QApplication::restoreOverrideCursor();
        Q_EMIT statusUpdate(tr("Wiederherstellung fehlgeschlagen!"));
        QMessageBox::critical(this,
            tr("Backup wiederherstellen"),
            tr("<p><b>Wiederherstellung fehlgeschlagen</b></p>"
               "<p>Das Backup „%1“ konnte nicht nach „%2“ kopiert werden.</p>"
               "<p>Die Sicherungskopie „%3“ der zu überschreibenden Datenbank wird jetzt wieder "
               "hergestellt und die zuletzt geöffnete Datenbank neu geladen.</p>").arg(
               backupInfo.fileName().toHtmlEscaped(),
               dbToOverwriteInfo.fileName().toHtmlEscaped(),
               oldDbEngine.backupFile().toHtmlEscaped()));

        if (dbToOverwriteExists && ! QFile::rename(dbToOverwriteBackup, m_dbToOverwrite)) {
            // This really should not happen ...
            QMessageBox::critical(this,
                tr("Backup wiederherstellen"),
                tr("<p><b>Wiederherstellung fehlgeschlagen</b></p>"
                   "<p>Die Sicherungskopie „%1“ der zu überschreibenden Datenbank konnte nicht "
                   "nach „%2“ umbenannt werden.</p>").arg(
                   oldDbEngine.backupFile().toHtmlEscaped(),
                   dbToOverwriteInfo.fileName().toHtmlEscaped()));
            return;
        }

        Q_EMIT openTournament(openedDb);
        return;
    }

    // Everything is fine :-)

    Q_EMIT openTournament(m_dbToOverwrite);
    QApplication::restoreOverrideCursor();

    QMessageBox::information(this,
        tr("Backup wiederherstellen"),
        tr("<p><b>Backup erfolgreich wiederhergestellt</b></p>"
           "<p>Das Backup „%1“ der Turnierdatenbank „%2“ wurde erfolgreich wiederhergestellt!"
           "</p>").arg(backupInfo.fileName().toHtmlEscaped(),
                       dbToOverwriteInfo.fileName().toHtmlEscaped()));

    if (dbToOverwriteExists && ! QFile::remove(dbToOverwriteBackup)) {
        // Also this should not happen
        QMessageBox::warning(this,
            tr("Backup wiederherstellen"),
            tr("Die temporäre Sicherungskopie „%1“, die vor dem Überschreiben der Datenbank "
               "angelegt wurde, konnte nicht gelöscht werden. Bitte manuell entfernen.").arg(
               QDir::toNativeSeparators(dbToOverwriteBackup).toHtmlEscaped()));
    }

    TitleDialog::accept();
}
