// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef ADJUSTDRAWDIALOG_H
#define ADJUSTDRAWDIALOG_H

// Local includes
#include "shared/TitleDialog.h"
#include "shared/Draw.h"
#include "shared/Players.h"

// Qt includes
#include <QHash>

// Local classes
class TemporaryFileHelper;

// Qt classes
class QTableWidget;

class AdjustDrawDialog : public TitleDialog
{
    Q_OBJECT

public:
    explicit AdjustDrawDialog(QWidget *parent, TemporaryFileHelper *tmpHelper,
                              const QVector<Players::Data> &playersData, bool isPairMode,
                              QString namesSeparator);

public Q_SLOTS:
    void listWasModified();

Q_SIGNALS:
    void adoptAdjustment(const QVector<QString> &names, const QVector<Draw::Seat> &seats);

private Q_SLOTS:
    void resizeView();
    void printList();

private: // Functions
    QVector<int> findIncompletePairs(int pair, QVector<QVector<QString>> &draw) const;
    bool compilePairsSameRole(int pair, QVector<QVector<QString>> &draw);
    bool compilePairsSwitchRole(int pair, QVector<QVector<QString>> &draw);
    QVector<int> findSinglePairs(int pair, QVector<QVector<QString>> &draw) const;
    bool compileOpponentPairs(int pair, QVector<QVector<QString>> &draw);
    bool compileSameTypePairs(int pair, QVector<QVector<QString>> &draw);
    bool fillTableGaps(QVector<QVector<QString>> &draw);

private: // Variables
    TemporaryFileHelper *m_tmpHelper;
    int m_tables;
    QTableWidget *m_changes;
    QHash<QString, Draw::Seat> m_moved;
    QVector<QString> m_names;
    QVector<Draw::Seat> m_newSeats;

};

#endif // ADJUSTDRAWDIALOG_H
