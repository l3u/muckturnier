// SPDX-FileCopyrightText: 2019-2020 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef ABSTRACTJSONENGINE_H
#define ABSTRACTJSONENGINE_H

// Local includes
#include "shared/StructTemplate.h"

// Qt includes
#include <QObject>
#include <QJsonObject>

// Qt classes
class QJsonObject;

using namespace StructTemplate;

class AbstractJsonEngine : public QObject
{
    Q_OBJECT

public:
    explicit AbstractJsonEngine(QObject *parent);

protected:
    QByteArray jsonObjectToByteArray(const QJsonObject &jsonObject) const;
    TSuccess<QJsonObject> jsonObjectFromByteArray(const QByteArray &byteArray) const;

};

#endif // ABSTRACTJSONENGINE_H
