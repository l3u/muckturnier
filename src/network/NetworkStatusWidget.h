// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef NETWORKSTATUSWIDGET_H
#define NETWORKSTATUSWIDGET_H

// Qt includes
#include <QWidget>

// Local classes
class IconSizeEngine;
class Led;

// Qt classes
class QLabel;

class NetworkStatusWidget : public QWidget
{
    Q_OBJECT

public:
    enum Mode {
        Server,
        Client
    };

    explicit NetworkStatusWidget(IconSizeEngine *iconSizeEngine, QWidget *parent = nullptr);
    void setMode(Mode mode);
    void setConnected(bool state);

public Q_SLOTS:
    void clientCountChanged(int count);
    void tx();
    void rx();

private: // Variables
    QLabel *m_connectionLabel;
    Led *m_connectionLed;
    Led *m_txLed;
    Led *m_rxLed;

};

#endif // NETWORKSTATUSWIDGET_H
