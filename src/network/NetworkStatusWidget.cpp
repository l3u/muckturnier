// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "NetworkStatusWidget.h"
#include "Led.h"

#include "SharedObjects/IconSizeEngine.h"

// Qt includes
#include <QHBoxLayout>
#include <QLabel>

NetworkStatusWidget::NetworkStatusWidget(IconSizeEngine *iconSizeEngine, QWidget *parent)
    : QWidget(parent)
{
    auto *layout = new QHBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 3);

    const int size = iconSizeEngine->relativeWidth(0.55);

    m_connectionLed = new Led(size);
    layout->addWidget(m_connectionLed);
    m_connectionLabel = new QLabel;
    layout->addWidget(m_connectionLabel);

    const QString txHelp = tr("Blinkt auf, wenn Daten gesendet werden");
    m_txLed = new Led(size);
    m_txLed->setToolTip(txHelp);
    layout->addWidget(m_txLed);
    auto *txLabel = new QLabel(tr("Tx"));
    txLabel->setToolTip(txHelp);
    layout->addWidget(txLabel);

    const QString rxHelp = tr("Blinkt auf, wenn Daten empfangen werden");
    m_rxLed = new Led(size);
    m_rxLed->setToolTip(rxHelp);
    layout->addWidget(m_rxLed);
    auto *rxLabel = new QLabel(tr("Rx"));
    rxLabel->setToolTip(rxHelp);
    layout->addWidget(rxLabel);
}

void NetworkStatusWidget::setMode(Mode mode)
{
    switch (mode) {
    case Mode::Server:
        m_connectionLabel->setText(tr("Server gestoppt"));
        break;
    case Mode::Client:
        m_connectionLabel->setText(tr("Nicht verbunden"));
        break;
    }

    m_connectionLed->setState(Led::Red);
}

void NetworkStatusWidget::clientCountChanged(int count)
{
    m_connectionLabel->setText(tr("%n Client(s) angemeldet", "", count));
    m_connectionLed->setState(count != 0 ? Led::Green : Led::Off);
}

void NetworkStatusWidget::setConnected(bool state)
{
    m_connectionLed->setState(state ? Led::Green : Led::Red);
    m_connectionLabel->setText(state ? tr("Verbunden") : tr("Nicht verbunden"));
}

void NetworkStatusWidget::tx()
{
    m_txLed->flash();
}

void NetworkStatusWidget::rx()
{
    m_rxLed->flash();
}
