//* SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "SocketAddressWidget.h"

// Qt includes
#include <QGridLayout>
#include <QNetworkInterface>
#include <QLabel>
#include <QComboBox>
#include <QLineEdit>
#include <QIntValidator>
#include <QToolTip>
#include <QTimer>

SocketAddressWidget::SocketAddressWidget(Mode mode, int defaultPort, bool preferIpv6,
                                         QWidget *parent)
    : QWidget(parent)
{
    setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);

    QGridLayout *layout = new QGridLayout(this);
    layout->addWidget(new QLabel(tr("IP-Adresse:")), 0, 0);

    m_ip = new QComboBox;
    m_ip->setEditable(true);
    m_ip->setInsertPolicy(QComboBox::NoInsert);

    layout->addWidget(m_ip, 0, 1);

    const QList<QHostAddress> allAddresses = QNetworkInterface::allAddresses();

    QVector<const QHostAddress *> ipv4Addresses;
    QVector<const QHostAddress *> ipv6GlobalAddresses;
    QVector<const QHostAddress *> ipv6LocalAddresses;

    for (const QHostAddress &address : allAddresses) {
        if (address.protocol() == QAbstractSocket::IPv4Protocol
            && address != QHostAddress::LocalHost) {

            ipv4Addresses.append(&address);
        } else if (address.protocol() == QAbstractSocket::IPv6Protocol
                   && address != QHostAddress::LocalHostIPv6) {

            const QString stringAddress = address.toString();
            if (stringAddress.startsWith(QStringLiteral("fe80"))
                || stringAddress.startsWith(QStringLiteral("fd00"))
                || stringAddress.startsWith(QStringLiteral("fc00"))) {

                ipv6LocalAddresses.append(&address);
            } else {
                ipv6GlobalAddresses.append(&address);
            }
        }
    }

    const auto addresses
        = preferIpv6 ? QVector<const QVector<const QHostAddress *> *> { &ipv6GlobalAddresses,
                                                                        &ipv6LocalAddresses,
                                                                        &ipv4Addresses }
                     : QVector<const QVector<const QHostAddress *> *> { &ipv4Addresses,
                                                                        &ipv6GlobalAddresses,
                                                                        &ipv6LocalAddresses };

    for (const QVector<const QHostAddress *> *list : addresses) {
        for (const QHostAddress *address : (*list)) {
            if (mode == Mode::Server) {
                m_ip->addItem((*address).toString());
            }

            if (m_hostAddress.isNull()) {
                m_hostAddress = (*address);
                if (mode == Mode::Client) {
                    m_ip->addItem((*address).toString());
                }
            }
        }
    }

    if (m_hostAddress.isNull()) {
        if (preferIpv6) {
            m_hostAddress = QHostAddress::LocalHostIPv6;
        } else {
            m_hostAddress = QHostAddress::LocalHost;
        }
    }

    m_ip->setCurrentText(m_hostAddress.toString());

    layout->addWidget(new QLabel(tr("Port:")), 1, 0);
    m_port = new QLineEdit(QString::number(defaultPort));
    m_port->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Maximum);
    m_port->setValidator(new QIntValidator(1024, 65535, this));
    layout->addWidget(m_port, 1, 1);

    connect(m_ip, QOverload<int>::of(&QComboBox::currentIndexChanged),
            this, &SocketAddressWidget::checkSettings);
    connect(m_ip, &QComboBox::editTextChanged, this, &SocketAddressWidget::checkSettings);
    connect(m_ip->lineEdit(), &QLineEdit::returnPressed, this, &SocketAddressWidget::returnPressed);

    connect(m_port, &QLineEdit::textEdited, this, &SocketAddressWidget::checkSettings);
    connect(m_port, &QLineEdit::returnPressed, this, &SocketAddressWidget::returnPressed);

    QTimer::singleShot(0, this, [this] { m_ip->lineEdit()->deselect(); });
}

void SocketAddressWidget::checkSettings()
{
    Q_EMIT settingsOkay(m_hostAddress.setAddress(m_ip->currentText())
                        && m_port->hasAcceptableInput());

    if (m_ip->currentText().isEmpty() || m_hostAddress.isNull()) {
        QToolTip::hideText();
    } else {
        if (m_hostAddress.toString() != m_ip->currentText()) {
            QToolTip::showText(mapToGlobal(m_ip->geometry().bottomLeft()),
                                m_hostAddress.toString());
        } else {
            QToolTip::hideText();
        }
    }
}

QString SocketAddressWidget::ip() const
{
    return m_hostAddress.toString();
}

int SocketAddressWidget::port() const
{
   return m_port->text().toInt();
}

void SocketAddressWidget::setSocketAddress(const QString &ip, int port)
{
    m_ip->setCurrentText(ip);
    m_port->setText(QString::number(port));
    checkSettings();
}

void SocketAddressWidget::focusIp()
{
    QTimer::singleShot(0, this, [this]
    {
        m_ip->setFocus();
    });
}
