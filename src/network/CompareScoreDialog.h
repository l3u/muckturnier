// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef COMPARESCOREDIALOG_H
#define COMPARESCOREDIALOG_H

// Local includes
#include "shared/TitleDialog.h"
#include "shared/Scores.h"

// Local classes

class SharedObjects;
class TournamentSettings;
class StringTools;
class Client;
class Server;

namespace Scores
{
struct Score;
}

// Qt classes
class QTimer;
class QLabel;
class QTableWidget;
class QPushButton;
class QTableWidget;

class CompareScoreDialog : public TitleDialog
{
    Q_OBJECT

public:
    explicit CompareScoreDialog(QWidget *parent, SharedObjects *SharedObjects, Client *client,
                                int remoteId, const int *currentRound,
                                const Scores::Score *localScore);
    explicit CompareScoreDialog(QWidget *parent, SharedObjects *SharedObjects, Server *server,
                                int remoteId, const int *currentRound,
                                const Scores::Score *localScore);
    int remoteId() const;

Q_SIGNALS:
    void scoreRequested(int round);

public Q_SLOTS:
    void closeDialog(const QString &message);
    void remoteScoreChecksumChanged(int remoteId, int round);
    void dataChanged();
    void compareScore(const Scores::Score &remoteScore);
    void clientDisconnected(int id);

private Q_SLOTS:
    void requestScore();

private: // Functions
    void initializeDialog();
    void resizeToTableSize();
    QVector<QString> createNormalizedPairsList(const Scores::Score &score) const;
    QString pairName(const Scores::Score &score, int index) const;

private: // Variables
    struct Id
    {
        int remote;
        int local;
    };

    TournamentSettings *m_tournamentSettings;
    StringTools *m_stringTools;
    Client *m_client = nullptr;
    Server *m_server = nullptr;
    const int *m_currentRound;
    const Scores::Score *m_localScore;
    const int m_remoteId;
    bool m_isClient = false;
    bool m_isServer = false;

    QTimer *m_requestTimer;

    QPushButton *m_request;
    QPushButton *m_close;
    QLabel *m_requesting;
    QLabel *m_differencesLabel;
    QLabel *m_noDifferencesLabel;
    QTableWidget *m_differences;
    QLabel *m_dataChangedLabel;

};

#endif // COMPARESCOREDIALOG_H
