// SPDX-FileCopyrightText: 2018-2020 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "LogDisplay.h"

// Qt includes
#include <QTime>
#include <QScrollBar>

LogDisplay::LogDisplay(QWidget *parent) : QPlainTextEdit(parent)
{
    setReadOnly(true);
}

QString LogDisplay::currentTime()
{
    return QTime::currentTime().toString(QStringLiteral("hh:mm:ss"));
}

void LogDisplay::displayMessage(const QString &text)
{
    appendPlainText(tr("[%1] %2").arg(currentTime(), text));
    scrollToBottom();
}

void LogDisplay::displayProtocolError(const QString &text)
{
    appendPlainText(tr("[%1] Protokollfehler: %2 – Paket wird ignoriert").arg(currentTime(), text));
    scrollToBottom();
}

void LogDisplay::scrollToBottom()
{
    verticalScrollBar()->setValue(verticalScrollBar()->maximum());
}
