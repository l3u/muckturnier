// SPDX-FileCopyrightText: 2019-2020 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef DISCOVER_PROTOCOL_H
#define DISCOVER_PROTOCOL_H

#include <QJsonObject>

namespace DiscoverProtocol
{

constexpr int protocolVersion = 1;

const QLatin1String action            ("action");
const QLatin1String protocolVersionKey("protocol version");
const QLatin1String ip                ("ip");
const QLatin1String port              ("port");

const QLatin1String searchingServer ("searching muckturnier server");
const QLatin1String announcingServer("announcing muckturnier server");

const QJsonObject searchingServerMessage {
    { action,             searchingServer },
    { protocolVersionKey, protocolVersion }
};

}

#endif // DISCOVER_PROTOCOL_H
