// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "AbstractDiscoverEngine.h"

// Qt includes
#include <QDebug>
#include <QUdpSocket>
#include <QJsonObject>
#include <QNetworkDatagram>

AbstractDiscoverEngine::AbstractDiscoverEngine(QObject *parent, int port)
    : AbstractJsonEngine(parent), m_port(port)
{
    m_socket = new QUdpSocket(this);
    connect(m_socket, &QAbstractSocket::readyRead, this, &AbstractDiscoverEngine::readDatagram);
}

bool AbstractDiscoverEngine::listen()
{
    return m_socket->bind(m_port, QUdpSocket::ShareAddress);
}

QString AbstractDiscoverEngine::errorString()
{
    return m_socket->errorString();
}

void AbstractDiscoverEngine::close()
{
    m_socket->abort();
}

void AbstractDiscoverEngine::readDatagram()
{
    while (m_socket->state() == QAbstractSocket::BoundState && m_socket->hasPendingDatagrams()) {
        const auto datagram = m_socket->receiveDatagram();
        const auto senderAddress = datagram.senderAddress();
        const auto [ jsonObject, success ] = jsonObjectFromByteArray(datagram.data());
        if (! success) {
            Q_EMIT invalidData(senderAddress);
            continue;
        }

        processData(jsonObject, senderAddress);
    }
}

void AbstractDiscoverEngine::broadcastData(const QJsonObject &jsonObject)
{
    m_socket->writeDatagram(jsonObjectToByteArray(jsonObject), QHostAddress::Broadcast, m_port);
}

void AbstractDiscoverEngine::sendData(const QJsonObject &jsonObject, const QHostAddress &address)
{
    m_socket->writeDatagram(jsonObjectToByteArray(jsonObject), address, m_port);
}

int AbstractDiscoverEngine::port() const
{
    return m_port;
}
