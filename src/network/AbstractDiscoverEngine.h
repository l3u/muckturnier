// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef ABSTRACTDISCOVERENGINE_H
#define ABSTRACTDISCOVERENGINE_H

// Local includes
#include "AbstractJsonEngine.h"

// Qt includes
#include <QHostAddress>

// Qt classes
class QUdpSocket;
class QJsonObject;

class AbstractDiscoverEngine : public AbstractJsonEngine
{
    Q_OBJECT

public:
    explicit AbstractDiscoverEngine(QObject *parent, int port);
    bool listen();
    QString errorString();
    void close();
    int port() const;

Q_SIGNALS:
    void invalidData(const QHostAddress &address);

protected: // Functions
    virtual void processData(const QJsonObject &jsonObject, const QHostAddress &senderAddress) = 0;
    void broadcastData(const QJsonObject &jsonObject);
    void sendData(const QJsonObject &jsonObject, const QHostAddress &address);

private Q_SLOTS:
    void readDatagram();

private: // Variables
    QUdpSocket *m_socket;
    const int m_port;

};

#endif // ABSTRACTDISCOVERENGINE_H
