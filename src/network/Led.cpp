// SPDX-FileCopyrightText: 2020 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "Led.h"

// Qt includes
#include <QTimer>
#include <QFontMetrics>
#include <QDebug>

// C++ includes
#include <functional>

static const QString s_css = QStringLiteral("border: 1 solid rgb(160, 160, 160); "
                                            "border-radius: %1; "
                                            "background-color: %2;");

Led::Led(int size, QWidget *parent) : QLabel(parent)
{
    // Make the size even
    size = size * 2 / 2;

    const QString borderRadius = QString::number(size / 2);

    m_css = { { Led::State::Off,
                s_css.arg(borderRadius, QStringLiteral("rgb(220, 220, 220)")) },
              { Led::State::Red,
                s_css.arg(borderRadius, QStringLiteral("rgb(230, 10, 10)")) },
              { Led::State::Green,
                s_css.arg(borderRadius, QStringLiteral("rgb(10, 230, 10)")) } };

    setFixedSize(size, size);
    setState(State::Off);

    m_flashTimer = new QTimer(this);
    m_flashTimer->setInterval(250);
    m_flashTimer->setSingleShot(true);
    connect(m_flashTimer, &QTimer::timeout, this, std::bind(&Led::setState, this, State::Off));
}

void Led::setState(State state)
{
    setStyleSheet(m_css.value(state));
}

void Led::flash()
{
    setStyleSheet(m_css.value(State::Green));
    m_flashTimer->start();
}
