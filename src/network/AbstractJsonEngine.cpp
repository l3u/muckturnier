// SPDX-FileCopyrightText: 2019-2020 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "AbstractJsonEngine.h"

// Qt includes
#include <QJsonDocument>

AbstractJsonEngine::AbstractJsonEngine(QObject *parent) : QObject(parent)
{
}

QByteArray AbstractJsonEngine::jsonObjectToByteArray(const QJsonObject &jsonObject) const
{
    return QJsonDocument(jsonObject).toJson(QJsonDocument::Compact);
}

TSuccess<QJsonObject> AbstractJsonEngine::jsonObjectFromByteArray(const QByteArray &byteArray) const
{
    QJsonParseError error;
    const QJsonDocument json = QJsonDocument::fromJson(byteArray, &error);
    if (error.error != QJsonParseError::NoError || ! json.isObject()) {
        return { QJsonObject(), false };
    }
    return { json.object(), true };
}
