// SPDX-FileCopyrightText: 2022-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "ScoreHelper.h"
#include "ServerProtocol.h"

// Qt includes
#include <QJsonObject>
#include <QJsonArray>

namespace sp = ServerProtocol;

namespace ScoreHelper
{

QJsonObject scoreToJson(const Scores::Score &score)
{
    QJsonArray tables;
    for (const auto table : score.tables) {
        tables.append(table);
    }

    QJsonArray pairsOrPlayers1;
    for (const auto &data : score.pairsOrPlayers1) {
        pairsOrPlayers1.append(dataToJson(data));
    }

    QJsonArray players2;
    for (const auto &data : score.players2) {
        players2.append(dataToJson(data));
    }

    QJsonArray scores;
    for (const auto &pairResults : score.scores) {
        QJsonArray singleScore;
        for (const auto singleResult : pairResults) {
            singleScore.append(singleResult);
        }
        scores.append(singleScore);
    }

    return QJsonObject { { sp::action,                   sp::scoreRequest },
                         { sp::scoreTablesList,          tables },
                         { sp::scorePairsOrPlayers1List, pairsOrPlayers1 },
                         { sp::scorePlayers2List,        players2 },
                         { sp::scoresList,               scores } };
}

QJsonObject dataToJson(const Players::DisplayData &data)
{
    return QJsonObject { { sp::playersName,   data.name },
                         { sp::playersNumber, data.number },
                         { sp::disqualified,  data.disqualified } };
}

Scores::Score jsonToScore(const QJsonObject &json)
{
    Scores::Score score;

    const auto jsonTables = json.value(sp::scoreTablesList).toArray();
    for (const auto &table : jsonTables) {
        score.tables.append(table.toInt());
    }

    const auto jsonPairsOrPlayers1 = json.value(sp::scorePairsOrPlayers1List).toArray();
    for (const auto &jsonValue : jsonPairsOrPlayers1) {
        score.pairsOrPlayers1.append(jsonToData(jsonValue));
    }

    const auto jsonPlayers2 = json.value(sp::scorePlayers2List).toArray();
    for (const auto &jsonValue : jsonPlayers2) {
        score.players2.append(jsonToData(jsonValue));
    }

    const auto jsonScores = json.value(sp::scoresList).toArray();
    for (const auto &jsonPairResult : jsonScores) {
        const auto jsonPairResultArray = jsonPairResult.toArray();
        QVector<int> pairResult;
        for (const auto &result : jsonPairResultArray) {
            pairResult.append(result.toInt());
        }
        score.scores.append(pairResult);
    }

    return score;
}

Players::DisplayData jsonToData(const QJsonValue &value)
{
    const auto data = value.toObject();
    return {
        data.value(sp::playersName).toString(), // DisplayData.name
        data.value(sp::playersNumber).toInt(),  //            .number
        data.value(sp::disqualified).toBool(),  //            .disqualified
        -1                                      //            .id is not set
    };
}

}
