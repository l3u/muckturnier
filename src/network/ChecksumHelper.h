// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef CHECKSUMHELPER_H
#define CHECKSUMHELPER_H

// Local includes
#include "shared/Markers.h"
#include "shared/Players.h"
#include "shared/Draw.h"

// Qt includes
#include <QObject>
#include <QString>
#include <QMap>

class ChecksumHelper : public QObject
{
    Q_OBJECT

public:
    explicit ChecksumHelper(QObject *parent = nullptr);

    void addData(const QString &data);
    void addData(int data);

    void addData(const Markers::Marker &marker);
    void addData(const Players::Data &data);
    void addData(const QMap<int, Draw::Seat> &draw);

    QString checksum() const;
    void clear();

private: // Variables
    QString m_data;

};

#endif // CHECKSUMHELPER_H
