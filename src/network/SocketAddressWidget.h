// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SOCKETADDRESSWIDGET_H
#define SOCKETADDRESSWIDGET_H

// Qt includes
#include <QWidget>
#include <QHostAddress>

// Qt classes
class QComboBox;
class QLineEdit;

class SocketAddressWidget : public QWidget
{
    Q_OBJECT

public:
    enum Mode {
        Server,
        Client
    };

    explicit SocketAddressWidget(Mode mode, int defaultPort, bool preferIpv6,
                                 QWidget *parent = nullptr);
    QString ip() const;
    int port() const;
    void setSocketAddress(const QString &ip, int port);
    void focusIp();

Q_SIGNALS:
    void settingsOkay(bool state);
    void returnPressed();

private Q_SLOTS:
    void checkSettings();

private: // Variables
    QComboBox *m_ip;
    QLineEdit *m_port;
    QHostAddress m_hostAddress;

};

#endif // SOCKETADDRESSWIDGET_H
