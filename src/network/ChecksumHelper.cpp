// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "ChecksumHelper.h"

#include "shared/ColorHelper.h"

// Qt includes
#include <QCryptographicHash>
#include <QDebug>

static const QChar s_space = QLatin1Char(' ');

ChecksumHelper::ChecksumHelper(QObject *parent) : QObject(parent)
{
}

void ChecksumHelper::addData(const QString &data)
{
    m_data.append(data);
    m_data.append(s_space);
}

void ChecksumHelper::addData(int data)
{
    m_data.append(QString::number(data));
    m_data.append(s_space);
}

void ChecksumHelper::addData(const Markers::Marker &marker)
{
    addData(marker.id);

    // Name and color of the "unmarked" marker depend on the local settings.
    // Thus, we have to handle this one in a special way
    if (marker.id != 0) {
        addData(marker.name);
        addData(ColorHelper::colorToString(marker.color));
    } else {
        addData(QString());
        addData(ColorHelper::colorToString(QColor(0, 0, 0)));
    }

    addData(marker.sorting);
}

void ChecksumHelper::addData(const Players::Data &data)
{
    addData(data.name);
    addData(data.number);
    addData(data.marker);
    addData(data.draw);
    addData(data.disqualified);
    addData(data.bookingChecksum);
}

void ChecksumHelper::addData(const QMap<int, Draw::Seat> &draw)
{
    QMap<int, Draw::Seat>::const_iterator it;
    for (it = draw.constBegin(); it != draw.constEnd(); it++) {
        addData(it.key());
        const auto &seat = it.value();
        addData(seat.table);
        addData(seat.pair);
        addData(seat.player);
    }
}

QString ChecksumHelper::checksum() const
{
    QCryptographicHash hash(QCryptographicHash::Md5);
    hash.addData(m_data.toUtf8());
    return QLatin1String(hash.result().toHex());
}

void ChecksumHelper::clear()
{
    m_data.clear();
}
