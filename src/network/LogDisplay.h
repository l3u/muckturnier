// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef LOGDISPLAY_H
#define LOGDISPLAY_H

// Qt includes
#include <QPlainTextEdit>

class LogDisplay : public QPlainTextEdit
{
    Q_OBJECT

public:
    explicit LogDisplay(QWidget *parent = nullptr);

public Q_SLOTS:
    void displayMessage(const QString &text);
    void displayProtocolError(const QString &text);

private: // Functions
    QString currentTime();
    void scrollToBottom();

};

#endif // LOGDISPLAY_H
