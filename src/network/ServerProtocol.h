// SPDX-FileCopyrightText: 2018-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SERVER_PROTOCOL_H
#define SERVER_PROTOCOL_H

#include <QJsonObject>
#include <QDataStream>

namespace ServerProtocol
{

// Basic protocol data

// At least up to Qt_5_15, the QDataStream version should not affect the way we serialize data here
// at all, because we only add QByteArray representations of JSON string data (zero-terminated
// const char *). Adding those to the stream hasn't changed since Qt 1.0: They will be added as the
// length represented by a 32 bit unsigned int along with the zero-terminated data itself.
//
// So we should be safe here, although we already have had different settings for streamVersion
// (and we shouldn't ;-)
//
// NOTICE: Just to leave this here literally: Don't change this unless there's a good reason to do
// so. Backwards compatibility might be lost if the way a QByteArray is added to a QDataStream ever
// changes.
const QDataStream::Version streamVersion = QDataStream::Qt_5_15;

constexpr int protocolVersion = 8;
const QLatin1String serverSignature("muckturnier server");

// Keys used in all requests and answers
const QLatin1String action  ("action");
const QLatin1String clientId("client id");
const QLatin1String checksum("checksum");

// Invalid request handling

const QLatin1String actionRejected("request rejected");
const QLatin1String rejectReason  ("reject reason");

enum RejectReason {
    RejectReasonOutdatedRequest,
    RejectReasonUnknownAction
};

// Data types
// ==========

// Markers
// -------

const QLatin1String markerName     ("marker name");
const QLatin1String markerId       ("marker id");
const QLatin1String markerColor    ("marker color");
const QLatin1String markerSorting  ("marker sorting");
const QLatin1String newMarkerName  ("new marker name");
const QLatin1String markerSequence ("marker sequence");
const QLatin1String markerDirection("marker direction");
const QLatin1String bookedMarker   ("booked marker");

// Players
// -------

// Players data
const QLatin1String playersName    ("players name");
const QLatin1String newPlayersName ("new players name");
const QLatin1String playersNumber  ("players number");
const QLatin1String disqualified   ("disqualified");
const QLatin1String bookingChecksum("booking checksum");

// Draw data
const QLatin1String playersDraw("players draw");
const QLatin1String drawnTable ("drawn table");
const QLatin1String drawnPair  ("drawn pair");
const QLatin1String drawnPlayer("drawn player");

// Booking settings
// ----------------

const QLatin1String bookingSettings("booking settings");

// Data lists
// ----------

// Shared keys
const QLatin1String playersList("players list");
const QLatin1String markersList("markers list");

// Draw lists
const QLatin1String drawnTablesList ("drawn tables list");
const QLatin1String drawnPairsList  ("drawn pairs list");
const QLatin1String drawnPlayersList("drawn players list");

// Score comparison lists
const QLatin1String scoreTablesList         ("score tables list");
const QLatin1String scorePairsOrPlayers1List("score pairs or players 1 list");
const QLatin1String scorePlayers2List       ("score players 2 list");
const QLatin1String scoresList              ("scores list");
const QLatin1String scoreChecksumsList      ("score checksums list");

// Other (request specific) keys
// -----------------------------

const QLatin1String singlesManagement("singles management");
const QLatin1String round            ("round");
const QLatin1String requestType      ("request type");

// Possible values for the "request type" key
const QLatin1String clientRequest("client request");
const QLatin1String serverRequest("server request");

// Stop watch synchronization
const QLatin1String stopWatchId ("stop watch id");
const QLatin1String syncPossible("sync possible");
const QLatin1String title       ("title");
const QLatin1String hours       ("hours");
const QLatin1String minutes     ("minutes");
const QLatin1String seconds     ("seconds");
const QLatin1String isRunning   ("is running");
const QLatin1String startOffset ("start offset");
const QLatin1String targetTime  ("target time");

// Handshake
// =========

const QLatin1String handshake("handshake");
const QLatin1String phase    ("phase");
const QLatin1String failed   ("failed");

// Welcome message

const QLatin1String initialization    ("initialization");
const QLatin1String serverSignatureKey("server signature");
const QLatin1String protocolVersionKey("protocol version");

const QJsonObject welcomeMessage {
    { action,             handshake },
    { phase,              initialization },
    { serverSignatureKey, serverSignature },
    { protocolVersionKey, protocolVersion }
};

// Synchronization request

const QLatin1String synchronization("synchronization");

const QJsonObject synchronizationRequest {
    { action, handshake },
    { phase,  synchronization }
};

// Synchronization answer

const QLatin1String settings         ("settings");
const QLatin1String locale           ("locale");
const QLatin1String namesSeparator   ("names separator");
const QLatin1String tournamentMode   ("tournament mode");
const QLatin1String boogerScore      ("booger score");
const QLatin1String boogersPerRound  ("boogers per round");
const QLatin1String tournamentStarted("tournament started");

// Synchronization success, transmit score checksums client --> server, request/allocate an id
const QLatin1String idAllocation("id allocation");

// Actions
// =======

// Registration changes

const QLatin1String registerPlayers("register players");
const QLatin1String renamePlayers  ("rename players");
const QLatin1String assemblePair   ("assemble pair");

const QLatin1String deletePlayers      ("delete players");
const QLatin1String deleteMarkedPlayers("delete marked players");
const QLatin1String deleteAllPlayers   ("delete all players");

const QLatin1String setMarker       ("set marker");
const QLatin1String setListMarker   ("set list marker");
const QLatin1String removeListMarker("remove list marker");

const QLatin1String setDisqualified("set disqualified");

const QLatin1String setNumber("set number");

const QLatin1String setBookingChecksum("set booking checksum");

// Draw changes
const QLatin1String setDraw       ("set draw");
const QLatin1String setListDraw   ("set list draw");
const QLatin1String deleteAllDraws("delete all draws");

// Markers changes
const QLatin1String addMarker   ("add marker");
const QLatin1String editMarker  ("edit marker");
const QLatin1String deleteMarker("delete marker");
const QLatin1String moveMarker  ("move marker");

// Special markers definitions
const QLatin1String setBookedMarker        ("set booked marker");
const QLatin1String singlesManagementChange("singles management change");

// Finish registration
const QLatin1String finishRegistration("finish registration");

// Score comparison
const QLatin1String scoreChecksumChange ("score checksum change");
const QLatin1String scoreRequest        ("score request");
const QLatin1String scoreChecksum       ("score checksum");

// Stop watch synchronization
const QLatin1String stopWatchSynchronization("stop watch synchronization");

}

#endif // SERVER_PROTOCOL_H
