// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "CompareScoreDialog.h"

#include "ClientPage/Client.h"
#include "ServerPage/Server.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/StringTools.h"
#include "SharedObjects/TournamentSettings.h"
#include "SharedObjects/Settings.h"

#include "shared/SharedStyles.h"
#include "shared/TableDelegate.h"

// Qt includes
#include <QDebug>
#include <QApplication>
#include <QMessageBox>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QLabel>
#include <QTimer>
#include <QSet>
#include <QTableWidget>
#include <QHeaderView>
#include <QScrollBar>
#include <QMultiMap>

// C++ includes
#include <utility>
#include <algorithm>
#include <functional>

CompareScoreDialog::CompareScoreDialog(QWidget *parent, SharedObjects *sharedObjects,
                                       Client *client, int remoteId, const int *currentRound,
                                       const Scores::Score *localScore)
    : TitleDialog(parent),
      m_tournamentSettings(sharedObjects->tournamentSettings()),
      m_stringTools(sharedObjects->stringTools()),
      m_client(client),
      m_currentRound(currentRound),
      m_localScore(localScore),
      m_remoteId(remoteId)
{
    m_isClient = true;
    initializeDialog();
}

CompareScoreDialog::CompareScoreDialog(QWidget *parent, SharedObjects *sharedObjects,
                                       Server *server, int remoteId, const int *currentRound,
                                       const Scores::Score *localScore)
    : TitleDialog(parent),
      m_tournamentSettings(sharedObjects->tournamentSettings()),
      m_stringTools(sharedObjects->stringTools()),
      m_server(server),
      m_currentRound(currentRound),
      m_localScore(localScore),
      m_remoteId(remoteId)
{
    m_isServer = true;

    m_requestTimer = new QTimer(this);
    m_requestTimer->setInterval(sharedObjects->settings()->requestTimeout());
    m_requestTimer->setSingleShot(true);
    connect(m_requestTimer, &QTimer::timeout,
            this, std::bind(&CompareScoreDialog::closeDialog, this,
                            tr("Client #%1 hat zu lange keine Antwort geschickt.\n"
                               "Der Dialog wird jetzt geschlossen.").arg(m_remoteId)));

    initializeDialog();
}

void CompareScoreDialog::initializeDialog()
{
    // This title is shown as long as request lasts, and possibly as the title of an error message
    setTitle(tr("Ergebnisse vergleichen"));

    auto box = new QGroupBox;
    auto *boxLayout = new QVBoxLayout(box);
    layout()->addWidget(box);

    m_requesting = new QLabel(tr("<i>Anfrage läuft …</i>"));
    m_requesting->setAlignment(Qt::AlignCenter);
    m_requesting->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);
    boxLayout->addWidget(m_requesting);

    m_differencesLabel = new QLabel(m_isClient
        ? tr("<span style=\"%1\">Bei folgenden Paaren gibt es Unterschiede zwischen dem Datensatz "
             "des Servers und dem Lokalen:</span>").arg(
             SharedStyles::redText)
        : tr("<span style=\"%1\">Bei folgenden Paaren gibt es Unterschiede zwischen dem Datensatz "
             "von Client #%2 und dem Lokalen:</span>").arg(
             SharedStyles::redText, QString::number(m_remoteId))
    );
    m_differencesLabel->setWordWrap(true);
    boxLayout->addWidget(m_differencesLabel);

    m_noDifferencesLabel = new QLabel(m_isClient
        ? tr("<span style=\"%1\">Die Ergebnisse des Servers sind identisch mit den lokal "
             "eingegebenen.</span>").arg(
             SharedStyles::greenText)
        : tr("<span style=\"%1\">Die Ergebnisse von Client #%2 sind identisch mit den lokal "
             "eingegebenen.</span>").arg(
             SharedStyles::greenText, QString::number(m_remoteId))
    );
    m_noDifferencesLabel->setWordWrap(true);
    boxLayout->addWidget(m_noDifferencesLabel);

    m_differences = new QTableWidget;
    m_differences->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_differences->setItemDelegate(new TableDelegate(this));
    m_differences->verticalHeader()->hide();
    m_differences->setShowGrid(false);
    m_differences->setWordWrap(false);
    m_differences->setSelectionMode(QTableWidget::NoSelection);
    m_differences->setFocusPolicy(Qt::NoFocus);
    m_differences->setContextMenuPolicy(Qt::CustomContextMenu);
    boxLayout->addWidget(m_differences);

    m_differences->setColumnCount(5);
    if (m_isClient) {
        m_differences->setHorizontalHeaderLabels({ tr("Tisch"), tr("Paar"), tr("Bobbl"),
                                                   tr("Server"), tr("Lokal") });
    } else {
        m_differences->setHorizontalHeaderLabels({ tr("Tisch"), tr("Paar"), tr("Bobbl"),
                                                   tr("Client #%1").arg(m_remoteId), tr("Lokal") });
    }

    m_dataChangedLabel = new QLabel(tr(
        "<span style=\"%1\">Seit dem letzten Abruf wurde entweder eine andere Runde geöffnet, "
        "oder der Datenbestand hat sich lokal oder entfernt geändert. Bitte die Ergebnisse erneut "
        "abfragen!</span>").arg(SharedStyles::redText));
    m_dataChangedLabel->setWordWrap(true);
    boxLayout->addWidget(m_dataChangedLabel);

    addButtonBox();

    m_request = buttonBox()->addButton(QDialogButtonBox::Yes);
    m_request->setText(tr("Ergebnisse abfragen"));
    connect(m_request, &QPushButton::clicked, this, &CompareScoreDialog::requestScore);

    m_close = buttonBox()->addButton(QDialogButtonBox::Close);
    connect(m_close, &QPushButton::clicked, this, &QDialog::reject);

    QTimer::singleShot(0, this, &CompareScoreDialog::requestScore);
}

void CompareScoreDialog::closeDialog(const QString &message)
{
    QApplication::restoreOverrideCursor();
    QMessageBox::warning(this, windowTitle(), message);
    QDialog::reject();
}

void CompareScoreDialog::remoteScoreChecksumChanged(int remoteId, int round)
{
    if (remoteId == m_remoteId && round == (*m_currentRound)) {
        dataChanged();
    }
}

void CompareScoreDialog::dataChanged()
{
    m_dataChangedLabel->show();
    m_request->setEnabled(true);
    m_request->setFocus();

    resizeToContents();
    if (m_differences->isVisible()) {
        resizeToTableSize();
    }
}

void CompareScoreDialog::requestScore()
{
    QApplication::setOverrideCursor(Qt::WaitCursor);
    setEnabled(false);

    m_request->setEnabled(false);

    m_requesting->show();
    m_differencesLabel->hide();
    m_noDifferencesLabel->hide();
    m_differences->hide();
    m_dataChangedLabel->hide();

    Q_EMIT scoreRequested(*m_currentRound);

    if (m_isServer) {
        m_requestTimer->start();
    }
}

QVector<QString> CompareScoreDialog::createNormalizedPairsList(const Scores::Score &score) const
{
    const bool isPairMode = m_tournamentSettings->isPairMode();

    QVector<QString> list;
    for (int i = 0; i < score.pairsOrPlayers1.count(); i++) {
        if (isPairMode) {
            list.append(score.pairsOrPlayers1.at(i).name);
        } else {
            list.append(m_stringTools->normalizedPairName(score.pairsOrPlayers1.at(i),
                                                          score.players2.at(i)));
        }
    }

    return list;
}

QString CompareScoreDialog::pairName(const Scores::Score &score, int index) const
{
    return m_tournamentSettings->isPairMode()
        ? m_stringTools->pairOrPlayerName(score.pairsOrPlayers1.at(index),
                                          StringTools::DisqualifiedName)
        : m_stringTools->pairName(score.pairsOrPlayers1.at(index),
                                  score.players2.at(index),
                                  StringTools::DisqualifiedName);
}

void CompareScoreDialog::compareScore(const Scores::Score &remoteScore)
{
    if (m_isServer) {
        m_requestTimer->stop();
    }

    QApplication::restoreOverrideCursor();
    setEnabled(true);

    m_dataChangedLabel->hide();
    m_requesting->hide();

    setTitle(tr("Vergleich der Ergebnisse für Runde %1").arg(*m_currentRound));

    // Generate a sorted list of all pair names.
    //
    // We use "normalized" pair names  here, so that pair names are guaranteed to be comparable for
    // single player tournaments (the pairs "John / Ben" and "Ben / John" are the same ones in this
    // case and could be entered both on a network).
    //
    // The list index is the same for the registered and the normalized pair name.

    const auto localNormalizedPairs = createNormalizedPairsList(*m_localScore);
    const auto remoteNormalizedPairs = createNormalizedPairsList(remoteScore);

    QSet<QString> allPairsSet;
    for (const auto &pair : localNormalizedPairs) {
        allPairsSet.insert(pair);
    }
    for (const auto &pair : remoteNormalizedPairs) {
        allPairsSet.insert(pair);
    }

    QVector<QString> allPairs(allPairsSet.begin(), allPairsSet.end());
    m_stringTools->sort(allPairs);

    // Find differences

    QVector<Id> differing;
    QVector<int> missingRemotely;
    QVector<int> missingLocally;

    for (const auto &pair : allPairs) {
        const int localId  = localNormalizedPairs.contains(pair)
                                 ? localNormalizedPairs.indexOf(pair)  : -1;
        const int remoteId = remoteNormalizedPairs.contains(pair)
                                 ? remoteNormalizedPairs.indexOf(pair) : -1;

        if (remoteId != -1 && localId != -1) {
            if (remoteScore.scores.at(remoteId) != m_localScore->scores.at(localId)
                || remoteScore.tables.at(remoteId / 2) != m_localScore->tables.at(localId / 2)) {

                differing.append({ remoteId, localId });
            }

        } else if (remoteId == -1) {
            missingRemotely.append(localId);

        } else if (localId == -1) {
            missingLocally.append(remoteId);
        }
    }

    // Display the differnces

    m_differences->show();
    m_differences->clearContents();
    m_differences->setRowCount(differing.count() + missingRemotely.count()
                               + missingLocally.count());

    int row = 0;

    // Differing results

    QMultiMap<int, Id> differingSorted;
    for (const auto &id : differing) {
        differingSorted.insert(m_localScore->tables.at(id.local / 2), id);
    }

    QMultiMap<int, Id>::const_iterator it1;
    for (it1 = differingSorted.constBegin(); it1 != differingSorted.constEnd(); it1++) {
        const auto &id = it1.value();
        const int localTable = it1.key();
        const int remoteTable = remoteScore.tables.at(id.remote / 2);

        m_differences->setItem(row, 0, new QTableWidgetItem(localTable == remoteTable
            ? QString::number(localTable)
            : tr("Lokal: %1\nServer: %2").arg(QString::number(localTable),
                                              QString::number(remoteTable))));

        QString localPairName = pairName(*m_localScore, id.local);
        if (localPairName != pairName(remoteScore, id.remote)) {
            if (m_isClient) {
                localPairName.append(tr("\n(Die Spielernamen sind beim Server vertauscht!)"));
            } else {
                localPairName.append(tr("\n(Die Spielernamen sind bei Client #%1 vertauscht!)").arg(
                                        m_remoteId));
            }
        }
        m_differences->setItem(row, 1, new QTableWidgetItem(localPairName));

        QString boogers;
        QString remote;
        QString local;
        const auto &remoteResult = remoteScore.scores.at(id.remote);
        const auto &localResult = m_localScore->scores.at(id.local);
        for (int booger = 0; booger < remoteResult.count(); booger++) {
            if (remoteResult.at(booger) != localResult.at(booger)) {
                boogers.append(tr("%1.\n").arg(booger + 1));
                remote.append(tr("%1\n").arg(m_stringTools->boogify(remoteResult.at(booger))));
                local.append(tr("%1\n").arg(m_stringTools->boogify(localResult.at(booger))));
            }
        }
        m_differences->setItem(row, 2, new QTableWidgetItem(boogers.trimmed()));
        m_differences->setItem(row, 3, new QTableWidgetItem(remote.trimmed()));
        m_differences->setItem(row, 4, new QTableWidgetItem(local.trimmed()));

        row++;
    }

    // Results missing on the server

    QMultiMap<int, int> remoteMissingTables;
    for (const int id : missingRemotely) {
        remoteMissingTables.insert(m_localScore->tables.at(id / 2), id);
    }

    QMultiMap<int, int>::const_iterator it2;
    for (it2 = remoteMissingTables.constBegin(); it2 != remoteMissingTables.constEnd(); it2++) {
        m_differences->setItem(row, 0, new QTableWidgetItem(tr("Lokal: %1").arg(it2.key())));
        m_differences->setItem(row, 1, new QTableWidgetItem(pairName(*m_localScore, it2.value())));
        m_differences->setItem(row, 3, new QTableWidgetItem(tr("Fehlt")));
        row++;
    }

    // Results missing locally

    QMultiMap<int, int> localMissingTables;
    for (const int id : missingLocally) {
        localMissingTables.insert(remoteScore.tables.at(id / 2), id);
    }

    QMultiMap<int, int>::const_iterator it3;
    for (it3 = localMissingTables.constBegin(); it3 != localMissingTables.constEnd(); it3++) {
        m_differences->setItem(row, 0, new QTableWidgetItem(tr("Server: %1").arg(it3.key())));
        m_differences->setItem(row, 1, new QTableWidgetItem(pairName(remoteScore, it3.value())));
        m_differences->setItem(row, 4, new QTableWidgetItem(tr("Fehlt")));
        row++;
    }

    m_differences->resizeRowsToContents();
    m_differences->resizeColumnsToContents();

    const bool identical = m_differences->rowCount() == 0;
    m_differencesLabel->setVisible(! identical);
    m_noDifferencesLabel->setVisible(identical);
    m_differences->setVisible(! identical);

    if (identical) {
        resizeToContents();
        m_close->setFocus();
    } else {
        resizeToTableSize();
        m_request->setFocus();
    }
}

void CompareScoreDialog::resizeToTableSize()
{
    QApplication::processEvents();
    QTimer::singleShot(0, this, [this]
    {
        const auto *parentWidget = qobject_cast<QWidget *>(parent());
        const auto parentSize = parentWidget->size();

        // FIXME: If anybody ever may know how to get the correct scroll bar
        //        width/height _before_ it's actually shown, tell me :-P
        const int scrollBarWidth = std::min(m_differences->verticalScrollBar()->width(),
                                            m_differences->horizontalScrollBar()->height());

        // Step 1: Adjust the width so that all columns are shown if the parent's
        // width allows this (we don't become wider than the parent)

        const int remainingDialogWidth = width() - m_differences->width();
        const int maximumTableWidth = m_differences->horizontalHeader()->length()
                                      + m_differences->width() - m_differences->viewport()->width()
                                      + scrollBarWidth;

        resize(std::min(parentSize.width(), maximumTableWidth + remainingDialogWidth), height());

        // Step 2: Adjust the width so that all rows are shown if the parent's
        // height allows this (we don't become taller than the parent)

        const int remainingDialogHeight = height() - m_differences->height();

        int tableHeight = 0;
        for (int i = 0; i < m_differences->rowCount(); i++) {
            tableHeight += m_differences->rowHeight(i);
        }
        tableHeight += m_differences->height() - m_differences->viewport()->height();

        resize(width(), std::min(tableHeight + remainingDialogHeight, parentSize.height()));

        // Step 3: (Somehow) Center this dialog on the parent
        move(parentWidget->geometry().center() - rect().center());
    });
}

int CompareScoreDialog::remoteId() const
{
    return m_remoteId;
}

void CompareScoreDialog::clientDisconnected(int id)
{
    if (id == m_remoteId) {
        closeDialog(tr("Client #%1 hat die Verbindung beendet.\n"
                       "Der Dialog wird jetzt geschlossen.").arg(id));
    }
}
