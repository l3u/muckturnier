// SPDX-FileCopyrightText: 2020 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef LED_H
#define LED_H

// Qt includes
#include <QLabel>
#include <QHash>

// Qt classes
class QTimer;

class Led : public QLabel
{
    Q_OBJECT

public:
    enum State {
        Off,
        Red,
        Green
    };

    explicit Led(int size, QWidget *parent = nullptr);
    void setState(State state);
    void flash();

private: // Variables
    QTimer *m_flashTimer;
    QHash<Led::State, QString> m_css;

};

#endif // LED_H
