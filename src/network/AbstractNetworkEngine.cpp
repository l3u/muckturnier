// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "AbstractNetworkEngine.h"
#include "ServerProtocol.h"

// Qt includes
#include <QDebug>
#include <QTcpSocket>
#include <QHostAddress>
#include <QJsonObject>
#include <QDataStream>
#include <QColor>

namespace sp = ServerProtocol;

AbstractNetworkEngine::AbstractNetworkEngine(QObject *parent) : AbstractJsonEngine(parent)
{
    m_socket = new QTcpSocket(this);
    connect(m_socket, &QTcpSocket::readyRead, this, &AbstractNetworkEngine::readData);
}

QTcpSocket *AbstractNetworkEngine::socket() const
{
    return m_socket;
}

QString AbstractNetworkEngine::ip() const
{
    return m_socket->peerAddress().toString();
}

void AbstractNetworkEngine::sendData(const QJsonObject &jsonObject)
{
    QDataStream stream(m_socket);
    stream.setVersion(sp::streamVersion);
    stream << jsonObjectToByteArray(jsonObject);
}

void AbstractNetworkEngine::readData()
{
    QByteArray data;
    QDataStream stream(m_socket);
    stream.setVersion(sp::streamVersion);

    for (;;) {
        // Probably, the connection has been closed in the meantime
        if (! m_socket->isValid()) {
            break;
        }

        // Read data from the socket
        stream.startTransaction();
        stream >> data;

        if (! stream.commitTransaction()) {
            // The data is incomplete, wait for more
            break;
        }

        // Parse the data
        const auto [ jsonObject, success ] = jsonObjectFromByteArray(data);

        if (! success) {
            Q_EMIT protocolError(tr("%1: Fehler beim Parsen der übertragenen Daten").arg(ip()));
            continue;
        }

        const QJsonValue actionValue = jsonObject.value(sp::action);
        if (actionValue.type() != QJsonValue::String) {
            Q_EMIT protocolError(tr("%1: Ungültiges Paket").arg(ip()));
            continue;
        }

        dataReceived(actionValue.toString(), jsonObject);

        // Look for other JSON objects in another loop
    }
}
