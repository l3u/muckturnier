// SPDX-FileCopyrightText: 2022 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef STOPWATCHSYNC_H
#define STOPWATCHSYNC_H

#include <QString>

namespace StopWatchSync
{

struct Data
{
    bool syncPossible = false;
    QString title;
    int hours = 0;
    int minutes = 0;
    int seconds = 0;
    bool isRunning = false;
    int startOffset = 0;
    qint64 targetTime = 0;
};

}

#endif // STOPWATCHSYNC_H
