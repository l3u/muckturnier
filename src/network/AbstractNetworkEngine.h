// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef ABSTRACTNETWORKENGINE_H
#define ABSTRACTNETWORKENGINE_H

// Local includes
#include "AbstractJsonEngine.h"

// Qt classes
class QJsonObject;
class QTcpSocket;

class AbstractNetworkEngine : public AbstractJsonEngine
{
    Q_OBJECT

public:
    void sendData(const QJsonObject &jsonObject);
    QString ip() const;

Q_SIGNALS:
    void protocolError(const QString &text);

protected:
    explicit AbstractNetworkEngine(QObject *parent);

protected Q_SLOTS:
    void readData();

protected: // Functions
    QTcpSocket *socket() const;
    virtual void dataReceived(const QString &action, const QJsonObject &jsonObject) = 0;

private: // Variables
    QTcpSocket *m_socket;

};

#endif // ABSTRACTNETWORKENGINE_H
