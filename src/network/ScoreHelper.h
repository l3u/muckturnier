// SPDX-FileCopyrightText: 2022-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SCOREHELPER_H
#define SCOREHELPER_H

// Local includes
#include "shared/Players.h"
#include "shared/Scores.h"

// Qt classes
class QJsonObject;
class QJsonValue;

namespace ScoreHelper
{

QJsonObject scoreToJson(const Scores::Score &score);
QJsonObject dataToJson(const Players::DisplayData &data);

Scores::Score jsonToScore(const QJsonObject &json);
Players::DisplayData jsonToData(const QJsonValue &value);

}

#endif // SCOREHELPER_H
