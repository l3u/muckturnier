// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "ImportPlayersDialog.h"

#include "Database/Database.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/StringTools.h"
#include "SharedObjects/SearchEngine.h"
#include "SharedObjects/TournamentSettings.h"

#include "shared/DefaultValues.h"
#include "shared/TableDelegate.h"

// Qt includes

#include <QFileDialog>
#include <QMessageBox>
#include <QTimer>
#include <QApplication>
#include <QTextStream>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QLabel>
#include <QTableWidget>
#include <QDebug>
#include <QHeaderView>
#include <QScrollBar>
#include <QCheckBox>
#include <QSplitter>

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
#include <QTextCodec>
#endif

// C++ includes
#include <utility>

ImportPlayersDialog::ImportPlayersDialog(QWidget *parent, SharedObjects *sharedObjects,
                                         bool phoneticSearch)
    : FileDialog(parent, sharedObjects->database()),
      m_db(sharedObjects->database()),
      m_tournamentSettings(sharedObjects->tournamentSettings()),
      m_stringTools(sharedObjects->stringTools()),
      m_searchEngine(sharedObjects->searchEngine()),
      m_phoneticSearch(phoneticSearch)
{
    m_importPlayersListString = m_tournamentSettings->isPairMode() ? tr("Paarliste")
                                                                   : tr("Spielerliste");
    m_title = tr("%1 importieren").arg(m_importPlayersListString),
    setTitle(m_title);

    QSplitter *splitter = new QSplitter(this);
    splitter->setOrientation(Qt::Vertical);
    splitter->setChildrenCollapsible(false);
    layout()->addWidget(splitter);

    QGroupBox *unambiguousNamesBox = new QGroupBox(m_tournamentSettings->isPairMode()
                                                       ? tr("Neue Paare") : tr("Neue Spieler"));
    splitter->addWidget(unambiguousNamesBox);
    QVBoxLayout *importedLayout = new QVBoxLayout(unambiguousNamesBox);

    m_unambiguousNamesLabel = new QLabel;
    m_unambiguousNamesLabel->setWordWrap(true);
    importedLayout->addWidget(m_unambiguousNamesLabel);

    m_unambiguousNames = new QTableWidget(0, 1);
    m_unambiguousNames->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_unambiguousNames->setItemDelegate(new TableDelegate(m_unambiguousNames));
    m_unambiguousNames->verticalHeader()->hide();
    m_unambiguousNames->horizontalHeader()->hide();
    m_unambiguousNames->setShowGrid(false);
    m_unambiguousNames->setWordWrap(false);
    m_unambiguousNames->setSelectionMode(QTableWidget::NoSelection);
    m_unambiguousNames->setFocusPolicy(Qt::NoFocus);
    m_unambiguousNames->setContextMenuPolicy(Qt::NoContextMenu);
    importedLayout->addWidget(m_unambiguousNames);

    m_possibleDuplicatesBox = new QGroupBox(tr("Mögliche Duplikate"));
    QVBoxLayout *duplicatesLayout = new QVBoxLayout(m_possibleDuplicatesBox);
    splitter->addWidget(m_possibleDuplicatesBox);
    m_possibleDuplicatesBox->hide();

    m_possibleDuplicatesLabel = new QLabel;
    m_possibleDuplicatesLabel->setWordWrap(true);
    duplicatesLayout->addWidget(m_possibleDuplicatesLabel);

    m_possibleDuplicates = new QTableWidget(0, 2);
    m_possibleDuplicates->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_possibleDuplicates->setItemDelegate(new TableDelegate(m_possibleDuplicates));
    m_possibleDuplicates->verticalHeader()->hide();
    m_possibleDuplicates->setShowGrid(false);
    m_possibleDuplicates->setWordWrap(false);
    m_possibleDuplicates->setSelectionMode(QTableWidget::NoSelection);
    m_possibleDuplicates->setFocusPolicy(Qt::NoFocus);
    m_possibleDuplicates->setContextMenuPolicy(Qt::NoContextMenu);
    m_possibleDuplicates->setHorizontalHeaderLabels({
        tr("Name aus der Import-%1").arg(m_importPlayersListString),
        tr("Mögliches Duplikat von") });
    duplicatesLayout->addWidget(m_possibleDuplicates);
    m_possibleDuplicates->blockSignals(true);
    connect(m_possibleDuplicates, &QTableWidget::itemChanged,
            this, &ImportPlayersDialog::updateImportCount);

    m_duplicatesPrefix = tr("(Duplikat?)");
    m_addDuplicatesPrefix = new QCheckBox(tr("Den Präfix „%1“ hinzufügen").arg(m_duplicatesPrefix));
    m_addDuplicatesPrefix->setChecked(true);
    duplicatesLayout->addWidget(m_addDuplicatesPrefix);

    QTimer::singleShot(0, this, &ImportPlayersDialog::prepareImport);
}

void ImportPlayersDialog::prepareImport()
{
    // Parse an input file
    importFile();

    // Search for possible duplicates
    findPossibleDuplicates();

    // Display the collision-free new names
    displayUnambiguousNames();

    // Try to resize the window so that we don't have scrollbars
    resizeView();

    updateImportCount(nullptr);
    m_possibleDuplicates->blockSignals(false);
    setEnabled(true);
}

void ImportPlayersDialog::importFile()
{
    // Get the import file's name
    QString fileName = QFileDialog::getOpenFileName(this,
        m_title,
        m_db->dbPath(),
        tr("Textdateien") + QStringLiteral(" (*.txt);;")
        + tr("Alle Dateien") + QStringLiteral(" (*)"));

    if (fileName.isEmpty()) {
        QDialog::reject();
        return;
    }

    // Try to open it
    QFile inputFile(fileName);
    if (! inputFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QMessageBox::critical(this,
            m_title,
            tr("Es bestehen keine Leserechte für die Datei „%1“. Bitte die Zugriffsrechte "
               "überprüfen!").arg(QDir::toNativeSeparators(fileName)),
            QMessageBox::Ok);
        QDialog::reject();
        return;
    }

    // Check if it's suspiciously large (the default 9600 B are about 200 average pair names)
    QFileInfo info(fileName);
    if (info.size() > DefaultValues::importSizeWarning) {
        if (QMessageBox::warning(this,
                m_title,
                tr("Die Datei „%1“ ist ungewöhnlich groß für eine %2. Soll sie trotzdem "
                   "importiert werden?").arg(QDir::toNativeSeparators(fileName),
                                             m_importPlayersListString),
                QMessageBox::Yes | QMessageBox::No,
                QMessageBox::No) == QMessageBox::No) {

            QDialog::reject();
            return;
        }
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    setEnabled(false);
    m_unambiguousNamesLabel->setText(tr("<i>Lese %1 …</i>").arg(m_importPlayersListString));
    QApplication::processEvents();

    Q_EMIT updateStatus(tr("Lese %1 …").arg(m_importPlayersListString));

    QTextStream stream(&inputFile);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    stream.setCodec(QTextCodec::codecForName("UTF-8"));
#endif

    int line = 0;
    bool askedAboutLongLines = false;
    int processed = 0;
    bool askedAboutManyLines = false;

    QStringList comparableList;
    int duplicates = 0;

    while (! stream.atEnd()) {
        line++;
        // Let's truncate lines at byte 256
        // just in case some user opens some binary file or whatnot ;-)

        // We also remove the legacy marker characters used in version 0.7.5 and 0.7.6, although
        // I don't think that anybody actually used this feature and still imports a pair/player
        // list containing those. But there we go ;-)
        const QString playersName = stream.readLine(256).simplified().remove(
                                        QStringLiteral("\u200B"));
        if (playersName.isEmpty()) {
            continue;
        }

        // If the file contains invalid characters, refuse to import it.
        // \uFFFD is Unicode's "replacement character", the boxed "?".
        if (playersName.contains(QStringLiteral("\uFFFD"))) {
            QString errorLine;
            if (playersName.length() > 45) {
                errorLine = playersName.left(45) + QStringLiteral("…");
            } else {
                errorLine = playersName;
            }

            QApplication::restoreOverrideCursor();
            Q_EMIT updateStatus(tr("Import fehlgeschlagen!"));
            QMessageBox::critical(this,
                m_title,
                tr("<p>Die Datei „%1“ enthält ungültige Zeichen. Vermutlich ist es keine reine "
                   "Textdatei, oder sie ist nicht UTF-8-kodiert. Zeile Nr. %2 war die erste "
                   "fehlerhalt kodierte:</p><p>%3</p>"
                   "<p>Es kann eine UTF-8-kodierte Textdatei importiert werden, die pro Zeile "
                   "einen Paar- bzw. Spielernamen enthält.</p>").arg(
                   QDir::toNativeSeparators(fileName).toHtmlEscaped(),
                   QString::number(line), errorLine),
                   QMessageBox::Ok);
            QDialog::reject();
            return;
        }

        // Check for suspiciously long lines

        if (! askedAboutLongLines
            && playersName.length() > DefaultValues::importLineLengthWarning) {

            QApplication::restoreOverrideCursor();
            if (QMessageBox::warning(this,
                m_title,
                tr("Die Datei „%1“ enthält ungewöhnlich lange Zeilen. Soll sie trotzdem importiert "
                   "werden?").arg(QDir::toNativeSeparators(fileName).toHtmlEscaped()),
                QMessageBox::Yes | QMessageBox::No,
                QMessageBox::No) == QMessageBox::No) {

                Q_EMIT updateStatus(tr("Import abgebrochen"));
                QDialog::reject();
                return;
            } else {
                askedAboutLongLines = true;
                QApplication::setOverrideCursor(Qt::WaitCursor);
            }
        }

        // Check for suspiciously many pairs/players

        if (! askedAboutManyLines
            && processed == DefaultValues::importLinesCountWarning) {

            QApplication::restoreOverrideCursor();
            if (QMessageBox::warning(this,
                m_title,
                tr("Es wurden bereits %1 Zeilen aus der Datei „%2“ ausgelesen. Das sind "
                   "ungewöhnlich viele für eine %3. Soll trotzdem fortgefahren werden?").arg(
                   QString::number(processed),
                   QDir::toNativeSeparators(fileName).toHtmlEscaped(),
                   m_importPlayersListString),
                QMessageBox::Yes | QMessageBox::No,
                QMessageBox::No) == QMessageBox::No) {

                inputFile.close();
                Q_EMIT updateStatus(tr("Einlesen abgebrochen"));
                return;
            } else {
                QApplication::setOverrideCursor(Qt::WaitCursor);
                askedAboutManyLines = true;
            }
        }

        const QString comparableName = m_stringTools->toLower(playersName);
        if (! comparableList.contains(comparableName)) {
            comparableList.append(comparableName);
            m_unambiguousNamesList.append(playersName);
        } else {
            duplicates++;
        }

        processed++;
    }

    inputFile.close();
    m_stringTools->sort(m_unambiguousNamesList);
    QApplication::restoreOverrideCursor();
    Q_EMIT updateStatus(tr("Einlesen beendet"));

    if (duplicates == 1) {
        QMessageBox::information(this, m_title, tr("Aus dem eingelesenen Datensatz wurde ein "
                                                   "exaktes Duplikat entfernt."));
    } else if (duplicates > 1) {
        QMessageBox::information(this, m_title, tr("Aus dem eingelesenen Datensatz wurden %1 "
                                                   "exakte Duplikate entfernt.").arg(
                                                   QString::number(duplicates)));
    }
}

void ImportPlayersDialog::findPossibleDuplicates()
{
    QApplication::setOverrideCursor(Qt::WaitCursor);

    // Search for possible duplicates inside the import list
    QHash<QString, QStringList> possibleDuplicatesImportList
        = m_searchEngine->checkListForPossibleDuplicates(m_unambiguousNamesList, m_phoneticSearch);

    // Check against all registered players

    const auto [ registeredNames, success ] = m_db->players();
    if (! success) {
        QDialog::reject();
    }

    QHash<QString, QStringList> possibleDuplicatesRegistered;
    for (const QString &importName : std::as_const(m_unambiguousNamesList)) {
        m_searchEngine->setSearchTerm(importName);
        for (const QString &registeredName : registeredNames) {
            if (m_searchEngine->checkMatch(registeredName, SearchEngine::DefaultSearch,
                                           m_phoneticSearch)) {

                possibleDuplicatesRegistered[importName].append(registeredName);
            }
        }
    }

    if (possibleDuplicatesImportList.count() == 0 && possibleDuplicatesRegistered.count() == 0) {
        // No possible duplicates, we can stop here
        QApplication::restoreOverrideCursor();
        return;
    }

    // Merge the possible duplicates lists

    QHash<QString, QStringList> displayPossibleDuplicates;
    QStringList noImportSelected;

    const auto possibleDuplicatesRegisteredKeys = possibleDuplicatesRegistered.keys();
    for (const QString &name : possibleDuplicatesRegisteredKeys) {
        const QString lowerName = m_stringTools->toLower(name);
        QStringList possibleDuplicates;

        const auto possibleDuplicatesRegisteredValueForName
            = possibleDuplicatesRegistered.value(name);
        for (const QString &possibleDuplicate : possibleDuplicatesRegisteredValueForName) {
            if (lowerName == m_stringTools->toLower(possibleDuplicate)) {
                possibleDuplicates.append(tr("(genau so angemeldet)"));
                noImportSelected.append(possibleDuplicate);
            } else {
                possibleDuplicates.append(tr("%1 (angemeldet)").arg(possibleDuplicate));
            }
            m_unambiguousNamesList.removeOne(possibleDuplicate);
        }

        if (possibleDuplicatesImportList.contains(name)) {
            const auto possibleDuplicatesImportListValueForName
                = possibleDuplicatesImportList.value(name);
            for (const QString &possibleDuplicate : possibleDuplicatesImportListValueForName) {
                possibleDuplicates.append(tr("%1 (in Importliste)").arg(possibleDuplicate));
                m_unambiguousNamesList.removeOne(possibleDuplicate);
            }
            possibleDuplicatesImportList.remove(name);
        }

        displayPossibleDuplicates[name] = possibleDuplicates;
    }

    const auto possibleDuplicatesImportListKeys = possibleDuplicatesImportList.keys();
    for (const QString &name : possibleDuplicatesImportListKeys) {
        const auto possibleDuplicatesImportListValueForName
            = possibleDuplicatesImportList.value(name);
        for (const QString &possibleDuplicate : possibleDuplicatesImportListValueForName) {
            displayPossibleDuplicates[name].append(tr("%1 (in Importliste)").arg(
                                                      possibleDuplicate));
            m_unambiguousNamesList.removeOne(possibleDuplicate);
        }
    }

    // Display the possible duplicates

    QStringList orderedPossbileDuplicateNames = displayPossibleDuplicates.keys();
    m_stringTools->sort(orderedPossbileDuplicateNames);
    m_possibleDuplicatesBox->show();
    m_possibleDuplicates->setRowCount(orderedPossbileDuplicateNames.count());

    QString text = tr("Es wurde(n) %n mögliche(s) Duplikat gefunden.", "",
                      orderedPossbileDuplicateNames.count());
    text += m_phoneticSearch ? tr(" Die phonetische Suche war aktiviert.\n")
                             : tr(" Die phonetische Suche war deaktiviert.\n");
    text += tr("Die markierten Einträge werden (trotzdem) importiert.");
    m_possibleDuplicatesLabel->setText(text);

    int row = -1;
    for (const QString &name : std::as_const(orderedPossbileDuplicateNames)) {
        QTableWidgetItem *cell = new QTableWidgetItem(name);
        if (noImportSelected.contains(name)) {
            cell->setForeground(Qt::red);
            cell->setCheckState(Qt::Unchecked);
        } else {
            cell->setCheckState(Qt::Checked);
        }
        m_possibleDuplicates->setItem(++row, 0, cell);
        m_possibleDuplicates->setItem(row, 1,
            new QTableWidgetItem(displayPossibleDuplicates.value(name).join(QStringLiteral("\n"))));
    }

    m_possibleDuplicates->resizeColumnsToContents();
    m_possibleDuplicates->resizeRowsToContents();
    QApplication::restoreOverrideCursor();
}

void ImportPlayersDialog::displayUnambiguousNames()
{
    const int count = m_unambiguousNamesList.count();
    if (count == 0) {
        m_unambiguousNames->setRowCount(1);
        m_unambiguousNames->setItem(0, 0,
            new QTableWidgetItem(tr("(Alle Einträge sind mögliche Duplikate, s.\u202FU.)")));
        m_unambiguousNamesLabel->hide();
    } else {
        m_unambiguousNames->setRowCount(count);
        int row = -1;
        for (const QString &name : std::as_const(m_unambiguousNamesList)) {
            row++;
            m_unambiguousNames->setItem(row, 0, new QTableWidgetItem(name));
        }
        if (count == 1) {
            m_unambiguousNamesLabel->setText(tr("Es wurde ein Eintrag gefunden:"));
        } else {
            m_unambiguousNamesLabel->setText(tr("Es wurden %1 Einträge gefunden:").arg(
                                                QString::number(count)));
        }
    }
    m_unambiguousNames->resizeColumnsToContents();
}

void ImportPlayersDialog::resizeView()
{
    int targetWidth;
    int targetHeight;
    if (m_possibleDuplicates->isVisible()) {
        targetWidth = m_possibleDuplicates->horizontalHeader()->length()
                      + (frameGeometry().width() - m_possibleDuplicates->width());
        targetHeight = m_unambiguousNames->sizeHint().height()
                       + m_possibleDuplicates->sizeHint().height()
                       + (frameGeometry().height() - m_unambiguousNames->height()
                                                   - m_possibleDuplicates->height());
    } else {
        targetWidth = m_unambiguousNames->horizontalHeader()->length()
                      + (frameGeometry().width() - m_unambiguousNames->width());
        targetHeight = m_unambiguousNames->sizeHint().height()
                       + (frameGeometry().height() - m_unambiguousNames->height());
    }

    const QRect parentGeometry = qobject_cast<QWidget *>(parent())->frameGeometry();

    if (targetWidth > parentGeometry.width()) {
        targetWidth = parentGeometry.width();
    }
    if (targetHeight > parentGeometry.height()) {
        targetHeight = parentGeometry.height();
    }
    if (targetWidth < 640) {
        targetWidth = 640;
    }
    if (targetHeight < 480) {
        targetHeight = 480;
    }

    if (targetWidth <= frameGeometry().width() && targetHeight <= frameGeometry().height()) {
        return;
    }

    targetWidth -= frameGeometry().width() - geometry().width();
    targetHeight -= frameGeometry().height() - geometry().height();

    setGeometry(parentGeometry.x() + (parentGeometry.width() - targetWidth) / 2,
                parentGeometry.y() + (parentGeometry.height() - targetHeight) / 2,
                targetWidth, targetHeight);
}

void ImportPlayersDialog::updateImportCount(QTableWidgetItem *)
{
    int count = 0;
    if (m_unambiguousNamesLabel->isVisible()) {
        // Otherwise, all entries are possible duplicates
        // but m_unambiguousNames contains one line with a message saying this
        count = m_unambiguousNames->rowCount();
    }

    int selectedDuplicates = 0;
    if (m_possibleDuplicates->isVisible()) {
        for (int i = 0; i < m_possibleDuplicates->rowCount(); i++) {
            if (m_possibleDuplicates->item(i, 0)->data(Qt::CheckStateRole) == Qt::Checked) {
                count++;
                selectedDuplicates++;
            }
        }
    }

    if (m_addDuplicatesPrefix->isVisible()) {
        m_addDuplicatesPrefix->setEnabled(selectedDuplicates > 0);
    }


    setButtonText(QDialogButtonBox::Ok,
                  m_tournamentSettings->isPairMode() ? tr("%n Paar(e) importieren", "", count)
                                                     : tr("%n Spieler importieren", "", count));
    setOkButtonEnabled(count > 0);
}

void ImportPlayersDialog::accept()
{
    QApplication::setOverrideCursor(Qt::WaitCursor);
    Q_EMIT updateStatus(m_tournamentSettings->isPairMode() ? tr("Importiere Paare …")
                                                           : tr("Importiere Spieler …"));
    QStringList comparableList;

    // Add a lower-case version of all existing names with markers removed
    const auto [ registeredPlayers, success ] = m_db->players();
    if (! success) {
        QDialog::reject();
    }
    for (const QString &name : registeredPlayers) {
        comparableList.append(m_stringTools->toLower(name));
    }

    // Add a lower-case version of the unambiguous names that will be imported with markers removed
    for (const QString &name : std::as_const(m_unambiguousNamesList)) {
        comparableList.append(name);
    }

    // Process all possible duplicates and generate non-colliding names
    if (m_possibleDuplicates->isVisible()) {
        QString nameToAdd;
        QString comparableName;
        for (int i = 0; i < m_possibleDuplicates->rowCount(); i++) {
            if (m_possibleDuplicates->item(i, 0)->data(Qt::CheckStateRole) != Qt::Checked) {
                continue;
            }

            nameToAdd = m_possibleDuplicates->item(i, 0)->text();
            if (m_addDuplicatesPrefix->isChecked()) {
                nameToAdd = m_duplicatesPrefix + QStringLiteral(" ") + nameToAdd;
            }

            // Generate a non-colliding name
            comparableName = m_stringTools->toLower(nameToAdd);
            if (comparableList.contains(comparableName)) {
                int i = 1;
                QString addition;
                do {
                    addition = QStringLiteral(" (%1)").arg(QString::number(++i));
                } while (comparableList.contains(comparableName + addition));
                comparableName += addition;
                nameToAdd += addition;
            }
            comparableList.append(comparableName);

            m_unambiguousNamesList.append(nameToAdd);
        }
    }

    if (! m_db->registerPlayers(m_unambiguousNamesList)) {
        QDialog::reject();
        return;
    }

    QApplication::restoreOverrideCursor();
    const int imported = m_unambiguousNamesList.count();

    successMessage(m_title,
                   m_tournamentSettings->isPairMode() ? tr("%n Paar(e) importiert!", "", imported)
                                                      : tr("%n Spieler importiert!", "", imported));
    QDialog::accept();
}
