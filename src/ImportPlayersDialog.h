// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef IMPORTPLAYERSDIALOG_H
#define IMPORTPLAYERSDIALOG_H

// Local includes
#include "shared/FileDialog.h"

// Local classes
class SharedObjects;
class Database;
class TournamentSettings;
class StringTools;
class SearchEngine;

// Qt classes
class QLabel;
class QTableWidget;
class QGroupBox;
class QCheckBox;
class QTableWidgetItem;

class ImportPlayersDialog : public FileDialog
{
    Q_OBJECT

public:
    explicit ImportPlayersDialog(QWidget *parent, SharedObjects *sharedObjects,
                                 bool phoneticSearch);

Q_SIGNALS:
    void updateStatus(const QString &text);

private Q_SLOTS:
    void prepareImport();
    void updateImportCount(QTableWidgetItem *);
    void accept() override;

private: // Functions
    void importFile();
    void findPossibleDuplicates();
    void displayUnambiguousNames();
    void resizeView();

private: // Variables
    Database *m_db;
    TournamentSettings *m_tournamentSettings;
    StringTools *m_stringTools;
    SearchEngine *m_searchEngine;
    const bool m_phoneticSearch;

    QString m_importPlayersListString;
    QString m_title;
    QLabel *m_unambiguousNamesLabel;
    QTableWidget *m_unambiguousNames;
    QGroupBox *m_possibleDuplicatesBox;
    QLabel *m_possibleDuplicatesLabel;
    QTableWidget *m_possibleDuplicates;
    QStringList m_unambiguousNamesList;
    QGroupBox *m_duplicatesBox;
    QCheckBox *m_addDuplicatesPrefix;
    QString m_duplicatesPrefix;

};

#endif // IMPORTPLAYERSDIALOG_H
