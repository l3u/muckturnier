// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "ScoreNotesGenerator2Boogers.h"

#include "shared/NoteType.h"

// Qt includes
#include <QDebug>

ScoreNotesGenerator2Boogers::ScoreNotesGenerator2Boogers(SharedObjects *sharedObjects,
                                                         SharedNoteSettingsWidget *sharedSettings,
                                                         QWidget *parent)
    : AbstractScoreNotesGenerator(sharedObjects,
                                  NoteType::ScoreNotes,
                                  sharedSettings,
                                  parent)
{
}

int ScoreNotesGenerator2Boogers::boogers() const
{
    return 2;
}

QString ScoreNotesGenerator2Boogers::tabTitle() const
{
    return tr("Spielstandzettel (2 Bobbl)");
}

QPageLayout::Orientation ScoreNotesGenerator2Boogers::orientation() const
{
    return QPageLayout::Landscape;
}

int ScoreNotesGenerator2Boogers::cols() const
{
    return 2;
}

int ScoreNotesGenerator2Boogers::rows() const
{
    return 2;
}

QString ScoreNotesGenerator2Boogers::tipsText() const
{
    return tr(
        "<p>Die hier benutzen Vorlagen sind für Spielstandzettel mit zwei Bobbln, entweder in der "
        "„horizontalen“ (Paare oben und unten, Bobbl von links nach rechts) oder der „vertikalen“ "
        "(Paare links und rechts, Bobbl von oben nach unten) Variante.</p>"
        "<p>Es werden vier DIN-A6-Zettel pro Seite generiert, in zwei Spalten und zwei Zeilen.</p>"
        "<p>Einfach immer so viele Blätter, wie man Runden hat, aufeinanderlegen und mit einem "
        "Papierschneider vierteln. Jedes Viertel ist dann ein Spielstandzettel-Block für einen "
        "Tisch.</p>"
        "<p>Mit der Vorlage „Nur mit Rundennummer“ können Ersatzblöcke generiert werden, die "
        "Vorlage „Ohne Runden- oder Tischnummer“ ist für Ersatz-Einzelzettel gedacht.</p>"
        "<p>Abhängig vom Drucker müssen die Abstandseinstellungen evtl. etwas angepasst werden, "
        "damit alle Zettel genau in der Mitte ausgegeben werden.</p>"
    );
}
