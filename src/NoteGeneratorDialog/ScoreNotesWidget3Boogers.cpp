// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "ScoreNotesWidget3Boogers.h"

#include "shared/NoteType.h"

// Qt includes
#include <QDebug>

ScoreNotesWidget3Boogers::ScoreNotesWidget3Boogers(SharedObjects *sharedObjects,
                                                   SharedNoteSettingsWidget *sharedSettings,
                                                   QWidget *parent)
    : AbstractScoreNotesWidget(sharedObjects,
                               NoteType::ScoreNotes3Boogers,
                               sharedSettings,
                               parent)
{
    addTemplate(tr("Mit Runden- und Tischnummer"),
                AbstractScoreNotesWidget::note);
    addTemplate(tr("Nur mit Rundennummer"),
                AbstractScoreNotesWidget::noteNoTable);
    addTemplate(tr("Ohne Runden- oder Tischnummer"),
                AbstractScoreNotesWidget::noteEmpty);
    addTemplate(tr("Mit Runden- und Tischnummer (vertikal)"),
                AbstractScoreNotesWidget::noteVertical);
    addTemplate(tr("Nur mit Rundennummer (vertikal)"),
                AbstractScoreNotesWidget::noteNoTableVertical);
    addTemplate(tr("Ohne Runden- oder Tischnummer (vertikal)"),
                AbstractScoreNotesWidget::noteEmptyVertical);

    restoreSettings();
}

int ScoreNotesWidget3Boogers::boogers() const
{
    return 3;
}

QString ScoreNotesWidget3Boogers::tabTitle() const
{
    return tr("Spielstandzettel (3 Bobbl)");
}

QPageLayout::Orientation ScoreNotesWidget3Boogers::orientation() const
{
    return QPageLayout::Portrait;
}

int ScoreNotesWidget3Boogers::cols() const
{
    return 1;
}

int ScoreNotesWidget3Boogers::rows() const
{
    return 3;
}

QString ScoreNotesWidget3Boogers::tipsText() const
{
    return tr(
        "<p>Die hier benutzen Vorlagen sind für Spielstandzettel mit drei Bobbln, entweder in der "
        "„horizontalen“ (Paare oben und unten, Bobbl von links nach rechts) oder der „vertikalen“ "
        "Variante (Paare links und rechts, Bobbl von oben nach unten).</p>"
        "<p>Es werden drei Zettel untereinander pro Seite generiert.</p>"
        "<p>Einfach immer so viele Blätter, wie man Runden hat, aufeinanderlegen und mit einem "
        "Papierschneider dritteln (ein Drittel eines DIN-A4-Blattes ist 99\u202Fmm hoch). Jedes "
        "Drittel ist dann ein Spielstandzettel-Block für einen Tisch.</p>"
        "<p>Mit der Vorlage „Nur mit Rundennummer“ können Ersatzblöcke generiert werden, die "
        "Vorlage „Ohne Runden- oder Tischnummer“ ist für Ersatz-Einzelzettel gedacht.</p>"
        "<p>Abhängig vom Drucker müssen die Abstandseinstellungen evtl. etwas angepasst werden, "
        "damit alle Zettel genau in der Mitte ausgegeben werden.</p>");
}
