// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SCORENOTESWIDGET3BOOGERS_H
#define SCORENOTESWIDGET3BOOGERS_H

// Local includes
#include "AbstractScoreNotesWidget.h"

// Local classes
class SharedObjects;
class SharedNoteSettingsWidget;

// Qt classes
class QComboBox;
class QSpinBox;

class ScoreNotesWidget3Boogers : public AbstractScoreNotesWidget
{
    Q_OBJECT

public:
    explicit ScoreNotesWidget3Boogers(SharedObjects *sharedObjects,
                                      SharedNoteSettingsWidget *sharedSettings,
                                      QWidget *parent = nullptr);
    QString tabTitle() const;

protected:
    QPageLayout::Orientation orientation() const;
    int cols() const;
    int rows() const;
    QString tipsText() const;
    int boogers() const;

};

#endif // SCORENOTESWIDGET3BOOGERS_H
