// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef TABLENUMBERSWIDGET_H
#define TABLENUMBERSWIDGET_H

// Local includes
#include "AbstractNotesGenerator.h"

// Local classes
class SharedObjects;
class SharedNoteSettingsWidget;
class Settings;

// Qt classes
class QSpinBox;

class TableNumbersWidget : public AbstractNotesGenerator
{
    Q_OBJECT

public:
    explicit TableNumbersWidget(SharedObjects *sharedObjects,
                                SharedNoteSettingsWidget *sharedSettings,
                                QWidget *parent = nullptr);
    QString tabTitle() const;
    QString defaultBaseName() const;
    AbstractNotesGenerator::Error generate(const QString &fileName);
    void saveSettings();

protected:
    QPageLayout::Orientation orientation() const;
    int cols() const;
    int rows() const;
    QString svgSource() const;
    QVector<QString> pathsToRemove() const;
    QVector<QString> textsToRemove() const;
    QString tipsText() const;

private: // Variables
    Settings *m_settings;
    QSpinBox *m_firstSticker;
    QSpinBox *m_firstNumber;
    QSpinBox *m_lastNumber;

};

#endif // TABLENUMBERSWIDGET_H
