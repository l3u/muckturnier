// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "AbstractNotesGenerator.h"
#include "AbstractNotesPage.h"
#include "SharedNoteSettingsWidget.h"
#include "SpacingWidget.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/ResourceFinder.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QPdfWriter>
#include <QSvgRenderer>
#include <QFile>
#include <QPainter>
#include <QLabel>
#include <QMessageBox>
#include <QRegularExpression>

static constexpr int s_dpi = 90;

AbstractNotesGenerator::AbstractNotesGenerator(NoteType::Type type,
                                               SharedObjects *sharedObjects,
                                               SharedNoteSettingsWidget *sharedSettings,
                                               QWidget *parent)
    : QWidget(parent),
      m_noteType(type),
      m_resourceFinder(sharedObjects->resourceFinder()),
      m_sharedSettings(sharedSettings)
{
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);

    m_layout = new QVBoxLayout(this);

    auto *tips = new QLabel(tr("<a href=\"#\">Hinweise und Tips zu dieser Vorlage</a>"));
    tips->setTextInteractionFlags(Qt::TextBrowserInteraction);
    connect(tips, &QLabel::linkActivated, this, [this]
            {
                QMessageBox::information(this, tr("Tips und Hinweise"),
                    tr("<p style=\"font-size: large; font-weight: bold;\">Vorlage „%1“</p>"
                       "%2").arg(qobject_cast<AbstractNotesPage *>(this->parent())->tabTitle(),
                                 tipsText()));
            });
    m_layout->addWidget(tips);

    m_spacingWidget = new SpacingWidget(m_noteType, sharedObjects->settings());
    m_layout->addWidget(m_spacingWidget);
}

NoteType::Type AbstractNotesGenerator::noteType() const
{
    return m_noteType;
}

QVBoxLayout *AbstractNotesGenerator::layout() const
{
    return m_layout;
}

QString AbstractNotesGenerator::prepareSvgData(QString svgData,
                                               const QHash<QString, QString> &data)
{
    QHash<QString, QString>::const_iterator it;
    for (it = data.constBegin(); it != data.constEnd(); it++) {
        svgData.replace(it.key(), it.value());
    }
    return svgData;
}

AbstractNotesGenerator::Error AbstractNotesGenerator::startPdf(const QString &pdfFile)
{
    // Find the template file
    m_currentSvgFile = svgSource();
    const auto svgPath = m_resourceFinder->find(m_currentSvgFile);
    if (svgPath.isEmpty()) {
        return Error::FileNotFound;
    }
    m_currentSvgFile = svgPath;

    // Read the raw data
    QFile file(m_currentSvgFile);
    if (! file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return Error::FileNotReadable;
    }
    m_svgData.clear();
    while (! file.atEnd()) {
        m_svgData.append(QString::fromUtf8(file.readLine()));
    }
    file.close();

    // Replace the shared placeholders
    m_svgData = prepareSvgData(m_svgData, QHash<QString, QString> {
        { QStringLiteral("@font-family@"), m_sharedSettings->fontFamily() },
        { QStringLiteral("@title@"),       m_sharedSettings->title() },
        { QStringLiteral("@subtitle@"),    m_sharedSettings->subTitle() }
    });

    // Remove elements we don't want.
    // Quick and dirty via regular expressions, without parsing the XML data ;-)

    // Remove paths
    for (const auto &id : pathsToRemove()) {
        QRegularExpression re(QStringLiteral(".+(<path.+?id=\"%1\".+?/>)").arg(id),
                              QRegularExpression::DotMatchesEverythingOption);
        const auto match = re.match(m_svgData);
        if (match.hasMatch()) {
            m_svgData.replace(match.captured(1), QString());
        }
    }

    // Remove texts
    for (const auto &id : textsToRemove()) {
        QRegularExpression re(QStringLiteral(".+(<text.+?id=\"%1\".+?</text>)").arg(id),
                              QRegularExpression::DotMatchesEverythingOption);
        const auto match = re.match(m_svgData);
        if (match.hasMatch()) {
            m_svgData.replace(match.captured(1), QString());
        }
    }

    // Setup the SVG renderer
    m_svgRenderer = new QSvgRenderer;
    if (! m_svgRenderer->load(m_currentSvgFile)) {
        m_svgRenderer->deleteLater();
        return Error::FileNotParseable;
    }
    m_renderSize = m_svgRenderer->defaultSize();

    // Setup the PDF writer

    m_pdfWriter = new QPdfWriter(pdfFile);
    m_pdfWriter->setPageSize(QPageSize(QPageSize::A4));
    m_pdfWriter->setPageOrientation(orientation());
    m_pdfWriter->setPageMargins(QMarginsF(0, 0, 0, 0));
    m_pdfWriter->setResolution(s_dpi);

    m_painter = new QPainter(m_pdfWriter);

    const auto pageSize = m_pdfWriter->pageLayout().pageSize();
    const auto pageSizePx = pageSize.sizePixels(s_dpi);
    const auto pageSizeMm = pageSize.size(QPageSize::Millimeter);

    int pageWidth = 0;
    int pageHeight = 0;

    double pxPerMmX = 0.0;
    double pxPerMmY = 0.0;

    switch (orientation()) {
    case QPageLayout::Portrait:
        pageWidth = pageSizePx.width();
        pxPerMmX = double(pageWidth) / double(pageSizeMm.width());
        pageHeight = pageSizePx.height();
        pxPerMmY = double(pageHeight) / double(pageSizeMm.height());
        break;
    case QPageLayout::Landscape:
        // We swap height and width, as we're using landscape mode
        pageWidth = pageSizePx.height();
        pxPerMmX = double(pageWidth) / double(pageSizeMm.height());
        pageHeight = pageSizePx.width();
        pxPerMmY = double(pageHeight) / double(pageSizeMm.width());
        break;
    }

    m_colWidth = double(pageWidth) / double(cols());
    m_rowHeight = double(pageHeight) / double(rows());

    m_outerSpacing = QPointF(m_spacingWidget->outerX() * pxPerMmX,
                             m_spacingWidget->outerY() * pxPerMmY);

    m_innerSpacingXMm = m_spacingWidget->innerX() * pxPerMmX;
    m_innerSpacingYMm = m_spacingWidget->innerY() * pxPerMmY;

    return AbstractNotesGenerator::NoError;
}

void AbstractNotesGenerator::renderNote(const QHash<QString, QString> &data, int col, int row)
{
    m_svgRenderer->load(prepareSvgData(m_svgData, data).toUtf8());

    const auto offset = QPointF(col * m_colWidth, row * m_rowHeight)
                        + m_outerSpacing
                        + QPointF(col * m_innerSpacingXMm, row * m_innerSpacingYMm);
    m_svgRenderer->render(m_painter, QRectF(offset, m_renderSize));
}

void AbstractNotesGenerator::newPage()
{
    m_pdfWriter->newPage();
}

void AbstractNotesGenerator::finishPdf()
{
    m_svgRenderer->deleteLater();
    delete m_painter;
    m_pdfWriter->deleteLater();
}

const QString &AbstractNotesGenerator::currentSvgFile() const
{
    return m_currentSvgFile;
}
