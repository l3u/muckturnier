// SPDX-FileCopyrightText: 2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef ABSTRACTNOTESPAGE_H
#define ABSTRACTNOTESPAGE_H

// Qt includes
#include <QWidget>

// Local classes
class AbstractNotesGenerator;

class AbstractNotesPage : public QWidget
{
    Q_OBJECT

public:
    virtual QString tabTitle() const = 0;
    virtual AbstractNotesGenerator *generator() const = 0;
    virtual void saveSettings() = 0;

Q_SIGNALS:
    void updateTabTitle();

protected:
    explicit AbstractNotesPage(QWidget *parent = nullptr);

};

#endif // ABSTRACTNOTESPAGE_H
