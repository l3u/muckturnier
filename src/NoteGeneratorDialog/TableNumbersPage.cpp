// SPDX-FileCopyrightText: 2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "TableNumbersPage.h"
#include "TableNumbersGenerator.h"

// Qt includes
#include <QVBoxLayout>

TableNumbersPage::TableNumbersPage(SharedObjects *sharedObjects,
                                   SharedNoteSettingsWidget *sharedNoteSettingsWidget,
                                   QWidget *parent)
    : AbstractNotesPage(parent)
{
    m_generator = new TableNumbersGenerator(sharedObjects, sharedNoteSettingsWidget);
    auto *layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->addWidget(m_generator);
    layout->addStretch();
}

QString TableNumbersPage::tabTitle() const
{
    return tr("Tischnummern");
}

AbstractNotesGenerator *TableNumbersPage::generator() const
{
    return m_generator;
}

void TableNumbersPage::saveSettings()
{
    m_generator->saveSettings();
}
