// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "TableNumbersGenerator.h"
#include "SpacingWidget.h"

#include "shared/SharedStyles.h"
#include "shared/NoteType.h"
#include "shared/Json.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/Settings.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QLabel>
#include <QGroupBox>
#include <QSpinBox>
#include <QGridLayout>
#include <QJsonObject>
#include <QProgressDialog>

// C++ includes
#include <cmath>

static const QLatin1String s_firstSticker("first sticker");
static const QLatin1String s_firstNumber ("first number");
static const QLatin1String s_lastNumber  ("last number");

TableNumbersGenerator::TableNumbersGenerator(SharedObjects *sharedObjects,
                                             SharedNoteSettingsWidget *sharedSettings,
                                             QWidget *parent)
    : AbstractNotesGenerator(NoteType::TableNumbers,
                             sharedObjects,
                             sharedSettings,
                             parent),
      m_settings(sharedObjects->settings())
{
    auto *box = new QGroupBox(tr("Tischnummern-spezifische Einstellungen"));
    box->setStyleSheet(SharedStyles::normalGroupBoxTitle);
    auto *boxLayout = new QGridLayout(box);
    layout()->addWidget(box);

    auto *label = new QLabel(tr("Ausgabeposition des 1. Tischnummern-Aufklebers\n"
                                "(von links nach rechts und von oben nach unten gezählt)"));
    boxLayout->addWidget(label, 0, 0);

    m_firstSticker = new QSpinBox;
    m_firstSticker->setMinimum(1);
    m_firstSticker->setMaximum(12);
    boxLayout->addWidget(m_firstSticker, 0, 1);

    boxLayout->addWidget(new QLabel(tr("1. Tischnummer")), 1, 0);
    m_firstNumber = new QSpinBox;
    m_firstNumber->setMinimum(1);
    m_firstNumber->setMaximum(24);
    boxLayout->addWidget(m_firstNumber, 1, 1);

    boxLayout->addWidget(new QLabel(tr("Letzte Tischnummer")), 2, 0);
    m_lastNumber = new QSpinBox;
    m_lastNumber->setMinimum(1);
    m_lastNumber->setMaximum(99);
    boxLayout->addWidget(m_lastNumber, 2, 1);

    connect(m_firstNumber, &QSpinBox::valueChanged, m_lastNumber, &QSpinBox::setMinimum);
    connect(m_lastNumber, &QSpinBox::valueChanged, m_firstNumber, &QSpinBox::setMaximum);

    layout()->addStretch();

    // Set the last values or the defaults

    const auto settings = Json::objectFromString(m_settings->notesSettings(noteType()));

    const auto firstSticker = settings.value(s_firstSticker);
    m_firstSticker->setValue(firstSticker.type() != QJsonValue::Double ? 1 : firstSticker.toInt());

    const auto firstNumber = settings.value(s_firstNumber);
    m_firstNumber->setValue(firstNumber.type() != QJsonValue::Double ? 1 : firstNumber.toInt());

    const auto lastNumber = settings.value(s_lastNumber);
    m_lastNumber->setValue(lastNumber.type() != QJsonValue::Double ? 24 : lastNumber.toInt());
}

QString TableNumbersGenerator::defaultBaseName() const
{
    return tr("Tischnummern");
}

QPageLayout::Orientation TableNumbersGenerator::orientation() const
{
    return QPageLayout::Portrait;
}

int TableNumbersGenerator::cols() const
{
    return 2;
}

int TableNumbersGenerator::rows() const
{
    return 6;
}

QString TableNumbersGenerator::svgSource() const
{
    return QStringLiteral("table_number.svg");
}

QVector<QString> TableNumbersGenerator::pathsToRemove() const
{
    return {};
}

QVector<QString> TableNumbersGenerator::textsToRemove() const
{
    return {};
}

QString TableNumbersGenerator::tipsText() const
{
    return tr(
        "<p>Es werden 12 Aufkleber pro Seite generiert, in 2 Spalten und 6 Zeilen.</p>"
        "<p>Passend für dieses Format wären z.\u202FB. die <a href=\"https://www.avery-"
        "zweckform.com/produkt/abloesbare-universal-etiketten-l4743rev-25\">ablösbaren Universal-"
        "Etiketten 99,1\u202F×\u202F42,3\u202Fmm</a> von Avery-Zweckform (von denen ich leider keine "
        "Provision bekomme ;-)</p>"
        "<p>Mit den passenden Abstandseinstellungen landen die Tischnummern genau in der Mitte "
        "der Etiketten. Abhängig vom Drucker müssen diese evtl. etwas angepasst werden.</p>");
}

AbstractNotesGenerator::Error TableNumbersGenerator::generate(const QString &fileName)
{
    const auto error = startPdf(fileName);
    if (error != AbstractNotesGenerator::NoError) {
        return error;
    }

    const int firstSticker = m_firstSticker->value();
    const int firstNumber = m_firstNumber->value();
    const int lastNumber = m_lastNumber->value();
    const int allStickers = firstSticker + (m_lastNumber->value() - firstNumber);
    const int allPages = std::ceil(double(allStickers) / 12.0);

    int sticker = 0;
    int stickersOnPage = 0;
    int number = firstNumber - 1;
    int currentSticker = 0;

    QProgressDialog progress(tr("Erstelle PDF-Datei …"), tr("Abbrechen"), 0, allStickers, this);
    progress.setWindowModality(Qt::WindowModal);
    progress.setMinimumDuration(1000);

    for (int page = 1; page <= allPages; page++) {
        for (int row = 0; row < rows(); row++) {
            for (int col = 0; col < cols(); col++) {
                sticker++;
                if (sticker < firstSticker || number >= lastNumber) {
                    stickersOnPage++;
                    continue;
                }

                number++;
                const QHash<QString, QString> data {
                    { QStringLiteral("@number@"), QString::number(number) }
                };
                renderNote(data, col, row);

                progress.setValue(++currentSticker);
                if (progress.wasCanceled()) {
                    finishPdf();
                    return AbstractNotesGenerator::Aborted;
                }

                stickersOnPage++;
                if (stickersOnPage == 12 && sticker != allStickers) {
                    stickersOnPage = 0;
                    newPage();
                }
            }
        }
    }

    finishPdf();
    return AbstractNotesGenerator::NoError;
}

void TableNumbersGenerator::saveSettings()
{
    m_settings->saveNotesSettings(noteType(), Json::serialize(QJsonObject {
        { s_firstSticker, m_firstSticker->value() },
        { s_firstNumber,  m_firstNumber->value() },
        { s_lastNumber,   m_lastNumber->value() }
    }));
}
