// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "NoteGeneratorDialog.h"
#include "SharedNoteSettingsWidget.h"
#include "AbstractNotesPage.h"
#include "TableNumbersPage.h"
#include "DrawNotesPage.h"
#include "ScoreNotesPage.h"
#include "AbstractNotesGenerator.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/TemporaryFileHelper.h"

// Qt includes
#include <QDebug>
#include <QStandardPaths>
#include <QVBoxLayout>
#include <QTabWidget>
#include <QDialogButtonBox>
#include <QApplication>
#include <QMessageBox>
#include <QDesktopServices>
#include <QDir>
#include <QUrl>
#include <QTimer>
#include <QLabel>

NoteGeneratorDialog::NoteGeneratorDialog(QWidget *parent, SharedObjects *sharedObjects)
    : TitleDialog(parent, TitleDialog::NoDelete),
      m_tmpHelper(sharedObjects->temporaryFileHelper())
{
    addButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Close);
    setOkButtonText(tr("Zettel drucken (via PDF)"));

    const auto tmpPath = m_tmpHelper->initialize();
    if (tmpPath.isEmpty()) {
        QTimer::singleShot(0, this, [this]
        {
            m_tmpHelper->displayErrorMessage(this, TemporaryFileHelper::CreateDirFailed);
            reject();
        });
    }

    m_sharedSettings = new SharedNoteSettingsWidget(sharedObjects->settings());
    layout()->addWidget(m_sharedSettings);

    m_tabWidget = new QTabWidget;
    layout()->addWidget(m_tabWidget);

    AbstractNotesPage *page;

    page = new TableNumbersPage(sharedObjects, m_sharedSettings);
    m_tabWidget->addTab(page, page->tabTitle());

    page = new DrawNotesPage(sharedObjects, m_sharedSettings);
    m_tabWidget->addTab(page, page->tabTitle());

    page = new ScoreNotesPage(sharedObjects, m_sharedSettings);
    m_tabWidget->addTab(page, page->tabTitle());
    connect(page, &AbstractNotesPage::updateTabTitle, this, &NoteGeneratorDialog::updateTabTitle);

    connect(m_tabWidget, &QTabWidget::currentChanged,
            this, &NoteGeneratorDialog::updateDialogTitle);
    updateDialogTitle();

    auto *dirLabel = new QLabel(tr(
        "<p>Alle Dateien werden in einem temporären Verzeichnis erstellt und mit dem Standard-"
        "PDF-Viewer geöffnet. Das Verzeichnis wird automatisch gelöscht, wenn das Programm "
        "geschlossen wird. <a href=\"%1\">Inhalte anzeigen</a>").arg(
        QUrl::fromLocalFile(tmpPath).toString()));
    dirLabel->setWordWrap(true);
    dirLabel->setTextInteractionFlags(Qt::TextBrowserInteraction);
    dirLabel->setOpenExternalLinks(true);
    layout()->addWidget(dirLabel);
}

void NoteGeneratorDialog::updateTabTitle()
{
    m_tabWidget->setTabText(m_tabWidget->currentIndex(),
                            qobject_cast<AbstractNotesPage *>(
                                m_tabWidget->currentWidget())->tabTitle());
    updateDialogTitle();
}

void NoteGeneratorDialog::updateDialogTitle()
{
    setTitle(tr("Zettel drucken: %1").arg(qobject_cast<AbstractNotesPage *>(
        m_tabWidget->currentWidget())->tabTitle()));
}

void NoteGeneratorDialog::accept()
{
    auto *notesGenerator = qobject_cast<AbstractNotesPage *>(
        m_tabWidget->currentWidget())->generator();

    const auto file = m_tmpHelper->getFileName(notesGenerator->defaultBaseName()
                                               + QStringLiteral(".pdf"));
    if (file.result != TemporaryFileHelper::FileCreated) {
        m_tmpHelper->displayErrorMessage(this, file.result);
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    const auto error = notesGenerator->generate(file.fileName);
    QApplication::restoreOverrideCursor();

    QString errorMessage;
    switch (error) {
    case AbstractNotesGenerator::NoError:
        break;
    case AbstractNotesGenerator::Aborted:
        errorMessage = tr("<p>Das Erstellen der PDF-Datei wurde abgebrochen!</p>");
        break;
    case AbstractNotesGenerator::FileNotFound:
        errorMessage = tr(
            "<p>Die Datei „%1“ konnte nicht gefunden werden. Bitte die Installation überprüfen!"
            "</p>").arg(
            notesGenerator->currentSvgFile());
        break;
    case AbstractNotesGenerator::FileNotReadable:
        errorMessage = tr(
            "<p>Die Datei „%1“ ist nicht lesbar. Bitte die Zugriffsrechte bzw. die Installation "
            "überprüfen!</p>").arg(
            QDir::toNativeSeparators(notesGenerator->currentSvgFile()).toHtmlEscaped());
        break;
    case AbstractNotesGenerator::FileNotParseable:
        errorMessage = tr(
            "<p>Die Datei „%1“ konnte nicht verarbeitet werden. Bitte die Installation überprüfen!"
            "</p>").arg(
            QDir::toNativeSeparators(notesGenerator->currentSvgFile()).toHtmlEscaped());
        break;
    }

    if (! errorMessage.isEmpty()) {
        QMessageBox::warning(this, tr("PDF-Datei erstellen"),
            tr("<p>Es ist ein Fehler beim Erstellen der PDF-Datei aufgetreten. Die Fehlermeldung "
               "war:</p>")
            + errorMessage);
        return;
    }

    if (! QDesktopServices::openUrl(QUrl::fromLocalFile(file.fileName))) {
        QMessageBox::warning(this, tr("PDF-Datei öffnen"),
            tr("<p><b>Automatisches Öffnen fehlgeschlagen</b></p>"
               "<p>Die generierte PDF-Datei</p>"
               "<p>%1</p>"
               "<p>konnte nicht automatisch geöffnet werden. Bitte die Datei manuell öffnen."
               "</p>").arg(QDir::toNativeSeparators(file.fileName).toHtmlEscaped()));
    }
}

void NoteGeneratorDialog::reject()
{
    m_sharedSettings->saveSettings();

    for (int i = 0; i < m_tabWidget->count(); i++) {
        qobject_cast<AbstractNotesPage *>(m_tabWidget->widget(i))->saveSettings();
    }

    TitleDialog::reject();
}
