// SPDX-FileCopyrightText: 2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "Logging.h"
#include "debugMode.h"

#ifdef DEBUG_MODE
// Enable all messages in debug mode
Q_LOGGING_CATEGORY(NoteGeneratorLog, "Muckturnier.NoteGenerator")
#else
// Only enable warnings otherwise
Q_LOGGING_CATEGORY(NoteGeneratorLog, "Muckturnier.NoteGenerator", QtWarningMsg)
#endif
