// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "SpacingWidget.h"

#include "shared/SharedStyles.h"

#include "SharedObjects/Settings.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QLabel>
#include <QGroupBox>
#include <QGridLayout>
#include <QDoubleSpinBox>
#include <QHBoxLayout>
#include <QPushButton>
#include <QMessageBox>

SpacingWidget::SpacingWidget(NoteType::Type type, Settings *settings, QWidget *parent)
    : QWidget(parent),
      m_type(type),
      m_settings(settings)
{
    auto *layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);

    auto *box = new QGroupBox(tr("Zusätzliche Innen- und Außenabstände"));
    box->setStyleSheet(SharedStyles::normalGroupBoxTitle);
    layout->addWidget(box);

    auto *wrapperLayout = new QHBoxLayout(box);

    auto *boxLayout = new QGridLayout;
    wrapperLayout->addLayout(boxLayout);

    boxLayout->addWidget(new QLabel(tr("Zusätzliche Außenabstände\n"
                                       "(verschiebt alle Zettel relativ zur Seite)")), 0, 0);
    boxLayout->addWidget(new QLabel(tr("X:")), 0, 1, Qt::AlignRight);
    m_outerX = mmSpinBox();
    boxLayout->addWidget(m_outerX, 0, 2);
    boxLayout->addWidget(new QLabel(tr("Y:")), 0, 3, Qt::AlignRight);
    m_outerY = mmSpinBox();
    boxLayout->addWidget(m_outerY, 0, 4);

    boxLayout->addWidget(new QLabel(tr("Zusätzliche Innenabstände\n"
                                       "(definiert die Abstände zwischen den Zetteln)")), 1, 0);
    boxLayout->addWidget(new QLabel(tr("X:")), 1, 1, Qt::AlignRight);
    m_innerX = mmSpinBox();
    boxLayout->addWidget(m_innerX, 1, 2);
    boxLayout->addWidget(new QLabel(tr("Y:")), 1, 3, Qt::AlignRight);
    m_innerY = mmSpinBox();
    boxLayout->addWidget(m_innerY, 1, 4);

    auto *line = new QFrame;
    line->setFrameShape(QFrame::VLine);
    line->setProperty("isseparator", true);
    wrapperLayout->addWidget(line);

    auto *buttonsLayout = new QVBoxLayout;
    wrapperLayout->addLayout(buttonsLayout);

    auto *saveSpacing = new QPushButton(tr("Speichern"));
    buttonsLayout->addWidget(saveSpacing);
    connect(saveSpacing, &QPushButton::clicked, this, [this]
            {
                m_settings->saveNotesSpacing(m_type,
                    QVector<double> { outerX(), outerY(), innerX(), innerY() });
                QMessageBox::information(this, tr("Werte speichern"), tr("Werte gespeichert!"));
            });

    auto *defaultValues = new QPushButton(tr("Standardwerte"));
    buttonsLayout->addWidget(defaultValues);
    connect(defaultValues, &QPushButton::clicked, this, [this]
            {
                setValues(m_settings->defaultNotesSpacing(m_type));
            });

    // Initialize the last saved or default values
    setValues(m_settings->notesSpacing(m_type));
}

void SpacingWidget::setValues(const QVector<double> &values)
{
    m_outerX->setValue(values.at(0));
    m_outerY->setValue(values.at(1));
    m_innerX->setValue(values.at(2));
    m_innerY->setValue(values.at(3));
}

QDoubleSpinBox *SpacingWidget::mmSpinBox() const
{
    auto *spinBox = new QDoubleSpinBox;
    spinBox->setSuffix(tr("\u202Fmm"));
    spinBox->setDecimals(1);
    spinBox->setMinimum(-99.9);
    spinBox->setMaximum(99.9);
    return spinBox;
}

double SpacingWidget::outerX() const
{
    return m_outerX->value();
}

double SpacingWidget::outerY() const
{
    return m_outerY->value();
}

double SpacingWidget::innerX() const
{
    return m_innerX->value();
}

double SpacingWidget::innerY() const
{
    return m_innerY->value();
}
