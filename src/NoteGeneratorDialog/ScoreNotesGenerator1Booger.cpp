// SPDX-FileCopyrightText: 2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "ScoreNotesGenerator1Booger.h"

#include "shared/NoteType.h"

// Qt includes
#include <QDebug>

ScoreNotesGenerator1Booger::ScoreNotesGenerator1Booger(SharedObjects *sharedObjects,
                                                       SharedNoteSettingsWidget *sharedSettings,
                                                       QWidget *parent)
    : AbstractScoreNotesGenerator(sharedObjects,
                                  NoteType::ScoreNotes1Booger,
                                  sharedSettings,
                                  parent)
{
}

int ScoreNotesGenerator1Booger::boogers() const
{
    return 1;
}

QString ScoreNotesGenerator1Booger::tabTitle() const
{
    return tr("Spielstandzettel (1 Bobbl)");
}

QPageLayout::Orientation ScoreNotesGenerator1Booger::orientation() const
{
    return QPageLayout::Landscape;
}

int ScoreNotesGenerator1Booger::cols() const
{
    return 2;
}

int ScoreNotesGenerator1Booger::rows() const
{
    return 2;
}

QString ScoreNotesGenerator1Booger::tipsText() const
{
    return tr(
        "<p>Die hier benutzen Vorlagen sind für Spielstandzettel mit einem Bobbl, entweder in der "
        "„horizontalen“ (Paare oben und unten) oder der „vertikalen“ (Paare links und rechts) "
        "Variante.</p>"
        "<p>Es werden vier DIN-A6-Zettel pro Seite generiert, in zwei Spalten und zwei Zeilen.</p>"
        "<p>Einfach immer so viele Blätter, wie man Runden hat, aufeinanderlegen und mit einem "
        "Papierschneider vierteln. Jedes Viertel ist dann ein Spielstandzettel-Block für einen "
        "Tisch.</p>"
        "<p>Mit der Vorlage „Nur mit Rundennummer“ können Ersatzblöcke generiert werden, die "
        "Vorlage „Ohne Runden- oder Tischnummer“ ist für Ersatz-Einzelzettel gedacht.</p>"
        "<p>Abhängig vom Drucker müssen die Abstandseinstellungen evtl. etwas angepasst werden, "
        "damit alle Zettel genau in der Mitte ausgegeben werden.</p>"
    );
}
