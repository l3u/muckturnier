// SPDX-FileCopyrightText: 2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "ScoreNotesPage.h"
#include "AbstractNotesGenerator.h"
#include "ScoreNotesGenerator1Booger.h"
#include "ScoreNotesGenerator2Boogers.h"
#include "ScoreNotesGenerator3Boogers.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/Settings.h"

// Qt includes
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QSpinBox>
#include <QStackedLayout>
#include <QTabWidget>
#include <QFrame>

ScoreNotesPage::ScoreNotesPage(SharedObjects *sharedObjects,
                               SharedNoteSettingsWidget *sharedNoteSettingsWidget,
                               QWidget *parent)
    : AbstractNotesPage(parent),
      m_settings(sharedObjects->settings())
{
    auto *mainLayout = new QVBoxLayout(this);

    auto *boogersLayout = new QHBoxLayout;
    mainLayout->addLayout(boogersLayout);

    boogersLayout->addWidget(new QLabel(tr("Anzahl Bobbl pro Runde:")));

    m_boogers = new QSpinBox;
    m_boogers->setMinimum(1);
    m_boogers->setMaximum(3);
    boogersLayout->addWidget(m_boogers);

    boogersLayout->addStretch();

    auto *line = new QFrame;
    line->setFrameShape(QFrame::HLine);
    line->setProperty("isseparator", true);
    mainLayout->addWidget(line);

    m_layout = new QStackedLayout;
    mainLayout->addLayout(m_layout);

    AbstractNotesGenerator *generator;

    generator = new ScoreNotesGenerator1Booger(sharedObjects, sharedNoteSettingsWidget);
    m_layout->addWidget(generator);

    generator = new ScoreNotesGenerator2Boogers(sharedObjects, sharedNoteSettingsWidget);
    m_layout->addWidget(generator);

    generator = new ScoreNotesGenerator3Boogers(sharedObjects, sharedNoteSettingsWidget);
    m_layout->addWidget(generator);

    connect(m_boogers, &QSpinBox::valueChanged, this, [this](int value)
    {
        m_layout->setCurrentIndex(value - 1);
        Q_EMIT updateTabTitle();
    });

    m_boogers->setValue(m_settings->scoreNotesBoogers());

    mainLayout->addStretch();
}

QString ScoreNotesPage::tabTitle() const
{
    return qobject_cast<AbstractScoreNotesGenerator *>(m_layout->currentWidget())->tabTitle();
}

AbstractNotesGenerator *ScoreNotesPage::generator() const
{
    return qobject_cast<AbstractNotesGenerator *>(m_layout->currentWidget());
}

void ScoreNotesPage::saveSettings()
{
    m_settings->saveScoreNotesBoogers(m_boogers->value());

    for (int i = 0; i < m_layout->count(); i++) {
        qobject_cast<AbstractNotesGenerator *>(m_layout->widget(i))->saveSettings();
    }
}
