// SPDX-FileCopyrightText: 2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "AbstractNotesPage.h"

AbstractNotesPage::AbstractNotesPage(QWidget *parent) : QWidget(parent)
{
}
