// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef NOTEGENERATORDIALOG_H
#define NOTEGENERATORDIALOG_H

// Local includes
#include "shared/TitleDialog.h"

// Local classes
class SharedObjects;
class TemporaryFileHelper;
class SharedNoteSettingsWidget;

// Qt classes
class QTabWidget;

class NoteGeneratorDialog : public TitleDialog
{
    Q_OBJECT

public:
    explicit NoteGeneratorDialog(QWidget *parent, SharedObjects *sharedObjects);

private Q_SLOTS:
    void accept() override;
    void reject() override;

private: // Variables
    TemporaryFileHelper *m_tmpHelper;
    SharedNoteSettingsWidget *m_sharedSettings;
    QTabWidget *m_tabWidget;

};

#endif // NOTEGENERATORDIALOG_H
