// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "SharedNoteSettingsWidget.h"

#include "shared/SharedStyles.h"
#include "shared/Json.h"
#include "shared/NoteType.h"

#include "SharedObjects/Settings.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QLineEdit>
#include <QLabel>
#include <QGridLayout>
#include <QComboBox>
#include <QJsonObject>
#include <QFontComboBox>

static const QString s_title = QStringLiteral("title");
static const QString s_subTitle = QStringLiteral("sub title");
static const QString s_fontFamily = QStringLiteral("font family");

SharedNoteSettingsWidget::SharedNoteSettingsWidget(Settings *settings, QWidget *parent)
    : QWidget(parent),
      m_settings(settings)
{
    auto *layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);

    auto *box = new QGroupBox(tr("Gemeinsame Einstellungen"));
    box->setStyleSheet(SharedStyles::normalGroupBoxTitle);
    layout->addWidget(box);

    auto *boxLayout = new QGridLayout(box);

    boxLayout->addWidget(new QLabel(tr("Überschrift\n"
                                       "(sofern vorhanden):")), 0, 0);
    m_title = new QLineEdit;
    boxLayout->addWidget(m_title, 0, 1);

    boxLayout->addWidget(new QLabel(tr("Unterüberschrift\n"
                                       "(sofern vorhanden):")), 1, 0);
    m_subTitle = new QLineEdit;
    boxLayout->addWidget(m_subTitle, 1, 1);

    auto *fontLabel = new QLabel(
        tr("Schriftart<br/>"
           "(empfohlen: Arimo, Liberation Sans, Arial)"));
    fontLabel->setTextInteractionFlags(Qt::TextBrowserInteraction);
    fontLabel->setOpenExternalLinks(true);
    boxLayout->addWidget(fontLabel, 2, 0);

    m_fontFamily = new QFontComboBox;
    boxLayout->addWidget(m_fontFamily, 2, 1);

    // Restore the last settings or set the defaults

    const auto savedSettings = Json::objectFromString(m_settings->notesSettings(NoteType::Shared));

    const auto title = savedSettings.value(s_title);
    m_title->setText(title.type() != QJsonValue::String ? tr("Muckturnier des Veranstalters XY")
                                                        : title.toString());

    const auto subTitle = savedSettings.value(s_subTitle);
    m_subTitle->setText(subTitle.type() != QJsonValue::String ? tr("am XX.XX.20XX")
                                                              : subTitle.toString());

    const auto fontFamily = savedSettings.value(s_fontFamily);
    setFontFamily(fontFamily.type() != QJsonValue::String ? QStringLiteral("Arimo")
                                                          : fontFamily.toString());
}

void SharedNoteSettingsWidget::setFontFamily(const QString &text)
{
    for (const auto &family : { text,
                                QStringLiteral("Arimo"),
                                QStringLiteral("Liberation Sans"),
                                QStringLiteral("Arial"),
                                QStringLiteral("Sans Serif") }) {

        const auto index = m_fontFamily->findText(family);
        if (index != -1) {
            m_fontFamily->setCurrentIndex(index);
            return;
        }
    }

    m_fontFamily->setCurrentIndex(0);
}

QString SharedNoteSettingsWidget::title() const
{
    return m_title->text().trimmed();
}

QString SharedNoteSettingsWidget::subTitle() const
{
    return m_subTitle->text().trimmed();
}

QString SharedNoteSettingsWidget::fontFamily() const
{
    return m_fontFamily->currentText();
}

void SharedNoteSettingsWidget::saveSettings()
{
    m_settings->saveNotesSettings(NoteType::Shared, Json::serialize(QJsonObject {
        { s_title,      title() },
        { s_subTitle,   subTitle() },
        { s_fontFamily, fontFamily() }
    }));
}
