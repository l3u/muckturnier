// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "ScoreNotesGenerator3Boogers.h"

#include "shared/NoteType.h"

// Qt includes
#include <QDebug>

ScoreNotesGenerator3Boogers::ScoreNotesGenerator3Boogers(SharedObjects *sharedObjects,
                                                         SharedNoteSettingsWidget *sharedSettings,
                                                         QWidget *parent)
    : AbstractScoreNotesGenerator(sharedObjects,
                                  NoteType::ScoreNotes3Boogers,
                                  sharedSettings,
                                  parent)
{
}

int ScoreNotesGenerator3Boogers::boogers() const
{
    return 3;
}

QString ScoreNotesGenerator3Boogers::tabTitle() const
{
    return tr("Spielstandzettel (3 Bobbl)");
}

QPageLayout::Orientation ScoreNotesGenerator3Boogers::orientation() const
{
    return QPageLayout::Portrait;
}

int ScoreNotesGenerator3Boogers::cols() const
{
    return 1;
}

int ScoreNotesGenerator3Boogers::rows() const
{
    return 3;
}

QString ScoreNotesGenerator3Boogers::tipsText() const
{
    return tr(
        "<p>Die hier benutzen Vorlagen sind für Spielstandzettel mit drei Bobbln, entweder in der "
        "„horizontalen“ (Paare oben und unten, Bobbl von links nach rechts) oder der „vertikalen“ "
        "(Paare links und rechts, Bobbl von oben nach unten) Variante.</p>"
        "<p>Es werden drei Zettel untereinander pro Seite generiert.</p>"
        "<p>Einfach immer so viele Blätter, wie man Runden hat, aufeinanderlegen und mit einem "
        "Papierschneider dritteln (ein Drittel eines DIN-A4-Blattes ist 99\u202Fmm hoch). Jedes "
        "Drittel ist dann ein Spielstandzettel-Block für einen Tisch.</p>"
        "<p>Mit der Vorlage „Nur mit Rundennummer“ können Ersatzblöcke generiert werden, die "
        "Vorlage „Ohne Runden- oder Tischnummer“ ist für Ersatz-Einzelzettel gedacht.</p>"
        "<p>Abhängig vom Drucker müssen die Abstandseinstellungen evtl. etwas angepasst werden, "
        "damit alle Zettel genau in der Mitte ausgegeben werden.</p>"
    );
}
