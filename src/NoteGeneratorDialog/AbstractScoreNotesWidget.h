// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef ABSTRACTSCORENOTESWIDGET_H
#define ABSTRACTSCORENOTESWIDGET_H

// Local includes

#include "AbstractNotesGenerator.h"

#include "shared/NoteType.h"

// Local classes
class SharedObjects;
class SharedNoteSettingsWidget;
class Settings;

// Qt classes
class QSpinBox;
class QComboBox;

class AbstractScoreNotesWidget : public AbstractNotesGenerator
{
    Q_OBJECT

public:
    explicit AbstractScoreNotesWidget(SharedObjects *sharedObjects,
                                      NoteType::Type type,
                                      SharedNoteSettingsWidget *sharedSettings,
                                      QWidget *parent = nullptr);
    QString defaultBaseName() const;
    AbstractNotesGenerator::Error generate(const QString &fileName);
    void saveSettings();

protected: // Strings
    inline static const auto note                = QLatin1String("note");
    inline static const auto noteNoTable         = QLatin1String("no table note");
    inline static const auto noteEmpty           = QLatin1String("empty note");
    inline static const auto noteVertical        = QLatin1String("vertical note");
    inline static const auto noteNoTableVertical = QLatin1String("vertical no table note");
    inline static const auto noteEmptyVertical   = QLatin1String("vertical empty note");

protected: // Functions
    void addTemplate(const QString &text, const QString &id);
    void restoreSettings();
    QString svgSource() const;
    QVector<QString> pathsToRemove() const;
    QVector<QString> textsToRemove() const;
    virtual int boogers() const = 0;

private: // Variables
    Settings *m_settings;
    QComboBox *m_noteTemplate;
    QSpinBox *m_tables;
    QSpinBox *m_rounds;

};

#endif // ABSTRACTSCORENOTESWIDGET_H
