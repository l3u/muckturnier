// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "ScoreNotesWidget.h"

#include "shared/NoteType.h"

// Qt includes
#include <QDebug>

ScoreNotesWidget::ScoreNotesWidget(SharedObjects *sharedObjects,
                                   SharedNoteSettingsWidget *sharedSettings,
                                   QWidget *parent)
    : AbstractScoreNotesWidget(sharedObjects,
                               NoteType::ScoreNotes,
                               sharedSettings,
                               parent)
{
    addTemplate(tr("Mit Runden- und Tischnummer"),
                AbstractScoreNotesWidget::note);
    addTemplate(tr("Nur mit Rundennummer"),
                AbstractScoreNotesWidget::noteNoTable);
    addTemplate(tr("Ohne Runden- oder Tischnummer"),
                AbstractScoreNotesWidget::noteEmpty);
    addTemplate(tr("Mit Runden- und Tischnummer (vertikal)"),
                AbstractScoreNotesWidget::noteVertical);
    addTemplate(tr("Nur mit Rundennummer (vertikal)"),
                AbstractScoreNotesWidget::noteNoTableVertical);
    addTemplate(tr("Ohne Runden- oder Tischnummer (vertikal)"),
                AbstractScoreNotesWidget::noteEmptyVertical);

    restoreSettings();
}

int ScoreNotesWidget::boogers() const
{
    return 2;
}

QString ScoreNotesWidget::tabTitle() const
{
    return tr("Spielstandzettel (2 Bobbl)");
}

QPageLayout::Orientation ScoreNotesWidget::orientation() const
{
    return QPageLayout::Landscape;
}

int ScoreNotesWidget::cols() const
{
    return 2;
}

int ScoreNotesWidget::rows() const
{
    return 2;
}

QString ScoreNotesWidget::tipsText() const
{
    return tr(
        "<p>Die hier benutzen Vorlagen sind für Spielstandzettel mit zwei Bobbln, entweder in der "
        "„horizontalen“ (Paare oben und unten, Bobbl von links nach rechts) oder der „vertikalen“ "
        "Variante (Paare links und rechts, Bobbl von oben nach unten).</p>"
        "<p>Es werden vier DIN-A6-Zettel pro Seite generiert, in zwei Spalten und zwei Zeilen.</p>"
        "<p>Einfach immer so viele Blätter, wie man Runden hat, aufeinanderlegen und mit einem "
        "Papierschneider vierteln. Jedes Viertel ist dann ein Spielstandzettel-Block für einen "
        "Tisch.</p>"
        "<p>Mit der Vorlage „Nur mit Rundennummer“ können Ersatzblöcke generiert werden, die "
        "Vorlage „Ohne Runden- oder Tischnummer“ ist für Ersatz-Einzelzettel gedacht.</p>"
        "<p>Abhängig vom Drucker müssen die Abstandseinstellungen evtl. etwas angepasst werden, "
        "damit alle Zettel genau in der Mitte ausgegeben werden.</p>");
}
