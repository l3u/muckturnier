// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "AbstractScoreNotesGenerator.h"
#include "SpacingWidget.h"
#include "Logging.h"

#include "shared/DefaultValues.h"
#include "shared/SharedStyles.h"
#include "shared/Json.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/Settings.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QLabel>
#include <QGroupBox>
#include <QSpinBox>
#include <QGridLayout>
#include <QJsonObject>
#include <QProgressDialog>
#include <QComboBox>

static const QString s_invalidTemplate = QStringLiteral("INVALID_TEMPLATE");

static const QLatin1String s_noteTemplate("note template");
static const QLatin1String s_tables      ("tables");
static const QLatin1String s_rounds      ("rounds");

static const QLatin1String s_note               ("note");
static const QLatin1String s_noteNoTable        ("no table note");
static const QLatin1String s_noteEmpty          ("empty note");
static const QLatin1String s_noteVertical       ("vertical note");
static const QLatin1String s_noteVerticalNoTable("vertical no table note");
static const QLatin1String s_noteVerticalEmpty  ("vertical empty note");

AbstractScoreNotesGenerator::AbstractScoreNotesGenerator(SharedObjects *sharedObjects,
                                                         NoteType::Type type,
                                                         SharedNoteSettingsWidget *sharedSettings,
                                                         QWidget *parent)
    : AbstractNotesGenerator(type,
                             sharedObjects,
                             sharedSettings,
                             parent),
      m_settings(sharedObjects->settings())
{
    layout()->setContentsMargins(0, 0, 0, 0);

    auto *box = new QGroupBox(tr("Spielstandzettel-spezifische Einstellungen"));
    box->setStyleSheet(SharedStyles::normalGroupBoxTitle);
    auto *boxLayout = new QGridLayout(box);
    layout()->addWidget(box);

    boxLayout->addWidget(new QLabel(tr("Vorlage für die Spielstandzettel:")), 0, 0);
    m_noteTemplate = new QComboBox;
    boxLayout->addWidget(m_noteTemplate, 0, 1);

    m_noteTemplate->addItem(tr("Mit Runden- und Tischnummer"),
                            QStringLiteral("note"));
    m_noteTemplate->addItem(tr("Nur mit Rundennummer"),
                            QStringLiteral("no table note"));
    m_noteTemplate->addItem(tr("Ohne Runden- oder Tischnummer"),
                            QStringLiteral("empty note"));
    m_noteTemplate->addItem(tr("Mit Runden- und Tischnummer (vertikal)"),
                            QStringLiteral("vertical note"));
    m_noteTemplate->addItem(tr("Nur mit Rundennummer (vertikal)"),
                            QStringLiteral("vertical no table note"));
    m_noteTemplate->addItem(tr("Ohne Runden- oder Tischnummer (vertikal)"),
                            QStringLiteral("vertical empty note"));

    boxLayout->addWidget(new QLabel(tr("Anzahl Tische:")), 1, 0);
    m_tables = new QSpinBox;
    m_tables->setMinimum(1);
    m_tables->setMaximum(DefaultValues::maximumNotesTables);
    boxLayout->addWidget(m_tables, 1, 1);

    boxLayout->addWidget(new QLabel(tr("Anzahl Runden:")), 2, 0);
    m_rounds = new QSpinBox;
    m_rounds->setMinimum(1);
    m_rounds->setMaximum(DefaultValues::maximumNotesRounds);
    boxLayout->addWidget(m_rounds, 2, 1);

    layout()->addStretch();

    // Restore the settings

    const auto settings = Json::objectFromString(m_settings->notesSettings(noteType()));

    const auto noteTemplate = settings.value(s_noteTemplate);
    if (noteTemplate.type() == QJsonValue::String) {
        const auto index = m_noteTemplate->findData(noteTemplate.toString());
        if (index != -1) {
            m_noteTemplate->setCurrentIndex(index);
        }
    }

    const auto tables = settings.value(s_tables);
    m_tables->setValue(tables.type() != QJsonValue::Double ? 12 : tables.toInt());

    const auto rounds = settings.value(s_rounds);
    m_rounds->setValue(rounds.type() != QJsonValue::Double ? 5 : rounds.toInt());
}

QString AbstractScoreNotesGenerator::defaultBaseName() const
{
    return tr("Spielstandzettel");
}

QString AbstractScoreNotesGenerator::svgSource() const
{
    const auto noteTemplate = m_noteTemplate->currentData().toString();

    if (   noteTemplate == s_note
        || noteTemplate == s_noteNoTable
        || noteTemplate == s_noteEmpty) {

        switch (boogers()) {
        case 1:
            return QStringLiteral("score_note_1_booger.svg");
        case 2:
            return QStringLiteral("score_note_2_boogers.svg");
        case 3:
            return QStringLiteral("score_note_3_boogers.svg");
        default:
            // This should not happen
            qCWarning(NoteGeneratorLog) << "Score notes template: Invalid boogers count passed!";
            return s_invalidTemplate;
        }

    } else if (   noteTemplate == s_noteVertical
               || noteTemplate == s_noteVerticalNoTable
               || noteTemplate == s_noteVerticalEmpty) {

        switch (boogers()) {
        case 1:
            return QStringLiteral("score_note_1_booger_vertical.svg");
        case 2:
            return QStringLiteral("score_note_2_boogers_vertical.svg");
        case 3:
            return QStringLiteral("score_note_3_boogers_vertical.svg");
        default:
            // This should not happen
            qCWarning(NoteGeneratorLog) << "Score notes template: Invalid boogers count passed!";
            return s_invalidTemplate;
        }

    } else {
        Q_UNREACHABLE();
        return s_invalidTemplate;
    }
}

QVector<QString> AbstractScoreNotesGenerator::pathsToRemove() const
{
    const auto noteTemplate = m_noteTemplate->currentData().toString();
    if (noteTemplate == s_note || noteTemplate == s_noteVertical) {
        return { QStringLiteral("round_line"),
                 QStringLiteral("table_line") };
    } else if (noteTemplate == s_noteNoTable || noteTemplate == s_noteVerticalNoTable) {
        return { QStringLiteral("round_line") };
    } else { // empty note
        return {};
    }
}

QVector<QString> AbstractScoreNotesGenerator::textsToRemove() const
{
    const auto noteTemplate = m_noteTemplate->currentData().toString();
    if (noteTemplate == s_note || noteTemplate == s_noteVertical) {
        return {};
    } else if (noteTemplate == s_noteNoTable || noteTemplate == s_noteVerticalNoTable) {
        return { QStringLiteral("table_placeholder") };
    } else { // empty note
        return { QStringLiteral("round_placeholder"),
                 QStringLiteral("table_placeholder") };
    }
}

AbstractNotesGenerator::Error AbstractScoreNotesGenerator::generate(const QString &fileName)
{
    const auto error = startPdf(fileName);
    if (error != AbstractNotesGenerator::NoError) {
        return error;
    }

    const int tables = m_tables->value();
    const int rounds = m_rounds->value();

    const int notesPerPage = cols() * rows();

    int allNotes = 0;
    if (tables < notesPerPage) {
        allNotes = notesPerPage * rounds;
    } else {
        const auto missingTables = tables % notesPerPage;
        if (missingTables > 0) {
            allNotes = (tables + (notesPerPage - missingTables)) * rounds;
        } else {
            allNotes = tables * rounds;
        }
    }

    int col = -1;
    int row = 0;
    int note = 0;

    QProgressDialog progress(tr("Erstelle PDF-Datei …"), tr("Abbrechen"), 0, allNotes, this);
    progress.setWindowModality(Qt::WindowModal);
    progress.setMinimumDuration(1000);

    for (int firstOnPage = 1; firstOnPage <= tables; firstOnPage += notesPerPage) {
        for (int round = 1; round <= rounds; round++) {
            for (int table = firstOnPage; table <= firstOnPage + notesPerPage - 1; table++) {
                note++;
                col++;
                if (col == cols()) {
                    col = 0;
                    row++;
                }

                if (table > tables) {
                    continue;
                }

                const QHash<QString, QString> data {
                    { QStringLiteral("@round@"), QString::number(round) },
                    { QStringLiteral("@table@"), QString::number(table) }
                };
                renderNote(data, col, row);

                progress.setValue(note);
                if (progress.wasCanceled()) {
                    finishPdf();
                    return AbstractNotesGenerator::Aborted;
                }
            }

            if (note < allNotes) {
                newPage();
                col = -1;
                row = 0;
            }
        }
    }

    finishPdf();
    return AbstractNotesGenerator::NoError;
}

void AbstractScoreNotesGenerator::saveSettings()
{
    m_settings->saveNotesSettings(noteType(), Json::serialize(QJsonObject {
        { s_noteTemplate, m_noteTemplate->currentData().toString() },
        { s_tables,       m_tables->value() },
        { s_rounds,       m_rounds->value() }
    }));
}
