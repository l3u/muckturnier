// SPDX-FileCopyrightText: 2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SCORENOTESGENERATOR1BOOGER_H
#define SCORENOTESGENERATOR1BOOGER_H

// Local includes
#include "AbstractScoreNotesGenerator.h"

// Local classes
class SharedObjects;
class SharedNoteSettingsWidget;

// Qt classes
class QComboBox;
class QSpinBox;

class ScoreNotesGenerator1Booger : public AbstractScoreNotesGenerator
{
    Q_OBJECT

public:
    explicit ScoreNotesGenerator1Booger(SharedObjects *sharedObjects,
                                        SharedNoteSettingsWidget *sharedSettings,
                                        QWidget *parent = nullptr);
    QString tabTitle() const;

protected:
    QPageLayout::Orientation orientation() const;
    int cols() const;
    int rows() const;
    QString tipsText() const;
    int boogers() const;

};

#endif // SCORENOTESGENERATOR1BOOGER_H
