// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef DRAWNOTESGENERATOR_H
#define DRAWNOTESGENERATOR_H

// Local includes
#include "AbstractNotesGenerator.h"

// Local classes
class SharedObjects;
class SharedNoteSettingsWidget;
class Settings;

// Qt classes
class QLabel;
class QComboBox;
class QSpinBox;
class QCheckBox;

class DrawNotesGenerator : public AbstractNotesGenerator
{
    Q_OBJECT

public:
    explicit DrawNotesGenerator(SharedObjects *sharedObjects,
                                SharedNoteSettingsWidget *sharedSettings,
                                QWidget *parent = nullptr);
    QString defaultBaseName() const;
    AbstractNotesGenerator::Error generate(const QString &fileName);
    void saveSettings();

protected:
    QPageLayout::Orientation orientation() const;
    int cols() const;
    int rows() const;
    QString svgSource() const;
    QVector<QString> pathsToRemove() const;
    QVector<QString> textsToRemove() const;
    QString tipsText() const;

private Q_SLOTS:
    void setBestStackSize(int tables);
    void checkEnableRound(int index);

private: // Variables
    Settings *m_settings;
    QComboBox *m_noteTemplate;
    QLabel *m_roundLabel;
    QSpinBox *m_round;
    QSpinBox *m_tables;
    QComboBox *m_pairFormat;
    QCheckBox *m_twoNotesPerPair;
    QSpinBox *m_pagesPerStack;

};

#endif // DRAWNOTESGENERATOR_H
