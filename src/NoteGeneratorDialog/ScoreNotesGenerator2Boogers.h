// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SCORENOTESGENERATOR2BOOGERS_H
#define SCORENOTESGENERATOR2BOOGERS_H

// Local includes
#include "AbstractScoreNotesGenerator.h"

// Local classes
class SharedObjects;
class SharedNoteSettingsWidget;

// Qt classes
class QComboBox;
class QSpinBox;

class ScoreNotesGenerator2Boogers : public AbstractScoreNotesGenerator
{
    Q_OBJECT

public:
    explicit ScoreNotesGenerator2Boogers(SharedObjects *sharedObjects,
                                         SharedNoteSettingsWidget *sharedSettings,
                                         QWidget *parent = nullptr);
    QString tabTitle() const;

protected:
    QPageLayout::Orientation orientation() const;
    int cols() const;
    int rows() const;
    QString tipsText() const;
    int boogers() const;

};

#endif // SCORENOTESGENERATOR2BOOGERS_H
