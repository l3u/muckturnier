// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "DrawNotesGenerator.h"
#include "SpacingWidget.h"

#include "shared/DefaultValues.h"
#include "shared/SharedStyles.h"
#include "shared/NoteType.h"
#include "shared/Json.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/Settings.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QLabel>
#include <QGroupBox>
#include <QSpinBox>
#include <QGridLayout>
#include <QComboBox>
#include <QCheckBox>
#include <QJsonObject>
#include <QProgressDialog>
#include <QHBoxLayout>
#include <QMessageBox>

static const QLatin1String s_note          ("note");
static const QLatin1String s_emptyNote     ("empty note");
static const QLatin1String s_roundNote     ("round note");
static const QLatin1String s_emptyRoundNote("empty round note");

static const QLatin1String s_movingPairs  ("moving pairs");
static const QLatin1String s_pairAndPlayer("pair and player");
static const QLatin1String s_onlyPair     ("only pair");

static const QLatin1String s_noteTemplate   ("template");
static const QLatin1String s_tables         ("tables");
static const QLatin1String s_round          ("round");
static const QLatin1String s_twoNotesPerPair("two notes per pair");
static const QLatin1String s_pairFormat     ("pair format");
static const QLatin1String s_pagesPerStack  ("pages per stack");

DrawNotesGenerator::DrawNotesGenerator(SharedObjects *sharedObjects,
                                       SharedNoteSettingsWidget *sharedSettings,
                                       QWidget *parent)
    : AbstractNotesGenerator(NoteType::DrawNotes,
                             sharedObjects,
                             sharedSettings,
                             parent),
      m_settings(sharedObjects->settings())
{
    auto *box = new QGroupBox(tr("Auslosungszettel-spezifische Einstellungen"));
    box->setStyleSheet(SharedStyles::normalGroupBoxTitle);
    auto *boxLayout = new QGridLayout(box);
    layout()->addWidget(box);

    boxLayout->addWidget(new QLabel(tr("Vorlage für die\nAuslosungszettel:")), 0, 0);

    m_noteTemplate = new QComboBox;
    m_noteTemplate->addItem(tr("Tisch und Paar (ohne Runde)"), s_note);
    m_noteTemplate->addItem(tr("Blanko ohne Runde"), s_emptyNote);
    m_noteTemplate->addItem(tr("Runde, Tisch und Paar"), s_roundNote);
    m_noteTemplate->addItem(tr("Blanko mit Runde"), s_emptyRoundNote);
    boxLayout->addWidget(m_noteTemplate, 0, 1);

    auto *subLayout = new QHBoxLayout;
    boxLayout->addLayout(subLayout, 0, 2);
    m_roundLabel = new QLabel(tr("Runde:"));
    m_roundLabel->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Preferred);
    subLayout->addWidget(m_roundLabel);
    m_round = new QSpinBox;
    m_round->setMinimum(1);
    m_round->setMaximum(DefaultValues::maximumNotesRounds);
    subLayout->addWidget(m_round);
    connect(m_noteTemplate, &QComboBox::currentIndexChanged,
            this, &DrawNotesGenerator::checkEnableRound);

    boxLayout->addWidget(new QLabel(tr("Tische:")), 1, 0);

    m_tables = new QSpinBox;
    m_tables->setMinimum(1);
    m_tables->setMaximum(DefaultValues::maximumNotesTables);
    connect(m_tables, &QSpinBox::valueChanged, this, &DrawNotesGenerator::setBestStackSize);
    boxLayout->addWidget(m_tables, 1, 1);

    m_twoNotesPerPair = new QCheckBox(tr("Zwei Auslosungszettel pro Paar\n"
                                         "(für Einzelspielerturniere)"));
    connect(m_twoNotesPerPair, &QCheckBox::toggled, this, [this](bool checked)
            {
                if (checked) {
                    m_pairFormat->setCurrentIndex(m_pairFormat->findData(s_pairAndPlayer));
                }
                setBestStackSize(m_tables->value());
            });
    boxLayout->addWidget(m_twoNotesPerPair, 1, 2);

    boxLayout->addWidget(new QLabel(tr("Format der\nPaarnummer:")), 4, 0);
    m_pairFormat = new QComboBox;
    m_pairFormat->addItem(tr("„Paar 1 (bleibt sitzen)“ / „Paar 2 (rutscht weiter)“"),
                          s_movingPairs);
    m_pairFormat->addItem(tr("„Paarnummer (Spielernummer)“"), s_pairAndPlayer);
    m_pairFormat->addItem(tr("Nur „Paarnummer“"), s_onlyPair);
    boxLayout->addWidget(m_pairFormat, 4, 1, 1, 2);

    auto *line = new QFrame;
    line->setFrameShape(QFrame::HLine);
    line->setProperty("isseparator", true);
    boxLayout->addWidget(line, 5, 0, 1, 3);

    boxLayout->addWidget(new QLabel(tr("Seiten pro Stapel:")), 6, 0);

    m_pagesPerStack = new QSpinBox;
    m_pagesPerStack->setMinimum(1);
    m_pagesPerStack->setMaximum(99);
    boxLayout->addWidget(m_pagesPerStack, 6, 1);

    auto *stackLabel = new QLabel(tr("<a href=\"#\">Was heißt das?</a>"));
    stackLabel->setTextInteractionFlags(Qt::TextBrowserInteraction);
    connect(stackLabel, &QLabel::linkActivated, this, [this]
            {
                QMessageBox::information(this, tr("Auslosungszettel drucken"),
                    tr("<p>Die Zettel-Bögen werden zum einfacheren Schneiden in Stapel "
                       "zusammengefasst, die dann nacheinander geschnitten werden müssen.</p>"
                       "<p>Die beste Anzahl Seiten pro Stapel (also die mit den wenigsten leeren "
                       "Zetteln, bei maximal 5 Seiten pro Stapel) wird automatisch hier "
                       "eingestellt, sobald die Anzahl der Tische bzw. die der Zettel pro Paar "
                       "verändert wird.</p>"
                       "<p>Ungeachtet dessen können auch größere oder kleinere Stapel erzeugt "
                       "werden. In dem Fall gibt es aber dann unnötig viele leere Zettel.</p>"));
            });
    boxLayout->addWidget(stackLabel, 6, 2);

    layout()->addStretch();

    // Set the last settings or the defaults

    const auto settings = Json::objectFromString(m_settings->notesSettings(noteType()));

    const auto noteTemplate = settings.value(s_noteTemplate);
    if (noteTemplate.type() == QJsonValue::String) {
        const auto index = m_noteTemplate->findData(noteTemplate.toString());
        if (index != -1) {
            m_noteTemplate->setCurrentIndex(index);
        }
    }
    checkEnableRound(m_noteTemplate->currentIndex());

    const auto tables = settings.value(s_tables);
    m_tables->setValue(tables.type() != QJsonValue::Double ? 16 : tables.toInt());

    const auto round = settings.value(s_round);
    m_round->setValue(round.type() != QJsonValue::Double ? 1 : round.toInt());

    const auto twoNotesPerPair = settings.value(s_twoNotesPerPair);
    m_twoNotesPerPair->setChecked(twoNotesPerPair.type() != QJsonValue::Bool
        ? false : twoNotesPerPair.toBool());

    const auto pairFormat = settings.value(s_pairFormat);
    if (pairFormat.type() == QJsonValue::String) {
        const auto index = m_pairFormat->findData(pairFormat.toString());
        if (index != -1) {
            m_pairFormat->setCurrentIndex(index);
        }
    }

    const auto pagesPerStack = settings.value(s_pagesPerStack);
    if (pagesPerStack.type() == QJsonValue::Double) {
        m_pagesPerStack->setValue(pagesPerStack.toInt());
    }
}

QPageLayout::Orientation DrawNotesGenerator::orientation() const
{
    return QPageLayout::Portrait;
}

int DrawNotesGenerator::cols() const
{
    return 2;
}

int DrawNotesGenerator::rows() const
{
    return 4;
}

QString DrawNotesGenerator::svgSource() const
{
    const auto noteTemplate = m_noteTemplate->currentData().toString();
    if (noteTemplate == s_note || noteTemplate == s_emptyNote) {
        return QStringLiteral("draw_note.svg");
    } else {
        return QStringLiteral("draw_note_round.svg");
    }
}

QVector<QString> DrawNotesGenerator::pathsToRemove() const
{
    const auto noteTemplate = m_noteTemplate->currentData().toString();
    if (noteTemplate == s_note) {
        return { QStringLiteral("table_line"),
                 QStringLiteral("pair_line") };
    } else if (noteTemplate == s_roundNote) {
        return { QStringLiteral("round_line"),
                 QStringLiteral("table_line"),
                 QStringLiteral("pair_line") };
    } else {
        return {};
    }
}

QVector<QString> DrawNotesGenerator::textsToRemove() const
{
    const auto noteTemplate = m_noteTemplate->currentData().toString();
    if (noteTemplate == s_emptyNote) {
        return { QStringLiteral("table_placeholder"),
                 QStringLiteral("pair_placeholder") };
    } else if (noteTemplate == s_emptyRoundNote) {
        return { QStringLiteral("round_placeholder"),
                 QStringLiteral("table_placeholder"),
                 QStringLiteral("pair_placeholder") };
    } else {
        return {};
    }
}

QString DrawNotesGenerator::defaultBaseName() const
{
    return tr("Auslosungszettel");
}

QString DrawNotesGenerator::tipsText() const
{
    return tr(
        "<p>Es werden 8 DIN-A7-Zettel pro Seite generiert, in 2 Spalten und 4 Zeilen.</p>"
        "<p>Einfach alle Seiten eines Stapels aufeinanderlegen und mit einem Papierschneider "
        "schneiden. Die einzelnen Haufen sind dann schon in der richtigen Reihenfolge und müssen "
        "nur noch aufeinander gelegt werden (links oben ist ganz oben und rechts unten ist ganz "
        "unten). Die Zettel-Haufen werden einach in der Reihenfolge der Stapel aufeinander gelegt "
        "(Stapel 1 ist ganz oben, der letzte ganz unten).<p>"
        "<p>Am Schluss sind dann alle Zettel von Tisch 1, Paar 1 und ggf. Spieler 1 bis zum "
        "letzten Tisch, Paar 2 und ggf. Spieler 2 sortiert.</p>"
        "<p>Abhängig vom Drucker müssen die Abstandseinstellungen evtl. etwas angepasst werden, "
        "damit alle Zettel genau in der Mitte ausgegeben werden.</p>"
    );
}

AbstractNotesGenerator::Error DrawNotesGenerator::generate(const QString &fileName)
{
    const auto error = startPdf(fileName);
    if (error != AbstractNotesGenerator::NoError) {
        return error;
    }

    const auto notesPerPair = m_twoNotesPerPair->isChecked() ? 2 : 1;
    const auto allNotes = m_tables->value() * 2 * notesPerPair;

    QProgressDialog progress(tr("Erstelle PDF-Datei …"), tr("Abbrechen"), 0, allNotes, this);
    progress.setWindowModality(Qt::WindowModal);
    progress.setMinimumDuration(1000);

    const auto pagesPerStack = m_pagesPerStack->value();
    const auto pairFormat = m_pairFormat->currentData().toString();

    // Prepare there data for all notes we want to render
    QVector<int> tableData;
    tableData.reserve(allNotes);
    QVector<int> pairData;
    pairData.reserve(allNotes);
    QVector<int> playerData;
    playerData.reserve(allNotes);
    for (int table = 0; table < m_tables->value(); table++) {
        for (int pair = 0; pair < 2; pair++) {
            for (int player = 0; player < notesPerPair; player++) {
                tableData.append(table + 1);
                pairData.append(pair + 1);
                playerData.append(player + 1);
            }
        }
    }

    int page = 0;
    int stack = 0;
    int col = 0;
    int row = 0;
    int renderedNotes = 0;

    while (true) {
        for (int i = 0; i < 8; i++) {
            col = (i % 2);
            row = (i / 2);

            const auto index = stack * pagesPerStack * 8 + page % pagesPerStack + i * pagesPerStack;
            if (index >= allNotes) {
                continue;
            }

            QString displayPair;
            if (pairFormat == s_movingPairs) {
                displayPair = pairData.value(index) == 1 ? tr("1 (bleibt sitzen)")
                                                         : tr("2 (rutscht weiter)");
            } else if (pairFormat == s_pairAndPlayer) {
                displayPair = tr("%1 (Spieler %2)").arg(QString::number(pairData.value(index)),
                                                        QString::number(playerData.value(index)));
            } else { // pairFormat == s_onlyPair
                displayPair = QString::number(pairData.value(index));
            }

            const QHash<QString, QString> data {
                { QStringLiteral("@round@"), QString::number(m_round->value()) },
                { QStringLiteral("@table@"), QString::number(tableData.value(index)) },
                { QStringLiteral("@pair@"), displayPair }
            };

            renderNote(data, col, row);

            progress.setValue(++renderedNotes);
            if (progress.wasCanceled()) {
                finishPdf();
                return AbstractNotesGenerator::Aborted;
            }
        }

        const auto nextPage = page + 1;
        const auto nextStack = nextPage % pagesPerStack != 0 ? stack : stack + 1;
        if (nextStack * pagesPerStack * 8 + nextPage % pagesPerStack >= allNotes) {
            break;
        }

        page++;
        stack += (page % pagesPerStack == 0);
        newPage();
    }

    finishPdf();
    return AbstractNotesGenerator::NoError;
}

void DrawNotesGenerator::saveSettings()
{
    m_settings->saveNotesSettings(noteType(), Json::serialize(QJsonObject {
        { s_noteTemplate,    m_noteTemplate->currentData().toString() },
        { s_tables,          m_tables->value() },
        { s_round,           m_round->value() },
        { s_twoNotesPerPair, m_twoNotesPerPair->isChecked() },
        { s_pairFormat,      m_pairFormat->currentData().toString() },
        { s_pagesPerStack,   m_pagesPerStack->value() }
    }));
}

void DrawNotesGenerator::setBestStackSize(int tables)
{
    const auto allNotes = tables * 2 * (m_twoNotesPerPair->isChecked() ? 2 : 1);
    int bestStackSize = -1;

    if (allNotes <= 8) {
        bestStackSize = 1;

    } else if (allNotes <= 16) {
        bestStackSize = 2;

    } else {
        int bestEmptyNotes = -1;

        for (const auto pagesPerStack : { 3, 4, 5 }) {
            int page = 0;
            int stack = 0;
            int emptyNotes = 0;

            while (true) {
                for (int i = 0; i < 8; i++) {
                    if (stack * pagesPerStack * 8 + page % pagesPerStack + i * pagesPerStack
                        >= allNotes) {

                        emptyNotes++;
                    }
                }

                const auto nextPage = page + 1;
                const auto nextStack = nextPage % pagesPerStack != 0 ? stack : stack + 1;
                if (nextStack * pagesPerStack * 8 + nextPage % pagesPerStack >= allNotes) {
                    break;
                }

                page++;
                stack += (page % pagesPerStack == 0);
            }

            if (bestStackSize == -1 || emptyNotes < bestEmptyNotes) {
                bestStackSize = pagesPerStack;
                bestEmptyNotes = emptyNotes;
            }
        }
    }

    m_pagesPerStack->setValue(bestStackSize);
}

void DrawNotesGenerator::checkEnableRound(int index)
{
    m_roundLabel->setEnabled(index > 1);
    m_round->setEnabled(index > 1);
}
