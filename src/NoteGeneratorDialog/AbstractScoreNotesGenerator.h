// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef ABSTRACTSCORENOTESGENERATOR_H
#define ABSTRACTSCORENOTESGENERATOR_H

// Local includes

#include "AbstractNotesGenerator.h"

#include "shared/NoteType.h"

// Local classes
class SharedObjects;
class SharedNoteSettingsWidget;
class Settings;

// Qt classes
class QSpinBox;
class QComboBox;

class AbstractScoreNotesGenerator : public AbstractNotesGenerator
{
    Q_OBJECT

public:
    explicit AbstractScoreNotesGenerator(SharedObjects *sharedObjects,
                                         NoteType::Type type,
                                         SharedNoteSettingsWidget *sharedSettings,
                                         QWidget *parent = nullptr);
    virtual QString tabTitle() const = 0;
    QString defaultBaseName() const;
    AbstractNotesGenerator::Error generate(const QString &fileName);
    void saveSettings();

protected: // Functions
    QString svgSource() const;
    QVector<QString> pathsToRemove() const;
    QVector<QString> textsToRemove() const;
    virtual int boogers() const = 0;

private: // Variables
    Settings *m_settings;
    QComboBox *m_noteTemplate;
    QSpinBox *m_tables;
    QSpinBox *m_rounds;

};

#endif // ABSTRACTSCORENOTESGENERATOR_H
