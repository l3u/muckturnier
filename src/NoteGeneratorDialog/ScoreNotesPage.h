// SPDX-FileCopyrightText: 2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SCORENOTESPAGE_H
#define SCORENOTESPAGE_H

// Local includes
#include "AbstractNotesPage.h"

// Local classes
class SharedObjects;
class Settings;
class SharedNoteSettingsWidget;
class AbstractNotesGenerator;

// Qt classes
class QSpinBox;
class QStackedLayout;

class ScoreNotesPage : public AbstractNotesPage
{
    Q_OBJECT

public:
    explicit ScoreNotesPage(SharedObjects *sharedObjects,
                            SharedNoteSettingsWidget *sharedNoteSettingsWidget,
                            QWidget *parent = nullptr);
    QString tabTitle() const;
    AbstractNotesGenerator *generator() const;
    void saveSettings();

private: // Variables
    Settings *m_settings;
    QSpinBox *m_boogers;
    QStackedLayout *m_layout;

};

#endif // SCORENOTESPAGE_H
