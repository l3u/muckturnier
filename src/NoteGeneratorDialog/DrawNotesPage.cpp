// SPDX-FileCopyrightText: 2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "DrawNotesPage.h"
#include "DrawNotesGenerator.h"

// Qt includes
#include <QVBoxLayout>

DrawNotesPage::DrawNotesPage(SharedObjects *sharedObjects,
                             SharedNoteSettingsWidget *sharedNoteSettingsWidget,
                             QWidget *parent)
    : AbstractNotesPage(parent)
{
    m_generator = new DrawNotesGenerator(sharedObjects, sharedNoteSettingsWidget);
    auto *layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->addWidget(m_generator);
    layout->addStretch();
}

QString DrawNotesPage::tabTitle() const
{
    return tr("Auslosungszettel");
}

AbstractNotesGenerator *DrawNotesPage::generator() const
{
    return m_generator;
}

void DrawNotesPage::saveSettings()
{
    m_generator->saveSettings();
}
