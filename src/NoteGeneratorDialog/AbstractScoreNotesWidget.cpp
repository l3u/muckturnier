// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "AbstractScoreNotesWidget.h"
#include "SpacingWidget.h"

#include "shared/DefaultValues.h"
#include "shared/SharedStyles.h"
#include "shared/Json.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/Settings.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QLabel>
#include <QGroupBox>
#include <QSpinBox>
#include <QGridLayout>
#include <QJsonObject>
#include <QProgressDialog>
#include <QComboBox>

static const QLatin1String s_noteTemplate("note template");
static const QLatin1String s_tables      ("tables");
static const QLatin1String s_rounds      ("rounds");

AbstractScoreNotesWidget::AbstractScoreNotesWidget(SharedObjects *sharedObjects,
                                                   NoteType::Type type,
                                                   SharedNoteSettingsWidget *sharedSettings,
                                                   QWidget *parent)
    : AbstractNotesGenerator(type,
                             sharedObjects,
                             sharedSettings,
                             parent),
      m_settings(sharedObjects->settings())
{
    auto *box = new QGroupBox(tr("Spielstandzettel-spezifische Einstellungen"));
    box->setStyleSheet(SharedStyles::normalGroupBoxTitle);
    auto *boxLayout = new QGridLayout(box);
    layout()->addWidget(box);

    boxLayout->addWidget(new QLabel(tr("Vorlage für die Spielstandzettel:")), 0, 0);
    m_noteTemplate = new QComboBox;
    boxLayout->addWidget(m_noteTemplate, 0, 1);

    boxLayout->addWidget(new QLabel(tr("Anzahl Tische:")), 1, 0);
    m_tables = new QSpinBox;
    m_tables->setMinimum(1);
    m_tables->setMaximum(DefaultValues::maximumNotesTables);
    boxLayout->addWidget(m_tables, 1, 1);

    boxLayout->addWidget(new QLabel(tr("Anzahl Runden:")), 2, 0);
    m_rounds = new QSpinBox;
    m_rounds->setMinimum(1);
    m_rounds->setMaximum(DefaultValues::maximumNotesRounds);
    boxLayout->addWidget(m_rounds, 2, 1);

    layout()->addStretch();
}

void AbstractScoreNotesWidget::addTemplate(const QString &text, const QString &id)
{
    m_noteTemplate->addItem(text, id);
}

void AbstractScoreNotesWidget::restoreSettings()
{
    const auto settings = Json::objectFromString(m_settings->notesSettings(noteType()));

    const auto noteTemplate = settings.value(s_noteTemplate);
    if (noteTemplate.type() == QJsonValue::String) {
        const auto index = m_noteTemplate->findData(noteTemplate.toString());
        if (index != -1) {
            m_noteTemplate->setCurrentIndex(index);
        }
    }

    const auto tables = settings.value(s_tables);
    m_tables->setValue(tables.type() != QJsonValue::Double ? 12 : tables.toInt());

    const auto rounds = settings.value(s_rounds);
    m_rounds->setValue(rounds.type() != QJsonValue::Double ? 5 : rounds.toInt());
}

QString AbstractScoreNotesWidget::defaultBaseName() const
{
    return tr("Spielstandzettel");
}

QString AbstractScoreNotesWidget::svgSource() const
{
    const auto noteTemplate = m_noteTemplate->currentData().toString();

    if (   noteTemplate == AbstractScoreNotesWidget::note
        || noteTemplate == AbstractScoreNotesWidget::noteNoTable
        || noteTemplate == AbstractScoreNotesWidget::noteEmpty) {

        return boogers() == 2 ? QStringLiteral("score_note.svg")
                              : QStringLiteral("score_note_3_boogers.svg");

    } else if (   noteTemplate == AbstractScoreNotesWidget::noteVertical
               || noteTemplate == AbstractScoreNotesWidget::noteNoTableVertical
               || noteTemplate == AbstractScoreNotesWidget::noteEmptyVertical) {

        return boogers() == 2 ? QStringLiteral("score_note_vertical.svg")
                              : QStringLiteral("score_note_3_boogers_vertical.svg");
    } else {
        Q_UNREACHABLE();
        return QString();
    }
}

QVector<QString> AbstractScoreNotesWidget::pathsToRemove() const
{
    const auto noteTemplate = m_noteTemplate->currentData().toString();

    if (   noteTemplate == AbstractScoreNotesWidget::note
        || noteTemplate == AbstractScoreNotesWidget::noteVertical) {

        return { QStringLiteral("round_line"),
                 QStringLiteral("table_line") };

    } else if (   noteTemplate == AbstractScoreNotesWidget::noteNoTable
               || noteTemplate == AbstractScoreNotesWidget::noteNoTableVertical) {

        return { QStringLiteral("round_line") };

    } else { // noteTemplate == NoteTemplates::ScoreNoteEmpty
        return {};
    }
}

QVector<QString> AbstractScoreNotesWidget::textsToRemove() const
{
    const auto noteTemplate = m_noteTemplate->currentData().toString();

    if (   noteTemplate == AbstractScoreNotesWidget::note
        || noteTemplate == AbstractScoreNotesWidget::noteVertical) {

        return {};

    } else if (   noteTemplate == AbstractScoreNotesWidget::noteNoTable
               || noteTemplate == AbstractScoreNotesWidget::noteNoTableVertical) {

        return { QStringLiteral("table_placeholder") };

    } else { // noteTemplate == NoteTemplates::ScoreNoteEmpty
        return { QStringLiteral("round_placeholder"),
                 QStringLiteral("table_placeholder") };
    }
}

AbstractNotesGenerator::Error AbstractScoreNotesWidget::generate(const QString &fileName)
{
    const auto error = startPdf(fileName);
    if (error != AbstractNotesGenerator::NoError) {
        return error;
    }

    const int tables = m_tables->value();
    const int rounds = m_rounds->value();

    const int notesPerPage = cols() * rows();

    int allNotes = 0;
    if (tables < notesPerPage) {
        allNotes = notesPerPage * rounds;
    } else {
        const auto missingTables = tables % notesPerPage;
        if (missingTables > 0) {
            allNotes = (tables + (notesPerPage - missingTables)) * rounds;
        } else {
            allNotes = tables * rounds;
        }
    }

    int col = -1;
    int row = 0;
    int note = 0;

    QProgressDialog progress(tr("Erstelle PDF-Datei …"), tr("Abbrechen"), 0, allNotes, this);
    progress.setWindowModality(Qt::WindowModal);
    progress.setMinimumDuration(1000);

    for (int firstOnPage = 1; firstOnPage <= tables; firstOnPage += notesPerPage) {
        for (int round = 1; round <= rounds; round++) {
            for (int table = firstOnPage; table <= firstOnPage + notesPerPage - 1; table++) {
                note++;
                col++;
                if (col == cols()) {
                    col = 0;
                    row++;
                }

                if (table > tables) {
                    continue;
                }

                const QHash<QString, QString> data {
                    { QStringLiteral("@round@"), QString::number(round) },
                    { QStringLiteral("@table@"), QString::number(table) }
                };
                renderNote(data, col, row);

                progress.setValue(note);
                if (progress.wasCanceled()) {
                    finishPdf();
                    return AbstractNotesGenerator::Aborted;
                }
            }

            if (note < allNotes) {
                newPage();
                col = -1;
                row = 0;
            }
        }
    }

    finishPdf();
    return AbstractNotesGenerator::NoError;
}

void AbstractScoreNotesWidget::saveSettings()
{
    m_settings->saveNotesSettings(noteType(), Json::serialize(QJsonObject {
        { s_noteTemplate, m_noteTemplate->currentData().toString() },
        { s_tables,       m_tables->value() },
        { s_rounds,       m_rounds->value() }
    }));
}
