// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef TABLENUMBERSGENERATOR_H
#define TABLENUMBERSGENERATOR_H

// Local includes
#include "AbstractNotesGenerator.h"

// Local classes
class SharedObjects;
class SharedNoteSettingsWidget;
class Settings;

// Qt classes
class QSpinBox;

class TableNumbersGenerator : public AbstractNotesGenerator
{
    Q_OBJECT

public:
    explicit TableNumbersGenerator(SharedObjects *sharedObjects,
                                   SharedNoteSettingsWidget *sharedSettings,
                                   QWidget *parent = nullptr);
    QString defaultBaseName() const;
    AbstractNotesGenerator::Error generate(const QString &fileName);
    void saveSettings();

protected:
    QPageLayout::Orientation orientation() const;
    int cols() const;
    int rows() const;
    QString svgSource() const;
    QVector<QString> pathsToRemove() const;
    QVector<QString> textsToRemove() const;
    QString tipsText() const;

private: // Variables
    Settings *m_settings;
    QSpinBox *m_firstSticker;
    QSpinBox *m_firstNumber;
    QSpinBox *m_lastNumber;

};

#endif // TABLENUMBERSGENERATOR_H
