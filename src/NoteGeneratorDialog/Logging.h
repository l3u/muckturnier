// SPDX-FileCopyrightText: 2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef LOGGING_NOTEGENERATOR_H
#define LOGGING_NOTEGENERATOR_H

#include <QLoggingCategory>

Q_DECLARE_LOGGING_CATEGORY(NoteGeneratorLog)

#endif // LOGGING_NOTEGENERATOR_H
