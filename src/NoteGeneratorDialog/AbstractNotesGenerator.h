// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef ABSTRACTNOTESGENERATOR_H
#define ABSTRACTNOTESGENERATOR_H

// Local includes
#include "shared/NoteType.h"

// Qt includes
#include <QWidget>
#include <QPageLayout>
#include <QHash>

// Local classes
class SharedNoteSettingsWidget;
class SpacingWidget;
class SharedObjects;
class ResourceFinder;

// Qt classes
class QVBoxLayout;
class QPdfWriter;
class QSvgRenderer;
class QPainter;

class AbstractNotesGenerator : public QWidget
{
    Q_OBJECT

public: // Enums
    enum Error {
        NoError,
        Aborted,
        FileNotFound,
        FileNotReadable,
        FileNotParseable
    };

public:
    const QString &currentSvgFile() const;
    virtual QString defaultBaseName() const = 0;
    virtual Error generate(const QString &fileName) = 0;
    virtual void saveSettings() = 0;

protected:
    explicit AbstractNotesGenerator(NoteType::Type type,
                                    SharedObjects *sharedObjects,
                                    SharedNoteSettingsWidget *sharedSettings,
                                    QWidget *parent = nullptr);

    virtual QPageLayout::Orientation orientation() const = 0;
    virtual int cols() const = 0;
    virtual int rows() const = 0;
    virtual QString svgSource() const = 0;
    virtual QVector<QString> pathsToRemove() const = 0;
    virtual QVector<QString> textsToRemove() const = 0;
    virtual QString tipsText() const = 0;

    NoteType::Type noteType() const;
    QVBoxLayout *layout() const;
    Error startPdf(const QString &pdfFile);
    void renderNote(const QHash<QString, QString> &data, int col, int row);
    void newPage();
    void finishPdf();

private: // Functions
    QString prepareSvgData(QString svgData, const QHash<QString, QString> &data);

private: // Variables
    const NoteType::Type m_noteType;
    ResourceFinder *m_resourceFinder;
    SharedNoteSettingsWidget *m_sharedSettings;

    QVBoxLayout *m_layout;
    SpacingWidget *m_spacingWidget;

    QString m_currentSvgFile;

    QPdfWriter *m_pdfWriter = nullptr;
    double m_colWidth = 0.0;
    double m_rowHeight = 0.0;

    QSvgRenderer *m_svgRenderer = nullptr;
    QPainter *m_painter = nullptr;
    QSizeF m_renderSize;
    QString m_svgData;

    QPointF m_outerSpacing;
    double m_innerSpacingXMm = 0.0;
    double m_innerSpacingYMm = 0.0;

};

#endif // ABSTRACTNOTESGENERATOR_H
