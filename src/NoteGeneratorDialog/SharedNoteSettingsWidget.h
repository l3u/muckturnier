// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SHAREDNOTESETTINGSWIDGET_H
#define SHAREDNOTESETTINGSWIDGET_H

// Qt includes
#include <QWidget>

// Local classes
class Settings;

// Qt classes
class QLineEdit;
class QFontComboBox;

class SharedNoteSettingsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SharedNoteSettingsWidget(Settings *settings, QWidget *parent = nullptr);
    QString title() const;
    QString subTitle() const;
    QString fontFamily() const;
    void saveSettings();

private: // Functions
    void setFontFamily(const QString &text);

private: // Variables
    Settings *m_settings;
    QLineEdit *m_title;
    QLineEdit *m_subTitle;
    QFontComboBox *m_fontFamily;

};

#endif // SHAREDNOTESETTINGSWIDGET_H
