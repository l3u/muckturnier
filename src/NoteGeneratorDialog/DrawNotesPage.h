// SPDX-FileCopyrightText: 2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef DRAWNOTESPAGE_H
#define DRAWNOTESPAGE_H

// Local includes
#include "AbstractNotesPage.h"

// Local classes
class SharedObjects;
class SharedNoteSettingsWidget;
class AbstractNotesGenerator;

class DrawNotesPage : public AbstractNotesPage
{
    Q_OBJECT

public:
    explicit DrawNotesPage(SharedObjects *sharedObjects,
                           SharedNoteSettingsWidget *sharedNoteSettingsWidget,
                           QWidget *parent = nullptr);
    QString tabTitle() const;
    AbstractNotesGenerator *generator() const;
    void saveSettings();

private: // Variables
    AbstractNotesGenerator *m_generator;

};

#endif // DRAWNOTESPAGE_H
