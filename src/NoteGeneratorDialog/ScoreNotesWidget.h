// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SCORENOTESWIDGET_H
#define SCORENOTESWIDGET_H

// Local includes
#include "AbstractScoreNotesWidget.h"

// Local classes
class SharedObjects;
class SharedNoteSettingsWidget;

// Qt classes
class QComboBox;
class QSpinBox;

class ScoreNotesWidget : public AbstractScoreNotesWidget
{
    Q_OBJECT

public:
    explicit ScoreNotesWidget(SharedObjects *sharedObjects,
                              SharedNoteSettingsWidget *sharedSettings,
                              QWidget *parent = nullptr);
    QString tabTitle() const;

protected:
    QPageLayout::Orientation orientation() const;
    int cols() const;
    int rows() const;
    QString tipsText() const;
    int boogers() const;

};

#endif // SCORENOTESWIDGET_H
