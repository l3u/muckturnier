// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SPACINGWIDGET_H
#define SPACINGWIDGET_H

// Local includes
#include "shared/NoteType.h"

// Qt includes
#include <QWidget>

// Local classes
class Settings;

// Qt classes
class QDoubleSpinBox;

class SpacingWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SpacingWidget(NoteType::Type type, Settings *settings,
                           QWidget *parent = nullptr);
    double outerX() const;
    double outerY() const;
    double innerX() const;
    double innerY() const;

private: // Functions
    QDoubleSpinBox *mmSpinBox() const;
    void setValues(const QVector<double> &values);

private: // Variables
    const NoteType::Type m_type;
    Settings *m_settings;
    QDoubleSpinBox *m_outerX;
    QDoubleSpinBox *m_outerY;
    QDoubleSpinBox *m_innerX;
    QDoubleSpinBox *m_innerY;

};

#endif // SPACINGWIDGET_H
