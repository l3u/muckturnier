// SPDX-FileCopyrightText: 2018-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "ExportPlayersDialog.h"

#include "Database/Database.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/StringTools.h"
#include "SharedObjects/SearchEngine.h"
#include "SharedObjects/TournamentSettings.h"

#include "shared/Markers.h"
#include "shared/SharedStyles.h"

// Qt includes

#include <QDebug>
#include <QGroupBox>
#include <QVBoxLayout>
#include <QCheckBox>
#include <QFile>
#include <QApplication>
#include <QDir>
#include <QListWidget>
#include <QSplitter>
#include <QLabel>
#include <QTimer>

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
#include <QTextCodec>
#endif

// C++ includes
#include <utility>

ExportPlayersDialog::ExportPlayersDialog(QWidget *parent, SharedObjects *sharedObjects)
    : FileDialog(parent, sharedObjects->database()),
      m_db(sharedObjects->database()),
      m_tournamentSettings(sharedObjects->tournamentSettings())
{
    // Dialog settings

    setTitle(m_tournamentSettings->isPairMode() ? tr("Paarliste exportieren")
                                                : tr("Spielerliste exportieren"));
    setOkButtonText(tr("Exportieren"));

    setTargetFileBaseName(m_tournamentSettings->isPairMode() ? tr("Paarliste")
                                                             : tr("Spielerliste"));
    setTargetFileExtension(QStringLiteral("txt"));
    setTargetFileDescription(tr("Textdateien"));
    setFileDialogTitle(m_tournamentSettings->isPairMode()
        ? tr("Bitte einen Dateinamen für die Paarliste auswählen")
        : tr("Bitte einen Dateinamen für die Spielerliste auswählen"));
    QGroupBox *selectionBox = new QGroupBox(m_tournamentSettings->isPairMode()
                                                ? tr("Paarauswahl")
                                                : tr("Spielerauswahl"));
    QVBoxLayout *selectionBoxLayout = new QVBoxLayout(selectionBox);
    QListWidget *markersList = new QListWidget;
    // Plasma 6's Breeze refuses to draw a frame here, cf. KDE Bug #499131
    // This is a workaround to make that specific style draw a frame anyway:
    markersList->setProperty("_breeze_force_frame", true);
    connect(markersList, &QListWidget::itemChanged, this, &ExportPlayersDialog::selectionChanged);
    selectionBoxLayout->addWidget(markersList);

    QGroupBox *previewBox = new QGroupBox(m_tournamentSettings->isPairMode()
                                              ? tr("Exportierte Paare")
                                              : tr("Exportierte Spieler"));
    QVBoxLayout *previewBoxLayout = new QVBoxLayout(previewBox);
    m_exportPreview = new QListWidget;
    m_exportPreview->setSelectionMode(QListWidget::NoSelection);
    m_exportPreview->setFocusPolicy(Qt::NoFocus);
    previewBoxLayout->addWidget(m_exportPreview);

    m_exportCount = new QLabel;
    previewBoxLayout->addWidget(m_exportCount);

    QSplitter *splitter = new QSplitter(this);
    splitter->setHandleWidth(2);
    splitter->setStyleSheet(SharedStyles::visibleHorizontalSplitter);
    splitter->setChildrenCollapsible(false);

    splitter->addWidget(selectionBox);
    splitter->addWidget(previewBox);
    splitter->setStretchFactor(0, 1);
    splitter->setStretchFactor(1, 2);

    layout()->addWidget(splitter);

    m_openAfterExport = new QCheckBox(tr("Datei nach dem Export öffnen"));
    m_openAfterExport->setChecked(true);
    layout()->addWidget(m_openAfterExport);

    // Get the data

    const auto [ playersData, success1 ] = m_db->playersData();
    if (! success1) {
        QTimer::singleShot(0, this, &QDialog::reject);
        return;
    }

    QStringList names;
    QHash<QString, int> nameIndex;
    for (int i = 0; i < playersData.count(); i++) {
        const auto &name = playersData.at(i).name;
        nameIndex[name] = i;
        names.append(name);
    }

    const auto [ markers, success2 ] = m_db->markers();
    if (! success2) {
        QTimer::singleShot(0, this, &QDialog::reject);
        return;
    }

    QHash<int, int> markerIndex;
    for (int i = 0; i < markers.count(); i++) {
        markerIndex[markers.at(i).id] = i;
    }

    // Fill the lists

    QHash<int, int> markersCount;

    sharedObjects->stringTools()->sort(names);
    for (const auto &name : std::as_const(names)) {
        QListWidgetItem *item = new QListWidgetItem(name);
        const auto marker = playersData.at(nameIndex.value(name)).marker;
        item->setData(Qt::UserRole, marker);
        item->setForeground(markers.at(markerIndex.value(marker)).color);
        markersCount[marker]++;
        m_exportPreview->addItem(item);
    }

    for (const auto &marker : markers) {
        const auto name = QStringLiteral("%1 (%2)").arg(
                                         marker.name.isEmpty() ? tr("(unmarkiert)") : marker.name,
                                         QString::number(markersCount.value(marker.id)));
        auto *item = new QListWidgetItem(name);
        item->setForeground(marker.color);
        item->setData(Qt::UserRole, marker.id);
        item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
        if (markersCount.value(marker.id) > 0) {
            item->setCheckState(Qt::Checked);
        } else {
            item->setCheckState(Qt::Unchecked);
            item->setFlags(item->flags() & ~Qt::ItemIsEnabled);
        }
        markersList->addItem(item);
    }

    selectionChanged(markersList->item(0));
}

void ExportPlayersDialog::selectionChanged(QListWidgetItem *changedItem)
{
    int markerId = changedItem->data(Qt::UserRole).toInt();
    bool hidden = changedItem->checkState() == Qt::Unchecked;
    int count = 0;
    for(int i = 0; i < m_exportPreview->count(); i++) {
        QListWidgetItem *nameItem = m_exportPreview->item(i);
        if (nameItem->data(Qt::UserRole).toInt() == markerId) {
            nameItem->setHidden(hidden);
            if (! hidden) {
                count++;
            }
        } else {
            if (! nameItem->isHidden()) {
                count++;
            }
        }
    }

    m_exportCount->setText(m_tournamentSettings->isPairMode()
                               ? tr("%n Paar(e) ausgewählt", "", count)
                               : tr("%n Spieler ausgewählt", "", count));
    setOkButtonEnabled(count > 0);
}

void ExportPlayersDialog::accept()
{
    const QString fileName = getTargetFileName(m_db->dbPath());
    if (fileName.isEmpty()) {
        return;
    }

    QFile playersListFile(fileName);
    playersListFile.open(QIODevice::WriteOnly | QIODevice::Text);

    QTextStream outStream(&playersListFile);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    outStream.setCodec(QTextCodec::codecForName("UTF-8"));
#endif

    Q_EMIT updateStatus(m_tournamentSettings->isPairMode() ? tr("Exportiere Paarliste …")
                                                           : tr("Exportiere Spielerliste …"));
    QApplication::setOverrideCursor(Qt::WaitCursor);

    for(int i = 0; i < m_exportPreview->count(); i++) {
        QListWidgetItem *item = m_exportPreview->item(i);
        if (item->isHidden()) {
            continue;
        }
        outStream << item->text() + QStringLiteral("\n");
    }

    playersListFile.close();
    QApplication::restoreOverrideCursor();
    Q_EMIT updateStatus(m_tournamentSettings->isPairMode()
        ? tr("Paarliste in Datei „%2“ exportiert").arg(QDir::toNativeSeparators(fileName))
        : tr("Spielerliste in Datei „%2“ exportiert").arg( QDir::toNativeSeparators(fileName)));
    successMessage(m_tournamentSettings->isPairMode() ? tr("Spielerlisten-Export")
                                                      : tr("Paarlisten-Export"),
                   m_tournamentSettings->isPairMode()
                       ? tr("Die Paarliste wurde erfolgreich in die Datei „%2“ exportiert!").arg(
                            QDir::toNativeSeparators(fileName))
                       : tr("Die Spielerliste wurde erfolgreich in die Datei „%2“ exportiert!").arg(
                            QDir::toNativeSeparators(fileName)));
    if (m_openAfterExport->isChecked()) {
        openFile(fileName);
    }

    QDialog::accept();
}
