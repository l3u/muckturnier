// SPDX-FileCopyrightText: 2020 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "RadioButtonLabel.h"

// Qt includes
#include <QRadioButton>
#include <QMouseEvent>

RadioButtonLabel::RadioButtonLabel(const QString &text, QRadioButton *buddy, QWidget *parent)
    : QLabel(text, parent), m_buddy(buddy)
{
    setWordWrap(true);
}

void RadioButtonLabel::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() != Qt::LeftButton) {
        return;
    }

    m_buddy->setChecked(true);
    m_buddy->setFocus();
}
