// SPDX-FileCopyrightText: 2022 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef EXPORT_H
#define EXPORT_H

namespace Export
{

enum UnimportantGoalsHandling {
    DisplayUnimportantGoals,
    BraceUnimportantGoals,
    HideUnimportantGoals
};

}

#endif // EXPORT_H
