// SPDX-FileCopyrightText: 2022-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "AbstractExporter.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/TournamentSettings.h"
#include "SharedObjects/StringTools.h"

// Qt includes
#include <QDebug>

AbstractExporter::AbstractExporter(const ExportSettings *exportSettings,
                                   SharedObjects *sharedObjects, QObject *parent)
    : QObject(parent),
      m_db(sharedObjects->database()),
      m_tournamentSettings(sharedObjects->tournamentSettings()),
      m_stringTools(sharedObjects->stringTools()),
      m_exportSettings(exportSettings),
      m_dateFormat(m_stringTools->dateFormat()),
      m_timeFormat(m_stringTools->timeFormat())
{
}

void AbstractExporter::open()
{
    m_file.setFileName(m_exportSettings->fileName);
    m_file.open(QIODevice::WriteOnly | QIODevice::Text);
    m_stream.setDevice(&m_file);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    m_stream.setCodec(QTextCodec::codecForName("UTF-8"));
#endif
}

void AbstractExporter::close()
{
    m_file.close();
}

QString AbstractExporter::displayScore(int score) const
{
    return m_exportSettings->displayBoogers ? m_stringTools->boogify(score)
                                            : QString::number(score);
}

QVector<AbstractExporter::GoalsData> AbstractExporter::prepareGoals(
    const QVector<Database::RankData> &ranking) const
{
    QVector<GoalsData> data;
    data.reserve(ranking.count());

    QVector<bool> goalsImportant;
    QVector<bool> opponentGoalsImportant;
    switch (m_exportSettings->unimportantGoalsHandling) {
    case Export::DisplayUnimportantGoals:
        break;
    case Export::BraceUnimportantGoals:
    case Export::HideUnimportantGoals:
        const auto [ a, b ] = m_db->goalsImportance(ranking);
        goalsImportant = a;
        opponentGoalsImportant = b;
    }

    for (int i = 0; i < ranking.count(); i++) {
        GoalsData entry;
        const auto &currentRank = ranking.at(i);

        switch (m_exportSettings->unimportantGoalsHandling) {
        case Export::DisplayUnimportantGoals:
            entry.goals = QString::number(currentRank.goals);
            entry.opponentGoals = QString::number(currentRank.opponentGoals);
            break;
        case Export::BraceUnimportantGoals:
            entry.goals = goalsImportant.at(i)
                ? QString::number(currentRank.goals)
                : QStringLiteral("(%1)").arg(currentRank.goals);
            entry.opponentGoals = opponentGoalsImportant.at(i)
                ? QString::number(currentRank.opponentGoals)
                : QStringLiteral("(%1)").arg(currentRank.opponentGoals);
            break;
        case Export::HideUnimportantGoals:
            if (goalsImportant.at(i)) {
                entry.goals = QString::number(currentRank.goals);
            }
            if (opponentGoalsImportant.at(i)) {
                entry.opponentGoals = QString::number(currentRank.opponentGoals);
            }
            break;
        }

        data.append(entry);
    }

    return data;
}

QString AbstractExporter::pairName(const Scores::Score &score, int index) const
{
    if (m_tournamentSettings->isPairMode()) {
        return m_stringTools->pairOrPlayerName(score.pairsOrPlayers1.at(index),
                                               StringTools::DisqualifiedName);
    } else {
        return m_stringTools->pairName(score.pairsOrPlayers1.at(index), score.players2.at(index),
                                       StringTools::DisqualifiedName);
    }
}
