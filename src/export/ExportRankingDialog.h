// SPDX-FileCopyrightText: 2022 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef EXPORTRANKINGDIALOG_H
#define EXPORTRANKINGDIALOG_H

// Local includes
#include "shared/TitleDialog.h"

// Local classes
class SharedObjects;

// Qt classes
class QWidget;
class QLineEdit;
class QComboBox;

class ExportRankingDialog : public TitleDialog
{
    Q_OBJECT

public:
    explicit ExportRankingDialog(QWidget *parent, SharedObjects *SharedObjects);

private:
    void accept() override;

private: // Variables
    SharedObjects *m_sharedObjects;
    QLineEdit *m_title;
    QComboBox *m_unimportantGoals;

};

#endif // EXPORTTOURNAMENTDIALOG_H
