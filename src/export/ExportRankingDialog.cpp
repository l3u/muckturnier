// SPDX-FileCopyrightText: 2022-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "ExportRankingDialog.h"
#include "Export.h"
#include "HtmlExporter.h"

#include "Database/Database.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/TournamentSettings.h"
#include "SharedObjects/TemporaryFileHelper.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QLabel>
#include <QGroupBox>
#include <QLineEdit>
#include <QGridLayout>
#include <QComboBox>
#include <QFileInfo>
#include <QDir>
#include <QDesktopServices>
#include <QUrl>
#include <QMessageBox>
#include <QApplication>

ExportRankingDialog::ExportRankingDialog(QWidget *parent, SharedObjects *sharedObjects)
    : TitleDialog(parent),
      m_sharedObjects(sharedObjects)
{
    setTitle(tr("Rangliste drucken"));
    addButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Close);
    setOkButtonText(tr("Drucken (via HTML)"));

    QLabel *label;

    label = new QLabel(tr(
        "Es wird eine temporäre HTML-Datei erzeugt, die dann mit dem Standardwebbrowser geöffnet "
        "und gedruckt werden kann. Beim Schließen des Programms wird die Datei automatisch "
        "gelöscht."));
    label->setWordWrap(true);
    layout()->addWidget(label);

    auto *box = new QGroupBox(tr("Einstellungen"));
    auto *boxLayout = new QGridLayout(box);
    layout()->addWidget(box);

    boxLayout->addWidget(new QLabel(tr("Turniername:")), 0, 0);
    m_title = new QLineEdit(QFileInfo(m_sharedObjects->database()->dbFile()).completeBaseName());
    boxLayout->addWidget(m_title, 0, 1);

    auto *tournamentSettings = m_sharedObjects->tournamentSettings();

    boxLayout->addWidget(new QLabel(tr("Nicht entscheidende Tore:")), 1, 0);
    m_unimportantGoals = new QComboBox;
    m_unimportantGoals->addItem(tr("anzeigen"));
    m_unimportantGoals->addItem(tr("einklammern"));
    m_unimportantGoals->addItem(tr("ausblenden"));
    m_unimportantGoals->setCurrentIndex(tournamentSettings->grayOutUnimportantGoals());
    boxLayout->addWidget(m_unimportantGoals, 1, 1);

    label = new QLabel(tr("Über „Datei“ → „Import und Export“ → „Turnierdaten exportieren“ ist "
                          "der Export aller Turnierdaten (incl. der Rangliste) möglich und "
                          "individuell formatierbar!"));
    label->setWordWrap(true);
    layout()->addWidget(label);
}

void ExportRankingDialog::accept()
{
    QApplication::setOverrideCursor(Qt::WaitCursor);
    Q_EMIT updateStatus(tr("Exportiere die Rangliste …"));

    auto *tmpHelper = m_sharedObjects->temporaryFileHelper();
    const auto [ result, fileName ] = tmpHelper->getFileName(tr("Rangliste.html"));
    if (result != TemporaryFileHelper::FileCreated) {
        QApplication::restoreOverrideCursor();
        tmpHelper->displayErrorMessage(this, result);
        TitleDialog::accept();
        return;
    }

    const QString title = m_title->text().simplified();

    AbstractExporter::ExportSettings exportSettings;
    exportSettings.fileName                 = fileName;
    exportSettings.exportTitle              = true;
    exportSettings.title                    = title.isEmpty() ? tr("Muckturnier") : title;
    exportSettings.exportHeadlines          = true;
    exportSettings.exportSubHeadlines       = true;
    exportSettings.unimportantGoalsHandling = static_cast<Export::UnimportantGoalsHandling>(
                                                  m_unimportantGoals->currentIndex());
    exportSettings.exportRanking            = true;

    HtmlExporter exporter(&exportSettings, m_sharedObjects);
    exporter.save();

    Q_EMIT updateStatus(tr("Rangliste exportiert"));
    QApplication::restoreOverrideCursor();

    if (! QDesktopServices::openUrl(QUrl::fromLocalFile(fileName))) {
        QMessageBox::warning(this, tr("Rangliste drucken"),
            tr("<p><b>Automatisches Öffnen fehlgeschlagen</b></p>"
               "<p>Die exportierte HTML-Datei</p>"
               "<p>%1</p>"
               "<p>konnte nicht automatisch mit dem Standardwebbrowser geöffnet werden. Bitte die "
               "Datei manuell öffnen und drucken.</p>").arg(
               QDir::toNativeSeparators(fileName).toHtmlEscaped()));
    }

    TitleDialog::accept();
}
