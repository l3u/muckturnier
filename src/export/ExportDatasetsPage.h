// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef EXPORTDATASETSPAGE_H
#define EXPORTDATASETSPAGE_H

// Local includes
#include "shared/AbstractSettingsPage.h"

// Qt includes
#include <QSet>
#include <QList>
#include <QPalette>

// Qt classes
class QCheckBox;
class QLabel;
class QLineEdit;

class ExportDatasetsPage : public AbstractSettingsPage
{
    Q_OBJECT

public:
    explicit ExportDatasetsPage(int rounds, bool isPairMode, QWidget *parent = nullptr);
    void saveSettings() override;

    bool exportGenericData() const;
    bool exportRanking() const;
    bool exportDisqualifiedPlayers() const;

    bool exportRoundResults() const;
    QList<int> roundSelection() const;

Q_SIGNALS:
    void checkExportPossible();

private Q_SLOTS:
    void checkRoundSelection(const QString &range);

private: // Functions
    void roundSelectionIsInvalid(const QString &message = QString());

private: // Variables
    const int m_allRounds;
    QPalette m_defaultPalette;
    QPalette m_redTextPalette;

    QCheckBox *m_exportGenericData;

    QCheckBox *m_exportRanking;
    QCheckBox *m_exportDisqualifiedPlayers;

    QString m_allRoundsString;
    QCheckBox *m_exportRoundResults;
    QLineEdit *m_roundSelect;
    QLabel *m_roundSelectLabel;
    QLabel *m_errorLabel;
    bool m_roundSelectionIsRed = false;
    QSet<int> m_selectedRounds;

};

#endif // EXPORTDATASETSPAGE_H
