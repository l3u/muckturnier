// SPDX-FileCopyrightText: 2010-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "ExportTournamentDialog.h"
#include "BasicExportPage.h"
#include "ExportDatasetsPage.h"
#include "ExportFormattingPage.h"
#include "HtmlExporter.h"
#include "TsvExporter.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/Settings.h"
#include "SharedObjects/TournamentSettings.h"

#include "shared/StackedWidgetList.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QDir>
#include <QApplication>

ExportTournamentDialog::ExportTournamentDialog(QWidget *parent, SharedObjects *sharedObjects)
    : FileDialog(parent, sharedObjects->database()),
      m_sharedObjects(sharedObjects)
{
    setTitle(tr("Turnierdaten exportieren"));
    setOkButtonText(tr("Exportieren"));
    setFileDialogTitle(tr("Bitte einen Dateinamen für die Turnierdaten-Exportdatei auswählen"));

    auto *stack = new StackedWidgetList;

    m_basicExportPage = new BasicExportPage(QFileInfo(m_db->dbFile()).completeBaseName());
    stack->addWidget(m_basicExportPage);
    connect(m_basicExportPage, &BasicExportPage::closeAfterExportToggled,
            this, [this](bool state)
            {
                setButtonText(QDialogButtonBox::Cancel, state ? tr("Abbrechen") : tr("Schließen"));
            });
    layout()->addWidget(stack);

    m_exportDatasetsPage = new ExportDatasetsPage(
        m_db->allRounds(), m_sharedObjects->tournamentSettings()->isPairMode());
    connect(m_exportDatasetsPage, &ExportDatasetsPage::checkExportPossible,
            this, &ExportTournamentDialog::checkExportPossible);
    stack->addWidget(m_exportDatasetsPage);

    m_exportFormattingPage = new ExportFormattingPage(sharedObjects->settings()->boogerSymbol());
    stack->addWidget(m_exportFormattingPage);

    stack->setCurrentIndex(0);
}

void ExportTournamentDialog::checkExportPossible()
{
    setOkButtonEnabled(m_exportDatasetsPage->exportGenericData()
                       || m_exportDatasetsPage->exportRanking()
                       || m_exportDatasetsPage->exportDisqualifiedPlayers()
                       || m_exportDatasetsPage->exportRoundResults());
}

void ExportTournamentDialog::accept()
{
    AbstractExporter::ExportSettings exportSettings;

    auto title = m_basicExportPage->title();
    if (! title.isEmpty()) {
        setTargetFileBaseName(title);
        exportSettings.title = title;
    } else {
        setTargetFileBaseName(tr("Protokoll"));
        exportSettings.title = tr("Protokoll");
    }

    const auto fileFormat = m_basicExportPage->fileFormat();

    QString message;
    switch (fileFormat) {
    case BasicExportPage::FileFormat::HTML:
        setTargetFileExtension(QStringLiteral("htm html"));
        setTargetFileDescription(tr("HTML-Dateien"));
        message = tr("Exportiere HTML-Daten …");
        break;
    case BasicExportPage::FileFormat::CSV:
        setTargetFileExtension(QStringLiteral("tsv"));
        setTargetFileDescription(tr("TSV-Dateien"));
        message = tr("Exportiere TSV-Daten …");
        break;
    }

    QString journalFileName = getTargetFileName(m_db->dbPath());
    if (journalFileName.isEmpty()) {
        return;
    }

    exportSettings.fileName                  = journalFileName;
    exportSettings.exportTitle               = m_basicExportPage->exportTitle();
    exportSettings.exportGenericData         = m_exportDatasetsPage->exportGenericData();
    exportSettings.exportHeadlines           = m_exportFormattingPage->exportHeadlines();
    exportSettings.exportSubHeadlines        = m_exportFormattingPage->exportSubHeadlines();
    exportSettings.displayBoogers            = m_exportFormattingPage->displayBoogers();
    exportSettings.unimportantGoalsHandling  = m_exportFormattingPage->unimportantGoalsHandling();
    exportSettings.exportRanking             = m_exportDatasetsPage->exportRanking();
    exportSettings.exportDisqualifiedPlayers = m_exportDatasetsPage->exportDisqualifiedPlayers();
    exportSettings.exportRoundResults        = m_exportDatasetsPage->exportRoundResults();
    exportSettings.roundSelection            = m_exportDatasetsPage->roundSelection();

    Q_EMIT updateStatus(message);
    QApplication::setOverrideCursor(Qt::WaitCursor);

    AbstractExporter *exporter;
    switch (fileFormat) {
    case BasicExportPage::FileFormat::HTML:
        exporter = new HtmlExporter(&exportSettings, m_sharedObjects);
        break;
    case BasicExportPage::FileFormat::CSV:
        exporter = new TsvExporter(&exportSettings, m_sharedObjects);
        break;
    }
    exporter->save();
    exporter->deleteLater();

    QApplication::restoreOverrideCursor();
    Q_EMIT updateStatus(tr("Daten in Datei „%1“ exportiert").arg(
                           QDir::toNativeSeparators(journalFileName)));

    successMessage(tr("Turnierdaten exportieren"),
                   tr("Die Daten wurden erfolgreich in die Datei „%1“ exportiert.").arg(
                      QDir::toNativeSeparators(journalFileName)));

    if (m_basicExportPage->openAfterExport()) {
        openFile(journalFileName);
    }

    if (m_basicExportPage->closeAfterExport()) {
        QDialog::accept();
    }
}
