// SPDX-FileCopyrightText: 2022-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef ABSTRACTEXPORTER_H
#define ABSTRACTEXPORTER_H

// Local includes

#include "Export.h"

#include "Database/Database.h"

// Qt includes

#include <QObject>
#include <QString>
#include <QList>

#include <QFile>
#include <QTextStream>
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
#include <QTextCodec>
#endif

// Local classes

class SharedObjects;
class TournamentSettings;
class StringTools;

namespace Scores
{
struct Score;
}

class AbstractExporter : public QObject
{
    Q_OBJECT

public:
    struct ExportSettings {
        QString fileName;
        bool exportTitle = false;
        QString title;
        bool exportGenericData = false;
        bool exportHeadlines = false;
        bool exportSubHeadlines = false;
        bool displayBoogers = false;
        Export::UnimportantGoalsHandling unimportantGoalsHandling = Export::DisplayUnimportantGoals;
        bool exportRanking = false;
        bool exportDisqualifiedPlayers = false;
        bool exportRoundResults = false;
        QList<int> roundSelection;
    };

    explicit AbstractExporter(const ExportSettings *exportSettings, SharedObjects *sharedObjects,
                              QObject *parent);
    virtual void save() = 0;

protected: // Structs
    struct GoalsData
    {
        QString goals;
        QString opponentGoals;
    };

protected: // Functions
    void open();
    void close();
    QString displayScore(int score) const;
    QVector<GoalsData> prepareGoals(const QVector<Database::RankData> &ranking) const;
    QString pairName(const Scores::Score &score, int index) const;

protected: // Variables
    Database *m_db;
    TournamentSettings *m_tournamentSettings;
    StringTools *m_stringTools;
    const ExportSettings *m_exportSettings;
    const QString m_dateFormat;
    const QString m_timeFormat;
    QFile m_file;
    QTextStream m_stream;

};

#endif // ABSTRACTEXPORTER_H
