// SPDX-FileCopyrightText: 2020 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef RADIOBUTTONLABEL_H
#define RADIOBUTTONLABEL_H

// Qt includes
#include <QLabel>

// Qt classes
class QRadioButton;

class RadioButtonLabel : public QLabel
{
    Q_OBJECT

public:
    explicit RadioButtonLabel(const QString &text, QRadioButton *buddy, QWidget *parent = nullptr);

protected:
    void mouseReleaseEvent(QMouseEvent *event) override;

private: // Variables
    QRadioButton *m_buddy;

};

#endif // RADIOBUTTONLABEL_H
