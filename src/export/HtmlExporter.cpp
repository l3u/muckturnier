// SPDX-FileCopyrightText: 2022-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "HtmlExporter.h"

#include "SharedObjects/TournamentSettings.h"

#include "shared/Urls.h"

#include "version.h"

// Qt includes
#include <QDebug>

static QString s_style = QStringLiteral(R"END(
table {
    border-spacing: 0;
}

th, td {
    padding: 0.2em 1em 0.2em;
    text-align: left;
}

tr.line td {
    border-top: 1px solid black;
}

@media print {

    body {
        width: 100%;
        margin: 0;
        font: 12pt serif;
    }

    @page {
        margin: 1.5cm 2cm;
    }

    @page:first {
        margin-top: 1cm;
    }

    h1, h2, h3 {
        page-break-after: avoid;
        page-break-inside: avoid;
    }

    tr.line td {
        border-top: 0.1pt solid black;
    }

}

)END");

HtmlExporter::HtmlExporter(const AbstractExporter::ExportSettings *exportSettings,
                           SharedObjects *sharedObjects, QObject *parent)
    : AbstractExporter(exportSettings, sharedObjects, parent)
{
}

void HtmlExporter::save()
{
    open();

    // Add the HTML header
    m_stream << "<!DOCTYPE html>\n\n"
             << "<html lang=\"de\">\n\n"
             << "<head>\n\n"
             << "<meta charset=\"utf-8\">\n\n"
             << "<title>" << m_exportSettings->title.toHtmlEscaped() << "</title>\n\n"
             << "<style>\n"
             << s_style
             << "</style>\n\n"
             << "</head>\n\n"
             << "<body>\n\n";

    if (m_exportSettings->exportTitle) {
        m_stream << "<h1>" << m_exportSettings->title.toHtmlEscaped() << "</h1>\n\n";
    }

    if (m_exportSettings->exportGenericData) {
        // Add tournament meta data

        m_stream << "<section>\n\n";

        if (m_exportSettings->exportHeadlines) {
            m_stream << "<h2>" << tr("Allgemeine Daten") << "</h2>\n\n";
        }
        m_stream << "<p>" << tr("Protokoll erstellt am %1 um %2 Uhr.").arg(
                                 QDateTime::currentDateTime().toString(m_dateFormat),
                                 QDateTime::currentDateTime().toString(m_timeFormat)) << "</p>\n\n";

        if (m_exportSettings->exportSubHeadlines) {
            m_stream << "<h3>Muckturnier</h3>\n\n";
        }

        m_stream << "<table>\n";
        m_stream << "<tr><td>" << tr("Homepage:") << "</td>"
                 << "<td><a href=\"" << Urls::homepage << "\">" << Urls::homepage << "</a></td>"
                 << "</tr>\n";
        if (QStringLiteral(MT_VERSION) == m_tournamentSettings->muckturnierVersion()) {
            m_stream << "<tr>"
                     << "<td>" << tr("Turnierdatenbank und Protokoll erstellt mit:") << "</td>"
                     << "<td>Version " << MT_VERSION << "</td>"
                     << "</tr>\n";
        } else {
            m_stream << "<tr><td>" << tr("Turnierdatenbank erstellt mit:")
                     << "</td><td>" << tr("Version ")
                     << m_tournamentSettings->muckturnierVersion() << "</td></tr>\n"
                     << "<tr><td>" << tr("Protokoll erstellt mit:")
                     << "</td><td>" << tr("Version ")
                     << MT_VERSION << "</td></tr>\n";
        }
        m_stream << "</table>\n\n";

        if (m_exportSettings->exportSubHeadlines) {
            m_stream << "<h3>" << tr("Turnier") << "</h3>\n\n";
        }

        m_stream << "<table>\n"
                 << "<tr><td>" << tr("Turniertyp:") << "</td>"
                 << "<td>" << (m_tournamentSettings->isPairMode() ? tr("Feste Paare")
                                                                   : tr("Einzelne Spieler"))
                 << "</td></tr>\n"
                 << "<tr><td>" << tr("Punkte pro Bobbl:") << "</td>"
                 << "<td>" << QString::number(m_tournamentSettings->boogerScore())
                 << "</td></tr>\n"
                 << "<tr><td>" << tr("Bobbl pro Runde:") << "</td>"
                 << "<td>" << QString::number(m_tournamentSettings->boogersPerRound())
                 << "</td></tr>\n"
                 << "<tr class=\"line\"><td>"
                 << (m_tournamentSettings->isPairMode() ? tr("Teilnehmende Paare:")
                                                        : tr("Teilnehmende Spieler:"))
                 << "</td>"
                 << "<td>" << QString::number(m_db->pairsOrPlayersCount())
                 << "</td></tr>\n"
                 << "<tr><td>" << tr("Gespielte Runden:") << "</td>"
                 << "<td>" << QString::number(m_db->allRounds()) << "</td></tr>\n"
                 << "</table>\n\n";

        m_stream << "</section>\n\n";
    }

    if (m_exportSettings->exportRanking) {
        // Add the ranking

        m_stream << "<section>\n\n";

        if (m_exportSettings->exportHeadlines) {
            m_stream << "<h2>" << tr("Rangliste nach %n Runde(n)", "", m_db->allRounds())
                     << "</h2>\n\n";
        }
        m_stream << "<table>\n"
                 << "<thead>\n"
                 << "<tr>"
                 << "<th>" << tr("Platz") << "</th>"
                 << "<th>" << (m_tournamentSettings->isPairMode() ? tr("Paar")
                                                                   : tr("Spieler")) << "</th>"
                 << "<th>" << tr("Bobbl") << "</th>"
                 << "<th>" << tr("Tore") << "</th>";
        if (m_tournamentSettings->includeOpponentGoals()) {
            m_stream << "<th>" << tr("Gegn. Tore") << "</th>";
        }
        m_stream << "</tr>\n</thead>\n"
                 << "<tbody>\n";

        int lastRank = -1;
        QString style;

        const auto [ ranking, success ] = m_db->ranking(-1);
        if (! success) {
            return;
        }

        const auto goalsData = prepareGoals(ranking);

        for (int i = 0; i < ranking.count(); i++) {
            const auto &currentRank = ranking.at(i);
            const auto &goals = goalsData.at(i);

            if (currentRank.rank != lastRank) {
                style = QStringLiteral(" class=\"line\"");
                lastRank = currentRank.rank;
            } else {
                style.clear();
            }

            m_stream << "<tr" << style << "><td>" << QString::number(currentRank.rank) << "</td>";
            m_stream << "<td>" << currentRank.name << "</td>";
            m_stream << "<td>" << currentRank.boogers << "</td>";
            m_stream << "<td>" << goals.goals << "</td>";
            if (m_tournamentSettings->includeOpponentGoals()) {
                m_stream << "<td>" << goals.opponentGoals << "</td>";
            }
            m_stream << "</tr>\n";
        }
        m_stream << "</tbody>\n"
                 << "</table>\n\n";

        m_stream << "</section>\n\n";
    }

    if (m_exportSettings->exportDisqualifiedPlayers) {
        // Add disqualified players

        const auto [ disqualifiedPlayers, success2 ] = m_db->disqualifiedPlayers(-1);
        if (! success2) {
            return;
        }

        if (! disqualifiedPlayers.isEmpty()) {
            m_stream << "<section>\n\n";

            if (m_exportSettings->exportHeadlines) {
                m_stream << "<h2>Disqualifizierte "
                         << (m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler"))
                         << "</h2>\n\n";
            }

            m_stream << "<table>\n";

            QString line;

            QMap<int, QVector<QString>>::const_iterator it = disqualifiedPlayers.constBegin();

            while (it != disqualifiedPlayers.constEnd()) {
                m_stream << QStringLiteral("<tr%1>").arg(line) << "<td>"
                         << tr("Runde %1:").arg(QString::number(it.key()))
                         << "</td>";
                const auto names = it.value();
                m_stream << "<td>" << names.at(0).toHtmlEscaped() << "</td></tr>\n";
                if (names.count() > 1) {
                    for (int i = 1; i < names.count(); i++) {
                        m_stream << "<tr><td></td><td>" << names.at(i).toHtmlEscaped()
                                 << "</td></tr>\n";
                    }
                }
                it++;

                if (line.isEmpty()) {
                    line = QStringLiteral(" class=\"line\"");
                }
            }
            m_stream << "</table>\n\n";

            m_stream << "</section>\n\n";
        }
    }

    if (m_exportSettings->exportRoundResults) {
        m_stream << "<section>\n\n";

        if (m_exportSettings->exportHeadlines) {
            m_stream << "<h2>" << tr("Einzelergebnisse") << "</h2>\n\n";
        }

        switch (m_tournamentSettings->scoreType()) {
        case Tournament::HorizontalScore:
            saveHorizontalRoundResults();
            break;
        case Tournament::VerticalScore:
            saveVerticalRoundResults();
            break;
        }

        m_stream << "</section>\n\n";
    }

    // Close HTML
    m_stream << "</body>\n\n"
             << "</html>\n";

    close();
}

void HtmlExporter::saveHorizontalRoundResults()
{
    const int boogersPerRound = m_tournamentSettings->boogersPerRound();

    for (const int round : m_exportSettings->roundSelection) {
        if (m_exportSettings->exportSubHeadlines) {
            m_stream << "<h3>" << tr("Spielergebnisse der %1. Runde").arg(round) << "</h3>\n\n";
        }

        m_stream << "<table>\n<thead>\n"
                 << "<tr><th>" << tr("Tisch") << "</th><th>" << tr("Paar") << "</th>";

        if (boogersPerRound > 1) {
            for (int booger = 1; booger <= boogersPerRound; booger++) {
                m_stream << "<th>" << tr("%1. Bobbl").arg(booger) << "</th>";
            }
        } else {
            m_stream << "<th>" << tr("Ergebnis") << "</th>";
        }

        m_stream << "</tr>\n"
                 << "</thead>\n<tbody>\n";

        const auto [ score, success ] = m_db->scores(round);
        if (! success) {
            return;
        }

        for (int i = 0; i < score.scores.count(); i++) {
            // Table numbers / top lines
            if (i % 2 == 0) {
                m_stream << "<tr class=\"line\">"
                         << "<td rowspan=\"2\">" << score.tables.at(i / 2) << "</td>";
            } else {
                m_stream << "<tr>";
            }

            // Pair names
            m_stream << "<td>" << pairName(score, i).toHtmlEscaped() << "</td>";

            // Results
            const auto &pairResults = score.scores.at(i);
            for (const int result : pairResults) {
                m_stream << "<td>" << displayScore(result) << "</td>";
            }

            m_stream << "</tr>\n";
        }

        m_stream << "</tbody>\n" << "</table>\n\n";
    }
}

void HtmlExporter::saveVerticalRoundResults()
{
    const int boogersPerRound = m_tournamentSettings->boogersPerRound();

    for (const int round : m_exportSettings->roundSelection) {
        if (m_exportSettings->exportSubHeadlines) {
            m_stream << "<h3>" << tr("Spielergebnisse der %1. Runde").arg(round) << "</h3>\n\n";
        }

        m_stream << "<table>\n<thead>\n"
                 << "<tr>"
                 << "<th>" << tr("Tisch") << "</th>";

        if (boogersPerRound > 1) {
            m_stream << "<th>" << tr("Bobbl") << "</th>";
        }

        m_stream << "<th>" << tr("Paar 1") << "</th>"
                 << "<th>" << tr("Paar 2") << "</th>"
                 << "</tr>\n"
                 << "</thead>\n<tbody>\n";

        const auto [ score, success ] = m_db->scores(round);
        if (! success) {
            return;
        }

        for (int i = 0; i < score.scores.count(); i += 2) {
            m_stream << "<tr class=\"line\">";

            // Table numbers
            m_stream << "<td rowspan=\"" << (boogersPerRound + 1) << "\">"
                     << score.tables.at(i / 2) << "</td>";

            // Pair names
            if (boogersPerRound > 1) {
                m_stream << "<td></td>";
            }
            m_stream << "<td>" << pairName(score, i).toHtmlEscaped() << "</td>"
                     << "<td>" << pairName(score, i + 1).toHtmlEscaped() << "</td>"
                     << "</tr>";

            for (int booger = 1; booger <= boogersPerRound; booger++) {
                m_stream << "<tr>";

                if (boogersPerRound > 1) {
                    // Booger label
                    m_stream << "<td>" << booger << ".</td>";
                }

                // Pair results
                m_stream << "<td>" << displayScore(score.scores.at(i).at(booger - 1)) << "</td>"
                         << "<td>" << displayScore(score.scores.at(i + 1).at(booger - 1)) << "</td>"
                         << "</tr>\n";
            }
        }

        m_stream << "</tbody>\n" << "</table>\n\n";
    }
}
