// SPDX-FileCopyrightText: 2022 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef TSVEXPORTER_H
#define TSVEXPORTER_H

// Local includes
#include "AbstractExporter.h"

class TsvExporter : public AbstractExporter
{
    Q_OBJECT

public:
    explicit TsvExporter(const AbstractExporter::ExportSettings *exportSettings,
                         SharedObjects *sharedObjects,
                         QObject *parent = nullptr);
    void save();

private: // Functions
    void saveHorizontalRoundResults();
    void saveVerticalRoundResults();

};

#endif // TSVEXPORTER_H
