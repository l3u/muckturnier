// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "ExportDatasetsPage.h"

// Qt includes
#include <QVBoxLayout>
#include <QLabel>
#include <QCheckBox>
#include <QLineEdit>
#include <QRegularExpression>
#include <QToolButton>
#include <QApplication>
#include <QStyle>

// C++ includes
#include <algorithm>

ExportDatasetsPage::ExportDatasetsPage(int rounds, bool isPairMode, QWidget *parent)
    : AbstractSettingsPage(tr("Datensätze"), parent), m_allRounds(rounds)
{
    m_defaultPalette.setColor(QPalette::Text, palette().color(QPalette::WindowText));
    m_redTextPalette.setColor(QPalette::Text, Qt::red);

    auto *header = new QLabel(tr("Folgende Datensätze können als Abschnitt exportiert werden\n"
                                 "(mindestens einer muss ausgewählt sein):"));
    header->setWordWrap(true);
    layout()->addWidget(header);

    m_exportGenericData = new QCheckBox(tr("Allgemeine Daten (Datum, Anzahl Teilnehmer etc.)"));
    m_exportGenericData->setChecked(true);
    layout()->addWidget(m_exportGenericData);
    connect(m_exportGenericData, &QCheckBox::toggled,
            this, &ExportDatasetsPage::checkExportPossible);

    m_exportRanking = new QCheckBox(tr("Rangliste zum Turnierende"));
    m_exportRanking->setChecked(true);
    layout()->addWidget(m_exportRanking);
    connect(m_exportRanking, &QCheckBox::toggled, this, &ExportDatasetsPage::checkExportPossible);

    m_exportDisqualifiedPlayers = new QCheckBox(tr("Disqualifizierte %1 (sofern vorhanden)").arg(
                                                   isPairMode ? tr("Paare") : tr("Spieler")));
    m_exportDisqualifiedPlayers->setChecked(true);
    layout()->addWidget(m_exportDisqualifiedPlayers);
    connect(m_exportDisqualifiedPlayers, &QCheckBox::toggled,
            this, &ExportDatasetsPage::checkExportPossible);

    m_exportRoundResults = new QCheckBox(tr("Einzelergebnisse der folgenden Runden\n"
                                            "(es wurde(n) %n Runde(n) gespielt)", "", m_allRounds));
    m_exportRoundResults->setChecked(true);
    layout()->addWidget(m_exportRoundResults);
    connect(m_exportRoundResults, &QCheckBox::toggled,
            this, &ExportDatasetsPage::checkExportPossible);

    auto *roundSelectLayout = new QHBoxLayout;
    layout()->addLayout(roundSelectLayout);

    roundSelectLayout->addSpacing(30);
    m_roundSelect = new QLineEdit;
    m_roundSelect->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    m_allRoundsString = m_allRounds == 1 ? QStringLiteral("1")
                                         : QStringLiteral("1-%1").arg(m_allRounds);
    m_roundSelect->setText(m_allRoundsString);
    checkRoundSelection(m_roundSelect->text());
    roundSelectLayout->addWidget(m_roundSelect);
    connect(m_roundSelect, &QLineEdit::textChanged, this, &ExportDatasetsPage::checkRoundSelection);

    auto *resetButton = new QToolButton;
    resetButton->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogResetButton));
    resetButton->setToolTip(tr("Alle Runden auswählen"));
    connect(resetButton, &QToolButton::clicked,
            this, [this]
            {
                m_roundSelect->setText(m_allRoundsString);
            });
    roundSelectLayout->addWidget(resetButton);

    m_roundSelectLabel = new QLabel(tr("Beispiel: „1, 3, 7-9, 11“"));
    roundSelectLayout->addWidget(m_roundSelectLabel);

    m_errorLabel = new QLabel;
    layout()->addWidget(m_errorLabel);
    m_errorLabel->hide();

    layout()->addStretch();
}

void ExportDatasetsPage::saveSettings()
{
}

bool ExportDatasetsPage::exportGenericData() const
{
    return m_exportGenericData->isChecked();
}

bool ExportDatasetsPage::exportRanking() const
{
    return m_exportRanking->isChecked();
}

bool ExportDatasetsPage::exportDisqualifiedPlayers() const
{
    return m_exportDisqualifiedPlayers->isChecked();
}

bool ExportDatasetsPage::exportRoundResults() const
{
    return m_exportRoundResults->isChecked();
}

void ExportDatasetsPage::checkRoundSelection(const QString &range)
{
    m_selectedRounds.clear();

    // No input: range is not okay, but set the default color
    if (range.isEmpty()) {
        roundSelectionIsInvalid();
        m_roundSelect->setPalette(m_defaultPalette);
        m_errorLabel->hide();
        return;
    }

    // Remove all whitespace
    const QString parsedRange = range.simplified().remove(QLatin1String(" "));

    // Search for input other than 0-9, "," and "-"
    const QRegularExpression check(QStringLiteral("([^\\d,-]+)"));
    const QRegularExpressionMatch match = check.match(parsedRange);
    if (match.hasMatch()) {
        roundSelectionIsInvalid(tr("Ungültige Eingabe: „%1“").arg(match.captured()));
        return;
    }

    // Process all parts

    const auto parsedRangeSplit = parsedRange.split(QLatin1String(","));
    for (const QString &part : parsedRangeSplit) {

        // Single round
        if (! part.contains(QLatin1String("-"))) {
            // Skip empty parts
            if (part.isEmpty()) {
                continue;
            }

            // Check the requested round
            const int requestedRound = part.toInt();
            if (requestedRound > 0 && requestedRound <= m_allRounds) {
                m_selectedRounds.insert(requestedRound);
            } else {
                roundSelectionIsInvalid(tr("Ungültige Rundenzahl: %1").arg(requestedRound));
                return;
            }

        // Range of rounds
        } else {
            const QStringList givenRange = part.split(QLatin1String("-"));

            // We have to find exacly two parts
            if (givenRange.count() != 2) {
                roundSelectionIsInvalid(tr("Ungültige Intervallangabe: „%1“").arg(part));
                return;
            }

            // About to enter, e. g. "1-", handle like a single round entry
            if (givenRange.at(1).isEmpty()) {
                const int requestedRound = givenRange.at(0).toInt();
                if (requestedRound > 0 && requestedRound <= m_allRounds) {
                    m_selectedRounds.insert(requestedRound);
                } else {
                    roundSelectionIsInvalid(tr("Ungültige Rundenzahl: „%1“").arg(requestedRound));
                    return;
                }

            // True range entered
            } else {
                const int fromRound = givenRange.at(0).toInt();
                const int toRound = givenRange.at(1).toInt();

                // Check the range
                if (fromRound == 0 || fromRound > m_allRounds
                    || toRound == 0 || toRound > m_allRounds
                    || fromRound > toRound) {

                    roundSelectionIsInvalid(tr("Ungültiges Intervall: „%1“").arg(part));
                    return;
                }

                // Add all rounds between
                for (int requestedRound = fromRound; requestedRound <= toRound; requestedRound++) {
                    m_selectedRounds.insert(requestedRound);
                }
            }
        }
    }

    if (m_roundSelectionIsRed) {
        m_roundSelect->setPalette(m_defaultPalette);
        m_roundSelectionIsRed = false;
        m_errorLabel->hide();
    }

    m_exportRoundResults->setEnabled(true);
    m_exportRoundResults->setChecked(true);

    checkExportPossible();
}

void ExportDatasetsPage::roundSelectionIsInvalid(const QString &message)
{
    if (! message.isEmpty()) {
        m_errorLabel->show();
        m_errorLabel->setText(message);
    }
    m_roundSelect->setPalette(m_redTextPalette);
    m_roundSelectionIsRed = true;
    m_exportRoundResults->setEnabled(false);
    m_exportRoundResults->setChecked(false);
    checkExportPossible();
}

QList<int> ExportDatasetsPage::roundSelection() const
{
    auto ordered = QList<int>(m_selectedRounds.begin(), m_selectedRounds.end());
    std::sort(ordered.begin(), ordered.end());
    return ordered;
}
