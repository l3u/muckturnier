// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef EXPORTPLAYERSDIALOG_H
#define EXPORTPLAYERSDIALOG_H

// Local includes
#include "shared/FileDialog.h"

// Local classes
class SharedObjects;
class Database;
class TournamentSettings;

// Qt Classes
class QListWidget;
class QCheckBox;
class QListWidgetItem;
class QLabel;

class ExportPlayersDialog : public FileDialog
{
    Q_OBJECT

public:
    explicit ExportPlayersDialog(QWidget *parent, SharedObjects *sharedObjects);

private Q_SLOTS:
    void accept() override;
    void selectionChanged(QListWidgetItem *changedItem);

private: // Variables
    Database *m_db;
    TournamentSettings *m_tournamentSettings;

    QStringList m_playersList;
    QListWidget *m_exportPreview;
    QCheckBox *m_openAfterExport;
    QLabel *m_exportCount;

};

#endif // EXPORTPLAYERSDIALOG_H
