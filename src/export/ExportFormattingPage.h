// SPDX-FileCopyrightText: 2020-2022 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef EXPORTFORMATTINGPAGE_H
#define EXPORTFORMATTINGPAGE_H

// Local includes

#include "Export.h"

#include "shared/AbstractSettingsPage.h"

// Qt classes
class QCheckBox;
class QRadioButton;

class ExportFormattingPage : public AbstractSettingsPage
{
    Q_OBJECT

public:
    explicit ExportFormattingPage(const QString &boogerSymbol, QWidget *parent = nullptr);
    void saveSettings() override;

    bool exportHeadlines() const;
    bool exportSubHeadlines() const;
    bool displayBoogers() const;
    Export::UnimportantGoalsHandling unimportantGoalsHandling() const;

private: // Variables
    QCheckBox *m_exportHeadlines;
    QCheckBox *m_exportSubHeadlines;
    QCheckBox *m_displayBoogers;
    QRadioButton *m_displayUnimportantGoals;
    QRadioButton *m_braceUnimportantGoals;
    QRadioButton *m_hideUnimportantGoals;

};

#endif // EXPORTFORMATTINGPAGE_H
