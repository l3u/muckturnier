// SPDX-FileCopyrightText: 2010-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef EXPORTTOURNAMENTDIALOG_H
#define EXPORTTOURNAMENTDIALOG_H

// Local includes
#include "shared/FileDialog.h"

// Qt classes
class QTextStream;

// Local classes
class SharedObjects;
class BasicExportPage;
class ExportDatasetsPage;
class ExportFormattingPage;

class ExportTournamentDialog : public FileDialog
{
    Q_OBJECT

public:
    explicit ExportTournamentDialog(QWidget *parent, SharedObjects *SharedObjects);

private Q_SLOTS:
    void accept() override;
    void checkExportPossible();

private: // Variables
    SharedObjects *m_sharedObjects;
    BasicExportPage *m_basicExportPage;
    ExportDatasetsPage *m_exportDatasetsPage;
    ExportFormattingPage *m_exportFormattingPage;

};

#endif // EXPORTTOURNAMENTDIALOG_H
