// SPDX-FileCopyrightText: 2022-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "TsvExporter.h"

#include "SharedObjects/TournamentSettings.h"

#include "shared/Urls.h"

#include "version.h"

// Qt includes
#include <QDebug>

TsvExporter::TsvExporter(const AbstractExporter::ExportSettings *exportSettings,
                         SharedObjects *sharedObjects, QObject *parent)
    : AbstractExporter(exportSettings, sharedObjects, parent)
{
}

void TsvExporter::save()
{
    open();

    if (m_exportSettings->exportTitle) {
        m_stream << m_exportSettings->title << "\n\n";
    }

    if (m_exportSettings->exportGenericData) {
        // Add tournament meta data

        if (m_exportSettings->exportHeadlines) {
            m_stream << tr("Allgemeine Daten") << "\n\n";
        }

        m_stream << tr("Protokoll erstellt am %1 um %2 Uhr.").arg(
                       QDateTime::currentDateTime().toString(m_dateFormat),
                       QDateTime::currentDateTime().toString(m_timeFormat))
                 << "\n\n";

        if (m_exportSettings->exportSubHeadlines) {
            m_stream << "Muckturnier\n\n";
        }

        m_stream << tr("Homepage:") << "\t" << Urls::homepage << "\n";
        if (QStringLiteral(MT_VERSION) == m_tournamentSettings->muckturnierVersion()) {
            m_stream << tr("Turnierdatenbank und Protokoll erstellt mit: \tVersion ")
                     << MT_VERSION << "\n";
        } else {
            m_stream << tr("Turnierdatenbank erstellt mit:\tVersion ")
                     << m_tournamentSettings->muckturnierVersion() << "\n"
                     << tr("Protokoll erstellt mit:\tVersion ")
                     << MT_VERSION << "\n";
        }
        m_stream << "\n";

        if (m_exportSettings->exportSubHeadlines) {
            m_stream << tr("Turnier") << "\n\n";
        }

        m_stream << tr("Turniertyp:")
                 << "\t" << (m_tournamentSettings->isPairMode() ? tr("Feste Paare")
                                                                : tr("Einzelne Spieler"))
                 << "\n"
                 << tr("Punkte pro Bobbl:")
                 << "\t" << QString::number(m_tournamentSettings->boogerScore()) << "\n"
                 << tr("Bobbl pro Runde:")
                 << "\t" << QString::number(m_tournamentSettings->boogersPerRound()) << "\n"
                 << (m_tournamentSettings->isPairMode() ? tr("Teilnehmende Paare:")
                                                        : tr("Teilnehmende Spieler:"))
                 << "\t" << QString::number(m_db->pairsOrPlayersCount()) << "\n"
                 << tr("Gespielte Runden:") << "\t"
                 << QString::number(m_db->allRounds()) << "\n\n";
    }

    if (m_exportSettings->exportRanking) {
        // Add the ranking

        if (m_exportSettings->exportHeadlines) {
            m_stream << tr("Rangliste nach %n Runde(n)", "", m_db->allRounds()) << "\n\n";
        }
        m_stream << tr("Platz") << "\t"
                 << (m_tournamentSettings->isPairMode() ? tr("Paar") : tr("Spieler")) << "\t"
                 << tr("Bobbl") << "\t"
                 << tr("Tore");
        if (m_tournamentSettings->includeOpponentGoals()) {
            m_stream << "\t" << tr("Gegn. Tore");
        }
        m_stream << "\n";

        const auto [ ranking, success ] = m_db->ranking(-1);
        if (! success) {
            return;
        }

        const auto goalsData = prepareGoals(ranking);

        for (int i = 0; i < ranking.count(); i++) {
            const auto &currentRank = ranking.at(i);
            const auto &goals = goalsData.at(i);

            m_stream << QString::number(currentRank.rank);
            m_stream << "\t" << currentRank.name;
            m_stream << "\t" << QString::number(currentRank.boogers);
            m_stream << "\t" << goals.goals;
            if (m_tournamentSettings->includeOpponentGoals()) {
                m_stream << "\t" << goals.opponentGoals;
            }
            m_stream << "\n";
        }
        m_stream << "\n";
    }

    if (m_exportSettings->exportDisqualifiedPlayers) {
        // Add disqualified players

        const auto [ disqualifiedPlayers, success2 ] = m_db->disqualifiedPlayers(-1);
        if (! success2) {
            return;
        }

        if (! disqualifiedPlayers.isEmpty()) {
            if (m_exportSettings->exportHeadlines) {
                m_stream << "Disqualifizierte "
                         << (m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler"))
                         << "\n\n";
            }

            QMap<int, QVector<QString>>::const_iterator it = disqualifiedPlayers.constBegin();
            while (it != disqualifiedPlayers.constEnd()) {
                m_stream << tr("Runde %1:").arg(QString::number(it.key()));
                for (const auto &names : it.value()) {
                    m_stream << tr("\t%1\n").arg(names);
                }
                it++;
            }
            m_stream << "\n";
        }
    }

    if (m_exportSettings->exportRoundResults) {
        if (m_exportSettings->exportHeadlines) {
            m_stream << tr("Einzelergebnisse") << "\n\n";
        }
        switch (m_tournamentSettings->scoreType()) {
        case Tournament::HorizontalScore:
            saveHorizontalRoundResults();
            break;
        case Tournament::VerticalScore:
            saveVerticalRoundResults();
            break;
        }
    }

    close();
}

void TsvExporter::saveHorizontalRoundResults()
{
    for (const int round : m_exportSettings->roundSelection) {
        if (m_exportSettings->exportSubHeadlines) {
            m_stream << tr("Spielergebnisse der %1. Runde").arg(round) << "\n";
        }

        m_stream << tr("Tisch") << "\t" << tr("Paar");

        if (m_tournamentSettings->boogersPerRound() > 1) {
            for (int booger = 1; booger <= m_tournamentSettings->boogersPerRound(); booger++) {
                m_stream << tr("\t%1. Bobbl").arg(booger);
            }
        } else {
            m_stream << tr("\tErgebnis");
        }

        m_stream << "\n";

        const auto [ score, success ] = m_db->scores(round);
        if (! success) {
            return;
        }

        for (int i = 0; i < score.scores.count(); i += 2) {
            // Table number
            m_stream << score.tables.at(i / 2) << "\t";

            for (int pair = 1; pair <= 2; pair++) {
                // Pair names
                if (pair == 2) {
                    m_stream << "\t";
                }
                m_stream << pairName(score, i + pair - 1);

                // Results
                for (int result : score.scores.at(i + pair - 1)) {
                    m_stream << "\t" << displayScore(result);
                }

                m_stream << "\n";
            }
        }

        m_stream << "\n";
    }
}

void TsvExporter::saveVerticalRoundResults()
{
    const int boogersPerRound = m_tournamentSettings->boogersPerRound();

    for (const int round : m_exportSettings->roundSelection) {
        if (m_exportSettings->exportSubHeadlines) {
            m_stream << tr("Spielergebnisse der %1. Runde").arg(round) << "\n";
        }

        m_stream << tr("Tisch") << "\t";
        if (boogersPerRound > 1) {
            m_stream << tr("Bobbl") << "\t";
        }
        m_stream << tr("Paar 1") << "\t"
                 << tr("Paar 2") << "\n";

        const auto [ score, success ] = m_db->scores(round);
        if (! success) {
            return;
        }

        for (int i = 0; i < score.scores.count(); i += 2) {
            // Table numbers
            m_stream << score.tables.at(i / 2);

            // Pair names
            m_stream << "\t";
            if (boogersPerRound > 1) {
                m_stream << "\t";
            }
            m_stream << pairName(score, i) << "\t"
                     << pairName(score, i + 1) << "\n";

            for (int booger = 1; booger <= boogersPerRound; booger++) {
                m_stream << "\t";

                if (boogersPerRound > 1) {
                    // Booger label
                    m_stream << booger << ".\t";
                }

                // Pair results
                m_stream << displayScore(score.scores.at(i).at(booger - 1)) << "\t"
                         << displayScore(score.scores.at(i + 1).at(booger - 1)) << "\n";
            }
        }

        m_stream << "\n";
    }
}
