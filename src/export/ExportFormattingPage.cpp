// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "ExportFormattingPage.h"

// Qt includes
#include <QVBoxLayout>
#include <QCheckBox>
#include <QGroupBox>
#include <QRadioButton>

ExportFormattingPage::ExportFormattingPage(const QString &boogerSymbol, QWidget *parent)
    : AbstractSettingsPage(tr("Formatierung"), parent)
{
    auto *headlinesBox = new QGroupBox(tr("Überschriften"));
    auto *headlinesLayout = new QVBoxLayout(headlinesBox);
    layout()->addWidget(headlinesBox);

    m_exportHeadlines = new QCheckBox(tr("Überschriften anzeigen "
                                         "(z.\u202FB. „Einzelergebnisse“)"));
    m_exportHeadlines->setChecked(true);
    headlinesLayout->addWidget(m_exportHeadlines);

    m_exportSubHeadlines = new QCheckBox(tr("Unterüberschriften anzeigen "
                                            "(z.\u202FB. „Spielergebnisse der 1. Runde“)"));
    m_exportSubHeadlines->setChecked(true);
    headlinesLayout->addWidget(m_exportSubHeadlines);

    auto *boogersBox = new QGroupBox(tr("Bobbl"));
    auto *boogersLayout = new QVBoxLayout(boogersBox);
    layout()->addWidget(boogersBox);

    m_displayBoogers = new QCheckBox(tr("Bobbl („%1“) anzeigen").arg(boogerSymbol));
    m_displayBoogers->setChecked(true);
    boogersLayout->addWidget(m_displayBoogers);

    auto *goalsBox = new QGroupBox(tr("Nicht entscheidende Tore"));
    auto *goalsLayout = new QVBoxLayout(goalsBox);
    layout()->addWidget(goalsBox);

    m_displayUnimportantGoals = new QRadioButton(tr("Anzeigen"));
    goalsLayout->addWidget(m_displayUnimportantGoals);

    m_braceUnimportantGoals = new QRadioButton(tr("Einklammern"));
    m_braceUnimportantGoals->setChecked(true);
    goalsLayout->addWidget(m_braceUnimportantGoals);

    m_hideUnimportantGoals = new QRadioButton(tr("Ausblenden"));
    goalsLayout->addWidget(m_hideUnimportantGoals);

    layout()->addStretch();
}

void ExportFormattingPage::saveSettings()
{
}

bool ExportFormattingPage::exportHeadlines() const
{
    return m_exportHeadlines->isChecked();
}

bool ExportFormattingPage::exportSubHeadlines() const
{
    return m_exportSubHeadlines->isChecked();
}

bool ExportFormattingPage::displayBoogers() const
{
    return m_displayBoogers->isChecked();
}

Export::UnimportantGoalsHandling ExportFormattingPage::unimportantGoalsHandling() const
{
    if (m_displayUnimportantGoals->isChecked()) {
        return Export::DisplayUnimportantGoals;
    } else if (m_braceUnimportantGoals->isChecked()) {
        return Export::BraceUnimportantGoals;
    } else { // m_hideUnimportantGoals->isChecked()
        return Export::HideUnimportantGoals;
    }
}
