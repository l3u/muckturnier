// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef BASICEXPORTSPAGE_H
#define BASICEXPORTSPAGE_H

// Local includes
#include "shared/AbstractSettingsPage.h"

// Qt classes
class QCheckBox;
class QLineEdit;
class QRadioButton;

class BasicExportPage : public AbstractSettingsPage
{
    Q_OBJECT

public:
    enum FileFormat {
        HTML,
        CSV
    };

    explicit BasicExportPage(const QString &title, QWidget *parent = nullptr);
    void saveSettings() override;

    bool exportTitle() const;
    QString title() const;

    FileFormat fileFormat() const;

    bool openAfterExport() const;
    bool closeAfterExport() const;

Q_SIGNALS:
    void closeAfterExportToggled(bool state);

private Q_SLOTS:
    void checkTitle(const QString &text);

private: // Variables
    QCheckBox *m_exportTitle;
    QLineEdit *m_title;

    QRadioButton *m_exportHTML;
    QRadioButton *m_exportTSV;

    QCheckBox *m_openAfterExport;
    QCheckBox *m_closeAfterExport;

};

#endif // BASICEXPORTSPAGE_H
