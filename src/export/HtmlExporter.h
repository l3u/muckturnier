// SPDX-FileCopyrightText: 2022 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef HTMLEXPORTER_H
#define HTMLEXPORTER_H

// Local includes
#include "AbstractExporter.h"

class HtmlExporter : public AbstractExporter
{
    Q_OBJECT

public:
    explicit HtmlExporter(const AbstractExporter::ExportSettings *exportSettings,
                          SharedObjects *sharedObjects,
                          QObject *parent = nullptr);
    void save();

private: // Functions
    void saveHorizontalRoundResults();
    void saveVerticalRoundResults();

};

#endif // HTMLEXPORTER_H
