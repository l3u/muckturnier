// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "BasicExportPage.h"
#include "RadioButtonLabel.h"

// Qt includes
#include <QVBoxLayout>
#include <QGridLayout>
#include <QGroupBox>
#include <QCheckBox>
#include <QLineEdit>
#include <QRadioButton>
#include <QLabel>

BasicExportPage::BasicExportPage(const QString &title, QWidget *parent)
    : AbstractSettingsPage(tr("Grundeinstellungen"), parent)
{
    auto *fileFormatBox = new QGroupBox(tr("Dateiformat"));
    fileFormatBox->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    auto *fileFormatLayout = new QGridLayout(fileFormatBox);
    layout()->addWidget(fileFormatBox);

    m_exportHTML = new QRadioButton(tr("HTML"));
    m_exportHTML->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    m_exportHTML->setChecked(true);
    fileFormatLayout->addWidget(m_exportHTML, 0, 0, Qt::AlignTop);

    auto *htmlLabel = new RadioButtonLabel(tr(
        "<p>Zum Betrachten mit einem Webbrowser, z.\u202FB. zum Archivieren:<br/>"
        "Es wird eine UTF-8-kodierte HTML-5-Datei erzeugt.</p>"),
        m_exportHTML);
    htmlLabel->setWordWrap(true);
    fileFormatLayout->addWidget(htmlLabel, 0, 1);

    m_exportTSV = new QRadioButton(tr("TSV"));
    m_exportTSV->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    fileFormatLayout->addWidget(m_exportTSV, 1, 0, Qt::AlignTop);

    auto *csvLabel = new RadioButtonLabel(tr(
        "<p>Zum Weiterverarbeiten mit LibreOffice Calc, Excel, einem Script etc.:<br/>"
        "Es wird eine UTF-8-kodierte CSV-Datei mit Tabulatoren als Feldtrennern und ohne "
        "Feldbegrenzer erzeugt, die direkt mit einem Tabellenkalkulationsprogramm geöffnet werden "
        "kann.</p>"),
        m_exportTSV);
    csvLabel->setWordWrap(true);
    fileFormatLayout->addWidget(csvLabel, 1, 1);

    auto *titleBox = new QGroupBox(tr("Titel"));
    auto *titleLayout = new QHBoxLayout(titleBox);
    layout()->addWidget(titleBox);

    m_exportTitle = new QCheckBox(tr("Turniertitel ausgeben:"));
    m_exportTitle->setChecked(true);
    titleLayout->addWidget(m_exportTitle);

    m_title = new QLineEdit;
    m_title->setText(title);
    connect(m_title, &QLineEdit::textEdited, this, &BasicExportPage::checkTitle);
    titleLayout->addWidget(m_title);

    auto *actionsBox = new QGroupBox(tr("Aktionen"));
    auto *actionsLayout = new QVBoxLayout(actionsBox);
    layout()->addWidget(actionsBox);

    m_openAfterExport = new QCheckBox(tr("Datei nach dem Export öffnen"));
    m_openAfterExport->setChecked(true);
    actionsLayout->addWidget(m_openAfterExport);

    m_closeAfterExport = new QCheckBox(tr("Dialog nach dem Export schließen"));
    m_closeAfterExport->setChecked(true);
    connect(m_closeAfterExport, &QCheckBox::toggled,
            this, &BasicExportPage::closeAfterExportToggled);
    actionsLayout->addWidget(m_closeAfterExport);

    layout()->addStretch();

    auto *description = new QLabel(tr(
        "Ohne Änderungen auf den Seiten „Datensätze“ und „Formatierung“ wird ein vollständiges "
        "Protokoll des Turniers mit allen verfügbaren Daten erzeugt."));
    description->setWordWrap(true);
    layout()->addWidget(description);
}

void BasicExportPage::saveSettings()
{
}

void BasicExportPage::checkTitle(const QString &text)
{
    const auto state = ! text.simplified().isEmpty();
    m_exportTitle->setChecked(state);
    m_exportTitle->setEnabled(state);
}

bool BasicExportPage::exportTitle() const
{
    return m_exportTitle->isChecked();
}

QString BasicExportPage::title() const
{
    return m_title->text().simplified();
}

BasicExportPage::FileFormat BasicExportPage::fileFormat() const
{
    if (m_exportHTML->isChecked()) {
        return FileFormat::HTML;
    } else {
        return FileFormat::CSV;
    }
}

bool BasicExportPage::openAfterExport() const
{
    return m_openAfterExport->isChecked();
}

bool BasicExportPage::closeAfterExport() const
{
    return m_closeAfterExport->isChecked();
}
