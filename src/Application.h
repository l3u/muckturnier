// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef APPLICATION_H
#define APPLICATION_H

#include <QApplication>

class Application : public QApplication
{
    Q_OBJECT

public:
    Application(int &argc, char **argv);
    bool event(QEvent *event) override;

Q_SIGNALS:
    void paletteChanged();
#ifdef Q_OS_MACOS
    void fileOpenRequested(const QString &path);
#endif

private: // Functions
    void updateColorMode();

};

#endif // APPLICATION_H
