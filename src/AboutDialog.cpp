// SPDX-FileCopyrightText: 2010-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "AboutDialog.h"

#include "Database/Database.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/ResourceFinder.h"
#include "SharedObjects/TournamentSettings.h"

#include "BookingPage/BookingEngine.h"

#include "shared/SharedStyles.h"
#include "shared/Urls.h"

#include "network/ServerProtocol.h"

#include "version.h"
#include "releaseDate.h"

// Qt includes
#include <QVBoxLayout>
#include <QGroupBox>
#include <QLabel>
#include <QDebug>
#include <QIcon>
#include <QDate>

AboutDialog::AboutDialog(QWidget *parent, SharedObjects *sharedObjects) : TitleDialog(parent)
{
    setWindowTitle(tr("Über Muckturnier"));
    addButtonBox(QDialogButtonBox::Ok);
    mainLayout()->setSizeConstraint(QLayout::SetFixedSize);

    QLabel *currentLabel;
    QGroupBox *currentBox;
    QVBoxLayout *currentLayout;

    const auto releaseDate = QDate::fromString(QLatin1String(MT_RELEASE_DATE), Qt::ISODate);

    currentLayout = new QVBoxLayout;
    currentLayout->setSpacing(0);
    layout()->addLayout(currentLayout);

    // Banner
    currentLabel = new QLabel;
    currentLabel->setPixmap(QPixmap(sharedObjects->resourceFinder()->find(
                                QStringLiteral("banner.png"))));
    currentLabel->setMinimumWidth(currentLabel->pixmap().size().width() * 1.3);
    currentLabel->setAlignment(Qt::AlignCenter);
    currentLabel->setStyleSheet(SharedStyles::headerLogo);
    currentLabel->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Maximum);
    currentLayout->addWidget(currentLabel);

    // Subtitle
    currentLabel = new QLabel(tr("Das Programm für die Turnierleitung"));
    currentLabel->setStyleSheet(SharedStyles::footerLogo);
    currentLabel->setAlignment(Qt::AlignCenter);
    currentLayout->addWidget(currentLabel);

    // Version box

    currentBox = new QGroupBox;
    currentLayout = new QVBoxLayout(currentBox);
    layout()->addWidget(currentBox);

    currentLabel = new QLabel(tr(
        "<p><b>Version %1 (%2)</b></p>"
        "<div style=\"text-align: center;\"><table>"
        "<tr><td>Datenbank:</td><td>Revision %3</td></tr>"
        "<tr><td>Einstellungen:</td><td>Revision %4</td></tr>"
        "<tr><td>Serverprotokoll:</td><td>Revision %5</td></tr>"
        "<tr><td>Anmeldungscodes:</td><td>Revision %6</td></tr>"
        "</table></div>"
        "<p>Kompiliert gegen <a href=\"https://www.qt.io/\">Qt</a> %7</p>").arg(
        QLatin1String(MT_VERSION),
        releaseDate.toString(QLatin1String("dd.MM.yyyy")),
        QString::number(sharedObjects->database()->dbVersion()),
        QString::number(sharedObjects->tournamentSettings()->version()),
        QString::number(ServerProtocol::protocolVersion),
        QString::number(BookingEngine::protocolVersion),
        QLatin1String(QT_VERSION_STR)));

    currentLabel->setAlignment(Qt::AlignCenter);
    currentLabel->setTextInteractionFlags(Qt::TextBrowserInteraction | Qt::TextSelectableByMouse);
    currentLabel->setOpenExternalLinks(true);
    currentLayout->addWidget(currentLabel);

    // License box

    currentBox = new QGroupBox;
    currentLayout = new QVBoxLayout(currentBox);
    layout()->addWidget(currentBox);

    currentLabel = new QLabel(
        tr("<p>Quellcode veröffentlicht unter der<br/>"
           "<a href=\"https://www.gnu.org/licenses/#GPL\">GNU General Public License (GPL)</a></p>"
           "<p>Copyright \u00A9 2010-%1 Tobias Leupold<br/>"
           "<a href=\"%2\">%2</a></p>").arg(QString::number(releaseDate.year()), Urls::homepage));

    currentLabel->setAlignment(Qt::AlignCenter);
    currentLabel->setTextInteractionFlags(Qt::TextBrowserInteraction | Qt::TextSelectableByMouse);
    currentLabel->setOpenExternalLinks(true);
    currentLayout->addWidget(currentLabel);

    // External libraries

    currentBox = new QGroupBox;
    currentLayout = new QVBoxLayout(currentBox);
    layout()->addWidget(currentBox);

    currentLabel = new QLabel(tr(
        "<p>QR-Codes werden mit der unter der "
        "<a href=\"https://opensource.org/license/mit/\">MIT-Lizenz</a><br/>"
        "veröffentlichten Bibliothek "
        "<a href=\"https://www.nayuki.io/page/qr-code-generator-library\">qrcodegen</a> generiert."
        "<br/>"
        "Vielen Dank an Project Nayuki dafür :-)</p>"));
    currentLabel->setAlignment(Qt::AlignCenter);
    currentLabel->setTextInteractionFlags(Qt::TextBrowserInteraction | Qt::TextSelectableByMouse);
    currentLabel->setOpenExternalLinks(true);
    currentLayout->addWidget(currentLabel);
}
