// SPDX-FileCopyrightText: 2018-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "Server.h"
#include "Connection.h"

#include "network/ServerProtocol.h"
#include "network/ScoreHelper.h"
#include "network/StopWatchSync.h"

#include "RegistrationPage/RegistrationPage.h"
#include "RegistrationPage/MarkersWidget.h"

#include "ScorePage/ScorePage.h"

#include "SchedulePage/StopWatchEngine.h"

#include "Database/Database.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/StringTools.h"
#include "SharedObjects/Settings.h"
#include "SharedObjects/TournamentSettings.h"

#include "shared/JsonHelper.h"
#include "shared/Players.h"
#include "shared/ColorHelper.h"
#include "shared/Logging.h"

// Qt includes
#include <QDebug>
#include <QTimer>
#include <QJsonArray>
#include <QApplication>

// C++ includes
#include <functional>
#include <utility>

namespace sp = ServerProtocol;

Server::Server(QObject *parent, int connectTimeout, SharedObjects *sharedObjects,
               RegistrationPage *registrationPage, ScorePage *scorePage,
               StopWatchEngine *stopWatchEngine)
    : QTcpServer(parent),
      m_connectTimeout(connectTimeout),
      m_db(sharedObjects->database()),
      m_settings(sharedObjects->settings()),
      m_tournamentSettings(sharedObjects->tournamentSettings()),
      m_registrationPage(registrationPage),
      m_markersWidget(registrationPage->markersWidget()),
      m_scorePage(scorePage),
      m_stopWatchEngine(stopWatchEngine),
      m_locale(sharedObjects->stringTools()->locale()),

      m_requestHandlers {
          // Registration changes

          { sp::registerPlayers, &Server::processRegisterPlayers },
          { sp::renamePlayers,   &Server::processRenamePlayers },
          { sp::assemblePair,    &Server::processAssemblePair },

          { sp::deletePlayers,         &Server::processDeletePlayers },
          { sp::deleteMarkedPlayers,   &Server::processDeleteMarkedPlayers },
          { sp::deleteAllPlayers,      &Server::processDeleteAllPlayers },

          { sp::setMarker,        &Server::processSetMarker },
          { sp::setListMarker,    &Server::processSetListMarker },
          { sp::removeListMarker, &Server::processRemoveListMarker },

          { sp::setNumber, &Server::processSetNumber },

          { sp::setBookingChecksum, &Server::processSetBookingChecksum },

          { sp::setDisqualified, &Server::processSetDisqualified },

          // Draw changes
          { sp::setDraw,        &Server::processSetDraw },
          { sp::setListDraw,    &Server::processSetListDraw },
          { sp::deleteAllDraws, &Server::processDeleteAllDraws },

          // Markers defintion changes
          { sp::addMarker,    &Server::processAddMarker },
          { sp::deleteMarker, &Server::processDeleteMarker },
          { sp::moveMarker,   &Server::processMoveMaker },
          { sp::editMarker,   &Server::processEditMarker },

          // Special markers changes
          { sp::setBookedMarker,         &Server::processSetBookedMarker },
          { sp::singlesManagementChange, &Server::processSinglesManagementChange },

          // Finish registration
          { sp::finishRegistration, &Server::processFinishRegistration }
      },

      m_singleRequestHandlers {
          // Score comparison
          { sp::scoreRequest,        &Server::processScoreRequest },
          { sp::scoreChecksumChange, &Server::processScoreChecksumChange },

          // Stop watch synchronization
          { sp::stopWatchSynchronization, &Server::processStopWatchSynchronization }
      }
{
}

void Server::setChecksum(const QString *checksum)
{
    m_checksum = checksum;
}

void Server::close()
{
    for (Connection *connection : std::as_const(m_connections)) {
        connection->close();
        removeConnection(connection);
    }
    QTcpServer::close();
}

void Server::incomingConnection(qintptr descriptor)
{
    // Create a new connection for the connection request
    Connection *connection = new Connection(this);
    if (! connection->setSocketDescriptor(descriptor)) {
        connection->deleteLater();
        return;
    }
    connect(connection, &Connection::disconnected,
            this, std::bind(&Server::removeConnection, this, connection));
    connect(connection, &Connection::handshakeResult,
            this, std::bind(&Server::processHandshake, this,
                            connection, std::placeholders::_1, std::placeholders::_2));
    connect(connection, &Connection::clientRequest,
            this, std::bind(&Server::processClientRequest, this, connection,
                            std::placeholders::_1, std::placeholders::_2));
    connect(connection, &AbstractNetworkEngine::protocolError, this, &Server::protocolError);

    // Initiate a handshake

    m_pendingConnections.append(connection);
    Q_EMIT message(tr("Verbindungsanfrage von %1, sende Handshake").arg(connection->ip()));

    // Send greeting message
    connection->sendData(sp::welcomeMessage);

    // Start a concurrent timeout call
    QTimer::singleShot(m_connectTimeout, this,
                       std::bind(&Server::checkHandshakeTimeout, this, connection));
}

void Server::removeConnection(Connection *connection)
{
    if (m_pendingConnections.removeOne(connection)) {
        Q_EMIT message(tr("Verbindung mit %1 beendet").arg(connection->ip()));
    } else if (m_connections.removeOne(connection)) {
        const auto id = connection->id();
        m_idMap.remove(id);
        Q_EMIT message(tr("Verbindung mit %1 beendet").arg(clientString(connection)));
        Q_EMIT statusUpdate(tr("Verbindung mit %1 beendet").arg(clientString(connection)));
        Q_EMIT clientCountChanged(m_connections.count());
        Q_EMIT clientDisconnected(id);
        m_scorePage->removeClientChecksums(id);
    }
    connection->deleteLater();
}

void Server::checkHandshakeTimeout(Connection *connection)
{
    if (m_pendingConnections.contains(connection)) {
        Q_EMIT message(tr("Keine Antwort von %1").arg(connection->ip()));
        removeConnection(connection);
    }
}

void Server::broadcastMessage(QJsonObject &jsonObject, int clientId, const QString &messageText)
{
    Q_EMIT tx();
    jsonObject.insert(sp::checksum, *m_checksum);
    jsonObject.insert(sp::clientId, clientId);
    for (Connection *connection : std::as_const(m_connections)) {
        connection->sendData(jsonObject);
    }
    Q_EMIT message(messageText);
}

void Server::clientMessage(QJsonObject &jsonObject, Connection *connection,
                           const QString &messageText, RequestType requestType)
{
    Q_EMIT tx();

    if (JsonHelper::doChecksumCheck(jsonObject)) {
        jsonObject.insert(sp::checksum, *m_checksum);
    }

    switch (requestType) {
    case Server::ClientRequest:
        jsonObject.insert(sp::clientId, connection->id());
        break;
    case Server::ServerRequest:
        jsonObject.insert(sp::clientId, 0);
        break;
    }

    connection->sendData(jsonObject);

    Q_EMIT message(messageText);
}

int Server::clientCount() const
{
    return m_connections.count();
}

QString Server::clientString(Connection *connection) const
{
    return QStringLiteral("%1 (Client-ID %2)").arg(connection->ip(),
                                                   QString::number(connection->id()));
}

// =========
// Handshake
// =========

void Server::processHandshake(Connection *connection, const QString &phase,
                              const QJsonObject &jsonObject)
{
    if (! m_pendingConnections.contains(connection)) {
        return;
    }

    if (phase == sp::synchronization) {
        Q_EMIT message(tr("Synchronisation von %1 angefragt, sende Daten").arg(connection->ip()));

        const QJsonObject settings {
            { sp::locale,            m_locale },
            { sp::namesSeparator,    m_settings->namesSeparator() },
            { sp::tournamentMode,    static_cast<int>(m_tournamentSettings->tournamentMode()) },
            { sp::boogerScore,       m_tournamentSettings->boogerScore() },
            { sp::boogersPerRound,   m_tournamentSettings->boogersPerRound() },
            { sp::tournamentStarted, m_db->networkTournamentStarted() } };

        const auto [ markers, success1 ] = m_db->markers();
        if (! success1) {
            return;
        }

        QJsonArray markersList;
        for (const Markers::Marker &marker : markers) {
            markersList.append(QJsonObject {
                { sp::markerId,      marker.id },
                { sp::markerName,    marker.name },
                { sp::markerColor,   ColorHelper::colorToString(marker.color) },
                { sp::markerSorting, static_cast<int>(marker.sorting) }
            });
        }

        const auto [ playersData, success2 ] = m_db->playersData();
        if (! success2) {
            return;
        }

        QJsonArray playersList;

        for (const auto &data : playersData) {
            QJsonObject jsonData;
            jsonData.insert(sp::playersName, data.name);

            if (data.number > 0) {
                jsonData.insert(sp::playersNumber, data.number);
            }

            if (data.marker != 0) {
                jsonData.insert(sp::markerId, data.marker);
            }

            if (! data.draw.isEmpty()) {
                QJsonObject jsonDraw;
                QMap<int, Draw::Seat>::const_iterator it;
                for (it = data.draw.constBegin(); it != data.draw.constEnd(); it++) {
                    const auto &seat = it.value();
                    jsonDraw.insert(QString::number(it.key()),
                                    QJsonObject { { sp::drawnTable,  seat.table  },
                                                  { sp::drawnPair,   seat.pair   },
                                                  { sp::drawnPlayer, seat.player } });
                }
                jsonData.insert(sp::playersDraw, jsonDraw);
            }

            if (data.disqualified != 0) {
                jsonData.insert(sp::disqualified, data.disqualified);
            }

            if (! data.bookingChecksum.isEmpty()) {
                jsonData.insert(sp::bookingChecksum, data.bookingChecksum);
            }

            playersList.append(jsonData);
        }

        QJsonObject scoreChecksums;

        QMap<int, QString>::const_iterator it;
        for (it = m_scorePage->checksums().constBegin(); it != m_scorePage->checksums().constEnd();
             it++) {

            scoreChecksums.insert(QString::number(it.key()), it.value());
        }

        connection->sendData(QJsonObject {
            { sp::action,             sp::handshake },
            { sp::phase,              sp::synchronization },
            { sp::settings,           settings },
            { sp::markersList,        markersList },
            { sp::bookedMarker,       m_tournamentSettings->specialMarker(Markers::Booked) },
            { sp::singlesManagement,  JsonHelper::singlesManagementToJson(
                                          m_tournamentSettings->singlesManagement()) },
            { sp::playersList,        playersList },
            { sp::scoreChecksumsList, scoreChecksums },
            { sp::checksum,           *m_checksum },
            { sp::bookingSettings,    JsonHelper::bookingSettingsToJson(
                                          m_tournamentSettings->bookingSettings()) }
        });

    } else if (phase == sp::idAllocation) {
        m_clientIdCounter++;
        connection->setId(m_clientIdCounter);

        // Adopt the client's initial score checksums
        const QJsonObject scoreChecksums = jsonObject.value(sp::scoreChecksumsList).toObject();
        const auto rounds = scoreChecksums.keys();
        for (const auto &round : rounds) {
            m_scorePage->setClientChecksum(m_clientIdCounter, round.toInt(),
                                           scoreChecksums.value(round).toString());
        }

        connection->sendData(QJsonObject { { sp::action,   sp::handshake },
                                           { sp::phase,    sp::idAllocation },
                                           { sp::clientId, m_clientIdCounter } });

        m_pendingConnections.removeOne(connection);
        m_connections.append(connection);
        m_idMap.insert(connection->id(), connection);

        Q_EMIT message(tr("Synchronisation mit %1 war erfolgreich, Verbindung hergestellt "
                          "(Client-ID %2)").arg(
                          connection->ip(), QString::number(connection->id())));
        Q_EMIT statusUpdate(tr("Verbindung mit %1 hergestellt (Client-ID %2)").arg(
                               connection->ip(), QString::number(connection->id())));
        Q_EMIT clientCountChanged(m_connections.count());
    }
}

// ====================
// Changes broadcasting
// ====================

// Registration changes
// ====================

void Server::broadcastRegisterPlayers(const QString &name, int marker, int clientId)
{
    QJsonObject message { { sp::action,      sp::registerPlayers },
                          { sp::playersName, name },
                          { sp::markerId,    marker } };
    broadcastMessage(message, clientId, tr("Sende Anmelden von „%1“").arg(name));
}

void Server::broadcastRenamePlayers(const QString &name, const QString &newName, int markerId,
                                    int clientId)
{
    QJsonObject message { { sp::action,         sp::renamePlayers },
                          { sp::playersName,    name },
                          { sp::newPlayersName, newName },
                          { sp::markerId,       markerId } };

    QString markerChange;
    if (markerId != -1) {
        if (markerId == 0) {
            markerChange = tr(" und Entfernen der Markierung");
        } else {
            markerChange = tr(" und Setzen der Markierung auf „%1“").arg(
                              m_markersWidget->marker(markerId).name);
        }
    }

    broadcastMessage(message, clientId, tr("Sende Umbenennen von „%1“ zu „%2“%3").arg(
                                           name, newName, markerChange));
}

void Server::broadcastAssemblePair(const QString &player1, const QString &player2,
                                   const QString &separator, int assignedMarker, int clientId)
{
    QJsonObject message { { sp::action,      sp::assemblePair },
                          { sp::playersName, QJsonArray { player1, player2, separator } },
                          { sp::markerId,    assignedMarker } };
    broadcastMessage(message, clientId, tr("Sende Zusammenfügen von „%1“ und „%2“ zu einem "
                                           "Paar").arg(player1, player2));
}

void Server::broadcastDeletePlayers(const QString &name, int clientId)
{
    QJsonObject message { { sp::action,      sp::deletePlayers },
                          { sp::playersName, name } };
    broadcastMessage(message, clientId, tr("Sende Löschen von „%1“").arg(name));
}

void Server::broadcastDeleteMarkedPlayers(int markerId, int clientId)
{
    QJsonObject message { { sp::action,   sp::deleteMarkedPlayers },
                          { sp::markerId, markerId } };

    const auto &marker = m_markersWidget->marker(markerId);
    broadcastMessage(message, clientId, markerId != 0
        ? tr("Sende Löschen aller als „%1“ markierten %2").arg(
             marker.name, m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler"))
        : tr("Sende Löschen aller unmarkierten %2").arg(
             m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler")));
}

void Server::broadcastDeleteAllPlayers(int clientId)
{
    QJsonObject message { { sp::action, sp::deleteAllPlayers } };
    broadcastMessage(message, clientId, tr("Sende Löschen aller %2").arg(
                                           m_tournamentSettings->isPairMode() ? tr("Paare")
                                                                              : tr("Spieler")));
}

void Server::broadcastSetNumber(const QString &name, int number, int clientId)
{
    QJsonObject message { { sp::action,        sp::setNumber },
                          { sp::playersName,   name },
                          { sp::playersNumber, number } };
    if (number > 0) {
        broadcastMessage(message, clientId,
                         (m_tournamentSettings->isPairMode()
                              ? tr("Sende Vergeben der Paarnummer %1 für „%2“")
                              : tr("Sende Vergeben der Spielernummer %1 für „%2“")).arg(
                          QString::number(number), name));
    } else {
        broadcastMessage(message, clientId,
                         (m_tournamentSettings->isPairMode()
                              ? tr("Sende Löschen der Paarnummer von „%1“")
                              : tr("Sende Löschen der Spielernummer von „%1“")).arg(name));
    }
}

void Server::broadcastSetBookingChecksum(const QString &name, const QString &checksum, int clientId)
{
    QJsonObject message { { sp::action,          sp::setBookingChecksum },
                          { sp::playersName,     name },
                          { sp::bookingChecksum, checksum } };
    broadcastMessage(message, clientId,
                     tr("Registrieren des Voranmeldungscodes für „%1“").arg(name));
}

void Server::broadcastSetDisqualified(const QString &name, int round, int clientId)
{
    QJsonObject message { { sp::action,      sp::setDisqualified },
                          { sp::playersName, name },
                          { sp::round,       round } };
    if (round != 0) {
        broadcastMessage(message, clientId,
                         tr("Sende Disqualifikation von „%1“ ab Runde %2").arg(
                            name, QString::number(round)));
    } else {
        broadcastMessage(message, clientId,
                         tr("Sende Zurücknehmen der Disqualifikation von „%1“").arg(name));
    }
}

void Server::broadcastSetMarker(const QString &name, int markerId, int clientId)
{
    QJsonObject message { { sp::action,      sp::setMarker },
                          { sp::playersName, name },
                          { sp::markerId,    markerId } };

    if (markerId != 0) {
        broadcastMessage(message, clientId,
                         tr("Sende Markieren von „%1“ als „%2“").arg(
                             name, m_markersWidget->marker(markerId).name));
    } else {
        broadcastMessage(message, clientId,
                         tr("Sende Entfernen der Markierung von „%1“").arg(name));
    }
}

void Server::broadcastSetListMarker(int markerId, int clientId)
{
    QJsonObject message { { sp::action,     sp::setListMarker },
                          { sp::markerId,   markerId } };

    if (markerId != 0) {
        broadcastMessage(message, clientId,
                         tr("Sende Markieren aller %1 als „%2“").arg(
                            m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler"),
                            m_markersWidget->marker(markerId).name));
    } else {
        broadcastMessage(message, clientId, tr("Sende Entfernen aller Markierungen"));
    }
}

void Server::broadcastRemoveListMarker(int markerId, int clientId)
{
    QJsonObject message { { sp::action,     sp::removeListMarker },
                          { sp::markerId,   markerId } };
    broadcastMessage(message, clientId,
                     tr("Sende Entfernen aller „%1“-Markierungen").arg(
                        m_markersWidget->marker(markerId).name));
}

// Draw changes
// ============

void Server::broadcastSetDraw(int round, const QString &name, const Draw::Seat &seat, int marker,
                              int clientId)
{
    QJsonObject message { { sp::action,      sp::setDraw },
                          { sp::round,       round },
                          { sp::playersName, name },
                          { sp::drawnTable,  seat.table },
                          { sp::drawnPair,   seat.pair },
                          { sp::drawnPlayer, seat.player },
                          { sp::markerId,    marker } };
    const QString text = seat.isEmpty()
        ? tr("Sende Löschen der Auslosung für „%1“ für Runde %2").arg(
             name, QString::number(round))
        : tr("Sende Speichern der Auslosung für „%1“ für Runde %2").arg(
             name, QString::number(round));
    broadcastMessage(message, clientId, text);
}

void Server::broadcastSetListDraw(int round, const QVector<QString> &names,
                                  const QVector<Draw::Seat> &draw, int clientId)
{
    QJsonArray namesList;
    QJsonArray tables;
    QJsonArray pairs;
    QJsonArray players;
    for (int i = 0; i < names.count(); i++) {
        namesList.append(names.at(i));
        tables.append(draw.at(i).table);
        pairs.append(draw.at(i).pair);
        players.append(draw.at(i).player);
    }

    QJsonObject message { { sp::action,           sp::setListDraw },
                          { sp::round,            round },
                          { sp::playersList,      namesList },
                          { sp::drawnTablesList,  tables },
                          { sp::drawnPairsList,   pairs },
                          { sp::drawnPlayersList, players } };

    broadcastMessage(message, clientId,
                     tr("Sende Setzen der Auslosung aller Plätze für Runde %1").arg(round));
}

void Server::broadcastDeleteAllDraws(int round, int clientId)
{
    QJsonObject message { { sp::action, sp::deleteAllDraws },
                          { sp::round,  round} };
    broadcastMessage(message, clientId,
                     tr("Sende Löschen aller Auslosungen für Runde %1").arg(round));
}

// Markers definition changes
// ==========================

void Server::broadcastAddMarker(const Markers::Marker &marker, int clientId)
{
    QJsonObject message { { sp::action,        sp::addMarker },
                          { sp::markerName,    marker.name },
                          { sp::markerColor,   ColorHelper::colorToString(marker.color) },
                          { sp::markerSorting, marker.sorting } };
    broadcastMessage(message, clientId,
                     tr("Sende Hinzufügen der Markierung „%1“").arg(marker.name));
}

void Server::broadcastDeleteMarker(const QString &name, int id, int sequence, int clientId)
{
    QJsonObject message { { sp::action,         sp::deleteMarker },
                          { sp::markerName,     name },
                          { sp::markerId,       id },
                          { sp::markerSequence, sequence } };
    broadcastMessage(message, clientId, tr("Sende Hinzufügen der Markierung „%1“").arg(name));
}

void Server::broadcastMoveMarker(const QString &name, int id, int sequence, int direction,
                                 int clientId)
{
    QJsonObject message { { sp::action,          sp::moveMarker },
                          { sp::markerName,      name },
                          { sp::markerId,        id },
                          { sp::markerSequence,  sequence },
                          { sp::markerDirection, direction } };
    broadcastMessage(message, clientId,
                     tr("Sende Verschieben der Markierung „%1“ %2").arg(
                        name, direction == 1 ? tr("nach unten") : tr("nach oben")));
}

void Server::broadcastEditMarker(const QString &name, int id, const QString &newName,
                                 const QColor &color, Markers::MarkerSorting sorting,
                                 int clientId)
{
    QJsonObject message { { sp::action,        sp::editMarker },
                          { sp::markerName,    name },
                          { sp::markerId,      id },
                          { sp::newMarkerName, newName },
                          { sp::markerColor,   ColorHelper::colorToString(color) },
                          { sp::markerSorting, sorting } };
    broadcastMessage(message, clientId, tr("Sende Änderung der Markierung „%1“").arg(name));
}

// Special markers changes
// =======================

void Server::broadcastSetBookedMarker(const QString &name, int id, int clientId)
{
    QJsonObject message { { sp::action,     sp::setBookedMarker },
                          { sp::markerName, name },
                          { sp::markerId,   id } };
    const QString text = id == -1
        ? tr("Sende Deaktivierung der Markierung für Voranmeldungen")
        : tr("Sende Setzen der Markierung für Voranmeldungen auf „%1“").arg(name);
    broadcastMessage(message, clientId, text);
}

void Server::broadcastSinglesManagementChange(const Markers::SinglesManagement &data,
                                              int clientId)
{
    QJsonObject message {
            { sp::action,            sp::singlesManagementChange },
            { sp::singlesManagement, JsonHelper::singlesManagementToJson(data) }
    };
    broadcastMessage(message, clientId, tr("Sende Änderung für das Behandeln allein gekommener "
                                           "Spieler"));
}

// Finishing the registration
// ==========================

void Server::broadcastFinishRegistration(int clientId)
{
    QJsonObject message { { sp::action, sp::finishRegistration } };
    broadcastMessage(message, clientId, tr("Sende Beenden der Anmeldung"));
}

// Score chechsum changes
// ======================

void Server::broadcastScoreChecksum(int round, int)
{
    QJsonObject message { { sp::action,        sp::scoreChecksumChange },
                          { sp::round,         round },
                          { sp::scoreChecksum, m_scorePage->checksums().value(round) } };
    broadcastMessage(message, 0, tr("Sende Änderung der Ergebnisse in Runde %1").arg(round));
}

// ===================
// Requests to clients
// ===================

// Score comparison
// ================

void Server::requestScore(int clientId, int round)
{
    QJsonObject message { { sp::action,      sp::scoreRequest },
                          { sp::requestType, sp::serverRequest },
                          { sp::round,       round } };

    clientMessage(message, m_idMap.value(clientId),
                  tr("Frage die Ergebnisse für Runde %1 bei Client #%2 an").arg(
                     QString::number(round), QString::number(clientId)),
                  Server::ServerRequest);
}

// =======================
// Client request handlers
// =======================

void Server::processClientRequest(Connection *connection, const QString &action,
                                  const QJsonObject &jsonObject)
{
    Q_EMIT rx();

    QApplication::setOverrideCursor(Qt::BusyCursor);

    const QString client = clientString(connection);
    const int clientId = jsonObject.value(sp::clientId).toInt();

    if (JsonHelper::doChecksumCheck(jsonObject)
        && *m_checksum != jsonObject.value(sp::checksum).toString()) {

        Q_EMIT message(tr("Ungültige Anfrage von %1: Die Prüfsummen stimmen nicht überein").arg(
                          client));
        connection->sendData(QJsonObject {
            { sp::action,       sp::actionRejected },
            { sp::rejectReason, static_cast<int>(sp::RejectReasonOutdatedRequest) },
            { sp::clientId,     clientId }
        });
        QApplication::restoreOverrideCursor();
        return;
    }

    // Pass the request to the respective handler function

    bool handled = false;

    // Check all broadcast handlers
    if (m_requestHandlers.contains(action)) {
        handled = true;
        (this->*m_requestHandlers.value(action))(jsonObject, client, clientId);
    }

    // Check all single reply handlers
    if (! handled && m_singleRequestHandlers.contains(action)) {
        handled = true;
        (this->*m_singleRequestHandlers.value(action))(connection, jsonObject, client, clientId);
    }

    if (! handled) {
        // This should not happen
        Q_EMIT message(tr("Ungültige Anfrage von %1: Unbekannte Aktion angefragt").arg(client));
        connection->sendData(QJsonObject {
            { sp::action,       sp::actionRejected },
            { sp::rejectReason, static_cast<int>(sp::RejectReasonUnknownAction) },
            { sp::clientId,     clientId }
        });
    }

    QApplication::restoreOverrideCursor();
}

// Registration changes
// ====================

void Server::processRegisterPlayers(const QJsonObject &jsonObject, const QString &client,
                                    int clientId)
{
    const QString name = jsonObject.value(sp::playersName).toString();
    const int marker = jsonObject.value(sp::markerId).toInt();
    Q_EMIT message(tr("Anfrage zum Anmelden von „%1“ von %2").arg(name, client));

    if (! m_db->registerPlayers(name, marker)) {
        return;
    }
    Q_EMIT statusUpdate(tr("Netzwerkänderung: %1 „%2“ angemeldet").arg(
                           m_tournamentSettings->isPairMode() ? tr("Paar") : tr("Spieler"), name));

    m_registrationPage->processNetworkChange();
    broadcastRegisterPlayers(name, marker, clientId);
}

void Server::processRenamePlayers(const QJsonObject &jsonObject, const QString &client,
                                  int clientId)
{
    const auto name    = jsonObject.value(sp::playersName).toString();
    const auto newName = jsonObject.value(sp::newPlayersName).toString();
    const auto &marker = m_markersWidget->marker(jsonObject.value(sp::markerId).toInt());

    QString markerChangeStart;
    QString markerChangeFinished;
    if (marker.id != -1) {
        if (marker.id == 0) {
            markerChangeStart = tr(" und Entfernen der Markierung");
            markerChangeFinished = tr(" und die Markierung entfernt");
        } else {
            markerChangeStart = tr(" und Setzen der Markierung auf „%1“").arg(marker.name);
            markerChangeFinished = tr(" und die Markierung auf „%1“ gesetzt").arg(marker.name);
        }
    }

    Q_EMIT message(tr("Anfrage zum Umbenennen von „%1“ zu „%2“%3 von %4").arg(
                      name, newName, markerChangeStart, client));

    if (! m_db->renamePlayers(name, newName, marker.id)) {
        return;
    }

    Q_EMIT statusUpdate(tr("Netzwerkänderung: %1 „%2“ zu „%3“ umbenannt%4").arg(
                           m_tournamentSettings->isPairMode() ? tr("Paar") : tr("Spieler"),
                           name, newName, markerChangeFinished));

    m_registrationPage->processNetworkChange();
    Q_EMIT m_registrationPage->requestUpdateDrawOverviewPage(0, QString());
    Q_EMIT m_registrationPage->nameEdited();
    broadcastRenamePlayers(name, newName, marker.id, clientId);
}

void Server::processAssemblePair(const QJsonObject &jsonObject, const QString &client,
                                 int clientId)
{
    const QJsonArray namesData = jsonObject.value(sp::playersName).toArray();
    const QString player1      = namesData.at(0).toString();
    const QString player2      = namesData.at(1).toString();
    const QString separator    = namesData.at(2).toString();
    const int marker           = jsonObject.value(sp::markerId).toInt();
    Q_EMIT message(tr("Anfrage zum Zusammenfügen von „%1“ und „%2“ zu einem Paar von %3").arg(
                      player1, player2, client));

    if (! m_db->assemblePair(player1, player2, separator, marker)) {
        return;
    }

    Q_EMIT statusUpdate(tr("Netzwerkänderung: „%1“ und „%2“ zu einem Paar zusammengefügt").arg(
                           player1, player2));

    m_registrationPage->processNetworkChange();
    Q_EMIT m_registrationPage->requestUpdateDrawOverviewPage(0, QString());
    broadcastAssemblePair(player1, player2, separator, marker, clientId);
}

void Server::processDeletePlayers(const QJsonObject &jsonObject, const QString &client,
                                  int clientId)
{
    const QString name = jsonObject.value(sp::playersName).toString();
    Q_EMIT message(tr("Anfrage zum Löschen von „%1“ von %2").arg(name, client));

    if (! m_db->deletePlayers(name)) {
        return;
    }

    Q_EMIT statusUpdate(tr("Netzwerkänderung: %1 „%2“ gelöscht").arg(
                           m_tournamentSettings->isPairMode() ? tr("Paar") : tr("Spieler"), name));

    m_registrationPage->processNetworkChange();
    Q_EMIT m_registrationPage->requestUpdateDrawOverviewPage(0, QString());
    broadcastDeletePlayers(name, clientId);
}

void Server::processDeleteMarkedPlayers(const QJsonObject &jsonObject, const QString &client,
                                        int clientId)
{
    const auto &marker = m_markersWidget->marker(jsonObject.value(sp::markerId).toInt());

    Q_EMIT message(marker.id != 0
        ? tr("Anfrage zum Löschen aller als „%1“ markierten %2 von %3").arg(
             marker.name, m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler"), client)
        : tr("Anfrage zum Löschen aller unmarkierten %1 von %2").arg(
             m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler"), client));

    if (! m_db->deleteMarkedPlayers(marker.id)) {
        return;
    }

    Q_EMIT statusUpdate(marker.id != 0
        ? tr("Netzwerkänderung: Alle als %1 markierten „%2“ gelöscht").arg(
             marker.name, m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler"))
        : tr("Netzwerkänderung: Alle unmarkierten %1 gelöscht").arg(
             m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler")));

    m_registrationPage->processNetworkChange();
    Q_EMIT m_registrationPage->requestUpdateDrawOverviewPage(0, QString());
    broadcastDeleteMarkedPlayers(marker.id, clientId);
}

void Server::processDeleteAllPlayers(const QJsonObject &, const QString &client, int clientId)
{
    Q_EMIT message(tr("Anfrage zum Löschen aller %1 von %2").arg(
                      m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler"), client));

    if (! m_db->deleteAllPlayers()) {
        return;
    }

    Q_EMIT statusUpdate(tr("Netzwerkänderung: Alle %1 gelöscht").arg(
                           m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler")));

    m_registrationPage->processNetworkChange();
    Q_EMIT m_registrationPage->requestUpdateDrawOverviewPage(0, QString());
    broadcastDeleteAllPlayers(clientId);
}

void Server::processSetNumber(const QJsonObject &jsonObject, const QString &client, int clientId)
{
    const QString name = jsonObject.value(sp::playersName).toString();
    const int number   = jsonObject.value(sp::playersNumber).toInt();
    if (number > 0) {
        Q_EMIT message((m_tournamentSettings->isPairMode()
                            ? tr("Anfrage zum Vergeben der Paarnummer %1 für „%2“ von %3")
                            : tr("Anfrage zum Vergeben der Spielernummer %1 für „%2“ von %3")).arg(
                        QString::number(number), name, client));
    } else {
        Q_EMIT message((m_tournamentSettings->isPairMode()
                            ? tr("Anfrage zum Löschen der Paarnummer von „%1“ von %2")
                            : tr("Anfrage zum Löschen der Spielernummer von „%1“ von %2")).arg(
                        name, client));
    }

    if (! m_db->setNumber(name, number)) {
        return;
    }

    if (number > 0) {
        Q_EMIT statusUpdate((m_tournamentSettings->isPairMode()
                                 ? tr("Netzwerkänderung: Paarnummer %1 für „%2“ vergeben")
                                 : tr("Netzwerkänderung: Spielernummer %1 für „%2“ vergeben")).arg(
                             QString::number(number), name));
    } else {
        Q_EMIT statusUpdate((m_tournamentSettings->isPairMode()
                                 ? tr("Netzwerkänderung: Paarnummer von „%1“ gelöscht")
                                 : tr("Netzwerkänderung: Spielernummer von „%1“ gelöscht")).arg(
                             name));
    }

    m_registrationPage->processNetworkChange();
    Q_EMIT m_registrationPage->requestUpdateDrawOverviewPage(0, QString());
    broadcastSetNumber(name, number, clientId);
}

void Server::processSetBookingChecksum(const QJsonObject &jsonObject, const QString &client,
                                       int clientId)
{
    const auto name     = jsonObject.value(sp::playersName).toString();
    const auto checksum = jsonObject.value(sp::bookingChecksum).toString();
    Q_EMIT message(tr("Anfrage zum Registrieren des Voranmeldungscodes für „%1“ von %2").arg(
                      name, client));

    if (! m_db->setBookingChecksum(name, checksum)) {
        return;
    }

    Q_EMIT statusUpdate(tr("Netzwerkänderung: Voranmeldungscode für „%1“ registriert").arg(name));

    m_registrationPage->processNetworkChange();
    broadcastSetBookingChecksum(name, checksum, clientId);
}

void Server::processSetDisqualified(const QJsonObject &jsonObject, const QString &client,
                                    int clientId)
{
    const QString name = jsonObject.value(sp::playersName).toString();
    const int round    = jsonObject.value(sp::round).toInt();
    if (round != 0) {
        Q_EMIT message(tr("Anfrage zum Disqualifizieren von „%1“ ab Runde %2 von %3").arg(
                          name, QString::number(round), client));
    } else {
        Q_EMIT message(tr("Anfrage zum Zurücknehmen der Disqualifikation von „%1“ von %2").arg(
                          name, client));
    }

    if (! m_db->setDisqualified(name, round)) {
        return;
    }

    if (round != 0) {
        Q_EMIT statusUpdate(tr("Netzwerkänderung: %1 „%2“ ab Runde %3 disqualifiziert").arg(
                               m_tournamentSettings->isPairMode() ? tr("Paar") : tr("Spieler"),
                               name, QString::number(round)));
    } else {
        Q_EMIT statusUpdate(tr("Netzwerkänderung: Disqualifikation von %1 „%2“ zurückgenommen").arg(
                               m_tournamentSettings->isPairMode() ? tr("Paar") : tr("Spieler"),
                               name));
    }

    m_registrationPage->processNetworkChange();
    Q_EMIT m_registrationPage->disqualificationChanged();
    Q_EMIT m_registrationPage->requestUpdateDrawOverviewPage(0, QString());
    broadcastSetDisqualified(name, round, clientId);
}

void Server::processSetMarker(const QJsonObject &jsonObject, const QString &client, int clientId)
{
    const auto name    = jsonObject.value(sp::playersName).toString();
    const auto &marker = m_markersWidget->marker(jsonObject.value(sp::markerId).toInt());

    if (marker.id != 0) {
        Q_EMIT message(tr("Anfrage zum Markieren von „%1“ als „%2“ von %3").arg(
                          name, marker.name, client));
    } else {
        Q_EMIT message(tr("Anfrage zum Entfernen der Markierung von „%1“ von %3").arg(
                          name, client));
    }

    if (! m_db->setMarker(name, marker.id)) {
        return;
    }

    if (marker.id != 0) {
        Q_EMIT statusUpdate(tr("Netzwerkänderung: %1 „%2“ als „%3“ markiert").arg(
                               m_tournamentSettings->isPairMode() ? tr("Paar") : tr("Spieler"),
                               name, marker.name));
    } else {
        Q_EMIT statusUpdate(tr("Netzwerkänderung: Markierung von %1 „%2“ entfernt").arg(
                               m_tournamentSettings->isPairMode() ? tr("Paar") : tr("Spieler"),
                               name));
    }

    m_registrationPage->processNetworkChange();
    broadcastSetMarker(name, marker.id, clientId);
}

void Server::processSetListMarker(const QJsonObject &jsonObject, const QString &client,
                                  int clientId)
{
    const auto &marker = m_markersWidget->marker(jsonObject.value(sp::markerId).toInt());

    if (marker.id != 0) {
        Q_EMIT message(tr("Anfrage zum Markieren aller %1 als „%2“ von %3").arg(
                          m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler"),
                          marker.name, client));
    } else {
        Q_EMIT message(tr("Anfrage zum Entfernen aller Markierungen von %1").arg(client));
    }

    if (! m_db->setListMarker(marker.id)) {
        return;
    }

    if (marker.id != 0) {
        Q_EMIT statusUpdate(tr("Netzwerkänderung: Alle %1 als „%2“ markiert").arg(
                               m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler"),
                               marker.name));
    } else {
        Q_EMIT statusUpdate(tr("Netzwerkänderung: Alle Markierungen entfernt"));
    }

    m_registrationPage->processNetworkChange();
    broadcastSetListMarker(marker.id, clientId);
}

void Server::processRemoveListMarker(const QJsonObject &jsonObject, const QString &client,
                                     int clientId)
{
    const auto &marker = m_markersWidget->marker(jsonObject.value(sp::markerId).toInt());

    Q_EMIT message(tr("Anfrage zum Entfernen aller „%1“-Markierungen von %2").arg(
                      marker.name, client));

    if (! m_db->removeListMarker(marker.id)) {
        return;
    }

    Q_EMIT statusUpdate(tr("Netzwerkänderung: Alle „%1“-Markierungen entfernt").arg(marker.name));

    m_registrationPage->processNetworkChange();
    broadcastRemoveListMarker(marker.id, clientId);
}

// Draw changes
// ============

void Server::processSetDraw(const QJsonObject &jsonObject, const QString &client, int clientId)
{
    const int round       = jsonObject.value(sp::round).toInt();
    const QString name    = jsonObject.value(sp::playersName).toString();
    const Draw::Seat seat = { jsonObject.value(sp::drawnTable).toInt(),
                              jsonObject.value(sp::drawnPair).toInt(),
                              jsonObject.value(sp::drawnPlayer).toInt() };
    const int marker      = jsonObject.value(sp::markerId).toInt();
    if (seat.isEmpty()) {
        Q_EMIT message(tr("Anfrage zum Löschen der Auslosung für „%1“ für Runde %2 von %3").arg(
                          name, QString::number(round), client));
    } else {
        Q_EMIT message(tr("Anfrage zum Speichern der Auslosung für „%1“ für Runde %2 von %3").arg(
                          name, QString::number(round), client));
    }

    if (! m_db->setDraw(round, name, seat, marker)) {
        return;
    }

    if (seat.isEmpty()) {
        Q_EMIT statusUpdate(tr("Netzwerkänderung: Auslosung für „%1“ für Runde %2 gelöscht").arg(
                               name, QString::number(round)));
    } else {
        if (m_tournamentSettings->isPairMode()) {
            Q_EMIT statusUpdate(tr("Netzwerkänderung: Auslosung für „%1“ für Runde %2 gespeichert: "
                                   "Tisch %3 Paar %4").arg(
                                   name, QString::number(round), QString::number(seat.table),
                                   QString::number(seat.pair)));
        } else {
            Q_EMIT statusUpdate(tr("Netzwerkänderung: Auslosung für „%1“ für Runde %2 gespeichert: "
                                   "Tisch %3 Paar %4 (%5)").arg(
                                   name, QString::number(round), QString::number(seat.table),
                                   QString::number(seat.pair), QString::number(seat.player)));
        }
    }

    m_registrationPage->processNetworkChange();
    Q_EMIT m_registrationPage->requestUpdateDrawOverviewPage(round, QString());
    Q_EMIT m_registrationPage->drawChanged();
    broadcastSetDraw(round, name, seat, marker, clientId);
}

void Server::processSetListDraw(const QJsonObject &jsonObject, const QString &client, int clientId)
{
    const int round = jsonObject.value(sp::round).toInt();
    Q_EMIT message(tr("Anfrage zum Setzen der Auslosung aller Plätze für Runde %1 von %2").arg(
                      QString::number(round), client));

    const QJsonArray namesData   = jsonObject.value(sp::playersList).toArray();
    const QJsonArray tablesData  = jsonObject.value(sp::drawnTablesList).toArray();
    const QJsonArray pairsData   = jsonObject.value(sp::drawnPairsList).toArray();
    const QJsonArray playersData = jsonObject.value(sp::drawnPlayersList).toArray();

    QVector<QString> names;
    QVector<Draw::Seat> draw;
    for (int i = 0; i < namesData.count(); i++) {
        names.append(namesData.at(i).toString());
        draw.append({ tablesData.at(i).toInt(),
                      pairsData.at(i).toInt(),
                      playersData.at(i).toInt() });
    }

    if (! m_db->setListDraw(round, names, draw)) {
        return;
    }

    Q_EMIT statusUpdate(tr("Netzwerkänderung: Auslosung aller Plätze für Runde %1 gesetzt").arg(
                           round));

    m_registrationPage->processNetworkChange();
    Q_EMIT m_registrationPage->requestUpdateDrawOverviewPage(round, QString());
    Q_EMIT m_registrationPage->drawChanged();
    broadcastSetListDraw(round, names, draw, clientId);
}

void Server::processDeleteAllDraws(const QJsonObject &jsonObject, const QString &client,
                                   int clientId)
{
    const int round = jsonObject.value(sp::round).toInt();
    Q_EMIT message(tr("Anfrage zum Löschen aller Auslosungen für Runde %1 von %2").arg(
                      QString::number(round), client));

    if (! m_db->deleteAllDraws(round)) {
        return;
    }

    Q_EMIT statusUpdate(tr("Netzwerkänderung: Alle Auslosungen für Runde %1 gelöscht").arg(round));

    m_registrationPage->processNetworkChange();
    Q_EMIT m_registrationPage->requestUpdateDrawOverviewPage(round, QString());
    Q_EMIT m_registrationPage->drawChanged();
    broadcastDeleteAllDraws(round, clientId);
}

// Markers defintion changes
// =========================

void Server::processAddMarker(const QJsonObject &jsonObject, const QString &client, int clientId)
{
    Markers::Marker marker;
    marker.name = jsonObject.value(sp::markerName).toString();
    marker.color = ColorHelper::colorFromString(jsonObject.value(sp::markerColor).toString());
    marker.sorting = static_cast<Markers::MarkerSorting>(
                         jsonObject.value(sp::markerSorting).toInt());

    Q_EMIT message(tr("Anfrage zum Hinzufügen der Markierung „%1“ von %2").arg(
                      marker.name, client));

    if (! m_db->addMarker(marker)) {
        return;
    }

    Q_EMIT statusUpdate(tr("Netzwerkänderung: Markierung „%1“ hinzugefügt").arg(marker.name));

    m_markersWidget->processNetworkChange();
    broadcastAddMarker(marker, clientId);
}

void Server::processDeleteMarker(const QJsonObject &jsonObject, const QString &client,
                                 int clientId)
{
    const QString name = jsonObject.value(sp::markerName).toString();
    const int id       = jsonObject.value(sp::markerId).toInt();
    const int sequence = jsonObject.value(sp::markerSequence).toInt();
    Q_EMIT message(tr("Anfrage zum Löschen der Markierung „%1“ von %2").arg(name, client));

    if (! m_db->deleteMarker(id, sequence)) {
        return;
    }

    Q_EMIT statusUpdate(tr("Netzwerkänderung: Markierung „%1“ gelöscht").arg(name));

    m_markersWidget->processNetworkChange();
    broadcastDeleteMarker(name, id, sequence, clientId);
}

void Server::processMoveMaker(const QJsonObject &jsonObject, const QString &client, int clientId)
{
    const QString name  = jsonObject.value(sp::markerName).toString();
    const int id        = jsonObject.value(sp::markerId).toInt();
    const int sequence  = jsonObject.value(sp::markerSequence).toInt();
    const int direction = jsonObject.value(sp::markerDirection).toInt();
    Q_EMIT message(tr("Anfrage zum Verschieben der Markierung „%1“ %2 von %3").arg(
                      name, direction == 1 ? tr("nach unten") : tr("nach oben"), client));

    if (! m_db->moveMarker(id, sequence, direction)) {
        return;
    }

    Q_EMIT statusUpdate(tr("Netzwerkänderung: Markierung „%1“ %2 verschoben").arg(
                           name, direction == 1 ? tr("nach unten") : tr("nach oben")));

    m_markersWidget->processNetworkChange();
    broadcastMoveMarker(name, id, sequence, direction, clientId);
}

void Server::processEditMarker(const QJsonObject &jsonObject, const QString &client, int clientId)
{
    const QString name    = jsonObject.value(sp::markerName).toString();
    const int id          = jsonObject.value(sp::markerId).toInt();
    const QString newName = jsonObject.value(sp::newMarkerName).toString();
    const QColor color    = ColorHelper::colorFromString(
                                jsonObject.value(sp::markerColor).toString());
    const Markers::MarkerSorting sorting = static_cast<Markers::MarkerSorting>(
                                               jsonObject.value(sp::markerSorting).toInt());
    Q_EMIT message(tr("Anfrage zum Ändern der Markierung „%1“ von %2").arg(name, client));

    if (! m_db->editMarker(id, newName, color, sorting)) {
        return;
    }

    Q_EMIT statusUpdate(tr("Netzwerkänderung: Markierung „%1“ geändert").arg(name));

    m_markersWidget->processNetworkChange();
    broadcastEditMarker(name, id, newName, color, sorting, clientId);
}

// Special markers changes
// =======================

void Server::processSetBookedMarker(const QJsonObject &jsonObject, const QString &client,
                                    int clientId)
{
    const QString name = jsonObject.value(sp::markerName).toString();
    const int id       = jsonObject.value(sp::markerId).toInt();
    Q_EMIT message(id == -1 ? tr("Anfrage zum Deaktivieren der Markierung für Voranmeldungen von "
                                 " %1").arg(client)
                            : tr("Anfrage zum Setzen der Markierung für Voranmeldungen auf „%1“ "
                                 "von %2").arg(name, client));

    m_tournamentSettings->setSpecialMarker(Markers::Booked, id);
    m_markersWidget->setBookedMarker(id);
    Q_EMIT statusUpdate(id == -1
        ? tr("Netzwerkänderung: Markierung für Voranmeldungen deaktiviert")
        : tr("Netzwerkänderung: Markierung für Voranmeldungen auf „%1“ gesetzt").arg(name));

    m_markersWidget->processNetworkChange();

    if (id != -1) {
        m_markersWidget->warnAboutSpecialMarkers();
    }

    broadcastSetBookedMarker(name, id, clientId);
}

void Server::processSinglesManagementChange(const QJsonObject &jsonObject, const QString &client,
                                            int clientId)
{
    const Markers::SinglesManagement singlesManagement = JsonHelper::parseSinglesManagement(
        jsonObject.value(sp::singlesManagement).toObject());
    Q_EMIT message(tr("Anfrage zum Ändern der Einstellungen zum Behandeln allein gekommner Spieler "
                      "von %1").arg(client));

    m_markersWidget->setSinglesManagement(singlesManagement);
    m_tournamentSettings->setSinglesManagement(singlesManagement);
    Q_EMIT statusUpdate(tr("Netzwerkänderung: Änderungen der Einstellungen zum Behandeln allein "
                           "gekommener Spieler übernommen"));

    m_markersWidget->processNetworkChange();

    if (singlesManagement.enabled) {
        m_markersWidget->warnAboutSpecialMarkers();
    }

    broadcastSinglesManagementChange(singlesManagement, clientId);
}

// Finishing the registration
// ==========================

void Server::processFinishRegistration(const QJsonObject &, const QString &client, int clientId)
{
    Q_EMIT message(tr("Anfrage zum Beenden der Anmeldung von %1").arg(client));

    m_registrationPage->networkFinishRegistration(clientId);
    Q_EMIT statusUpdate(tr("Netzwerkänderung: Anmeldung beendet"));

    broadcastFinishRegistration(clientId);
}

// Score comparison
// ================

void Server::processScoreRequest(Connection *connection, const QJsonObject &jsonObject,
                                 const QString &client, int clientId)
{
    const auto requestType = jsonObject.value(sp::requestType).toString();
    const auto round = jsonObject.value(sp::round).toInt();

    if (requestType == sp::clientRequest) {
        // A client asks for our results

        Q_EMIT message(tr("Ergebnisse für Runde %1 von %2 angefragt").arg(
                          QString::number(round), client));

        const auto [ score, success ] = m_db->scores(round);
        if (! success) {
            return;
        }

        auto data = ScoreHelper::scoreToJson(score);
        data.insert(sp::requestType, sp::clientRequest);
        data.insert(sp::clientId,    clientId);

        clientMessage(data, connection,
                    tr("Ergebnisse für Runde %1 an %2 gesendet").arg(
                       QString::number(round), client));

    } else if (requestType == sp::serverRequest) {
        // We asked for a client's results and got an answer
        Q_EMIT message(tr("Ergebnisse für Runde %1 von %2 erhalten").arg(
                          QString::number(round), client));

        auto data = QJsonObject { { sp::action,      sp::scoreRequest },
                                  { sp::requestType, sp::serverRequest },
                                  { sp::round,       round } };

        clientMessage(data, connection,
                      tr("Sende Okay für das Verarbeiten der Ergebnisse von Runde %1 an %2").arg(
                         QString::number(round), client));

        const auto score = ScoreHelper::jsonToScore(jsonObject);
        Q_EMIT compareScore(score);

    } else {
        // This should not happen
        qCWarning(MuckturnierLog) << "Received a score request that's neither a client nor a "
                                     "server request!";
        const auto text = tr("Ungültige Spielstandanfrage empfangen!");
        Q_EMIT message(text);
        Q_EMIT statusUpdate(text);
    }
}

void Server::processScoreChecksumChange(Connection *connection, const QJsonObject &jsonObject,
                                        const QString &client, int clientId)
{
    const auto round = jsonObject.value(sp::round).toInt();
    const auto checksum = jsonObject.value(sp::scoreChecksum).toString();
    Q_EMIT message(tr("Ergebnisänderung von %1 für Runde %2 erhalten").arg(
                      client, QString::number(round)));

    m_scorePage->setClientChecksum(clientId, round, checksum);
    Q_EMIT remoteScoreChecksumChanged(clientId, round);

    QJsonObject data { { sp::action, sp::scoreChecksumChange },
                       { sp::round,  round } };
    clientMessage(data, connection,
                  tr("Ergebnisänderung für %1 für Runde %2 verarbeitet, sende Okay").arg(
                     client, QString::number(round)));
}

// Stop watch synchronization
// ==========================

void Server::processStopWatchSynchronization(Connection *connection, const QJsonObject &jsonObject,
                                             const QString &client, int)
{
    const auto stopWatchId = jsonObject.value(sp::stopWatchId).toInt();
    Q_EMIT message(tr("Anfrage zur Synchronisation der Stoppuhr #%1 von %2 erhalten").arg(
                      QString::number(stopWatchId), client));

    const auto syncData = m_stopWatchEngine->syncData();
    QJsonObject data { { sp::action,       sp::stopWatchSynchronization },
                       { sp::stopWatchId,  stopWatchId },
                       { sp::syncPossible, syncData.syncPossible }
    };
    JsonHelper::setNoChecksumCheck(data);

    if (syncData.syncPossible) {
        data.insert(sp::title,     syncData.title);
        data.insert(sp::hours,     syncData.hours);
        data.insert(sp::minutes,   syncData.minutes);
        data.insert(sp::seconds,   syncData.seconds);
        data.insert(sp::isRunning, syncData.isRunning);
    }

    if (syncData.isRunning) {
        data.insert(sp::startOffset, syncData.startOffset);
        // We wrap the target time into a string, because it's data type is qint64, which is an
        // alias for long long int. This is is not supported by the QJsonObject, so we extract
        // the string later and to QString::toLongLongInt() to preserve the original value.
        data.insert(sp::targetTime,  QString::number(syncData.targetTime));
    }

    clientMessage(data, connection,
                  tr("Sende Stoppuhrdaten an %1").arg(client));
}
