// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "DiscoverServer.h"

#include "network/DiscoverProtocol.h"

// Qt includes
#include <QDebug>
#include <QJsonValue>

// C++ includes
#include <functional>

namespace dp = DiscoverProtocol;

DiscoverServer::DiscoverServer(QObject *parent, int port)
    : AbstractDiscoverEngine(parent, port)
{
    connect(this, &AbstractDiscoverEngine::invalidData,
            this, std::bind(&DiscoverServer::invalidRequest, this, std::placeholders::_1,
                            tr("Ungültiger Datensatz")));
}

bool DiscoverServer::serve(const QString &ip, int port)
{
    m_serverAnnouncement = QJsonObject { { dp::action, dp::announcingServer },
                                         { dp::ip,     ip },
                                         { dp::port,   port } };
    return listen();
}

void DiscoverServer::invalidRequest(const QHostAddress &senderAddress, const QString &text)
{
    Q_EMIT message(tr("Ungültige Anfrage von %1: %2").arg(senderAddress.toString(), text));
}

void DiscoverServer::processData(const QJsonObject &jsonObject, const QHostAddress &senderAddress)
{
    if (jsonObject.value(dp::action).toString() != dp::searchingServer) {
        invalidRequest(senderAddress, tr("Ungültige Server-Suchanfrage"));
        return;
    }

    const int protocolVersion = jsonObject.value(dp::protocolVersionKey).toInt();
    if (protocolVersion != dp::protocolVersion) {
        invalidRequest(senderAddress, tr("Falsche Protokollversion (%1)").arg(protocolVersion));
        return;
    }

    Q_EMIT message(tr("Server-Suchanfrage von %1 erhalten. Sende Socket-Adresse").arg(
                      senderAddress.toString()));
    sendData(m_serverAnnouncement, senderAddress);
}
