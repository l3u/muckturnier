// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef CONNECTION_H
#define CONNECTION_H

// Local includes
#include "network/AbstractNetworkEngine.h"

// Qt includes
#include <QAbstractSocket>

// Qt classes
class QJsonObject;

class Connection : public AbstractNetworkEngine
{
    Q_OBJECT

public:
    explicit Connection(QObject *parent);
    bool setSocketDescriptor(qintptr descriptor);
    void close();
    void setId(int id);
    int id() const;

Q_SIGNALS:
    void error(QAbstractSocket::SocketError error);
    void disconnected();
    void handshakeResult(const QString &phase, const QJsonObject &jsonObject);
    void clientRequest(const QString &action, const QJsonObject &jsonObject);

private: // Functions
    void dataReceived(const QString &action, const QJsonObject &jsonObject) override;

private: // Variables
    int m_id;

};

#endif // CONNECTION_H
