// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SERVERPAGE_H
#define SERVERPAGE_H

// Local includes
#include "shared/OptionalPage.h"

// Local classes
class SharedObjects;
class RegistrationPage;
class ScorePage;
class StopWatchEngine;
class Server;
class SocketAddressWidget;
class LogDisplay;
class DiscoverServer;

// Qt classes
class QLabel;
class QPushButton;
class QGroupBox;

class ServerPage : public OptionalPage
{
    Q_OBJECT

public:
    explicit ServerPage(QWidget *parent, SharedObjects *sharedObjects,
                        RegistrationPage *registrationPage, ScorePage *scorePage,
                        StopWatchEngine *stopWatchEngine);
    bool checkServerRunning();
    Server *server() const;

Q_SIGNALS:
    void serverStarted();
    void serverStopped();
    void statusUpdate(const QString &text);

public Q_SLOTS:
    bool closePage();

private Q_SLOTS:
    void startServer();
    bool stopServer();
    void updateClientCount(int count);

private: // Variables
    Server *m_server;
    SocketAddressWidget *m_socketAddress;
    QGroupBox *m_settingsBox;
    QPushButton *m_startServer;
    QPushButton *m_stopServer;
    QPushButton *m_closeButton;
    QLabel *m_address;
    QGroupBox *m_displayBox;
    LogDisplay *m_display;
    QString m_serverStoppedMessage;
    DiscoverServer *m_discoverServer;

};

#endif // SERVERPAGE_H
