// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "ServerPage.h"
#include "Server.h"
#include "DiscoverServer.h"

#include "network/SocketAddressWidget.h"
#include "network/LogDisplay.h"

#include "Database/Database.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/Settings.h"

#include "shared/SharedStyles.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QMessageBox>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QApplication>
#include <QStyle>
#include <QTimer>

ServerPage::ServerPage(QWidget *parent, SharedObjects *sharedObjects,
                       RegistrationPage *registrationPage, ScorePage *scorePage,
                       StopWatchEngine *stopWatchEngine)
    : OptionalPage(parent)
{
    Settings *settings = sharedObjects->settings();

    m_discoverServer = new DiscoverServer(this, settings->discoverPort());

    m_server = new Server(this, settings->connectTimeout(), sharedObjects, registrationPage,
                          scorePage, stopWatchEngine);
    connect(m_server, &Server::statusUpdate, this, &ServerPage::statusUpdate);
    connect(m_server, &Server::clientCountChanged, this, &ServerPage::updateClientCount);

    m_settingsBox = new QGroupBox(tr("Socket-Adresse für den Server"));
    auto *settingsLayout = new QHBoxLayout(m_settingsBox);
    layout()->addWidget(m_settingsBox);

    m_serverStoppedMessage = tr("<span style=\"%1\">Server<br/>gestoppt</span>").arg(
                                SharedStyles::redText);
    m_address = new QLabel(m_serverStoppedMessage);
    m_address->setTextInteractionFlags(Qt::TextSelectableByMouse);
    settingsLayout->addWidget(m_address);

    m_socketAddress = new SocketAddressWidget(SocketAddressWidget::Mode::Server,
                                              settings->serverPort(),
                                              settings->preferIpv6());
    settingsLayout->addWidget(m_socketAddress);

    // Buttons

    m_startServer = new QPushButton(tr("Server starten"));
    m_startServer->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogYesButton));
    connect(m_socketAddress, &SocketAddressWidget::settingsOkay,
            m_startServer, &QPushButton::setEnabled);
    connect(m_startServer, &QPushButton::clicked, this, &ServerPage::startServer);
    settingsLayout->addWidget(m_startServer);

    m_stopServer = new QPushButton(tr("Server stoppen"));
    m_stopServer->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogCancelButton));
    connect(m_stopServer, &QPushButton::clicked, this, &ServerPage::stopServer);
    settingsLayout->addWidget(m_stopServer);

    settingsLayout->addStretch();

    m_closeButton = new QPushButton(tr("Schließen"));
    m_closeButton->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogCloseButton));
    connect(m_closeButton, &QPushButton::clicked, this, &ServerPage::closePage);
    settingsLayout->addWidget(m_closeButton);

    // Server log

    m_displayBox = new QGroupBox(tr("Netzwerkinformationen"));
    auto *displayLayout = new QVBoxLayout(m_displayBox);
    layout()->addWidget(m_displayBox);

    m_display = new LogDisplay;
    connect(m_server, &Server::protocolError, m_display, &LogDisplay::displayProtocolError);
    connect(m_server, &Server::message, m_display, &LogDisplay::displayMessage);
    connect(m_discoverServer, &DiscoverServer::message, m_display, &LogDisplay::displayMessage);

    displayLayout->addWidget(m_display);

    m_stopServer->hide();
    m_startServer->setFocus();
}

void ServerPage::startServer()
{
    m_display->clear();

    if (! m_server->listen(QHostAddress(m_socketAddress->ip()), m_socketAddress->port())) {
        m_display->displayMessage(tr("Der Muckturnier-Server konnte nicht gestartet werden: "
                                     "%1").arg(m_server->errorString()));
        QMessageBox::critical(this,
            tr("Server starten"),
            tr("<p>Der Muckturnier-Server konnte nicht gestartet werden:</p><p>%1</p>").arg(
               m_server->errorString()));
        return;
    }
    m_display->displayMessage(tr("Muckturnier-Server auf TCP-Port %1 gestartet").arg(
                                 QString::number(m_socketAddress->port())));

    if (! m_discoverServer->serve(m_socketAddress->ip(), m_socketAddress->port())) {
        QTimer::singleShot(0, this, [this]
        {
            QMessageBox::warning(this,
                tr("Server starten"),
                tr("<p><b>Der Discover-Server konnte nicht gestartet werden</b></p>"
                   "<p>Die Fehlermeldung war: %1</p>"
                   "<p>Das automatische Auffinden des Muckturnier-Servers wird nicht "
                   "funktionieren, die Clients müssen die Socket-Adresse (IP-Adresse und Port) "
                   "manuell eingeben.</p>"
                   "<p><b>Die Funktion des Muckturnier-Servers ist dadurch aber nicht "
                   "eingeschränkt!</b></p>").arg(m_discoverServer->errorString()));
        });
        m_display->displayMessage(tr("Starten des Discover-Servers fehlgeschlagen"));
    } else {
        m_display->displayMessage(tr("Discover-Server auf UDP-Port %1 gestartet").arg(
                                     QString::number(m_discoverServer->port())));
    }

    m_startServer->hide();
    m_socketAddress->hide();
    m_stopServer->show();
    m_closeButton->clearFocus();
    updateClientCount(0);

    Q_EMIT serverStarted();
    Q_EMIT statusUpdate(tr("Server unter IP %1 Port %2 gestartet").arg(
                           m_socketAddress->ip(), QString::number(m_socketAddress->port())));
}

bool ServerPage::stopServer()
{
    if (! m_server->isListening()) {
        return true;

    } else {
        if (m_server->clientCount() > 0) {
            if (QMessageBox::warning(this, tr("Server schließen"),
                    tr("<p>Es ist mindestens ein Client angemeldet.</p>"
                       "<p>Wenn der Server gestoppt wird, dann werden alle Verbindungen getrennt "
                       "und es werden keine Änderungen mehr an die anderen Netzwerkteilnehmer "
                       "gesendet oder von ihnen empfangen!</p>"),
                    QMessageBox::Ok | QMessageBox::Cancel,
                    QMessageBox::Cancel) == QMessageBox::Cancel) {

                    return false;
            }
        } else {
            if (QMessageBox::warning(this, tr("Server schließen"),
                    tr("<p>Wenn der Server gestoppt wird, dann sind keine Verbindungen zum "
                       "Muckturnier-Netzwerk mehr möglich!</p>"),
                    QMessageBox::Ok | QMessageBox::Cancel,
                    QMessageBox::Cancel) == QMessageBox::Cancel) {

                    return false;
            }
        }
    }

    m_server->close();
    m_display->displayMessage(tr("Muckturnier-Server gestoppt"));

    m_discoverServer->close();
    m_display->displayMessage(tr("Discover-Server gestoppt"));

    m_address->setText(m_serverStoppedMessage);
    m_startServer->show();
    m_stopServer->hide();
    m_socketAddress->show();
    m_startServer->setFocus();

    Q_EMIT serverStopped();
    Q_EMIT statusUpdate(tr("Server gestoppt"));

    return true;
}

bool ServerPage::closePage()
{
    if (! stopServer()) {
        return false;
    }

    return OptionalPage::closePage();
}

void ServerPage::updateClientCount(int count)
{
    m_address->setText(tr("<span style=\"%1\">Server läuft</span><br/>"
                          "IP: <b>%2</b> Port: <b>%3</b><br/>"
                          "%4").arg(SharedStyles::greenText,
                                    m_socketAddress->ip(),
                                    QString::number(m_socketAddress->port()),
                                    tr("%n Client(s) angemeldet", "", count)));
}

Server *ServerPage::server() const
{
    return m_server;
}
