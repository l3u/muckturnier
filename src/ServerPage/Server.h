// SPDX-FileCopyrightText: 2018-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SERVER_H
#define SERVER_H

// Local includes
#include "shared/Markers.h"
#include "shared/Draw.h"

// Qt includes
#include <QTcpServer>

// Local classes

class SharedObjects;
class Database;
class TournamentSettings;
class RegistrationPage;
class ScorePage;
class StopWatchEngine;
class MarkersWidget;
class Connection;
class Settings;

namespace Scores
{
struct Score;
}

class Server : public QTcpServer
{
    Q_OBJECT

public:
    explicit Server(QObject *parent, int connectTimeout, SharedObjects *sharedObjects,
                    RegistrationPage *registrationPage, ScorePage *scorePage,
                    StopWatchEngine *stopWatchEngine);
    void setChecksum(const QString *checksum);
    int clientCount() const;
    void close();

    // Changes broadcasting
    // ====================

    // Helper macro to keep the header a bit more tidy
    #define BROADCAST_FUNCTION(name, ...) void broadcast##name(__VA_ARGS__, int clientId = 0)
    #define BROADCAST_FUNCTION_NA(name) void broadcast##name(int clientId = 0)

    // Registration changes

    BROADCAST_FUNCTION(RegisterPlayers, const QString &name, int marker);
    BROADCAST_FUNCTION(RenamePlayers, const QString &name, const QString &newName, int markerId);
    BROADCAST_FUNCTION(AssemblePair, const QString &player1, const QString &player2,
                                     const QString &separator, int assignedMarker);

    BROADCAST_FUNCTION(DeletePlayers, const QString &name);
    BROADCAST_FUNCTION(DeleteMarkedPlayers, int markerId);
    BROADCAST_FUNCTION_NA(DeleteAllPlayers);

    BROADCAST_FUNCTION(SetNumber, const QString &name, int number);

    BROADCAST_FUNCTION(SetBookingChecksum, const QString &name, const QString &checksum);

    BROADCAST_FUNCTION(SetDisqualified, const QString &name, int round);

    BROADCAST_FUNCTION(SetMarker, const QString &name, int markerId);
    BROADCAST_FUNCTION(SetListMarker, int markerId);
    BROADCAST_FUNCTION(RemoveListMarker, int markerId);

    // Draw changes
    BROADCAST_FUNCTION(SetDraw, int round, const QString &name, const Draw::Seat &seat, int marker);
    BROADCAST_FUNCTION(SetListDraw, int round, const QVector<QString> &names,
                                    const QVector<Draw::Seat> &draw);
    BROADCAST_FUNCTION(DeleteAllDraws, int round);

    // Markers definition changes
    BROADCAST_FUNCTION(AddMarker, const Markers::Marker &marker);
    BROADCAST_FUNCTION(DeleteMarker, const QString &name, int id, int sequence);
    BROADCAST_FUNCTION(MoveMarker, const QString &name, int id, int sequence, int direction);
    BROADCAST_FUNCTION(EditMarker, const QString &name, int id, const QString &newName,
                                   const QColor &color, Markers::MarkerSorting sorting);

    // Special markers changes
    BROADCAST_FUNCTION(SetBookedMarker, const QString &name, int id);
    BROADCAST_FUNCTION(SinglesManagementChange, const Markers::SinglesManagement &data);

    // Finishing the registration
    BROADCAST_FUNCTION_NA(FinishRegistration);

    // Score comparison
    BROADCAST_FUNCTION(ScoreChecksum, int round);

    #undef BROADCAST_FUNCTION
    #undef BROADCAST_FUNCTION_NA

    // Requests to clients
    // ===================

    // Score comparison
    void requestScore(int clientId, int round);

Q_SIGNALS:
    void tx();
    void rx();
    void protocolError(const QString &text);
    void message(const QString &text);
    void statusUpdate(const QString &text);
    void clientCountChanged(int count);
    void compareScore(const Scores::Score &score);
    void clientDisconnected(int id);
    void remoteScoreChecksumChanged(int remoteId, int round);

private Q_SLOTS:
    void checkHandshakeTimeout(Connection *connection);
    void removeConnection(Connection *connection);
    void processHandshake(Connection *connection, const QString &phase,
                          const QJsonObject &jsonObject);
    void processClientRequest(Connection *connection, const QString &action,
                              const QJsonObject &jsonObject);
private: // Enums
    enum RequestType {
        ClientRequest,
        ServerRequest
    };

private: // Functions
    void incomingConnection(qintptr descriptor) override;
    void broadcastMessage(QJsonObject &jsonObject, int clientId, const QString &messageText);
    void clientMessage(QJsonObject &jsonObject, Connection *connection, const QString &messageText,
                       RequestType requestType = RequestType::ClientRequest);

    QString clientString(Connection *connection) const;

    // Client request handlers
    // =======================

    // Helper macro to keep the header a bit more tidy
    #define REQUEST_HANDLER(name) \
            void process##name(const QJsonObject &jsonObject, const QString &client, int clientId)
    #define SINGLE_REQUEST_HANDLER(name) \
            void process##name(Connection *connection, const QJsonObject &jsonObject, \
                               const QString &client, int clientId)

    // Registration changes

    REQUEST_HANDLER(RegisterPlayers);
    REQUEST_HANDLER(RenamePlayers);
    REQUEST_HANDLER(AssemblePair);

    REQUEST_HANDLER(DeletePlayers);
    REQUEST_HANDLER(DeleteMarkedPlayers);
    REQUEST_HANDLER(DeleteAllPlayers);

    REQUEST_HANDLER(SetNumber);

    REQUEST_HANDLER(SetBookingChecksum);

    REQUEST_HANDLER(SetDisqualified);

    REQUEST_HANDLER(SetMarker);
    REQUEST_HANDLER(SetListMarker);
    REQUEST_HANDLER(RemoveListMarker);

    // Draw changes
    REQUEST_HANDLER(SetDraw);
    REQUEST_HANDLER(SetListDraw);
    REQUEST_HANDLER(DeleteAllDraws);

    // Markers definition changes
    REQUEST_HANDLER(AddMarker);
    REQUEST_HANDLER(DeleteMarker);
    REQUEST_HANDLER(MoveMaker);
    REQUEST_HANDLER(EditMarker);

    // Special markers changes
    REQUEST_HANDLER(SetBookedMarker);
    REQUEST_HANDLER(SinglesManagementChange);

    // Finishing the registration
    REQUEST_HANDLER(FinishRegistration);

    // Score comparison
    SINGLE_REQUEST_HANDLER(ScoreRequest);
    SINGLE_REQUEST_HANDLER(ScoreChecksumChange);

    // Stop watch synchronization
    SINGLE_REQUEST_HANDLER(StopWatchSynchronization);

    #undef REQUEST_HANDLER
    #undef SINGLE_REQUEST_HANDLER

private: // Variables
    const int m_connectTimeout;
    Database *m_db;
    Settings *m_settings;
    TournamentSettings *m_tournamentSettings;

    RegistrationPage *m_registrationPage;
    MarkersWidget *m_markersWidget;
    ScorePage *m_scorePage;
    StopWatchEngine *m_stopWatchEngine;

    const QString *m_checksum;

    const QString m_locale;
    const QHash<QString, void(Server::*)(const QJsonObject &, const QString &, int)>
              m_requestHandlers;
    const QHash<QString, void(Server::*)(Connection *, const QJsonObject &, const QString &, int)>
              m_singleRequestHandlers;

    QVector<Connection *> m_pendingConnections;
    QVector<Connection *> m_connections;
    QHash<int, Connection *> m_idMap;
    int m_clientIdCounter = 0;

};

#endif // SERVER_H
