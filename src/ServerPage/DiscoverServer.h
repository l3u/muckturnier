// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef DISCOVERSERVER_H
#define DISCOVERSERVER_H

// Local includes
#include "network/AbstractDiscoverEngine.h"

// Qt includes
#include <QJsonObject>

class DiscoverServer : public AbstractDiscoverEngine
{
    Q_OBJECT

public:
    explicit DiscoverServer(QObject *parent, int port);
    bool serve(const QString &ip, int port);

Q_SIGNALS:
    void message(const QString &text);

private Q_SLOTS:
    void invalidRequest(const QHostAddress &senderAddress, const QString &text);

private: // Functions
    void processData(const QJsonObject &jsonObject, const QHostAddress &senderAddress) override;

private: // Variables
    QJsonObject m_serverAnnouncement;

};

#endif // DISCOVERYSERVER_H
