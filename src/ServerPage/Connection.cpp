// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "Connection.h"

#include "network/ServerProtocol.h"

// Qt includes
#include <QDebug>
#include <QTcpSocket>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDataStream>

namespace sp = ServerProtocol;

Connection::Connection(QObject *parent) : AbstractNetworkEngine(parent)
{
    connect(socket(), &QAbstractSocket::errorOccurred, this, &Connection::error);
    connect(socket(), &QAbstractSocket::disconnected, this, &Connection::disconnected);
}

bool Connection::setSocketDescriptor(qintptr descriptor)
{
    return socket()->setSocketDescriptor(descriptor);
}

void Connection::close()
{
    socket()->abort();
    Q_EMIT disconnected();
}

void Connection::dataReceived(const QString &action, const QJsonObject &jsonObject)
{
    if (action == sp::handshake) {
        const QJsonValue phaseValue = jsonObject.value(sp::phase);
        if (phaseValue.type() != QJsonValue::String) {
            Q_EMIT handshakeResult(sp::failed, jsonObject);
            close();
            return;
        }
        Q_EMIT handshakeResult(phaseValue.toString(), jsonObject);
    } else {
        Q_EMIT clientRequest(action, jsonObject);
    }
}

void Connection::setId(int id)
{
    m_id = id;
}

int Connection::id() const
{
    return m_id;
}
