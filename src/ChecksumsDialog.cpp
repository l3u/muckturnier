// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "ChecksumsDialog.h"

#include "Database/Database.h"

#include "network/ChecksumHelper.h"

// Qt includes
#include <QGroupBox>
#include <QLabel>
#include <QGridLayout>
#include <QDebug>
#include <QTimer>

// C++ includes
#include <utility>

ChecksumsDialog::ChecksumsDialog(QWidget *parent, Database *db, bool isPairMode)
    : TitleDialog(parent)
{
    setTitle(tr("Prüfsummen"));
    addButtonBox(QDialogButtonBox::Ok);

    QGroupBox *checksumsBox = new QGroupBox;
    layout()->addWidget(checksumsBox);
    QGridLayout *layout = new QGridLayout(checksumsBox);

    // Pairs/Players list checksum

    QLabel *playersListLabel = new QLabel(QStringLiteral("<b>%1</b>").arg(
                                              isPairMode ? tr("Paarliste:") : tr("Spielerliste:")));
    playersListLabel->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    layout->addWidget(playersListLabel, 0, 0);

    auto [ playersList, success ] = db->players();
    if (! success) {
        QTimer::singleShot(0, this, &QDialog::reject);
        return;
    }

    ChecksumHelper checksumHelper;

    playersList.sort(); // We use the locale-independent sort here so that
                        // the checksum won't depend on the used locale
    for (const QString &text : std::as_const(playersList)) {
        checksumHelper.addData(text);
    }

    QLabel *playersChecksum = new QLabel(formatChecksum(checksumHelper.checksum()));
    playersChecksum->setTextInteractionFlags(Qt::TextSelectableByMouse);
    layout->addWidget(playersChecksum, 0, 1);

    // Ranking checksum

    QLabel *rankingLabel = new QLabel(tr("<b>Rangliste:</b>"));
    rankingLabel->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    layout->addWidget(rankingLabel, 1, 0);

    QLabel *rankingChecksum = new QLabel;
    layout->addWidget(rankingChecksum, 1, 1);

    if (! db->localTournamentStarted()) {
        rankingChecksum->setText(tr("<i>Bisher wurden keine Ergebnisse eingetragen</i>"));
    } else {
        rankingChecksum->setTextInteractionFlags(Qt::TextSelectableByMouse);

        const auto [ ranking, success ] = db->ranking(-1);
        if (! success) {
            QTimer::singleShot(0, this, &QDialog::reject);
            return;
        }

        checksumHelper.clear();
        for (int i = 0; i < ranking.count(); i++) {
            const auto &rank = ranking.at(i);
            checksumHelper.addData(rank.name);
            checksumHelper.addData(rank.boogers);
            checksumHelper.addData(rank.goals);
            checksumHelper.addData(rank.opponentGoals);
        }
        rankingChecksum->setText(formatChecksum(checksumHelper.checksum()));
    }
}

QString ChecksumsDialog::formatChecksum(QString checksum) const
{
    for (int i = 4; i < checksum.size(); i += 5) {
        checksum.insert(i, QStringLiteral(" "));
    }
    return QStringLiteral("<pre>%1</pre>").arg(checksum);
}
