// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef WLANCODESCANNERSERVER_H
#define WLANCODESCANNERSERVER_H

#include <QTcpServer>

class WlanCodeScannerServer : public QTcpServer
{
    Q_OBJECT

public:
    explicit WlanCodeScannerServer(QObject *parent);
    void setContentKey(const QString &key);

Q_SIGNALS:
    void displayMessage(const QString &text);
    void codeReceived(const QString &data);

private Q_SLOTS:
    void processNewConnection();
    void dataReceived(const QString &data);

private: // Variables
    QString m_contentKey;

};

#endif // WLANCODESCANNERSERVER_H
