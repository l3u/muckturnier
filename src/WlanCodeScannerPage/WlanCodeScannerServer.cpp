// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "WlanCodeScannerServer.h"
#include "WlanCodeScannerRequest.h"

// Qt includes
#include <QTimer>
#include <QTcpSocket>

// C++ includes
#include <functional>

WlanCodeScannerServer::WlanCodeScannerServer(QObject *parent) : QTcpServer(parent)
{
    connect(this, &QTcpServer::newConnection, this, &WlanCodeScannerServer::processNewConnection);
}

void WlanCodeScannerServer::setContentKey(const QString &key)
{
    m_contentKey = key;
}

void WlanCodeScannerServer::processNewConnection()
{
    Q_EMIT displayMessage(tr("Eingehende Verbindung, verarbeite Anfrage …"));
    auto *connection = nextPendingConnection();
    auto *request = new WlanCodeScannerRequest(this, connection, m_contentKey);
    connect(connection, &QAbstractSocket::disconnected, connection, &QObject::deleteLater);
    connect(connection, &QAbstractSocket::disconnected, request, &QObject::deleteLater);
    connect(request, &WlanCodeScannerRequest::dataReceived,
            this, &WlanCodeScannerServer::dataReceived);
    connect(request, &QObject::destroyed, this,
            std::bind(&WlanCodeScannerServer::displayMessage, this, tr("Verbindung beendet")));
}

void WlanCodeScannerServer::dataReceived(const QString &data)
{
    if (! data.isEmpty()) {
        Q_EMIT displayMessage(tr("Code-Daten gefunden! Der Datensatz ist:"));
        Q_EMIT displayMessage(data);

        // Announce the scan when we're completely done here
        QTimer::singleShot(0, this, std::bind(&WlanCodeScannerServer::codeReceived, this, data));

    } else {
        Q_EMIT displayMessage(tr("Die Anfrage konnte nicht verarbeitet werden!"));
    }
}
