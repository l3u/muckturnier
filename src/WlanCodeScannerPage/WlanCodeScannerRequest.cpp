// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "WlanCodeScannerRequest.h"

#include "shared/DefaultValues.h"
#include "shared/Json.h"

// Qt includes
#include <QTcpSocket>
#include <QDebug>
#include <QUrl>
#include <QUrlQuery>
#include <QTextStream>
#include <QDateTime>
#include <QJsonObject>
#include <QJsonValue>

static const QByteArray s_specialFieldChars("!#$%&'*+-.^_`|~");
static const QByteArray s_urlEncoded("application/x-www-form-urlencoded");
static const QByteArray s_json("application/json");

WlanCodeScannerRequest::WlanCodeScannerRequest(QObject *parent, QTcpSocket *socket,
                                               const QString &contentKey)
    : QObject(parent),
      m_socket(socket),
      m_contentKey(contentKey),
      m_locale(QLocale::c())
{
    connect(m_socket, &QTcpSocket::readyRead, this, &WlanCodeScannerRequest::readyRead);
    m_fragment.reserve(1024);
}

bool WlanCodeScannerRequest::readByte()
{
    qint64 haveRead = 0;
    char c;

    while (true) {
        haveRead = m_socket->read(&c, 1);

        if (haveRead == -1) {
            // Unexpected EOF, this should not happen
            sendReply(QStringLiteral("400 Bad Request"),
                      tr("Konnte die Anfrage nicht verarbeiten: Unerwartetes EOF"));
            return false;

        } else if (haveRead == 0) {
            // Read more later
            return false;

        } else if (haveRead == 1 && m_fragment.size() == 0
                   && (c == '\r' || c == '\n' || c == '\t' || c == '\v' || c == ' ')) {
            // Ignore all whitespace when m_fragment is currently empty
            continue;
        }

        // We ignore CR to also allow using only LF as header field delimiter,
        // according to RFC 7230, 3.5. "Message Parsing Robustness"
        if (c != '\r') {
            m_fragment.append(c);
            break;
        }
    }

    if (m_fragment.size() > DefaultValues::maximumHttpFragmentSize) {
        sendReply(QStringLiteral("501 Not Implemented"),
                  tr("Die Anfrage ist zu lang"));
        return false;
    }

    return true;
}

bool WlanCodeScannerRequest::readContent()
{
    qint64 haveRead = 0;
    char c;

    while (true) {
        haveRead = m_socket->read(&c, 1);

        if (haveRead == -1) {
            // Unexpected EOF, this should not happen
            sendReply(QStringLiteral("400 Bad Request"),
                      tr("Konnte die Anfrage nicht verarbeiten: Unerwartetes EOF"));
            return false;

        } else if (haveRead == 0) {
            // Read more later
            return true;

        } else {
            m_fragment.append(c);
            if (m_fragment.size() > DefaultValues::maximumHttpFragmentSize) {
                sendReply(QStringLiteral("501 Not Implemented"),
                          tr("Die Anfrage ist zu lang"));
                return false;
            }
        }
    }

    return true;
}

QString WlanCodeScannerRequest::extractContentFromUrl() const
{
    // Construct a query
    const auto query = QUrlQuery(QUrl::fromEncoded(m_url));

    // Extract the content
    auto content = query.queryItemValue(m_contentKey);

    // Replace the "+" characters by spaces
    content.replace(QLatin1Char('+'), QLatin1Char(' '));

    // Unescape the data and return it
    return QString::fromUtf8(QByteArray::fromPercentEncoding(content.toUtf8())).trimmed();
}

void WlanCodeScannerRequest::sendReply(const QString &reply, const QString &message)
{
    Q_EMIT dataReceived(m_data);

    QTextStream stream(m_socket);
    stream << "HTTP/1.1 " << reply << "\r\n"
           << "Date: " << m_locale.toString(QDateTime::currentDateTimeUtc(),
                              QStringLiteral("ddd, dd MMM yyyy hh:mm:ss 'GMT'")) << "\r\n"
           << "Content-Type: text/plain; charset=UTF-8\r\n"
           << "Content-Length: " << message.toUtf8().size() << "\r\n"
           << "\r\n"
           << message;

    m_socket->close();
}

void WlanCodeScannerRequest::removeLeadingWhitespace()
{
    while (true) {
        const auto &first = m_fragment.at(0);
        if (first == '\t' || first == ' ') {
            m_fragment.remove(0, 1);
        } else {
            break;
        }
    }
}

void WlanCodeScannerRequest::readyRead()
{
    if (! m_socket->isValid()) {
        // The connection has been closed in the meantime
        return;
    }

    // Get the request type

    if (! m_requestLineRead) {
        while (readByte()) {
            if (m_fragment.endsWith('\n')) {
                m_requestLineRead = true;
                m_fragment.chop(1);
                break;
            }
        }

        if (m_requestLineRead) {
            // Get the request method
            int index = m_fragment.indexOf(' ');
            if (index == -1) {
                sendReply(QStringLiteral("400 Bad Request"),
                        tr("Konnte die Anfrage nicht verarbeiten: "
                           "Konnte den Anfragetyp nicht lesen"));
                return;
            }
            const auto method = m_fragment.left(index);
            if (method != "GET" && method != "POST") {
                sendReply(QStringLiteral("400 Bad Request"),
                        tr("Konnte die Anfrage nicht verarbeiten: "
                           "Es können nur GET- und POST-Anfragen verarbeitet werden"));
                return;
            }
            m_fragment.remove(0, index);
            m_fragment = m_fragment.trimmed();

            // Get the URL
            index = m_fragment.indexOf(' ');
            if (index == -1) {
                sendReply(QStringLiteral("400 Bad Request"),
                        tr("Konnte die Anfrage nicht verarbeiten: "
                           "Konnte die URL nicht auslesen"));
                return;
            }
            const auto url = m_fragment.left(index);
            m_fragment.remove(0, index);
            m_fragment = m_fragment.trimmed();

            // Get the HTTP version, this is the rest of the line
            if (m_fragment != "HTTP/1.1") {
                sendReply(QStringLiteral("505 HTTP Version Not Supported"),
                          tr("Konnte die Anfrage nicht verarbeiten: "
                             "Es können nur HTTP/1.1-Anfragen verarbeitet werden"));
                return;
            }

            m_method = method;
            m_url = url;
            m_fragment.clear();
        }
    }

    // If we received a GET request, we're done here already
    if (m_method == "GET") {
        m_data = extractContentFromUrl();
        m_dataParsed = true;
    }

    // If we received a POST request, we need the content type and the content length
    if (m_method == "POST") {
        // Read the header
        if (! m_headerComplete) {
            while (readByte()) {
                if (m_fragment.endsWith("\n\n")) {
                    m_fragment.chop(1);
                    m_headerComplete = true;
                    break;
                }
            }
        }

        if (m_headerComplete && m_contentType.isEmpty()) {
            int index = -1;

            while (m_fragment.size() > 0) {
                // Get the keys and values from the header

                // Read the field name
                index = m_fragment.indexOf(':');
                const auto rawFieldName = m_fragment.left(index);

                // Check the field name to be correct

                for (const auto &c : rawFieldName) {
                    if (! (('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z')
                           || ('0' <= c && c <= '9') || s_specialFieldChars.contains(c))) {

                        sendReply(QStringLiteral("400 Bad Request"),
                                  tr("Konnte die Anfrage nicht verarbeiten: "
                                     "Fehler beim Parsen des Headers"));
                        return;
                    }
                }

                // Parse the field name and remove it from the fragment.
                // Per RFC 7230, 3.2. "Header Fields", the field names are case-insensitive.
                const auto fieldName = QString::fromUtf8(rawFieldName).toLower();
                m_fragment.remove(0, index + 1);

                // Get the field value
                removeLeadingWhitespace();
                QByteArray fieldValue;
                fieldValue.reserve(128);
                while (true) {
                    index = m_fragment.indexOf('\n');
                    fieldValue.append(m_fragment.left(index).trimmed());
                    m_fragment.remove(0, index + 1);
                    if (m_fragment.size() == 0) {
                        break;
                    }

                    // Values can span multiple lines, indicated by a leading tab or space
                    const auto &first = m_fragment.at(0);
                    if (first == ' ' || first == '\t') {
                        removeLeadingWhitespace();
                    } else {
                        break;
                    }
                }

                // Check if it's something we want to know

                // Check for a suitable content type
                if (fieldName == QLatin1String("content-type")) {
                    if (fieldValue != s_urlEncoded && fieldValue != s_json) {
                        sendReply(QStringLiteral("400 Bad Request"),
                                  tr("Konnte die Anfrage nicht verarbeiten: "
                                     "Der Content-Type muss entweder "
                                     "application/x-www-form-urlencoded oder "
                                     "application/json sein"));
                        return;
                    }
                    m_contentType = fieldValue;

                } else if (fieldName == QLatin1String("content-length")) {
                    bool okay;
                    const auto intVal = fieldValue.toInt(&okay);
                    if (! okay) {
                        sendReply(QStringLiteral("400 Bad Request"),
                                  tr("Konnte die Anfrage nicht verarbeiten: "
                                     "Fehler beim Auslesen des Content-Length-Headers"));
                        return;
                    }
                    m_contentLength = intVal;
                }

                // Check if we already have what we need
                if (! m_contentType.isEmpty() && m_contentLength != -1) {
                    break;
                }
            }

            // Check if we could extract both the Content-Type and the Content-Length header
            if (m_contentType.isEmpty() || m_contentLength == -1) {
                sendReply(QStringLiteral("400 Bad Request"),
                          tr("Konnte die Anfrage nicht verarbeiten: "
                             "Fehler beim Auslesen des Content-Type- oder Content-Length-Headers"));
                return;
            } else {
                m_fragment.clear();
                m_fragment.reserve(1024);
            }
        }

        if (! m_contentType.isEmpty()) {
            // Read all content we can get
            if (! readContent()) {
                // An error occured whilst reading
                return;
            }

            if (m_fragment.size() < m_contentLength) {
                // We have to wait for more
                return;

            }

            if (m_contentType == s_urlEncoded) {
                m_url = "/?" + m_fragment;
                m_data = extractContentFromUrl();
                m_dataParsed = true;

            } else if (m_contentType == s_json) {
                const auto object = Json::objectFromByteArray(m_fragment);
                m_data = object.value(m_contentKey).toString().trimmed();
                m_dataParsed = true;
            }
        }
    }

    // If a dataset was parsed (successfully or not), we send a reply and close the connection
    if (m_dataParsed) {
        if (! m_data.isEmpty()) {
            sendReply(QStringLiteral("200 OK"),
                      tr("Code empfangen!"));
        } else {
            sendReply(QStringLiteral("400 Bad Request"),
                      tr("Konnte den Code nicht verarbeiten :-("));
        }
    }
}
