// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef WLANCODESCANNERPAGE_H
#define WLANCODESCANNERPAGE_H

// Local includes
#include "shared/OptionalPage.h"

// Qt includes
#include <QHash>

// Local classes
class Settings;
class SocketAddressWidget;
class LogDisplay;
class WlanCodeScannerServer;

// Qt classes
class QLabel;
class QPushButton;
class QTcpServer;
class QTcpSocket;

class WlanCodeScannerPage : public OptionalPage
{
    Q_OBJECT

public:
    explicit WlanCodeScannerPage(QWidget *parent, Settings *settings);
    void forceClosePage();

Q_SIGNALS:
    void statusUpdate(const QString &text);
    void codeReceived(const QString &data);

private Q_SLOTS:
    void startServer();
    bool stopServer();
    bool closePage() override;
    void showUrlInfo();

private: // Variables
    Settings *m_settings;
    WlanCodeScannerServer *m_server;
    QLabel *m_serverStatus;
    SocketAddressWidget *m_socketAddress;
    QPushButton *m_closeButton;
    QPushButton *m_startServer;
    QPushButton *m_stopServer;
    LogDisplay *m_display;

};

#endif // WLANCODESCANNERPAGE_H
