// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "WlanCodeScannerPage.h"
#include "WlanCodeScannerServer.h"

#include "network/SocketAddressWidget.h"
#include "network/LogDisplay.h"

#include "shared/SharedStyles.h"

#include "SharedObjects/Settings.h"

// Qt includes
#include <QHBoxLayout>
#include <QPushButton>
#include <QApplication>
#include <QStyle>
#include <QGroupBox>
#include <QLabel>
#include <QDebug>
#include <QMessageBox>
#include <QRegularExpression>

WlanCodeScannerPage::WlanCodeScannerPage(QWidget *parent, Settings *settings)
    : OptionalPage(parent),
      m_settings(settings)
{
    // Info label
    auto *info = new QLabel;
    info->setWordWrap(true);
    info->setTextInteractionFlags(Qt::TextBrowserInteraction);
    info->setText(tr("<a href=\"#\">Was ist das?</a>"));
    connect(info, &QLabel::linkActivated,
            this, [this]
            {
                QMessageBox::information(this, tr("WLAN-Code-Scanner-Server"), tr(
                    "<p>Mit dem WLAN-Code-Scanner-Server ist es möglich, eine passende "
                    "Smartphone-App anstatt eines Hardware-Barcode-Scanners zum Scannen von "
                    "Anmeldungscodes zu benutzen. Hierfür muss die App die gescannten Daten per "
                    "HTTP-Request an einen Server weitergeben können (und natürlich müssen dieser "
                    "Rechner und das Smartphone im selbem WLAN sein und sich erreichen können).</p>"
                    "<p>Eine App die das kann ist z.\u202FB. <a href=\"https://github.com/"
                    "markusfisch/BinaryEye\">Binary Eye</a>, ein kostenlos erhältlicher Open-"
                    "Source-Barcode-Scanner für Android (gibt es auf <a href=\"https://"
                    "f-droid.org/de/packages/de.markusfisch.android.binaryeye/\">F-Droid</a> "
                    "und auch im <a href=\"https://play.google.com/store/apps/details?id="
                    "de.markusfisch.android.binaryeye&amp;hl=de\">Google Play Store</a>).</p>"
                    "<p>Empfangene Codes werden an die Anmeldungsseite weitergegeben – so, als ob "
                    "ein Hardware-Barcode-Scanner benutzt worden wäre.</p>"));
            });
    layout()->addWidget(info);

    // Socket address

    auto *settingsBox = new QGroupBox(tr("Socket-Adresse für den Server"));
    auto *settingsLayout = new QHBoxLayout(settingsBox);
    layout()->addWidget(settingsBox);

    m_serverStatus = new QLabel(tr("<span style=\"%1\">Server<br/>gestoppt</span>").arg(
                              SharedStyles::redText));
    m_serverStatus->setTextInteractionFlags(Qt::TextSelectableByMouse | Qt::TextBrowserInteraction);
    connect(m_serverStatus, &QLabel::linkActivated, this, &WlanCodeScannerPage::showUrlInfo);
    settingsLayout->addWidget(m_serverStatus);

    m_socketAddress = new SocketAddressWidget(SocketAddressWidget::Server,
                                              m_settings->wlanCodeScannerServerPort(),
                                              m_settings->preferIpv6());
    settingsLayout->addWidget(m_socketAddress);

    // Buttons

    m_startServer = new QPushButton(tr("Server starten"));
    m_startServer->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogYesButton));
    connect(m_socketAddress, &SocketAddressWidget::settingsOkay,
            m_startServer, &QPushButton::setEnabled);
    connect(m_startServer, &QPushButton::clicked, this, &WlanCodeScannerPage::startServer);
    settingsLayout->addWidget(m_startServer);

    m_stopServer = new QPushButton(tr("Server stoppen"));
    m_stopServer->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogCancelButton));
    connect(m_stopServer, &QPushButton::clicked, this, &WlanCodeScannerPage::stopServer);
    settingsLayout->addWidget(m_stopServer);

    m_closeButton = new QPushButton(tr("Schließen"));
    m_closeButton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    m_closeButton->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogCloseButton));
    connect(m_closeButton, &QPushButton::clicked, this, &WlanCodeScannerPage::closePage);
    settingsLayout->addStretch();
    settingsLayout->addWidget(m_closeButton);

    // Server log
    auto *displayBox = new QGroupBox(tr("Serverlog"));
    auto *displayLayout = new QVBoxLayout(displayBox);
    layout()->addWidget(displayBox);
    m_display = new LogDisplay;
    displayLayout->addWidget(m_display);

    // Setup

    m_server = new WlanCodeScannerServer(this);
    connect(m_server, &WlanCodeScannerServer::displayMessage,
            m_display, &LogDisplay::displayMessage);
    connect(m_server, &WlanCodeScannerServer::codeReceived,
            this, &WlanCodeScannerPage::codeReceived);

    m_stopServer->hide();
    m_startServer->setFocus();
}

void WlanCodeScannerPage::startServer()
{
    m_display->clear();

    if (! m_server->listen(QHostAddress(m_socketAddress->ip()), m_socketAddress->port())) {
        m_display->displayMessage(tr("Der WLAN-Code-Scanner-Server konnte nicht gestartet werden: "
                                     "%1").arg(m_server->errorString()));
        QMessageBox::warning(this, tr("Server starten"),
            tr("<p>Der WLAN-Code-Scanner-Server konnte nicht gestartet werden:</p><p>%1</p>").arg(
               m_server->errorString()));
        return;
    }
    m_display->displayMessage(tr("WLAN-Code-Scanner-Server auf TCP-Port %1 gestartet").arg(
                                 m_socketAddress->port()));
    const auto contentKey = m_settings->wlanCodeScannerServerKey();
    m_server->setContentKey(contentKey);
    m_display->displayMessage(tr("Der Parameter für einen Datensatz ist „%1“").arg(contentKey));

    m_startServer->hide();
    m_socketAddress->hide();
    m_stopServer->show();
    m_closeButton->clearFocus();

    m_serverStatus->setText(tr("<span style=\"%1\">Server gestartet</span><br/>"
                               "IP: <b>%2</b> Port: <b>%3</b><br/>"
                               "<a href=\"#\">Infos zur URL für die App</a>").arg(
                               SharedStyles::greenText,
                               m_socketAddress->ip(),
                               QString::number(m_socketAddress->port())));

    Q_EMIT statusUpdate(tr("WLAN-Code-Scanner-Server unter IP %1 Port %2 gestartet").arg(
                           m_socketAddress->ip(), QString::number(m_socketAddress->port())));
}

bool WlanCodeScannerPage::stopServer()
{
    if (! m_server->isListening()) {
        return true;

    } else {
        if (QMessageBox::warning(this, tr("Server schließen"),
                tr("<p>Wenn der Server gestoppt wird, dann können keine gescannten Codes mehr "
                   "empfangen werden!</p>"
                   "<p>Soll der Server wirklich gestoppt werden?</p>"),
                QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Cancel)
                != QMessageBox::Yes) {

            return false;
        }
    }

    m_server->close();
    m_display->displayMessage(tr("WLAN-Code-Scanner-Server gestoppt"));

    m_serverStatus->setText(tr("<span style=\"%1\">Server<br/>gestoppt</span>").arg(
                               SharedStyles::redText));
    m_startServer->show();
    m_stopServer->hide();
    m_socketAddress->show();
    m_startServer->setFocus();

    Q_EMIT statusUpdate(tr("WLAN-Code-Scanner-Server gestoppt"));

    return true;
}

bool WlanCodeScannerPage::closePage()
{
    if (! stopServer()) {
        return false;
    }
    return OptionalPage::closePage();
}

void WlanCodeScannerPage::forceClosePage()
{
    if (m_server->isListening()) {
        m_server->close();
    }
    OptionalPage::closePage();
}

void WlanCodeScannerPage::showUrlInfo()
{
    QString ip = m_socketAddress->ip();
    const auto re = QRegularExpression(QStringLiteral("(\\d+\\.){3}\\d+"));
    const auto match = re.match(ip);
    if (! match.hasMatch()) {
        // IPv6 address --> add square brackets
        ip = QStringLiteral("[%1]").arg(ip);
    }

    QMessageBox::information(this, tr("WLAN-Code-Scanner-Server"), tr(
        "<p>Der Server unterstützt sowohl GET- als auch POST-Anfragen.</p>"
        "<h3>Verwendung von GET</h3>"
        "<p>Bei GET wird der Datensatz an die angefragte URL angehängt. Es muss ein "
        "Parametername dafür angegeben werden.</p>"
        "<p>Die aktuell passende URL für GET-Anfragen wäre:<br/>"
        "<b><kbd>http://%1:%2/?%3=</kbd></b></p>"
        "<h3>Verwendung von POST</h3>"
        "<p>Bei POST wird der Datensatz innerhalb des Datenanteils der Anfrage übermittelt. Auf "
        "die passende Benennung des Datensatzes durch die App achten, der Parametername muss "
        "<kbd>%3</kbd> sein!). Als Content-Type kann entweder "
        "<kbd>application/x-www-form-urlencoded</kbd> oder <kbd>application/json</kbd> benutzt "
        "werden.</p>"
        "<p>Die aktuell passende URL für POST-Anfragen wäre:<br/>"
        "<b><kbd>http://%1:%2/</kbd></b></p>"
        "<p>Am besten einfach ausprobieren :-)</p>").arg(
        ip, QString::number(m_socketAddress->port()), m_settings->wlanCodeScannerServerKey()));
}
