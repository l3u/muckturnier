// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef WLANCODESCANNERREQUEST_H
#define WLANCODESCANNERREQUEST_H

// Qt includes
#include <QObject>
#include <QLocale>

// Qt classes
class QTcpSocket;

class WlanCodeScannerRequest : public QObject
{
    Q_OBJECT

public:
    explicit WlanCodeScannerRequest(QObject *parent, QTcpSocket *socket, const QString &contentKey);

Q_SIGNALS:
    void dataReceived(const QString &data);

private Q_SLOTS:
    void readyRead();

private: // Functions
    bool readByte();
    QString extractContentFromUrl() const;
    void sendReply(const QString &reply, const QString &message);
    void removeLeadingWhitespace();
    bool readContent();

private: // Variables
    QTcpSocket *m_socket;
    const QString m_contentKey;
    const QLocale m_locale;

    QByteArray m_fragment;
    bool m_requestLineRead = false;
    QByteArray m_method;
    QByteArray m_url;
    bool m_headerComplete = false;
    QByteArray m_contentType;
    int m_contentLength = -1;
    QByteArray m_content;
    bool m_dataParsed = false;
    QString m_data;

};

#endif // WLANCODESCANNERREQUEST_H
