// SPDX-FileCopyrightText: 2020-2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "ScoreWidget.h"
#include "BoogerWidget.h"

#include "shared/SharedStyles.h"

// Qt includes
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QLabel>
#include <QDebug>

ScoreWidget::ScoreWidget(Tournament::ScoreType scoreType,
                         const QVector<BoogerWidget *> &boogerWidgets,
                         QPushButton *saveScore, QWidget *parent)
    : MinimumViewportWidget(parent)
{
    setStyleSheet(QStringLiteral("ScoreWidget { background-color: transparent; }"));

    switch (scoreType) {

    case Tournament::HorizontalScore: {
        QHBoxLayout *layout = new QHBoxLayout(this);
        layout->setContentsMargins(0, 0, 0, 0);

        for (BoogerWidget *boogerWidget : boogerWidgets) {
            layout->addWidget(boogerWidget);
        }

        layout->addWidget(saveScore);

        setMinimumViewport(sizeHint());

        } break;

    case Tournament::VerticalScore: {
        QHBoxLayout *layout = new QHBoxLayout(this);
        layout->setContentsMargins(0, 0, 0, 0);

        QGridLayout *boogerLayout = new QGridLayout;
        layout->addLayout(boogerLayout);
        int i = 0;
        for (BoogerWidget *boogerWidget : boogerWidgets) {
            QLabel *boogerHeader = new QLabel(tr("%1. Bobbl").arg(++i));
            boogerHeader->setStyleSheet(SharedStyles::boldText);
            boogerLayout->addWidget(boogerHeader, i - 1, 0);
            boogerLayout->addWidget(boogerWidget, i - 1, 1);
        }

        QVBoxLayout *saveLayout = new QVBoxLayout;
        saveLayout->setAlignment(Qt::AlignBottom);
        layout->addLayout(saveLayout);
        saveLayout->addWidget(saveScore);

        const int count = boogerWidgets.count();
        const QSize hint = sizeHint();
        setMinimumViewport(QSize(hint.width(), hint.height() / count * (count == 1 ? 1 : 2)));

        } break;

    }
}
