// SPDX-FileCopyrightText: 2010-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "BoogerRadioButton.h"
#include "BoogerSpinBox.h"

// Qt includes
#include <QDebug>
#include <QApplication>
#include <QWheelEvent>

BoogerRadioButton::BoogerRadioButton(QWidget *parent, BoogerSpinBox *spinBox)
    : QRadioButton(parent), m_spinBox(spinBox)
{
    setContextMenuPolicy(Qt::PreventContextMenu);
}

void BoogerRadioButton::wheelEvent(QWheelEvent *event)
{
    QRadioButton::wheelEvent(event);
    if (isChecked()) {
        QApplication::sendEvent(m_spinBox, event);
    }
}

void BoogerRadioButton::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::RightButton) {
        // Simulate a left mouse click so that the GUI responses accordingly
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
        QMouseEvent leftEvent(QEvent::MouseButtonPress, event->localPos(),
                              Qt::LeftButton, Qt::LeftButton, Qt::NoModifier);
#else
        QMouseEvent leftEvent(QEvent::MouseButtonPress, event->position(), event->globalPosition(),
                              Qt::LeftButton, Qt::LeftButton, Qt::NoModifier);
#endif
        QRadioButton::mousePressEvent(&leftEvent);
    } else {
        QRadioButton::mousePressEvent(event);
    }
}

void BoogerRadioButton::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::RightButton) {
        if (! isChecked()) {
            setChecked(true);
            Q_EMIT clicked();
        } else {
            m_spinBox->setFocus();
            m_spinBox->selectAll();
        }
        Q_EMIT scorePopupRequested(m_spinBox);
    }

    QRadioButton::mouseReleaseEvent(event);
}
