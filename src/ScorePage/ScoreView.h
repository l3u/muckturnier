// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SCOREVIEW_H
#define SCOREVIEW_H

// Local includes
#include "shared/TableView.h"

// Local classes
class ScoreModel;

class ScoreView : public TableView
{
    Q_OBJECT

public:
    explicit ScoreView(QWidget *parent = nullptr);
    void setModel(QAbstractItemModel *model) override;
    int selectedTable() const;
    const QString &selectedPair(int pair) const;
    void scrollToTable(int table);

Q_SIGNALS:
    void requestContextMenu();

public Q_SLOTS:
    void setAlternationStep(int rows);

protected Q_SLOTS:
    void dataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight,
                     const QVector<int> &roles = QVector<int>()) override;

private Q_SLOTS:
    void checkShowContextMenu(const QModelIndex &index);

private: // Variables
    ScoreModel *m_model;
    int m_selectedRow = -1;

};

#endif // SCOREVIEW_H
