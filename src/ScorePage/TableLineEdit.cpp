// SPDX-FileCopyrightText: 2019-2020 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "TableLineEdit.h"

// Qt includes
#include <QKeyEvent>
#include <QFocusEvent>
#include <QTimer>
#include <QDebug>

TableLineEdit::TableLineEdit(QWidget *parent)
    : StyledLineEdit(parent), m_noItemText(tr("–"))
{
}

const QString &TableLineEdit::noItemText() const
{
    return m_noItemText;
}

void TableLineEdit::keyPressEvent(QKeyEvent *event)
{
    const int key = event->key();

    // Disallow leading zeros and negative numbers
    if ((cursorPosition() == 0 || selectionStart() == 0)
        && (key == Qt::Key_0 || key == Qt::Key_Minus)) {

        return;
    }

    QLineEdit::keyPressEvent(event);
}

void TableLineEdit::focusInEvent(QFocusEvent *event)
{
    QLineEdit::focusInEvent(event);
    if (event->reason() == Qt::MouseFocusReason || event->reason() == Qt::TabFocusReason) {
        if (text() == m_noItemText) {
            clear();
        } else {
            QTimer::singleShot(0, this, &QLineEdit::selectAll);
        }
    }
}

void TableLineEdit::focusOutEvent(QFocusEvent *event)
{
    QLineEdit::focusOutEvent(event);
    if (text().isEmpty()) {
        setText(m_noItemText);
        setStyleSheet(QString());
    }
}
