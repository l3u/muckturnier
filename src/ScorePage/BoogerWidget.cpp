// SPDX-FileCopyrightText: 2010-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "BoogerWidget.h"
#include "BoogerSpinBox.h"
#include "BoogerRadioButton.h"

#include "shared/SharedStyles.h"

// Qt includes
#include <QDebug>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QCheckBox>
#include <QApplication>
#include <QStyle>

BoogerWidget::BoogerWidget(QWidget *parent, Tournament::ScoreType scoreType,
                           int booger, int boogersPerRound, int boogerScore)
    : QWidget(parent),
      m_boogerScore(boogerScore),
      m_boogerNumber(booger)
{
    setAutoFillBackground(false);

    // The "Aborted" button
    m_boogerAborted = new QPushButton;
    m_boogerAborted->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogCancelButton));
    m_boogerAborted->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    m_boogerAborted->setCheckable(true);
    m_boogerAborted->setFlat(true);
    m_boogerAborted->setToolTip(boogersPerRound > 1
        ? tr("<p>Als abgebrochen markieren</p>"
             "<p>Wenn diese Option gesetzt ist, wird der Bobbl und alle folgenden als abgebrochen "
             "markiert (z.&thinsp;B. wenn mit einer Zeitbegrenzung pro Runde gespielt wird, und "
             "die Zeit abgelaufen ist).</p>"
             "<p>In einem abgebrochenen Bobbl können zwei Spielstände eingegeben werden, "
             "entsprechend dem jeweils erreichten Punktestand. Auswertungstechnisch haben beide "
             "Paare den Bobbl verloren und bekommen die Punkte als „geschossene Tore“ "
             "gutgeschrieben.")
        : tr("<p>Als abgebrochen markieren</p>"
             "<p>Wenn diese Option gesetzt ist, wird die Runde (also der einzige Bobbl) als "
             "abgebrochen markiert (z.&thinsp;B. wenn mit einer Zeitbegrenzung pro Runde gespielt "
             "wird, und die Zeit abgelaufen ist).</p>"
             "<p>In einer abgebrochenen Runde können zwei Spielstände eingegeben werden, "
             "entsprechend dem jeweils erreichten Punktestand. Auswertungstechnisch haben beide "
             "Paare die Runde verloren und bekommen die Punkte als „geschossene Tore“ "
             "gutgeschrieben.")
    );

    connect(m_boogerAborted, &QPushButton::clicked, this, &BoogerWidget::abortedButtonClicked);

    // The boogers and scores

    m_score.reserve(2);
    m_booger.reserve(2);

    for (int i = 0; i < 2; i++) {
        BoogerSpinBox *spinBox = new BoogerSpinBox;
        spinBox->setMaximum(m_boogerScore - (i == 0));
        connect(spinBox, &BoogerSpinBox::scorePopupRequested,
                this, &BoogerWidget::scorePopupRequested);
        if (booger == boogersPerRound) {
            connect(spinBox, &BoogerSpinBox::returnPressed,
                    this, &BoogerWidget::returnPressed);
        }

        BoogerRadioButton *radioButton = new BoogerRadioButton(this, spinBox);
        connect(radioButton, &QRadioButton::clicked, this, &BoogerWidget::toggleScore);
        connect(radioButton, &BoogerRadioButton::scorePopupRequested,
                this, &BoogerWidget::scorePopupRequested);

        m_score.append(spinBox);
        m_booger.append(radioButton);
    }

    // Set the default values
    m_booger.at(0)->setChecked(true);
    m_score.at(1)->setValue(m_boogerScore);
    m_score.at(1)->setEnabled(false);

    // Create the layout

    QLabel *boogerHeader = new QLabel;
    boogerHeader->setStyleSheet(SharedStyles::boldText);

    switch (scoreType) {

    case Tournament::HorizontalScore: {
        QGridLayout *layout = new QGridLayout(this);

        QHBoxLayout *headerLayout = new QHBoxLayout;
        layout->addLayout(headerLayout, 0, 0, 1, 2);

        boogerHeader->setText(boogersPerRound > 1 ? tr("%1. Bobbl").arg(booger)
                                                  : tr("Ergebnis"));
        headerLayout->addWidget(boogerHeader);

        headerLayout->addStretch();

        headerLayout->addWidget(m_boogerAborted);

        for (int i = 0; i < 2; i++) {
            layout->addWidget(new QLabel(tr("Paar %1").arg(i + 1)), i + 1, 0);
            layout->addWidget(m_score.at(i), i + 1, 1);
            layout->addWidget(m_booger.at(i), i + 1, 2);
        }

        } break;

    case Tournament::VerticalScore: {
        QHBoxLayout *layout = new QHBoxLayout(this);

        QFrame *line = new QFrame;
        line->setFrameShape(QFrame::VLine);
        line->setProperty("isseparator", true);
        layout->addWidget(line);

        for (int i = 0; i < 2; i++) {
            layout->addWidget(new QLabel(tr("Paar %1:").arg(i + 1)));
            layout->addWidget(m_score.at(i));
            layout->addWidget(m_booger.at(i));

            QFrame *line = new QFrame;
            line->setFrameShape(QFrame::VLine);
            line->setProperty("isseparator", true);
            layout->addWidget(line);
        }

        layout->addWidget(m_boogerAborted);

        } break;

    }
}

void BoogerWidget::toggleScore()
{
    for (int i = 0; i < 2; i++) {
        if (m_booger.at(i)->isChecked()) {
            m_score.at(i)->setMaximum(m_boogerScore - 1);
            m_score.at(i)->setValue(0);
            m_score.at(i)->setEnabled(true);
            m_score.at(i)->setFocus();
            m_score.at(i)->selectAll();
        } else {
            m_score.at(i)->setMaximum(m_boogerScore);
            m_score.at(i)->setValue(m_boogerScore);
            m_score.at(i)->setEnabled(false);
        }
    }
}

int BoogerWidget::score(int pair) const
{
    return m_score.at(pair - 1)->value();
}

void BoogerWidget::setScore(int pair1Score, int pair2Score)
{
    if (pair1Score == m_boogerScore || pair2Score == m_boogerScore) {
        m_booger.at(0)->setChecked(pair1Score != m_boogerScore);
        m_booger.at(1)->setChecked(pair2Score != m_boogerScore);
        toggleScore();
    }
    m_score.at(0)->setValue(pair1Score);
    m_score.at(1)->setValue(pair2Score);
    m_score.at(0)->deselect();
    m_score.at(1)->deselect();
    m_score.at(0)->clearFocus();
    m_score.at(1)->clearFocus();
}

void BoogerWidget::abortedButtonClicked(bool aborted)
{
    Q_EMIT abortedStateChanged(m_boogerNumber - 1, aborted);
    setAborted(aborted);
    if (aborted) {
        setAsFirstAborted();
        m_score.at(0)->setFocus();
        m_score.at(0)->selectAll();
    }
}

void BoogerWidget::setAsFirstAborted()
{
    m_score.at(0)->setEnabled(true);
    m_score.at(1)->setEnabled(true);
}

void BoogerWidget::setAborted(bool aborted)
{
    if (m_boogerAborted->isChecked() != aborted) {
        m_boogerAborted->setChecked(aborted);
    }
    if (aborted) {
        for (int i = 0; i < 2; i++) {
            m_booger.at(i)->setAutoExclusive(false);
            m_booger.at(i)->setChecked(true);
            m_booger.at(i)->setEnabled(false);
            m_score.at(i)->setEnabled(true);
            m_score.at(i)->setValue(0);
            m_score.at(i)->setMaximum(m_boogerScore - 1);
            m_score.at(i)->setEnabled(false);
        }
    } else {
        for (int i = 0; i < 2; i++) {
            m_booger.at(i)->setAutoExclusive(true);
            m_booger.at(i)->setEnabled(true);
        }
        m_booger.at(1)->setChecked(false);
        toggleScore();
    }
}

void BoogerWidget::reset()
{
    if (m_boogerAborted->isChecked()) {
        setAborted(false);
    }
    m_booger.at(0)->setChecked(true);
    toggleScore();
}

bool BoogerWidget::isAborted() const
{
    return m_boogerAborted->isChecked();
}
