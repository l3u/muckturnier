// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SCOREMODEL_H
#define SCOREMODEL_H

// Local includes

#include "shared/Scores.h"

#include "SharedObjects/SharedObjects.h"

// Qt includes
#include <QAbstractTableModel>

// Local classes
class Database;
class TournamentSettings;
class StringTools;

class ScoreModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit ScoreModel(QObject *parent, SharedObjects *sharedObjects);
    int rowCount(const QModelIndex & = QModelIndex()) const override;
    int columnCount(const QModelIndex & = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;

    void setIsPairMode(bool state);
    void scoreTypeChanged();
    void refresh(int round);
    int rowsPerTable() const;
    int resultsCount() const;
    int tableForRow(int row) const;
    int rowForTable(int table) const;
    const QString &pairForRow(int row, int pair) const;
    bool rowContainsPlayersNumber(int row, int number) const;
    const Scores::Score &score() const;
    const QVector<QString> &assembledPairs() const;

Q_SIGNALS:
    void alternationStepChanged(int rows);

private: // Functions
    void clear();
    void processChangedData();

private: // Variables
    Database *m_db;
    TournamentSettings *m_tournamentSettings;
    StringTools *m_stringTools;
    int m_columnCount = 0;
    int m_rowCount = 0;
    bool m_oneBoogerPerRound = false;
    bool m_isHorizontalScore = true;
    bool m_isPairMode = true;
    int m_rowsPerTable = 2;
    Scores::Score m_score;
    QVector<QString> m_displayPairs;
    QVector<QString> m_assembledPairs;

};

#endif // SCOREMODEL_H
