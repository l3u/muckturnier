// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "VisibilityWatcher.h"

// Qt includes
#include <QDebug>
#include <QEvent>

VisibilityWatcher::VisibilityWatcher(QObject *parent) : QObject(parent)
{
}

bool VisibilityWatcher::eventFilter(QObject *object, QEvent *event)
{
    const auto type = event->type();
    if (type == QEvent::Show) {
        Q_EMIT visibilityChanged(true);
    } else if (type == QEvent::Hide) {
        Q_EMIT visibilityChanged(false);
    }

    // standard event processing
    return QObject::eventFilter(object, event);
}
