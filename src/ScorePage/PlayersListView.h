// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef PLAYERSLISTVIEW_H
#define PLAYERSLISTVIEW_H

// Qt includes
#include <QListView>

class PlayersListView : public QListView
{
    Q_OBJECT

public:
    explicit PlayersListView(QWidget *parent = nullptr);

Q_SIGNALS:
    void popupHidden();

protected:
    void hideEvent(QHideEvent *event) override;

};

#endif // PLAYERSLISTVIEW_H
