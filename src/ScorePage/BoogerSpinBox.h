// SPDX-FileCopyrightText: 2010-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef BOOGERSPINBOX_H
#define BOOGERSPINBOX_H

// Qt includes
#include <QSpinBox>

class BoogerSpinBox : public QSpinBox
{
    Q_OBJECT

public:
    explicit BoogerSpinBox(QWidget *parent = nullptr);
    void deselect();

Q_SIGNALS:
    void returnPressed();
    void scorePopupRequested(BoogerSpinBox *spinBox);

protected:
    void contextMenuEvent(QContextMenuEvent *) override;
    void focusInEvent(QFocusEvent *event) override;

};

#endif // BOOGERSPINBOX_H
