// SPDX-FileCopyrightText: 2010-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef BOOGERWIDGET_H
#define BOOGERWIDGET_H

// Local includes
#include "shared/Tournament.h"

// Qt includes
#include <QWidget>
#include <QPushButton>

// Qt Classes
class QPushButton;

// Local classes
class BoogerSpinBox;
class BoogerRadioButton;

class BoogerWidget : public QWidget
{
    Q_OBJECT

public:
    explicit BoogerWidget(QWidget *parent, Tournament::ScoreType scoreType,
                          int booger, int boogersPerRound, int boogerScore);
    int score(int pair) const;
    void setScore(int pair1Score, int pair2Score);
    void setAsFirstAborted();
    void setAborted(bool aborted);
    void reset();
    bool isAborted() const;

Q_SIGNALS:
    void returnPressed();
    void abortedStateChanged(int booger, bool aborted);
    void scorePopupRequested(BoogerSpinBox *spinBox);

private Q_SLOTS:
    void toggleScore();
    void abortedButtonClicked(bool aborted);

private: // Variables
    const int m_boogerScore;
    const int m_boogerNumber;
    QVector<BoogerSpinBox *> m_score;
    QVector<BoogerRadioButton *> m_booger;
    QPushButton *m_boogerAborted;

};

#endif // BOOGERWIDGET_H
