// SPDX-FileCopyrightText: 2020 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SCOREWIDGET_H
#define SCOREWIDGET_H

// Local includes
#include "shared/MinimumViewportWidget.h"
#include "shared/Tournament.h"

// Local classes
class BoogerWidget;

// Qt classes
class QPushButton;

class ScoreWidget : public MinimumViewportWidget
{
    Q_OBJECT

public:
    explicit ScoreWidget(Tournament::ScoreType scoreType,
                         const QVector<BoogerWidget *> &boogerWidgets,
                         QPushButton *saveScore, QWidget *parent = nullptr);

};

#endif // SCOREWIDGET_H
