// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SCOREPOPUP_H
#define SCOREPOPUP_H

// Local includes
#include "shared/SelectionPopup.h"

class ScorePopup : public SelectionPopup
{
    Q_OBJECT

public:
    explicit ScorePopup(QWidget *parent, const int *boogerScore);
    void setupGui();
    int score() const;

private Q_SLOTS:
    void scoreClicked(QPushButton *button);

private: // Variables
    const int *m_boogerScore;
    int m_score;

};

#endif // SCOREPOPUP_H
