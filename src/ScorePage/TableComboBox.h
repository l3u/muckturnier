// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef TABLECOMBOBOX_H
#define TABLECOMBOBOX_H

// Local includes
#include "shared/NumbersComboBox.h"

// Qt includes
#include <QListView>
#include <QStandardItemModel>

// Local classes
class TableLineEdit;

// Qt classes
class QLineEdit;
class QIntValidator;

class TableComboBox : public NumbersComboBox
{
    Q_OBJECT

public:
    explicit TableComboBox(QWidget *parent = nullptr);
    void clear();
    void addItem(const QString &text, const QVariant &userData);
    bool setCurrentNumber(int number);
    bool inputOkay() const;

Q_SIGNALS:
    void checkSavePossible();

private Q_SLOTS:
    void lineEditTextChanged(const QString &text);
    void tableSelected();

private: // Variables
    TableLineEdit *m_lineEdit;
    QIntValidator *m_validator;
    bool m_inputOkay = true;

};

#endif // TABLECOMBOBOX_H
