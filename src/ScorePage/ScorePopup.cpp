// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "ScorePopup.h"

// Qt includes
#include <QDebug>
#include <QGridLayout>
#include <QPushButton>

// C++ includes
#include <functional>

ScorePopup::ScorePopup(QWidget *parent, const int *boogerScore)
    : SelectionPopup(parent), m_boogerScore(boogerScore)
{
}

void ScorePopup::setupGui()
{
    QWidget *scoreWidget = new QWidget;
    QGridLayout *layout = new QGridLayout(scoreWidget);

    int maximumColumns;
    if (*m_boogerScore <= 31) {
        maximumColumns = 5;
    } else {
        maximumColumns = 10;
    }

    int row = -1;
    int currentScore = -1;
    bool finished = false;
    while (! finished) {
        row++;
        for (int column = 0; column < maximumColumns; column++) {
            currentScore++;
            if (currentScore == *m_boogerScore) {
                finished = true;
                break;
            }

            // We use a custiom version of SelectionPopup::textButton here to get square buttons
            auto *scoreButton = new QPushButton(QString::number(currentScore));
            const QFontMetrics fontMetrics(scoreButton->font());
            const auto height = scoreButton->sizeHint().height() + fontMetrics.height() * 0.3;
            scoreButton->setMinimumHeight(height);
            scoreButton->setMinimumWidth(height);

            layout->addWidget(scoreButton, row, column);
            connect(scoreButton, &QPushButton::clicked,
                    this, std::bind(&ScorePopup::scoreClicked, this, scoreButton));
        }
    }

    setScrollWidget(scoreWidget);
}

void ScorePopup::scoreClicked(QPushButton *button)
{
    m_score = button->text().toInt();
    // This is needed to prevent the button to be highlighted afterwards on Windows
    button->clearFocus();
    accept();
}

int ScorePopup::score() const
{
    return m_score;
}
