// SPDX-FileCopyrightText: 2010-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SCOREPAGE_H
#define SCOREPAGE_H

// Local includes

#include "Database/Database.h"

#include "shared/Tournament.h"

// Qt includes
#include <QWidget>
#include <QMap>

// Local classes
class SharedObjects;
class StringTools;
class TournamentSettings;
class SearchEngine;
class BoogerWidget;
class TitleMenu;
class NumbersComboBox;
class PlayersComboBox;
class BoogerSpinBox;
class ScorePopup;
class SearchWidget;
class Settings;
class TableComboBox;
class PlayersWidget;
class ScoreWidget;
class PhoneticSearchButton;
class IconButton;
class MinimumViewportScroll;
class SyncDisplay;
class Server;
class Client;
class ScoreModel;
class ScoreView;
class ChecksumHelper;
class DrawModeMenu;

// Local structs
namespace Scores
{
struct Score;
}

// Qt classes
class QGroupBox;
class QPushButton;
class QAction;
class QLabel;
class QHBoxLayout;
class QSplitter;
class QMenu;
class QStackedLayout;

class ScorePage : public QWidget
{
    Q_OBJECT

public:
    explicit ScorePage(QWidget *parent, SharedObjects *sharedObjects);
    void setupGui();
    void setUsable(bool state);
    void setNetworkBlocked(bool state);
    void reload(int lastRound = -1);
    void setAutoSelectTableForDraw(bool state);
    void toggleAutoSelectTableForDraw();
    const QMap<int, QString> &checksums() const;

    void serverStarted(Server *server);
    void serverStopped();
    void clientConnected(Client *client);
    void clientDisconnected();

    void initializeChecksums();
    void setServerChecksum(int round, const QString &checksum);
    void setClientChecksum(int clientId, int round, const QString &checksum);
    void removeClientChecksums(int clientId);
    void setProcessingRequest(bool processing);

Q_SIGNALS:
    void statusUpdate(const QString &message);
    void tournamentStarted();
    void scoreChanged();
    void requestCompareScoreDialog(int remoteId, const int *currentRound,
                                   const Scores::Score *localScore) const;
    void roundSelectionChanged();
    void finishRegistration() const;
    void requestDrawNextRound(const QVector<QString> &names, int drawRound, bool raise);

public Q_SLOTS:
    void resetSearch();
    void updateAssignment(int round);
    void playersNameEdited();

private Q_SLOTS:
    void openRound(int round);
    void tableChanged(int table);
    void playersChanged(int index, int pairId);
    void saveScore();
    void showContextMenu();
    void editScore();
    void deleteScore();
    void checkEnterScore();
    void updateSearch(const QString &searchText);
    void blockSelectSignals(bool state);
    void swapPairs();
    void drawModeChanged();
    void boogerAbortedStateChanged(int booger, bool aborted);
    void checkSavePossible();
    void showScorePopup(BoogerSpinBox *spinBox);
    void setSelectedScore();
    void scoreTypeChanged(Tournament::ScoreType scoreType);
    void processLinkActivated(const QString &anchor) const;
    void swapPlayers(int pair);

private: // Functions
    void insertAssignment(int id, const Draw::Seat &seat);
    void updateScore();
    void doDeleteScore(int round, int table);
    QString formatOpponentsString(const QString &pair1, const QString &pair2) const;
    void fillInScore(int round, int table, const QVector<Database::BoogerResult> &score);
    QVector<Database::BoogerResult> currentScore() const;
    void addPairResultToChecksum(ChecksumHelper &checksumHelper, const QString &pair,
                                 const QVector<int> &scores) const;
    QString scoreChecksum(const Scores::Score &score) const;
    void updateLastChange(QString &cache);
    void updateSyncStatus();
    QVector<QString> playersForTable(int table) const;

private: // Variables
    Database *m_db;
    Settings *m_settings;
    TournamentSettings *m_tournamentSettings;
    StringTools *m_stringTools;
    SearchEngine *m_searchEngine;
    ScorePopup *m_scorePopup;
    BoogerSpinBox *m_targetSpinBox;

    QStackedLayout *m_layout;

    QLabel *m_networkBlocked;

    QGroupBox *m_roundBox;
    NumbersComboBox *m_roundSelect;

    DrawModeMenu *m_drawModeMenu;
    QAction *m_autoSelectTableForDraw;
    QMenu *m_scoreTypeMenu;
    QAction *m_horizontalScore;
    QAction *m_verticalScore;

    QGroupBox *m_resultBox;
    QSplitter *m_splitter;

    QHBoxLayout *m_playersLayout;
    PlayersWidget *m_playersWidget = nullptr;
    IconButton *m_noAutoselect;
    ScoreWidget *m_scoreWidget = nullptr;
    PhoneticSearchButton *m_phoneticSearch;
    TableComboBox *m_tableSelect;
    QVector<PlayersComboBox *> m_playersSelect;
    MinimumViewportScroll *m_scoreBoxScroll;
    QVector<BoogerWidget *> m_boogerWidgets;
    QPushButton *m_saveScoreButton;

    QGroupBox *m_roundSummaryBox;
    ScoreModel *m_scoreModel;
    ScoreView *m_scoreView;
    SearchWidget *m_searchWidget;
    QLabel *m_isSearchLabel;

    TitleMenu *m_contextMenu;
    QAction *m_drawNextRoundAction;
    QAction *m_editAction;
    QAction *m_deleteAction;
    QAction *m_swapPairsAction;
    QMenu *m_swapPlayersMenu;
    QAction *m_swapPair1PlayersAction;
    QAction *m_swapPair2PlayersAction;
    QLabel *m_tableLabel;

    int m_lastOpenedRound;
    int m_selectedTable;
    int m_scoreSize;

    QHash<int, int> m_assignedTable;
    QHash<int, QVector<int>> m_assignedIds;

    Server *m_server = nullptr;
    Client *m_client = nullptr;
    bool m_isServer = false;
    bool m_isClient = false;

    SyncDisplay *m_syncDisplay;
    QMap<int, QString> m_checksums;
    QMap<int, QString> m_serverChecksums;
    QMap<int, QMap<int, QString>> m_clientChecksums;
    QString m_lastServerChange;
    QString m_lastLocalChange;
    QHash<int, QString> m_lastClientChange;
    int m_currentRound = -1;

};

#endif // SCOREPAGE_H
