// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "ScoreView.h"
#include "ScoreModel.h"

#include "shared/TableDelegate.h"

// Qt includes
#include <QDebug>
#include <QMouseEvent>
#include <QTimer>

ScoreView::ScoreView(QWidget *parent) : TableView(parent, TableDelegate::AlternatingNRows)
{
    setEditTriggers(QAbstractItemView::NoEditTriggers);
    setContextMenuPolicy(Qt::CustomContextMenu);

    connect(this, &QTableView::doubleClicked, this, &ScoreView::checkShowContextMenu);
    connect(this, &QTableView::customContextMenuRequested,
            this, [this](const QPoint &pos)
            {
                checkShowContextMenu(indexAt(pos));
            });
}

void ScoreView::setModel(QAbstractItemModel *model)
{
    m_model = qobject_cast<ScoreModel *>(model);
    QTableView::setModel(model);
}

void ScoreView::setAlternationStep(int rows)
{
    setRowsPerDataset(rows);
    tableDelegate()->setAlternationStep(rows);
}

void ScoreView::dataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight,
                            const QVector<int> &roles)
{
    // First clear all spans
    clearSpans();

    // Pass the update to the base class
    QTableView::dataChanged(topLeft, bottomRight, roles);

    // Update the spans
    for (int row = 0; row < m_model->rowCount(); row += m_model->rowsPerTable()) {
        setSpan(row, 0, m_model->rowsPerTable(), 1);
    }

    // Resize the columns
    QTimer::singleShot(0, this, &QTableView::resizeColumnsToContents);
}

void ScoreView::checkShowContextMenu(const QModelIndex &index)
{
    updateHoveredRow(index);
    if (hoveredRow() != -1) {
        // Store the currently hovered row as the selected one. The hovered row should not change
        // as long as the context menu is opened and/or an action is triggered by it however ...
        m_selectedRow = hoveredRow();
        Q_EMIT requestContextMenu();
    }
}

int ScoreView::selectedTable() const
{
    return m_model->tableForRow(m_selectedRow);
}

const QString &ScoreView::selectedPair(int pair) const
{
    return m_model->pairForRow(m_selectedRow, pair);
}

void ScoreView::scrollToTable(int table)
{
    const auto row = m_model->rowForTable(table);
    if (row == -1) {
        // This should not happen
        return;
    }

    const auto index = m_model->index(row, 0);
    if (row == m_model->rowCount() - m_model->rowsPerTable()) {
        scrollToBottom();
    } else {
        scrollTo(index, QAbstractItemView::PositionAtCenter);
    }

    updateHoveredRow(index);
}
