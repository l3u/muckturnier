// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef PLAYERSCOMBOBOX_H
#define PLAYERSCOMBOBOX_H

// Local includes
#include "shared/NumbersComboBox.h"
#include "shared/Players.h"

// Local classes
class SearchEngine;
class PlayersLineEdit;

// Qt classes
class QStringListModel;
class QStandardItemModel;

class PlayersComboBox : public NumbersComboBox
{
    Q_OBJECT

public:
    explicit PlayersComboBox(SearchEngine *searchEngine, int index, QWidget *parent = nullptr);
    void addEntry(const Players::DisplayData &data, const QString &displayName);
    QString currentName() const;
    QString itemName(int index) const;
    bool selectName(const QString &name);

Q_SIGNALS:
    void disableSaveScore();
    void checkSavePossible();
    void selectionChanged(int pair, int pairId);

public Q_SLOTS:
    void lineEditTextChanged(const QString &text);
    void setPhoneticSearch(bool state);

private: // Enums
    enum Data {
        Id     = Qt::UserRole,
        Name   = Qt::UserRole + 1,
        Number = Qt::UserRole + 2
    };

private Q_SLOTS:
    void resetLineEditText();
    void completerActivated(const QString &text);
    void processGivenText();
    void popupHidden();
    void finishSearch();

private: // Variables
    SearchEngine *m_searchEngine;
    const int m_index = -1;
    QStandardItemModel *m_model;
    PlayersLineEdit *m_lineEdit;
    QStringListModel *m_matches;
    bool m_isSearching = false;
    bool m_completerActivated = false;
    bool m_phoneticSearch = false;

};

#endif // PLAYERSCOMBOBOX_H
