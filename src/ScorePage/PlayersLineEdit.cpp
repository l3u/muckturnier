// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "PlayersLineEdit.h"

// Qt includes
#include <QKeyEvent>
#include <QFocusEvent>
#include <QTimer>
#include <QDebug>

PlayersLineEdit::PlayersLineEdit(QWidget *parent) : StyledLineEdit(parent)
{
}

void PlayersLineEdit::keyPressEvent(QKeyEvent *event)
{
    QLineEdit::keyPressEvent(event);
    if (event->key() == Qt::Key_Escape) {
        Q_EMIT escapePressed();
    }
}

void PlayersLineEdit::focusInEvent(QFocusEvent *event)
{
    QLineEdit::focusInEvent(event);
    if (event->reason() == Qt::MouseFocusReason || event->reason() == Qt::TabFocusReason) {
        QTimer::singleShot(0, this, &QLineEdit::selectAll);
    }
}
