// SPDX-FileCopyrightText: 2020 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef PLAYERSWIDGET_H
#define PLAYERSWIDGET_H

// Local includes
#include "shared/Tournament.h"

// Qt includes
#include <QWidget>

// Local classes
class PlayersComboBox;

// Qt classes
class QLabel;

class PlayersWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PlayersWidget(Tournament::ScoreType scoreType,
                           QLabel *tableLabel,
                           QWidget *table,
                           const QVector<PlayersComboBox *> &players,
                           QWidget *phoneticSearch,
                           QWidget *parent = nullptr);

};

#endif // PLAYERSWIDGET_H
