// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "PlayersComboBox.h"
#include "PlayersLineEdit.h"
#include "PlayersListView.h"

#include "SharedObjects/SearchEngine.h"

// Qt includes
#include <QDebug>
#include <QStringListModel>
#include <QCompleter>
#include <QStandardItemModel>

PlayersComboBox::PlayersComboBox(SearchEngine *searchEngine, int index, QWidget *parent)
    : NumbersComboBox(parent),
      m_searchEngine(searchEngine),
      m_index(index)
{
    m_model = qobject_cast<QStandardItemModel *>(model());

    setEditable(true);
    setInsertPolicy(QComboBox::NoInsert);
    setCompleter(nullptr);

    connect(this, QOverload<int>::of(&QComboBox::activated), this, &PlayersComboBox::finishSearch);
    connect(this, QOverload<int>::of(&QComboBox::currentIndexChanged),
            this, [this]
            {
                Q_EMIT selectionChanged(m_index, currentNumber());
            });

    m_matches = new QStringListModel(this);
    QCompleter *completer = new QCompleter(m_matches, this);
    completer->setCompletionMode(QCompleter::UnfilteredPopupCompletion);
    connect(completer, QOverload<const QString &>::of(&QCompleter::activated),
            this, &PlayersComboBox::completerActivated);

    PlayersListView *listView = new PlayersListView;
    completer->setPopup(listView);
    connect(listView, &PlayersListView::popupHidden, this, &PlayersComboBox::popupHidden);

    m_lineEdit = new PlayersLineEdit;
    m_lineEdit->setCompleter(completer);
    setLineEdit(m_lineEdit);

    disconnect(m_lineEdit, &QLineEdit::editingFinished, 0, 0);
    connect(m_lineEdit, &QLineEdit::textEdited, this, &PlayersComboBox::lineEditTextChanged);
    connect(m_lineEdit, &PlayersLineEdit::escapePressed,
            this, &PlayersComboBox::resetLineEditText);
    connect(m_lineEdit, &QLineEdit::editingFinished, this, &PlayersComboBox::processGivenText);

    m_isSearching = false;
}

void PlayersComboBox::addEntry(const Players::DisplayData &data, const QString &displayName)
{
    // Construct the item with the appropriate name
    auto *item = new QStandardItem(displayName);
    // Add the registration's ID
    item->setData(data.id, Data::Id);
    // Add the (unchanged) registered name we use for searching
    item->setData(data.name, Data::Name);
    // Add the players number
    item->setData(data.number, Data::Number);
    // Append the entry to the model
    m_model->appendRow(item);
}

QString PlayersComboBox::currentName() const
{
    return currentData(Data::Name).toString();
}

QString PlayersComboBox::itemName(int index) const
{
    return itemData(index, Data::Name).toString();
}

void PlayersComboBox::lineEditTextChanged(const QString &text)
{
    if (! m_isSearching) {
        m_isSearching = true;
        m_completerActivated = false;
        m_lineEdit->displayInvalidInput(true);
        Q_EMIT disableSaveScore();
    }

    bool isNumber = false;
    const auto number = text.toInt(&isNumber);

    QStringList matches;

    if (isNumber && number > 0) {
        // We do a pure number search. Only check the players number
        for(int i = 0; i < m_model->rowCount(); i++) {
            const auto *item = m_model->item(i, 0);
            if (item->data(Data::Number).toInt() == number) {
                matches.append(item->text());
                break;
            }
        }

    } else {
        // We query the search engine
        m_searchEngine->setSearchTerm(text);
        for(int i = 0; i < m_model->rowCount(); i++) {
            const auto *item = m_model->item(i, 0);
            // Check the original registered name for a match ...
            if (m_searchEngine->checkMatch(item->data(Data::Name).toString(),
                                           SearchEngine::DefaultSearch,
                                           m_phoneticSearch)) {
                // ... but add the displayed name if we found a match
                matches.append(item->text());
            }
        }
    }

    m_matches->setStringList(matches);
}

void PlayersComboBox::resetLineEditText()
{
    m_lineEdit->setText(itemText(currentIndex()));
    finishSearch();
    m_lineEdit->setFocus();
}

void PlayersComboBox::processGivenText()
{
    if (! m_isSearching || m_completerActivated) {
        return;
    }

    if (m_matches->rowCount() == 1) {
        completerActivated(m_matches->index(0, 0).data().toString());
        return;
    }

    resetLineEditText();
}

void PlayersComboBox::completerActivated(const QString &text)
{
    m_completerActivated = true;
    setCurrentIndex(findText(text));
    finishSearch();
}

void PlayersComboBox::finishSearch()
{
    if (m_isSearching) {
        m_isSearching = false;
        m_lineEdit->displayInvalidInput(false);
    }

    Q_EMIT checkSavePossible();
}

void PlayersComboBox::popupHidden()
{
    if (m_completerActivated || m_matches->rowCount() != 1) {
        return;
    }

    const QString onlyMatch = m_matches->index(0, 0).data().toString();
    if (m_lineEdit->text() != onlyMatch) {
        completerActivated(onlyMatch);
    }
}

void PlayersComboBox::setPhoneticSearch(bool state)
{
    m_phoneticSearch = state;
    if (m_isSearching) {
        resetLineEditText();
    }
}

bool PlayersComboBox::selectName(const QString &name)
{
    if (currentName() == name) {
        return true;
    }

    for (int i = 0; i < m_model->rowCount(); i++) {
        if (m_model->item(i)->data(Data::Name).toString() == name) {
            setCurrentIndex(i);
            return true;
        }
    }

    return false;
}
