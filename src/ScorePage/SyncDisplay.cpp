// SPDX-FileCopyrightText: 2022-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "SyncDisplay.h"

#include "shared/MinimumViewportWidget.h"

// Qt includes
#include <QVBoxLayout>
#include <QLabel>

SyncDisplay::SyncDisplay(QWidget *parent) : MinimumViewportScroll(Qt::Horizontal, parent)
{
    setWidgetResizable(true);
    setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Ignored);
    setFrameShape(QFrame::NoFrame);

    m_displayWidget = new MinimumViewportWidget;
    auto *layout = new QVBoxLayout(m_displayWidget);
    layout->setContentsMargins(0, 0, 0, 0);

    m_lastChange = new QLabel;
    layout->addWidget(m_lastChange);

    m_syncStatus = new QLabel;
    m_syncStatus->setTextInteractionFlags(Qt::TextBrowserInteraction);
    connect(m_syncStatus, &QLabel::linkActivated, this, &SyncDisplay::linkActivated);
    layout->addWidget(m_syncStatus);

    layout->addStretch();

    setWidget(m_displayWidget);
}

void SyncDisplay::setLastChange(const QString &text)
{
    m_lastChange->setText(text);
}

void SyncDisplay::setSyncStatus(const QString &text)
{
    m_syncStatus->setText(text.isEmpty()
        ? tr("<i>Bisher keine Ergebnisse<br/>an Clients eingegeben</i>")
        : text);
}
