// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "PlayersListView.h"

// Qt includes
#include <QDebug>

PlayersListView::PlayersListView(QWidget *parent) : QListView(parent)
{
    setEditTriggers(QAbstractItemView::NoEditTriggers);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setSelectionBehavior(QAbstractItemView::SelectRows);
    setSelectionMode(QAbstractItemView::SingleSelection);
    setModelColumn(0);
}

void PlayersListView::hideEvent(QHideEvent *event)
{
    QListView::hideEvent(event);
    Q_EMIT popupHidden();
}
