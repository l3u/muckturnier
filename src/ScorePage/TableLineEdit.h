// SPDX-FileCopyrightText: 2019-2020 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef TABLELINEEDIT_H
#define TABLELINEEDIT_H

// Local includes
#include "StyledLineEdit.h"

// Qt classes
class QKeyEvent;
class QFocusEvent;

class TableLineEdit : public StyledLineEdit
{
    Q_OBJECT

public:
    explicit TableLineEdit(QWidget *parent = nullptr);
    const QString &noItemText() const;

protected:
    void keyPressEvent(QKeyEvent *event) override;
    void focusInEvent(QFocusEvent *event) override;
    void focusOutEvent(QFocusEvent *event) override;

private: // Variables
    const QString m_noItemText;

};

#endif // TABLELINEEDIT_H
