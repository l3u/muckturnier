// SPDX-FileCopyrightText: 2010-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "BoogerSpinBox.h"

// Qt includes
#include <QDebug>
#include <QLineEdit>
#include <QFocusEvent>
#include <QTimer>

BoogerSpinBox::BoogerSpinBox(QWidget *parent) : QSpinBox(parent)
{
    setWrapping(true);
    connect(lineEdit(), &QLineEdit::returnPressed, this, &BoogerSpinBox::returnPressed);
}

void BoogerSpinBox::contextMenuEvent(QContextMenuEvent *)
{
    Q_EMIT scorePopupRequested(this);
}

void BoogerSpinBox::focusInEvent(QFocusEvent *event)
{
    QSpinBox::focusInEvent(event);
    if (event->reason() == Qt::MouseFocusReason || event->reason() == Qt::TabFocusReason) {
        QTimer::singleShot(0, this, &QSpinBox::selectAll);
    }
}

void BoogerSpinBox::deselect()
{
    lineEdit()->deselect();
}
