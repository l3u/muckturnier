// SPDX-FileCopyrightText: 2019-2020 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "StyledLineEdit.h"

#include "shared/SharedStyles.h"

StyledLineEdit::StyledLineEdit(QWidget *parent) : QLineEdit(parent)
{
}

void StyledLineEdit::displayInvalidInput(bool state)
{
    setStyleSheet(state ? SharedStyles::redText : QString());
}
