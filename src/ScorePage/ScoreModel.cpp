// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "ScoreModel.h"

#include "shared/Tournament.h"
#include "shared/Logging.h"

#include "Database/Database.h"

#include "SharedObjects/TournamentSettings.h"
#include "SharedObjects/StringTools.h"

// Qt includes
#include <QDebug>

ScoreModel::ScoreModel(QObject *parent, SharedObjects *sharedObjects)
    : QAbstractTableModel(parent),
      m_db(sharedObjects->database()),
      m_tournamentSettings(sharedObjects->tournamentSettings()),
      m_stringTools(sharedObjects->stringTools())
{
}

int ScoreModel::rowCount(const QModelIndex &) const
{
    return m_rowCount;
}

int ScoreModel::columnCount(const QModelIndex &) const
{
    return m_columnCount;
}

QVariant ScoreModel::data(const QModelIndex &index, int role) const
{
    if (! index.isValid()) {
        return QVariant();
    }

    const auto row = index.row();
    const auto column = index.column();
    if (row >= m_rowCount || column >= m_columnCount) {
        return QVariant();
    }

    if (role != Qt::DisplayRole) {
        return QVariant();
    }

    const auto subRow = row % m_rowsPerTable;

    if (column == 0 && subRow == 0) {
        // Table number
        return m_score.tables.at(row / m_rowsPerTable);
    }

    // Horizontal score mode is quite easy :-)
    if (m_isHorizontalScore) {
        if (column == 1) {
            // Pair 1 or 2
            return m_displayPairs.at(row);
        } else if (column > 1) {
            // Pair 1 or 2 results
            return m_stringTools->boogify(m_score.scores.at(row).at(column - 2));
        }

    // Vertical score mode is a bit more complicated ;-)
    } else {
        if (subRow == 0) {
            if (column == 2 - m_oneBoogerPerRound) {
                // Pair 1
                return m_displayPairs.at(row / m_rowsPerTable * 2);
            } else if (column == 3 - m_oneBoogerPerRound) {
                // Pair 2
                return m_displayPairs.at(row / m_rowsPerTable * 2 + 1);
            }

        } else {
            if (column == 1 - m_oneBoogerPerRound) {
                // The booger number
                // If m_oneBoogerPerRound is true (which will evaluate to 1 in the above
                // subtraction) we can't reach here and the booger number will be omitted
                return tr("%1.").arg(subRow);
            } else if (column == 2 - m_oneBoogerPerRound) {
                // Pair 1 results
                return m_stringTools->boogify(
                           m_score.scores.at(row / m_rowsPerTable * 2).at(subRow - 1));
            } else if (column == 3 - m_oneBoogerPerRound) {
                // Pair 2 results
                return m_stringTools->boogify(
                           m_score.scores.at(row / m_rowsPerTable * 2 + 1).at(subRow - 1));
            }
        }
    }

    return QVariant();
}

QVariant ScoreModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole || orientation == Qt::Vertical) {
        return QVariant();
    }

    if (section == 0) {
        return tr("Tisch");
    }

    if (m_isHorizontalScore) {
        if (section == 1) {
            return tr("Paare");
        } else {
            return m_oneBoogerPerRound ? tr("Ergebnis") : tr("%1. Bobbl").arg(section - 1);
        }
    } else { // vertical score
        if (m_oneBoogerPerRound) {
            if (section == 1) {
                return tr("Paar 1");
            } else if (section == 2) {
                return tr("Paar 2");
            }
        } else {
            if (section == 1) {
                return tr("Bobbl");
            } else if (section == 2) {
                return tr("Paar 1");
            } else if (section == 3) {
                return tr("Paar 2");
            }
        }
    }

    return QVariant();
}

void ScoreModel::setIsPairMode(bool state)
{
    m_isPairMode = state;
}

void ScoreModel::scoreTypeChanged()
{
    // Clear the model for the view, without actually touching the data (which stays unchanged)
    beginRemoveRows(QModelIndex(), 0, m_rowCount - 1);
    endRemoveRows();

    // Update the parameters

    m_oneBoogerPerRound = m_tournamentSettings->boogersPerRound() == 1;

    switch (m_tournamentSettings->scoreType()) {
    case Tournament::HorizontalScore:
        m_columnCount = 2 + m_tournamentSettings->boogersPerRound();
        m_rowsPerTable = 2;
        m_isHorizontalScore = true;
        break;
    case Tournament::VerticalScore:
        m_columnCount = 4 - m_oneBoogerPerRound;
        m_rowsPerTable = 1 + m_tournamentSettings->boogersPerRound();
        m_isHorizontalScore = false;
        break;
    }

    // Announce the header label change
    Q_EMIT headerDataChanged(Qt::Horizontal, 1, m_columnCount);

    // Display the new data
    processChangedData();
    Q_EMIT alternationStepChanged(m_rowsPerTable);
}

int ScoreModel::rowsPerTable() const
{
    return m_rowsPerTable;
}

int ScoreModel::resultsCount() const
{
    return m_score.tables.count();
}

void ScoreModel::clear()
{
    beginRemoveRows(QModelIndex(), 0, m_rowCount - 1);
    m_score = Scores::Score();
    m_displayPairs.clear();
    m_assembledPairs.clear();
    endRemoveRows();

    Q_EMIT dataChanged(index(0, 0), index(m_rowCount - 1, m_columnCount - 1), { Qt::DisplayRole });

    m_rowCount = 0;
}

void ScoreModel::processChangedData()
{
    m_rowCount = m_score.tables.count() * m_rowsPerTable;

    beginInsertRows(QModelIndex(), 0, m_rowCount - 1);
    endInsertRows();

    Q_EMIT dataChanged(index(0, 0), index(m_rowCount - 1, m_columnCount - 1), { Qt::DisplayRole });
}

void ScoreModel::refresh(int round)
{
    clear();

    const auto [ score, success ] = m_db->scores(round);
    if (! success) {
        return;
    }

    m_score = score;

    for (int i = 0; i < m_score.pairsOrPlayers1.count(); i++) {
        if (m_isPairMode) {
            m_displayPairs.append(m_stringTools->pairOrPlayerName(m_score.pairsOrPlayers1.at(i),
                                                                  StringTools::FormattedName));
        } else {
            m_displayPairs.append(m_stringTools->pairName(m_score.pairsOrPlayers1.at(i),
                                                          m_score.players2.at(i),
                                                          StringTools::FormattedName));
            m_assembledPairs.append(m_stringTools->pairName(m_score.pairsOrPlayers1.at(i),
                                                            m_score.players2.at(i)));
        }
    }

    processChangedData();
}

int ScoreModel::tableForRow(int row) const
{
    Q_ASSERT_X(row % m_rowsPerTable == 0, "ScoreModel::tableForRow", "Invalid row requested");
    return m_score.tables.at(row / m_rowsPerTable);
}

int ScoreModel::rowForTable(int table) const
{
    const auto index = m_score.tables.indexOf(table);

    if (index == -1) {
        qCWarning(MuckturnierLog) << "ScoreModel::rowForTable: Row for table" << table
                                  << "requested, but the table is not present!";
        return -1;
    }

    return index * m_rowsPerTable;
}

const QString &ScoreModel::pairForRow(int row, int pair) const
{
    Q_ASSERT_X(row % m_rowsPerTable == 0, "ScoreModel::pairsForRow", "Invalid row requested");
    Q_ASSERT_X(pair == 1 || pair == 2, "ScoreModel::pairsForRow", "Invalid pair number requested");
    const auto index = row / m_rowsPerTable * 2 + pair - 1;
    return m_isPairMode ? m_score.pairsOrPlayers1.at(index).name
                        : m_assembledPairs.at(index);
}

bool ScoreModel::rowContainsPlayersNumber(int row, int number) const
{
    Q_ASSERT_X(row % m_rowsPerTable == 0, "ScoreModel::rowContainsPlayersNumber",
                                          "Invalid row requested");
    const auto index = row / m_rowsPerTable * 2;
    if (m_isPairMode) {
        return    m_score.pairsOrPlayers1.at(index).number == number
               || m_score.pairsOrPlayers1.at(index + 1).number == number;
    } else {
        return    m_score.pairsOrPlayers1.at(index).number == number
               || m_score.pairsOrPlayers1.at(index + 1).number == number
               || m_score.players2.at(index).number == number
               || m_score.players2.at(index + 1).number == number;
    }
}

const Scores::Score &ScoreModel::score() const
{
    return m_score;
}

const QVector<QString> &ScoreModel::assembledPairs() const
{
    return m_assembledPairs;
}
