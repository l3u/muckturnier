// SPDX-FileCopyrightText: 2010-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "ScorePage.h"
#include "BoogerWidget.h"
#include "PlayersComboBox.h"
#include "BoogerSpinBox.h"
#include "ScorePopup.h"
#include "TableComboBox.h"
#include "PlayersWidget.h"
#include "ScoreWidget.h"
#include "SyncDisplay.h"
#include "VisibilityWatcher.h"
#include "ScoreModel.h"
#include "ScoreView.h"

#include "ServerPage/Server.h"
#include "ClientPage/Client.h"
#include "network/ChecksumHelper.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/StringTools.h"
#include "SharedObjects/SearchEngine.h"
#include "SharedObjects/Settings.h"
#include "SharedObjects/TournamentSettings.h"

#include "shared/SharedStyles.h"
#include "shared/SearchWidget.h"
#include "shared/TitleMenu.h"
#include "shared/PhoneticSearchButton.h"
#include "shared/IconButton.h"
#include "shared/Scores.h"
#include "shared/MinimumViewportScroll.h"
#include "shared/NumbersComboBox.h"
#include "shared/DrawModeHelper.h"
#include "shared/DrawModeMenu.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QGridLayout>
#include <QCheckBox>
#include <QPushButton>
#include <QTableView>
#include <QAction>
#include <QMessageBox>
#include <QApplication>
#include <QScrollBar>
#include <QLineEdit>
#include <QSplitter>
#include <QActionGroup>
#include <QTime>
#include <QTimer>
#include <QStackedLayout>

// C++ includes
#include <functional>
#include <utility>

ScorePage::ScorePage(QWidget *parent, SharedObjects *sharedObjects)
    : QWidget(parent),
      m_db(sharedObjects->database()),
      m_settings(sharedObjects->settings()),
      m_tournamentSettings(sharedObjects->tournamentSettings()),
      m_stringTools(sharedObjects->stringTools()),
      m_searchEngine(sharedObjects->searchEngine())
{
    m_layout = new QStackedLayout(this);

    // The "Not usable" label
    auto *notUsable = new QLabel(tr("<i>Es können noch keine Ergebnisse eingetragen werden</i>"));
    notUsable->setAlignment(Qt::AlignCenter);
    notUsable->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    m_layout->addWidget(notUsable);

    // A wrapper widget for the form
    auto *formWidget = new QWidget;
    auto *layout = new QVBoxLayout(formWidget);
    m_layout->addWidget(formWidget);

    // Info label about finishing the registration is mandatory in network mode
    m_networkBlocked = new QLabel(tr(
        "<p>Im Netzwerkbetrieb muss die Anmeldung explizit abgeschlossen werden,<br/>"
        "bevor Ergebnisse eingegeben werden können. <a href=\"#finishRegistration\">"
        "Anmeldung beenden</a></p>"));
    m_networkBlocked->setTextInteractionFlags(Qt::TextBrowserInteraction);
    connect(m_networkBlocked, &QLabel::linkActivated, this, &ScorePage::processLinkActivated);
    m_networkBlocked->setAlignment(Qt::AlignCenter);
    m_networkBlocked->setWordWrap(true);
    layout->addWidget(m_networkBlocked);
    m_networkBlocked->hide();

    // Round selection part
    // ====================

    m_roundBox = new QGroupBox;
    QHBoxLayout *roundBoxLayout = new QHBoxLayout(m_roundBox);
    layout->addWidget(m_roundBox);

    QLabel *roundLabel = new QLabel(tr("Runde:"));
    roundLabel->setStyleSheet(SharedStyles::boldText);
    roundBoxLayout->addWidget(roundLabel);

    m_roundSelect = new NumbersComboBox;
    roundBoxLayout->addWidget(m_roundSelect);
    connect(m_roundSelect, &NumbersComboBox::currentNumberChanged, this, &ScorePage::openRound);

    roundBoxLayout->addStretch();

    QPushButton *optionsButton = new QPushButton(tr("Einstellungen"));

    QMenu *optionsMenu = new QMenu(this);

    m_drawModeMenu = new DrawModeMenu(DrawModeMenu::SelectTexts, m_tournamentSettings, this);
    optionsMenu->addMenu(m_drawModeMenu);
    connect(m_drawModeMenu, &DrawModeMenu::drawModeChanged, this, &ScorePage::drawModeChanged);

    m_autoSelectTableForDraw = optionsMenu->addAction(tr("Tisch automatisch für Auslosung "
                                                         "auswählen"));
    m_autoSelectTableForDraw->setCheckable(true);
    connect(m_autoSelectTableForDraw, &QAction::toggled,
            m_tournamentSettings, &TournamentSettings::setAutoSelectTableForDraw);

    QActionGroup *scoreTypeGroup = new QActionGroup(this);

    m_horizontalScore = new QAction(tr("horizontal"), scoreTypeGroup);
    m_horizontalScore->setCheckable(true);
    connect(m_horizontalScore, &QAction::triggered, this,
            std::bind(&ScorePage::scoreTypeChanged, this, Tournament::HorizontalScore));

    m_verticalScore = new QAction(tr("vertikal"), scoreTypeGroup);
    m_verticalScore->setCheckable(true);
    connect(m_verticalScore, &QAction::triggered, this,
            std::bind(&ScorePage::scoreTypeChanged, this, Tournament::VerticalScore));

    optionsMenu->addSeparator();

    m_scoreTypeMenu = optionsMenu->addMenu(tr("Spielstandzettel"));
    m_scoreTypeMenu->addAction(m_horizontalScore);
    m_scoreTypeMenu->addAction(m_verticalScore);

    optionsButton->setMenu(optionsMenu);
    roundBoxLayout->addWidget(optionsButton);

    // Entering results part
    // =====================

    m_resultBox = new QGroupBox(tr("Ergebnisse eintragen"));
    QVBoxLayout *resultBoxLayout = new QVBoxLayout(m_resultBox);

    // Table and pairs/players

    m_playersLayout = new QHBoxLayout;
    resultBoxLayout->addLayout(m_playersLayout);
    resultBoxLayout->setSizeConstraint(QLayout::SetMaximumSize);

    m_tableLabel = new QLabel(tr("Tisch:"));
    m_tableLabel->setStyleSheet(SharedStyles::boldText);

    m_tableSelect = new TableComboBox;
    connect(m_tableSelect, &TableComboBox::currentNumberChanged, this, &ScorePage::tableChanged);
    connect(m_tableSelect, &TableComboBox::checkSavePossible, this, &ScorePage::checkSavePossible);

    m_phoneticSearch = new PhoneticSearchButton(QStringLiteral("score_page_players"),
                                                sharedObjects);

    m_playersSelect.reserve(4);
    for (int i = 0; i < 4; i++) {
        PlayersComboBox *players = new PlayersComboBox(m_searchEngine, i);
        connect(players, &PlayersComboBox::selectionChanged,
                this, &ScorePage::playersChanged);
        connect(players, &PlayersComboBox::disableSaveScore,
                this, [this]
                {
                    m_saveScoreButton->setEnabled(false);
                });
        connect(players, &PlayersComboBox::checkSavePossible,
                this, &ScorePage::checkSavePossible);
        connect(m_phoneticSearch, &PhoneticSearchButton::toggled,
                players, &PlayersComboBox::setPhoneticSearch);
        m_playersSelect.append(players);
    }

    m_noAutoselect = new IconButton(sharedObjects, QStringLiteral("noautoselect.png"), this);
    m_noAutoselect->setToolTip(tr("<p>Automatische Paar-, Spieler- bzw. Tischauswahl "
                                  "(de)aktivieren</p>"
                                  "<p>Wenn diese Option aktiviert ist, können Ergebnisse "
                                  "eingegeben werden, die nicht zur Auslosung passen.</p>"));

    auto *line = new QFrame;
    line->setFrameShape(QFrame::HLine);
    line->setProperty("isseparator", true);
    resultBoxLayout->addWidget(line);

    // Score input

    m_scoreBoxScroll = new MinimumViewportScroll(Qt::Vertical);
    m_scoreBoxScroll->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Maximum);
    m_scoreBoxScroll->setFrameShape(QFrame::NoFrame);
    resultBoxLayout->addWidget(m_scoreBoxScroll);

    m_saveScoreButton = new QPushButton(tr("Ergebnis\neintragen"));
    connect(m_saveScoreButton, &QPushButton::clicked, this, &ScorePage::saveScore);

    // Round summary
    // =============

    m_roundSummaryBox = new QGroupBox;
    m_roundSummaryBox->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    auto *roundSummaryBoxLayout = new QVBoxLayout(m_roundSummaryBox);

    // Search Label
    m_isSearchLabel = new QLabel;
    m_isSearchLabel->setAlignment(Qt::AlignCenter);
    m_isSearchLabel->hide();
    roundSummaryBoxLayout->addWidget(m_isSearchLabel);

    // Scores

    auto *scoresLayout = new QHBoxLayout;
    roundSummaryBoxLayout->addLayout(scoresLayout);

    m_scoreModel = new ScoreModel(this, sharedObjects);
    m_scoreView = new ScoreView;
    m_scoreView->setModel(m_scoreModel);
    connect(m_scoreModel, &ScoreModel::alternationStepChanged,
            m_scoreView, &ScoreView::setAlternationStep);
    connect(m_scoreView, &ScoreView::requestContextMenu, this, &ScorePage::showContextMenu);
    scoresLayout->addWidget(m_scoreView);

    // Search widget
    m_searchWidget = new SearchWidget(QStringLiteral("score_page_search"), sharedObjects);
    connect(m_searchWidget, &SearchWidget::searchChanged, this, &ScorePage::updateSearch);
    roundSummaryBoxLayout->addWidget(m_searchWidget);

    // Server/Client score sync display
    m_syncDisplay = new SyncDisplay;
    connect(m_syncDisplay, &SyncDisplay::linkActivated, this, &ScorePage::processLinkActivated);
    scoresLayout->addWidget(m_syncDisplay);
    m_syncDisplay->hide();

    // Splitter
    // ========

    m_splitter = new QSplitter(Qt::Vertical);
    m_splitter->setHandleWidth(3);

    m_splitter->addWidget(m_resultBox);
    m_splitter->setCollapsible(0, false);
    m_splitter->setStretchFactor(0, 1);

    m_splitter->addWidget(m_roundSummaryBox);
    m_roundSummaryBox->setMinimumHeight(100);
    m_splitter->setCollapsible(1, false);
    m_splitter->setStretchFactor(1, 100);

    // Automatically show or hide the splitter's handle according to
    // the scroll area's vertical scroll bar being shown or hidden
    auto *visibilityWatcher = new VisibilityWatcher(this);
    m_scoreBoxScroll->verticalScrollBar()->installEventFilter(visibilityWatcher);
    connect(visibilityWatcher, &VisibilityWatcher::visibilityChanged,
            this, [this](bool isVisible)
            {
                m_splitter->setStyleSheet(isVisible ? SharedStyles::visibleVerticalSplitter
                                                    : SharedStyles::hiddenSplitter);
            });

    layout->addWidget(m_splitter);

    // Context menu
    // ============

    m_contextMenu = new TitleMenu(this);

    m_drawNextRoundAction = m_contextMenu->addAction(tr("Für die nächste Runde auslosen"));
    connect(m_drawNextRoundAction, &QAction::triggered, this, [this]
    {
        Q_EMIT requestDrawNextRound(playersForTable(m_selectedTable),
                                    m_roundSelect->currentNumber(), true);
    });

    m_contextMenu->addSeparator();

    m_editAction = m_contextMenu->addAction(tr("Ändern (Löschen und neu eingeben)"));
    connect(m_editAction, &QAction::triggered, this, &ScorePage::editScore);

    m_swapPairsAction = m_contextMenu->addAction(tr("Paar 1 und Paar 2 vertauschen"));
    connect(m_swapPairsAction, &QAction::triggered, this, &ScorePage::swapPairs);

    m_swapPlayersMenu = m_contextMenu->addMenu(tr("Spieler vertauschen"));
    m_swapPair1PlayersAction = m_swapPlayersMenu->addAction(tr("Für Paar 1"));
    connect(m_swapPair1PlayersAction, &QAction::triggered,
            this, std::bind(&ScorePage::swapPlayers, this, 1));
    m_swapPair2PlayersAction = m_swapPlayersMenu->addAction(tr("Für Paar 2"));
    connect(m_swapPair2PlayersAction, &QAction::triggered,
            this, std::bind(&ScorePage::swapPlayers, this, 2));

    m_contextMenu->addSeparator();

    m_deleteAction = m_contextMenu->addAction(tr("Löschen"));
    connect(m_deleteAction, &QAction::triggered, this, &ScorePage::deleteScore);

    // Score dialog
    // ------------
    m_scorePopup = new ScorePopup(this, &m_tournamentSettings->boogerScore());
    connect(m_scorePopup, &QDialog::accepted, this, &ScorePage::setSelectedScore);
}

void ScorePage::setupGui()
{
    // Setup the players part
    // ----------------------

    if (m_playersWidget != nullptr) {
        m_tableLabel->setParent(this);
        m_tableSelect->setParent(this);
        for (PlayersComboBox *players : std::as_const(m_playersSelect)) {
            players->setParent(this);
        }
        m_phoneticSearch->setParent(this);
        m_playersWidget->deleteLater();
        m_playersLayout->removeWidget(m_noAutoselect);
    }

    m_playersWidget = new PlayersWidget(m_tournamentSettings->scoreType(),
                                        m_tableLabel,
                                        m_tableSelect,
                                        m_playersSelect,
                                        m_phoneticSearch);
    m_playersLayout->addWidget(m_playersWidget);
    m_playersLayout->addWidget(m_noAutoselect);

    // Show the needed input fields for pairs or players
    m_playersSelect.at(1)->setVisible(m_tournamentSettings->isSinglePlayerMode());
    m_playersSelect.at(3)->setVisible(m_tournamentSettings->isSinglePlayerMode());

    // Show or hide the "no autoselect" button according to the tournament settings
    m_noAutoselect->setVisible(m_tournamentSettings->autoSelectPairs());

    // Setup the score part
    // --------------------

    if (m_scoreWidget != nullptr) {
        m_saveScoreButton->setParent(this);
        m_scoreWidget->deleteLater();
    }

    m_boogerWidgets.clear();
    const Tournament::ScoreType scoreType = m_tournamentSettings->scoreType();
    for (int i = 1; i <= m_tournamentSettings->boogersPerRound(); i++) {
        BoogerWidget *boogerWidget = new BoogerWidget(this, scoreType, i,
                                                      m_tournamentSettings->boogersPerRound(),
                                                      m_tournamentSettings->boogerScore());
        connect(boogerWidget, &BoogerWidget::abortedStateChanged,
                this, &ScorePage::boogerAbortedStateChanged);
        connect(boogerWidget, &BoogerWidget::scorePopupRequested,
                this, &ScorePage::showScorePopup);

        m_boogerWidgets.append(boogerWidget);
    }
    connect(m_boogerWidgets.last(), &BoogerWidget::returnPressed,
            this, &ScorePage::checkEnterScore);

    m_scoreSize = m_boogerWidgets.count() * m_db->necessaryDivisor();

    m_scoreWidget = new ScoreWidget(scoreType, m_boogerWidgets, m_saveScoreButton);
    m_saveScoreButton->setEnabled(false);
    m_scoreBoxScroll->setWidget(m_scoreWidget);

    m_horizontalScore->blockSignals(true);
    m_verticalScore->blockSignals(true);
    m_horizontalScore->setChecked(scoreType == Tournament::HorizontalScore);
    m_verticalScore->setChecked(scoreType == Tournament::VerticalScore);
    m_horizontalScore->blockSignals(false);
    m_verticalScore->blockSignals(false);

    const auto dbIsChangeable = m_db->isChangeable();

    // Disable data input if we have a read-only database
    m_resultBox->setEnabled(dbIsChangeable);
    m_scoreTypeMenu->setEnabled(dbIsChangeable);
    m_drawModeMenu->setEnabled(dbIsChangeable);
    m_autoSelectTableForDraw->setEnabled(dbIsChangeable);

    // Inform the score view
    m_scoreModel->setIsPairMode(m_tournamentSettings->isPairMode());
    m_scoreModel->scoreTypeChanged();
    m_scoreView->setTrackHovering(dbIsChangeable);

    // Set the "Swap players" menu's visibility
    m_swapPlayersMenu->menuAction()->setVisible(m_tournamentSettings->isSinglePlayerMode());

    // Update the score dialog
    m_scorePopup->setupGui();

    // This hopefully makes the resultBoxLayout adopt a potentially changed m_scoreWidget height
    m_resultBox->adjustSize();

    // If we're in vertical mode: Try to show all booger widgets without a scroll bar.

    // FIXME: Apparently, there's no reliable way to know when a widget and a
    // layout will have their definitive geometry. So we do this using a delayed
    // QTimer single shot, cross our fingers and hope for the best ...

    if (m_tournamentSettings->scoreType() == Tournament::VerticalScore
        && m_tournamentSettings->boogersPerRound() > 2) {

        // Let's hope 100 ms are enough ;-)
        QTimer::singleShot(100, this, [this]
        {
            const auto sizes = m_splitter->sizes();
            auto topHeight = m_resultBox->sizeHint().height();

            if (sizes.first() == topHeight) {
                return;
            }

            int size = 0;
            for (const auto i : sizes) {
                size += i;
            }

            int bottomHeight = size - topHeight;
            const auto bottomMinimumHeight = m_roundSummaryBox->minimumHeight();
            if (bottomHeight < bottomMinimumHeight) {
                topHeight -= bottomMinimumHeight - bottomHeight;
                bottomHeight = bottomMinimumHeight;
            }

            m_splitter->setSizes({ topHeight, bottomHeight });
        });
    }
}

void ScorePage::setUsable(bool state)
{
    m_layout->setCurrentIndex(state);
}

void ScorePage::setNetworkBlocked(bool state)
{
    m_networkBlocked->setVisible(state);
    m_roundBox->setEnabled(! state);
    m_splitter->setEnabled(! state);
}

void ScorePage::playersNameEdited()
{
    const QVector<Database::BoogerResult> score = currentScore();
    const int lastOpenedRound = m_roundSelect->currentNumber();
    const int selectedTable = m_tableSelect->currentNumber();

    // Check if we currently search for a name
    int editedIndex = -1;
    QString editedText;

    for (int i = 0; i < 4; i += m_tournamentSettings->isSinglePlayerMode() ? 1 : 2) {
        if (m_playersSelect.at(i)->itemName(m_playersSelect.at(i)->currentIndex())
            != m_playersSelect.at(i)->currentName()) {

            editedIndex = i;
            editedText = m_playersSelect.at(i)->currentName();
            break;
        }
    }

    reload(m_roundSelect->currentNumber());

    fillInScore(lastOpenedRound, selectedTable, score);
    if (editedIndex != -1) {
        m_playersSelect.at(editedIndex)->lineEdit()->setText(editedText);
        m_playersSelect.at(editedIndex)->lineEditTextChanged(editedText);
    }
}

void ScorePage::reload(int lastRound)
{
    if (! isVisible()) {
        return;
    }

    // Reset all booger widgets if the GUI has been already setup
    if (lastRound != -1) {
        for (BoogerWidget *boogerWidget : std::as_const(m_boogerWidgets)) {
            boogerWidget->reset();
        }
        m_boogerWidgets.at(0)->setFocus();
    }

    // Fill the round select

    blockSelectSignals(true);

    int allRounds = 0;
    int firstOpenRound = -1;

    m_roundSelect->clear();
    const auto roundsState = m_db->roundsState();
    const auto roundsStateKeys = roundsState.keys();
    for (const int roundNumber : roundsStateKeys) {
        if (roundsState.value(roundNumber)) {
            m_roundSelect->addItem(tr("%1 (abgeschlossen)").arg(roundNumber),
                                   roundNumber);
        } else {
            m_roundSelect->addItem(tr("%1 (offen)").arg(roundNumber),
                                   roundNumber);
            if (firstOpenRound == -1) {
                firstOpenRound = roundNumber;
            }
        }
        allRounds++;
    }

    if (firstOpenRound == -1) {
        firstOpenRound = 1;
    }
    if (lastRound != -1) {
        firstOpenRound = lastRound;
    }

    // Add one for a new round
    if (m_db->isChangeable()) {
        allRounds++;
        m_roundSelect->addItem(tr("%1 (neue Runde)").arg(allRounds), allRounds);
        // The "false" value for the new round's missing roundsState entry will be provided
        // by setting the default value to "false" in openRound().
    }

    // Display the data
    m_roundSelect->setCurrentNumber(firstOpenRound);
    blockSelectSignals(false);
    openRound(firstOpenRound);
}

void ScorePage::openRound(int round)
{
    if (round == -1) {
        return;
    }

    m_currentRound = round;
    Q_EMIT roundSelectionChanged();

    blockSelectSignals(true);

    m_searchWidget->saveSearch();

    // We set the "false" default value here because if a new round is selected,
    // the Database's roundsState map won't contain a value for it yet

    if (! m_db->roundsState().value(round, false)) {
        m_resultBox->setVisible(true);

        // Insert all missing tables

        m_tableSelect->clear();

        const auto [ missingTables, success1 ] = m_db->missingTables(round);
        if (! success1) {
            return;
        }

        for (const int table : missingTables) {
            m_tableSelect->addItem(QString::number(table), table);
        }
        m_tableSelect->setCurrentIndex(0);

        // Insert all missing players/pairs

        const auto [ missingPlayers, success2 ] = m_db->missingPlayers(round);
        if (! success2) {
            return;
        }

        // Create a sorted list of all missing registration's names and build a name <--> index map
        QVector<QString> names;
        QHash<QString, int> namesIndex;
        for (int i = 0; i < missingPlayers.count(); i++) {
            const auto &data = missingPlayers.at(i);
            names.append(data.name);
            namesIndex[data.name] = i;
        }
        m_stringTools->sort(names);

        // Insert all names and their respective id into the player select combo box's models
        for (int i = 0; i < 4; i += m_tournamentSettings->isSinglePlayerMode() ? 1 : 2) {
            auto *playersSelect = m_playersSelect[i];
            playersSelect->clear();
            for (const QString &name : std::as_const(names)) {
                const auto &data = missingPlayers.at(namesIndex.value(name));
                playersSelect->addEntry(data,
                    m_stringTools->pairOrPlayerName(data, StringTools::FormattedName));
            }
        }

    } else {
        m_resultBox->setVisible(false);
    }

    updateScore();
    updateAssignment(round);

    blockSelectSignals(false);
    checkSavePossible();
    m_searchWidget->restoreSearch();
}

void ScorePage::insertAssignment(int id, const Draw::Seat &seat)
{
    // Insert the drawn table for the id
    m_assignedTable.insert(id, seat.table);

    // If we don't have had the table yet: Insert an empty set of IDs
    if (! m_assignedIds.contains(seat.table)) {
        m_assignedIds.insert(seat.table, { 0, 0, 0, 0 });
    }

    // Insert the ID at the correct index for the table-ids mapping
    m_assignedIds[seat.table][Draw::indexForPlayer(seat.pair, seat.player)] = id;
}

void ScorePage::updateAssignment(int round)
{
    if (! m_tournamentSettings->autoSelectPairs()) {
        // If we don't use automatic selection, we can skip the assignment generation
        return;
    }

    if (round == -1) {
        // This happens when this function is triggered by an assignment change on the players
        // page. In this case, we have to set the round variable to the opened round
        round = m_roundSelect->currentNumber();
    }

    // Clear the last assignment
    m_assignedTable.clear();
    m_assignedIds.clear();

    if ((m_tournamentSettings->selectByLastRound() && round == 1)
        || ! m_tournamentSettings->selectByLastRound()) {

        // If we select by the last round but we're working on the first round or we want to
        // select by a draw ignoring the results of the last round, we want to select a draw:
        const auto [ assignment, success ] = m_db->draw(round);
        if (! success) {
            return;
        }

        QHash<int, Draw::Seat>::const_iterator it;
        for (it = assignment.constBegin(); it != assignment.constEnd(); it++) {
            // We can insert the drawn seats as-is
            insertAssignment(it.key(), it.value());
        }

    } else {
        // Otherwise, we want to select by the last round and work on round 2+.
        // We thus want to select the assignment of the previous round:
        const auto [ assignment, success ] = m_db->assignment(round - 1);
        if (! success) {
            return;
        }

        // Get the number of tables from the database
        const int tablesCount = m_db->tablesCount();

        QHash<int, Draw::Seat>::const_iterator it;
        for (it = assignment.constBegin(); it != assignment.constEnd(); it++) {
            const auto &seat = it.value();

            // Determine the correct table. This is the same table like in the last round if the
            // pair was a pair 1 or the player was part of a pair 1. For pairs 2 or players of a
            // pair 2, we increment the table by 1, possibly starting again at table 1:
            const int table = seat.pair == 1 ? seat.table : seat.table % tablesCount + 1;

            // Insert the processed seat
            insertAssignment(it.key(), Draw::Seat { table, seat.pair, seat.player });
        }
    }

    // Reset the selection if there's one
    if (m_tableSelect->currentNumber() != -1) {
        blockSelectSignals(true);
        m_tableSelect->setCurrentIndex(0);
        for (int i = 0; i < 4; i++) {
            m_playersSelect.at(i)->setCurrentIndex(0);
        }
        blockSelectSignals(false);
        checkSavePossible();
    }
}

void ScorePage::updateScore()
{
    const int round = m_roundSelect->currentNumber();

    // Get and display all scores for the current round
    m_scoreModel->refresh(round);

    const int resultsPerRound = m_db->pairsOrPlayersCount() / m_db->necessaryDivisor();
    const int currentResults = m_scoreModel->resultsCount();

    // Update the box header
    if (currentResults == 0) {
        m_roundSummaryBox->setTitle(
            tr("Bisher keine Ergebnisse aus der %1. Runde").arg(round));
    } else if (currentResults < resultsPerRound) {
        m_roundSummaryBox->setTitle(
            tr("Bisher %n Ergebnis(se) aus der %1. Runde", "", currentResults).arg(round)
            + QStringLiteral(" ")
            + tr("(%n fehlt/fehlen noch)", "", resultsPerRound - currentResults));
    } else {
        m_roundSummaryBox->setTitle(
            tr("Ergebnisse der %1. Runde").arg(round));
    }

    // Hide/Show the search widget and update the cache for single player tournaments
    if (currentResults == 0) {
        m_searchWidget->clear();
        m_isSearchLabel->hide();
        m_searchWidget->hide();
    } else {
        m_searchWidget->show();
        if (m_tournamentSettings->isSinglePlayerMode()) {
            m_searchEngine->clearSearchCache(SearchEngine::DrawnPairsScoreSearch);
            for (const QString &pairName : m_scoreModel->assembledPairs()) {
                m_searchEngine->updateSearchCache(pairName, SearchEngine::DrawnPairsScoreSearch);
            }
        }
    }

    // If we're on a network: Process checksums

    if (m_isServer || m_isClient) {
        const QString newChecksum = scoreChecksum(m_scoreModel->score());
        const bool checksumChanged = newChecksum != m_checksums[round];
        m_checksums[round] = newChecksum;

        if (checksumChanged) {
            updateLastChange(m_lastLocalChange);

            // We have to announce the checksum change via a single shot call, because otherwise,
            // if this happens via a remote name change, the round checksum is updated and it's
            // change is broadcasted before the name change is broadcasted and thus the checkums
            // won't match on the client side.
            QTimer::singleShot(0, this, [this, round]
            {
                if (m_isServer) {
                    m_server->broadcastScoreChecksum(round);
                } else if (m_isClient) {
                    m_client->requestScoreChecksumChange(round);
                }
            });
        }

        updateSyncStatus();
    }
}

void ScorePage::tableChanged(int table)
{
    if (! m_tournamentSettings->autoSelectPairs() || m_noAutoselect->isChecked()) {
        checkSavePossible();
        return;
    }

    blockSelectSignals(true);

    bool allSelectable = true;
    const bool isPairMode = m_tournamentSettings->isPairMode();
    QVector<int> missingPlayers;

    if (m_assignedIds.contains(table)) {
        const auto &ids = m_assignedIds[table];
        for (int i = 0; i < 4; i++) {
            const int id = ids.at(i);
            if (id != 0) {
                // We need to unfocus the playersSelect, because if the change was triggered by
                // entering a name and we're in single player mode, and the name of the second
                // player is entered in the first select or vice versa, we end up with that
                // player's name being displayed two times. Which won't happen if the select has
                // no focus.
                m_playersSelect.at(i)->clearFocus();
                if (! m_playersSelect.at(i)->setCurrentNumber(id)) {
                    allSelectable = false;
                }
            } else {
                if ((isPairMode && Draw::playerForIndex(i) == 1) || ! isPairMode) {
                    missingPlayers.append(i);
                }
            }
        }
    }

    checkSavePossible();

    const int missingPlayersCount = missingPlayers.count();
    if (missingPlayersCount > 0) {
        if (isPairMode) {
            const int pair = Draw::pairForIndex(missingPlayers.at(0));
            QMessageBox::warning(this,
                tr("Automatische Auswahl"),
                tr("Für Tisch %1 wurde Paar %2 bisher nicht zugelost.\n"
                   "Es wurde nur Paar %3 ausgewählt!").arg(
                   QString::number(table), QString::number(pair),
                   QString::number(Draw::otherPair(pair))));
        } else {
            QString list;
            for (int i = 0; i < missingPlayersCount; i++) {
                if (i > 0 && i < missingPlayersCount - 1) {
                    list.append(tr(", "));
                } else if (missingPlayersCount > 1 && i == missingPlayersCount - 1) {
                    list.append(tr(" und "));
                }
                const int index = missingPlayers.at(i);
                list.append(tr("Paar %1 Spieler %2").arg(
                               QString::number(Draw::pairForIndex(index)),
                               QString::number(Draw::playerForIndex(index))));
            }
            QMessageBox::warning(this,
                tr("Automatische Auswahl"),
                tr("<p>Tisch %1 wurde noch nicht komplett ausgelost. Folgende(r) Spieler wurde(n) "
                   "nicht ausgewählt:</p><p>%2</p>", "", missingPlayersCount).arg(
                   QString::number(table), list));
        }
    }

    if (! allSelectable) {
        QMessageBox::warning(this,
            tr("Automatische Auswahl"),
            tr("Es konnten nicht alle %1 entsprechend der Auslosung ausgewählt werden, da entweder "
               "für die entsprechenden %1 bereits manuell ein Ergebnis eingegeben wurde, oder die "
               "Auslosung geändert wurde und noch nicht alle Ergebnisse eingegeben waren.").arg(
               m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler")));
    }

    blockSelectSignals(false);
}

void ScorePage::playersChanged(int index, int pairId)
{
    if (! m_tournamentSettings->autoSelectPairs() || m_noAutoselect->isChecked()
        || ! m_assignedTable.contains(pairId)) {

        return;
    }

    const auto &ids = m_assignedIds[m_assignedTable.value(pairId)];

    const auto selectedPairNumber = (index > 1) + 1;
    const auto assignedPairNumber = (ids.indexOf(pairId) > 1) + 1;
    if (selectedPairNumber != assignedPairNumber) {
        QString text = tr("<p><b>Die Auswahl passt nicht zur Auslosung</b><p>");
        if (m_tournamentSettings->isPairMode()) {
            text += tr("<p>Als Paar %1 wurde gerade „%2“ ausgewählt. Laut der Auslosung bzw. dem "
                       "Ergebnis der letzten Runde ist dieses Paar aber ein Paar %3!</p>").arg(
                       QString::number(selectedPairNumber),
                       m_playersSelect.at(index)->currentText().toHtmlEscaped(),
                       QString::number(assignedPairNumber));
        } else {
            text += tr("<p>Als Spieler von Paar %1 wurde gerade „%2“ ausgewählt. Laut der "
                       "Auslosung bzw. dem Ergebnis der letzten Runde gehört dieser Spieler aber "
                       "zu Paar %3!</p>").arg(
                       QString::number(selectedPairNumber),
                       m_playersSelect.at(index)->currentText().toHtmlEscaped(),
                       QString::number(assignedPairNumber));
        }
        QMessageBox::warning(this, tr("Die Auswahl passt nicht zur Auslosung"), text);
        return;
    }

    const int assignedTable = m_assignedTable.value(pairId);
    if (m_tableSelect->currentNumber() == assignedTable) {
        tableChanged(assignedTable);
    } else {
        if (! m_tableSelect->setCurrentNumber(assignedTable)) {
            QMessageBox::warning(this,
                tr("Automatische Auswahl"),
                tr("Der zum ausgewählten %1 zugeloste Tisch Nr. %2 kann nicht ausgewählt werden, "
                   "weil entweder bereits manuell ein Ergebnis für diesen Tisch eingegeben wurde, "
                   "oder die Auslosung geändert wurde und noch nicht alle Ergebnisse eingegeben "
                   "waren.").arg(
                   m_tournamentSettings->isPairMode() ? tr("Paar") : tr("Spieler"),
                   QString::number(assignedTable)));
        }
    }

    // Re-select the entered pair's or player's playersSelect
    for (int i = 0; i < 4; i += m_tournamentSettings->isSinglePlayerMode() ? 1 : 2) {
        if (m_playersSelect.at(i)->currentNumber() == pairId) {
            m_playersSelect.at(i)->setFocus();
            break;
        }
    }
}

void ScorePage::checkSavePossible()
{
    switch (m_tournamentSettings->tournamentMode()) {
    case Tournament::FixedPairs:
        m_saveScoreButton->setEnabled(
            m_tableSelect->inputOkay()
            && m_tableSelect->currentNumber() != -1
            && m_playersSelect.at(0)->currentIndex() != m_playersSelect.at(2)->currentIndex());
        break;
    case Tournament::SinglePlayers:
        m_saveScoreButton->setEnabled(
            m_tableSelect->inputOkay()
            && m_tableSelect->currentNumber() != -1
            && m_playersSelect.at(0)->currentIndex() != m_playersSelect.at(1)->currentIndex()
            && m_playersSelect.at(0)->currentIndex() != m_playersSelect.at(2)->currentIndex()
            && m_playersSelect.at(0)->currentIndex() != m_playersSelect.at(3)->currentIndex()
            && m_playersSelect.at(1)->currentIndex() != m_playersSelect.at(2)->currentIndex()
            && m_playersSelect.at(1)->currentIndex() != m_playersSelect.at(3)->currentIndex()
            && m_playersSelect.at(2)->currentIndex() != m_playersSelect.at(3)->currentIndex()
        );
        break;
    }
}

QVector<Database::BoogerResult> ScorePage::currentScore() const
{
    QVector<Database::BoogerResult> score;
    score.reserve(m_scoreSize);
    for (int i = 0; i < 4; i += m_tournamentSettings->isSinglePlayerMode() ? 1 : 2) {
        for (const BoogerWidget *boogerWidget : std::as_const(m_boogerWidgets)) {
            score.append(Database::BoogerResult { m_playersSelect.at(i)->currentNumber(),
                                                  boogerWidget->score(i < 2 ? 1 : 2) });
        }
    }
    return score;
}

void ScorePage::saveScore()
{
    Q_EMIT statusUpdate(tr("Speichere Spielergebnis …"));
    QApplication::setOverrideCursor(Qt::WaitCursor);

    const bool showRankingPage = ! m_db->localTournamentStarted();

    const int lastOpenedRound = m_roundSelect->currentNumber();
    const int selectedTable = m_tableSelect->currentNumber();
    if (! m_db->saveScore(lastOpenedRound, selectedTable, currentScore())) {
        return;
    }

    if (showRankingPage) {
        Q_EMIT tournamentStarted();
    }

    Q_EMIT scoreChanged();
    reload(lastOpenedRound);

    m_scoreView->scrollToTable(selectedTable);

    QApplication::restoreOverrideCursor();
    Q_EMIT statusUpdate(tr("Spielergebnis für Tisch %1 in Runde %2 gespeichert").arg(
                           QString::number(selectedTable), QString::number(lastOpenedRound)));

    if (m_autoSelectTableForDraw->isChecked()) {
        Q_EMIT requestDrawNextRound(playersForTable(selectedTable), m_roundSelect->currentNumber(),
                                    false);
    }
}

void ScorePage::showContextMenu()
{
    if (! m_db->isChangeable()) {
        return;
    }

    m_selectedTable = m_scoreView->selectedTable();

    m_contextMenu->setTitle(tr("Ergebnis für Tisch %1").arg(m_selectedTable));

    if (m_tournamentSettings->isSinglePlayerMode()) {
        m_swapPair1PlayersAction->setText(m_scoreView->selectedPair(1));
        m_swapPair2PlayersAction->setText(m_scoreView->selectedPair(2));
    }

    m_scoreView->setKeepHoveredRow(true);
    m_contextMenu->exec(QCursor::pos());
    m_scoreView->setKeepHoveredRow(false);
}

void ScorePage::deleteScore()
{
    QMessageBox messageBox(this);
    messageBox.setIcon(QMessageBox::Question);
    messageBox.setWindowTitle(tr("Spielergebnis löschen"));
    messageBox.setText(tr("Soll das Spielergebnis von Tisch %1 wirklich gelöscht werden?").arg(
                          m_selectedTable));
    messageBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    messageBox.setDefaultButton(QMessageBox::No);

    if (messageBox.exec() == QMessageBox::No) {
        return;
    }

    doDeleteScore(m_roundSelect->currentNumber(), m_selectedTable);
}

void ScorePage::doDeleteScore(int round, int table)
{
    QApplication::setOverrideCursor(Qt::WaitCursor);
    Q_EMIT statusUpdate(tr("Lösche Spielergebnis …"));

    if (! m_db->deleteScore(round, table)) {
        return;
    }

    Q_EMIT statusUpdate(tr("Spielergebnis für Tisch %1 in Runde %2 gelöscht").arg(
                           QString::number(m_selectedTable), QString::number(round)));
    reload(round);
    QApplication::restoreOverrideCursor();

    if (! m_db->localTournamentStarted()) {
        // Re-emitting this will cause the players page to be updated again,
        // as it checks for db.tournamentStarted() itself
        Q_EMIT tournamentStarted();
    }
    Q_EMIT scoreChanged();
}

void ScorePage::editScore()
{
    const int round = m_roundSelect->currentNumber();

    const auto [ score, success ] = m_db->singleScore(round, m_selectedTable);
    if (! success) {
        return;
    }

    doDeleteScore(round, m_selectedTable);
    fillInScore(round, m_selectedTable, score);
}

void ScorePage::fillInScore(int round, int table, const QVector<Database::BoogerResult> &score)
{
    m_roundSelect->setCurrentNumber(round);

    blockSelectSignals(true);
    m_tableSelect->setCurrentNumber(table);
    switch (m_tournamentSettings->tournamentMode()) {
    case Tournament::FixedPairs:
        m_playersSelect.at(0)->setCurrentNumber(
            score.at(0).pairOrPlayer);
        m_playersSelect.at(2)->setCurrentNumber(
            score.at(m_tournamentSettings->boogersPerRound()).pairOrPlayer);
        break;
    case Tournament::SinglePlayers:
        for (int i = 0; i < 4; i++) {
            m_playersSelect.at(i)->setCurrentNumber(
                score.at(m_tournamentSettings->boogersPerRound() * i).pairOrPlayer);
        }
        break;
    }
    blockSelectSignals(false);

    // Get the scores to set
    QVector<int> scores;
    for (int i = 0; i < score.count(); i++) {
        scores.append(score.at(i).score);
        for (int j = 1; j < m_tournamentSettings->boogersPerRound(); j++) {
            scores.append(score.at(++i).score);
        }
        if (m_tournamentSettings->isSinglePlayerMode()) {
            // Leave out the double scores
            i += m_tournamentSettings->boogersPerRound();
        }
    }

    // Check if we have an aborted score
    int abortedBooger = -1;
    for (int i = 0; i < m_boogerWidgets.count(); i++) {
        if (scores.at(i) < m_tournamentSettings->boogerScore()
            && scores.at(i + scores.count() / 2) < m_tournamentSettings->boogerScore()) {

            abortedBooger = i;
            break;
        }
    }

    // First reset all boogers
    for (BoogerWidget *booger : std::as_const(m_boogerWidgets)) {
        booger->reset();
    }

    // Then mark the first aborted booger if we have one
    if (abortedBooger > -1) {
        boogerAbortedStateChanged(abortedBooger, true);
        m_boogerWidgets.at(abortedBooger)->setAborted(true);
        m_boogerWidgets.at(abortedBooger)->setAsFirstAborted();
    }

    // Pre-set all scores
    for (int i = 0; i < m_boogerWidgets.count(); i++) {
        m_boogerWidgets.at(i)->setScore(scores.at(i), scores.at(i + scores.count() / 2));
    }

    checkSavePossible();
}

void ScorePage::blockSelectSignals(bool state)
{
    m_roundSelect->blockSignals(state);
    m_tableSelect->blockSignals(state);
    for (int i = 0; i < 4; i += m_tournamentSettings->isSinglePlayerMode() ? 1 : 2) {
        m_playersSelect.at(i)->blockSignals(state);
    }
}

void ScorePage::checkEnterScore()
{
    if (! m_saveScoreButton->isEnabled()) {
        return;
    }
    saveScore();
}

void ScorePage::updateSearch(const QString &searchText)
{
    if (searchText.isEmpty()) {
        for (int row = 0; row < m_scoreModel->rowCount(); row++) {
            m_scoreView->showRow(row);
        }
        m_scoreView->resizeColumnsToContents();
        m_isSearchLabel->hide();
        return;
    }

    int matches = 0;
    const auto isPairMode = m_tournamentSettings->isPairMode();

    bool isNumber = false;
    const auto number = searchText.toInt(&isNumber);

    if (isNumber && number > 0) {
        // We do a pure number search. Only check the players numbers
        for (int row = 0; row < m_scoreModel->rowCount(); row++) {
            m_scoreView->hideRow(row);
        }
        for (int row = 0; row < m_scoreModel->rowCount(); row += m_scoreModel->rowsPerTable()) {
            if (m_scoreModel->rowContainsPlayersNumber(row, number)) {
                for (int i = row; i < row + m_scoreModel->rowsPerTable(); i++) {
                    m_scoreView->showRow(i);
                }
                matches = 1;
                break;
            }
        }

    } else {
        // We query the search engine

        m_searchEngine->setSearchTerm(searchText);
        const SearchEngine::SearchType searchType =
            isPairMode ? SearchEngine::DefaultSearch
                       : SearchEngine::DrawnPairsScoreSearch;
        const bool phoneticSearch = m_searchWidget->phoneticSearch();

        for (int row = 0; row < m_scoreModel->rowCount(); row += m_scoreModel->rowsPerTable()) {
            const bool rowMatches =
                m_searchEngine->checkMatch(m_scoreModel->pairForRow(row, 1),
                                           searchType,
                                           phoneticSearch)
                || m_searchEngine->checkMatch(m_scoreModel->pairForRow(row, 2),
                                              searchType,
                                              phoneticSearch);

            for (int i = row; i < row + m_scoreModel->rowsPerTable(); i++) {
                m_scoreView->setRowHidden(i, ! rowMatches);
            }

            matches += rowMatches;
        }
    }

    m_scoreView->resizeColumnsToContents();

    m_searchWidget->updateMatches(matches);
    m_isSearchLabel->setText(tr("(Suchergebnis für „%1“)").arg(searchText.simplified()));
    m_isSearchLabel->show();
}

void ScorePage::swapPairs()
{
    const int round = m_roundSelect->currentNumber();

    if (! m_db->swapPairs(round, m_selectedTable)) {
        return;
    }

    Q_EMIT statusUpdate(tr("Paar 1 und 2 für Tisch %1 in Runde %2 vertauscht").arg(
                           QString::number(m_selectedTable), QString::number(round)));

    // This will reload the page and restore the currently entered score
    playersNameEdited();
}

void ScorePage::swapPlayers(int pair)
{
    const int round = m_roundSelect->currentNumber();

    if (! m_db->swapPlayers(round, m_selectedTable, pair)) {
        return;
    }

    Q_EMIT statusUpdate(tr("Spieler 1 und 2 für Tisch %1 Paar %2 in Runde %3 vertauscht").arg(
                           QString::number(m_selectedTable), QString::number(pair),
                           QString::number(round)));

    // This will reload the page and restore the currently entered score
    playersNameEdited();
}

void ScorePage::drawModeChanged()
{
    QApplication::setOverrideCursor(Qt::WaitCursor);
    updateAssignment(m_roundSelect->currentNumber());
    QApplication::restoreOverrideCursor();

    m_noAutoselect->setVisible(m_tournamentSettings->autoSelectPairs());

    QMessageBox::information(this, tr("Einstellungen für die automatische Auswahl geändert"),
        tr("<p><b>Folgende Einstellungen gelten jetzt für die automatische Auswahl:</b></p>")
        + DrawModeHelper::description(m_tournamentSettings->autoSelectPairs(),
                                      m_tournamentSettings->selectByLastRound()));

    toggleAutoSelectTableForDraw();
}

void ScorePage::toggleAutoSelectTableForDraw()
{
    m_autoSelectTableForDraw->setEnabled(true);
    m_drawNextRoundAction->setVisible(true);
}

QString ScorePage::formatOpponentsString(const QString &pair1, const QString &pair2) const
{
    if (pair1.size() <= 15 && pair2.size() <= 15) {
        return tr("„%1“ gegen „%2“").arg(pair1, pair2);
    } else if (pair1.size() > 15 && pair2.size() <= 15) {
        return tr("„%1…“ gegen „%2“").arg(pair1.left(15), pair2);
    } else if (pair1.size() <= 15 && pair2.size() > 15) {
        return tr("„%1“ gegen „%2…“").arg(pair1, pair2.left(15));
    } else {
        return tr("„%1…“ gegen „%2…“").arg(pair1.left(15), pair2.left(15));
    }
}

void ScorePage::setAutoSelectTableForDraw(bool state)
{
    m_autoSelectTableForDraw->setChecked(state);
}

void ScorePage::boogerAbortedStateChanged(int booger, bool aborted)
{
    if (aborted) {
        for (int i = booger + 1; i < m_boogerWidgets.count(); i++) {
            m_boogerWidgets.at(i)->setAborted(true);
        }
    } else {
        for (int i = 0; i <= booger; i++) {
            if (m_boogerWidgets.at(i)->isAborted()) {
                m_boogerWidgets.at(i)->reset();
            }
        }
        if (booger + 1 < m_boogerWidgets.count()) {
            m_boogerWidgets.at(booger + 1)->setAsFirstAborted();
        }
    }
}

void ScorePage::resetSearch()
{
    m_searchWidget->clear();
}

void ScorePage::showScorePopup(BoogerSpinBox *spinBox)
{
    m_targetSpinBox = spinBox;
    m_scorePopup->show();
}

void ScorePage::setSelectedScore()
{
    m_targetSpinBox->setValue(m_scorePopup->score());
    m_targetSpinBox->setFocus();
    m_targetSpinBox->selectAll();
}

void ScorePage::scoreTypeChanged(Tournament::ScoreType scoreType)
{
    QApplication::setOverrideCursor(Qt::WaitCursor);
    m_searchWidget->saveSearch();
    m_tournamentSettings->setScoreType(scoreType);
    setupGui();
    m_searchWidget->restoreSearch();
    QApplication::restoreOverrideCursor();
}

void ScorePage::addPairResultToChecksum(ChecksumHelper &checksumHelper, const QString &pair,
                                        const QVector<int> &scores) const
{
    checksumHelper.addData(pair);
    for (const auto score : scores) {
        checksumHelper.addData(score);
    }
}

QString ScorePage::scoreChecksum(const Scores::Score &score) const
{
    if (score.tables.count() == 0) {
        return QString();
    }

    const auto isPairMode = m_tournamentSettings->isPairMode();
    ChecksumHelper checksumHelper;


    for (int i = 0; i < score.tables.count(); i++) {
        // Add the table number
        checksumHelper.addData(score.tables.at(i));

        // Cache the pair names.
        // For fixed pairs tournaments, we can use the pair name as-is,
        // For single player tournaments, we create a "normalized" (sorted) pair name,
        // so that "Joe / Ben" and "Ben / Joe" is handled as the same pair
        const auto pair1 = isPairMode
            ? score.pairsOrPlayers1.at(i * 2).name
            : m_stringTools->normalizedPairName(score.pairsOrPlayers1.at(i * 2),
                                                score.players2.at(i * 2));
        const auto pair2 = isPairMode
            ? score.pairsOrPlayers1.at(i * 2 + 1).name
            : m_stringTools->normalizedPairName(score.pairsOrPlayers1.at(i * 2 + 1),
                                                score.players2.at(i * 2 + 1));

        // The order in which we add this table's pair names and results depends on the pair name.
        // For the ranking, it doesn't matter if the pair was a pair 1 or a pair 2, so we use a
        // "normalized" order, just like with the players of a pair
        if (pair1 <= pair2) {
            addPairResultToChecksum(checksumHelper, pair1, score.scores.at(i * 2));
            addPairResultToChecksum(checksumHelper, pair2, score.scores.at(i * 2 + 1));
        } else {
            addPairResultToChecksum(checksumHelper, pair2, score.scores.at(i * 2 + 1));
            addPairResultToChecksum(checksumHelper, pair1, score.scores.at(i * 2));
        }
    }

    return checksumHelper.checksum();
}

const QMap<int, QString> &ScorePage::checksums() const
{
    return m_checksums;
}

void ScorePage::updateLastChange(QString &cache)
{
    if (! m_syncDisplay->isVisible()) {
        return;
    }

    cache = QTime::currentTime().toString(QStringLiteral("hh:mm:ss"));

    if (m_isClient) {
        m_syncDisplay->setLastChange(tr("<table>"
                                        "<tr><td colspan=\"2\"><b>Letzte Änderung:</b></td></tr>"
                                        "<tr><td>Server:</td><td>%1</td></tr>"
                                        "<tr><td>Lokal:</td><td>%2</td></tr>"
                                        "</table>").arg(m_lastServerChange, m_lastLocalChange));
    } else if (m_isServer) {
        m_syncDisplay->setLastChange(tr("<b>Letzte Änderung:</b> %1").arg(m_lastLocalChange));
    }
}

void ScorePage::updateSyncStatus()
{
    const int round = m_roundSelect->currentNumber();

    if (m_isClient) {
        const bool synced = m_checksums.value(round) == m_serverChecksums.value(round);
        m_syncDisplay->setSyncStatus(synced
            ? tr("<span style=\"%1\">Ergebnisse sind<br/>identisch</span>").arg(
                SharedStyles::greenText)
            : tr("<p><span style=\"%1\">Ergebnisse sind<br/>nicht identisch</span></p>"
                 "<p><a href=\"#showCompareScoreDialog\">Unterschiede<br/>anzeigen</a></p>").arg(
                 SharedStyles::redText));

    } else if (m_isServer) {
        QString state;
        const auto clients = m_clientChecksums.keys();
        for (const int clientId : clients) {
            const bool synced = m_checksums.value(round)
                                == m_clientChecksums.value(clientId).value(round);
            state.append(tr("<p>"
                            "<b>Client #%1</b><br/>"
                            "Letzte Änderung: %2<br/>").arg(
                            QString::number(clientId), m_lastClientChange.value(clientId)));
            state.append(synced
                ? tr("<span style=\"%1\">Ergebnisse sind identisch</span></p>").arg(
                     SharedStyles::greenText)
                : tr("<span style=\"%1\">Ergebnisse sind<br/>nicht identisch</span><br/>"
                     "<a href=\"#showCompareScoreDialog&%2\">"
                     "Unterschiede anzeigen</a></p>").arg(
                     SharedStyles::redText, QString::number(clientId)));
        }
        m_syncDisplay->setSyncStatus(state);
    }
}

void ScorePage::processLinkActivated(const QString &anchor) const
{
    if (anchor.startsWith(QStringLiteral("#showCompareScoreDialog"))) {
        Q_EMIT requestCompareScoreDialog(
            m_isClient ? 0 : anchor.split(QStringLiteral("&")).at(1).toInt(),
            &m_currentRound, &m_scoreModel->score());
    } else if (anchor == QStringLiteral("#finishRegistration")) {
        Q_EMIT finishRegistration();
    }
}

void ScorePage::serverStarted(Server *server)
{
    m_isServer = true;
    m_server = server;
    initializeChecksums();
    m_syncDisplay->show();
    updateLastChange(m_lastLocalChange);
    m_syncDisplay->setSyncStatus(QString());
}

void ScorePage::serverStopped()
{
    m_isServer = false;
    m_checksums.clear();
    m_syncDisplay->hide();
}

void ScorePage::clientConnected(Client *client)
{
    m_isClient = true;
    m_client = client;
    m_syncDisplay->show();

    updateLastChange(m_lastLocalChange);
    updateLastChange(m_lastServerChange);
    updateSyncStatus();
}

void ScorePage::clientDisconnected()
{
    m_isClient = false;
    m_serverChecksums.clear();
    m_syncDisplay->hide();
}

void ScorePage::initializeChecksums()
{
    QApplication::setOverrideCursor(Qt::BusyCursor);

    for (int round = 1; round <= m_db->roundsState().count(); round++) {
        const auto [ score, success ] = m_db->scores(round);
        if (! success) {
            QApplication::restoreOverrideCursor();
            return;
        }

        m_checksums[round] = scoreChecksum(score);
    }

    QApplication::restoreOverrideCursor();
}

void ScorePage::setServerChecksum(int round, const QString &checksum)
{
    m_serverChecksums[round] = checksum;
    updateLastChange(m_lastServerChange);
    if (m_roundSelect->currentNumber() == round) {
        updateSyncStatus();
    }
}

void ScorePage::setClientChecksum(int clientId, int round, const QString &checksum)
{
    m_clientChecksums[clientId][round] = checksum;
    updateLastChange(m_lastClientChange[clientId]);
    if (m_roundSelect->currentNumber() == round) {
        updateSyncStatus();
    }
}

void ScorePage::removeClientChecksums(int clientId)
{
    m_clientChecksums.remove(clientId);
    updateSyncStatus();
}

void ScorePage::setProcessingRequest(bool processing)
{
    m_syncDisplay->setEnabled(! processing);

    if (processing) {
        QApplication::setOverrideCursor(Qt::BusyCursor);
    } else {
        QApplication::restoreOverrideCursor();
    }
}

QVector<QString> ScorePage::playersForTable(int table) const
{
    const auto &score = m_scoreModel->score();
    const int index = score.tables.indexOf(table) * 2;

    QVector<QString> players;
    players.append(score.pairsOrPlayers1.at(index).name);
    players.append(score.pairsOrPlayers1.at(index + 1).name);
    if (m_tournamentSettings->isSinglePlayerMode()) {
        players.append(score.players2.at(index).name);
        players.append(score.players2.at(index + 1).name);
    }

    return players;
}
