// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef PLAYERSLINEEDIT_H
#define PLAYERSLINEEDIT_H

// Local includes
#include "StyledLineEdit.h"

// Qt classes
class QKeyEvent;
class QFocusEvent;

class PlayersLineEdit : public StyledLineEdit
{
    Q_OBJECT

public:
    explicit PlayersLineEdit(QWidget *parent = nullptr);

Q_SIGNALS:
    void escapePressed();

protected:
    void keyPressEvent(QKeyEvent *event) override;
    void focusInEvent(QFocusEvent *event) override;

};

#endif // PLAYERSLINEEDIT_H
