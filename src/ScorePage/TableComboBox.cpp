// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "TableComboBox.h"
#include "TableLineEdit.h"

// Qt includes
#include <QDebug>
#include <QIntValidator>

TableComboBox::TableComboBox(QWidget *parent) : NumbersComboBox(parent)
{
    setEditable(true);
    setInsertPolicy(QComboBox::NoInsert);
    connect(this, QOverload<int>::of(&QComboBox::activated), this, &TableComboBox::tableSelected);

    m_lineEdit = new TableLineEdit;
    m_validator = new QIntValidator(this);
    m_lineEdit->setValidator(m_validator);
    setLineEdit(m_lineEdit);
    disconnect(m_lineEdit, &QLineEdit::editingFinished, 0, 0);
    connect(m_lineEdit, &QLineEdit::textEdited, this, &TableComboBox::lineEditTextChanged);
}

void TableComboBox::clear()
{
    QComboBox::clear();

    m_inputOkay = true;
    m_lineEdit->displayInvalidInput(false);

    m_validator->setBottom(0);
    m_validator->setTop(0);

    // Add a "no table selected" dummy entry ...
    addItem(m_lineEdit->noItemText(), -1);

    // ... and make it unselectable, both for the popup (hide it) ...
    qobject_cast<QListView *>(view())->setRowHidden(0, true);

    // ... and the mouse wheel/arrow keys selection (disable it).
    QStandardItemModel *itemModel = qobject_cast<QStandardItemModel *>(model());
    QStandardItem *item = itemModel->item(0);
    item->setFlags(item->flags() & ~Qt::ItemIsEnabled);
}

void TableComboBox::addItem(const QString &text, const QVariant &userData)
{
    QComboBox::addItem(text, userData);
    const int number = userData.toInt();
    const int bottom = m_validator->bottom();

    if (bottom == 0) {
        m_validator->setBottom(number);
        m_validator->setTop(number);
    } else {
        if (bottom > number) {
            m_validator->setBottom(number);
        }
        if (m_validator->top() < number) {
            m_validator->setTop(number);
        }
    }
}

bool TableComboBox::setCurrentNumber(int number)
{
    if (! NumbersComboBox::setCurrentNumber(number)) {
        return false;
    }

    if (! m_inputOkay) {
        m_inputOkay = true;
        m_lineEdit->displayInvalidInput(false);
    }

    return true;
}

void TableComboBox::lineEditTextChanged(const QString &text)
{
    const int index = findData(text.toInt());

    if (index == -1) {
        m_lineEdit->displayInvalidInput(true);
        // This will disable the save button:
        m_inputOkay = false;
        Q_EMIT checkSavePossible();
        return;
    }

    if (! m_inputOkay) {
        m_inputOkay = true;
        m_lineEdit->displayInvalidInput(false);
    }

    if (currentIndex() != index) {
        setCurrentIndex(index);
    } else {
        // The index didn't change, thus, numberChanged() won't be emitted. But the save button is
        // still disabled, so we have to trigger ScorePage::checkSavePossible() manually here to
        // re-enable it
        Q_EMIT checkSavePossible();
    }
}

bool TableComboBox::inputOkay() const
{
    return m_inputOkay;
}

void TableComboBox::tableSelected()
{
    m_lineEdit->displayInvalidInput(false);

    if (! m_inputOkay) {
        // If m_inputOkay is false, currentNumberChanged() already triggered
        // ScorePage::checkSavePossible(), but with m_inputOkay still set to false.
        // Thus, we have to emit checkSavePossible() to trigger it again, but with m_inputOkay
        // set to true.
        m_inputOkay = true;
        Q_EMIT checkSavePossible();
    }
}
