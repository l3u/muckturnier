// SPDX-FileCopyrightText: 2019-2020 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef STYLEDLINEEDIT_H
#define STYLEDLINEEDIT_H

// Qt includes
#include <QLineEdit>

class StyledLineEdit : public QLineEdit
{
    Q_OBJECT

public:
    explicit StyledLineEdit(QWidget *parent = nullptr);
    void displayInvalidInput(bool state);

};

#endif // STYLEDLINEEDIT_H
