// SPDX-FileCopyrightText: 2010-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef BOOGERRADIOBUTTON_H
#define BOOGERRADIOBUTTON_H

// Qt includes
#include <QRadioButton>

// Local classes
class BoogerSpinBox;

class BoogerRadioButton : public QRadioButton
{
    Q_OBJECT

public:
    explicit BoogerRadioButton(QWidget *parent = nullptr, BoogerSpinBox *spinBox = nullptr);

Q_SIGNALS:
    void scorePopupRequested(BoogerSpinBox *spinBox);

protected:
    void wheelEvent(QWheelEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;

private: // Variables
    BoogerSpinBox *m_spinBox;

};

#endif // BOOGERRADIOBUTTON_H
