// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef VISIBILITYWATCHER_H
#define VISIBILITYWATCHER_H

// Qt includes
#include <QObject>

class VisibilityWatcher : public QObject
{
    Q_OBJECT

public:
    VisibilityWatcher(QObject *parent);

Q_SIGNALS:
    void visibilityChanged(bool isVisible);

protected:
    bool eventFilter(QObject *object, QEvent *event) override;

};

#endif // VISIBILITYWATCHER_H
