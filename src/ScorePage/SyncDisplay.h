// SPDX-FileCopyrightText: 2022-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SYNCWIDGET_H
#define SYNCWIDGET_H

// Local includes
#include "shared/MinimumViewportScroll.h"

// Local classes
class MinimumViewportWidget;

// Qt classes
class QLabel;

class SyncDisplay : public MinimumViewportScroll
{
    Q_OBJECT

public:
    explicit SyncDisplay(QWidget *parent = nullptr);
    void setLastChange(const QString &text);
    void setSyncStatus(const QString &text);

Q_SIGNALS:
    void linkActivated(const QString &anchor);

private: // Variables
    MinimumViewportWidget *m_displayWidget;
    QLabel *m_lastChange;
    QLabel *m_syncStatus;

};

#endif // SYNCWIDGET_H
