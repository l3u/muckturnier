// SPDX-FileCopyrightText: 2020-2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "PlayersWidget.h"
#include "PlayersComboBox.h"

#include "shared/SharedStyles.h"

// Qt includes
#include <QHBoxLayout>
#include <QLabel>
#include <QGridLayout>
#include <QVBoxLayout>

PlayersWidget::PlayersWidget(Tournament::ScoreType scoreType,
                             QLabel *tableLabel,
                             QWidget *table,
                             const QVector<PlayersComboBox *> &players,
                             QWidget *phoneticSearch,
                             QWidget *parent)
    : QWidget(parent)
{
    switch (scoreType) {

    case Tournament::HorizontalScore: {
        QHBoxLayout *layout = new QHBoxLayout(this);
        layout->setContentsMargins(0, 0, 0, 0);

        layout->addWidget(tableLabel);
        layout->addWidget(table);

        QGridLayout *playersLayout = new QGridLayout;
        layout->addLayout(playersLayout);

        int playersIndex = 0;
        for (int pair = 1; pair < 3; pair++) {
            QLabel *pairLabel = new QLabel(tr("Paar %1:").arg(pair));
            pairLabel->setStyleSheet(SharedStyles::boldText);
            playersLayout->addWidget(pairLabel, pair - 1, 0);

            for (int pairPart = 1; pairPart <= 2; pairPart++) {
                playersLayout->addWidget(players.at(playersIndex++), pair - 1, pairPart);
            }
        }

        layout->addWidget(phoneticSearch);

        layout->addStretch();

        } break;

    case Tournament::VerticalScore: {
        QGridLayout *layout = new QGridLayout(this);
        layout->setContentsMargins(0, 0, 0, 0);

        layout->addWidget(tableLabel, 0, 0);
        QLabel *pair1Label = new QLabel(tr("Paar 1:"));
        pair1Label->setStyleSheet(SharedStyles::boldText);
        layout->addWidget(pair1Label, 1, 0);

        QHBoxLayout *tableLayout = new QHBoxLayout;
        layout->addLayout(tableLayout, 0, 1);

        tableLayout->addWidget(table);

        tableLayout->addStretch();

        QHBoxLayout *playersLayout = new QHBoxLayout;
        layout->addLayout(playersLayout, 1, 1);

        int playersIndex = 0;

        playersLayout->addWidget(players.at(playersIndex++));
        playersLayout->addWidget(players.at(playersIndex++));

        QFrame *line = new QFrame;
        line->setFrameShape(QFrame::VLine);
        line->setProperty("isseparator", true);
        playersLayout->addWidget(line);

        QLabel *pair2Label = new QLabel(tr("Paar 2:"));
        pair2Label->setStyleSheet(SharedStyles::boldText);
        playersLayout->addWidget(pair2Label);

        playersLayout->addWidget(players.at(playersIndex++));
        playersLayout->addWidget(players.at(playersIndex++));

        QFrame *line2 = new QFrame;
        line2->setFrameShape(QFrame::VLine);
        line2->setProperty("isseparator", true);
        playersLayout->addWidget(line2);

        playersLayout->addWidget(phoneticSearch);
        playersLayout->addStretch();

        } break;

    }
}
