// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SPLASHBUTTON_H
#define SPLASHBUTTON_H

// Qt includes
#include <QToolButton>

// Local classes
class ResourceFinder;

class SplashButton : public QToolButton
{
    Q_OBJECT

public:
    explicit SplashButton(const QString &text, const QString &icon, ResourceFinder *resourceFinder,
                          QWidget *parent = nullptr);

private Q_SLOTS:
    void initializeIcon();

private: // Variables
    ResourceFinder *m_resourceFinder;
    const QString m_icon;

};

#endif // SPLASHBUTTON_H
