// SPDX-FileCopyrightText: 2010-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "Application.h"

#include "MainWindow.h"
#include "AboutDialog.h"
#include "ImportPlayersDialog.h"
#include "RestoreBackupDialog.h"
#include "AdjustDrawDialog.h"
#include "SplashButton.h"

#include "BookingPage/BookingPage.h"
#include "BookingPage/BookingEngine.h"
#include "BookingPage/CodeScannerTestDialog.h"

#include "WlanCodeScannerPage/WlanCodeScannerPage.h"

#include "DrawOverviewPage/DrawOverviewPage.h"

#include "SchedulePage/StopWatchEngine.h"
#include "SchedulePage/SchedulePage.h"

#include "export/ExportTournamentDialog.h"
#include "export/ExportRankingDialog.h"
#include "export/ExportPlayersDialog.h"

#include "NewTournamentDialog/NewTournamentDialog.h"

#include "RegistrationPage/RegistrationPage.h"
#include "RegistrationPage/MarkersWidget.h"

#include "ScorePage/ScorePage.h"

#include "RankingPage/RankingPage.h"

#include "SettingsDialog/SettingsDialog.h"

#include "network/NetworkStatusWidget.h"
#include "network/CompareScoreDialog.h"

#include "ClientPage/Client.h"
#include "ClientPage/ClientPage.h"
#include "ServerPage/Server.h"
#include "ServerPage/ServerPage.h"

#include "NoteGeneratorDialog/NoteGeneratorDialog.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/StringTools.h"
#include "SharedObjects/ResourceFinder.h"
#include "SharedObjects/Settings.h"
#include "SharedObjects/TournamentSettings.h"
#include "SharedObjects/DismissableMessage.h"

#include "shared/SharedStyles.h"
#include "shared/BackupEngine.h"
#include "shared/Tournament.h"
#include "shared/Urls.h"

// Qt includes
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QTabWidget>
#include <QMenuBar>
#include <QAction>
#include <QApplication>
#include <QStatusBar>
#include <QMessageBox>
#include <QUrl>
#include <QDesktopServices>
#include <QFileInfo>
#include <QDebug>
#include <QFileDialog>
#include <QDir>
#include <QDockWidget>
#include <QCloseEvent>
#include <QToolButton>
#include <QCheckBox>
#include <QRegularExpression>
#include <QScreen>
#include <QShortcut>
#include <QTimer>
#include <QActionGroup>
#include <QStorageInfo>

// C++ includes
#include <functional>

// Conditional includes
#ifdef DEBUG_MODE
#include "ChecksumsDialog.h"
#endif

static const QHash<QString, QTabWidget::TabPosition> s_tabPositionMap {
    { QStringLiteral("north"), QTabWidget::TabPosition::North },
    { QStringLiteral("south"), QTabWidget::TabPosition::South },
    { QStringLiteral("west"),  QTabWidget::TabPosition::West  },
    { QStringLiteral("east"),  QTabWidget::TabPosition::East  }
};

MainWindow::MainWindow(SharedObjects *sharedObjects)
    : QMainWindow(),
      m_sharedObjects(sharedObjects),
      m_resourceFinder(sharedObjects->resourceFinder()),
      m_db(sharedObjects->database()),
      m_settings(sharedObjects->settings()),
      m_tournamentSettings(sharedObjects->tournamentSettings()),
      m_stringTools(sharedObjects->stringTools()),
      m_dismissableMessage(sharedObjects->dismissableMessage())
{
#if defined(Q_OS_LINUX) || defined(Q_OS_FREEBSD)
    QIcon appIcon;
    for (int i : { 16, 22, 32, 48, 64, 128 }) {
        appIcon.addFile(
            m_resourceFinder->find(QStringLiteral("icons/hicolor/%1x%1/apps/muckturnier.png").arg(
                                                  QString::number(i))),
            QSize(i, i));
    }
    setWindowIcon(appIcon);
#endif
    setWindowTitle(tr("Muckturnier"));

    connect(m_db, &Database::sqlError, this, &MainWindow::sqlError);

    // Splash Screen
    // =============

    m_splashScreen = new QWidget(this);
    setCentralWidget(m_splashScreen);
    QVBoxLayout *layout = new QVBoxLayout(m_splashScreen);

    // Banner

    QLabel *banner = new QLabel;
    banner->setPixmap(QPixmap(m_resourceFinder->find(QStringLiteral("banner.png"))));
    banner->setAlignment(Qt::AlignCenter);
    banner->setStyleSheet(SharedStyles::headerLogo);
    banner->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Maximum);
    layout->addWidget(banner);
    layout->addStretch();

    // Startup buttons

    auto *buttonLayout = new QHBoxLayout;
    layout->addLayout(buttonLayout);

    auto *printNotesButton = new SplashButton(
        tr("Zettel drucken"), QStringLiteral("notes.png"), m_resourceFinder);
    buttonLayout->addWidget(printNotesButton);

    auto *newTournamentButton = new SplashButton(
        tr("Ein neues Turnier starten"), QStringLiteral("new.png"), m_resourceFinder);
    buttonLayout->addWidget(newTournamentButton);

    auto *openTournamentButton = new SplashButton(
        tr("Ein Turnier öffnen"), QStringLiteral("open.png"), m_resourceFinder);
    buttonLayout->addWidget(openTournamentButton);

    // Footer

    QLabel *footer = new QLabel(tr("Das Programm für die Turnierleitung"));
    footer->setAlignment(Qt::AlignCenter);
    footer->setStyleSheet(SharedStyles::footerLogo);
    layout->addStretch();
    layout->addWidget(footer);

    // Pages and DockWidgets
    // =====================

    // Registration Page
    // -----------------

    m_registrationPage = new RegistrationPage(this, m_sharedObjects);
    connect(m_registrationPage, &RegistrationPage::statusUpdate, this, &MainWindow::updateStatus);
    connect(m_registrationPage, &RegistrationPage::registrationsAvailable,
            this, &MainWindow::registrationsAvailable);
    connect(m_registrationPage, &RegistrationPage::playersCountChanged,
            this, &MainWindow::playersCountChanged);
    connect(this, &MainWindow::resetAllSearches,
            m_registrationPage, &RegistrationPage::resetSearch);

    // ScorePage
    // ---------

    m_scorePage = new ScorePage(this, m_sharedObjects);
    connect(m_scorePage, &ScorePage::statusUpdate, this, &MainWindow::updateStatus);
    connect(m_scorePage, &ScorePage::tournamentStarted,  // tournamentStarted is also emitted
            this, &MainWindow::tournamentStarted);       // if the last score has been deleted
    connect(m_scorePage, &ScorePage::scoreChanged,
            m_registrationPage, &RegistrationPage::scoreChanged);
    connect(m_registrationPage, &RegistrationPage::networkTournamentStarted,
            this, &MainWindow::networkTournamentStarted);
    connect(this, &MainWindow::resetAllSearches, m_scorePage, &ScorePage::resetSearch);
    connect(m_registrationPage, &RegistrationPage::drawChanged,
            m_scorePage, std::bind(&ScorePage::updateAssignment, m_scorePage, -1));
    connect(m_registrationPage, &RegistrationPage::nameEdited,
            m_scorePage, &ScorePage::playersNameEdited);
    connect(m_scorePage, &ScorePage::requestCompareScoreDialog,
            this, &MainWindow::showCompareScoreDialog);
    connect(m_registrationPage, &RegistrationPage::disqualificationChanged,
            m_scorePage, &ScorePage::playersNameEdited);
    connect(m_registrationPage, &RegistrationPage::playersNumberChanged,
            m_scorePage, &ScorePage::playersNameEdited);
    connect(m_scorePage, &ScorePage::requestDrawNextRound,
            m_registrationPage, &RegistrationPage::prepareDrawNextRound);
    connect(m_registrationPage, &RegistrationPage::drawChanged, m_db, &Database::updateFixtures);

    // RankingPage
    // -----------

    m_rankingPage = new RankingPage(this, m_sharedObjects);
    connect(m_rankingPage, &RankingPage::statusUpdate, this, &MainWindow::updateStatus);
    connect(m_scorePage, &ScorePage::scoreChanged, m_rankingPage, &RankingPage::dataChanged);
    connect(this, &MainWindow::resetAllSearches, m_rankingPage, &RankingPage::resetSearch);
    connect(m_registrationPage, &RegistrationPage::nameEdited,
            m_rankingPage, &RankingPage::dataChanged);
    connect(m_registrationPage, &RegistrationPage::disqualificationChanged,
            m_rankingPage, &RankingPage::dataChanged);
    connect(m_rankingPage, &RankingPage::requestPrintRanking,
            this, &MainWindow::showExportRankingDialog);
    connect(m_scorePage, &ScorePage::scoreChanged, m_db, &Database::updateFixtures);

    // DockWidgets
    // -----------

    setDockNestingEnabled(true);

    m_playersDock = new QDockWidget(tr("Anmeldung"), this);
    m_playersDock->setWidget(m_registrationPage);
    formatDockWidget(m_playersDock, QStringLiteral("playersDock"));
    addDockWidget(Qt::TopDockWidgetArea, m_playersDock);
    connect(m_registrationPage, &RegistrationPage::raiseMe, m_playersDock, &QDockWidget::raise);
    m_playersDock->hide();

    // HACK: Adding \u200A ("Hair Space") at the end of "Ergebnisse" is already enough to avoid
    // https://bugreports.qt.io/browse/QTBUG-126859. This can be removed once this bug is fixed
    // (if this will happen at all).
    m_scoreDock = new QDockWidget(tr("Ergebnisse\u200A"), this);
    m_scoreDock->setWidget(m_scorePage);
    formatDockWidget(m_scoreDock, QStringLiteral("scoreDock"));
    addDockWidget(Qt::TopDockWidgetArea, m_scoreDock);
    tabifyDockWidget(m_playersDock, m_scoreDock);
    m_scoreDock->hide();

    m_rankingDock = new QDockWidget(tr("Rangliste"), this);
    m_rankingDock->setWidget(m_rankingPage);
    formatDockWidget(m_rankingDock, QStringLiteral("rankingDock"));
    addDockWidget(Qt::TopDockWidgetArea, m_rankingDock);
    tabifyDockWidget(m_scoreDock, m_rankingDock);
    m_rankingDock->hide();

    // Main Menu
    // =========

    // "File"
    // ------

    QMenu *fileMenu = menuBar()->addMenu(tr("Datei"));

    m_newTournamentAction = fileMenu->addAction(tr("Neues Turnier starten"));
    m_newTournamentAction->setShortcut(QKeySequence(tr("Ctrl+N")));
    connect(m_newTournamentAction, &QAction::triggered, this, &MainWindow::startNewTournament);
    connect(newTournamentButton, &QToolButton::clicked, m_newTournamentAction, &QAction::triggered);

    m_openTournamentAction = fileMenu->addAction(tr("Turnier öffnen"));
    m_openTournamentAction->setShortcut(QKeySequence(tr("Ctrl+O")));
    connect(m_openTournamentAction, &QAction::triggered,
            this, std::bind(&MainWindow::openTournament, this, QString()));
    connect(openTournamentButton, &QToolButton::clicked,
            m_openTournamentAction, &QAction::triggered);

    fileMenu->addSeparator();

    auto *showNoteGeneratorDialogAction = fileMenu->addAction(tr("Zettel drucken"));
    showNoteGeneratorDialogAction->setShortcut(QKeySequence(tr("Ctrl+Z")));
    connect(showNoteGeneratorDialogAction, &QAction::triggered, this, [this]
            {
                auto dialog = NoteGeneratorDialog(this, m_sharedObjects);
                dialog.exec();
            });
    connect(printNotesButton, &QToolButton::clicked,
            showNoteGeneratorDialogAction, &QAction::triggered);

    fileMenu->addSeparator();

    // Import and export menu

    m_imExportMenu = fileMenu->addMenu(tr("Import und Export"));
    m_imExportMenu->setEnabled(false);

    m_importPlayersListAction = m_imExportMenu->addAction(tr("Paarliste importieren"));
    connect(m_importPlayersListAction, &QAction::triggered, this, &MainWindow::importPlayersList);
    m_importPlayersListAction->setEnabled(false);

    m_exportPlayersListAction = m_imExportMenu->addAction(tr("Paarliste exportieren"));
    connect(m_exportPlayersListAction, &QAction::triggered, this, &MainWindow::exportPlayersList);
    m_exportPlayersListAction->setEnabled(false);

    m_imExportMenu->addSeparator();

    m_exportRankingAction = m_imExportMenu->addAction(tr("Rangliste exportieren"));
    connect(m_exportRankingAction, &QAction::triggered, this, &MainWindow::showExportRankingDialog);
    m_exportRankingAction->setEnabled(false);

    m_imExportMenu->addSeparator();

    m_exportTournamentAction = m_imExportMenu->addAction(tr("Turnierdaten exportieren"));
    connect(m_exportTournamentAction, &QAction::triggered,
            this, &MainWindow::showExportTournamentDialog);
    m_exportTournamentAction->setEnabled(false);

    // Tournament state menu

    m_tournamentStateMenu = fileMenu->addMenu(tr("Turnierstatus festlegen"));
    m_tournamentStateMenu->setEnabled(false);

    QActionGroup *tournamentStateGroup = new QActionGroup(this);

    m_setTournamentOpenAction = new QAction(tr("Offen"), tournamentStateGroup);
    m_setTournamentOpenAction->setCheckable(true);
    m_setTournamentOpenAction->setEnabled(false);
    connect(m_setTournamentOpenAction, &QAction::triggered,
            this, std::bind(&MainWindow::askSetTournamentOpen, this, true));
    m_tournamentStateMenu->addAction(m_setTournamentOpenAction);

    m_setTournamentFinishedAction = new QAction(tr("Abgeschlossen"), tournamentStateGroup);
    m_setTournamentFinishedAction->setCheckable(true);
    m_setTournamentFinishedAction->setEnabled(false);
    connect(m_setTournamentFinishedAction, &QAction::triggered,
            this, std::bind(&MainWindow::askSetTournamentOpen, this, false));
    m_tournamentStateMenu->addAction(m_setTournamentFinishedAction);

    connect(m_rankingPage, &RankingPage::enableExport, this, &MainWindow::setExportEnabled);

    fileMenu->addSeparator();

    m_closeTournamentAction = fileMenu->addAction(tr("Turnier schließen"));
    m_closeTournamentAction->setEnabled(false);
    connect(m_closeTournamentAction, &QAction::triggered, this, &MainWindow::closeTournament);

    fileMenu->addSeparator();

    QAction *quitAction = fileMenu->addAction(tr("Beenden"));
#ifdef Q_OS_MACOS
    quitAction->setMenuRole(QAction::QuitRole);
#endif
    quitAction->setShortcut(QKeySequence(tr("Ctrl+Q")));
    connect(quitAction, &QAction::triggered, this, &QWidget::close);

    // "Additional functions"
    // ----------------------

    auto *additionalFunctionsMenu = menuBar()->addMenu(tr("Zusatzfunktionen"));

    m_showBookingPage = additionalFunctionsMenu->addAction(tr("Voranmeldung"));
    connect(m_showBookingPage, &QAction::triggered, this, &MainWindow::showBookingPage);
    m_showBookingPage->setEnabled(false);

    additionalFunctionsMenu->addSeparator();

    m_showDrawOverviewPageAction = additionalFunctionsMenu->addAction(tr("Übersicht Auslosung"));
#ifdef Q_OS_MACOS
    // This is really weird. As macOS is.
    // Heaven knows why Qt thinks this was the "About" action. However, if we don't do this, this
    // action doesn't show up in "Additional functions". The real "About" action defined below is
    // showing up in the "Muckturnier" menu as long as no database is open. But as soon as a
    // database is opened, the "Draw Overview" action shows up instead (also labeled "About ...").
    // So here we go ...
    m_showDrawOverviewPageAction->setMenuRole(QAction::NoRole);
#endif
    connect(m_showDrawOverviewPageAction, &QAction::triggered,
            this, std::bind(&MainWindow::showDrawOverviewPage, this, 1));
    connect(m_registrationPage, &RegistrationPage::requestShowDrawOverviewPage,
            this, &MainWindow::showDrawOverviewPage);
    m_showDrawOverviewPageAction->setEnabled(false);

    m_showAdjustDrawDialogAction = additionalFunctionsMenu->addAction(tr("Auslosung bereinigen"));
    m_showAdjustDrawDialogAction->setEnabled(false);
    connect(m_registrationPage, &RegistrationPage::drawAdjustmentNeeded,
            m_showAdjustDrawDialogAction, &QAction::setEnabled);
    connect(m_showAdjustDrawDialogAction, &QAction::triggered,
            this, &MainWindow::showAdjustDrawDialog);
    connect(m_registrationPage, &RegistrationPage::requestShowAdjustDrawDialog,
            m_showAdjustDrawDialogAction,
            std::bind(&QAction::triggered, m_showAdjustDrawDialogAction, false));

    additionalFunctionsMenu->addSeparator();

    m_showSchedulePage = additionalFunctionsMenu->addAction(tr("Turnier-Zeitplan"));
    m_showSchedulePage->setEnabled(false);
    connect(m_showSchedulePage, &QAction::triggered, this, &MainWindow::showSchedulePage);

    // "Network"
    // ---------

    QMenu *networkMenu = menuBar()->addMenu(tr("Netzwerk"));

    m_showServerPageAction = networkMenu->addAction(tr("Als Server einrichten"));
    connect(m_showServerPageAction, &QAction::triggered, this, &MainWindow::showServerPage);
    m_showServerPageAction->setEnabled(false);

    m_showClientPageAction = networkMenu->addAction(tr("Mit Server verbinden"));
    connect(m_showClientPageAction, &QAction::triggered, this, &MainWindow::showClientPage);
    m_showClientPageAction->setEnabled(false);

    networkMenu->addSeparator();

    m_showWlanCodeScannerPage = networkMenu->addAction(tr("WLAN-Code-Scanner nutzen"));
    connect(m_showWlanCodeScannerPage, &QAction::triggered,
            this, &MainWindow::showWlanCodeScannerPage);
    m_showWlanCodeScannerPage->setEnabled(false);

    networkMenu->addSeparator();

    m_finishRegistrationAction = networkMenu->addAction(tr("Anmeldung beenden"));
    connect(m_finishRegistrationAction, &QAction::triggered,
            m_registrationPage, &RegistrationPage::finishRegistration);
    connect(m_scorePage, &ScorePage::finishRegistration, m_finishRegistrationAction,
            std::bind(&QAction::triggered, m_finishRegistrationAction, false));
    m_finishRegistrationAction->setEnabled(false);

    // "Extras"
    // --------

    QMenu *extrasMenu = menuBar()->addMenu(tr("Extras"));

    QAction *showSettingsDialogAction = extrasMenu->addAction(tr("Einstellungen"));
#ifdef Q_OS_MACOS
    showSettingsDialogAction->setMenuRole(QAction::PreferencesRole);
#endif
    connect(showSettingsDialogAction, &QAction::triggered, this, &MainWindow::showSettingsDialog);

    extrasMenu->addSeparator();

    m_databaseExtrasMenu = extrasMenu->addMenu(tr("Datenbank"));
    m_databaseExtrasMenu->menuAction()->setEnabled(false);

    m_createDatabaseBackupAction = m_databaseExtrasMenu->addAction(tr("Backup anlegen"));
    m_createDatabaseBackupAction->setShortcut(QKeySequence(tr("Ctrl+B")));
    connect(m_createDatabaseBackupAction, &QAction::triggered,
            this, &MainWindow::createDatabaseBackup);
    m_createDatabaseBackupAction->setEnabled(false);

    m_showRestoreBackupDialogAction = m_databaseExtrasMenu->addAction(
                                          tr("Backup wiederherstellen"));
    m_showRestoreBackupDialogAction->setShortcut(QKeySequence(tr("Ctrl+W")));
    connect(m_showRestoreBackupDialogAction, &QAction::triggered,
            this, &MainWindow::triggerShowRestoreBackupDialog);
    m_showRestoreBackupDialogAction->setEnabled(false);

    m_databaseExtrasMenu->addSeparator();

    m_resetDatabaseAction = m_databaseExtrasMenu->addAction(tr("Zurücksetzen"));
    connect(m_resetDatabaseAction, &QAction::triggered, this, &MainWindow::resetDatabase);
    m_resetDatabaseAction->setEnabled(false);

    extrasMenu->addSeparator();

    m_stopWatchEngine = new StopWatchEngine(this, m_sharedObjects);
    QAction *showStopWatchAction = extrasMenu->addAction(tr("Stoppuhr"));
    showStopWatchAction->setShortcut(QKeySequence(tr("Ctrl+S")));
    connect(showStopWatchAction, &QAction::triggered, m_stopWatchEngine,
            &StopWatchEngine::newStopWatch);

    auto *testCodeScannerAction = extrasMenu->addAction(tr("Code-Scanner testen"));
    connect(testCodeScannerAction, &QAction::triggered, this, [this]
    {
        auto *dialog = new CodeScannerTestDialog(this);
        dialog->exec();
    });

    extrasMenu->addSeparator();

#ifdef DEBUG_MODE
    m_showChecksumsDialogAction = extrasMenu->addAction(tr("Prüfsummen"));
    m_showChecksumsDialogAction->setShortcut(QKeySequence(tr("Ctrl+P")));
    connect(m_showChecksumsDialogAction, &QAction::triggered,
            this, &MainWindow::showChecksumsDialog);
    m_showChecksumsDialogAction->setEnabled(false);
#endif

    auto showAllDismissedMessagesAction = extrasMenu->addAction(tr("Alle Infos wieder anzeigen"));
    connect(showAllDismissedMessagesAction, &QAction::triggered,
            this, &MainWindow::showAllDismissedMessages);

    // "View"
    // ------

    QMenu *viewMenu = menuBar()->addMenu(tr("Ansicht"));

    m_hideWindowFrameAction = viewMenu->addAction(tr("Fenstertitel und -rahmen ausblenden"));
    m_hideWindowFrameAction->setCheckable(true);
    m_hideWindowFrameAction->setShortcut(QKeySequence(tr("F11")));
    connect(m_hideWindowFrameAction, &QAction::triggered, this, &MainWindow::hideWindowFrame);

    m_hideMenubarAction = viewMenu->addAction(tr("Menüleiste ausblenden"));
    m_hideMenubarAction->setCheckable(true);
    m_hideMenubarAction->setShortcut(QKeySequence(tr("Ctrl+M")));
    connect(m_hideMenubarAction, &QAction::triggered, this, &MainWindow::hideMenuBar);

    // All menu shortcuts are disabled as soon as the menu is hidden,
    // so we also need a global shortcut to re-show it again
    m_viewMenubarShortcut = new QShortcut(QKeySequence(tr("Ctrl+M")), this);
    m_viewMenubarShortcut->setContext(Qt::ApplicationShortcut);
    m_viewMenubarShortcut->setEnabled(false);
    connect(m_viewMenubarShortcut, &QShortcut::activated, m_hideMenubarAction, &QAction::toggle);
    connect(m_viewMenubarShortcut, &QShortcut::activated, this, &MainWindow::hideMenuBar);

    viewMenu->addSeparator();

    const QTabWidget::TabPosition tabPosition = s_tabPositionMap.value(m_settings->tabPosition());
    setDockTabPosition(tabPosition);

    m_tabPositionMenu = viewMenu->addMenu(tr("Position der Tabs für Seiten"));
    QActionGroup *tabPositionGroup = new QActionGroup(this);

    QAction *tabPositionSouth = new QAction(tr("Unten"), tabPositionGroup);
    m_tabPositionMenu->addAction(tabPositionSouth);
    tabPositionSouth->setCheckable(true);
    if (tabPosition == QTabWidget::TabPosition::South) {
        tabPositionSouth->setChecked(true);
    }
    connect(tabPositionSouth, &QAction::toggled,
            this, std::bind(&MainWindow::setDockTabPosition, this,
                            QTabWidget::TabPosition::South, std::placeholders::_1));

    QAction *tabPositionNorth = new QAction(tr("Oben"), tabPositionGroup);
    m_tabPositionMenu->addAction(tabPositionNorth);
    tabPositionNorth->setCheckable(true);
    if (tabPosition == QTabWidget::TabPosition::North) {
        tabPositionNorth->setChecked(true);
    }
    connect(tabPositionNorth, &QAction::toggled,
            this, std::bind(&MainWindow::setDockTabPosition, this,
                            QTabWidget::TabPosition::North, std::placeholders::_1));

    QAction *tabPositionWest = new QAction(tr("Links"), tabPositionGroup);
    m_tabPositionMenu->addAction(tabPositionWest);
    tabPositionWest->setCheckable(true);
    if (tabPosition == QTabWidget::TabPosition::West) {
        tabPositionWest->setChecked(true);
    }
    connect(tabPositionWest, &QAction::toggled,
            this, std::bind(&MainWindow::setDockTabPosition, this,
                            QTabWidget::TabPosition::West, std::placeholders::_1));

    QAction *tabPositionEast = new QAction(tr("Rechts"), tabPositionGroup);
    m_tabPositionMenu->addAction(tabPositionEast);
    tabPositionEast->setCheckable(true);
    if (tabPosition == QTabWidget::TabPosition::East) {
        tabPositionEast->setChecked(true);
    }
    connect(tabPositionEast, &QAction::toggled,
            this, std::bind(&MainWindow::setDockTabPosition, this,
                            QTabWidget::TabPosition::East, std::placeholders::_1));

    m_tabPositionMenu->setEnabled(false);

    m_pagesMenu = viewMenu->addMenu(tr("Seiten arrangieren"));

    QAction *arrangeTabifiedAction = m_pagesMenu->addAction(tr("Als Tabs"));
    arrangeTabifiedAction->setShortcut(QKeySequence(tr("Ctrl+T")));
    connect(arrangeTabifiedAction, &QAction::triggered, this, &MainWindow::arrangeTabified);

    QAction *arrangeHorizontallyAction = m_pagesMenu->addAction(tr("Nebeneinander"));
    connect(arrangeHorizontallyAction, &QAction::triggered, this, &MainWindow::arrangeHorizontally);

    QAction *arrangeVerticallyAction = m_pagesMenu->addAction(tr("Übereinander"));
    connect(arrangeVerticallyAction, &QAction::triggered, this, &MainWindow::arrangeVertically);

    QAction *arrangeOwnWindowAction = m_pagesMenu->addAction(tr("Als einzelne Fenster"));
    connect(arrangeOwnWindowAction, &QAction::triggered, this, &MainWindow::arrangeOwnWindow);

    m_pagesMenu->setEnabled(false);

    viewMenu->addSeparator();

    m_resetMainWindowStateAction = viewMenu->addAction(tr("Seitenanordnung zurücksetzen"));
    m_resetMainWindowStateAction->setEnabled(false);
    connect(m_resetMainWindowStateAction, &QAction::triggered,
            this, &MainWindow::resetMainWindowState);

    // "Help"
    // ------

    QMenu *helpMenu = menuBar()->addMenu(tr("Hilfe"));

    QAction *handbookAction = helpMenu->addAction(tr("Handbuch zu Muckturnier"));
    handbookAction->setShortcut(QKeySequence(tr("F1")));
    connect(handbookAction, &QAction::triggered, this, &MainWindow::showHandbook);

    helpMenu->addSeparator();

    QAction *aboutAction = helpMenu->addAction(tr("Über Muckturnier"));
#ifdef Q_OS_MACOS
    aboutAction->setMenuRole(QAction::AboutRole);
#endif
    connect(aboutAction, &QAction::triggered, this, &MainWindow::showAboutDialog);

    // Restore or set the MainWindow geometry
    // ======================================

    const bool geometryRestored = m_settings->saveOnCloseEnabled(Settings::SaveWindowGeometry)
                                      ? restoreGeometry(m_settings->mainWindowGeometry())
                                      : false;

    if (! geometryRestored) {
        const QRect availableGeometry = QGuiApplication::primaryScreen()->availableGeometry();
        if (availableGeometry.width() > 1000 && availableGeometry.height() > 750) {
            resize(QSize(1000, 750));
        } else {
            setWindowState(Qt::WindowMaximized);
        }
    }

    // Done :-)
    // ========

    updateStatus(tr("Bitte ein neues Turnier starten oder ein bestehendes öffnen!"));
}

void MainWindow::playersCountChanged(int count)
{
    const bool tournamentCanBeStarted = m_db->playersDividable(count)
                                        && ! m_registrationPage->singlesPresent()
                                        && ! m_registrationPage->bookedPresent();
    m_scorePage->setUsable(tournamentCanBeStarted);
    m_scorePage->reload();
    m_finishRegistrationAction->setEnabled((m_isServer || m_isClient)
                                           && ! m_db->networkTournamentStarted()
                                           && tournamentCanBeStarted);
}

void MainWindow::startNewTournament()
{
    // Save the current tournament settings if applicable
    if (! m_db->saveSettings()) {
        return;
    }

    NewTournamentDialog *dialog = new NewTournamentDialog(this, m_sharedObjects);
    connect(dialog, &NewTournamentDialog::updateStatus, this, &MainWindow::updateStatus);
    connect(dialog, &NewTournamentDialog::reopenLastOpenedDb,
            this, &MainWindow::reopenLastOpenedDb);

    if (! dialog->exec()) {
        return;
    }

    m_tournamentSettings->setLoadingDb(true);

    processDbReset();
    databaseChanged();

    QTimer::singleShot(0, this, [this]
    {
        m_tournamentSettings->setLoadingDb(false);
    });
}

void MainWindow::openTournament(QString dbFileName)
{
    // Save the current tournament settings if applicable
    if (! m_db->saveSettings()) {
        return;
    }

    const QString lastOpenedDb = m_db->dbFile();

    if (dbFileName.isEmpty()) {
        QString startPath = m_db->dbPath();
        if (startPath.isEmpty()) {
            startPath = m_settings->lastDbPath();
        }

        dbFileName = QFileDialog::getOpenFileName(this,
            tr("Turnier öffnen"),
            startPath,
            tr("Muckturnier-Datenbanken") + QLatin1String(" (*.mtdb);;")
            + tr("Alle Dateien") + QLatin1String(" (*)"));

        if (dbFileName.isEmpty()) {
            return;
        }
    }

    if (dbFileName == lastOpenedDb) {
        QMessageBox::information(this,
            tr("Turnier öffnen"),
            tr("Die Turnierdatenbank „%1“ ist bereits geöffnet!").arg(
               QDir::toNativeSeparators(dbFileName)));
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    updateStatus(tr("Öffne Turnierdatenbank …"));

    m_tournamentSettings->setLoadingDb(true);

    // Close the current database connection without touching the GUI. If no database is currently
    // opened, nothing happens. Otherwise, the GUI stays the same for now (in case the requested
    // database can't be opened and we re-open the last one).
    m_db->closeDatabaseConnection();
    Database::Error openResult = m_db->openDatabase(dbFileName);
    QApplication::restoreOverrideCursor();

    switch (openResult) {

    case Database::Error::NoError:
        if (! m_db->isWritable()) {
            QMessageBox::warning(this,
                tr("Turnier öffnen"),
                tr("<p><b>Die Turnierdatenbank ist nicht beschreibbar</b></p>"
                   "<p>Die Datei „%1“ ist nicht beschreibbar. Die Turnierdatenbank kann nur "
                   "angezeigt, aber nicht verändert werden.</p>").arg(
                   QDir::toNativeSeparators(dbFileName).toHtmlEscaped()),
                QMessageBox::Ok);
        }
        break;

    case Database::Error::DbVersion3:
    case Database::Error::DbVersion4:
    case Database::Error::DbVersion5:
    case Database::Error::DbVersion6:
    case Database::Error::DbVersion7:
    case Database::Error::DbVersion8:
    case Database::Error::DbVersion9:
    case Database::Error::DbVersion10:
    case Database::Error::DbVersion11:
    case Database::Error::DbVersion12:
    case Database::Error::DbVersion13:
        // We have to do a database revision update before we can use the database
        m_tournamentSettings->setLoadingDb(false);
        updateDatabase(lastOpenedDb, dbFileName, openResult);
        return;

    case Database::Error::CouldNotOpen:
        // Something went wrong. We can't use the requested file.
        m_tournamentSettings->setLoadingDb(false);

        QMessageBox::warning(this,
            tr("Turnier öffnen"),
            tr("<p><b>Das Öffnen der Turnierdatenbank ist nicht möglich:</b></p>"
               "<p>Beim Öffnen der Datei „%1“ trat folgender Fehler auf:</p>").arg(
               QDir::toNativeSeparators(dbFileName).toHtmlEscaped()) + m_db->errorMessage());

        if (! lastOpenedDb.isEmpty()) {
            // Re-open the last opened database. The GUI still remains unchanged, so the user
            // won't notice that the currently opened database has been closed and re-opened.
            m_db->openDatabase(lastOpenedDb);
        }

        return;
    }

    databaseChanged();
    updateStatus(tr("Turnierdatenbank %1 geöffnet").arg(QDir::toNativeSeparators(dbFileName)));

    // We use a QTimer::singleShot lambda call here, so that the load state change is queued after
    // all other possible QTimer::singleShots that are initiated by other functions and all still
    // know that we are currently loading a database.
    QTimer::singleShot(0, this, [this]
    {
        m_tournamentSettings->setLoadingDb(false);
    });

    QApplication::restoreOverrideCursor();

    // Did we just open a backup?
    if (! m_openingAfterRestore) {
        BackupEngine backupEngine(dbFileName);
        if (! backupEngine.originalBaseName().isEmpty() && askRestoreOriginalDb(backupEngine)) {
            backupEngine.setFileName(backupEngine.originalFileName());
            showRestoreBackupDialog(backupEngine, backupEngine.originalFileName(), true);
        }
    }
}

void MainWindow::updateDatabase(const QString &lastOpenedDb, const QString &dbFileName,
                                Database::Error openError)
{
    if (! m_db->isWritable()) {
        updateStatus(tr("Datenbankupdate nicht möglich"));
        QMessageBox::warning(this,
            tr("Turnier öffnen"),
            tr("<p><b>Das Datenbankformat von Muckturnier hat sich geändert.</b></p>"
               "<p>Um die Turnierdatenbank „%1“ benutzen zu können, müsste ein Update der "
               "Datenbank durchgeführt werden, aber die Datei ist nicht beschreibbar.</p>"
               "<p>Bitte die Zugriffsrechte überprüfen, und die Datei erneut laden!</p>").arg(
               QDir::toNativeSeparators(dbFileName).toHtmlEscaped()));
        reopenLastOpenedDb(lastOpenedDb);
        return;
    }

    QMessageBox messageBox(QMessageBox::Question,
        tr("Update des Datenbankformats"),
        tr("<p><b>Das Datenbankformat von Muckturnier hat sich geändert.</b></p>"
           "<p>Soll die Turnierdatenbank „%1“ aktualisiert werden? Es wird dabei ein Backup der "
           "Original-Datenbank im selben Ordner angelegt.</p>").arg(
           QDir::toNativeSeparators(dbFileName).toHtmlEscaped()),
        QMessageBox::Cancel, this);
    QAbstractButton *yesButton = messageBox.addButton(tr("Aktualisieren"), QMessageBox::YesRole);

    messageBox.exec();
    if (messageBox.clickedButton() != yesButton) {
        updateStatus(tr("Datenbankupdate abgebrochen!"));
        QMessageBox::warning(this,
            tr("Update des Datenbankformats"),
            tr("<p><b>Das Update des Datenbankformats wurde abgebrochen.</b></p>"
               "<p>Die Turnierdatenbank „%1“ kann nicht geöffnet werden.</p>").arg(
               QDir::toNativeSeparators(dbFileName)));
        reopenLastOpenedDb(lastOpenedDb);
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    updateStatus(tr("Datenbankupdate läuft …"));

    auto backupSuffix = QStringLiteral("backup_dbv");
    switch (openError) {
    case Database::Error::NoError:
    case Database::Error::CouldNotOpen:
        Q_UNREACHABLE();
        break;
    case Database::Error::DbVersion3:
        backupSuffix += QLatin1String("3");
        break;
    case Database::Error::DbVersion4:
        backupSuffix += QLatin1String("4");
        break;
    case Database::Error::DbVersion5:
        backupSuffix += QLatin1String("5");
        break;
    case Database::Error::DbVersion6:
        backupSuffix += QLatin1String("6");
        break;
    case Database::Error::DbVersion7:
        backupSuffix += QLatin1String("7");
        break;
    case Database::Error::DbVersion8:
        backupSuffix += QLatin1String("8");
        break;
    case Database::Error::DbVersion9:
        backupSuffix += QLatin1String("9");
        break;
    case Database::Error::DbVersion10:
        backupSuffix += QLatin1String("10");
        break;
    case Database::Error::DbVersion11:
        backupSuffix += QLatin1String("11");
        break;
    case Database::Error::DbVersion12:
        backupSuffix += QLatin1String("12");
        break;
    case Database::Error::DbVersion13:
        backupSuffix += QLatin1String("13");
        break;
    }

    BackupEngine backupEngine(dbFileName);
    if (! backupEngine.createBackup(backupSuffix)) {
        updateStatus(tr("Datenbankupdate abgebrochen!"));
        QApplication::restoreOverrideCursor();
        QMessageBox::warning(this,
            tr("Update des Datenbankformats"),
            tr("<p><b>Die Backup-Datei konnte nicht angelegt werden.</b></p>"
               "<p>Das Update wird abgebrochen. Bitte die Zugriffsrechte (des Verzeichnisses „%2“) "
               "überprüfen!</p>").arg(
               QDir::toNativeSeparators(backupEngine.path()).toHtmlEscaped()));

        reopenLastOpenedDb(lastOpenedDb);
        return;
    }

    if (! m_db->updateDatabase(dbFileName, openError)) {
        updateStatus(tr("Datenbankupdate fehlgeschlagen!"));
        QApplication::restoreOverrideCursor();
        QMessageBox::critical(this,
            tr("Update des Datenbankformats"),
            tr("<p><b>Das Update der Datenbank „%1“ ist fehlgeschlagen!</b></p>"
               "<p>Das sollte nicht passieren. Entweder ist die Datei keine gültige "
               "Muckturnier-Turnierdatenbank, oder sie wurde während des Updates von einem anderen "
               "Prozess verändert.</p>%2").arg(
               QDir::toNativeSeparators(dbFileName).toHtmlEscaped(), m_db->errorMessage()));

        QFile backupFile(backupEngine.path() + QLatin1String("/") + backupEngine.backupFile());
        backupFile.remove();

        reopenLastOpenedDb(lastOpenedDb);
        return;
    }

    updateStatus(tr("Datenbankupdate beendet!"));
    QApplication::restoreOverrideCursor();

    QMessageBox::information(this,
        tr("Update des Datenbankformats"),
        tr("<p><b>Die Datenbank „%1“ wurde erfolgreich aktualisiert!</b></p>"
           "<p>Ein Backup der Originaldatei wurde mit dem Namen „%2“ erstellt.</p>").arg(
           QDir::toNativeSeparators(dbFileName).toHtmlEscaped(),
           QDir::toNativeSeparators(backupEngine.backupFile()).toHtmlEscaped()),
        QMessageBox::Ok);

    openTournament(dbFileName);
}

void MainWindow::reopenLastOpenedDb(const QString &lastOpenedDb)
{
    closeTournament();
    if (! lastOpenedDb.isEmpty()) {
        openTournament(lastOpenedDb);
    }
}

void MainWindow::showHandbook()
{
    const QString readmePath = m_resourceFinder->find(QStringLiteral("Readme.html"));

    if (readmePath.isEmpty()) {
        QMessageBox::warning(this, tr("Handbuch anzeigen"),
        tr("<p>Das Handbuch kann nicht angezeigt werden: Die Datei „Readme.html“ wurde nicht "
           "gefunden.</p>"
           "<p>Online ist unter <a href=\"%1\">%1</a> das aktuelle Handbuch zu finden!</p>").arg(
           Urls::readme));
        return;
    }

    if (! QDesktopServices::openUrl(QUrl::fromLocalFile(readmePath))) {
        QMessageBox::warning(this, tr("Handbuch anzeigen"),
        tr("<p>Das Handbuch kann nicht angezeigt werden: Die Datei „%1“ konnte nicht mit einem "
           "Browser geöffnet werden.<p>"
           "<p>Bitte die Readme-Datei manuell öffnen oder unter <a href=\"%2\">%2</a> online "
           "lesen!</p>").arg(readmePath, Urls::readme));
    }
}

void MainWindow::showAboutDialog()
{
    QDialog *aboutDialog = new AboutDialog(this, m_sharedObjects);
    aboutDialog->show();
}

void MainWindow::updateStatus(const QString &message)
{
    statusBar()->showMessage(message, 5000);
}

void MainWindow::databaseChanged()
{
    // Update all fixtures
    m_db->updateFixtures();

    // Update the GUI

    setWindowTitle(tr("Muckturnier: %1").arg(QFileInfo(m_db->dbFile()).fileName()));

    switch (m_tournamentSettings->tournamentMode()) {
    case Tournament::FixedPairs:
        m_exportPlayersListAction->setText(tr("Paarliste exportieren"));
        m_importPlayersListAction->setText(tr("Paarliste importieren"));
        break;
    case Tournament::SinglePlayers:
        m_exportPlayersListAction->setText(tr("Spielerliste exportieren"));
        m_importPlayersListAction->setText(tr("Spielerliste importieren"));
        break;
    }

    m_splashScreen->hide();
    m_scorePage->setupGui();
    m_rankingPage->setupGui();
    m_playersDock->show();
    m_scoreDock->show();
    m_rankingDock->show();

    // Update the StringTools
    m_stringTools->setBoogerScore(m_tournamentSettings->boogerScore());
    if (m_settings->saveOnCloseEnabled(Settings::SaveLastDbPath)) {
        m_settings->saveLastDbPath(m_db->dbPath());
    }

    // Setup the MarkersWidget
    MarkersWidget *markersWidget = m_registrationPage->markersWidget();
    // First, we block the markersChanged() signal so that it won't trigger
    // RegistrationPage::updatePlayersList here (first, we have to load the singles management
    // settings)
    markersWidget->blockSignals(true);
    // Then we reload the MarkersWidget so that we have the current markers definitions
    markersWidget->reload();
    // Here, we reset the visibility settings and load the singles management settings
    markersWidget->reset();
    markersWidget->blockSignals(false);

    // Update the booking QR helper
    m_sharedObjects->bookingEngine()->reload();

    // Show optional pages
    for (OptionalPages::Page page : m_tournamentSettings->optionalPages()) {
        switch (page) {
        case OptionalPages::DrawOverview:
            showDrawOverviewPage(1);
            break;
        case OptionalPages::Schedule:
            showSchedulePage();
            break;
        case OptionalPages::Booking:
            showBookingPage();
        }
    }

    // Update the Registration Page

    m_registrationPage->setColumnsVisible(m_tournamentSettings->registrationColumns());

    // Block signals so that the playersCountChanged signal is not emitted yet, as we want to run
    // MainWindow::playersCountChanged in each case (also if the count is the same, because another
    // database could have the same number of players like the currently opened one, and in this
    // case the signal would not be emitted, and the ScorePage update would not be triggered).
    m_registrationPage->blockSignals(true);

    // Reload, but suppress the MarkersWidget reload (as this has been already done)
    m_registrationPage->reload(false);

    m_registrationPage->blockSignals(false);

    // Emit registration page signals blocked above
    Q_EMIT m_registrationPage->drawAdjustmentNeeded(
               m_registrationPage->drawAdjustmentNeededState());

    auto *drawOverviewDock = findOptionalPageDock(OptionalPages::DrawOverview);
    if (drawOverviewDock != nullptr) {
        qobject_cast<DrawOverviewPage *>(drawOverviewDock->widget())->setRound(1);
    }

    // Set the tournament state
    m_setTournamentOpenAction->setChecked(m_tournamentSettings->tournamentOpen());
    m_setTournamentFinishedAction->setChecked(! m_tournamentSettings->tournamentOpen());

    // Enable/Disable menu actions

    registrationsAvailable(m_db->pairsOrPlayersCount() > 0);

    m_tournamentStateMenu->setEnabled(m_db->isWritable());
    m_setTournamentOpenAction->setEnabled(false);
    m_setTournamentFinishedAction->setEnabled(false);
    m_exportTournamentAction->setEnabled(false);
    m_closeTournamentAction->setEnabled(true);

    m_createDatabaseBackupAction->setEnabled(true);
    m_showRestoreBackupDialogAction->setEnabled(m_db->isChangeable());
    m_resetMainWindowStateAction->setEnabled(true);
    m_databaseExtrasMenu->menuAction()->setEnabled(true);

    m_showDrawOverviewPageAction->setEnabled(true);
    m_showServerPageAction->setEnabled(m_db->isChangeable()
                                       && m_serverDock == nullptr && m_clientDock == nullptr);
    m_showClientPageAction->setEnabled(m_db->isChangeable()
                                       && m_serverDock == nullptr && m_clientDock == nullptr);
    m_tabPositionMenu->setEnabled(true);
    m_pagesMenu->setEnabled(true);
    m_imExportMenu->setEnabled(true);
    m_resetDatabaseAction->setEnabled(m_db->isChangeable());
    m_showBookingPage->setEnabled(true);
    m_showSchedulePage->setEnabled(true);
    m_showWlanCodeScannerPage->setEnabled(true);
    m_showDrawOverviewPageAction->setEnabled(true);

    if (m_db->tournamentStarted()) {
        m_rankingPage->dataChanged();
        m_importPlayersListAction->setEnabled(false);
    } else {
        m_importPlayersListAction->setEnabled(m_db->isChangeable());
    }

    // Restore the state of the options we save in the database
    // The autoselect menu state is updated automatically when the tournamentSettings are loaded
    m_scorePage->setAutoSelectTableForDraw(m_tournamentSettings->autoSelectTableForDraw());
    m_scorePage->toggleAutoSelectTableForDraw();

    // This also reloads the ScorePage
    // We have to do it after m_scorePage->setAutoSelectState so that the assignment
    // mapping is created if automatic pairs/players selection is selected
    playersCountChanged(m_db->pairsOrPlayersCount());

    // Clear all possibly entered searches
    Q_EMIT resetAllSearches();

    // Restore the dock widget arrangement if we have a saved state (otherwise, an empty QByteArray
    // is returned by Settings::mainWindowState, and restoreState() returns without doing anything).
    restoreState(m_settings->mainWindowState());

    // Be sure to show all dock widgets. This should not be necessary, but caused by crashes or
    // whatever errors, it may be possible that the window state was saved when only the splash
    // screen was visible, and now the docks are hidden, making the whole program unusable.
    // This can be fixed by deleting the settings, but we can't expect an end-user to do this.
    const auto docks = findChildren<QDockWidget *>();
    for (auto *dock : docks) {
        dock->setVisible(true);
    }

    // Raise the correct dock widget
    if (m_db->isChangeable()) {
        if (m_db->tournamentStarted()) {
            m_scoreDock->raise();
        } else {
            m_playersDock->raise();
        }
    } else {
        m_rankingDock->raise();
    }

    // Ask again if the tournament should be closed
    m_dismissableMessage->setDismissed(DismissableMessage::AskFinishTournament, false);

    // Inform the user about the database being not changeable and/or the network not being usable

    if (m_db->isWritable() && ! m_tournamentSettings->tournamentOpen()) {
        QMessageBox::information(this,
            tr("Turnier ist abgeschlossen"),
            tr("<p><b>Turnier ist abgeschlossen</b></p>"
               "<p>Das geöffnete Turnier wurde als „abgeschlossen“ markiert und kann deswegen nur "
               "angezeigt, aber nicht verändert werden.</p>"
               "<p>Wenn Änderungen erfolgen sollen, kann das Turnier via „Datei“ → „Turnierstatus "
               "festlegen“ wieder als „offen“ markiert werden.</p>"));
    }

    if (m_serverDock != nullptr && ! m_db->isChangeable()) {
        QMessageBox::warning(this, tr("Netzwerk"),
            tr("<p><b>Turnierdatenbank ist nicht beschreibbar</b></p>"
               "<p>Die Netzwerkfunktionen können nur mit einer beschreibbaren Datenbank genutzt "
               "werden. Die Server-Seite wird geschlossen.</p>"));
        qobject_cast<ServerPage *>(m_serverDock->widget())->closePage();
    }

    if (m_clientDock != nullptr && ! m_db->isChangeable()) {
        QMessageBox::warning(this, tr("Netzwerk"),
            tr("<p><b>Turnierdatenbank ist nicht beschreibbar</b></p>"
               "<p>Die Netzwerkfunktionen können nur mit einer beschreibbaren Datenbank genutzt "
               "werden. Die Client-Seite wird geschlossen.</p>"));
        qobject_cast<ClientPage *>(m_clientDock->widget())->closePage();
    }

    // Possibly toggle the schedule dock if the "tournament open" state changed
    auto scheduleDock = findOptionalPageDock(OptionalPages::Schedule);
    if (scheduleDock != nullptr) {
        qobject_cast<SchedulePage *>(scheduleDock->widget())->reload();
    }

    // Update the booking page if it's shown
    auto bookingDock = findOptionalPageDock(OptionalPages::Booking);
    if (bookingDock != nullptr) {
        qobject_cast<BookingPage *>(bookingDock->widget())->reload();
    }

    // Check if we should raise the booking page
    if (bookingDock != nullptr
        && m_db->isChangeable()
        && ! m_db->tournamentStarted()
        && m_tournamentSettings->specialMarker(Markers::Booked) != -1
        && m_tournamentSettings->bookingEnabled()) {

        // In this case, we can suppose the user knows about special markers,
        // so we skip displaying the below "special markers" warning
        markersWidget->setShowSpecialMarkersWarning(false);

        bookingDock->raise();

    }

    // Possibly warn about special markers
    if (m_db->isChangeable()
        && ! m_db->tournamentStarted()
        && (m_tournamentSettings->specialMarker(Markers::Booked) != -1
            || m_tournamentSettings->singlesManagement().enabled)) {

        markersWidget->warnAboutSpecialMarkers();
    }
}

void MainWindow::tournamentStarted()
{
    m_registrationPage->reload();
    m_importPlayersListAction->setEnabled(! m_db->tournamentStarted());

    // This is a rare case. However, a user could mess up the database. So let's warn them.

    if (! m_db->tournamentStarted()) {
        const auto [ round2PlusDraws, success ] = m_db->round2PlusDrawsPresent();
        if (! success) {
            return;
        }

        if (round2PlusDraws) {
            QMessageBox::warning(this, tr("Auslosungen für Runde 2 oder später gesetzt"),
                tr("<p><b>Vorsicht!</b></p>"
                   "<p>Es wurde gerade das letzte eingegebene Ergebnis gelöscht. Somit können "
                   "neue Anmeldungen registriert und auch bestehende gelöscht werden. Gleichzeitig "
                   "sind aber bereits Auslosungen für Runde 2 oder später eingegeben worden.</p>"
                   "<p>Wenn jetzt die Anzahl der Anmeldungen verändert (verringert) wird, dann "
                   "kann es passieren, dass, bedingt durch die schon vorhandene Auslosung, bei der "
                   "Ergebniseingabe Tische ausgewählt werden, die gar nicht existieren.</p>"
                   "<p><b>Bitte alle Auslosungen für Runden ab Runde 2 kontrollieren und ggf. "
                   "korrigieren, falls die Anzahl der Anmeldungen verändert wird!</p>"
                   "<p>Auslosungen ab Runde 2 können wieder verändert werden, sobald mindestens "
                   "ein Ergebnis eingegeben wurde.</p>"));
        }
    }
}

void MainWindow::showExportRankingDialog()
{
    askFinishTournament();
    auto *dialog = new ExportRankingDialog(this, m_sharedObjects);
    connect(dialog, &ExportRankingDialog::updateStatus, this, &MainWindow::updateStatus);
    dialog->exec();
}

void MainWindow::showExportTournamentDialog()
{
    askFinishTournament();
    ExportTournamentDialog *dialog = new ExportTournamentDialog(this, m_sharedObjects);
    connect(dialog, &ExportTournamentDialog::updateStatus, this, &MainWindow::updateStatus);
    dialog->exec();
}

void MainWindow::formatDockWidget(QDockWidget *dock, const QString &id)
{
    dock->setObjectName(id);
    dock->setContextMenuPolicy(Qt::PreventContextMenu);
    dock->setFeatures(QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
    dock->setStyleSheet(QStringLiteral("QDockWidget { font-weight: bold; font-size: %1pt; }").arg(
                                       (int) double(dock->font().pointSize()) * 1.2));
}

void MainWindow::closeTournament()
{
    // Save the current tournament settings
    if (! m_db->saveSettings()) {
        return;
    }

    // Close a possibly existing WLAN scanner page without asking
    auto *wlanCodeScannerDock = findOptionalPageDock(OptionalPages::wlanCodeScannerPageId);
    if (wlanCodeScannerDock != nullptr) {
        qobject_cast<WlanCodeScannerPage *>(wlanCodeScannerDock->widget())->forceClosePage();
    }

    // Close all optional pages
    const auto docks = findChildren<QDockWidget *>();
    for (auto *dock : docks) {
        auto *widget = dock->widget();
        if (qobject_cast<OptionalPage *>(widget)) {
            qobject_cast<OptionalPage *>(widget)->closePage();
        }
    }

    m_db->closeDatabaseConnection();

    if (m_settings->saveOnCloseEnabled(Settings::SaveWindowState)) {
        m_settings->saveMainWindowState(saveState());
    }

    m_playersDock->hide();
    m_scoreDock->hide();
    m_rankingDock->hide();
    m_splashScreen->show();

    m_closeTournamentAction->setEnabled(false);

    m_createDatabaseBackupAction->setEnabled(false);
    m_showRestoreBackupDialogAction->setEnabled(false);
    m_resetDatabaseAction->setEnabled(false);
    m_databaseExtrasMenu->menuAction()->setEnabled(false);

    m_tournamentStateMenu->setEnabled(false);
    m_imExportMenu->setEnabled(false);
    m_exportPlayersListAction->setEnabled(false);
    m_exportTournamentAction->setEnabled(false);
    m_importPlayersListAction->setEnabled(false);
    m_showServerPageAction->setEnabled(false);
    m_showClientPageAction->setEnabled(false);
    m_showDrawOverviewPageAction->setEnabled(true);
    m_tabPositionMenu->setEnabled(false);
    m_pagesMenu->setEnabled(false);
    m_resetMainWindowStateAction->setEnabled(false);
    m_showSchedulePage->setEnabled(false);
    m_showBookingPage->setEnabled(false);
    m_showWlanCodeScannerPage->setEnabled(false);
    m_showDrawOverviewPageAction->setEnabled(false);

#ifdef DEBUG_MODE
    m_showChecksumsDialogAction->setEnabled(false);
#endif

    setWindowTitle(tr("Muckturnier"));
    updateStatus(tr("Bitte ein neues Turnier starten oder ein bestehendes öffnen!"));
}

void MainWindow::exportPlayersList()
{
    if (m_isClient || m_isServer) {
        QString message = tr("<p><b>Netzwerk läuft</b></p>"
                             "<p>Während das Netzwerk läuft, ist kein Listenexport möglich. ");
        if (m_isClient) {
            message.append(tr("Bitte die Verbindung zum Server beenden und den Export erneut "
                              "anfragen.</p>"));
        } else {
            message.append(tr("Bitte den Server stoppen und den Export erneut anfragen.</p>"));
        }

        QMessageBox::information(this, m_exportPlayersListAction->text(), message);
        return;
    }

    ExportPlayersDialog *dialog = new ExportPlayersDialog(this, m_sharedObjects);
    connect(dialog, &ExportPlayersDialog::updateStatus, this, &MainWindow::updateStatus);
    dialog->exec();
    return;
}

void MainWindow::hideWindowFrame()
{
    if (m_hideWindowFrameAction->isChecked()) {
        setWindowFlags(Qt::FramelessWindowHint);
    } else {
        setWindowFlags(windowFlags() & ~Qt::FramelessWindowHint);
    }
    show();
}

void MainWindow::hideMenuBar()
{
    const bool visible = ! m_hideMenubarAction->isChecked();

    menuBar()->setVisible(visible);
    m_viewMenubarShortcut->setEnabled(! visible);

    if (! visible) {
        QMessageBox::information(this, tr("Menüleiste ausgeblendet"),
            tr("<p>Die Menüleiste kann mit <b>Strg+M</b> wieder eingeblendet werden.</p>"
               "<p>Solang die Menüleiste ausgeblendet ist, sind alle Menü-Tastaturkürzel "
               "deaktiviert.</p>"));
    }
}

void MainWindow::attachDocks(Qt::DockWidgetArea area)
{
    const auto docks = findChildren<QDockWidget *>();
    for (auto *dock : docks) {
        dock->setFloating(false);
        addDockWidget(area, dock);
    }
}

void MainWindow::arrangeTabified()
{
    attachDocks(Qt::TopDockWidgetArea);
    const auto docks = findChildren<QDockWidget *>();
    for (int i = 1; i < docks.count(); i++) {
        tabifyDockWidget(docks.at(i - 1), docks.at(i));
    }
}

void MainWindow::arrangeHorizontally()
{
    attachDocks(Qt::TopDockWidgetArea);
    const auto docks = findChildren<QDockWidget *>();
    for (int i = 1; i < docks.count(); i++) {
        splitDockWidget(docks.at(i - 1), docks.at(i), Qt::Horizontal);
    }
}

void MainWindow::arrangeVertically()
{
    attachDocks(Qt::LeftDockWidgetArea);
    const auto docks = findChildren<QDockWidget *>();
    for (int i = 1; i < docks.count(); i++) {
        splitDockWidget(docks.at(i - 1), docks.at(i), Qt::Vertical);
    }
}

void MainWindow::arrangeOwnWindow()
{
    const auto docks = findChildren<QDockWidget *>();
    for (auto *dock : docks) {
        dock->setFloating(true);
    }
}

void MainWindow::processDbReset()
{
    m_registrationPage->markersWidget()->reset();
    m_registrationPage->reset();
    m_importPlayersListAction->setEnabled(true);
    m_exportPlayersListAction->setEnabled(false);
    m_exportTournamentAction->setEnabled(false);

    auto *scheduleDock = findOptionalPageDock(OptionalPages::Schedule);
    if (scheduleDock != nullptr) {
        qobject_cast<SchedulePage *>(scheduleDock->widget())->reset();
    }

    m_sharedObjects->bookingEngine()->reload();
    auto *bookingDock = findOptionalPageDock(OptionalPages::Booking);
    if (bookingDock != nullptr) {
        qobject_cast<BookingPage *>(bookingDock->widget())->reload();
    }
}

void MainWindow::resetDatabase()
{
    QCheckBox *createBackup = new QCheckBox(tr("Vorher ein Backup der Datenbank anlegen"));
    createBackup->setChecked(true);
    QMessageBox messageBox(this);
    messageBox.setIcon(QMessageBox::Warning);
    messageBox.setWindowTitle(tr("Datenbank zurücksetzen"));
    messageBox.setText(tr("Sollen wirklich alle angemeldeten %1 und alle eingegebenen "
                          "Ergebnisse gelöscht werden?").arg(
                          m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler")));
    messageBox.setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
    messageBox.setDefaultButton(QMessageBox::Cancel);
    messageBox.setCheckBox(createBackup);

    if (messageBox.exec() == QMessageBox::Cancel) {
        return;
    }

    if (createBackup->isChecked()) {
        if (! createDatabaseBackup()) {
            QMessageBox::warning(this,
                tr("Datenbank zurücksetzen"),
                tr("Das angefragte Datenbankbackup konnte nicht angelegt werden. Das Zurücksetzen "
                   "der Datenbank wird abgebrochen."));
            return;
        }
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    updateStatus(tr("Setzte Turnierdatenbank zurück …"));

    if (! m_db->reset()) {
        return;
    }

    if (! m_db->addMarkers(m_settings->markersTemplate(m_tournamentSettings->tournamentMode()))) {
        return;
    }

    m_tournamentSettings->setSpecialMarker(Markers::Default, m_settings->marker(Markers::Default));

    const int bookedMarker = m_settings->marker(Markers::Booked);
    if (bookedMarker == -1) {
        // We need to explicitely set checkable markers if disabled, so that "(unmarked)" will
        // be selected in the disabled group box instead of a probably other setting being chosen
        // before.
        m_registrationPage->markersWidget()->setBookedMarker(0);
    }
    m_tournamentSettings->setSpecialMarker(Markers::Booked, bookedMarker);

    const int drawnMarker = m_settings->marker(Markers::Drawn);
    if (drawnMarker == -1) {
        m_registrationPage->markersWidget()->setDrawnMarker(0);
    }
    m_tournamentSettings->setSpecialMarker(Markers::Drawn, drawnMarker);

    m_tournamentSettings->setSinglesManagement(m_settings->singlesManagementTemplate());

    m_tournamentSettings->resetBookingSettings();

    processDbReset();

    m_rankingPage->reload();
    m_registrationPage->reload();
    m_playersDock->raise();

    m_createDatabaseBackupAction->setEnabled(true);
    m_showRestoreBackupDialogAction->setEnabled(m_db->isChangeable());

    QApplication::restoreOverrideCursor();
    updateStatus(tr("Turnierdatenbank zurückgesetzt."));

    QMessageBox::information(this,
                             tr("Datenbank zurücksetzen"),
                             tr("Die Datenbank wurde zurückgesetzt."));
}

void MainWindow::importPlayersList()
{
    if (m_isClient || m_isServer) {
        QString message = tr("<p><b>Netzwerk läuft</b></p>"
                             "<p>Während eine Netzwerkverbindung bzw. der Server läuft, ist kein "
                             "Listenimport möglich.</p>"
                             "<p>Bitte den Server stoppen, die Liste am Server importieren und "
                             "danach alle Clients neu verbinden (die übernehmen dann die neuen "
                             "Anmeldungen automatisch).</p>");
        if (m_isClient) {
            message.append(tr("<p><b>Vorsicht:</b> Dieser Rechner läuft als Client. Alle "
                              "Änderungen an der Anmeldungsliste, die hier „offline“ durchgeführt "
                              "werden, werden beim nächsten Verbinden mit dem Server verworfen!"
                              "</p>"));
        }
        QMessageBox::information(this, m_importPlayersListAction->text(), message);
        return;
    }

    ImportPlayersDialog *dialog = new ImportPlayersDialog(
        this, m_sharedObjects, m_registrationPage->registerPhoneticSearchEnabled());
    connect(dialog, &ImportPlayersDialog::updateStatus, this, &MainWindow::updateStatus);

    if (! dialog->exec()) {
        updateStatus(tr("Import abgebrochen"));
    } else {
        m_registrationPage->updatePlayersList();
        updateStatus(tr("Import beendet"));
    }
}

void MainWindow::sqlError(const QString &text)
{
    // This should not happen

    while (QApplication::overrideCursor() != nullptr) {
        QApplication::restoreOverrideCursor();
    }

    QTimer::singleShot(0, this, [this, text]
    {
        // Close the server connection if we're connected to a server
        if (m_clientDock != nullptr) {
            qobject_cast<ClientPage *>(m_clientDock->widget())->closePage();
        }

        // Close the server if are a server
        if (m_serverDock != nullptr) {
            qobject_cast<ServerPage *>(m_serverDock->widget())->closePage();
        }

        QMessageBox::critical(this, tr("Fehler in der Datenbankkommunikation"),
            tr("<p><b>Fehler in der Datenbankkommunikation</b></p>"
               "<p>Es ist ein unerwarteter Fehler während einer Datenbankabfrage aufgetreten. Das "
               "sollte nicht passieren!</p>"
               "<p>Um einen Datenverlust zu vermeiden, wird versucht, den letzten konsistenten "
               "Zustand der Datenbank wiederherzustellen und die Datenbank geschlossen.</p>")
            + text);

        closeTournament();
    });
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    // Do we have a running server?
    if (m_serverDock != nullptr
        && ! qobject_cast<ServerPage *>(m_serverDock->widget())->closePage()) {

        event->ignore();
        return;
    }

    // Do we have a connected client?
    if (m_clientDock != nullptr
        && ! qobject_cast<ClientPage *>(m_clientDock->widget())->closePage()) {

        event->ignore();
        return;
    }

    // Do we have running stopwatches?
    if (m_stopWatchEngine->stopWatchRunning() && QMessageBox::warning(this,
            tr("Muckturnier beenden"),
            tr("<p>Es läuft mindestens eine Stoppuhr! Alle Stoppuhren werden beim Schließen "
               "ebenfalls geschlossen, und die Zeitinformation geht verloren!</p>"
               "<p>Soll Muckturnier trotzdem geschlossen werden?</p>"),
            QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Cancel)  == QMessageBox::Cancel) {

        event->ignore();
        return;
    }

    m_stopWatchEngine->closeAll();

    // Close the draw display if shown
    auto *drawOverviewDock = findOptionalPageDock(OptionalPages::Schedule);
    if (drawOverviewDock != nullptr) {
        qobject_cast<SchedulePage *>(drawOverviewDock->widget())->closeDisplay();
    }

    // Close the schedule display if shown
    auto *scheduleDock = findOptionalPageDock(OptionalPages::DrawOverview);
    if (scheduleDock != nullptr) {
        qobject_cast<DrawOverviewPage *>(scheduleDock->widget())->closeDisplay();
    }

    // Save the main window geometry
    if (m_settings->saveOnCloseEnabled(Settings::SaveWindowGeometry)) {
        m_settings->saveMainWindowGeometry(saveGeometry());
    }

    // If a database is currently opened, save the current settings and close it
    if (m_db->isOpen()) {
        closeTournament();
    }

    // Accept the close
    event->accept();
}

bool MainWindow::createDatabaseBackup()
{
    const QString openedDb = m_db->dbFile();
    BackupEngine backupEngine(openedDb);

    if (! backupEngine.originalBaseName().isEmpty()
        && QMessageBox::question(this,
            tr("Backup der Datenbank anlegen"),
            tr("<p>Es sieht so aus, als wäre die geöffnete Turnierdatenbank<br/>"
               "<b>%1.mtdb</b><br/>"
               "ein Backup von<br/>"
               "<b>%2.mtdb</b><br/>"
               "das %3 erstellt wurde. Soll wirklich ein Backup von der Backup-Datei erstellt "
               "werden?</p>").arg(
               backupEngine.baseName().toHtmlEscaped(),
               backupEngine.originalBaseName().toHtmlEscaped(),
               backupEngine.messageDate()),
            QMessageBox::Yes | QMessageBox::No) == QMessageBox::No) {

        return false;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    updateStatus(tr("Backup wird angelegt …"));

    if (! m_db->saveSettings()) {
        return false;
    }
    m_db->closeDatabaseConnection();
    bool success = backupEngine.createBackup();
    m_db->openDatabase(openedDb);

    QApplication::restoreOverrideCursor();

    if (! success) {
        updateStatus(tr("Backup konnte nicht erstellt werden!"));
        QMessageBox::critical(this,
            tr("Backup der Datenbank anlegen"),
            tr("<p><b>Das Anlegen eines Backups ist fehlgeschlagen!</b></p>"
               "<p>Die Datei konnte nicht angelegt werden. Bitte die Zugriffsrechte von „%1“ bzw. "
               "dem Verzeichnis „%2“) überprüfen!</p>").arg(
               QDir::toNativeSeparators(backupEngine.backupFile().toHtmlEscaped()),
               QDir::toNativeSeparators(backupEngine.path()).toHtmlEscaped()),
            QMessageBox::Ok);
        return false;
    }

    updateStatus(tr("Backup erfolgreich erstellt!"));
    QMessageBox::information(this,
        tr("Backup der Datenbank anlegen"),
        tr("Es wurde erfolgreich ein Backup der Datenbank mit dem Dateinamen „%1“ erstellt.").arg(
           QDir::toNativeSeparators(backupEngine.backupFile()).toHtmlEscaped()),
        QMessageBox::Ok);

    return true;
}

bool MainWindow::askRestoreOriginalDb(BackupEngine &backupEngine)
{
    if (backupEngine.originalBaseName().isEmpty()) {
        return false;
    }

    if (m_dismissableMessage->dismissed(DismissableMessage::AskRestoreOriginalDb)) {
        return m_restoreOriginalDb;
    }

    m_restoreOriginalDb = m_dismissableMessage->question(
        DismissableMessage::AskRestoreOriginalDb,
        this,
        tr("Backup wiederherstellen"),
        tr("<p><b>Soll ein Backup wiederhergestellt werden?</b></p>"
           "<p>Es sieht so aus, als wäre die soeben geöffnete Turnierdatenbank<br/>"
           "<b>%1.mtdb</b><br/>"
           "ein Backup von<br/>"
           "<b>%2.mtdb</b><br/>"
           "das %3 erstellt wurde. Soll das Backup wiederhergestellt werden?</p>").arg(
           backupEngine.baseName().toHtmlEscaped(),
           backupEngine.originalBaseName().toHtmlEscaped(),
           backupEngine.messageDate())) == QMessageBox::Yes;

    return m_restoreOriginalDb;
}

void MainWindow::triggerShowRestoreBackupDialog()
{
    BackupEngine backupEngine(m_db->dbFile());
    if (askRestoreOriginalDb(backupEngine)) {
        backupEngine.setFileName(backupEngine.originalFileName());
        showRestoreBackupDialog(backupEngine, backupEngine.originalFileName(), false);
    } else {
        showRestoreBackupDialog(backupEngine, m_db->dbFile(), false);
    }
}

void MainWindow::showRestoreBackupDialog(BackupEngine &backupEngine, const QString &dbToOverwrite,
                                         bool autoAccept)
{
    RestoreBackupDialog *dialog = new RestoreBackupDialog(this, m_db, backupEngine, dbToOverwrite,
                                                          autoAccept);
    connect(dialog, &RestoreBackupDialog::statusUpdate, this, &MainWindow::updateStatus);
    connect(dialog, &RestoreBackupDialog::closeTournament, this, &MainWindow::closeTournament);
    connect(dialog, &RestoreBackupDialog::openTournament, this, &MainWindow::openRestoredDb);
    dialog->exec();
}

void MainWindow::openRestoredDb(const QString &dbFile)
{
    m_openingAfterRestore = true;
    openTournament(dbFile);
    m_openingAfterRestore = false;
}

void MainWindow::registrationsAvailable(bool state)
{
    m_exportPlayersListAction->setEnabled(state);
#ifdef DEBUG_MODE
    m_showChecksumsDialogAction->setEnabled(state);
#endif
}

#ifdef DEBUG_MODE
void MainWindow::showChecksumsDialog()
{
    auto *dialog = new ChecksumsDialog(this, m_db, m_tournamentSettings->isPairMode());
    dialog->exec();
}
#endif

void MainWindow::showSettingsDialog()
{
    const auto oldNamesSeparator = m_settings->namesSeparator();
    const bool resetSingleConditionString
        = m_tournamentSettings->isPairMode()
          && (m_registrationPage->markersWidget()->singleConditionString()
              == m_settings->namesSeparator().simplified());

    SettingsDialog *dialog = new SettingsDialog(this, m_sharedObjects, m_isServer || m_isClient);
    if (! dialog->exec()) {
        return;
    }

    if (resetSingleConditionString) {
        m_registrationPage->markersWidget()->resetSingleConditionString();
    }

    if (m_db->isOpen() && oldNamesSeparator != m_settings->namesSeparator()) {
        // The names separator has been changed.
        // We have to reload the score page and the draw overview page (if shown)

        // This will reload the score page and preserve possibly entered data
        m_scorePage->playersNameEdited();

        // Update the draw overview page if it's shown
        auto *drawOverviewDock = findOptionalPageDock(OptionalPages::DrawOverview);
        if (drawOverviewDock != nullptr) {
            auto *page = qobject_cast<DrawOverviewPage *>(drawOverviewDock->widget());
            page->updateDraw(0, QString());
        }
    }
}

void MainWindow::showServerPage()
{
    m_showServerPageAction->setEnabled(false);
    m_showClientPageAction->setEnabled(false);

    if (m_networkStatusWidget == nullptr) {
        m_networkStatusWidget = new NetworkStatusWidget(m_sharedObjects->iconSizeEngine());
    }
    m_networkStatusWidget->setMode(NetworkStatusWidget::Mode::Server);
    statusBar()->addPermanentWidget(m_networkStatusWidget);
    m_networkStatusWidget->show();

    auto *serverPage = new ServerPage(this, m_sharedObjects, m_registrationPage, m_scorePage,
                                      m_stopWatchEngine);
    auto *server = serverPage->server();

    connect(server, &Server::clientCountChanged,
            m_networkStatusWidget, &NetworkStatusWidget::clientCountChanged);
    connect(server, &Server::tx, m_networkStatusWidget, &NetworkStatusWidget::tx);
    connect(server, &Server::rx, m_networkStatusWidget, &NetworkStatusWidget::rx);

    connect(serverPage, &ServerPage::serverStarted, this, &MainWindow::serverStarted);
    connect(serverPage, &ServerPage::serverStopped, this, &MainWindow::serverStopped);
    connect(serverPage, &ServerPage::statusUpdate, this, &MainWindow::updateStatus);
    connect(serverPage, &ServerPage::pageClosed, this, &MainWindow::serverPageClosed);

    server->setChecksum(&m_registrationPage->checksum());

    m_serverDock = new QDockWidget(tr("Netzwerk (Server)"), this);
    m_serverDock->setWidget(serverPage);
    formatDockWidget(m_serverDock, OptionalPages::serverPageId);
    restoreOrTabifyDockWidget(m_serverDock);
}

void MainWindow::serverStarted()
{
    m_networkStatusWidget->clientCountChanged(0);

    m_db->setNetworkTournamentStarted(m_db->tournamentStarted());

    auto *server = qobject_cast<ServerPage *>(m_serverDock->widget())->server();
    m_registrationPage->serverStarted(server);
    m_scorePage->serverStarted(server);
    m_stopWatchEngine->serverStarted(server);

    setNetworkActionsEnabled(false);
    m_isServer = true;
    m_finishRegistrationAction->setEnabled(! m_db->networkTournamentStarted()
                                           && m_db->playersDividable()
                                           && ! m_registrationPage->singlesPresent()
                                           && ! m_registrationPage->bookedPresent());
    m_scorePage->setNetworkBlocked(! m_db->networkTournamentStarted());

    auto *existingDock = findOptionalPageDock(OptionalPages::Booking);
    if (existingDock != nullptr) {
        qobject_cast<BookingPage *>(existingDock->widget())->setNetworkBlocked(true);
    }
}

void MainWindow::serverStopped()
{
    m_networkStatusWidget->setMode(NetworkStatusWidget::Mode::Server);

    m_registrationPage->serverStopped();
    m_scorePage->serverStopped();
    m_stopWatchEngine->serverStopped();

    setNetworkActionsEnabled(true);
    m_isServer = false;
    m_finishRegistrationAction->setEnabled(false);
    m_db->setNetworkTournamentStarted(false);
    m_registrationPage->reload();
    m_scorePage->setNetworkBlocked(false);

    auto *existingDock = findOptionalPageDock(OptionalPages::Booking);
    if (existingDock != nullptr) {
        qobject_cast<BookingPage *>(existingDock->widget())->setNetworkBlocked(false);
    }
}

void MainWindow::showClientPage()
{
    m_showServerPageAction->setEnabled(false);
    m_showClientPageAction->setEnabled(false);

    if (m_networkStatusWidget == nullptr) {
        m_networkStatusWidget = new NetworkStatusWidget(m_sharedObjects->iconSizeEngine());
    }
    m_networkStatusWidget->setMode(NetworkStatusWidget::Mode::Client);
    statusBar()->addPermanentWidget(m_networkStatusWidget);
    m_networkStatusWidget->show();

    auto *clientPage = new ClientPage(this, m_sharedObjects, m_registrationPage, m_scorePage,
                                      m_stopWatchEngine, m_lastServerIp, m_lastServerPort);
    auto *client = clientPage->client();

    connect(client, &Client::tx, m_networkStatusWidget, &NetworkStatusWidget::tx);
    connect(client, &Client::rx, m_networkStatusWidget, &NetworkStatusWidget::rx);

    connect(clientPage, &ClientPage::namesDifferencesDisplayed,
            this, &MainWindow::setNameAdoptionMode);
    connect(clientPage, &ClientPage::connectionRequested,
            this, &MainWindow::serverConnectionRequested);
    connect(clientPage, &ClientPage::connected, this, &MainWindow::clientConnected);
    connect(clientPage, &ClientPage::connectionClosed, this, &MainWindow::clientConnectionClosed);
    connect(clientPage, &ClientPage::statusUpdate, this, &MainWindow::updateStatus);
    connect(clientPage, &ClientPage::pageClosed, this, &MainWindow::clientPageClosed);
    connect(client, &Client::requestRejected,
            m_registrationPage, &RegistrationPage::requestRejected);

    client->setChecksum(&m_registrationPage->checksum());

    m_clientDock = new QDockWidget(tr("Netzwerk (Client)"), this);
    connect(clientPage, &ClientPage::raiseMe, m_clientDock, &QDockWidget::raise);

    m_clientDock->setWidget(clientPage);
    formatDockWidget(m_clientDock, OptionalPages::clientPageId);
    restoreOrTabifyDockWidget(m_clientDock);
}

void MainWindow::serverConnectionRequested(const QString &ip, int port)
{
    m_lastServerIp = ip;
    m_lastServerPort = port;
}

void MainWindow::clientConnected()
{
    m_networkStatusWidget->setConnected(true);

    auto *client = qobject_cast<ClientPage *>(m_clientDock->widget())->client();
    m_registrationPage->clientConnected(client);
    m_scorePage->clientConnected(client);
    m_scorePage->reload();
    m_stopWatchEngine->clientConnected(client);

    setNetworkActionsEnabled(false);
    m_isClient = true;
    m_finishRegistrationAction->setEnabled(! m_db->networkTournamentStarted()
                                           && m_db->playersDividable()
                                           && ! m_registrationPage->singlesPresent()
                                           && ! m_registrationPage->bookedPresent());
    m_scorePage->setNetworkBlocked(! m_db->networkTournamentStarted());

    m_sharedObjects->bookingEngine()->reload();
    auto *existingDock = findOptionalPageDock(OptionalPages::Booking);
    if (existingDock != nullptr) {
        auto *bookingPage = qobject_cast<BookingPage *>(existingDock->widget());
        bookingPage->setNetworkBlocked(true);
        bookingPage->reload();
    }
}

void MainWindow::clientConnectionClosed()
{
    m_networkStatusWidget->setConnected(false);

    m_registrationPage->clientDisconnected();
    m_scorePage->clientDisconnected();
    m_stopWatchEngine->clientDisconnected();

    setNetworkActionsEnabled(true);
    m_isClient = false;
    m_db->setNetworkTournamentStarted(false);

    if (m_stringTools->resetLocale()) {
        QMessageBox::information(this,
            tr("Gebietsschema wiederhergestellt"),
            tr("Das ursprünglich benutzte Gebietsschema „%1“ wurde wiederhergestellt.").arg(
               m_stringTools->defaultLocale()));
    }
    m_registrationPage->reload();

    if (m_settings->resetNamesSeparator()) {
        // This will reload the page and restore the currently entered score
        m_scorePage->playersNameEdited();
    }

    m_finishRegistrationAction->setEnabled(false);
    m_scorePage->setNetworkBlocked(false);

    auto *existingDock = findOptionalPageDock(OptionalPages::Booking);
    if (existingDock != nullptr) {
        qobject_cast<BookingPage *>(existingDock->widget())->setNetworkBlocked(false);
    }
}

void MainWindow::serverPageClosed()
{
    m_serverDock = nullptr;
    networkPageClosed();
}

void MainWindow::clientPageClosed()
{
    m_clientDock = nullptr;
    networkPageClosed();
}

void MainWindow::networkPageClosed()
{
    m_showServerPageAction->setEnabled(true);
    m_showClientPageAction->setEnabled(true);
    setNetworkActionsEnabled(true);
    m_isServer = false;
    m_isClient = false;
    m_finishRegistrationAction->setEnabled(false);
    m_db->setNetworkTournamentStarted(false);
    m_registrationPage->reload();
    m_scorePage->setNetworkBlocked(false);
    statusBar()->removeWidget(m_networkStatusWidget);
}

void MainWindow::setNameAdoptionMode(bool state)
{
    menuBar()->setEnabled(! state);
    const auto docks = findChildren<QDockWidget *>();
    for (auto *dock : docks) {
        if (dock->objectName() != OptionalPages::clientPageId) {
            dock->setEnabled(! state);
        }
    }
}

void MainWindow::setNetworkActionsEnabled(bool state)
{
    m_resetDatabaseAction->setEnabled(state);
    m_showRestoreBackupDialogAction->setEnabled(state);
    m_newTournamentAction->setEnabled(state);
    m_openTournamentAction->setEnabled(state);
    m_closeTournamentAction->setEnabled(state);
}

void MainWindow::networkTournamentStarted()
{
    m_scorePage->setNetworkBlocked(false);
    m_finishRegistrationAction->setEnabled(false);
}

QDockWidget *MainWindow::findOptionalPageDock(OptionalPages::Page page) const
{
    return findOptionalPageDock(OptionalPages::pageId.value(page));
}

QDockWidget *MainWindow::findOptionalPageDock(const QString &id) const
{
    const auto docks = findChildren<QDockWidget *>();
    for (auto *dock : docks) {
        if (dock->objectName() == id) {
            return dock;
        }
    }
    return nullptr;
}

void MainWindow::showDrawOverviewPage(int round)
{
    auto *existingDock = findOptionalPageDock(OptionalPages::DrawOverview);
    if (existingDock != nullptr) {
        existingDock->raise();
        return;
    }

    auto *page = new DrawOverviewPage(this, m_sharedObjects, &m_registrationPage->playersData(),
                                      m_registrationPage->drawAdjustmentNeededState());
    connect(page, &DrawOverviewPage::pageClosed,
            this, [this]
            {
                m_registrationPage->setDrawOverviewPage(nullptr);
                m_tournamentSettings->setOptionalPageShown(OptionalPages::Page::DrawOverview,
                                                           false);
            });
    connect(m_registrationPage, &RegistrationPage::drawAdjustmentNeeded,
            page, &DrawOverviewPage::drawAdjustmentNeeded);
    connect(m_registrationPage, &RegistrationPage::requestUpdateDrawOverviewPage,
            page, &DrawOverviewPage::updateDraw);
    connect(page, &DrawOverviewPage::requestShowAdjustDrawDialog,
            this, &MainWindow::showAdjustDrawDialog);
    connect(this, &MainWindow::resetAllSearches, page, &DrawOverviewPage::resetSearch);

    auto *dock = new QDockWidget(OptionalPages::pageTitle.value(OptionalPages::DrawOverview), this);
    dock->setWidget(page);
    m_registrationPage->setDrawOverviewPage(page);
    page->setRound(round);
    formatDockWidget(dock, OptionalPages::pageId.value(OptionalPages::DrawOverview));
    restoreOrTabifyDockWidget(dock);

    m_tournamentSettings->setOptionalPageShown(OptionalPages::DrawOverview, true);

    if (! m_tournamentSettings->loadingDb()) {
        m_registrationPage->askShowColumn(RegistrationColumns::Draw);
    }
}

void MainWindow::showSchedulePage()
{
    auto *existingDock = findOptionalPageDock(OptionalPages::Schedule);
    if (existingDock != nullptr) {
        existingDock->raise();
        return;
    }

    auto *page = new SchedulePage(m_sharedObjects, m_stopWatchEngine, this);
    connect(page, &OptionalPage::pageClosed,
            m_tournamentSettings, std::bind(&TournamentSettings::setOptionalPageShown,
                                            m_tournamentSettings,
                                            OptionalPages::Schedule, false));

    auto *dock = new QDockWidget(OptionalPages::pageTitle.value(OptionalPages::Schedule), this);
    dock->setWidget(page);
    connect(page, &SchedulePage::raiseMe, dock, &QDockWidget::raise);
    formatDockWidget(dock, OptionalPages::pageId.value(OptionalPages::Schedule));
    restoreOrTabifyDockWidget(dock);

    m_tournamentSettings->setOptionalPageShown(OptionalPages::Schedule, true);
}

void MainWindow::showBookingPage()
{
    auto *existingDock = findOptionalPageDock(OptionalPages::Booking);
    if (existingDock != nullptr) {
        qobject_cast<BookingPage *>(existingDock->widget())->setEnabled(m_db->isChangeable());
        existingDock->raise();
        return;
    }

    auto *page = new BookingPage(this, m_sharedObjects, m_registrationPage);
    connect(page, &OptionalPage::pageClosed,
            m_tournamentSettings, std::bind(&TournamentSettings::setOptionalPageShown,
                                            m_tournamentSettings,
                                            OptionalPages::Booking, false));
    connect(m_registrationPage->markersWidget(), &MarkersWidget::bookedMarkerChange,
            page, &BookingPage::bookedMarkerChange);
    connect(m_registrationPage, &RegistrationPage::listWasModified,
            page, &BookingPage::updateBookings);
    connect(m_registrationPage, &RegistrationPage::requestShowBookingData,
            page, &BookingPage::selectBookingEntry);
    connect(m_registrationPage, &RegistrationPage::requestSaveBookingPng,
            page, &BookingPage::savePngFile);

    page->setEnabled(m_db->isChangeable());
    page->setNetworkBlocked(m_isServer || m_isClient);
    page->updateBookings();

    auto *dock = new QDockWidget(OptionalPages::pageTitle.value(OptionalPages::Booking), this);
    dock->setWidget(page);
    formatDockWidget(dock, OptionalPages::pageId.value(OptionalPages::Booking));
    restoreOrTabifyDockWidget(dock);

    m_tournamentSettings->setOptionalPageShown(OptionalPages::Booking, true);
}

void MainWindow::showWlanCodeScannerPage()
{
    auto *existingDock = findOptionalPageDock(OptionalPages::wlanCodeScannerPageId);
    if (existingDock != nullptr) {
        existingDock->raise();
        return;
    }

    auto *page = new WlanCodeScannerPage(this, m_settings);
    connect(page, &WlanCodeScannerPage::statusUpdate, this, &MainWindow::updateStatus);
    connect(page, &WlanCodeScannerPage::codeReceived,
            m_registrationPage, &RegistrationPage::bookingCodeScanned);

    auto *dock = new QDockWidget(tr("WLAN-Code-Scanner"), this);
    dock->setWidget(page);
    formatDockWidget(dock, OptionalPages::wlanCodeScannerPageId);
    restoreOrTabifyDockWidget(dock);
}

void MainWindow::resetMainWindowState()
{
    QDockWidget *activeDock = nullptr;
    QObject *widget = QApplication::focusWidget();

    if (widget == nullptr) {
        activeDock = m_playersDock;
    } else {
        while (true) {
            if (widget == nullptr) {
                break;
            }

            activeDock = qobject_cast<QDockWidget *>(widget);
            if (activeDock != nullptr) {
                break;
            }

            widget = widget->parent();
        }
    }

    arrangeTabified();
    if (activeDock != nullptr) {
        activeDock->raise();
    }

    QString text(tr("<p><b>Seitenanordnung zurücksetzen</b></p>"
                    "<p>Die Seitenanordnung wurde auf die Voreinstellung (alle Seiten werden als "
                    "Tabs nebeneinander dargestellt) zurückgesetzt.</p>"));

    if (m_settings->saveOnCloseEnabled(Settings::SaveWindowState)) {
        text += tr("<p>Die Anordnung der Seiten wird beim Beenden gespeichert und beim Neuanlegen "
                   "bzw. Öffnen einer Turnierdatenbank wiederhergestellt.</p>"
                   "<p>Diese Einstellung kann unter „Extras“ → „Einstellungen“ → „Beim Beenden "
                   "speichern“ deaktiviert werden.</p>");
    }

    QMessageBox::information(this, tr("Seitenanordnung zurücksetzen"), text);
}

void MainWindow::restoreOrTabifyDockWidget(QDockWidget *dock)
{
    if (! restoreDockWidget(dock)) {
        tabifyDockWidget(m_playersDock, dock);
    }

    QTimer::singleShot(0, this, [this, dock]
    {
        if (! m_tournamentSettings->loadingDb()) {
            dock->raise();
        }
        dock->show();
    });
}

void MainWindow::setDockTabPosition(QTabWidget::TabPosition position, bool checked)
{
    if (! checked) {
        // The respective main menu action was deselected
        return;
    }

    setTabPosition(Qt::AllDockWidgetAreas, position);
    m_settings->saveTabPosition(s_tabPositionMap.key(position));
}

void MainWindow::askSetTournamentOpen(bool state)
{
    if (m_tournamentSettings->tournamentOpen() == state) {
        return;
    }

    if (! state) {
        QString networkWarning;
        if (m_isServer) {
            networkWarning = tr("Der Server wird dadurch geschlossen und alle "
                                "Netzwerkverbindungen werden beendet.");
        }
        if (m_isClient) {
            networkWarning = tr("Die Serververbindung wird dadurch geschlossen.");
        }

        if (QMessageBox::question(this,
                tr("Turnierstatus ändern"),
                tr("<p><b>Turnierstatus ändern</b></p>"
                   "<p>Wenn das Turnier als „abgeschlossen“ markiert wird, dann können (unabhängig "
                   "davon, ob die Datenbankdatei beschreibbar ist oder nicht) keine Änderungen "
                   "mehr vorgenommen werden.</p>"
                   "<p>Soll das Turnier als „abgeschlossen“ markiert werden? %1</p>").arg(
                   networkWarning),
                QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Yes) == QMessageBox::Cancel) {

            m_setTournamentOpenAction->setChecked(true);
            m_setTournamentFinishedAction->setChecked(false);
            return;
        }

    } else {
        if (QMessageBox::question(this,
                tr("Turnierstatus ändern"),
                tr("<p><b>Turnierstatus ändern</b></p>"
                   "<p>Das Turnier ist momentan als „abgeschlossen“ markiert und damit (unabhängig "
                   "von den Zugriffsrechten der Datenbankdatei) schreibgeschützt. Wenn der Status "
                   "auf „offen“ geändert wird, dann sind wieder Änderungen möglich.</p>"
                   "<p>Soll das Turnier als „offen“ markiert werden?</p>"),
                QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Yes) == QMessageBox::Cancel) {

            return;
        }
    }

    setTournamentOpen(state);
}

void MainWindow::askFinishTournament()
{
    if (m_dismissableMessage->dismissed(DismissableMessage::AskFinishTournament)
        || ! m_tournamentSettings->tournamentOpen()) {

        return;
    }

    QString networkWarning;
    if (m_isServer) {
        networkWarning = tr("<p>Der Server wird dann geschlossen und alle Netzwerkverbindungen "
                            "werden beendet.</p>");
    }
    if (m_isClient) {
        networkWarning = tr("<p>Die Serververbindung wird dann geschlossen.</p>");
    }

    if (m_dismissableMessage->question(DismissableMessage::AskFinishTournament, this,
            tr("Turnierdaten exportieren"),
            tr("<p>Ist das Turnier beendet?</p>"
               "<p>Soll das Turnier als „abgeschlossen“ markiert werden? Dann sind keine "
               "(unabsichtlichen) Änderungen mehr möglich (einstellbar über „Datei“ → "
               "„Turnierstatus“).</p>"
               "%1").arg(networkWarning),
            QMessageBox::No) == QMessageBox::Yes) {

        setTournamentOpen(false);
    }
}

void MainWindow::setTournamentOpen(bool state)
{
    if (! state) {
        if (m_isServer) {
            qobject_cast<ServerPage *>(m_serverDock->widget())->closePage();
        }
        if (m_isClient) {
            qobject_cast<ClientPage *>(m_clientDock->widget())->closePage();
        }
    }

    // Close the possibly currently running round if the schedule page is shown
    auto *scheduleDock = findOptionalPageDock(OptionalPages::Schedule);
    if (scheduleDock != nullptr) {
        qobject_cast<SchedulePage *>(scheduleDock->widget())->checkStopRound();
    }

    m_tournamentSettings->setTournamentOpen(state);

    // Update the GUI

    m_tournamentSettings->setLoadingDb(true);

    databaseChanged();

    // We have to queue resetting loadingDb back to false via a QTimer singleShot call, because
    // otherwise, RegistrationPage::setColumnVisible will be triggered for the Draw column when
    // it's already set to false and the question whether to also show the draw overview page
    // will pop up if it's shown.
    QTimer::singleShot(0, this, [this]
    {
        m_tournamentSettings->setLoadingDb(false);
    });
}

void MainWindow::setExportEnabled(bool state)
{
    m_exportRankingAction->setEnabled(state);
    m_exportTournamentAction->setEnabled(state);
    m_setTournamentOpenAction->setEnabled(state);
    m_setTournamentFinishedAction->setEnabled(state);
}

void MainWindow::showAdjustDrawDialog()
{
    auto *dialog = new AdjustDrawDialog(this,
                                        m_sharedObjects->temporaryFileHelper(),
                                        m_registrationPage->playersData(),
                                        m_tournamentSettings->isPairMode(),
                                        m_settings->namesSeparator());
    connect(m_registrationPage, &RegistrationPage::listWasModified,
            dialog, &AdjustDrawDialog::listWasModified);
    connect(dialog, &AdjustDrawDialog::adoptAdjustment,
            m_registrationPage, &RegistrationPage::adoptDrawAdjustment);
    dialog->exec();
}

void MainWindow::showCompareScoreDialog(int remoteId, const int *currentRound,
                                        const Scores::Score *localScore)
{
    auto *existingDialog = findChild<CompareScoreDialog *>();
    if (existingDialog != nullptr) {
        existingDialog->setWindowState((existingDialog->windowState()
                                        & ~Qt::WindowMinimized) | Qt::WindowActive);

        const auto existingRemoteId = existingDialog->remoteId();
        if (existingRemoteId != remoteId) {
            QMessageBox::warning(existingDialog,
                tr("Ergebnisse vergleichen"),
                tr("Es werden momentan Ergebnisse mit Client #%1 verglichen. Bitte vor dem "
                   "Vergleichen der Ergebnisse mit Client #%2 den Dialog schließen.").arg(
                   QString::number(existingRemoteId), QString::number(remoteId)));
        }

        return;
    }

    CompareScoreDialog *dialog = nullptr;

    if (remoteId == 0) {
        // We're a client and talk to the server

        auto *clientPage = qobject_cast<ClientPage *>(m_clientDock->widget());
        auto *client = clientPage->client();
        dialog = new CompareScoreDialog(this, m_sharedObjects, client, remoteId, currentRound,
                                        localScore);

        connect(dialog, &CompareScoreDialog::scoreRequested, client, &Client::requestScore);
        connect(client, &Client::compareScore, dialog, &CompareScoreDialog::compareScore);
        connect(clientPage, &ClientPage::connectionClosed,
                dialog, std::bind(&CompareScoreDialog::closeDialog, dialog,
                                  tr("Die Netzwerkverbindung wurde beendet.\n"
                                     "Der Dialog wird jetzt geschlossen.")));
        connect(client, &Client::remoteScoreChecksumChanged,
                dialog, std::bind(&CompareScoreDialog::remoteScoreChecksumChanged, dialog, 0,
                                  std::placeholders::_1));

    } else {
        // We're the server and talk to a client

        auto *server = qobject_cast<ServerPage *>(m_serverDock->widget())->server();
        dialog = new CompareScoreDialog(this, m_sharedObjects, server, remoteId, currentRound,
                                        localScore);

        connect(dialog, &CompareScoreDialog::scoreRequested,
                server, std::bind(&Server::requestScore, server, remoteId, std::placeholders::_1));
        connect(server, &Server::compareScore, dialog, &CompareScoreDialog::compareScore);
        connect(server, &Server::clientDisconnected,
                dialog, &CompareScoreDialog::clientDisconnected);
        connect(server, &Server::remoteScoreChecksumChanged,
                dialog, &CompareScoreDialog::remoteScoreChecksumChanged);
    }

    connect(m_scorePage, &ScorePage::scoreChanged, dialog, &CompareScoreDialog::dataChanged);
    connect(m_scorePage, &ScorePage::roundSelectionChanged,
            dialog, &CompareScoreDialog::dataChanged);

    dialog->show();
}

void MainWindow::showAllDismissedMessages()
{
    m_settings->showAllDismissedMessages();
    QMessageBox::information(this,
        tr("Ausgeblendete Infos"),
        tr("Alle (nicht sitzungsspezifischen) Hinweise, bei denen „Nicht mehr anzeigen“ ausgewählt "
           "wurde, werden jetzt wieder angezeigt."));
}
