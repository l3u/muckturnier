// SPDX-FileCopyrightText: 2010-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

// Local includes

#include "debugMode.h"

#include "shared/OptionalPages.h"

#include "Database/Database.h"

// Qt includes
#include <QMainWindow>

// Local classes

class SharedObjects;
class ResourceFinder;
class RegistrationPage;
class ScorePage;
class RankingPage;
class Settings;
class BackupEngine;
class TournamentSettings;
class StringTools;
class DismissableMessage;
class NetworkStatusWidget;
class StopWatchEngine;
class OptionalPage;

namespace Scores
{
struct Score;
}

// Qt classes
class QAction;
class QDockWidget;
class QWidget;
class QShortcut;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(SharedObjects *sharedObjects);

public Q_SLOTS:
    void updateStatus(const QString &message);
    void tournamentStarted();
    void openTournament(QString dbFileName);
    void closeTournament();
    void reopenLastOpenedDb(const QString &lastOpenedDb);

Q_SIGNALS:
    void resetAllSearches();

protected:
    void closeEvent(QCloseEvent *event) override;

private: // Functions
    void formatDockWidget(QDockWidget *dock, const QString &id);
    void restoreOrTabifyDockWidget(QDockWidget *dock);
    void attachDocks(Qt::DockWidgetArea area);
    void updateDatabase(const QString &lastOpenedDb, const QString &dbFileName,
                        Database::Error openError);
    void processDbReset();
    bool askRestoreOriginalDb(BackupEngine &backupEngine);
    void showRestoreBackupDialog(BackupEngine &backupEngine, const QString &dbToOverwrite,
                                 bool autoAccept);
    void askFinishTournament();
    void setTournamentOpen(bool state);

    QDockWidget *findOptionalPageDock(OptionalPages::Page page) const;
    QDockWidget *findOptionalPageDock(const QString &id) const;

private Q_SLOTS:
    void playersCountChanged(int count);
    void registrationsAvailable(bool state);
    void databaseChanged();
    void startNewTournament();
    void showHandbook();
    void showAboutDialog();
    void showExportRankingDialog();
    void showExportTournamentDialog();
    void exportPlayersList();
    void hideWindowFrame();
    void hideMenuBar();
    void arrangeTabified();
    void arrangeHorizontally();
    void arrangeVertically();
    void arrangeOwnWindow();
    void resetDatabase();
    void importPlayersList();
    void sqlError(const QString &text);
    void triggerShowRestoreBackupDialog();
    bool createDatabaseBackup();
#ifdef DEBUG_MODE
    void showChecksumsDialog();
#endif
    void showSettingsDialog();
    void showServerPage();
    void showClientPage();
    void setNetworkActionsEnabled(bool state);
    void clientConnected();
    void clientConnectionClosed();
    void serverStopped();
    void serverStarted();
    void serverConnectionRequested(const QString &ip, int port);
    void serverPageClosed();
    void clientPageClosed();
    void networkPageClosed();
    void setNameAdoptionMode(bool state);
    void openRestoredDb(const QString &dbFile);
    void networkTournamentStarted();
    void showDrawOverviewPage(int round);
    void resetMainWindowState();
    void setDockTabPosition(QTabWidget::TabPosition position, bool checked = true);
    void askSetTournamentOpen(bool state);
    void setExportEnabled(bool state);
    void showAdjustDrawDialog();
    void showCompareScoreDialog(int remoteId, const int *currentRound,
                                const Scores::Score *localScore);
    void showAllDismissedMessages();
    void showSchedulePage();
    void showBookingPage();
    void showWlanCodeScannerPage();

private:
    // Shared objects
    SharedObjects *m_sharedObjects;
    ResourceFinder *m_resourceFinder;
    Database *m_db;
    Settings *m_settings;
    TournamentSettings *m_tournamentSettings;
    StringTools *m_stringTools;
    DismissableMessage *m_dismissableMessage;

    // GUI obects
    QWidget *m_splashScreen;
    QDockWidget *m_playersDock;
    RegistrationPage *m_registrationPage;
    QDockWidget *m_scoreDock;
    ScorePage *m_scorePage;
    QDockWidget *m_rankingDock;
    RankingPage *m_rankingPage;
    QDockWidget *m_serverDock = nullptr;
    QDockWidget *m_clientDock = nullptr;
    QDockWidget *m_drawOverviewDock = nullptr;

    // Menus
    QMenu *m_tournamentStateMenu;
    QMenu *m_imExportMenu;
    QMenu *m_tabPositionMenu;
    QMenu *m_pagesMenu;

    // Menu actions
    QAction *m_newTournamentAction;
    QAction *m_openTournamentAction;
    QAction *m_closeTournamentAction;
    QAction *m_resetDatabaseAction;
    QAction *m_importPlayersListAction;
    QAction *m_exportPlayersListAction;
    QAction *m_exportRankingAction;
    QAction *m_exportTournamentAction;
    QAction *m_hideWindowFrameAction;
    QAction *m_hideMenubarAction;
    QMenu *m_databaseExtrasMenu;
    QAction *m_createDatabaseBackupAction;
    QAction *m_showRestoreBackupDialogAction;
#ifdef DEBUG_MODE
    QAction *m_showChecksumsDialogAction;
#endif
    QAction *m_showServerPageAction;
    QAction *m_finishRegistrationAction;
    QAction *m_showClientPageAction;
    QAction *m_showDrawOverviewPageAction;
    QAction *m_showAdjustDrawDialogAction;
    QAction *m_resetMainWindowStateAction;
    QAction *m_setTournamentOpenAction;
    QAction *m_setTournamentFinishedAction;
    QAction *m_showRegistrationHeader;
    QAction *m_showSchedulePage;
    QAction *m_showBookingPage;
    QAction *m_showWlanCodeScannerPage;

    // Global shortcuts
    QShortcut *m_viewMenubarShortcut;

    // Cache variables
    QString m_lastServerIp;
    int m_lastServerPort;
    bool m_askRestoreOriginalDb = true;
    bool m_restoreOriginalDb = false;
    bool m_openingAfterRestore = false;
    bool m_isServer = false;
    bool m_isClient = false;

    NetworkStatusWidget *m_networkStatusWidget = nullptr;
    StopWatchEngine *m_stopWatchEngine;

};

#endif // MAINWINDOW_H
