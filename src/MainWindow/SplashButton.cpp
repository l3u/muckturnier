// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "Application.h"

#include "SplashButton.h"

#include "SharedObjects/ResourceFinder.h"

SplashButton::SplashButton(const QString &text, const QString &icon, ResourceFinder *resourceFinder,
                           QWidget *parent)
    : QToolButton(parent),
      m_resourceFinder(resourceFinder),
      m_icon(icon)
{
    setAutoRaise(true);
    setStyleSheet(QStringLiteral("border: none;"));
    setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    setIconSize(QSize(100, 100));
    setText(text);

    initializeIcon();
    connect(qobject_cast<Application *>(qApp), &Application::paletteChanged,
            this, &SplashButton::initializeIcon);
}

void SplashButton::initializeIcon()
{
    setIcon(QIcon(m_resourceFinder->find(m_icon)));
}
