// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef RESTOREBACKUPDIALOG_H
#define RESTOREBACKUPDIALOG_H

// Local includes
#include "shared/TitleDialog.h"

// Qt includes
#include <QDateTime>

// Local classes
class Database;
class BackupEngine;

// Qt classes
class QListWidget;

class RestoreBackupDialog : public TitleDialog
{
    Q_OBJECT

public:
    explicit RestoreBackupDialog(QWidget *parent, Database *db, BackupEngine &backupEngine,
                                 const QString &dbToOverwrite, bool autoAccept);

Q_SIGNALS:
    void statusUpdate(const QString &text);
    void closeTournament();
    void openTournament(const QString &dbFile);

protected Q_SLOTS:
    void accept() override;

private Q_SLOTS:
    void runAutoAccept();

private: // Functions
    QString formatDateString(const QString &backupDate) const;

private: // Variables
    Database *m_db;
    QListWidget *m_backups;
    QString m_dbToOverwrite;
    bool m_autoAccept;

};

#endif // RESTOREBACKUPDIALOG_H
