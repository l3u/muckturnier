// SPDX-FileCopyrightText: 2010-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "Application.h"

#include "MainWindow/MainWindow.h"

#include "shared/Logging.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/ResourceFinder.h"

// Qt includes

#include <QDebug>
#include <QTranslator>
#include <QLibraryInfo>
#include <QFileInfo>
#include <QStyle>
#include <QPalette>

#ifndef Q_OS_MACOS
#include <QStringList>
#endif

int main(int argc, char *argv[])
{
    // Create the core application
    Application application(argc, argv);

    // Set application-wide styles

    // Shared style
    QString style = QStringLiteral(
        "QGroupBox[checkable=false] { "
            "font-weight: bold; "
        "} "
    );

    const auto styleName = application.style()->objectName();

    // Fix the HLine and VLine QFrames appearance for Qt's standard styles
    // (Don't display those as a 1px black line, but use some gray color)
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    if (styleName == QLatin1String("fusion")
        || styleName.startsWith(QLatin1String("windows"))
        || styleName == QLatin1String("macintosh")) {

        // With Qt 5, we can use the "frameShape" selector, which currently won't work with Qt 6
        style.append(QStringLiteral(
            "QFrame[frameShape=\"4\"],"
            "QFrame[frameShape=\"5\"] { "
                "color: palette(Mid); "
            "} "
        ));
    }
#else
    if (styleName == QLatin1String("fusion")
        || styleName.startsWith(QLatin1String("windows"))
        || styleName == QLatin1String("macintosh")) {

        // This is a workaround for Bug #101877 (https://bugreports.qt.io/browse/QTBUG-101877)
        // which hopefully can be removed once the bug is actually fixed in Qt 6, along with all
        // setProperty("isseparator", true) calls on QFrames.
        style.append(QStringLiteral(
            "QFrame[isseparator=true] {"
                "border: 1px solid palette(Window); "
                "background-color: palette(Mid); "
            "} "
        ));
    }
#endif

    application.setStyleSheet(style);

    // Create the application-wide shared objects
    SharedObjects sharedObjects;
    auto *resourceFinder = sharedObjects.resourceFinder();

    // Install the translations

    // Nobody outside of Germany will use this, and nobody will ever translate
    // it to any other language. So let's just load the de locale stuff :-P

    // Add the Qt localizations

    QTranslator qtTranslator;
    bool qtBaseLoaded = false;

    // First, we check if we have a packaged Qt translation file
    const auto qtQm = resourceFinder->find(QStringLiteral("qtbase_de.qm"));
    if (! qtQm.isEmpty()) {
        if (! qtTranslator.load(QStringLiteral("qtbase_de"), QFileInfo(qtQm).absolutePath())) {
            qCDebug(MuckturnierLog) << "Failed to initialize the packaged Qt translation!";
        } else {
            qCDebug(MuckturnierLog) << "Initialized the packaged Qt translation";
            qtBaseLoaded = true;
        }
    } else {
        qCDebug(MuckturnierLog) << "Could not find a packaged Qt translation. "
                                   "Trying to find a system-provided one";
    }

    // If we don't have a packaged Qt translation or could not load it, we try to load a system one
    if (! qtBaseLoaded) {
        if (! qtTranslator.load(QStringLiteral("qtbase_de"),
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
                                QLibraryInfo::location(QLibraryInfo::TranslationsPath)
#else
                                QLibraryInfo::path(QLibraryInfo::TranslationsPath)
#endif
        )) {
            qCDebug(MuckturnierLog) << "Failed to initialize initialize a system-provided Qt "
                                       "translation!";
        } else {
            qCDebug(MuckturnierLog) << "Initialized the system-provided Qt translation";
        }
    }

    // Add the translator if we could initialize it
    if (! qtTranslator.isEmpty()) {
        application.installTranslator(&qtTranslator);
    }

    // Add the Muckturnier localizations (plural forms)

    QTranslator muckturnierTranslator;
    const auto muckturnierQm = resourceFinder->find(QStringLiteral("muckturnier_de.qm"));

    if (! muckturnierQm.isEmpty()) {
        if (muckturnierTranslator.load(QStringLiteral("muckturnier_de"),
                                       QFileInfo(muckturnierQm).absolutePath())) {

            qCDebug(MuckturnierLog) << "Initialized the Muckturnier translation";

        } else {
            qCDebug(MuckturnierLog) << "Failed to initialize the Muckturnier translation! "
                                    << "Plurals will not be displayed correctly!";
        }

    } else {
        qCDebug(MuckturnierLog) << "Could not find the Muckturnier translation! "
                                   "Plurals will not be displayed correctly!";
    }

    // Add the translator if we could initialize it
    if (! muckturnierTranslator.isEmpty()) {
        application.installTranslator(&muckturnierTranslator);
    }

    // Create the main window
    MainWindow mainWindow(&sharedObjects);
    mainWindow.show();

    // Handle "Open with ..." requests
#ifndef Q_OS_MACOS
    // On Linux and Windows, "Open with" simply passes the file
    // to be opened as a command line argument that we handle here:
    const QStringList arguments(application.arguments());
    if (arguments.count() > 1) {
        mainWindow.setEnabled(false);
        QApplication::processEvents();
        mainWindow.openTournament(arguments.at(1));
        mainWindow.setEnabled(true);
    }
#else
    // On macOS, we listen for a QFileOpenEvent in the custom QApplication, catch the file
    // name to be opened and pass it to the main window via the fileOpenRequested signal:
    QObject::connect(&application, &Application::fileOpenRequested,
                     &mainWindow, &MainWindow::openTournament);
#endif

    return application.exec();
}
