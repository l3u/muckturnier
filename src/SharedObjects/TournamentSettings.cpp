// SPDX-FileCopyrightText: 2019-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "TournamentSettings.h"

#include "shared/JsonHelper.h"
#include "shared/TournamentTemplate.h"
#include "shared/Markers.h"
#include "shared/Json.h"
#include "shared/Logging.h"

// Qt includes
#include <QDebug>
#include <QJsonObject>
#include <QJsonArray>

// C++ includes
#include <utility>

namespace tt = TournamentTemplate;

// The revision we use
static constexpr int s_tournamentSettingsVersion = 4;

// JSON Keys for all settings

static const QLatin1String s_muckturnierVersion("muckturnier version");
static const QLatin1String s_tournamentMode    ("tournament mode");
static const QLatin1String s_boogerScore       ("booger score");
static const QLatin1String s_boogersPerRound   ("boogers per round");

static const QLatin1String s_tournamentOpen("tournament open");

static const QLatin1String s_autoSelectPairs       ("autoselect pairs");
static const QLatin1String s_selectByLastRound     ("select by last round");
static const QLatin1String s_autoSelectTableForDraw("auto select table for draw");
static const QLatin1String s_scoreType             ("score type");

static const QLatin1String s_includeOpponentGoals   ("include opponent goals");
static const QLatin1String s_grayOutUnimportantGoals("gray out unimportant goals");

static const QLatin1String s_defaultMarker           ("default marker");
static const QLatin1String s_drawnMarker             ("drawn marker");
static const QLatin1String s_bookedMarker            ("booked marker");
static const QLatin1String s_singlesManagementVersion("singles management version");
static const QLatin1String s_singlesManagement       ("singles management");

static const QLatin1String s_registrationColumns         ("registration columns");
static const QLatin1String s_registrationListHeaderHidden("registration list header hidden");

static const QLatin1String s_drawSettingsVersion("draw settings version");
static const QLatin1String s_drawSettings       ("draw settings");

static const QLatin1String s_scheduleSettingsVersion("schedule settings version");
static const QLatin1String s_scheduleSettings       ("schedule settings");

static const QLatin1String s_bookingSettingsVersion("booking settings version");
static const QLatin1String s_bookingSettings("booking settings");

static const QLatin1String s_optionalPages("optional pages");

TournamentSettings::TournamentSettings(QObject *parent) : QObject(parent)
{
    // Initialize all variables to default values
    reset();
}

int TournamentSettings::version() const
{
    return s_tournamentSettingsVersion;
}

bool TournamentSettings::loadingDb() const
{
    return m_loadingDb;
}

void TournamentSettings::setLoadingDb(bool state)
{
    m_loadingDb = state;
}

const QVector<OptionalPages::Page> &TournamentSettings::optionalPages() const
{
    return m_optionalPages;
}

void TournamentSettings::setOptionalPageShown(OptionalPages::Page page, bool state)
{
    if (m_loadingDb) {
        return;
    }

    if (state) {
        m_optionalPages.append(page);
    } else {
        m_optionalPages.removeOne(page);
    }
}

void TournamentSettings::reset()
{
    // Initialize the default values from the tournament template
    m_tournamentMode               = tt::defaultSettings.tournamentMode;
    m_scoreType                    = tt::defaultSettings.scoreType;
    m_boogerScore                  = tt::defaultSettings.boogerScore;
    m_boogersPerRound              = tt::defaultSettings.boogersPerRound;
    m_drawSettings                 = tt::defaultSettings.drawSettings;
    m_scheduleSettings             = tt::defaultSettings.scheduleSettings;
    m_registrationColumns          = tt::defaultSettings.registrationColumns;
    m_registrationListHeaderHidden = tt::defaultSettings.registrationListHeaderHidden;
    m_autoSelectPairs              = tt::defaultSettings.autoSelectPairs;
    m_selectByLastRound            = tt::defaultSettings.selectByLastRound;
    m_autoSelectTableForDraw       = tt::defaultSettings.autoSelectTableForDraw;
    m_includeOpponentGoals         = tt::defaultSettings.includeOpponentGoals;
    m_grayOutUnimportantGoals      = tt::defaultSettings.grayOutUnimportantGoals;
    m_optionalPages                = tt::defaultSettings.optionalPages;

    // Fallback values for database-read settings
    m_muckturnierVersion     = tr("unbekannt");
    m_tournamentOpen         = true;
    m_originalTournamentOpen = true;
    m_marker                 = Markers::defaultSpecialMarkerValues;
    m_singlesManagement      = Markers::defaultSinglesManagement;
    m_bookingSettings        = Booking::Settings();
}

QVariant TournamentSettings::getValue(const QJsonObject &json, const QLatin1String &key,
                                      QJsonValue::Type type) const
{
    const auto value = json.value(key);
    if (value.isUndefined() || value.type() != type) {
        return QVariant();
    }
    return value.toVariant();
}

bool TournamentSettings::load(const QString &data)
{
    // Set default fall-back values
    reset();

    // Parse the JSON data
    const auto json = Json::objectFromString(data);
    QVariant value;

    // There are some essential values that have to be present and valid. We parse them here:

    // tournamentMode

    value = getValue(json, s_tournamentMode, QJsonValue::String);
    if (! value.isValid()) {
        return false;
    }

    const auto tournamentMode = value.toString();
    bool tournamentModeValid = false;
    QHash<Tournament::Mode, QString>::const_iterator it;
    for (it = Tournament::modeMap.constBegin(); it != Tournament::modeMap.constEnd(); it++) {
        if (it.value() == tournamentMode) {
            m_tournamentMode = it.key();
            tournamentModeValid = true;
            break;
        }
    }
    if (! tournamentModeValid) {
        return false;
    }

    // boogerScore
    value = getValue(json, s_boogerScore, QJsonValue::Double);
    if (! value.isValid()) {
        return false;
    }
    m_boogerScore = value.toInt();

    // boogersPerRound
    value = getValue(json, s_boogersPerRound, QJsonValue::Double);
    if (! value.isValid()) {
        return false;
    }
    m_boogersPerRound = value.toInt();

    // For all other values, we can fall back to the default setting set by reset()
    // so we only set them if they are present or actually parsed

    // scoreType

    value = getValue(json, s_scoreType, QJsonValue::String);
    const auto scoreType = value.toString();

    QHash<Tournament::ScoreType, QString>::const_iterator it2;
    for (it2 = Tournament::scoreTypeMap.constBegin(); it2 != Tournament::scoreTypeMap.constEnd();
         it2++) {

        if (it2.value() == scoreType) {
            m_scoreType = it2.key();
            break;
        }
    }

    // drawSettings
    m_drawSettings = JsonHelper::parseDrawSettings(json.value(s_drawSettingsVersion).toInt(),
                                                   json.value(s_drawSettings).toObject());

    // scheduleSettings
    const auto scheduleSettingsVersion = json.value(s_scheduleSettingsVersion).toInt();
    if (scheduleSettingsVersion == Schedule::settingsVersion) {
        m_scheduleSettings = JsonHelper::parseScheduleSettings(
            json.value(s_scheduleSettings).toObject());
    } else {
        qCWarning(MuckturnierLog) << "Schedule settings with version " <<  scheduleSettingsVersion
                                  << "are not parseable, falling back to defaults";
    }

    // registrationColumns
    m_registrationColumns = JsonHelper::parseRegistrationColumns(
                                json.value(s_registrationColumns).toArray());

    // registrationListHeaderHidden
    value = getValue(json, s_registrationListHeaderHidden, QJsonValue::Bool);
    if (value.isValid()) {
        m_registrationListHeaderHidden = value.toBool();
    }

    // autoSelectPairs
    value = getValue(json, s_autoSelectPairs, QJsonValue::Bool);
    if (value.isValid()) {
        m_autoSelectPairs = value.toBool();
    }

    // selectByLastRound
    if (! m_autoSelectPairs) {
        m_selectByLastRound = false;
    } else {
        value = getValue(json, s_selectByLastRound, QJsonValue::Bool);
        if (value.isValid()) {
            m_selectByLastRound = value.toBool();
        }
    }

    Q_EMIT drawModeChanged();

    // autoSelectTableForDraw
    value = getValue(json, s_autoSelectTableForDraw, QJsonValue::Bool);
    if (value.isValid()) {
        m_autoSelectTableForDraw = value.toBool();
    }

    // includeOpponentGoals
    value = getValue(json, s_includeOpponentGoals, QJsonValue::Bool);
    if (value.isValid()) {
        m_includeOpponentGoals = value.toBool();
    }

    // grayOutUnimportantGoals
    value = getValue(json, s_grayOutUnimportantGoals, QJsonValue::Bool);
    if (value.isValid()) {
        m_grayOutUnimportantGoals = value.toBool();
    }

    // optionalPages
    m_optionalPages = JsonHelper::parseOptionalPages(json.value(s_optionalPages).toArray());

    // muckturnierVersion
    value = getValue(json, s_muckturnierVersion, QJsonValue::String);
    if (value.isValid()) {
        m_muckturnierVersion = value.toString();
    }

    // tournamentOpen
    value = getValue(json, s_tournamentOpen, QJsonValue::Bool);
    if (value.isValid()) {
        m_tournamentOpen = value.toBool();
        m_originalTournamentOpen = m_tournamentOpen;
    }

    // Special markers

    value = getValue(json, s_defaultMarker, QJsonValue::Double);
    if (value.isValid()) {
        m_marker[Markers::Default] = value.toInt();
    }

    value = getValue(json, s_drawnMarker, QJsonValue::Double);
    if (value.isValid()) {
        m_marker[Markers::Drawn] = value.toInt();
    }

    value = getValue(json, s_bookedMarker, QJsonValue::Double);
    if (value.isValid()) {
        m_marker[Markers::Booked] = value.toInt();
    }

    // singlesManagement
    m_singlesManagement = JsonHelper::parseSinglesManagement(
        json.value(s_singlesManagement).toObject());

    // Booking settings
    m_bookingSettings = JsonHelper::parseBookingSettings(json.value(s_bookingSettings).toObject());

    return true;
}

QString TournamentSettings::toString() const
{
    QJsonObject settings {
        { s_muckturnierVersion,           m_muckturnierVersion },
        { s_tournamentMode,               Tournament::modeMap.value(m_tournamentMode) },
        { s_tournamentOpen,               m_tournamentOpen },
        { s_scoreType,                    Tournament::scoreTypeMap.value(m_scoreType) },
        { s_boogerScore,                  m_boogerScore },
        { s_boogersPerRound,              m_boogersPerRound },
        { s_autoSelectPairs,              m_autoSelectPairs },
        { s_selectByLastRound,            m_selectByLastRound },
        { s_autoSelectTableForDraw,       m_autoSelectTableForDraw },
        { s_includeOpponentGoals,         m_includeOpponentGoals },
        { s_grayOutUnimportantGoals,      m_grayOutUnimportantGoals },
        { s_defaultMarker,                m_marker.value(Markers::Default) },
        { s_drawnMarker,                  m_marker.value(Markers::Drawn) },
        { s_bookedMarker,                 m_marker.value(Markers::Booked) },
        { s_registrationListHeaderHidden, m_registrationListHeaderHidden },

        // Registration columns
        { s_registrationColumns, JsonHelper::registrationColumnsToJson(m_registrationColumns) },

        // Optional pages
        { s_optionalPages, JsonHelper::optionalPagesToJson(m_optionalPages) },

        // Singles management
        { s_singlesManagementVersion, Markers::singlesManagementVersion },
        { s_singlesManagement,        JsonHelper::singlesManagementToJson(m_singlesManagement) },

        // Draw settings
        { s_drawSettingsVersion, Draw::settingsVersion },
        { s_drawSettings,        JsonHelper::drawSettingsToJson(m_drawSettings) },

        // Schedule settings
        { s_scheduleSettingsVersion, Schedule::settingsVersion },
        { s_scheduleSettings,        JsonHelper::scheduleSettingsToJson(m_scheduleSettings) },

        // Booking settings
        { s_bookingSettingsVersion, Booking::settingsVersion },
        { s_bookingSettings,        JsonHelper::bookingSettingsToJson(m_bookingSettings) }
    };

    return Json::serialize(settings);
}

void TournamentSettings::setMuckturnierVersion(const QString &text)
{
    m_muckturnierVersion = text;
}

const QString &TournamentSettings::muckturnierVersion() const
{
    return m_muckturnierVersion;
}

void TournamentSettings::setTournamentMode(Tournament::Mode tournamentMode)
{
    m_tournamentMode = tournamentMode;
}

Tournament::Mode TournamentSettings::tournamentMode() const
{
    return m_tournamentMode;
}

bool TournamentSettings::isPairMode() const
{
    return m_tournamentMode == Tournament::FixedPairs;
}

bool TournamentSettings::isSinglePlayerMode() const
{
    return m_tournamentMode == Tournament::SinglePlayers;
}

void TournamentSettings::initializeTournamentOpen()
{
    m_tournamentOpen = true;
    m_originalTournamentOpen = true;
}

void TournamentSettings::setTournamentOpen(bool state)
{
    m_tournamentOpen = state;
}

bool TournamentSettings::tournamentOpen() const
{
    return m_tournamentOpen;
}

bool TournamentSettings::tournamentOpenChanged() const
{
    return m_tournamentOpen != m_originalTournamentOpen;
}

void TournamentSettings::setBoogerScore(int score)
{
    m_boogerScore = score;
}

const int &TournamentSettings::boogerScore() const
{
    return m_boogerScore;
}

void TournamentSettings::setBoogersPerRound(int count)
{
    m_boogersPerRound = count;
}

int TournamentSettings::boogersPerRound() const
{
    return m_boogersPerRound;
}

void TournamentSettings::setDrawModeParameters(bool autoSelectPairs, bool selectByLastRound)
{
    Q_ASSERT_X(autoSelectPairs || (! autoSelectPairs && ! selectByLastRound),
               "TournamentSettings::setDrawModeParameters",
               "selectByLastRound can't be true if autoSelectPairs is false");
    m_autoSelectPairs = autoSelectPairs;
    m_selectByLastRound = selectByLastRound;
    Q_EMIT drawModeChanged();
}

void TournamentSettings::setAutoSelectPairs(bool state)
{
    m_autoSelectPairs = state;
    Q_EMIT drawModeChanged();
}

bool TournamentSettings::autoSelectPairs() const
{
    return m_autoSelectPairs;
}

void TournamentSettings::setSelectByLastRound(bool state)
{
    m_selectByLastRound = state;
    Q_EMIT drawModeChanged();
}

bool TournamentSettings::selectByLastRound() const
{
    return m_selectByLastRound;
}

void TournamentSettings::setAutoSelectTableForDraw(bool state)
{
    m_autoSelectTableForDraw = state;
}

bool TournamentSettings::autoSelectTableForDraw() const
{
    return m_autoSelectTableForDraw;
}

void TournamentSettings::setIncludeOpponentGoals(bool state)
{
    m_includeOpponentGoals = state;
}

bool TournamentSettings::includeOpponentGoals() const
{
    return m_includeOpponentGoals;
}

void TournamentSettings::setGrayOutUnimportantGoals(bool state)
{
    m_grayOutUnimportantGoals = state;
}

bool TournamentSettings::grayOutUnimportantGoals() const
{
    return m_grayOutUnimportantGoals;
}

void TournamentSettings::setSpecialMarker(Markers::Type type, int id)
{
    m_marker[type] = id;
}

void TournamentSettings::setSpecialMarkers(const QHash<Markers::Type, int> &markers)
{
    QHash<Markers::Type, int>::const_iterator it;
    for (it = markers.constBegin(); it != markers.constEnd(); it++) {
        m_marker[it.key()] = it.value();
    }
}

int TournamentSettings::specialMarker(Markers::Type type) const
{
    return m_marker.value(type);
}

QHash<Markers::Type, int> TournamentSettings::specialMarkers() const
{
    QHash<Markers::Type, int> markers;
    for (const auto type : Markers::specialMarkers) {
        markers[type] = m_marker.value(type);
    }
    return markers;
}

void TournamentSettings::setSinglesManagement(const Markers::SinglesManagement &data)
{
    m_singlesManagement = data;
}

const Markers::SinglesManagement &TournamentSettings::singlesManagement() const
{
    return m_singlesManagement;
}

void TournamentSettings::setRegistrationColumns(const QVector<RegistrationColumns::Column> &data)
{
    m_registrationColumns = data;
}

const QVector<RegistrationColumns::Column> &TournamentSettings::registrationColumns() const
{
    return m_registrationColumns;
}

void TournamentSettings::setRegistrationListHeaderHidden(bool state)
{
    m_registrationListHeaderHidden = state;
}

bool TournamentSettings::registrationListHeaderHidden() const
{
    return m_registrationListHeaderHidden;
}

void TournamentSettings::setScoreType(Tournament::ScoreType type)
{
    m_scoreType = type;
}

Tournament::ScoreType TournamentSettings::scoreType() const
{
    return m_scoreType;
}

void TournamentSettings::setDrawSettings(const Draw::Settings &settings)
{
    m_drawSettings = settings;
    Q_EMIT drawSettingsChanged();
}

const Draw::Settings &TournamentSettings::drawSettings() const
{
    return m_drawSettings;
}

void TournamentSettings::setScheduleSettings(const Schedule::Settings &settings)
{
    m_scheduleSettings = settings;
}

const Schedule::Settings &TournamentSettings::scheduleSettings() const
{
    return m_scheduleSettings;
}

void TournamentSettings::setBookingSettings(const Booking::Settings &settings)
{
    m_bookingSettings = settings;
}

void TournamentSettings::resetBookingSettings()
{
    m_bookingSettings = Booking::Settings();
}

const Booking::Settings &TournamentSettings::bookingSettings() const
{
    return m_bookingSettings;
}

void TournamentSettings::setBookingEnabled(bool state)
{
    m_bookingEnabled = state;
    Q_EMIT bookingEnabledChanged(state);
}

bool TournamentSettings::bookingEnabled() const
{
    return m_bookingEnabled;
}
