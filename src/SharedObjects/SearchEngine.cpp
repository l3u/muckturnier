// SPDX-FileCopyrightText: 2018-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "SearchEngine.h"
#include "StringTools.h"

// Qt includes
#include <QRegularExpression>
#include <QDebug>

static const QVector<QString> s_replacedUmlauts {
    QStringLiteral("ae"),
    QStringLiteral("oe"),
    QStringLiteral("ue"),
};

static const QVector<QString> s_replacedUmlautsMapping {
    QStringLiteral("a"),
    QStringLiteral("o"),
    QStringLiteral("u"),
};

static const QChar s_space(QLatin1Char(' '));

static const QRegularExpression s_sequentialCharacters(QStringLiteral("(.)\\1+"));

namespace Koeln
{

static const QChar zero (QLatin1Char('0'));
static const QChar one  (QLatin1Char('1'));
static const QChar two  (QLatin1Char('2'));
static const QChar three(QLatin1Char('3'));
static const QChar four (QLatin1Char('4'));
static const QChar five (QLatin1Char('5'));
static const QChar six  (QLatin1Char('6'));
static const QChar seven(QLatin1Char('7'));
static const QChar eight(QLatin1Char('8'));

static const QLatin1String fourEight("48");

static const QChar a(QLatin1Char('a'));
static const QChar b(QLatin1Char('b'));
static const QChar c(QLatin1Char('c'));
static const QChar d(QLatin1Char('d'));
static const QChar e(QLatin1Char('e'));
static const QChar f(QLatin1Char('f'));
static const QChar g(QLatin1Char('g'));
static const QChar h(QLatin1Char('h'));
static const QChar i(QLatin1Char('i'));
static const QChar j(QLatin1Char('j'));
static const QChar k(QLatin1Char('k'));
static const QChar l(QLatin1Char('l'));
static const QChar m(QLatin1Char('m'));
static const QChar n(QLatin1Char('n'));
static const QChar o(QLatin1Char('o'));
static const QChar p(QLatin1Char('p'));
static const QChar q(QLatin1Char('q'));
static const QChar r(QLatin1Char('r'));
static const QChar s(QLatin1Char('s'));
static const QChar t(QLatin1Char('t'));
static const QChar u(QLatin1Char('u'));
static const QChar v(QLatin1Char('v'));
static const QChar w(QLatin1Char('w'));
static const QChar x(QLatin1Char('x'));
static const QChar y(QLatin1Char('y'));
static const QChar z(QLatin1Char('z'));

}

SearchEngine::SearchEngine(QObject *parent, StringTools *stringTools)
    : QObject(parent),
      m_stringTools(stringTools),
      m_variantsCache {
          { SearchType::DefaultSearch,         &m_defaultVariants },
          { SearchType::DrawnPairsScoreSearch, &m_drawnPairsScoreVariants },
          { SearchType::DrawnPairsDrawSearch,  &m_drawnPairsDrawVariants }
      }
{
}

QVector<QString> SearchEngine::searchVariants(const QString &text)
{
    QString workText;
    QVector<QString> variants;

    // We only work on a lower-case, simplified version of the string with "ß" replaced by "s" in
    // each case (the replacement with "ss" would be removed by compressing the string anyway).
    // Additionally, we replace typical pair delimiters with spaces, so that "John Doe/Jane Smith"
    // won't be handled as "John", "Doe/Jane" and "Smith", but as "John", Doe", "Jane" and "Smith".

    // Strip multiple spaces, tabs etc.
    workText = text.simplified();

    // Generate a lower-case version
    workText = m_stringTools->toLower(workText);

    // Replace "ß" with "s"
    workText = workText.replace(QStringLiteral("ß"), QStringLiteral("s"));

    // Replace common delimiters with spaces
    for (const auto &delimiter : { QLatin1Char('/'), QLatin1Char('+'), QLatin1Char('&') }) {
        workText.replace(delimiter, QLatin1Char(' '));
    }

    // Index 0 has to be the phonetic representation.
    // We calculate the Kölner Phonetik representation of the name and add it. We use the
    // simplified version with "ß" replaced for this but additionally strip diacritical marks.
    // We need the un-compressed version for the algorithm to work properly.
    variants.append(koelnerPhonetik(m_stringTools->stripDiacritics(workText)));

    // From here on, we only work on the compressed version
    // (with sequential characters replaced by one character)
    const QString compressed = removeSequentialCharacters(workText);

    // Generate the version with diacritial marks stripped
    const QString diacriticsStripped = m_stringTools->stripDiacritics(compressed);
    variants.append(diacriticsStripped);

    // Generate the version with Umlauts replaced by two characters
    // and the remainung diacritial marks stripped afterwards
    QString umlautsReplaced = compressed;
    umlautsReplaced = m_stringTools->replaceUmlauts(umlautsReplaced);
    umlautsReplaced = m_stringTools->stripDiacritics(umlautsReplaced);
    if (! variants.contains(umlautsReplaced)) {
        variants.append(umlautsReplaced);
    }

    // ... and the same for the opposite direction: Two characters replaced by the corresponding
    // Umlaut and all diacritial marks stripped afterwards.
    // We need this so that if we have e. g. "Bär / Rödel", a search for "bar roed" will also match.
    QString replacementsToUmlautMapping = compressed;
    for (int i = 0; i < s_replacedUmlauts.size(); i++) {
        replacementsToUmlautMapping.replace(s_replacedUmlauts.at(i),
                                            s_replacedUmlautsMapping.at(i));
    }
    replacementsToUmlautMapping = m_stringTools->stripDiacritics(replacementsToUmlautMapping);
    if (! variants.contains(replacementsToUmlautMapping)) {
        variants.append(replacementsToUmlautMapping);
    }

    return variants;
}

QString SearchEngine::removeSequentialCharacters(QString text) const
{
    QRegularExpressionMatch match = s_sequentialCharacters.match(text);
    while (match.hasMatch()) {
        text.replace(match.captured(0), match.captured(1));
        match = s_sequentialCharacters.match(text);
    }
    return text;
}

bool SearchEngine::checkMatch(const QString &text, SearchType searchType, bool phoneticSearch) const
{
    if (text.isEmpty()) {
        return false;
    }

    return checkMatch(m_variantsCache.value(searchType)->value(text), phoneticSearch);
}

bool SearchEngine::checkMatch(const QVector<QString> &variantList, bool phoneticSearch) const
{
    const int variantListCount = variantList.count();
    const int start = phoneticSearch ? 0 : 1;

    for (int searchTermIndex = start; searchTermIndex < m_searchTerm.count(); searchTermIndex++) {
        const auto wordsCount = m_searchTerm.at(searchTermIndex);
        const auto words = wordsCount.keys();

        for (int variantsIndex = start; variantsIndex < variantListCount; variantsIndex++) {
            const QString &variant = variantList.at(variantsIndex);

            bool match = true;
            for (const QString &word : words) {
                if (variant.count(word) < wordsCount.value(word)) {
                    match = false;
                    break;
                }
            }

            if (match) {
                return true;
            }
        }
    }

    return false;
}

void SearchEngine::clearSearchCache(SearchType searchType)
{
    (*m_variantsCache.value(searchType)).clear();
}

void SearchEngine::updateSearchCache(const QString &name, SearchType searchType)
{
    (*m_variantsCache.value(searchType)).insert(name, searchVariants(name));
}

void SearchEngine::setSearchTerm(const QString &text)
{
    setSearchTerm(searchVariants(text));
}

void SearchEngine::setSearchTerm(const QVector<QString> &variantsList)
{
    m_searchTerm.clear();
    for (const QString &variant : variantsList) {
        QHash<QString, int> variantWords;
        const auto splitted = variant.split(s_space);
        for (const QString &word : splitted) {
            variantWords[word]++;
        }
        m_searchTerm.append(variantWords);
    }
}

QString SearchEngine::koelnerPhonetik(const QString &text) const
{
    // Calculate the Kölner Phonetik representation of a string
    // cf. https://de.wikipedia.org/wiki/Kölner_Phonetik

    // WARNING
    // This function expects text to be lower-case, "ß" replaced by "s", Umlaut diacritical marks
    // stripped and all words only separated by one space.
    // If it's used in some other context, be sure to do the proper preparation here!

    QStringList phonetic;

    const auto words = text.split(s_space);
    for (QString word : words) {
        // Step 1: Translate the string using the translation table
        QString translated;
        const int count = word.size();

        for (int i = 0; i < count; i++) {
            const QChar current = word.at(i);
            const QChar before = i > 0 ? word.at(i - 1) : QChar();
            const QChar after = i < count - 1 ? word.at(i + 1) : QChar();

            // Keep spaces between words
            if (current == s_space) {
                translated += s_space;

            // a, e, i, j, o, u, y
            } else if (current == Koeln::a
                    || current == Koeln::e
                    || current == Koeln::i
                    || current == Koeln::j
                    || current == Koeln::o
                    || current == Koeln::u
                    || current == Koeln::y) {

                translated += Koeln::zero;

            // b
            } else if (current == Koeln::b) {
                translated += Koeln::one;

            // p, but not before h
            } else if (current == Koeln::p
                    && after != Koeln::h) {

                translated += Koeln::one;

            // d, t, but not before c, s, z
            } else if ((current == Koeln::d
                        || current == Koeln::t)
                    && (   after != Koeln::c
                        && after != Koeln::s
                        && after != Koeln::z)) {

                translated += Koeln::two;

            // f, v, w
            } else if (current == Koeln::f
                    || current == Koeln::v
                    || current == Koeln::w) {

                translated += Koeln::three;

            // p before h
            } else if (current == Koeln::p
                    && after == Koeln::h) {

                translated += Koeln::three;

            // g, k, q
            } else if (current == Koeln::g
                    || current == Koeln::k
                    || current == Koeln::q) {

                translated += Koeln::four;

            // c at the beginning before a, h, k, l, o, q, r, u, x
            } else if (i == 0
                    && current == Koeln::c
                    && (after == Koeln::a
                        || after == Koeln::h
                        || after == Koeln::k
                        || after == Koeln::l
                        || after == Koeln::o
                        || after == Koeln::q
                        || after == Koeln::r
                        || after == Koeln::u
                        || after == Koeln::x)) {

                translated += Koeln::four;

            // c after the beginning before a, h, k, o, q, u, x but not after s, z
            } else if (i > 0
                    && current == Koeln::c
                    && (before != Koeln::s
                        && before != Koeln::z)
                    && (after == Koeln::a
                        || after == Koeln::h
                        || after == Koeln::k
                        || after == Koeln::o
                        || after == Koeln::q
                        || after == Koeln::u
                        || after == Koeln::x)) {

                translated += Koeln::four;

            // x but not after c, k, q
            } else if (current == Koeln::x
                    && (before != Koeln::c
                        && before != Koeln::k
                        && before != Koeln::q)) {

                translated += Koeln::fourEight;

            // l
            } else if (current == Koeln::l) {
                translated += Koeln::five;

            // m, n
            } else if (current == Koeln::m
                    || current == Koeln::n) {

                translated += Koeln::six;

            // r
            } else if (current == Koeln::r) {
                translated += Koeln::seven;

            // s, z
            } else if (current == Koeln::s
                    || current == Koeln::z) {

                translated += Koeln::eight;

            // c after s, z
            } else if (current == Koeln::c
                    && (before == Koeln::s
                        || before == Koeln::z)) {

                translated += Koeln::eight;

            // c at the beginning, but not before a, h, k, l, o, q, r, u, x
            } else if (i == 0
                    && current == Koeln::c
                    && (after != Koeln::a
                        && after != Koeln::h
                        && after != Koeln::k
                        && after != Koeln::l
                        && after != Koeln::o
                        && after != Koeln::q
                        && after != Koeln::r
                        && after != Koeln::u
                        && after != Koeln::x)) {

                translated += Koeln::eight;

            // c, but not before a, h, k, o, q, u, x
            } else if (current == Koeln::c
                    && (after != Koeln::a
                        && after != Koeln::h
                        && after != Koeln::k
                        && after != Koeln::o
                        && after != Koeln::q
                        && after != Koeln::u
                        && after != Koeln::x)) {

                translated += Koeln::eight;

            // d, t before c, s, z
            } else if ((current == Koeln::d
                        || current == Koeln::t)
                    && (after == Koeln::c
                        || after == Koeln::s
                        || after == Koeln::z)) {

                translated += Koeln::eight;

            // x after c, k, q
            } else if (current == Koeln::x
                    && (before == Koeln::c
                        || before == Koeln::k
                        || before == Koeln::q)) {

                translated += Koeln::eight;
            }

        }

        // Step 2: Replace sequences of numbers with only one
        translated = removeSequentialCharacters(translated);

        // Step 3: Remove all occurences of 0 that are not at the start
        // This has to be done in an extra step: Sequences of numbers can appear after doing this.
        // If we don't add the zeroes in the first place, those would be eliminated by step 2.
        QString zeroesStripped;
        for (int i = 0; i < translated.size(); i++) {
            const QChar current = translated.at(i);
            if (current != Koeln::zero || i == 0) {
                zeroesStripped.append(current);
            }
        }

        // Add the result to the phonetic representation word list (if it was a translatable word)
        if (! zeroesStripped.isEmpty()) {
            phonetic.append(zeroesStripped);
        }
    }

    // Join the list and return it
    return phonetic.join(s_space);
}

QHash<QString, QStringList> SearchEngine::checkListForPossibleDuplicates(const QStringList &list,
                                                                         bool phoneticSearch)
{
    QHash<QString, QStringList> possibleDuplicates;
    const int count = list.count();

    if (count == 1) {
        return possibleDuplicates;
    }

    QHash<QString, QVector<QString>> variantsCache;
    for (const QString &name : list) {
        variantsCache.insert(name, searchVariants(name));
    }

    // We actually do have to compare each entry to each other, because if we "only" do an upper
    // triangular matrix comparison, we lose potentional duplicates of entries that are on a higher
    // position in the list, because they aren't compared to the lower indices anymore.

    for (int searchIndex = 0; searchIndex < count; searchIndex++) {
        setSearchTerm(variantsCache.value(list.at(searchIndex)));

        for (int compareIndex = 0; compareIndex < count; compareIndex++) {
            if (searchIndex == compareIndex) {
                continue;
            }

            if (checkMatch(variantsCache.value(list.at(compareIndex).simplified()),
                phoneticSearch)) {

                possibleDuplicates[list.at(searchIndex)].append(list.at(compareIndex));
            }
        }
    }

    return possibleDuplicates;
}
