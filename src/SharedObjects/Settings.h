// SPDX-FileCopyrightText: 2019-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SETTINGS_H
#define SETTINGS_H

// Local includes

#include "shared/RegistrationColumns.h"
#include "shared/Tournament.h"
#include "shared/Markers.h"
#include "shared/TournamentTemplate.h"
#include "shared/Draw.h"
#include "shared/Schedule.h"
#include "shared/NoteType.h"

#include "SchedulePage/StopWatchState.h"

// Qt includes
#include <QString>
#include <QSettings>

// Qt classes
class QAction;

class Settings : public QSettings
{
    Q_OBJECT

public:
    inline static const QString registrationPhoneticId
        = QStringLiteral("registration_page_register");

    enum SaveOnCloseOption {
        SaveWindowGeometry,
        SaveWindowState,
        SaveLastDbPath,
        SaveStopWatchState,
        SavePhoneticSearchState,
        SaveRegistrationOptionsState,
        SaveBookingCodesDir,
        SaveBookingCodesScaleFactor,
        SaveNotesSettings,
        SaveOnCloseCount
    };

    explicit Settings(QObject *parent);

    void saveSaveOnCloseState(SaveOnCloseOption option, bool state);
    bool saveOnCloseEnabled(SaveOnCloseOption option) const;

    void saveTabPosition(const QString &position);
    QString tabPosition() const;

    void saveMainWindowGeometry(const QByteArray &data);
    QByteArray mainWindowGeometry() const;

    void saveMainWindowState(const QByteArray &data);
    QByteArray mainWindowState() const;

    void saveLastDbPath(const QString &dbPath);
    QString lastDbPath() const;

    void saveStopWatchState(StopWatchState::State state);
    StopWatchState::State stopWatchState() const;

    bool newTournamentShowExtendedOptions() const;
    void saveNewTournamentShowExtendedOptions(bool state);

    bool scheduleStartStopWatch() const;
    void saveScheduleStartStopWatch(bool state);

    QVector<QString> savedScheduleDisplaySettings();
    void saveLastUsedDisplaySettings(const QString &id);
    QString lastUsedScheduleDisplaySettings() const;
    void saveScheduleDisplaySettings(const QString &id, const QString &settings, int version);
    int scheduleDisplaySettingsVersion(const QString &id) const;
    QString scheduleDisplaySettings(const QString &id);

    bool phoneticSearchState(const QString &searchId) const;
    void savePhoneticSearchState(const QString &searchId, bool state);

    bool searchAsYouTypeState() const;
    void saveSearchAsYouTypeState(bool state);
    bool autoCapitalizeState() const;
    void saveAutoCapitalizeState(bool state);

    void saveTournamentTemplate(const TournamentTemplate::Settings &tournamentTemplate);
    TournamentTemplate::Settings tournamentTemplate() const;

    void saveDbFileNameTemplate(const QString &text);
    QString dbFileNameTemplate() const;

    void saveMarkersTemplate(Tournament::Mode tournamentMode,
                             const QVector<Markers::Marker> &markers);
    QVector<Markers::Marker> defaultMarkersTemplate(Tournament::Mode tournamentMode) const;
    QVector<Markers::Marker> markersTemplate(Tournament::Mode tournamentMode) const;
    QHash<Markers::Type, int> specialMarkers() const;

    void saveMarker(Markers::Type type, int id);
    void saveMarkers(const QHash<Markers::Type, int> &markers);
    int marker(Markers::Type type) const;

    void saveSinglesManagementTemplate(const Markers::SinglesManagement &data);
    Markers::SinglesManagement defaultSinglesManagementTemplate() const;
    Markers::SinglesManagement singlesManagementTemplate() const;

    void saveBoogerSymbol(const QString &text);
    const QString &boogerSymbol() const;

    void saveNamesSeparator(const QString &text);
    void setTransientNamesSeparator(const QString &text);
    bool resetNamesSeparator();
    const QString &namesSeparator() const;

    void savePreferIpv6(bool state);
    bool preferIpv6() const;

    void saveServerPort(int port);
    int serverPort() const;
    void saveConnectTimeout(int timeout);
    int connectTimeout() const;
    void saveRequestTimeout(int timeout);
    int requestTimeout() const;

    void saveDiscoverPort(int port);
    int discoverPort() const;
    void saveDiscoverTimeout(int timeout);
    int discoverTimeout() const;
    void saveDiscoverAttempts(int attempts);
    int discoverAttempts() const;

    void saveWlanCodeScannerServerPort(int port);
    int wlanCodeScannerServerPort() const;
    void saveWlanCodeScannerServerKey(const QString &key);
    QString wlanCodeScannerServerKey() const;

    void saveRegistrationColumns(const QHash<RegistrationColumns::Column, bool> &columns);
    QHash<RegistrationColumns::Column, bool> registrationColumns() const;

    void saveDrawSettingsTemplate(const Draw::Settings &data);
    Draw::Settings drawSettingsTemplate() const;

    void saveScheduleSettingsTemplate(const Schedule::Settings &data);
    Schedule::Settings scheduleSettingsTemplate() const;

    void saveMessageDismissed(const QString &id, bool state);
    void showAllDismissedMessages();
    bool messageDismissed(const QString &id);

    void saveBookingCodesDir(const QString &dir);
    QString bookingCodesDir() const;
    void saveBookingCodesScaleFactor(int factor);
    int bookingCodesScaleFactor() const;

    QVector<double> defaultNotesSpacing(NoteType::Type type) const;
    QVector<double> notesSpacing(NoteType::Type type) const;
    void saveNotesSpacing(NoteType::Type type, const QVector<double> &values);

    QString notesSettings(NoteType::Type type) const;
    void saveNotesSettings(NoteType::Type type, const QString &settings);

    int scoreNotesBoogers() const;
    void saveScoreNotesBoogers(int boogers);

private: // Functions
    bool parseMarkerData(int &id, const QJsonValue &value, QVector<Markers::Marker> &markers) const;
    QString scheduleDisplaySettingsKey(const QString &id) const;

private: // Variables
    QString m_boogerSymbol;
    QString m_namesSeparator;
    QString m_originalNamesSeparator;

};

#endif // SETTINGS_H
