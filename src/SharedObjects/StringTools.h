// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef STRINGTOOLS_H
#define STRINGTOOLS_H

// Qt includes
#include <QObject>
#include <QLocale>
#include <QCollator>

// C++ includes
#include <algorithm>

// Local classes
namespace Players
{
struct DisplayData;
}

class StringTools : public QObject
{
    Q_OBJECT

public:
    enum NameFormat {
        BareName,
        DisqualifiedName,
        FormattedName
    };

    explicit StringTools(QObject *parent, const QString *boogerSymbol,
                         const QString *namesSeparator);

    template<typename T>
    void sort(T &data) const
    {
        std::sort(data.begin(), data.end(), m_collator);
    }

    QString toLower(const QString &text) const;
    QString toUpper(const QString &text) const;
    QString replaceUmlauts(QString text) const;
    QString stripDiacritics(const QString &text) const;

    void setBoogerScore(int score);
    QString boogify(int score) const;
    QString locale() const;
    QString defaultLocale() const;
    void setLocale(const QString &name);
    bool resetLocale();
    bool localeChanged() const;
    QString dateFormat() const;
    QString timeFormat() const;

    QString pairOrPlayerName(const Players::DisplayData &data, NameFormat format) const;
    QString pairName(const Players::DisplayData &player1, const Players::DisplayData &player2,
                     NameFormat format = NameFormat::BareName) const;
    QString normalizedPairName(const Players::DisplayData &player1,
                               const Players::DisplayData &player2) const;

private: // Variables
    const QString *m_boogerSymbol;
    const QString *m_namesSeparator;
    QLocale m_locale;
    QCollator m_collator;
    bool m_localeChanged = false;
    int m_boogerScore;

};

#endif // STRINGTOOLS_H
