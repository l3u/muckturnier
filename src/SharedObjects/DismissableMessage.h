// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef DISMISSABLEMESSAGEBOX_H
#define DISMISSABLEMESSAGEBOX_H

// Qt includes
#include <QObject>
#include <QHash>
#include <QMessageBox>

// Local classes
class Settings;

// Qt classes
class QWidget;

class DismissableMessage : public QObject
{
    Q_OBJECT

public: // Enums
    enum SessionMessage {
        // This should never be used
        NonExistingFallback,

        // MainWindow
        AskRestoreOriginalDb,
        AskFinishTournament,

        // RegistrationPage

        AskShowDrawColumn,
        AskHideDrawColumn,
        AskShowDrawOverview,

        AskShowNumberColumn,
        AskHideNumberColumn
    };

    // NOTICE: When a message is added here, also update
    // s_globalMessageMap in DismissableMessage.cpp!
    enum GlobalMessage {
        UnDismissHint,

        // RegistrationPage
        DrawHint
    };

public:
    explicit DismissableMessage(QObject *parent, Settings *settings);

    bool dismissed(SessionMessage id) const;
    bool dismissed(GlobalMessage id) const;

    void setDismissed(SessionMessage id, bool state);
    void setDismissed(GlobalMessage id, bool state);

    QMessageBox::StandardButton question(
        SessionMessage id, QWidget *parent, const QString &title, const QString &text,
        QMessageBox::StandardButton defaultButton = QMessageBox::Yes);
    void info(GlobalMessage id, QWidget *parent, const QString &title, const QString &text);

private: // Variables
    Settings *m_settings;
    QHash<SessionMessage, bool> m_dismissed;

};

#endif // DISMISSABLEMESSAGEBOX_H
