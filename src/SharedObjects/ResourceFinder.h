// SPDX-FileCopyrightText: 2018-2022 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef RESOURCEFINDER_H
#define RESOURCEFINDER_H

// Qt includes
#include <QObject>
#include <QHash>
#include <QVector>

class ResourceFinder : public QObject
{
    Q_OBJECT

public:
    explicit ResourceFinder(QObject *parent);
    QString find(const QString &fileName);

private: // Functions
    QString locate(const QString &fileName);

private: // Variables
    QVector<QString> m_possibleLocations;
    QHash<QString, QString> m_path;

};

#endif // RESOURCEFINDER_H
