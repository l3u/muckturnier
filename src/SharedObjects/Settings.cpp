// SPDX-FileCopyrightText: 2019-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "Settings.h"

#include "shared/DefaultValues.h"
#include "shared/Markers.h"
#include "shared/JsonHelper.h"
#include "shared/ColorHelper.h"
#include "shared/Logging.h"
#include "shared/Json.h"

// Qt includes
#include <QDebug>
#include <QAction>
#include <QJsonObject>
#include <QJsonArray>
#include <QStandardPaths>
#include <QDir>

// C++ includes
#include <utility>

// Config file string literals
// ===========================

// Save on close settings
static const QString s_saveOnClose = QStringLiteral("save_on_close/");
static const QString s_saveOnClose_windowGeometry
    = s_saveOnClose + QLatin1String("mainwindow_geometry");
static const QString s_saveOnClose_windowState
    = s_saveOnClose + QLatin1String("mainwindow_state");
static const QString s_saveOnClose_lastDbPath
    = s_saveOnClose + QLatin1String("last_db_path");
static const QString s_saveOnClose_stopWatchState
    = s_saveOnClose + QLatin1String("stopwatch_state");
static const QString s_saveOnClose_phoneticSearchState
    = s_saveOnClose + QLatin1String("phonetic_search");
static const QString s_saveOnClose_registrationOptionsState
    = s_saveOnClose + QLatin1String("registration_options");
static const QString s_saveOnClose_bookingCodesDir
    = s_saveOnClose + QLatin1String("booking_codes_dir");
static const QString s_saveOnClose_bookingCodesScaleFactor
    = s_saveOnClose + QLatin1String("booking_codes_scale_factor");
static const QString s_saveOnClose_notesSettings
    = s_saveOnClose + QLatin1String("notes_settings");

// Session data
static const QString s_sessionData = QStringLiteral("session_data/");
static const QString s_sessionData_tabPosition
    = s_sessionData + QLatin1String("tab_position");
static const QString s_sessionData_mainWindowGeometry
    = s_sessionData + QLatin1String("mainwindow_geometry");
static const QString s_sessionData_mainWindowState
    = s_sessionData + QLatin1String("mainwindow_state");
static const QString s_sessionData_lastDbPath
    = s_sessionData + QLatin1String("last_db_path");
static const QString s_sessionData_newTournamentShowExtendedOptions
    = s_sessionData + QLatin1String("new_tournament_show_extended_options");

// Schedule page settings
static const QString s_schedulePage = QStringLiteral("schedule_page/");
static const QString s_schedulePage_startStopWatch
    = s_schedulePage + QLatin1String("start_stop_watch");
static const QString s_schedulePage_settingsVersion
    = s_schedulePage + QLatin1String("settings_version");
static const QString s_schedulePage_settings
    = s_schedulePage + QLatin1String("settings");

// Schedule display settings
static const QLatin1String s_displaySettings("display_settings");
static const QLatin1String s_version("version");
static const QLatin1Char s_underscore('_');
static const QString s_schedulePage_displaySettingsPrefix = s_schedulePage + s_displaySettings;
static const QString s_schedulePage_lastUsedDisplaySettings
    = s_schedulePage + QLatin1String("last_used_settings");

// Registration page settings
static const QString s_registrationPage = QStringLiteral("registration_page/");
static const QString s_searchAsYouType = s_registrationPage + QStringLiteral("search_as_you_type");
static const QString s_autoCapitalize  = s_registrationPage + QStringLiteral("auto_capitalize");
static const QVector<QString> s_registrationPageOptionsDataSets {
    s_searchAsYouType,
    s_autoCapitalize
};

// Stop watch state
static const QLatin1String s_stopWatchState("stopwatch_state/");
static const QString s_stopWatchState_version
    = s_stopWatchState + QLatin1String("version");
static const QString s_stopWatchState_hours
    = s_stopWatchState + QLatin1String("hours");
static const QString s_stopWatchState_minutes
    = s_stopWatchState + QLatin1String("minutes");
static const QString s_stopWatchState_seconds
    = s_stopWatchState + QLatin1String("seconds");
static const QString s_stopWatchState_title
    = s_stopWatchState + QLatin1String("title");
static const QString s_stopWatchState_geometry
    = s_stopWatchState + QLatin1String("geometry");

// Phonetic search state
static const QString s_phoneticSearch = QStringLiteral("phonetic_search");

// Strings
static const QLatin1String s_strings("strings/");
static const QString s_strings_boogerSymbol = s_strings + QLatin1String("booger_symbol");
static const QString s_strings_namesSeparator = s_strings + QLatin1String("names_separator");

// Tournament template

static const QString s_tournamentTemplate = QStringLiteral("tournament_template");
static const QString s_tournamentTemplate_version
    = s_tournamentTemplate + QLatin1String("/version");
static const QString s_tournamentTemplate_dbFileNameTemplate
    = s_tournamentTemplate + QLatin1String("/db_file_name_template");
static const QString s_tournamentTemplate_tournamentMode
    = s_tournamentTemplate + QLatin1String("/tournament_mode");
static const QString s_tournamentTemplate_scoreType
    = s_tournamentTemplate + QLatin1String("/score_type");
static const QString s_tournamentTemplate_boogerScore
    = s_tournamentTemplate + QLatin1String("/booger_score");
static const QString s_tournamentTemplate_boogersPerRound
    = s_tournamentTemplate + QLatin1String("/boogers_per_round");
static const QString s_tournamentTemplate_registrationColumns
    = s_tournamentTemplate + QLatin1String("/registration_columns");
static const QString s_tournamentTemplate_registrationListHeaderHidden
    = s_tournamentTemplate + QLatin1String("/registration_list_header_hidden");
static const QString s_tournamentTemplate_autoSelectPairs
    = s_tournamentTemplate + QLatin1String("/auto_select_pairs");
static const QString s_tournamentTemplate_selectByLastRound
    = s_tournamentTemplate + QLatin1String("/select_by_last_round");
static const QString s_tournamentTemplate_autoSelectTableForDraw
    = s_tournamentTemplate + QLatin1String("/auto_select_table_for_draw");
static const QString s_tournamentTemplate_includeOpponentGoals
    = s_tournamentTemplate + QLatin1String("/include_opponent_goals");
static const QString s_tournamentTemplate_grayOutUnimportantGoals
    = s_tournamentTemplate + QLatin1String("/gray_out_unimportant_goals");
static const QString s_tournamentTemplate_optionalPages
    = s_tournamentTemplate + QLatin1String("/optional_pages");

// Markers template

static constexpr int s_markersTemplate_version = 2;

static const QString s_markersTemplate = QStringLiteral("markers_template");

// The legacy universal template
static const QString s_markersTemplate_legacyVersionKey
    = s_markersTemplate + QLatin1String("/version");
static const QString s_markersTemplate_legacyMarkers
    = s_markersTemplate + QLatin1String("/markers");

// The new "fixed pairs" and "single players" version templates
static const QHash<Tournament::Mode, QString> s_markersTemplate_versionKey {
    { Tournament::FixedPairs,    s_markersTemplate + QLatin1String("/fixed_pairs_version") },
    { Tournament::SinglePlayers, s_markersTemplate + QLatin1String("/single_players_version") }
};
static const QHash<Tournament::Mode, QString> s_markersTemplate_markers {
    { Tournament::FixedPairs,    s_markersTemplate + QLatin1String("/fixed_pairs_markers") },
    { Tournament::SinglePlayers, s_markersTemplate + QLatin1String("/single_players_markers") }
};

static const QLatin1String s_markersTemplate_nameKey   ("n");
static const QLatin1String s_markersTemplate_colorKey  ("c");
static const QLatin1String s_markersTemplate_sortingKey("s");

static const QHash<Markers::Type, QString> s_markersTemplate_markerKey {
    { Markers::Default, s_markersTemplate + QLatin1String("/default_marker") },
    { Markers::Drawn,   s_markersTemplate + QLatin1String("/drawn_marker") },
    { Markers::Booked,  s_markersTemplate + QLatin1String("/booked_marker") }
};

// Singles Management

static const QString s_singlesManagementTemplate = QStringLiteral("singles_management_template");

static const QString s_singlesManagementTemplate_version
    = s_singlesManagementTemplate + QLatin1String("/version");
static const QString s_singlesManagementTemplate_data
    = s_singlesManagementTemplate + QLatin1String("/data");

// Network settings

static const QString s_network = QStringLiteral("network");

static const QString s_network_preferIpv6       = s_network + QLatin1String("/prefer_ipv6");

static const QString s_network_serverPort       = s_network + QLatin1String("/server_port");
static const QString s_network_connectTimeout   = s_network + QLatin1String("/connect_timeout");
static const QString s_network_requestTimeout   = s_network + QLatin1String("/request_timeout");

static const QString s_network_discoverPort     = s_network + QLatin1String("/discover_port");
static const QString s_network_discoverTimeout  = s_network + QLatin1String("/discover_timeout");
static const QString s_network_discoverAttempts = s_network + QLatin1String("/discover_attempts");

static const QString s_network_wlanCodeScannerServerPort
    = s_network + QLatin1String("/wlan_code_scanner_server_port");
static const QString s_network_wlanCodeScannerServerKey
    = s_network + QLatin1String("/wlan_code_scanner_server_key");

// QTabWidget::TabPosition map
static const QVector<QString> s_possibleTabPositions {
    QStringLiteral("north"),
    QStringLiteral("south"),
    QStringLiteral("west"),
    QStringLiteral("east")
};

// Draw settings
static const QString s_drawSettingsTemplate = QStringLiteral("draw_settings_template");
static const QString s_drawSettingsTemplate_version
    = s_drawSettingsTemplate + QLatin1String("/version");
static const QString s_drawSettingsTemplate_data
    = s_drawSettingsTemplate + QLatin1String("/data");

// Dismissable messages

static const QString s_dismissedMessages = QStringLiteral("dismissed_messages/");

// Booking settings
static const QString s_bookingSettings = QStringLiteral("booking/");
static const QString s_bookingSettings_codesDir = s_bookingSettings + QLatin1String("codes_dir");
static const QString s_bookingSettings_codesScaleFactor
    = s_bookingSettings + QLatin1String("codes_scale_factor");

// Notes generator settings

static const QHash<NoteType::Type, QString> s_noteTypeStrings {
    { NoteType::Shared,             QStringLiteral("shared") },
    { NoteType::TableNumbers,       QStringLiteral("table_numbers") },
    { NoteType::DrawNotes,          QStringLiteral("draw_notes") },
    { NoteType::ScoreNotes1Booger,  QStringLiteral("score_notes_1_booger") },
    { NoteType::ScoreNotes,         QStringLiteral("score_notes_2_boogers") },
    { NoteType::ScoreNotes3Boogers, QStringLiteral("score_notes_3_boogers") }
};

static const QString s_notesSpacing = QStringLiteral("notes_spacing/");
static const QString s_notesSpacing_spacing = s_notesSpacing + QLatin1String("%1");
static const QString s_notesSettings = QStringLiteral("notes_settings/");
static const QString s_notesSettings_setting = s_notesSettings + QLatin1String("%1");
static const QString s_notesSettings_scoreNotesBoogers
    = s_notesSettings + QLatin1String("score_note_boogers");

// Settings functions
// ==================

Settings::Settings(QObject *parent)
    : QSettings(QStringLiteral("muckturnier.org"), QStringLiteral("muckturnier"), parent)
{
    m_boogerSymbol = value(s_strings_boogerSymbol, DefaultValues::boogerSymbol).toString();
    if (m_boogerSymbol.size() > 1 || m_boogerSymbol.simplified().isEmpty()) {
        m_boogerSymbol = DefaultValues::boogerSymbol;
    }

    m_namesSeparator = value(s_strings_namesSeparator, DefaultValues::namesSeparator).toString();
    m_namesSeparator.remove(QLatin1Char('\t'));

    // If we still have the old "absent" marker saved, move it to "booked"

    const auto legacyBookedMarker = value(QStringLiteral("markers_template/absent_marker"),
                                          -1).toInt();
    if (legacyBookedMarker != -1
        && ! contains(s_markersTemplate_markerKey.value(Markers::Booked))) {

        qCDebug(MuckturnierLog) << "Copying the old \"absent\" marker defintion for the \"booked\" "
                                << "marker";
        saveMarker(Markers::Booked, legacyBookedMarker);
    }

    // If we have "score_notes" settings, move them to "score_notes_2_boogers"

    const auto scoreNotesSettings = value(QStringLiteral("notes_settings/score_notes"),
                                          QString()).toString();
    if (! scoreNotesSettings.isEmpty()) {
        qCDebug(MuckturnierLog) << "Moving the old \"score_notes\" settings to"
                                << "\"score_notes_2_boogers\"";
        setValue(QStringLiteral("notes_settings/score_notes_2_boogers"), scoreNotesSettings);
        remove(QStringLiteral("notes_settings/score_notes"));
    }

    const auto scoreNotesSpacing = value(QStringLiteral("notes_spacing/score_notes"),
                                         QString()).toString();
    if (! scoreNotesSpacing.isEmpty()) {
        qCDebug(MuckturnierLog) << "Moving the old \"score_notes\" spacing to"
                                << "\"score_notes_2_boogers\"";
        setValue(QStringLiteral("notes_spacing/score_notes_2_boogers"), scoreNotesSpacing);
        remove(QStringLiteral("notes_spacing/score_notes"));
    }
}

// Save on close values
// --------------------

void Settings::saveSaveOnCloseState(Settings::SaveOnCloseOption option, bool state)
{
    switch(option) {
    case SaveOnCloseOption::SaveWindowGeometry:
        setValue(s_saveOnClose_windowGeometry, state);
        break;
    case SaveOnCloseOption::SaveWindowState:
        setValue(s_saveOnClose_windowState, state);
        break;
    case SaveOnCloseOption::SaveLastDbPath:
        setValue(s_saveOnClose_lastDbPath, state);
        break;
    case SaveOnCloseOption::SaveStopWatchState:
        setValue(s_saveOnClose_stopWatchState, state);
        break;
    case SaveOnCloseOption::SavePhoneticSearchState:
        setValue(s_saveOnClose_phoneticSearchState, state);
        break;
    case SaveOnCloseOption::SaveRegistrationOptionsState:
        setValue(s_saveOnClose_registrationOptionsState, state);
        break;
    case SaveOnCloseOption::SaveBookingCodesDir:
        setValue(s_saveOnClose_bookingCodesDir, state);
        break;
    case SaveOnCloseOption::SaveBookingCodesScaleFactor:
        setValue(s_saveOnClose_bookingCodesScaleFactor, state);
        break;
    case SaveOnCloseOption::SaveNotesSettings:
        setValue(s_saveOnClose_notesSettings, state);
        break;
    case SaveOnCloseOption::SaveOnCloseCount:
        Q_ASSERT_X(false, "Settings::saveSaveOnCloseState",
                          "Tried to save the \"Count\" value");
        break;
    };

    if (! state) {
        switch(option) {
        case SaveOnCloseOption::SaveWindowGeometry:
            remove(s_sessionData_mainWindowGeometry);
            break;
        case SaveOnCloseOption::SaveWindowState:
            remove(s_sessionData_mainWindowState);
            break;
        case SaveOnCloseOption::SaveLastDbPath:
            remove(s_sessionData_lastDbPath);
            break;
        case SaveOnCloseOption::SaveStopWatchState:
            remove(s_stopWatchState);
            break;
        case SaveOnCloseOption::SavePhoneticSearchState:
            remove(s_phoneticSearch);
            break;
        case SaveOnCloseOption::SaveRegistrationOptionsState:
            remove(s_searchAsYouType);
            remove(s_autoCapitalize);
            break;
        case SaveOnCloseOption::SaveBookingCodesDir:
            remove(s_bookingSettings_codesDir);
            break;
        case SaveOnCloseOption::SaveBookingCodesScaleFactor:
            remove(s_bookingSettings_codesScaleFactor);
            break;
        case SaveOnCloseOption::SaveNotesSettings:
            remove(s_notesSettings);
            break;
        case SaveOnCloseOption::SaveOnCloseCount:
            Q_ASSERT_X(false, "Settings::saveSaveOnCloseState",
                              "Tried to save the \"Count\" value");
            break;
        }
    }
}

bool Settings::saveOnCloseEnabled(Settings::SaveOnCloseOption option) const
{
    QVariant data;
    switch(option) {
    case SaveOnCloseOption::SaveWindowGeometry:
        data = value(s_saveOnClose_windowGeometry);
        break;
    case SaveOnCloseOption::SaveWindowState:
        data = value(s_saveOnClose_windowState);
        break;
    case SaveOnCloseOption::SaveLastDbPath:
        data = value(s_saveOnClose_lastDbPath);
        break;
    case SaveOnCloseOption::SaveStopWatchState:
        data = value(s_saveOnClose_stopWatchState);
        break;
    case SaveOnCloseOption::SavePhoneticSearchState:
        data = value(s_saveOnClose_phoneticSearchState);
        break;
    case SaveOnCloseOption::SaveRegistrationOptionsState:
        data = value(s_saveOnClose_registrationOptionsState);
        break;
    case SaveOnCloseOption::SaveBookingCodesDir:
        data = value(s_saveOnClose_bookingCodesDir);
        break;
    case SaveOnCloseOption::SaveBookingCodesScaleFactor:
        data = value(s_saveOnClose_bookingCodesScaleFactor);
        break;
    case SaveOnCloseOption::SaveNotesSettings:
        data = value(s_saveOnClose_notesSettings);
        break;
    case SaveOnCloseOption::SaveOnCloseCount:
        Q_ASSERT_X(false, "Settings::saveSaveOnCloseState",
                          "Tried to save the \"Count\" value");
        break;
    };
    return data.isValid() ? data.toBool() : true;
}

// Tab position
// ------------

void Settings::saveTabPosition(const QString &position)
{
    setValue(s_sessionData_tabPosition, position);
}

QString Settings::tabPosition() const
{
    const QString position = value(s_sessionData_tabPosition, QStringLiteral("south")).toString();
    if (s_possibleTabPositions.contains(position)) {
        return position;
    } else {
        return QStringLiteral("south");
    }
}

// Main window geometry
// --------------------

void Settings::saveMainWindowGeometry(const QByteArray &data)
{
    setValue(s_sessionData_mainWindowGeometry, data);
}

QByteArray Settings::mainWindowGeometry() const
{
    return value(s_sessionData_mainWindowGeometry, QByteArray()).toByteArray();
}

// Main window state
// -----------------

void Settings::saveMainWindowState(const QByteArray &data)
{
    if (! data.isEmpty()) {
        setValue(s_sessionData_mainWindowState, data);
    } else {
        remove(s_sessionData_mainWindowState);
    }
}

QByteArray Settings::mainWindowState() const
{
    return value(s_sessionData_mainWindowState, QByteArray()).toByteArray();
}

// Last database path
// ------------------

void Settings::saveLastDbPath(const QString &dbPath)
{
    setValue(s_sessionData_lastDbPath, dbPath);
}

QString Settings::lastDbPath() const
{
    const QString documentsLocation
        = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
    const QString path = value(s_sessionData_lastDbPath, documentsLocation).toString();
    return QDir(path).exists() ? path : documentsLocation;
}

// Schedule page settings
// ----------------------

// Schedule settings

void Settings::saveScheduleSettingsTemplate(const Schedule::Settings &data)
{
    setValue(s_schedulePage_settingsVersion, Schedule::settingsVersion);
    setValue(s_schedulePage_settings,        JsonHelper::scheduleSettingsToString(data));
}

Schedule::Settings Settings::scheduleSettingsTemplate() const
{
    if (value(s_schedulePage_settingsVersion, 0).toInt() != Schedule::settingsVersion) {
        return Schedule::defaultSettings;
    }

    return JsonHelper::parseScheduleSettings(value(s_schedulePage_settings).toString());
}

// "Also start stop watch" setting

bool Settings::scheduleStartStopWatch() const
{
    return value(s_schedulePage_startStopWatch, true).toBool();
}

void Settings::saveScheduleStartStopWatch(bool state)
{
    setValue(s_schedulePage_startStopWatch, state);
}

// Schedule display settings

void Settings::saveLastUsedDisplaySettings(const QString &id)
{
    setValue(s_schedulePage_lastUsedDisplaySettings, id);
}

QString Settings::lastUsedScheduleDisplaySettings() const
{
    return value(s_schedulePage_lastUsedDisplaySettings, QString()).toString();
}

QVector<QString> Settings::savedScheduleDisplaySettings()
{
    QVector<QString> list;
    beginGroup(s_schedulePage);
    for (const auto &key : childKeys()) {
        if (key.startsWith(s_displaySettings) && ! key.endsWith(s_version)) {
            const auto id = key.mid(s_displaySettings.size() + 1);
            list.append(QString::fromUtf8(QByteArray::fromPercentEncoding(id.toUtf8(), '_')));
        }
    }
    endGroup();
    return list;
}

QString Settings::scheduleDisplaySettingsKey(const QString &id) const
{
    if (id.isEmpty()) {
        return s_schedulePage_displaySettingsPrefix;
    } else {
        return s_schedulePage_displaySettingsPrefix + s_underscore
               + QString::fromUtf8(id.toUtf8().toPercentEncoding(QByteArray(), QByteArray(), '_'));
    }
}

void Settings::saveScheduleDisplaySettings(const QString &id, const QString &settings, int version)
{
    const auto key = scheduleDisplaySettingsKey(id);
    const auto versionKey = key + s_underscore + s_version;
    if (! settings.isEmpty()) {
        setValue(key, settings);
        setValue(versionKey, version);
        saveLastUsedDisplaySettings(id);
    } else {
        remove(key);
        remove(versionKey);
        saveLastUsedDisplaySettings(QString());
    }
}

int Settings::scheduleDisplaySettingsVersion(const QString &id) const
{
    return value(scheduleDisplaySettingsKey(id) + s_underscore + s_version,
                 0).toInt();
}

QString Settings::scheduleDisplaySettings(const QString &id)
{
    return value(scheduleDisplaySettingsKey(id), QString()).toString();
    saveLastUsedDisplaySettings(id);
}

// Stop watch state
// ----------------

void Settings::saveStopWatchState(StopWatchState::State state)
{
    setValue(s_stopWatchState_version,  StopWatchState::version);
    setValue(s_stopWatchState_hours,    state.hours);
    setValue(s_stopWatchState_minutes,  state.minutes);
    setValue(s_stopWatchState_seconds,  state.seconds);
    setValue(s_stopWatchState_title,    state.title);
    setValue(s_stopWatchState_geometry, state.geometry);
}

StopWatchState::State Settings::stopWatchState() const
{
    if (! saveOnCloseEnabled(SaveOnCloseOption::SaveStopWatchState)
        || value(s_stopWatchState_version, 0).toInt() != StopWatchState::version) {

        return StopWatchState::defaultState;
    }

    int hours = value(s_stopWatchState_hours, 0).toInt();
    if (hours < 0 || hours > 99) {
        hours = 0;
    }

    int minutes = value(s_stopWatchState_minutes, 0).toInt();
    if (minutes < 0 || minutes > 59) {
        minutes = 0;
    }

    int seconds = value(s_stopWatchState_seconds, 0).toInt();
    if (seconds < 0 || seconds > 59) {
        seconds = 0;
    }

    return StopWatchState::State {
        hours,
        minutes,
        seconds,
        value(s_stopWatchState_title, 0).toString(),
        value(s_stopWatchState_geometry, QByteArray()).toByteArray()
    };
}

// "Extended options" of the "New tournament" dialog

bool Settings::newTournamentShowExtendedOptions() const
{
    return value(s_sessionData_newTournamentShowExtendedOptions, false).toBool();
}

void Settings::saveNewTournamentShowExtendedOptions(bool state)
{
    setValue(s_sessionData_newTournamentShowExtendedOptions, state);
}

// Phonetic search state

bool Settings::phoneticSearchState(const QString &searchId) const
{
    const bool defaultValue = searchId == Settings::registrationPhoneticId
                                  ? true : false;
    return value(s_phoneticSearch + QStringLiteral("/") + searchId, defaultValue).toBool();
}

void Settings::savePhoneticSearchState(const QString &searchId, bool state)
{
    if (saveOnCloseEnabled(SaveOnCloseOption::SavePhoneticSearchState)) {
        setValue(s_phoneticSearch + QStringLiteral("/") + searchId, state);
    }
}

// Search as you type state on the registration page

bool Settings::searchAsYouTypeState() const
{
    return value(s_searchAsYouType, true).toBool();
}

void Settings::saveSearchAsYouTypeState(bool state)
{
    if (saveOnCloseEnabled(SaveOnCloseOption::SaveRegistrationOptionsState)) {
        setValue(s_searchAsYouType, state);
    }
}

bool Settings::autoCapitalizeState() const
{
    return value(s_autoCapitalize, true).toBool();
}

void Settings::saveAutoCapitalizeState(bool state)
{
    if (saveOnCloseEnabled(SaveOnCloseOption::SaveRegistrationOptionsState)) {
        setValue(s_autoCapitalize, state);
    }
}

// Tournament template
// -------------------

void Settings::saveTournamentTemplate(const TournamentTemplate::Settings &tournamentTemplate)
{
    const int version = value(s_tournamentTemplate_version, -1).toInt();

    if (version < 3) {
        // Clean up the old "players page" category
        const QString playersPage = QStringLiteral("players_page");

        // The "columns" key has been moved to "tournament_template/players_page_columns"
        remove(playersPage + QLatin1String("/columns"));

        // Check if there are other keys in "players_page" (maybe added by later versions)
        beginGroup(playersPage);
        const bool otherKeysExist = allKeys().count() > 0;
        endGroup();

        // If not, remove the "players_page" category
        if (! otherKeysExist) {
            remove(playersPage);
        }
    }

    // Clean up the old "no table numbers" option
    if (version < 5) {
        remove(QStringLiteral("tournament_template/no_table_numbers"));
    }

    setValue(s_tournamentTemplate_version, TournamentTemplate::version);
    setValue(s_tournamentTemplate_tournamentMode,
             Tournament::modeMap.value(tournamentTemplate.tournamentMode));
    setValue(s_tournamentTemplate_scoreType,
             Tournament::scoreTypeMap.value(tournamentTemplate.scoreType));
    setValue(s_tournamentTemplate_boogerScore, tournamentTemplate.boogerScore);
    setValue(s_tournamentTemplate_boogersPerRound, tournamentTemplate.boogersPerRound);
    setValue(s_tournamentTemplate_registrationColumns,
             JsonHelper::registrationColumnsToString(tournamentTemplate.registrationColumns));
    setValue(s_tournamentTemplate_registrationListHeaderHidden,
             tournamentTemplate.registrationListHeaderHidden);
    setValue(s_tournamentTemplate_autoSelectPairs, tournamentTemplate.autoSelectPairs);
    setValue(s_tournamentTemplate_selectByLastRound, tournamentTemplate.selectByLastRound);
    setValue(s_tournamentTemplate_autoSelectTableForDraw,
             tournamentTemplate.autoSelectTableForDraw);
    setValue(s_tournamentTemplate_includeOpponentGoals, tournamentTemplate.includeOpponentGoals);
    setValue(s_tournamentTemplate_grayOutUnimportantGoals,
             tournamentTemplate.grayOutUnimportantGoals);
    setValue(s_tournamentTemplate_optionalPages,
             JsonHelper::optionalPagesToString(tournamentTemplate.optionalPages));

    saveDrawSettingsTemplate(tournamentTemplate.drawSettings);
    saveScheduleSettingsTemplate(tournamentTemplate.scheduleSettings);
}

TournamentTemplate::Settings Settings::tournamentTemplate() const
{
    const int version = value(s_tournamentTemplate_version, -1).toInt();
    if (version <= 0 || version > TournamentTemplate::version) {
        return TournamentTemplate::defaultSettings;
    }

    // Get the tournament mode
    Tournament::Mode tournamentMode;
    if (version >= 2) {
        // Check for a correct value of s_tournamentTemplate_tournamentMode
        const QString tournamentModeString
            = value(s_tournamentTemplate_tournamentMode,
                    Tournament::fixedPairsMode).toString();
        if (! Tournament::modeMap.values().contains(tournamentModeString)) {
            return TournamentTemplate::defaultSettings;
        } else {
            tournamentMode = Tournament::modeMap.key(tournamentModeString);
        }
    } else if (version < 2) {
        // Read the old bool "pair_mode" value
        tournamentMode = value(QStringLiteral("tournament_template/pair_mode"), true).toBool()
                                   ? Tournament::FixedPairs
                                   : Tournament::SinglePlayers;
    }

    // Get the score type
    Tournament::ScoreType scoreType;
    const QString scoreTypeString = value(s_tournamentTemplate_scoreType,
                                          Tournament::horizontalScoreType).toString();
    if (! Tournament::scoreTypeMap.values().contains(scoreTypeString)) {
        return TournamentTemplate::defaultSettings;
    } else {
        scoreType = Tournament::scoreTypeMap.key(scoreTypeString);
    }

    // Check for correct booger score and boogers per round
    const int boogerScore = value(s_tournamentTemplate_boogerScore,
                                  DefaultValues::boogerScore).toInt();
    const int boogersPerRound = value(s_tournamentTemplate_boogersPerRound,
                                      DefaultValues::boogersPerRound).toInt();
    if (boogerScore < 1 || boogersPerRound < 1) {
        return TournamentTemplate::defaultSettings;
    }

    // Get the players page columns
    QVector<RegistrationColumns::Column> registrationColumns;
    if (version >= 3) {
        registrationColumns = JsonHelper::parseRegistrationColumns(
                                  value(s_tournamentTemplate_registrationColumns).toString());
    } else if (version < 3) {
        const auto columns = Json::objectFromString(
            value(QStringLiteral("players_page/columns")).toString());
        if (columns.value(QStringLiteral("mark")).toBool()) {
            registrationColumns.append(RegistrationColumns::Mark);
        }
        if (columns.value(QStringLiteral("assignment")).toBool()) {
            // "assignment" has been renamed to "draw"
            registrationColumns.append(RegistrationColumns::Draw);
        }
        if (columns.value(QStringLiteral("marker")).toBool()) {
            registrationColumns.append(RegistrationColumns::Marker);
        }
    }

    // Assemble the template and return it
    return TournamentTemplate::Settings {
        tournamentMode,
        scoreType,
        boogerScore,
        boogersPerRound,
        registrationColumns,
        value(s_tournamentTemplate_registrationListHeaderHidden,
              TournamentTemplate::defaultSettings.registrationListHeaderHidden).toBool(),
        value(s_tournamentTemplate_autoSelectPairs,
              TournamentTemplate::defaultSettings.autoSelectPairs).toBool(),
        value(s_tournamentTemplate_selectByLastRound,
              TournamentTemplate::defaultSettings.selectByLastRound).toBool(),
        value(s_tournamentTemplate_autoSelectTableForDraw,
              TournamentTemplate::defaultSettings.autoSelectTableForDraw).toBool(),
        value(s_tournamentTemplate_includeOpponentGoals,
              TournamentTemplate::defaultSettings.includeOpponentGoals).toBool(),
        value(s_tournamentTemplate_grayOutUnimportantGoals,
              TournamentTemplate::defaultSettings.grayOutUnimportantGoals).toBool(),
        JsonHelper::parseOptionalPages(value(s_tournamentTemplate_optionalPages).toString()),
        drawSettingsTemplate(),
        scheduleSettingsTemplate()
    };
}

// Database file name template
// ---------------------------

void Settings::saveDbFileNameTemplate(const QString &text)
{
    setValue(s_tournamentTemplate_dbFileNameTemplate, text);
}

QString Settings::dbFileNameTemplate() const
{
    return value(s_tournamentTemplate_dbFileNameTemplate,
                 DefaultValues::dbFileNameTemplate).toString();
}

// Markers template
// ----------------

void Settings::saveMarkersTemplate(Tournament::Mode tournamentMode,
                                   const QVector<Markers::Marker> &markers)
{
    QJsonArray json;
    for (const auto &marker : markers) {
        const auto &markerToAppend = marker.id != 0 ? marker : Markers::unmarkedMarker;
        json.append(QJsonObject {
            { s_markersTemplate_nameKey,    markerToAppend.name },
            { s_markersTemplate_colorKey,   ColorHelper::colorToString(markerToAppend.color) },
            { s_markersTemplate_sortingKey, static_cast<int>(markerToAppend.sorting) }
        });
    }

    setValue(s_markersTemplate_versionKey.value(tournamentMode), s_markersTemplate_version);
    setValue(s_markersTemplate_markers.value(tournamentMode), Json::serialize(json));
}

bool Settings::parseMarkerData(int &id, const QJsonValue &data,
                               QVector<Markers::Marker> &markers) const
{
    Markers::Marker marker;
    const QJsonObject markerData = data.toObject();
    QJsonValue value;

    value = markerData.value(s_markersTemplate_nameKey);
    if (value.type() != QJsonValue::String) {
        return false;
    } else {
        marker.name = value.toString();
    }

    value = markerData.value(s_markersTemplate_colorKey);
    if (value.type() != QJsonValue::String) {
        return false;
    } else {
        marker.color = ColorHelper::colorFromString(value.toString());
    }

    value = markerData.value(s_markersTemplate_sortingKey);
    if (value.type() != QJsonValue::Double) {
        return false;
    } else {
        marker.sorting = static_cast<Markers::MarkerSorting>(value.toInt());
    }

    if (marker.name.isEmpty()) {
        marker.id = 0;
    } else {
        marker.id = ++id;
    }

    markers.append(marker);

    return true;
}

QVector<Markers::Marker> Settings::defaultMarkersTemplate(Tournament::Mode tournamentMode) const
{
    switch (tournamentMode) {
    case Tournament::FixedPairs:
        return QVector<Markers::Marker> {
            //                id    name                color                  sorting
            Markers::Marker { 3, tr("Auslosung fehlt"), QColor(0,   150, 0  ), Markers::NewBlock },
            Markers::Marker { 2, tr("Allein da"),       QColor(0,   120, 255), Markers::NewBlock },
            Markers::Marker { 1, tr("Vorangemeldet"),   QColor(255, 0,   0  ), Markers::NewBlock },
            Markers::unmarkedMarker
        };
    case Tournament::SinglePlayers:
        return QVector<Markers::Marker> {
            //                id    name                color                  sorting
            Markers::Marker { 2, tr("Auslosung fehlt"), QColor(0,   150, 0  ), Markers::NewBlock },
            Markers::Marker { 1, tr("Vorangemeldet"),   QColor(255, 0,   0  ), Markers::NewBlock },
            Markers::unmarkedMarker
        };
    default:
        // This is only here to make the compiler happy
        Q_UNREACHABLE();
        return { };
    }
}

QVector<Markers::Marker> Settings::markersTemplate(Tournament::Mode tournamentMode) const
{
    // Check useLegacy for a tournament mode specific template
    bool useLegacyMarkers = false;
    int version = value(s_markersTemplate_versionKey.value(tournamentMode), -1).toInt();

    if (version == -1) {
        // Check for the old universal template
        version = value(s_markersTemplate_legacyVersionKey, -1).toInt();

        if (version == -1) {
            qCDebug(MuckturnierLog) << "No markers template defined. Using the default one for"
                                    << (tournamentMode == Tournament::FixedPairs
                                            ? "fixed pairs." : "single players.");
            return defaultMarkersTemplate(tournamentMode);
        } else {
            useLegacyMarkers = true;
        }
    }

    bool versionOkay = true;
    QJsonArray data;

    if (! useLegacyMarkers) {
        // We want to load the a mode-specific markers template
        if (version == 2) {
            data = Json::arrayFromString(value(s_markersTemplate_markers.value(tournamentMode),
                                               QString()).toString());
        } else {
            versionOkay = false;
        }

    } else {
        // We want to load the legacy markers template
        qCDebug(MuckturnierLog) << "Using the legacy (mode-less) markers template";
        if (version == 2) {
            data = Json::arrayFromString(value(s_markersTemplate_legacyMarkers,
                                               QString()).toString());
        } else if (version == 1) {
            data = Json::objectFromString(QString::fromUtf8(
                value(QStringLiteral("markers_template/data"), QByteArray()).toByteArray())).value(
                      QStringLiteral("d")).toArray();
        } else {
            versionOkay = false;
        }
    }

    if (! versionOkay) {
        qCWarning(MuckturnierLog) << "Parsing the markers template failed: Invalid version:"
                                  << version
                                  << "- falling back to the default template for"
                                  << (tournamentMode == Tournament::FixedPairs
                                          ? "fixed pairs" : "single players");
        return defaultMarkersTemplate(tournamentMode);
    }

    if (data.isEmpty()) {
        qCWarning(MuckturnierLog) << "Parsing the markers template failed: Template is empty."
                                  << "- falling back to the default template for"
                                  << (tournamentMode == Tournament::FixedPairs
                                          ? "fixed pairs" : "single players");
        return defaultMarkersTemplate(tournamentMode);
    }

    QVector<Markers::Marker> markers;

    bool templateParsed = true;
    int id = 0;

    for (const QJsonValue &value : std::as_const(data)) {
        if (! parseMarkerData(id, value, markers)) {
            templateParsed = false;
            break;
        }
    }

    // Be sure to have an "(unmarked)" marker with ID 0 and no name
    if (templateParsed) {
        bool unmarkedPresent = true;
        for (const Markers::Marker &marker : std::as_const(markers)) {
            if (marker.id == 0 && marker.name.isEmpty()) {
                break;
            }
        }
        if (! unmarkedPresent) {
            qCWarning(MuckturnierLog) << "The saved markers template does not contain an"
                                      << "\"unmarked\" marker! This should not happen!";
            templateParsed = false;
        }
    }

    if (! templateParsed) {
        qCWarning(MuckturnierLog) << "Parsing the saved markers template failed, falling back to"
                                  << "the default one";
        return defaultMarkersTemplate(tournamentMode);
    }

    return markers;
}

QHash<Markers::Type, int> Settings::specialMarkers() const
{
    QHash<Markers::Type, int> markers;
    for (const auto type : Markers::specialMarkers) {
        markers[type] = marker(type);
    }
    return markers;
}

void Settings::saveMarker(Markers::Type type, int id)
{
    setValue(s_markersTemplate_markerKey.value(type), id);
}

void Settings::saveMarkers(const QHash<Markers::Type, int> &markers)
{
    QHash<Markers::Type, int>::const_iterator it;
    for (it = markers.constBegin(); it != markers.constEnd(); it++) {
        saveMarker(it.key(), it.value());
    }
}

int Settings::marker(Markers::Type type) const
{
    return value(s_markersTemplate_markerKey.value(type),
                 Markers::defaultSpecialMarkerValues.value(type)).toInt();
}

// Singles Management
// ------------------

void Settings::saveSinglesManagementTemplate(const Markers::SinglesManagement &data)
{
    setValue(s_singlesManagementTemplate_version, Markers::singlesManagementVersion);
    setValue(s_singlesManagementTemplate_data,    JsonHelper::singlesManagementToString(data));
}

Markers::SinglesManagement Settings::defaultSinglesManagementTemplate() const
{
    Markers::SinglesManagement data = Markers::defaultSinglesManagement;
    data.conditionString = namesSeparator().simplified();
    return data;
}

Markers::SinglesManagement Settings::singlesManagementTemplate() const
{
    if (value(s_singlesManagementTemplate_version, 0).toInt()
        != Markers::singlesManagementVersion) {

        return defaultSinglesManagementTemplate();
    }

    return JsonHelper::parseSinglesManagement(value(s_singlesManagementTemplate_data).toString());
}

// Strings
// -------

void Settings::saveBoogerSymbol(const QString &text)
{
    setValue(s_strings_boogerSymbol, text);
    m_boogerSymbol = text;
}

const QString &Settings::boogerSymbol() const
{
    return m_boogerSymbol;
}

void Settings::saveNamesSeparator(const QString &text)
{
    setValue(s_strings_namesSeparator, text);
    m_namesSeparator = text;
}

void Settings::setTransientNamesSeparator(const QString &text)
{
    if (m_namesSeparator != text) {
        m_originalNamesSeparator = m_namesSeparator;
        m_namesSeparator = text;
    } else {
        m_originalNamesSeparator.clear();
    }
}

bool Settings::resetNamesSeparator()
{
    if (! m_originalNamesSeparator.isEmpty()) {
        m_namesSeparator = m_originalNamesSeparator;
        m_originalNamesSeparator.clear();
        return true;
    } else {
        return false;
    }
}

const QString &Settings::namesSeparator() const
{
    return m_namesSeparator;
}

// Network
// -------

void Settings::saveServerPort(int port)
{
    setValue(s_network_serverPort, port);
}

int Settings::serverPort() const
{
    const int port = value(s_network_serverPort, DefaultValues::serverPort).toInt();
    return (port >= 1024 && port <= 65535) ? port : DefaultValues::serverPort;
}

void Settings::saveConnectTimeout(int timeout)
{
    setValue(s_network_connectTimeout, timeout);
}

int Settings::connectTimeout() const
{
    return value(s_network_connectTimeout, DefaultValues::connectTimeout).toInt();
}

void Settings::saveRequestTimeout(int timeout)
{
    setValue(s_network_requestTimeout, timeout);
}

int Settings::requestTimeout() const
{
    return value(s_network_requestTimeout, DefaultValues::requestTimeout).toInt();
}

void Settings::saveDiscoverPort(int port)
{
    setValue(s_network_discoverPort, port);
}

int Settings::discoverPort() const
{
    const int port = value(s_network_discoverPort, DefaultValues::discoverServerPort).toInt();
    return (port >= 1024 && port <= 65535) ? port : DefaultValues::discoverServerPort;
}

void Settings::saveDiscoverTimeout(int timeout)
{
    setValue(s_network_discoverTimeout, timeout);
}

int Settings::discoverTimeout() const
{
    return value(s_network_discoverTimeout, DefaultValues::discoverTimeout).toInt();
}

void Settings::saveDiscoverAttempts(int attempts)
{
    setValue(s_network_discoverAttempts, attempts);
}

int Settings::discoverAttempts() const
{
    return value(s_network_discoverAttempts, DefaultValues::discoverAttempts).toInt();
}

void Settings::savePreferIpv6(bool state)
{
    setValue(s_network_preferIpv6, state);
}

bool Settings::preferIpv6() const
{
    return value(s_network_preferIpv6, false).toBool();
}

void Settings::saveWlanCodeScannerServerPort(int port)
{
    setValue(s_network_wlanCodeScannerServerPort, port);
}

int Settings::wlanCodeScannerServerPort() const
{
    const int port = value(s_network_wlanCodeScannerServerPort,
                           DefaultValues::wlanCodeScannerServerPort).toInt();
    return (port >= 1024 && port <= 65535) ? port : DefaultValues::wlanCodeScannerServerPort;
}

void Settings::saveWlanCodeScannerServerKey(const QString &key)
{
    setValue(s_network_wlanCodeScannerServerKey, key);
}

QString Settings::wlanCodeScannerServerKey() const
{
    return value(s_network_wlanCodeScannerServerKey,
                 DefaultValues::wlanCodeScannerServerKey).toString();
}

// Draw settings

void Settings::saveDrawSettingsTemplate(const Draw::Settings &data)
{
    setValue(s_drawSettingsTemplate_version, Draw::settingsVersion);
    setValue(s_drawSettingsTemplate_data,    JsonHelper::drawSettingsToString(data));
}

Draw::Settings Settings::drawSettingsTemplate() const
{
    const auto version = value(s_drawSettingsTemplate_version, -1).toInt();
    if (version == -1) {
        // There's no template stored. We use the default settings
        qCDebug(MuckturnierLog) << "No draw settings template defined. Using the defaults.";
        return Draw::defaultSettings;
    }

    return JsonHelper::parseDrawSettings(version, value(s_drawSettingsTemplate_data).toString());
}

// Dismissable messages

void Settings::saveMessageDismissed(const QString &id, bool state)
{
    setValue(s_dismissedMessages + id, state);
}

void Settings::showAllDismissedMessages()
{
    remove(s_dismissedMessages);
}

bool Settings::messageDismissed(const QString &id)
{
    return value(s_dismissedMessages + id, false).toBool();
}

// Booking

void Settings::saveBookingCodesDir(const QString &dir)
{
    if (saveOnCloseEnabled(SaveOnCloseOption::SaveBookingCodesDir)) {
        setValue(s_bookingSettings_codesDir, dir);
    }
}

QString Settings::bookingCodesDir() const
{
    return value(s_bookingSettings_codesDir, QString()).toString();
}

void Settings::saveBookingCodesScaleFactor(int factor)
{
    if (saveOnCloseEnabled(SaveOnCloseOption::SaveBookingCodesScaleFactor)) {
        setValue(s_bookingSettings_codesScaleFactor, factor);
    }
}

int Settings::bookingCodesScaleFactor() const
{
    return value(s_bookingSettings_codesScaleFactor, 5).toInt();
}

QVector<double> Settings::defaultNotesSpacing(NoteType::Type type) const
{
    switch (type) {
    case NoteType::TableNumbers:
        return QVector<double> { 2.5, 19.0, 0.0, -5.7 };
    case NoteType::DrawNotes:
        return QVector<double> { -2.0, -2.0, 4.5, 3.0 };
    case NoteType::ScoreNotes1Booger:
        return QVector<double> { -3.0, -3.5, 6.0, 4.0 };
    case NoteType::ScoreNotes:
        return QVector<double> { -3.0, -3.5, 6.0, 4.0 };
    case NoteType::ScoreNotes3Boogers:
        return QVector<double> { -0.5, -4.0, 0.0, 4.0 };
    case NoteType::Shared:
        // This should not happen
        qCWarning(MuckturnierLog, "Requested notes spacing for \"shared\"!");
        return QVector<double> { 0.0, 0.0, 0.0, 0.0 };
    }

    // We can't reach here, but the compiler thinks we can
    return QVector<double> { 0.0, 0.0, 0.0, 0.0 };
}

QVector<double> Settings::notesSpacing(NoteType::Type type) const
{
    const auto spacing = value(s_notesSpacing_spacing.arg(s_noteTypeStrings.value(type)),
                               QString()).toString();

    if (! spacing.isEmpty()) {
        QVector<double> parsedSpacing;
        const auto values = spacing.split(QStringLiteral(","));
        for (const auto &value : values) {
            parsedSpacing.append(value.toDouble());
        }

        if (parsedSpacing.count() == 4) {
            return parsedSpacing;
        }
    }

    // Otherwise return the default values
    return defaultNotesSpacing(type);
}

void Settings::saveNotesSpacing(NoteType::Type type, const QVector<double> &values)
{
    QString serialized;
    for (int i = 0; i < 4; i++) {
        if (i > 0) {
            serialized.append(QLatin1String(","));
        }
        serialized.append(QString::number(values.at(i), 'f', 1));
    }

    setValue(s_notesSpacing_spacing.arg(s_noteTypeStrings.value(type)), serialized);
}

QString Settings::notesSettings(NoteType::Type type) const
{
    return value(s_notesSettings_setting.arg(s_noteTypeStrings.value(type)), QString()).toString();
}

void Settings::saveNotesSettings(NoteType::Type type, const QString &settings)
{
    if (saveOnCloseEnabled(SaveOnCloseOption::SaveNotesSettings)) {
        setValue(s_notesSettings_setting.arg(s_noteTypeStrings.value(type)), settings);
    }
}

int Settings::scoreNotesBoogers() const
{
    return value(s_notesSettings_scoreNotesBoogers, 2).toInt();
}

void Settings::saveScoreNotesBoogers(int boogers)
{
    setValue(s_notesSettings_scoreNotesBoogers, boogers);
}
