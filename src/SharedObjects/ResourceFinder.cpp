// SPDX-FileCopyrightText: 2018-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "ResourceFinder.h"

#include "shared/Logging.h"

// Qt includes
#include <QDebug>
#include <QFileInfo>
#include <QStandardPaths>
#include <QCoreApplication>
#include <QUrl>
#include <QDir>
#include <QFileInfo>

// C++ includes
#include <utility>

static const QLatin1String s_slash("/");

ResourceFinder::ResourceFinder(QObject *parent) : QObject(parent)
{
    QVector<QString> possibleLocations;
    const auto appDir = QCoreApplication::applicationDirPath();

#if defined(Q_OS_LINUX) || defined(Q_OS_FREEBSD)
    const QLatin1String resourcesPath("muckturnier/");

    // Add a possibly prefixed directory tree
    const auto prefixed = QUrl(appDir + s_slash).resolved(
                              QUrl(QStringLiteral("../share/"))).toString();
    possibleLocations.append(prefixed);
    possibleLocations.append(prefixed + resourcesPath);

    // Add system-wide locations
    const auto genericDataLocation = QStandardPaths::standardLocations(
                                         QStandardPaths::GenericDataLocation);
    for (const QString &path : genericDataLocation) {
        possibleLocations.append(path + s_slash);
        possibleLocations.append(path + s_slash + resourcesPath);
    }
#elif defined(Q_OS_WIN)
    possibleLocations.append(appDir + QLatin1String("/res/"));
#elif defined(Q_OS_MACOS)
    possibleLocations.append(QUrl(appDir + s_slash).resolved(
                                 QUrl(QStringLiteral("../Resources/"))).toString());
#else
    qCWarning(MuckturnierLog) << "No custom resource locations are defined for your OS."
                              << "Resource locating may not work correctly!"
#endif

    // Assemble the list
    const QDir dir;
    for (const QString &location : std::as_const(possibleLocations)) {
        if (dir.exists(location) && ! m_possibleLocations.contains(location)) {
            m_possibleLocations.append(location);
        }
    }
}

QString ResourceFinder::find(const QString &file)
{
    // If we're in dark mode, we first check if we have a matching dark resource

    if (qApp->property("DARK_MODE").toBool()) {
        QFileInfo info(file);
        const QString suffix = info.suffix();

        if (suffix == QStringLiteral("png")) {
            const QString darkPath = locate(info.completeBaseName() + QLatin1String("_dark.png"));
            if (! darkPath.isEmpty()) {
                return darkPath;
            }
        }
    }

    return locate(file);
}

QString ResourceFinder::locate(const QString &file)
{
    // Check if we already have it
    if (m_path.contains(file)) {
        return m_path.value(file);
    }

    // ... then we check all other possible data locations
    for (const QString &path : std::as_const(m_possibleLocations)) {
        const QString filePath = path + file;
        if (QFileInfo::exists(filePath)) {
            m_path[file] = filePath;
            return filePath;
        }
    }

    // This will also return QString() if nothing is found
    return m_path[file];
}
