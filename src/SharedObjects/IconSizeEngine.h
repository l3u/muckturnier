// SPDX-FileCopyrightText: 2019-2022 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef ICONSIZEENGINE_H
#define ICONSIZEENGINE_H

// Qt includes
#include <QObject>
#include <QSize>

class IconSizeEngine : public QObject
{
    Q_OBJECT

public:
    explicit IconSizeEngine(QObject *parent);
    QSize relativeSize(double factor) const;
    int relativeWidth(double factor) const;

private:
    double m_MHeight;

};

#endif // ICONSIZEENGINE_H
