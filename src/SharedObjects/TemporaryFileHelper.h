// SPDX-FileCopyrightText: 2022-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef TEMPORARYFILEHELPER_H
#define TEMPORARYFILEHELPER_H

// Qt includes
#include <QObject>

// Qt classes
class QTemporaryDir;

class TemporaryFileHelper : public QObject
{
    Q_OBJECT

public:
    enum CreateResult {
        FileCreated,
        CreateFileFailed,
        CreateDirFailed
    };

    struct NewFileResult
    {
        CreateResult result;
        QString fileName;
    };

    explicit TemporaryFileHelper(QObject *parent);
    ~TemporaryFileHelper();
    QString initialize();
    NewFileResult getFileName(const QString &fileName);
    void displayErrorMessage(QWidget *parent, CreateResult createResult) const;

private: // Variables
    QTemporaryDir *m_tmpDir = nullptr;
    QString m_tmpDirPath;

};

#endif // TEMPORARYFILEHELPER_H
