// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SHAREDOBJECTS_H
#define SHAREDOBJECTS_H

// Qt includes
#include <QObject>

// Local classes
class StringTools;
class SearchEngine;
class Database;
class ResourceFinder;
class Settings;
class TournamentSettings;
class DismissableMessage;
class IconSizeEngine;
class TemporaryFileHelper;
class BookingEngine;

class SharedObjects : public QObject
{
    Q_OBJECT

public:
    explicit SharedObjects(QObject *parent = nullptr);
    StringTools *stringTools() const;
    SearchEngine *searchEngine() const;
    ResourceFinder *resourceFinder() const;
    Database *database() const;
    Settings *settings() const;
    TournamentSettings *tournamentSettings() const;
    DismissableMessage *dismissableMessage() const;
    IconSizeEngine *iconSizeEngine() const;
    TemporaryFileHelper *temporaryFileHelper() const;
    BookingEngine *bookingEngine() const;

private: // Variables
    StringTools *m_stringTools;
    SearchEngine *m_searchEngine;
    Database *m_database;
    ResourceFinder *m_resourceFinder;
    Settings *m_settings;
    TournamentSettings *m_tournamentSettings;
    DismissableMessage *m_dismissableMessage;
    IconSizeEngine *m_iconSizeEngine;
    TemporaryFileHelper *m_temporaryFileHelper;
    BookingEngine *m_bookingEngine;

};

#endif // SHAREDOBJECTS_H
