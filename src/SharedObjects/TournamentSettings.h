// SPDX-FileCopyrightText: 2019-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef TOURNAMENTSETTINGS_H
#define TOURNAMENTSETTINGS_H

// Local includes
#include "shared/Tournament.h"
#include "shared/Markers.h"
#include "shared/RegistrationColumns.h"
#include "shared/Draw.h"
#include "shared/OptionalPages.h"
#include "shared/Schedule.h"
#include "shared/Booking.h"

// Qt includes
#include <QObject>
#include <QJsonValue>

class TournamentSettings : public QObject
{
    Q_OBJECT

public:
    explicit TournamentSettings(QObject *parent);
    int version() const;
    void setLoadingDb(bool state);
    bool loadingDb() const;
    bool load(const QString &data);
    QString toString() const;

    // Value access functions

    void setTournamentMode(Tournament::Mode tournamentMode);
    Tournament::Mode tournamentMode() const;
    bool isPairMode() const;
    bool isSinglePlayerMode() const;

    void setScoreType(Tournament::ScoreType type);
    Tournament::ScoreType scoreType() const;

    void setBoogerScore(int score);
    const int &boogerScore() const;

    void setBoogersPerRound(int count);
    int boogersPerRound() const;

    void setDrawSettings(const Draw::Settings &settings);
    const Draw::Settings &drawSettings() const;

    void setScheduleSettings(const Schedule::Settings &settings);
    const Schedule::Settings &scheduleSettings() const;

    void setRegistrationColumns(const QVector<RegistrationColumns::Column> &data);
    const QVector<RegistrationColumns::Column> &registrationColumns() const;

    void setRegistrationListHeaderHidden(bool state);
    bool registrationListHeaderHidden() const;

    void setDrawModeParameters(bool autoSelectPairs, bool selectByLastRound);

    void setAutoSelectPairs(bool state);
    bool autoSelectPairs() const;

    void setSelectByLastRound(bool state);
    bool selectByLastRound() const;

    void setAutoSelectTableForDraw(bool state);
    bool autoSelectTableForDraw() const;

    void setIncludeOpponentGoals(bool state);
    bool includeOpponentGoals() const;

    void setGrayOutUnimportantGoals(bool state);
    bool grayOutUnimportantGoals() const;

    const QVector<OptionalPages::Page> &optionalPages() const;

    void setMuckturnierVersion(const QString &muckturnierVersion);
    const QString &muckturnierVersion() const;

    void setTournamentOpen(bool state);
    bool tournamentOpen() const;
    void initializeTournamentOpen();
    bool tournamentOpenChanged() const;

    void setSpecialMarkers(const QHash<Markers::Type, int> &markers);
    int specialMarker(Markers::Type type) const;
    QHash<Markers::Type, int> specialMarkers() const;

    void setSinglesManagement(const Markers::SinglesManagement &data);
    const Markers::SinglesManagement &singlesManagement() const;

    void setBookingSettings(const Booking::Settings &settings);
    const Booking::Settings &bookingSettings() const;
    void resetBookingSettings();
    void setBookingEnabled(bool state);
    bool bookingEnabled() const;

Q_SIGNALS:
    void bookingEnabledChanged(bool state);
    void drawModeChanged();
    void drawSettingsChanged();

public Q_SLOTS:
    void setOptionalPageShown(OptionalPages::Page page, bool state);
    void setSpecialMarker(Markers::Type type, int id);

private: // Functions
    void reset();
    QVariant getValue(const QJsonObject &json, const QLatin1String &key,
                      QJsonValue::Type type) const;

private: // Variables

    // Values represented in the tournament template
    Tournament::Mode m_tournamentMode;
    Tournament::ScoreType m_scoreType;
    int m_boogerScore;
    int m_boogersPerRound;
    Draw::Settings m_drawSettings;
    Schedule::Settings m_scheduleSettings;
    Booking::Settings m_bookingSettings;
    bool m_bookingEnabled = false;
    QVector<RegistrationColumns::Column> m_registrationColumns;
    bool m_registrationListHeaderHidden;
    bool m_autoSelectPairs;
    bool m_selectByLastRound;
    bool m_autoSelectTableForDraw;
    bool m_includeOpponentGoals;
    bool m_grayOutUnimportantGoals;
    QVector<OptionalPages::Page> m_optionalPages;

    // Database-read values with fallback values
    QString m_muckturnierVersion;
    bool m_tournamentOpen;
    bool m_originalTournamentOpen;
    QHash<Markers::Type, int> m_marker;
    Markers::SinglesManagement m_singlesManagement;

    // Other variables needed by the class itself
    bool m_loadingDb = false;

};

#endif // TOURNAMENTSETTINGS_H
