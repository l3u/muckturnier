// SPDX-FileCopyrightText: 2022-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "TemporaryFileHelper.h"

#include "shared/Urls.h"

// Qt includes
#include <QDebug>
#include <QTemporaryDir>
#include <QFileInfo>
#include <QFile>
#include <QMessageBox>
#include <QDir>

TemporaryFileHelper::TemporaryFileHelper(QObject *parent) : QObject(parent)
{
}

TemporaryFileHelper::~TemporaryFileHelper()
{
    if (m_tmpDir != nullptr) {
        delete m_tmpDir;
    }
}

QString TemporaryFileHelper::initialize()
{
    if (m_tmpDir == nullptr) {
        m_tmpDir = new QTemporaryDir;
        m_tmpDirPath = m_tmpDir->path() + QStringLiteral("/");
    }

    return m_tmpDir->isValid() ? m_tmpDirPath : QString();
}

TemporaryFileHelper::NewFileResult TemporaryFileHelper::getFileName(const QString &fileName)
{
    // Setup the QTemporaryDir and check if it's valid
    initialize();
    if (! m_tmpDir->isValid()) {
        return { CreateResult::CreateDirFailed, QString() };
    }

    // Find a suitable file name

    QFileInfo info(fileName);
    const auto baseName = info.baseName();
    const auto suffix = info.completeSuffix();

    QString tmpFileName(m_tmpDirPath + fileName);
    int number = 0;

    while (QFileInfo::exists(tmpFileName)) {
        tmpFileName = QStringLiteral("%1%2_%3.%4").arg(m_tmpDirPath, baseName,
                                                       QString::number(++number), suffix);
    }

    // Create the file
    QFile file(tmpFileName);
    if (! file.open(QIODevice::WriteOnly)) {
        return { CreateResult::CreateFileFailed, QString() };
    }
    file.close();

    return { CreateResult::FileCreated, tmpFileName };
}

void TemporaryFileHelper::displayErrorMessage(QWidget *parent,
                                              TemporaryFileHelper::CreateResult createResult) const
{
    switch (createResult) {
    case CreateResult::FileCreated:
        return;
    case CreateResult::CreateFileFailed:
        QMessageBox::critical(parent,
            tr("Anlegen einer temporären Datei fehlgeschlagen"),
            tr("<p>Es konnte keine temporäre Datei im Verzeichnis „%1“ angelegt werden. Bitte "
               "die Zugriffsrechte übrprüfen!</p>"
               "<p>Das sollte nicht passieren. Bitte die Zugriffsrechte des aktuellen Benutzers "
               "überprüfen! Ggf. auch einen Bugreport mit so vielen Informationen "
               "wie möglich auf <a href=\"%2\">%2</a> anlegen!</p>"
            ).arg(QDir::toNativeSeparators(m_tmpDirPath).toHtmlEscaped(), Urls::bugtracker));
        break;
    case CreateResult::CreateDirFailed:
        QMessageBox::critical(parent,
            tr("Anlegen einer temporären Datei fehlgeschlagen"),
            tr("<p>Es konnte keine temporäres Verzeichnis erstellt werden, und deswegen können "
               "keine temporären Dateien erzeugt werden.</p>"
               "<p>Das sollte nicht passieren. Bitte die Zugriffsrechte des aktuellen Benutzers "
               "überprüfen! Ggf. auch einen Bugreport mit so vielen Informationen "
               "wie möglich auf <a href=\"%1\">%1</a> anlegen!</p>").arg(Urls::bugtracker));
            break;
    }
}
