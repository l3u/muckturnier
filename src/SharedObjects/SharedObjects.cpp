// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "SharedObjects.h"

#include "Database/Database.h"

#include "StringTools.h"
#include "SearchEngine.h"
#include "ResourceFinder.h"
#include "Settings.h"
#include "TournamentSettings.h"
#include "DismissableMessage.h"
#include "IconSizeEngine.h"
#include "TemporaryFileHelper.h"

#include "BookingPage/BookingEngine.h"

SharedObjects::SharedObjects(QObject *parent) : QObject(parent)
{
    m_settings = new Settings(this);
    m_stringTools = new StringTools(this, &m_settings->boogerSymbol(),
                                    &m_settings->namesSeparator());
    m_searchEngine = new SearchEngine(this, m_stringTools);
    m_tournamentSettings = new TournamentSettings(this);
    m_database = new Database(this, this);
    m_resourceFinder = new ResourceFinder(this);
    m_dismissableMessage = new DismissableMessage(this, m_settings);
    m_iconSizeEngine = new IconSizeEngine(this);
    m_temporaryFileHelper = new TemporaryFileHelper(this);
    m_bookingEngine = new BookingEngine(this, m_tournamentSettings);
}

StringTools *SharedObjects::stringTools() const
{
    return m_stringTools;
}

SearchEngine *SharedObjects::searchEngine() const
{
    return m_searchEngine;
}

Database *SharedObjects::database() const
{
    return m_database;
}

ResourceFinder *SharedObjects::resourceFinder() const
{
    return m_resourceFinder;
}

Settings *SharedObjects::settings() const
{
    return m_settings;
}

TournamentSettings *SharedObjects::tournamentSettings() const
{
    return m_tournamentSettings;
}

DismissableMessage *SharedObjects::dismissableMessage() const
{
    return m_dismissableMessage;
}

IconSizeEngine *SharedObjects::iconSizeEngine() const
{
    return m_iconSizeEngine;
}

TemporaryFileHelper *SharedObjects::temporaryFileHelper() const
{
    return m_temporaryFileHelper;
}

BookingEngine *SharedObjects::bookingEngine() const
{
    return m_bookingEngine;
}
