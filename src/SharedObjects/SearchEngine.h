// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SEARCHENGINE_H
#define SEARCHENGINE_H

// Qt includes
#include <QObject>
#include <QHash>
#include <QVector>

// Local classes
class StringTools;

class SearchEngine : public QObject
{
    Q_OBJECT

public:
    enum SearchType {
        DefaultSearch,
        DrawnPairsScoreSearch,
        DrawnPairsDrawSearch
    };

    explicit SearchEngine(QObject *parent, StringTools *stringTools);
    bool checkMatch(const QString &text, SearchType searchType, bool phoneticSearch) const;
    void clearSearchCache(SearchType searchType = SearchType::DefaultSearch);
    void updateSearchCache(const QString &name, SearchType searchType = SearchType::DefaultSearch);
    void setSearchTerm(const QString &text);
    QHash<QString, QStringList> checkListForPossibleDuplicates(const QStringList &list,
                                                               bool phoneticSearch);

private: // Functions
    QVector<QString> searchVariants(const QString &text);
    bool checkMatch(const QVector<QString> &variantList, bool phoneticSearch) const;
    QString removeSequentialCharacters(QString text) const;
    QString koelnerPhonetik(const QString &text) const;
    void setSearchTerm(const QVector<QString> &variantsList);

private: // Variables
    StringTools *m_stringTools;
    const QHash<SearchType, QHash<QString, QVector<QString>> *> m_variantsCache;
    QHash<QString, QVector<QString>> m_defaultVariants;
    QHash<QString, QVector<QString>> m_drawnPairsScoreVariants;
    QHash<QString, QVector<QString>> m_drawnPairsDrawVariants;
    QVector<QHash<QString, int>> m_searchTerm;

};

#endif // SEARCHENGINE_H
