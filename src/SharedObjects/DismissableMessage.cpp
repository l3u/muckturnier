// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "DismissableMessage.h"
#include "Settings.h"

#include "shared/Logging.h"

// Qt includes
#include <QCheckBox>

static const QHash<DismissableMessage::GlobalMessage, QString> s_globalMessageMap {
    { DismissableMessage::UnDismissHint, QStringLiteral("un_dismiss_hint") },
    { DismissableMessage::DrawHint,      QStringLiteral("draw_hint") }
};

DismissableMessage::DismissableMessage(QObject *parent, Settings *settings)
    : QObject(parent),
      m_settings(settings)
{
}

bool DismissableMessage::dismissed(DismissableMessage::SessionMessage id) const
{
    return m_dismissed.value(id, false);
}

bool DismissableMessage::dismissed(GlobalMessage id) const
{
    return m_settings->messageDismissed(s_globalMessageMap.value(id));
}

void DismissableMessage::setDismissed(DismissableMessage::SessionMessage id, bool state)
{
    m_dismissed[id] = state;
}

void DismissableMessage::setDismissed(GlobalMessage id, bool state)
{
    m_settings->saveMessageDismissed(s_globalMessageMap.value(id), state);
}

QMessageBox::StandardButton DismissableMessage::question(
    DismissableMessage::SessionMessage id, QWidget *parent, const QString &title,
    const QString &text, QMessageBox::StandardButton defaultButton)
{
    if (id == SessionMessage::NonExistingFallback) {
        qCWarning(MuckturnierLog) << "DismissableMessage::question: Invalid message ID given."
                                     "Skipping the message and returning QMessageBox::NoButton";
        return QMessageBox::NoButton;
    }

    auto *dismissed = new QCheckBox(tr("In dieser Sitzung nicht mehr nachfragen"));
    QMessageBox messageBox(parent);
    messageBox.setIcon(QMessageBox::Question);
    messageBox.setWindowTitle(title);
    messageBox.setText(text);
    messageBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    messageBox.setDefaultButton(defaultButton);
    messageBox.setCheckBox(dismissed);

    const auto result = static_cast<QMessageBox::StandardButton>(messageBox.exec());
    m_dismissed[id] = dismissed->isChecked();

    return result;
}

void DismissableMessage::info(GlobalMessage id, QWidget *parent,
                              const QString &title, const QString &text)
{
    auto *dismissedCheckBox = new QCheckBox(tr("Nicht mehr anzeigen"));
    QMessageBox messageBox(parent);
    messageBox.setIcon(QMessageBox::Information);
    messageBox.setWindowTitle(title);
    messageBox.setText(text);
    messageBox.setCheckBox(dismissedCheckBox);

    messageBox.exec();
    const auto dismissedSelected = dismissedCheckBox->isChecked();
    setDismissed(id, dismissedSelected);

    if (dismissedSelected && ! dismissed(DismissableMessage::UnDismissHint)) {
        info(DismissableMessage::UnDismissHint, parent,
             tr("Infos ausblenden"),
             tr("Alle ausgeblendeten Nachrichten können über „Extras“ → „Alle Infos wieder "
                "anzeigen“ wieder angezeigt werden."));
    }
}
