// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "StringTools.h"

#include "shared/Players.h"

// Qt includes
#include <QDebug>
#include <QCollator>

static const QVector<QChar> s_umlautsToReplace {
    QChar(0x00e4), // ä
    QChar(0x00f6), // ö
    QChar(0x00fc), // ü
    QChar(0x00E6), // æ
    QChar(0x0153), // œ
    QChar(0x00F8)  // ø
};

static const QVector<QString> s_umlautReplacements {
    QStringLiteral("ae"),
    QStringLiteral("oe"),
    QStringLiteral("ue"),
    QStringLiteral("ae"),
    QStringLiteral("oe"),
    QStringLiteral("oe")
};

StringTools::StringTools(QObject *parent, const QString *boogerSymbol,
                         const QString *namesSeparator)
    : QObject(parent),
      m_boogerSymbol(boogerSymbol),
      m_namesSeparator(namesSeparator)
{
    m_locale = QLocale::system();
    m_collator.setLocale(m_locale);
    m_collator.setCaseSensitivity(Qt::CaseInsensitive);
    m_collator.setNumericMode(true);
}

QString StringTools::toLower(const QString &text) const
{
    return m_locale.toLower(text);
}

QString StringTools::toUpper(const QString &text) const
{
    return m_locale.toUpper(text);
}

QString StringTools::replaceUmlauts(QString text) const
{
    for (int i = 0; i < s_umlautsToReplace.size(); i++) {
        text.replace(s_umlautsToReplace.at(i), s_umlautReplacements.at(i));
    }
    return text;
}

QString StringTools::stripDiacritics(const QString &text) const
{
    // Strip out all diacritical marks and accents by first separating the combinig characters
    // and then removing them, cf. http://en.wikipedia.org/wiki/Combining_character
    QString replaced = text.normalized(QString::NormalizationForm_D);
    for (int i = 0x0300; i <= 0x036F; i++) {
        replaced.remove(QChar(i));
    }
    return replaced;
}

void StringTools::setBoogerScore(int score)
{
    m_boogerScore = score;
}

QString StringTools::boogify(int score) const
{
    return score == m_boogerScore ? QString::number(score)
                                  : QStringLiteral("%1 %2").arg(QString::number(score),
                                                                *m_boogerSymbol);
}

QString StringTools::pairOrPlayerName(const Players::DisplayData &data,
                                      StringTools::NameFormat format) const
{
    if (format == NameFormat::FormattedName) {
        if (data.number <= 0) {
            if (! data.disqualified) {
                return data.name;
            } else {
                return pairOrPlayerName(data, NameFormat::DisqualifiedName);
            }
        } else {
            if (! data.disqualified) {
                return tr("%1 (#%2)").arg(data.name, QString::number(data.number));
            } else {
                return tr("%1 (#%2, disqualifiziert)").arg(data.name, QString::number(data.number));
            }
        }
    } else if (format == NameFormat::DisqualifiedName && data.disqualified) {
        return tr("%1 (disqualifiziert)").arg(data.name);
    } else {
        return data.name;
    }
}

QString StringTools::pairName(const Players::DisplayData &player1,
                              const Players::DisplayData &player2,
                              StringTools::NameFormat format) const
{
    return tr("%1%2%3").arg(pairOrPlayerName(player1, format),
                            *m_namesSeparator,
                            pairOrPlayerName(player2, format));
}

QString StringTools::normalizedPairName(const Players::DisplayData &player1,
                                        const Players::DisplayData &player2) const
{
    const auto &name1 = player1.name;
    const auto &name2 = player2.name;
    if (name1 <= name2) {
        return QStringLiteral("%1 %2").arg(name1, name2);
    } else {
        return QStringLiteral("%1 %2").arg(name2, name1);
    }
}

QString StringTools::locale() const
{
    return m_locale.name();
}

QString StringTools::defaultLocale() const
{
    return QLocale::system().name();
}

void StringTools::setLocale(const QString &name)
{
    m_locale = QLocale(name);
    m_collator.setLocale(m_locale);
    m_localeChanged = true;
}

bool StringTools::resetLocale()
{
    if (! m_localeChanged) {
        return false;
    }

    m_locale = QLocale::system();
    m_collator.setLocale(m_locale);
    m_localeChanged = false;
    return true;
}

bool StringTools::localeChanged() const
{
    return m_localeChanged;
}

QString StringTools::dateFormat() const
{
    QString dateFormat = m_locale.dateFormat(QLocale::ShortFormat);
    return dateFormat.contains(QStringLiteral("yyyy"))
        ? dateFormat
        : dateFormat.replace(QStringLiteral("yy"), QStringLiteral("yyyy"));
}

QString StringTools::timeFormat() const
{
    return m_locale.timeFormat(QLocale::ShortFormat);
}
