// SPDX-FileCopyrightText: 2019-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "IconSizeEngine.h"

#include "shared/Logging.h"

// Qt includes
#include <QWidget>
#include <QFont>
#include <QFontMetrics>
#include <QDebug>

// C++ includes
#include <cmath>

IconSizeEngine::IconSizeEngine(QObject *parent) : QObject(parent)
{
    // The following calculation should set m_MHeight to 20(px) if the default
    // font size and family is used. We use os-dependant factors to achieve this.

    QWidget widget;
    const auto metrics = QFontMetrics(widget.font());
    m_MHeight = metrics.boundingRect(QStringLiteral("M")).height()
#if defined(Q_OS_LINUX)
                    * 1.17647;
#elif defined(Q_OS_WIN)
                    * 1.54836;
#elif defined(Q_OS_MACOS) || defined(Q_OS_FREEBSD)
                    * 1.33333;
#else
                    * 1.5;
    qCWarning(MuckturnierLog) << "No icon scale factor set for your OS - falling back to 1.5!"
                              << "Icon size may not be okay!"
#endif

    // If the user's desktop defines another font size, the icons should be scaled accordingly.
}

QSize IconSizeEngine::relativeSize(double factor) const
{
    const int size = relativeWidth(factor);
    return QSize(size, size);
}

int IconSizeEngine::relativeWidth(double factor) const
{
    return int(std::round(m_MHeight * factor));
}
