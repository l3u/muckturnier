// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

// =================================================================================================
// CAUTION: This class must not use any shared headers, enums etc. but the literal strings
// and values instead, as all this can change. So only rely on stuff that *won't* change.
// =================================================================================================

#include "DatabaseUpdater.h"

#include "shared/Json.h"

// Qt includes
#include <QSqlQuery>
#include <QJsonObject>
#include <QJsonArray>

// C++ includes
#include <utility>

DatabaseUpdater::DatabaseUpdater(QObject *parent) : SqLiteDatabase(parent)
{
}

void DatabaseUpdater::updateFromV3(QSqlQuery &query)
{
    // Get the tournament mode
    queryExec(query, QStringLiteral("SELECT value FROM config WHERE key = 'TOURNAMENT_MODE'"));
    query.next();
    const QString tournamentMode = query.value(0).toString();

    // Add the missing column to "assignment"
    queryExec(query, QStringLiteral("ALTER TABLE assignment ADD COLUMN player_number INTEGER"));

    // Fill in the data. For a single player mode database, the fetch result hopefully is still in
    // the correct order, so that the right players become player 1 and 2 inside a team.
    if (tournamentMode == QStringLiteral("FIXED_PAIRS")) {
        queryExec(query, QStringLiteral("UPDATE assignment SET player_number = 1"));
    } else {
        QVariantList round;
        QVariantList pair_or_player;
        QVariantList pair_number;
        QVariantList table_number;
        QVariantList player_number;
        queryExec(query, QStringLiteral("SELECT round, pair_or_player, pair_number, table_number "
                                        "FROM assignment "
                                        "ORDER BY round, table_number, pair_number"));
        int playerNumber = 1;
        while (query.next()) {
            round.append(query.value(0).toInt());
            pair_or_player.append(query.value(1).toInt());
            pair_number.append(query.value(2).toInt());
            table_number.append(query.value(3).toInt());
            playerNumber = (playerNumber + 1) % 2;
            player_number.append(playerNumber + 1);
        }

        queryPrepare(query, QStringLiteral("UPDATE assignment SET player_number = ? "
                                           "WHERE round = ? "
                                           "AND pair_or_player = ? "
                                           "AND pair_number = ? "
                                           "AND table_number = ?"));
        query.addBindValue(player_number);
        query.addBindValue(round);
        query.addBindValue(pair_or_player);
        query.addBindValue(pair_number);
        query.addBindValue(table_number);
        queryExecBatch(query);
    }

    // Update the "DB_VERSION" key
    queryExec(query, QStringLiteral("UPDATE config SET value = 4 WHERE key = 'DB_VERSION'"));
}

void DatabaseUpdater::updateFromV4(QSqlQuery &query)
{
    // Update "GAMES_PER_ROUND" to "BOOGERS_PER_ROUND"
    queryExec(query, QStringLiteral("UPDATE config SET key = 'BOOGERS_PER_ROUND' "
                                                "WHERE key = 'GAMES_PER_ROUND'"));

    // Update the score table's "game" column to "booger"
    queryExec(query, QStringLiteral("ALTER TABLE score RENAME TO score_tmp"));
    queryExec(query, QStringLiteral("CREATE TABLE score(\n"
                                    "    round INTEGER,\n"
                                    "    table_number INTEGER,\n"
                                    "    booger INTEGER,\n"
                                    "    pair_or_player INTEGER,\n"
                                    "    score INTEGER\n"
                                    ")"));
    queryExec(query, QStringLiteral("INSERT INTO score(round, table_number, booger, "
                                                      "pair_or_player, score) "
                                    "SELECT round, table_number, game, pair_or_player, score "
                                    "FROM score_tmp"));
    queryExec(query, QStringLiteral("DROP TABLE score_tmp"));

    // Update the "DB_VERSION" key
    queryExec(query, QStringLiteral("UPDATE config SET value = 5 WHERE key = 'DB_VERSION'"));
}

void DatabaseUpdater::updateFromV5(QSqlQuery &query)
{
    // The value for the new AUTO_SELECT_PAIRS setting depends on the tournament mode. This option
    // has always been on by default for fixed pairs, but must not be set for single player
    // tournaments, as it's not applicable in this mode (and also not changeable, as the respective
    // checkbox will be hidden)
    queryExec(query, QStringLiteral("SELECT value FROM config WHERE key = 'TOURNAMENT_MODE'"));
    query.next();
    const QString autoSelectPairs = query.value(0).toString() == QLatin1String("FIXED_PAIRS")
                                                                     ? QStringLiteral("1")
                                                                     : QStringLiteral("0");

    // Insert the new config keys. We don't use the defined constants here in case they change with
    // some later version.
    queryPrepare(query, QStringLiteral("INSERT INTO config(key, value) VALUES(?, ?)"));
    query.addBindValue(QVariantList { QStringLiteral("NO_TABLE_NUMBERS"),
                                      QStringLiteral("AUTO_SELECT_PAIRS"),
                                      QStringLiteral("INCLUDE_OPPONENT_GOALS") });
    query.addBindValue(QVariantList { QStringLiteral("0"),
                                      autoSelectPairs,
                                      QStringLiteral("0") });
    queryExecBatch(query);

    // Update the "DB_VERSION" key
    queryExec(query, QStringLiteral("UPDATE config SET value = 6 WHERE key = 'DB_VERSION'"));
}

void DatabaseUpdater::updateFromV6(QSqlQuery &query)
{
    // Add the new "markers" table
    queryExec(query, QStringLiteral("CREATE TABLE markers(\n"
                                    "    id INTEGER PRIMARY KEY,\n"
                                    "    name TEXT,\n"
                                    "    color INTEGER,\n"
                                    "    sorting INTEGER,\n"
                                    "    sequence INTEGER\n"
                                    ")"));

    // Add the new "marker" column to the "players" table
    queryExec(query, QStringLiteral("ALTER TABLE players ADD COLUMN marker INTEGER"));

    // Add the default markers using a direct query in case the format changes later:
    queryPrepare(query, QStringLiteral("INSERT INTO markers(id, name, color, sorting, sequence) "
                                       "VALUES(?, ?, ?, ?, ?)"));
    query.addBindValue(QVariantList { 0, 1, 2, 3 });
    query.addBindValue(QVariantList { QString(),
                                      tr("Rot"),
                                      tr("Blau"),
                                      tr("Grün") });
    // These are the output of QColor(0,   0,   0,   0).rgb()
    //                         QColor(255, 0,   0,   0).rgb()
    //                         QColor(0,   0,   255, 0).rgb()
    //                         QColor(0,   128, 0,   0).rgb()
    query.addBindValue(QVariantList { (uint) 4278190080,
                                      (uint) 4294901760,
                                      (uint) 4278190335,
                                      (uint) 4278222848 });
    query.addBindValue(QVariantList { 0, 0, 0, 1 });
    query.addBindValue(QVariantList { 2, 0, 1, 3 });
    queryExecBatch(query);

    // Map the old markers to the new ones

    QVariantList oldNames;
    QVariantList newNames;
    QVariantList markerIds;

    // Search for marked names
    const QString markerCharacter = QStringLiteral("\u200B");
    queryPrepare(query, QStringLiteral("SELECT name FROM players"));
    queryExec(query);
    while (query.next()) {
        const QString name = query.value(0).toString();
        int marker = -1;
        if (name.startsWith(markerCharacter) && ! name.endsWith(markerCharacter)) {
            marker = 1;
        } else if (! name.startsWith(markerCharacter) && name.endsWith(markerCharacter)) {
            marker = 3;
        }
        if (marker == -1) {
            continue;
        }
        QString strippedName = name;
        strippedName.remove(markerCharacter);
        oldNames.append(name);
        newNames.append(strippedName);
        markerIds.append(marker);
    }

    // Update the names and the marker ids
    queryPrepare(query, QStringLiteral("UPDATE players SET name = ?, marker = ? WHERE name = ?"));
    query.addBindValue(newNames);
    query.addBindValue(markerIds);
    query.addBindValue(oldNames);
    queryExecBatch(query);

    // Update the "DB_VERSION" key
    queryExec(query, QStringLiteral("UPDATE config SET value = 7 WHERE key = 'DB_VERSION'"));
}

void DatabaseUpdater::updateFromV7(QSqlQuery &query)
{
    // Add the preassignment cleanup trigger
    queryExec(query, QStringLiteral(
        "CREATE TRIGGER cleanup_preassignment AFTER DELETE ON players\n"
        "FOR EACH ROW\n"
        "BEGIN\n"
        "    DELETE FROM assignment WHERE pair_or_player = OLD.id;\n"
        "END"));

    // Move the settings to the new JSON format
    QString jsonSettings(QStringLiteral("{"));

    // We have to fix a bug introduced with version 0.7.7 when updating from dbv6 or dbv7 (when
    // updating a dbv5 database, it will be fixed directly in updateDatabaseFromV5 now).
    // If a dbv5 database was updated with v0.7.7 up to v3.2, AUTO_SELECT_PAIRS was erroneously set
    // to TRUE for single player databases and not deselected later anymore, which lead to wrong
    // warnings or complete unusability of the score page when selecting players 1.

    // First, we need the tournament mode. It's selected again below, but we need it when processing
    // the AUTO_SELECT_PAIRS key. So here we go:
    queryExec(query, QStringLiteral("SELECT value FROM config WHERE key = 'TOURNAMENT_MODE'"));
    query.next();
    const bool singlePlayerMode = query.value(0).toString() == QLatin1String("SINGLE_PLAYERS");

    // Convert the existing keys and values
    queryExec(query, QStringLiteral("SELECT key, value FROM config"));
    while (query.next()) {
        const QString key(query.value(0).toString());
        const QString value(query.value(1).toString());

        if (key == QStringLiteral("MUCKTURNIER_VERSION")) {
            jsonSettings.append(QLatin1String("\"muckturnier version\":\""));
            jsonSettings.append(value);
            jsonSettings.append(QLatin1String("\","));
        } else if (key == QStringLiteral("TOURNAMENT_MODE")) {
            jsonSettings.append(QLatin1String("\"tournament mode\":\""));
            jsonSettings.append(value == QStringLiteral("FIXED_PAIRS")
                                    ? QLatin1String("fixed pairs\",")
                                    : QLatin1String("single players\","));
        } else if (key == QStringLiteral("BOOGER_SCORE")) {
            jsonSettings.append(QLatin1String("\"booger score\":"));
            jsonSettings.append(value);
            jsonSettings.append(QLatin1String(","));
        } else if (key == QStringLiteral("BOOGERS_PER_ROUND")) {
            jsonSettings.append(QLatin1String("\"boogers per round\":"));
            jsonSettings.append(value);
            jsonSettings.append(QLatin1String(","));
        } else if (key == QStringLiteral("NO_TABLE_NUMBERS")) {
            jsonSettings.append(QLatin1String("\"no table numbers\":"));
            jsonSettings.append(value == QStringLiteral("0") ? QLatin1String("false")
                                                             : QLatin1String("true"));
            jsonSettings.append(QLatin1String(","));
        } else if (key == QStringLiteral("AUTO_SELECT_PAIRS")) {
            jsonSettings.append(QLatin1String("\"autoselect pairs\":"));
            // And here's the fix :-)
            if (singlePlayerMode) {
                jsonSettings.append(QLatin1String("false"));
            } else {
                jsonSettings.append(value == QStringLiteral("0") ? QLatin1String("false")
                                                                 : QLatin1String("true"));
            }
            jsonSettings.append(QLatin1String(","));
        } else if (key == QStringLiteral("INCLUDE_OPPONENT_GOALS")) {
            jsonSettings.append(QLatin1String("\"include opponent goals\":"));
            jsonSettings.append(value == QStringLiteral("0") ? QLatin1String("false")
                                                             : QLatin1String("true"));
            jsonSettings.append(QLatin1String(","));
        }
    }

    // Add the default new ones
    jsonSettings.append(QLatin1String("\"absent marker\":-1,"));
    jsonSettings.append(QLatin1String("\"default marker\":0,"));
    jsonSettings.append(QLatin1String("\"singles management version\":1,"));
    jsonSettings.append(QLatin1String("\"singles management\":{"
                                          "\"assigned marker\":0,"
                                          "\"condition\":0,"
                                          "\"condition string\":\"/\","
                                          "\"enabled\":false,"
                                          "\"mark singles\":false,"
                                          "\"single marker\":0},"));
    jsonSettings.append(QLatin1String("\"players page columns\":{"
                                          "\"assignment\":false,"
                                          "\"mark\":true,"
                                          "\"marker\":false}"));

    // Finish the string representation
    jsonSettings.append(QLatin1String("}"));

    // Delete the old config keys and values
    queryExec(query, QStringLiteral("DELETE FROM config"));

    // Insert the new config keys and values
    queryPrepare(query, QStringLiteral("INSERT INTO config(key, value) VALUES(?, ?)"));
    query.addBindValue(QVariantList {
        QStringLiteral("DB_VERSION"),
        QStringLiteral("DB_IDENTIFIER"),
        QStringLiteral("SETTINGS")
    });
    query.addBindValue(QVariantList {
        8, // <-- This updates DB_VERSION
        QStringLiteral("Muckturnier tournament database"),
        jsonSettings
    });
    queryExecBatch(query);

    // Add the new custom header fields
    queryExec(query, QStringLiteral("PRAGMA application_id = 1299538795"));
    queryExec(query, QStringLiteral("PRAGMA user_version = 8"));
}

void DatabaseUpdater::updateFromV8(QSqlQuery &query)
{
    // Add the SETTINGS_VERSION key to be able to track changes in the settings
    queryExec(query, QStringLiteral("INSERT INTO config(key, value) "
                                    "VALUES('SETTINGS_VERSION', 2)"));

    // Move the "players page columns" hash to the "registration columns" array

    // Read and raw-parse the settings
    queryExec(query, QStringLiteral("SELECT value FROM config WHERE key = 'SETTINGS'"));
    query.next();
    auto settings = Json::objectFromString(query.value(0).toString());

    // Get the old columns definition and remove it from the settings
    const auto oldColumns = settings.value(QStringLiteral("players page columns")).toObject();
    settings.remove(QStringLiteral("players page columns"));

    // Translate the old definition into the new one
    QJsonArray newColumns;
    if (oldColumns.value(QStringLiteral("mark")).toBool()) {
        newColumns.append(QStringLiteral("mark"));
    }
    if (oldColumns.value(QStringLiteral("assignment")).toBool()) {
        // "assignment" has been renamed to "draw"
        newColumns.append(QStringLiteral("draw"));
    }
    if (oldColumns.value(QStringLiteral("marker")).toBool()) {
        newColumns.append(QStringLiteral("marker"));
    }
    if (newColumns.isEmpty()) {
        // If nothing is found: Use the standard
        newColumns.append(QStringLiteral("mark"));
    }

    // Add the new definition
    settings.insert(QStringLiteral("registration columns"), newColumns);

    // Save the updated settings
    query.prepare(QStringLiteral("UPDATE config SET value = ? WHERE key = 'SETTINGS'"));
    query.bindValue(0, Json::serialize(settings));
    queryExec(query);

    // Rename the cleanup_preassignment trigger
    queryExec(query, QStringLiteral("DROP TRIGGER cleanup_preassignment"));
    queryExec(query, QStringLiteral("CREATE TRIGGER cleanup_draw AFTER DELETE ON players\n"
                                    "FOR EACH ROW\n"
                                    "BEGIN\n"
                                    "    DELETE FROM assignment WHERE pair_or_player = OLD.id;\n"
                                    "END"));

    // Update the DB_VERSION
    queryExec(query, QStringLiteral("UPDATE config SET value = 9 WHERE key = 'DB_VERSION'"));
    queryExec(query, QStringLiteral("PRAGMA user_version = 9"));
}

void DatabaseUpdater::updateFromV9(QSqlQuery &query)
{
    // Add the "disqualified" column to the players table
    queryExec(query, QStringLiteral("ALTER TABLE players ADD COLUMN disqualified INTEGER"));

    // Update the DB_VERSION
    queryExec(query, QStringLiteral("UPDATE config SET value = 10 WHERE key = 'DB_VERSION'"));
    queryExec(query, QStringLiteral("PRAGMA user_version = 10"));
}

void DatabaseUpdater::updateFromV10(QSqlQuery &query)
{
    // Add the new "schedule" table
    queryExec(query, QStringLiteral("CREATE TABLE schedule(\n"
                                    "    round INTEGER PRIMARY KEY,\n"
                                    "    start TEXT,\n"
                                    "    end TEXT\n"
                                    ")"));

    // Update the DB_VERSION
    queryExec(query, QStringLiteral("UPDATE config SET value = 11 WHERE key = 'DB_VERSION'"));
    queryExec(query, QStringLiteral("PRAGMA user_version = 11"));
}

void DatabaseUpdater::updateFromV11(QSqlQuery &query)
{
    // Add the "number" column to the "players" table
    queryExec(query, QStringLiteral("ALTER TABLE players ADD COLUMN number INTEGER"));

    // Add the "booking_checksum" column to the "players" table
    queryExec(query, QStringLiteral("ALTER TABLE players ADD COLUMN booking_checksum TEXT"));

    // Fill in round 0 assignment player numbers. This should have already been done during the
    // v10 --> v11 update ... thus, we only alter those having still NULL as the player number, as
    // draws set with version 3.6.0 do have a valid player number.

    queryExec(query, QStringLiteral("SELECT pair_or_player, table_number, pair_number "
                                    "FROM assignment WHERE round = 0 AND player_number IS NULL"));

    QVector<std::pair<int, int>> takenSeats;
    QSqlQuery drawQuery(m_db);
    while (query.next()) {
        const auto id = query.value(0).toInt();
        const auto table = query.value(1).toInt();
        const auto pair = query.value(2).toInt();
        const auto seat = std::pair<int, int> { table, pair };
        int player = 1;
        if (takenSeats.contains(seat)) {
            player = 2;
        } else {
            takenSeats.append(seat);
        }
        drawQuery.prepare(QStringLiteral("UPDATE assignment SET player_number = :player "
                                         "WHERE round = 0 "
                                         "AND pair_or_player = :pair_or_player "
                                         "AND table_number = :table_number "
                                         "AND pair_number = :pair_number"));
        drawQuery.bindValue(QStringLiteral(":player"), player);
        drawQuery.bindValue(QStringLiteral(":pair_or_player"), id);
        drawQuery.bindValue(QStringLiteral(":table_number"), table);
        drawQuery.bindValue(QStringLiteral(":pair_number"), pair);
        queryExec(drawQuery);
    }

    // Add the "fix player 2 draw" trigger
    // This one makes a player 2 of a pair player 1 if player 1's draw is deleted
    queryExec(query, QStringLiteral(
        "CREATE TRIGGER fix_player2_draw AFTER DELETE ON assignment\n"
        "FOR EACH ROW WHEN OLD.round = 0 AND OLD.player_number = 1\n"
        "BEGIN\n"
        "    UPDATE assignment SET player_number = 1\n"
        "    WHERE round = 0\n"
        "    AND table_number = OLD.table_number\n"
        "    AND pair_number = OLD.pair_number\n"
        "    AND player_number = 2;\n"
        "END"));

    // Update the DB_VERSION
    queryExec(query, QStringLiteral("UPDATE config SET value = 12 WHERE key = 'DB_VERSION'"));
    queryExec(query, QStringLiteral("PRAGMA user_version = 12"));
}

void DatabaseUpdater::updateFromV12(QSqlQuery &query)
{
    // Remove the old draw cleanup triggers which reference the old "assignment" table
    queryExec(query, QStringLiteral("DROP TRIGGER cleanup_draw"));
    queryExec(query, QStringLiteral("DROP TRIGGER fix_player2_draw"));

    // Re-order the columns of the "assignment" table

    // Rename the old "assignment" table
    queryExec(query, QStringLiteral("ALTER TABLE assignment RENAME TO assignment_old"));

    // Create a new "assignment" table with the new column order
    queryExec(query, QStringLiteral("CREATE TABLE assignment(\n"
                                    "    round INTEGER,\n"
                                    "    pair_or_player INTEGER,\n"
                                    "    table_number INTEGER,\n"
                                    "    pair_number INTEGER,\n"
                                    "    player_number INTEGER\n"
                                    ")"));

    // Copy all data from the old to the new "assignment" table.
    // We have to reference all columns here separately, because the column order differs.
    queryExec(query, QStringLiteral("INSERT INTO assignment("
                                    "    round,"
                                    "    pair_or_player,"
                                    "    table_number,"
                                    "    pair_number,"
                                    "    player_number"
                                    ") "
                                    "SELECT "
                                    "    round,"
                                    "    pair_or_player,"
                                    "    table_number,"
                                    "    pair_number,"
                                    "    player_number "
                                    "FROM assignment_old"));

    // Delete the old "assignment" table
    queryExec(query, QStringLiteral("DROP TABLE assignment_old"));

    // Add the new "draw" table
    queryExec(query, QStringLiteral("CREATE TABLE draw(\n"
                                    "    round INTEGER,\n"
                                    "    pair_or_player INTEGER,\n"
                                    "    table_number INTEGER,\n"
                                    "    pair_number INTEGER,\n"
                                    "    player_number INTEGER\n"
                                    ")"));

    // Copy all possibly existing draws from the "assignment" table to the new "draw" table
    // This can be done this way, because the "assignment" and the "draw" tables now have the same
    // column order
    queryExec(query, QStringLiteral("INSERT INTO draw SELECT * FROM assignment WHERE round = 0"));

    // Update the copied datasets to point to round 1
    // (we only had draws for the first round, marked by "round 0")
    queryExec(query, QStringLiteral("UPDATE draw SET round = 1"));

    // Delete the draws from the "assignment" table
    queryExec(query, QStringLiteral("DELETE FROM assignment WHERE round = 0"));

    // Re-create the cleanup triggers

    queryExec(query, QStringLiteral(
        "CREATE TRIGGER cleanup_draw AFTER DELETE ON players\n"
        "FOR EACH ROW\n"
        "BEGIN\n"
        "    DELETE FROM draw WHERE pair_or_player = OLD.id;\n"
        "END"));

    queryExec(query, QStringLiteral(
        "CREATE TRIGGER fix_player2_draw AFTER DELETE ON draw\n"
        "FOR EACH ROW WHEN OLD.player_number = 1\n"
        "BEGIN\n"
        "    UPDATE draw SET player_number = 1\n"
        "    WHERE round = OLD.round\n"
        "    AND table_number = OLD.table_number\n"
        "    AND pair_number = OLD.pair_number\n"
        "    AND player_number = 2;\n"
        "END"));

    // Update the DB_VERSION
    queryExec(query, QStringLiteral("UPDATE config SET value = 13 WHERE key = 'DB_VERSION'"));
    queryExec(query, QStringLiteral("PRAGMA user_version = 13"));
}

void DatabaseUpdater::updateFromV13(QSqlQuery &query)
{
    // Add the new "revoked_bookings" table

    queryExec(query, QStringLiteral("CREATE TABLE revoked_bookings(\n"
                                    "    checksum TEXT PRIMARY KEY,\n"
                                    "    revoked TEXT,\n"
                                    "    reason TEXT\n"
                                    ")"));

    // Parse the settings
    queryExec(query, QStringLiteral("SELECT value FROM config WHERE key = 'SETTINGS'"));
    query.next();
    auto settings = Json::objectFromString(query.value(0).toString());

    // Move the booking settings to the booking v2 JSON representation

    const auto bookingSettingsKey = QStringLiteral("booking settings");

    // Parse the old booking settings
    auto oldBookingSettings = Json::objectFromString(settings.value(bookingSettingsKey).toString());

    // Create the new JSON representation. This will also work if empty settings have been stored.
    const auto tournamentName = QStringLiteral("tournament name");
    const auto securityKey = QStringLiteral("security key");
    const auto newBookingSettings = QJsonObject {
        { tournamentName, oldBookingSettings.value(tournamentName).toString() },
        { securityKey,    oldBookingSettings.value(securityKey).toString() }
    };

    // Replace the old serialization with the new one
    settings.remove(bookingSettingsKey);
    settings.insert(bookingSettingsKey, newBookingSettings);
    settings.insert(QStringLiteral("booking settings version"), 2);

    // Move "absent" marker to "booked" marker

    const auto absentMarkerKey = QStringLiteral("absent marker");
    const auto bookedMarker = settings.value(absentMarkerKey).toInt();
    settings.remove(absentMarkerKey);
    settings.insert(QStringLiteral("booked marker"), bookedMarker);

    // Write the updated settings to the database and update the db revision

    queryPrepare(query, QStringLiteral("UPDATE config SET value = ? WHERE key = ?"));
    query.addBindValue(QVariantList { Json::serialize(settings),
                                      14 });
    query.addBindValue(QVariantList { QStringLiteral("SETTINGS"),
                                      QStringLiteral("DB_VERSION") });
    queryExecBatch(query);

    queryExec(query, QStringLiteral("PRAGMA user_version = 14"));
}
