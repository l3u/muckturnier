﻿// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SQLITEDATABASE_H
#define SQLITEDATABASE_H

// Qt includes
#include <QObject>
#include <QSqlDatabase>
#include <QSqlQuery>

class SqLiteDatabase : public QObject
{
    Q_OBJECT

Q_SIGNALS:
    void sqlError(const QString &text);

protected: // Functions
    explicit SqLiteDatabase(QObject *parent);

    QString sqlErrorText() const;
    QString sqlErrorText(const QSqlQuery &query) const;
    void emitSqlError(const QString &text);

    bool startTransaction();
    bool commitTransaction();
    bool rollbackTransaction(QSqlQuery &query);

    bool queryPrepare(QSqlQuery &query, const QString &text);
    bool queryExec(QSqlQuery &query);
    bool queryExec(QSqlQuery &query, const QString &text);
    bool queryExecBatch(QSqlQuery &query);

    // Templates for helper functions to catch SQL errors

    template<typename SqlFunction>
    bool writeHelper(SqlFunction sqlFunction)
    {
        if (! startTransaction()) {
            return false;
        }

        QSqlQuery query(m_db);
        try {
            sqlFunction(query);
            commitTransaction();
        } catch (...) {
            rollbackTransaction(query);
            return false;
        }

        return true;
    }

    template<typename SqlFunction>
    void readHelper(SqlFunction sqlFunction, bool &success)
    {
        try {
            QSqlQuery query(m_db);
            sqlFunction(query);
            success = true;
        } catch (...) {
            success = false;
        }
    }

protected: // Variables
    static inline QSqlDatabase m_db;
    static inline bool m_isWritable = false;
    static inline bool m_emitSqlErrors = false;

};

#endif // SQLITEDATABASE_H
