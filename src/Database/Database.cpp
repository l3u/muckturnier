// SPDX-FileCopyrightText: 2010-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "Database.h"
#include "DatabaseUpdater.h"
#include "Logging.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/StringTools.h"
#include "SharedObjects/TournamentSettings.h"

#include "shared/Tournament.h"
#include "shared/Urls.h"
#include "shared/Logging.h"

// Qt includes
#include <QDebug>
#include <QDir>
#include <QSqlError>
#include <QLockFile>

// C++ includes
#include <utility>

// The SQLite application_id for Muckurnier
// The hex representation of this number is 0x4d75636b, which maps to "Muck"
static constexpr int s_applicationId = 1299538795;

// The file format identifier for the DB_IDENTIFIER config key
static const QString s_dbIdentifier(QLatin1String("Muckturnier tournament database"));

// The revision we're using
static constexpr int s_dbVersion = 14;

// Key literals for the database's "config" table
static const QString s_keyDbIdentifier   (QLatin1String("DB_IDENTIFIER"));
static const QString s_keyDbVersion      (QLatin1String("DB_VERSION"));
static const QString s_keySettings       (QLatin1String("SETTINGS"));
static const QString s_keySettingsVersion(QLatin1String("SETTINGS_VERSION"));

// This one is only used to check if we can write to the database in a transaction that
// is rolled back afterwards. So this key will never be actually written to a database.
static const QString s_keyWriteTest(QLatin1String("WRITE_TEST"));

// Internally used connection name
static const QString s_connectionName(QLatin1String("SQLiteDB"));

// This will map to SQLite's NULL value
static const QVariant s_sqliteNull;

Database::Database(QObject *parent, SharedObjects *sharedObjects)
    : SqLiteDatabase(parent),
      m_tournamentSettings(sharedObjects->tournamentSettings()),
      m_stringTools(sharedObjects->stringTools())
{
    connect(m_tournamentSettings, &TournamentSettings::drawModeChanged,
            this, &Database::updateFixtures);
}

// ================================
// Database connecting and checking
// ================================

bool Database::connectDatabase(const QString &dbFile)
{
    // First close a connection if we have one
    closeDatabaseConnection();

    // Connect the database
    m_db = QSqlDatabase::addDatabase(QStringLiteral("QSQLITE"), s_connectionName);
    m_db.setDatabaseName(dbFile);

    updateDbFileInfo();

    if (! m_db.open()) {
        // This should not happen
        QSqlError error = m_db.lastError();
        m_errorMessage = tr("<p>Unerwarteter Fehler: Konnte keine Datenbankverbindung herstellen."
                            "</p>"
                            "<p>Die Fehlermeldung war: %1</p>").arg(error.text());
        return false;
    }

    return true;
}

void Database::closeDatabaseConnection()
{
    if (! m_db.isOpen()) {
        return;
    }

    // Close the existing connection
    m_db.close();
    m_db = QSqlDatabase::database();
    QSqlDatabase::removeDatabase(s_connectionName);

    // Remove the lock file
    if (m_lockFile != nullptr) {
        delete m_lockFile;
        m_lockFile = nullptr;
    }

    // Clear cached variables
    m_dbFileName.clear();
    m_dbFilePath.clear();
    m_tournamentStarted = false;

    // Don't emit SQL error signals
    m_emitSqlErrors = false;
}

QString Database::lockFileName(const QString &dbFile) const
{
    const QFileInfo info(dbFile);
    QString path = info.path();
    if (path == QStringLiteral("/")) {
        path.clear();
    }
    return QStringLiteral("%1/.muckturnier.%2.lockfile").arg(path, info.fileName());
}

bool Database::lockDatabase()
{
    m_lockFile = new QLockFile(lockFileName(m_dbFileName));
    m_lockFile->setStaleLockTime(0);

    if (! m_lockFile->tryLock(0)) {
        delete m_lockFile;
        m_lockFile = nullptr;
        return false;
    }

    return true;
}

QString Database::checkForLockFile(const QString &dbFile) const
{
    // If the file doesn't exist, it can't be locked
    QFileInfo fileInfo(dbFile);
    if (! fileInfo.exists()) {
        return QString();
    }

    // If it exists, try to lock it
    const QString fileName(lockFileName(dbFile));
    QLockFile lockFile(fileName);
    lockFile.setStaleLockTime(0);
    const bool noLock = lockFile.tryLock(0);
    return noLock ? QString() : fileName;
}

void Database::updateDbFileInfo()
{
    m_dbFileName = m_db.databaseName();
    const QFileInfo info(m_dbFileName);
    m_dbFilePath = info.canonicalPath();
}

Database::Error Database::openDatabase(const QString &dbFile)
{
    // Do a check if we actually do have a Muckturnier Tournament Database and if it's the correct
    // revision. If we need to do an update or the database is not usable, we will return here.

    const Error testResult = checkDatabase(dbFile);
    if (testResult != Error::NoError) {
        return testResult;
    }

    QSqlQuery query(m_db);

    // Get the settings

    QString settings;

    try {
        queryPrepare(query, QStringLiteral("SELECT value FROM config WHERE key = ?"));
        query.bindValue(0, s_keySettings);
        queryExec(query);
        if (! query.next()) {
            m_errorMessage = tr(
                "<p>Der Einstellungs-Datensatz konnte nicht ausgelesen werden. Entweder ist die "
                "Datenbank beschädigt, oder sie wurde unsachgemäß manuell verändert.</p>");
            closeDatabaseConnection();
            return Error::CouldNotOpen;
        }
        settings = query.value(0).toString();
    } catch (...) {
        m_errorMessage = tr(
            "<p>Der Einstellungs-Datensatz konnte nicht ausgelesen werden. Entweder ist die "
            "Datenbank beschädigt, oder sie wurde unsachgemäß manuell verändert.</p>")
            + sqlErrorText(query);
        closeDatabaseConnection();
        return Error::CouldNotOpen;
    }

    if (! m_tournamentSettings->load(settings)) {
        m_errorMessage = tr("<p>Beim Parsen der Turniereinstellungen ist ein Fehler aufgetreten. "
                            "Entweder ist die Datenbank „%1“ beschädigt, oder sie wurde "
                            "unsachgemäß manuell verändert.</p>").arg(
                            QDir::toNativeSeparators(dbFile).toHtmlEscaped());
        closeDatabaseConnection();
        return Error::CouldNotOpen;
    }

    // Initialize the players count, scores per round and the rounds state

    try {
        queryExec(query, QStringLiteral("SELECT COUNT(id) FROM players"));
        query.next();
        setPairsOrPlayersCount(query.value(0).toInt());
    } catch (...) {
        m_errorMessage = tr("<p>Beim Zählen der Anmeldungen ist ein Fehler aufgetreten. "
                            "Entweder ist die Datenbank „%1“ beschädigt, oder sie wurde "
                            "unsachgemäß manuell verändert.</p>").arg(
                            QDir::toNativeSeparators(dbFile).toHtmlEscaped())
                         + sqlErrorText(query);
        closeDatabaseConnection();
        return Error::CouldNotOpen;
    }

    if (! updateRoundsState()) {
        closeDatabaseConnection();
        return Error::CouldNotOpen;
    }

    if (! checkIfTournamentStarted()) {
        closeDatabaseConnection();
        return Error::CouldNotOpen;
    }

    // From here on, not a single SQL query should fail, so we activate the MainWindow's SQL
    // error handling (which displays a critical error and closes the database if something goes
    // wrong)
    m_emitSqlErrors = true;
    return Error::NoError;
}

Database::Error Database::checkDatabase(const QString &dbFile)
{
    // NOTE: This has to be an extra function, as it's also called when restoring a backup.

    // Check the given file name to be an actual file and readable.
    // No interaction with the database itself is done yet here.

    // Check if it's a file and if we can read it
    const QFileInfo fileInfo = QFileInfo(dbFile);
    if (! fileInfo.exists()) {
        m_errorMessage = tr("<p>Die Datei konnte nicht gefunden werden.</p>");
        return Error::CouldNotOpen;
    }
    if (! (fileInfo.isFile() || fileInfo.isSymLink())) {
        m_errorMessage = tr("<p>Der Pfad ist weder eine Datei, noch ein symbolischer Link.</p>");
        return Error::CouldNotOpen;
    }
    if (! fileInfo.isReadable()) {
        m_errorMessage = tr(
            "<p>Die Datei ist nicht lesbar. Bitte die Zugriffsrechte überprüfen!</p>");
        return Error::CouldNotOpen;
    }

    // Try to establish a connection to the file
    if (! connectDatabase(dbFile)) {
        m_errorMessage = tr("<p>Es konnte keine Datenbankverbindung aufgebaut werden.</p>");
        return Error::CouldNotOpen;
    }

    // We could connect to the file

    // Let's see if we can acquire a lock (before starting a normal transaction)
    const bool dbIsLocked = checkIfDbIsLocked();

    // First we try if we can start a transaction (needed for the write test later) ...
    if (! m_db.transaction()) {
        m_errorMessage = tr(
            "<p>Es konnte keine Transaktion gestartet werden. Das sollte nicht passieren. Die "
            "Datenbank kann nicht verwendet werden!</p>");
        closeDatabaseConnection();
        return Error::CouldNotOpen;
    }

    // ... then we create a query
    QSqlQuery query(m_db);

    // Do an internal check to see if it's an SQLite database and if it's in a sane condition
    try {
        queryExec(query, QStringLiteral("PRAGMA quick_check"));
        if (! query.next() || query.value(0).toString() != QLatin1String("ok")) {
            throw false;
        }
    } catch (...) {
        m_errorMessage = tr("<p>Der Datenbankcheck ist fehlgeschlagen:</p>")
                         + sqlErrorText(query);
        closeDatabaseConnection();
        return Error::CouldNotOpen;
    }

    // Check if we actually have a Muckturnier tournament database

    // Let's see if we have a valid database revision config entry

    int dbVersion = 0;
    try {
        queryPrepare(query, QStringLiteral("SELECT value FROM config WHERE key = ?"));
        query.bindValue(0, s_keyDbVersion);
        queryExec(query);
        if (! query.next()) {
            m_errorMessage = tr(
                "<p>Die verwendete Datenbankrevision konnte nicht ausgelesen werden.</p>"
                "<p>Entweder ist die Datenbank beschädigt, oder sie wurde unsachgemäß manuell "
                "verändert.</p>");
            closeDatabaseConnection();
            return Error::CouldNotOpen;
        }
        dbVersion = query.value(0).toInt();
    } catch (...) {
        m_errorMessage = tr(
            "<p>Die verwendete Datenbankrevision konnte nicht ausgelesen werden.</p>"
            "<p>Entweder ist die Datenbank beschädigt, oder sie wurde unsachgemäß manuell "
            "verändert.</p>")
            + sqlErrorText(query);
        closeDatabaseConnection();
        return Error::CouldNotOpen;
    }

    if (dbVersion < 3) {
        m_errorMessage = tr(
            "<p>Die von der Datenbank benutzte Revision %2 ist unbekannt und kann nicht "
            "verarbeitet werden.</p>").arg(QString::number(dbVersion));
        closeDatabaseConnection();
        return Error::CouldNotOpen;
    } else if (dbVersion > s_dbVersion) {
        m_errorMessage = tr(
            "<p>Die von der Datenbank benutzte Revision %2 ist unbekannt und kann nicht "
            "verarbeitet werden.</p>"
            "<p>Vermutlich wurde die Datei mit einer neueren Version von Muckturnier erstellt. "
            "Bitte <a href=\"%3\">aktualisieren</a>!").arg(QString::number(dbVersion),
                                                           Urls::download);
        closeDatabaseConnection();
        return Error::CouldNotOpen;
    }

    // If the revision is between 3 and s_dbVersion, we should have a valid Muckturnier tournament
    // database. Let's see if this is really the case

    // Starting with dbv8, we have a (most probably) unique identifier, which we check first.
    if (dbVersion >= 8) {
        bool identifierOkay = false;
        try {
            queryPrepare(query, QStringLiteral("SELECT value FROM config WHERE key = ?"));
            query.bindValue(0, s_keyDbIdentifier);
            queryExec(query);
            if (! query.next()) {
                m_errorMessage = tr(
                    "<p>Die Datenbank-ID ist nicht auffindbar. Entweder ist die Datenbank "
                    "beschädigt, oder sie wurde unsachgemäß manuell verändert.</p>");
                closeDatabaseConnection();
                return Error::CouldNotOpen;
            }
            identifierOkay = query.value(0).toString() == s_dbIdentifier;
        } catch (...) {
            m_errorMessage = tr(
                "<p>Die Datenbank-ID konnte nicht ausgelesen werden. Entweder ist die Datenbank "
                "beschädigt, oder sie wurde unsachgemäß manuell verändert.</p>")
                + sqlErrorText(query);
            closeDatabaseConnection();
            return Error::CouldNotOpen;
        }

        if (! identifierOkay) {
            m_errorMessage = tr(
                "<p>Die Datenbank hat keine gültige Datenbank-ID. Vermutlich ist es keine "
                "Muckturnier-Turnierdatenbank.</p>");
            closeDatabaseConnection();
            return Error::CouldNotOpen;
        }

    // For older revisions, we have to rely on the old structure check:
    // We simply look for all tables to be present

    } else {
        const QStringList tables = m_db.tables();
        bool allTablesPresent = false;

        if (dbVersion >= 7) {
            allTablesPresent =     tables.contains(QLatin1String("config"))
                                && tables.contains(QLatin1String("players"))
                                && tables.contains(QLatin1String("markers"))
                                && tables.contains(QLatin1String("assignment"))
                                && tables.contains(QLatin1String("score"));
        } else {
            // The "markers" table wasn't present until dbv7
            allTablesPresent =     tables.contains(QLatin1String("config"))
                                && tables.contains(QLatin1String("players"))
                                && tables.contains(QLatin1String("assignment"))
                                && tables.contains(QLatin1String("score"));
        }

        if (! allTablesPresent) {
            m_errorMessage = tr(
                "<p>Die Datenbank enthält nicht alle nötigen Tabellen.</p>"
                "<p>Entweder ist es keine Muckturnier-Turnierdatenbank, die Datenbank ist "
                "beschädigt oder sie wurde unsachgemäß manuell verändert.</p>");
            closeDatabaseConnection();
            return Error::CouldNotOpen;
        }
    }

    // Check if all configuration keys other than DB_VERSION and DB_IDENTIFIER are present
    // (those have already been checked to be present above)

    QString remainingConfigKeys;
    int expectedRemainingKeysCount = 0;

    if (dbVersion >= 3 && dbVersion <= 4) {
        expectedRemainingKeysCount = 4;
        remainingConfigKeys = QStringLiteral("'MUCKTURNIER_VERSION',"
                                             "'TOURNAMENT_MODE',"
                                             "'BOOGER_SCORE',"
                                             "'GAMES_PER_ROUND'");
    } else if (dbVersion == 5) {
        expectedRemainingKeysCount = 4;
        remainingConfigKeys = QStringLiteral("'MUCKTURNIER_VERSION',"
                                             "'TOURNAMENT_MODE',"
                                             "'BOOGER_SCORE',"
                                             "'BOOGERS_PER_ROUND'");
    } else if (dbVersion >= 6 && dbVersion <= 7) {
        expectedRemainingKeysCount = 7;
        remainingConfigKeys = QStringLiteral("'MUCKTURNIER_VERSION',"
                                             "'TOURNAMENT_MODE',"
                                             "'BOOGER_SCORE',"
                                             "'BOOGERS_PER_ROUND',"
                                             "'NO_TABLE_NUMBERS',"
                                             "'AUTO_SELECT_PAIRS',"
                                             "'INCLUDE_OPPONENT_GOALS'");
    } else if (dbVersion == 8) {
        expectedRemainingKeysCount = 1;
        remainingConfigKeys = QStringLiteral("'SETTINGS'");
    } else { // dbVersion >= 9
        expectedRemainingKeysCount = 2;
        remainingConfigKeys = QStringLiteral("'SETTINGS',"
                                             "'SETTINGS_VERSION'");
    }

    int foundKeysCount = 0;
    try {
        queryExec(query, QStringLiteral("SELECT COUNT(key) FROM config WHERE key IN(%1)").arg(
                                        remainingConfigKeys));
        query.next();
        foundKeysCount = query.value(0).toInt();
    } catch (...) {
        m_errorMessage = tr(
            "<p>Die Konfigurationsschlüssel konnten nicht ausgelesen werden.</p>"
            "<p>Entweder ist die Datenbank beschädigt, oder sie wurde unsachgemäß manuell "
            "verändert.</p>")
            + sqlErrorText(query);
        closeDatabaseConnection();
        return Error::CouldNotOpen;
    }

    if (foundKeysCount != expectedRemainingKeysCount) {
        m_errorMessage = tr(
            "<p>Die Datenbank enthält nicht alle nötigen Konfigurationsschlüssel.</p>"
            "<p>Entweder ist es keine Muckturnier-Turnierdatenbank, die Datenbank ist beschädigt "
            "oder sie wurde unsachgemäß manuell verändert.</p>");
        closeDatabaseConnection();
        return Error::CouldNotOpen;
    }

    // Let's see if we can write to the database.
    //
    // We do have to check if a database is writable by actually trying to insert some data (is
    // this a bug or a feature?!) because:
    //
    // - If the file itself has write access, QFileInfo::isWritable() will return true, but if the
    //   parent directory is not writable, we won't be able to write to the database.
    //
    // - Checking for the parent directory to be writable in addition does also not help, as we will
    //   get false for "My Documents" on Windows, although we can write to it.
    //
    // - Opening the file via QFile::open() does actually work if the file is writable and the
    //   parent directory is not (with the QIODevice::WriteOnly flag, the file will even be
    //   trucated!), but we still can't insert data in this case.
    //
    // So here we go:

    try {
        // Even if the database is locked by another instance, it's save to do this here:
        // The transaction won't be actually written to the database. And even if it was,
        // it wouldn't affect the other instance, because s_keyWriteTest is never used.
        // So this is save in each case:
        queryPrepare(query, QStringLiteral("INSERT INTO config(key, value) VALUES(?, ?)"));
        query.bindValue(0, s_keyWriteTest);
        query.bindValue(1, QStringLiteral("test"));
        queryExec(query);
        m_isWritable = true;
    } catch (...) {
        m_isWritable = false;
    }

    // Here, we don't interact with the database anymore, so we can rollback the transaction
    // so that no changes will be actually written to the database
    rollbackTransaction(query);

    // If the database is writable, we have to check if it's locked,
    // either from SQLite itself, or by another instance.

    if (m_isWritable) {
        if (dbIsLocked) {
            closeDatabaseConnection();
            m_errorMessage = tr(
                "<p><b>Unerwarteter Fehler:</b> Die Datenbank ist gesperrt</p>"
                "<p>Liegt die Datenbank auf einer Windows-Netzwerk-Freigabe (bzw. einem "
                "Samba-Share/CIFS-Mount)? Das Öffnen einer solchen Datenbank ist nur mit "
                "dateisystemseitigem Schreibschutz möglich.</p>"
                "<p>Ansonsten sollte das nicht passieren, sofern kein anderer Prozess die "
                "Datenbank gesperrt hat (und das sollte eigentlich auch nicht der Fall sein)!");
            return Error::CouldNotOpen;
        }

        // If the database is writable, we have to make sure we're the only instance using it
        if (! lockDatabase()) {
            closeDatabaseConnection();
            m_errorMessage = tr(
                "<p>Die Datenbank ist bereits von einer anderen Instanz von Muckturnier geöffnet."
                "</p>"
                "<p>Sollte dies nicht der Fall sein (evtl. nach einen Crash, Stromausfall etc.), "
                "dann bitte die Lock-Datei „%1“ manuell löschen und die Datenbank erneut öffnen."
                "</p>").arg(QDir::toNativeSeparators(lockFileName(dbFile)).toHtmlEscaped());
            return Error::CouldNotOpen;
        }
    }

    // Check the database revision

    if (dbVersion < s_dbVersion) {
        closeDatabaseConnection();
    }

    if (dbVersion == 3) {
        return Error::DbVersion3;
    } else if (dbVersion == 4) {
        return Error::DbVersion4;
    } else if (dbVersion == 5) {
        return Error::DbVersion5;
    } else if (dbVersion == 6) {
        return Error::DbVersion6;
    } else if (dbVersion == 7) {
        return Error::DbVersion7;
    } else if (dbVersion == 8) {
        return Error::DbVersion8;
    } else if (dbVersion == 9) {
        return Error::DbVersion9;
    } else if (dbVersion == 10) {
        return Error::DbVersion10;
    } else if (dbVersion == 11) {
        return Error::DbVersion11;
    } else if (dbVersion == 12) {
        return Error::DbVersion12;
    } else if (dbVersion == 13) {
        return Error::DbVersion13;

    } else if (dbVersion != s_dbVersion) {
        // This should not happen, as long as we update this code block with each version bump :-P
        m_errorMessage = tr(
            "<p>Die Datenbank hat die falsche Revision (ist %2, die benutzte Version von "
            "Muckturnier nutzt %3). Die Datenbank kann nicht geöffnet werden").arg(
                QString::number(dbVersion), QString::number(s_dbVersion));
        closeDatabaseConnection();
        return Error::CouldNotOpen;
    }

    // Check the settings revision

    int settingsVersion = 0;

    try {
        queryPrepare(query, QStringLiteral("SELECT value FROM config WHERE key = ?"));
        query.bindValue(0, s_keySettingsVersion);
        queryExec(query);
        if (! query.next()) {
            m_errorMessage = tr(
                "<p>Die Revision der Einstellungen konnte nicht ausgelesen werden. Entweder ist "
                "die Datenbank beschädigt, oder sie wurde unsachgemäß manuell verändert.</p>");
            closeDatabaseConnection();
            return Error::CouldNotOpen;
        }
        settingsVersion = query.value(0).toInt();
    } catch (...) {
        m_errorMessage = tr(
            "<p>Die Revision der Einstellungen konnte nicht ausgelesen werden. Entweder ist die "
            "Datenbank beschädigt, oder sie wurde unsachgemäß manuell verändert.</p>")
            + sqlErrorText(query);
        closeDatabaseConnection();
        return Error::CouldNotOpen;
    }

    if (settingsVersion > m_tournamentSettings->version()) {
        // This should not happen
        m_errorMessage = tr(
            "<p>Die in der Datenbank für die Einstellungen benutzte Revision %1 ist unbekannt und "
            "kann nicht verarbeitet werden.</p>"
            "<p>Vermutlich wurde die Datei mit einer neueren Version von Muckturnier erstellt. "
            "Bitte <a href=\"%2\">aktualisieren</a>!").arg(QString::number(settingsVersion),
                                                           Urls::download);
        closeDatabaseConnection();
        return Error::CouldNotOpen;
    }

    // Everything is fine :-)
    return Error::NoError;
}

// Functions using SQL, used while opening/creating a database
// ===========================================================

bool Database::checkIfDbIsLocked()
{
    QSqlQuery query(m_db);
    if (query.exec(QStringLiteral("BEGIN EXCLUSIVE"))) {
        query.exec(QStringLiteral("COMMIT"));
        return false;
    } else {
        return true;
    }
}

bool Database::create(const QString &dbFile)
{
    if (! connectDatabase(dbFile)) {
        return false;
    }

    if (checkIfDbIsLocked()) {
        closeDatabaseConnection();
        m_errorMessage = tr("<p><b>Unerwarteter Fehler:</b> Die Datenbank ist gesperrt</p>"
                            "<p>Das tritt auf, wenn eine Datenbank auf einer Windows-Netzwerk-"
                            "Freigabe (bzw. einem Samba-Share/CIFS-Mount) angelegt werden soll; "
                            "das ist nicht möglich.</p>"
                            "<p>Ansonsten sollte das nicht passieren, sofern kein anderer Prozess "
                            "die Datenbank gesperrt hat!</p>");
        return false;
    }

    if (! lockDatabase()) {
        closeDatabaseConnection();
        m_errorMessage = tr("<p><b>Unerwarteter Fehler:</b> Die Lock-Datei „%1“ konnte nicht "
                            "erstellt werden. Bitte die Zugriffsrechte des Verzeichnisses „%2“ "
                            "überprüfen!</p>").arg(
                            QDir::toNativeSeparators(m_dbFilePath).toHtmlEscaped(),
                            QDir::toNativeSeparators(lockFileName(m_dbFileName)).toHtmlEscaped());
        return false;
    }

    if (! startTransaction()) {
        return false;
    }

    QSqlQuery query(m_db);

    try {
        // Save the custom header fields
        // Using bind values in a PRAGMA call doesn't work, so we have to use a literal query
        queryExec(query, QStringLiteral("PRAGMA application_id = %1").arg(s_applicationId));
        queryExec(query, QStringLiteral("PRAGMA user_version = %1").arg(s_dbVersion));

        // Setup the tables
        queryExec(query, QStringLiteral("CREATE TABLE config(\n"
                                        "    key TEXT,\n"
                                        "    value TEXT\n"
                                        ")"));
        queryExec(query, QStringLiteral("CREATE TABLE players(\n"
                                        "    id INTEGER PRIMARY KEY,\n"
                                        "    name TEXT,\n"
                                        "    marker INTEGER,\n"
                                        "    disqualified INTEGER,\n"
                                        "    number INTEGER,\n"
                                        "    booking_checksum TEXT\n"
                                        ")"));
        queryExec(query, QStringLiteral("CREATE TABLE markers(\n"
                                        "    id INTEGER PRIMARY KEY,\n"
                                        "    name TEXT,\n"
                                        "    color INTEGER,\n"
                                        "    sorting INTEGER,\n"
                                        "    sequence INTEGER\n"
                                        ")"));
        queryExec(query, QStringLiteral("CREATE TABLE draw(\n"
                                        "    round INTEGER,\n"
                                        "    pair_or_player INTEGER,\n"
                                        "    table_number INTEGER,\n"
                                        "    pair_number INTEGER,\n"
                                        "    player_number INTEGER\n"
                                        ")"));
        queryExec(query, QStringLiteral("CREATE TABLE assignment(\n"
                                        "    round INTEGER,\n"
                                        "    pair_or_player INTEGER,\n"
                                        "    table_number INTEGER,\n"
                                        "    pair_number INTEGER,\n"
                                        "    player_number INTEGER\n"
                                        ")"));
        queryExec(query, QStringLiteral("CREATE TABLE score(\n"
                                        "    round INTEGER,\n"
                                        "    table_number INTEGER,\n"
                                        "    booger INTEGER,\n"
                                        "    pair_or_player INTEGER,\n"
                                        "    score INTEGER\n"
                                        ")"));
        queryExec(query, QStringLiteral("CREATE TABLE schedule(\n"
                                        "    round INTEGER PRIMARY KEY,\n"
                                        "    start TEXT,\n"
                                        "    end TEXT\n"
                                        ")"));
        queryExec(query, QStringLiteral("CREATE TABLE revoked_bookings(\n"
                                        "    checksum TEXT PRIMARY KEY,\n"
                                        "    revoked TEXT,\n"
                                        "    reason TEXT\n"
                                        ")"));

        // Add the draw cleanup trigger
        // This one removes all assignments when a pair or player is deleted
        queryExec(query, QStringLiteral(
            "CREATE TRIGGER cleanup_draw AFTER DELETE ON players\n"
            "FOR EACH ROW\n"
            "BEGIN\n"
            "    DELETE FROM draw WHERE pair_or_player = OLD.id;\n"
            "END"));

        // Add the fix player 2 draw trigger
        // This one makes a player 2 of a pair player 1 if player 1's draw is deleted
        queryExec(query, QStringLiteral(
            "CREATE TRIGGER fix_player2_draw AFTER DELETE ON draw\n"
            "FOR EACH ROW WHEN OLD.player_number = 1\n"
            "BEGIN\n"
            "    UPDATE draw SET player_number = 1\n"
            "    WHERE round = OLD.round\n"
            "    AND table_number = OLD.table_number\n"
            "    AND pair_number = OLD.pair_number\n"
            "    AND player_number = 2;\n"
            "END"));

        // Insert the default config keys and values
        queryPrepare(query, QStringLiteral("INSERT INTO config(key, value) VALUES(?, ?)"));
        query.addBindValue(QVariantList { s_keyDbIdentifier,
                                          s_keyDbVersion,
                                          s_keySettings,
                                          s_keySettingsVersion });
        query.addBindValue(QVariantList { s_dbIdentifier,
                                          s_dbVersion,
                                          s_sqliteNull,
                                          m_tournamentSettings->version() });
        queryExecBatch(query);

        commitTransaction();

    } catch (...) {
        const auto errorText = sqlErrorText(query);
        rollbackTransaction(query);
        m_errorMessage = tr("<p>Beim Erstellen der Datenbankstruktur ist ein Fehler aufgetreten. "
                            "Das sollte nicht passieren! Evtl. wurde die Datei während des "
                            "Erstellens von einem anderen Prozess verändert?</p>")
                         + errorText;
        m_isWritable = false;
        return false;
    }

    updateDbFileInfo();
    m_pairsOrPlayersCount = 0;
    m_scoresPerRound = 0;
    m_roundsState = QMap<int, bool> { { 1, false } };
    m_scoresPresent.clear();
    m_isWritable = true;
    m_tournamentStarted = false;
    m_emitSqlErrors = true;

    return true;
}

bool Database::checkIfTournamentStarted()
{
    QSqlQuery query(m_db);
    try {
        queryExec(query, QStringLiteral("SELECT COUNT(round) FROM score"));
        query.next();
        m_tournamentStarted = query.value(0).toInt() != 0;
        return true;
    } catch (...) {
        if (! m_emitSqlErrors) {
            m_errorMessage = tr("<p>Beim Zählen der Ergebnisse ist ein Fehler aufgetreten. "
                                "Entweder ist die Datenbank „%1“ beschädigt, oder sie wurde "
                                "unsachgemäß manuell verändert.</p>").arg(
                                QDir::toNativeSeparators(m_dbFileName).toHtmlEscaped())
                             + sqlErrorText(query);
        }
        return false;
    }
}

bool Database::updateRoundsState()
{
    const auto oldRoundsState = m_roundsState;
    m_roundsState.clear();
    m_scoresPresent.clear();

    QSqlQuery query(m_db);

    try {
        queryExec(query, QStringLiteral("SELECT round, COUNT(round) FROM score GROUP BY round"));
        while (query.next()) {
            const int round = query.value(0).toInt();
            const int scores = query.value(1).toInt();
            m_roundsState[round] = scores == m_scoresPerRound;
            m_scoresPresent[round] = scores > 0;
        }
    } catch (...) {
        if (! m_emitSqlErrors) {
            m_errorMessage = tr("<p>Beim Aktualisieren des Rundenstatus ist ein Fehler "
                                "aufgetreten. Entweder ist die Datenbank „%1“ beschädigt, oder "
                                "sie wurde unsachgemäß manuell verändert.</p>").arg(
                                QDir::toNativeSeparators(m_dbFileName).toHtmlEscaped())
                             + sqlErrorText(query);
        }
        return false;
    }

    // Be sure to have a state for each round. All data from a round could have been deleted.
    // If we have no data, round 1 will be added as open.
    const int lastRound = m_roundsState.isEmpty() ? 1 : m_roundsState.lastKey();
    for (int i = 1; i <= lastRound; i++) {
        if (! m_roundsState.contains(i)) {
            m_roundsState[i] = false;
        }
    }

    m_allRounds = m_roundsState.count();

    if (oldRoundsState != m_roundsState) {
        Q_EMIT roundsStateChanged();
    }

    return true;
}

bool Database::updateDatabase(const QString &dbFileName, Error openError)
{
    if (! connectDatabase(dbFileName) || ! startTransaction()) {
        return false;
    }

    DatabaseUpdater updater;
    QSqlQuery query(m_db);

    try {
        switch (openError) {
        case NoError:
        case CouldNotOpen:
            Q_UNREACHABLE();
            break;
        case Error::DbVersion3:
            updater.updateFromV3(query);
            [[fallthrough]];
        case Error::DbVersion4:
            updater.updateFromV4(query);
            [[fallthrough]];
        case Error::DbVersion5:
            updater.updateFromV5(query);
            [[fallthrough]];
        case Error::DbVersion6:
            updater.updateFromV6(query);
            [[fallthrough]];
        case Error::DbVersion7:
            updater.updateFromV7(query);
            [[fallthrough]];
        case Error::DbVersion8:
            updater.updateFromV8(query);
            [[fallthrough]];
        case Error::DbVersion9:
            updater.updateFromV9(query);
            [[fallthrough]];
        case Error::DbVersion10:
            updater.updateFromV10(query);
            [[fallthrough]];
        case Error::DbVersion11:
            updater.updateFromV11(query);
            [[fallthrough]];
        case Error::DbVersion12:
            updater.updateFromV12(query);
            [[fallthrough]];
        case Error::DbVersion13:
            updater.updateFromV13(query);
        }

        commitTransaction();
        closeDatabaseConnection();

    } catch (...) {
        m_errorMessage = sqlErrorText(query);
        rollbackTransaction(query);
        closeDatabaseConnection();
        m_isWritable = false;
        return false;
    }

    return true;
}

// =============================================================
// Functions using SQL, used while working on an opened database
// =============================================================

// Functions reading data
// ======================

TSuccess<QStringList> Database::players()
{
    QStringList result;
    bool success;

    readHelper([this, &result](QSqlQuery &query) {
        queryExec(query, QStringLiteral("SELECT name FROM players"));
        while (query.next()) {
            result.append(query.value(0).toString());
        }
    }, success);

    return { result, success };
}

TSuccess<QVector<Players::Data>> Database::playersData()
{
    QVector<Players::Data> result;
    bool success;

    readHelper([this, &result](QSqlQuery &query) {
        // Get all draws

        queryExec(query, QStringLiteral("SELECT "
                                            "pair_or_player, "
                                            "round, "
                                            "table_number, "
                                            "pair_number, "
                                            "player_number "
                                        "FROM draw"));

        QHash<int, QMap<int, Draw::Seat>> draw;
        while (query.next()) {
            const int id = query.value(0).toInt();
            if (! draw.contains(id)) {
                draw.insert(id, QMap<int, Draw::Seat>());
            }
            draw[id].insert(query.value(1).toInt(), Draw::Seat { query.value(2).toInt(),
                                                                 query.value(3).toInt(),
                                                                 query.value(4).toInt() });
        }

        // Get all registrations data

        queryExec(query, QStringLiteral("SELECT "
                                            "id, "
                                            "name, "
                                            "number, "
                                            "marker, "
                                            "disqualified, "
                                            "booking_checksum "
                                        "FROM players"));

        while (query.next()) {
            const int id = query.value(0).toInt();
            result.append({
                id,                                      // id
                query.value(1).toString(),               // name
                query.value(2).toInt(),                  // number
                query.value(3).toInt(),                  // marker
                draw.value(id, QMap<int, Draw::Seat>()), // draw
                query.value(4).toInt(),                  // disqualified
                query.value(5).toString()                // bookingChecksum
            });
        }

    }, success);

    return { result, success };
}

TSuccess<QVector<int>> Database::missingTables(int round)
{
    QVector<int> result;
    bool success;

    readHelper([this, round, &result](QSqlQuery &query) {
        queryPrepare(query, QStringLiteral("SELECT DISTINCT table_number FROM score "
                                           "WHERE round = ?"));
        query.bindValue(0, round);
        queryExec(query);

        QVector<int> tablesWithScores;
        while (query.next()) {
            tablesWithScores.append(query.value(0).toInt());
        }
        result.reserve(m_tablesCount - tablesWithScores.size());

        for (int i = 1; i <= m_tablesCount; i++) {
            if (! tablesWithScores.contains(i)) {
                result.append(i);
            }
        }
    }, success);

    return { result, success };
}

TSuccess<QVector<Players::DisplayData>> Database::missingPlayers(int round)
{
    QVector<Players::DisplayData> result;
    bool success;

    readHelper([this, round, &result](QSqlQuery &query) {
        queryPrepare(query, QStringLiteral(
            "SELECT "
                "players.name, "
                "players.number, "
                "players.disqualified IS NOT NULL AND players.disqualified <= :round, "
                "players.id "
            "FROM players "
            "WHERE NOT EXISTS ("
                "SELECT score.pair_or_player FROM score "
                "WHERE players.id = score.pair_or_player "
                "AND round = :round"
            ")"));
        query.bindValue(QStringLiteral(":round"), round);
        queryExec(query);
        while (query.next()) {
            result.append({
                query.value(0).toString(), // DisplayData.name
                query.value(1).toInt(),    //            .number
                query.value(2).toBool(),   //            .disqualified
                query.value(3).toInt()     //            .id
            });
        }
    }, success);

    return { result, success };
}

TSuccess<QHash<int, Draw::Seat>> Database::draw(int round)
{
    QHash<int, Draw::Seat> result;
    bool success;

    readHelper([this, round, &result](QSqlQuery &query) {
        queryPrepare(query, QStringLiteral(
            "SELECT pair_or_player, table_number, pair_number, player_number "
            "FROM draw WHERE round = :round"));
        query.bindValue(QStringLiteral(":round"), round);
        queryExec(query);

        while(query.next()) {
            const int id = query.value(0).toInt();
            const int table = query.value(1).toInt();
            const int pair = query.value(2).toInt();
            const int player = query.value(3).toInt();
            result.insert(id, Draw::Seat { table, pair, player });
        }
    }, success);

    return { result, success };
}

TSuccess<QHash<int, Draw::Seat>> Database::assignment(int round)
{
    QHash<int, Draw::Seat> result;
    bool success;

    readHelper([this, round, &result](QSqlQuery &query) {
        queryPrepare(query, QStringLiteral(
            "SELECT pair_or_player, table_number, pair_number, player_number "
            "FROM assignment WHERE round = :round"));
        query.bindValue(QStringLiteral(":round"), round);
        queryExec(query);

        while(query.next()) {
            const int id = query.value(0).toInt();
            const int table = query.value(1).toInt();
            const int pair = query.value(2).toInt();
            const int player = query.value(3).toInt();
            result.insert(id, Draw::Seat { table, pair, player });
        }
    }, success);

    return { result, success };
}

TSuccess<Scores::Score> Database::scores(int round)
{
    Scores::Score score;
    bool success;

    readHelper([this, round, &score](QSqlQuery &query) {
        switch (m_tournamentSettings->tournamentMode()) {

        case Tournament::FixedPairs:
            queryPrepare(query, QStringLiteral(
                "SELECT "
                    "score.table_number, "
                    "score.score, "
                    "players.name, "
                    "players.number, "
                    "players.disqualified IS NOT NULL AND players.disqualified <= :round "
                "FROM score "
                "INNER JOIN players ON "
                    "players.id = score.pair_or_player "
                "INNER JOIN assignment ON "
                    "assignment.round = score.round "
                    "AND assignment.pair_or_player = score.pair_or_player "
                "WHERE score.round = :round "
                "ORDER BY "
                    "score.table_number, "
                    "assignment.pair_number, "
                    "score.booger"
            ));
            query.bindValue(QStringLiteral(":round"), round);
            break;

        case Tournament::SinglePlayers:
            // It's a bit more complicated for single players ;-)
            queryPrepare(query, QStringLiteral(
                "SELECT "
                    "score_one.table_number, "
                    "score_one.score, "
                    "player_one.name, "
                    "player_one.number, "
                    "player_one.disqualified IS NOT NULL AND player_one.disqualified <= :round, "
                    "player_two.name, "
                    "player_two.number, "
                    "player_two.disqualified IS NOT NULL AND player_two.disqualified <= :round "
                "FROM "
                    "score AS score_one "
                "INNER JOIN players AS player_one ON "
                    "player_one.id = score_one.pair_or_player "
                "INNER JOIN assignment AS assignment_one ON "
                    "assignment_one.round = score_one.round "
                    "AND assignment_one.table_number = score_one.table_number "
                    "AND assignment_one.pair_or_player = score_one.pair_or_player "
                "INNER JOIN players AS player_two ON "
                    "player_two.id = score_two.pair_or_player "
                "INNER JOIN score AS score_two ON "
                    "score_two.round = score_one.round "
                    "AND score_two.table_number = score_one.table_number "
                    "AND score_two.booger = score_one.booger "
                    "AND assignment_two.pair_number = assignment_one.pair_number "
                    "AND assignment_two.player_number = 2 "
                "INNER JOIN assignment AS assignment_two ON "
                    "assignment_two.round = score_one.round "
                    "AND assignment_two.table_number = score_one.table_number "
                    "AND assignment_two.pair_or_player = score_two.pair_or_player "
                "WHERE "
                    "score_one.round = :round "
                    "AND assignment_one.player_number = 1 "
                "ORDER BY "
                    "score_one.table_number, "
                    "assignment_one.pair_number, "
                    "score_one.booger "
            ));
            query.bindValue(QStringLiteral(":round"), round);
            break;
        }

        queryExec(query);

        const int boogersPerRound = m_tournamentSettings->boogersPerRound();
        const int boogersPerTable = boogersPerRound * 2;
        int row = 0;

        QVector<int> pairResults;
        pairResults.reserve(boogersPerRound);

        // Here, we sort out the select result

        const auto isSinglePlayersMode = m_tournamentSettings->isSinglePlayerMode();

        while (query.next()) {
            // Every boogersPerTable rows, a new table starts. We add the number to score.tables:
            if (row % (boogersPerTable) == 0) {
                score.tables.append(query.value(0).toInt());
            }

            // Each row contains a result we collect here:
            row++;
            pairResults.append(query.value(1).toInt());
            if (row % boogersPerRound == 0) {
                // If we have all scores of a booger, we add them to the scores list:
                score.scores.append(pairResults);
                pairResults.clear();
            }

            // For each table, we have two half parts: All scores for pair 1 and all for pair 2.
            // We have to add pair 1's name when we start and pair 2's name after boogersPerRound
            // rows:

            if (row % boogersPerRound == 0) {
                // We always have a pair or player1 name along with it's disqualified state
                score.pairsOrPlayers1.append({
                    query.value(2).toString(), // Score::PlayersData.name
                    query.value(3).toInt(),    //                   .number
                    query.value(4).toBool(),   //                   .disqualified
                    -1                         //                   .id is not used
                });

                // In single players mode, there's also a player 2 dataset
                if (isSinglePlayersMode) {
                    score.players2.append({
                        query.value(5).toString(),  // Score::PlayersData.name
                        query.value(6).toInt(),     //                   .number
                        query.value(7).toBool(),    //                   .disqualified
                        -1                          //                   .id is not used
                    });
                }
            }
        }

    }, success);

    return { score, success };
}

TSuccess<QVector<Database::BoogerResult>> Database::singleScore(int round, int table)
{
    QVector<BoogerResult> result;
    bool success;

    readHelper([this, round, table, &result](QSqlQuery &query) {
        queryPrepare(query, QStringLiteral(
            "SELECT "
                "score.pair_or_player, "
                "score.score "
            "FROM score "
            "INNER JOIN assignment ON "
                "assignment.round = score.round "
                "AND assignment.pair_or_player = score.pair_or_player "
            "WHERE "
                "score.round = ? "
                "AND score.table_number = ? "
            "ORDER BY "
                "assignment.pair_number, "
                "assignment.player_number, "
                "score.booger"
        ));
        query.bindValue(0, round);
        query.bindValue(1, table);
        queryExec(query);

        while (query.next()) {
            BoogerResult boogerResult;
            boogerResult.pairOrPlayer = query.value(0).toInt();
            boogerResult.score = query.value(1).toInt();
            result.append(boogerResult);
        }
    }, success);

    return { result, success };
}

TSuccess<QVector<Database::RankData>> Database::ranking(int upToRound)
{
    QVector<RankData> result;
    bool success;

    if (upToRound == -1) {
        upToRound = roundsState().count();
    }

    readHelper([this, upToRound, &result](QSqlQuery &query) {
        queryPrepare(query, QStringLiteral(
            "SELECT "
                "players.name, "
                "players.number, "
                "SUM(CASE "
                        "WHEN one_score.score = :booger_score THEN 1 "
                        "ELSE 0 "
                    "END) AS won_boogers, "
                "SUM(CASE "
                        "WHEN one_score.score = :booger_score THEN 0 "
                        "ELSE one_score.score "
                    "END) AS scored_goals, "
                "SUM(CASE "
                        "WHEN other_score.score = :booger_score THEN 0 "
                        "ELSE other_score.score "
                    "END) AS opponent_goals, "
                "COUNT(one_score.score) AS played_boogers "
            "FROM "
                "score AS one_score "
            "INNER JOIN players ON "
                "players.id = one_score.pair_or_player "
            "INNER JOIN assignment AS one_assignment ON "
                "one_assignment.round = one_score.round "
                "AND one_assignment.table_number = one_score.table_number "
                "AND one_assignment.pair_or_player = one_score.pair_or_player "
            "INNER JOIN assignment AS other_assignment ON "
                "other_assignment.round = other_score.round "
                "AND other_assignment.table_number = other_score.table_number "
                "AND other_assignment.pair_or_player = other_score.pair_or_player "
            "INNER JOIN score AS other_score ON "
                "other_score.round = one_score.round "
                "AND other_score.table_number = one_score.table_number "
                "AND other_score.booger = one_score.booger "
                "AND other_assignment.player_number = one_assignment.player_number "
                "AND other_assignment.pair_number != one_assignment.pair_number "
            "WHERE "
                "one_score.round <= :up_to_round "
                "AND ("
                    "players.disqualified IS NULL "
                    "OR players.disqualified > :up_to_round"
                ") "
            "GROUP BY "
                "one_score.pair_or_player "
            "ORDER BY "
                "won_boogers DESC, "
                "scored_goals DESC, "
                "opponent_goals, "
                "players.name"
        ));
        query.bindValue(QStringLiteral(":booger_score"), m_tournamentSettings->boogerScore());
        query.bindValue(QStringLiteral(":up_to_round"), upToRound);
        queryExec(query);

        int rankingLine = 0;
        RankData lastRank;
        lastRank.rank = 0;
        lastRank.boogers = 0;
        lastRank.goals = 0;
        lastRank.opponentGoals = 0;

        while (query.next()) {
            RankData currentRank {
                -1,                        // rank (defined below)
                query.value(0).toString(), // name
                query.value(1).toInt(),    // number
                query.value(2).toInt(),    // boogers
                query.value(3).toInt(),    // goals
                query.value(4).toInt(),    // opponentGoals
                query.value(5).toInt()     // playedBoogers
            };

            rankingLine++;
            if (ranksDiffer(currentRank, lastRank)) {
                currentRank.rank = rankingLine;
            } else {
                currentRank.rank = lastRank.rank;
            }

            result.append(currentRank);
            lastRank = currentRank;
        }

        if (result.count() < m_pairsOrPlayersCount) {
            // Some pair(s)/player(s) don't have a score yet,
            // so we insert empty scores to get a complete ranking list

            queryExec(query, QStringLiteral("SELECT players.name FROM players "
                                            "WHERE NOT EXISTS ( "
                                                "SELECT score.pair_or_player "
                                                "FROM score "
                                                "WHERE players.id = score.pair_or_player "
                                            ")"));

            QStringList missingNames;
            while (query.next()) {
                missingNames.append(query.value(0).toString());
            }
            m_stringTools->sort(missingNames);

            RankData missingRank;
            missingRank.boogers = 0;
            missingRank.goals = 0;
            missingRank.opponentGoals = 0;
            missingRank.playedBoogers = 0;
            if (ranksDiffer(missingRank, lastRank)) {
                missingRank.rank = lastRank.rank + 1;
            } else {
                missingRank.rank = lastRank.rank;
            }

            for (const QString &name : std::as_const(missingNames)) {
                missingRank.name = name;
                result.append(missingRank);
            }
        }
    }, success);

    return { result, success };
}

TSuccess<QVector<Markers::Marker>> Database::markers()
{
    QVector<Markers::Marker> result;
    bool success;

    readHelper([this, &result](QSqlQuery &query) {
        queryExec(query, QStringLiteral("SELECT id, name, color, sorting FROM markers "
                                        "ORDER BY sequence"));
        while (query.next()) {
            const auto id = query.value(0).toInt();
            Markers::Marker marker;
            marker.id = id;
            marker.name = query.value(1).toString();
            marker.color = QColor(query.value(2).toInt());
            marker.sorting = static_cast<Markers::MarkerSorting>(query.value(3).toInt());
            result.append(marker);
        }
    }, success);

    bool unmarkedPresent = false;
    for (const auto &marker : result) {
        if (marker.id == 0) {
            unmarkedPresent = true;
            break;
        }
    }

    if (! unmarkedPresent) {
        qCWarning(MuckturnierLog) << "The \"unmarked\" marker is not present in the database! This "
                                  << "should not happen! Adding it.";
        result.append(Markers::unmarkedMarker);
    }

    return { result, success };
}

TSuccess<QMap<int, QVector<QString>>> Database::disqualifiedPlayers(int round)
{
    if (round == -1) {
        round = roundsState().count();
    }

    QHash<QString, int> result;
    bool success;

    readHelper([this, round, &result](QSqlQuery &query) {
        queryPrepare(query, QStringLiteral("SELECT name, disqualified FROM players "
                                           "WHERE disqualified IS NOT NULL "
                                           "AND disqualified <= :round"));
        query.bindValue(QStringLiteral(":round"), round);
        query.exec();

        while (query.next()) {
            result.insert(query.value(0).toString(), query.value(1).toInt());
        }
    }, success);

    if (! success) {
        return { { }, success };
    }

    QMap<int, QVector<QString>> sortedResult;

    // Group all names by round
    QHash<QString, int>::const_iterator resultIterator = result.constBegin();
    while (resultIterator != result.constEnd()) {
        sortedResult[resultIterator.value()].append(resultIterator.key());
        resultIterator++;
    }

    // Oder all names alphabetically for all rounds
    QMap<int, QVector<QString>>::iterator sortIterator = sortedResult.begin();
    while (sortIterator != sortedResult.end()) {
        auto list = sortIterator.value();
        m_stringTools->sort(list);
        sortedResult[sortIterator.key()] = list;
        sortIterator++;
    }

    return { sortedResult, success };
}

// Functions writing data
// ======================

bool Database::reset()
{
    if (! writeHelper([this](QSqlQuery &query) {
        queryExec(query, QStringLiteral("DELETE FROM players"));
        queryExec(query, QStringLiteral("DELETE FROM markers"));
        queryExec(query, QStringLiteral("DELETE FROM draw"));
        queryExec(query, QStringLiteral("DELETE FROM assignment"));
        queryExec(query, QStringLiteral("DELETE FROM score"));
        queryExec(query, QStringLiteral("DELETE FROM schedule"));
        queryExec(query, QStringLiteral("DELETE FROM revoked_bookings"));
    })) {
        return false;
    }

    m_pairsOrPlayersCount = 0;
    m_scoresPerRound = 0;
    m_roundsState = QMap<int, bool> { { 1, false } };
    m_scoresPresent.clear();
    m_tournamentStarted = false;

    Q_EMIT roundsStateChanged();

    return true;
}

bool Database::saveSettings()
{
    if (   ! isOpen()
        || ! isWritable()
        || (! isChangeable() && ! m_tournamentSettings->tournamentOpenChanged())) {

        return true;
    }

    return writeHelper([this](QSqlQuery &query) {
        queryPrepare(query, QStringLiteral("UPDATE config SET value = ? WHERE key = ?"));
        query.addBindValue(QVariantList { m_tournamentSettings->version(),
                                          m_tournamentSettings->toString() });
        query.addBindValue(QVariantList { s_keySettingsVersion,
                                          s_keySettings });
        queryExecBatch(query);
    });
}

bool Database::registerPlayers(const QString &name, int markerId)
{
    return writeHelper([this, &name, markerId](QSqlQuery &query) {
        queryPrepare(query, QStringLiteral("INSERT INTO players(id, name, marker) "
                                           "VALUES(NULL, ?, ?)"));
        query.bindValue(0, name);
        query.bindValue(1, markerId != 0 ? markerId : s_sqliteNull);
        queryExec(query);
    });
}

bool Database::registerPlayers(const QStringList &names)
{
    return writeHelper([this, &names](QSqlQuery &query) {
        registerPlayers(query, names);
    });
}

void Database::registerPlayers(QSqlQuery &query, const QStringList &names)
{
    queryPrepare(query, QStringLiteral("INSERT INTO players(id, name) VALUES(NULL, ?)"));
    query.addBindValue(QVariantList(names.begin(), names.end()));
    queryExecBatch(query);
}

bool Database::deletePlayers(const QString &name)
{
    return writeHelper([this, &name](QSqlQuery &query) {
        deletePlayers(query, name);
    });
}

void Database::deletePlayers(QSqlQuery &query, const QString &name)
{
    // If we have a booking saved: Revoke it
    revokeBooking(query, name, Booking::RegistrationDeleted);

    // Delete the registration
    queryPrepare(query, QStringLiteral("DELETE FROM players WHERE name = ?"));
    query.bindValue(0, name);
    queryExec(query);
}

bool Database::renamePlayers(const QString &oldName, const QString &newName, int newMarker)
{
    return writeHelper([this, &oldName, &newName, newMarker](QSqlQuery &query) {
        renamePlayers(query, oldName, newName, newMarker);
    });
}

void Database::renamePlayers(QSqlQuery &query, const QString &oldName, const QString &newName,
                             int newMarker)
{
    queryPrepare(query, QStringLiteral("UPDATE players SET name = ? WHERE name = ?"));
    query.bindValue(0, newName);
    query.bindValue(1, oldName);
    queryExec(query);

    if (newMarker != -1) {
        setMarker(query, newName, newMarker);
    }
}

bool Database::renamePlayers(const QStringList &oldNames, const QStringList &newNames)
{
    return writeHelper([this, &oldNames, &newNames](QSqlQuery &query) {
        queryPrepare(query, QStringLiteral("UPDATE players SET name = ? WHERE name = ?"));
        query.addBindValue(QVariantList(newNames.begin(), newNames.end()));
        query.addBindValue(QVariantList(oldNames.begin(), oldNames.end()));
        queryExecBatch(query);
    });
}

bool Database::saveScore(int round, int table, const QVector<Database::BoogerResult> &score)
{
    const bool scoreSaved = writeHelper([this, round, table, &score](QSqlQuery &query) {
        switch (m_tournamentSettings->tournamentMode()) {
        case Tournament::FixedPairs:
            updateAssignment(query, round,
                             score.at(0).pairOrPlayer,
                             1, 1, table);
            updateAssignment(query, round,
                             score.at(m_tournamentSettings->boogersPerRound()).pairOrPlayer,
                             2, 1, table);
            break;
        case Tournament::SinglePlayers:
            updateAssignment(query, round,
                             score.at(0).pairOrPlayer,
                             1, 1, table);
            updateAssignment(query, round,
                             score.at(m_tournamentSettings->boogersPerRound()).pairOrPlayer,
                             1, 2, table);
            updateAssignment(query, round,
                             score.at(m_tournamentSettings->boogersPerRound() * 2).pairOrPlayer,
                             2, 1, table);
            updateAssignment(query, round,
                             score.at(m_tournamentSettings->boogersPerRound() * 3).pairOrPlayer,
                             2, 2, table);
            break;
        }

        int booger = 1;
        for (const BoogerResult &boogerResult : score) {
            queryPrepare(query, QStringLiteral(
                "INSERT INTO score(round, table_number, booger, pair_or_player, score) "
                "VALUES(?, ?, ?, ?, ?)"
            ));
            query.bindValue(0, round);
            query.bindValue(1, table);
            query.bindValue(2, booger);
            query.bindValue(3, boogerResult.pairOrPlayer);
            query.bindValue(4, boogerResult.score);
            queryExec(query);

            booger++;
            if (booger > m_tournamentSettings->boogersPerRound()) {
                booger = 1;
            }
        }
    });

    if (! scoreSaved || ! updateRoundsState()) {
        return false;
    }

    m_tournamentStarted = true;
    return true;
}

void Database::updateAssignment(QSqlQuery &query, int round, int pairOrPlayer, int pairNumber,
                                int playerNumber, int table)
{
    queryPrepare(query, QStringLiteral(
        "INSERT INTO assignment(round, pair_or_player, pair_number, player_number, table_number) "
        "VALUES(?, ?, ?, ?, ?)"
    ));
    query.bindValue(0, round);
    query.bindValue(1, pairOrPlayer);
    query.bindValue(2, pairNumber);
    query.bindValue(3, playerNumber);
    query.bindValue(4, table);
    queryExec(query);
}

bool Database::deleteScore(int round, int table)
{
    const bool scoreDeleted = writeHelper([this, round, table](QSqlQuery &query) {
        queryPrepare(query, QStringLiteral("DELETE FROM score "
                                           "WHERE round = ? "
                                           "AND table_number = ?"));
        query.bindValue(0, round);
        query.bindValue(1, table);
        queryExec(query);

        queryPrepare(query, QStringLiteral("DELETE FROM assignment "
                                           "WHERE round = ? "
                                           "AND table_number = ?"));
        query.bindValue(0, round);
        query.bindValue(1, table);
        queryExec(query);
    });

    if (! scoreDeleted || ! checkIfTournamentStarted() || ! updateRoundsState()) {
        return false;
    }

    return true;
}

bool Database::swapPairs(int round, int table)
{
    return writeHelper([this, round, table](QSqlQuery &query) {
        queryPrepare(query, QStringLiteral(
            "UPDATE assignment "
            "SET pair_number = CASE WHEN pair_number = 1 THEN 2 ELSE 1 END "
            "WHERE round = ? AND table_number = ?"));
        query.bindValue(0, round);
        query.bindValue(1, table);
        queryExec(query);
    });
}

bool Database::swapPlayers(int round, int table, int pair)
{
    return writeHelper([this, round, table, pair](QSqlQuery &query) {
        queryPrepare(query, QStringLiteral(
            "UPDATE assignment "
            "SET player_number = CASE WHEN player_number = 1 THEN 2 ELSE 1 END "
            "WHERE round = ? AND table_number = ? AND pair_number = ?"));
        query.bindValue(0, round);
        query.bindValue(1, table);
        query.bindValue(2, pair);
        queryExec(query);
    });
}

bool Database::addMarkers(const QVector<Markers::Marker> &markers)
{
    return writeHelper([this, &markers](QSqlQuery &query) {
        for (const Markers::Marker &marker : markers) {
            addMarker(query, marker);
        }
    });
}

bool Database::addMarker(const Markers::Marker &marker)
{
    return writeHelper([this, &marker](QSqlQuery &query) {
        addMarker(query, marker);
    });
}

void Database::addMarker(QSqlQuery &query, const Markers::Marker &marker)
{
    queryPrepare(query, QStringLiteral("INSERT INTO markers(id, name, color, sorting, sequence) "
                                       "VALUES(?, ?, ?, ?, "
                                              "(SELECT COUNT(sequence) FROM markers))"));
    // An ID can either be set (marker.id != -1), or it can be requested (marker.id == -1).
    // In this case, we insert SQLite's NULL value as the ID, so that a new ID is allocated
    // automatically by the INTEGER PRIMARY KEY id column.
    query.bindValue(0, marker.id != -1 ? marker.id : s_sqliteNull);
    query.bindValue(1, marker.name);
    query.bindValue(2, marker.id != 0 ? marker.color.rgb() : s_sqliteNull);
    query.bindValue(3, marker.sorting);
    queryExec(query);
}

bool Database::deleteMarker(int id, int sequence)
{
    return writeHelper([this, id, sequence](QSqlQuery &query) {
        // Delete the marker
        queryPrepare(query, QStringLiteral("DELETE FROM markers WHERE id = ?"));
        query.bindValue(0, id);
        queryExec(query);

        // Close gaps in the sequence counter
        queryPrepare(query, QStringLiteral("UPDATE markers SET sequence = sequence - 1 "
                                           "WHERE sequence > ?"));
        query.bindValue(0, sequence);
        queryExec(query);

        // Remove all respective marker links from the pairs/players list
        queryPrepare(query, QStringLiteral("UPDATE players SET marker = NULL WHERE marker = ?"));
        query.bindValue(0, id);
        queryExec(query);
    });
}

bool Database::editMarker(int id, const QString &name, const QColor &color,
                          Markers::MarkerSorting sorting)
{
    return writeHelper([this, id, &name, &color, sorting](QSqlQuery &query) {
        queryPrepare(query, QStringLiteral("UPDATE markers SET name = ?, color = ?, sorting = ? "
                                           "WHERE id = ?"));
        query.bindValue(0, name);
        query.bindValue(1, color.rgb());
        query.bindValue(2, sorting);
        query.bindValue(3, id);
        queryExec(query);
    });
}

bool Database::moveMarker(int id, int sequence, int direction)
{
    return writeHelper([this, id, sequence, direction](QSqlQuery &query) {
        if ((sequence == 0 && direction == 1) || (sequence == 1 && direction == -1)) {
            // Be sure that the new first marker will create a new block
            queryPrepare(query, QStringLiteral("UPDATE markers SET sorting = ? "
                                               "WHERE sequence = 1"));
            query.bindValue(0, Markers::MarkerSorting::NewBlock);
            queryExec(query);
        }

        queryPrepare(query, QStringLiteral("UPDATE markers SET sequence = sequence - ? "
                                           "WHERE sequence = ?"));
        query.bindValue(0, direction);
        query.bindValue(1, sequence + direction);
        queryExec(query);

        queryPrepare(query, QStringLiteral("UPDATE markers SET sequence = ? WHERE id = ?"));
        query.bindValue(0, sequence + direction);
        query.bindValue(1, id);
        queryExec(query);
    });
}

bool Database::setMarker(const QString &name, int markerId)
{
    return writeHelper([this, &name, markerId](QSqlQuery &query) {
        setMarker(query, name, markerId);

        if (markerId == m_tournamentSettings->specialMarker(Markers::Booked)) {
            setDraw(query, 1, name, Draw::EmptySeat, -1);
        }
    });
}

void Database::setMarker(QSqlQuery &query, const QString &name, int markerId)
{
    queryPrepare(query, QStringLiteral("UPDATE players SET marker = ? WHERE name = ?"));
    query.bindValue(0, markerId != 0 ? markerId : s_sqliteNull);
    query.bindValue(1, name);
    queryExec(query);
}

bool Database::setListMarker(int markerId)
{
    return writeHelper([this, markerId](QSqlQuery &query) {
        setListMarker(query, markerId);

        if (markerId == m_tournamentSettings->specialMarker(Markers::Booked)) {
            deleteAllDraws(query, 1);
        }
    });
}

void Database::setListMarker(QSqlQuery &query, int markerId)
{
    queryPrepare(query, QStringLiteral("UPDATE players SET marker = ?"));
    query.bindValue(0, markerId != 0 ? markerId : s_sqliteNull);
    queryExec(query);
}

bool Database::removeListMarker(int markerId)
{
    return writeHelper([this, markerId](QSqlQuery &query) {
        queryPrepare(query, QStringLiteral("UPDATE players SET marker = NULL where marker = ?"));
        query.bindValue(0, markerId);
        queryExec(query);
    });
}

bool Database::deleteMarkedPlayers(int markerId)
{
    return writeHelper([this, markerId](QSqlQuery &query) {
        if (markerId != 0) {
            queryPrepare(query, QStringLiteral("DELETE FROM players where marker = ?"));
            query.bindValue(0, markerId);
            queryExec(query);
        } else {
            queryExec(query, QStringLiteral("DELETE FROM players where marker IS NULL"));
        }
    });
}

bool Database::deleteAllPlayers()
{
    return writeHelper([this](QSqlQuery &query) {
        queryExec(query, QStringLiteral("DELETE FROM players"));
    });
}

bool Database::adoptAllData(const QVector<Players::Data> &playersData,
                            const QVector<Markers::Marker> &markers)
{
    return writeHelper([this, &playersData, &markers](QSqlQuery &query) {
        // Adopt the players list
        queryExec(query, QStringLiteral("DELETE FROM players"));
        QStringList names;
        for (const auto &data : playersData) {
            names.append(data.name);
        }
        registerPlayers(query, names);

        // Adopt the markers and the draws
        adoptPlayersMetadata(query, playersData, markers);
    });
}

bool Database::adoptPlayersMetadata(const QVector<Players::Data> &playersData,
                                    const QVector<Markers::Marker> &markers)
{
    return writeHelper([this, &playersData, &markers](QSqlQuery &query) {
        adoptPlayersMetadata(query, playersData, markers);
    });
}

void Database::adoptPlayersMetadata(QSqlQuery &query,
                                    const QVector<Players::Data> &playersData,
                                    const QVector<Markers::Marker> &markers)
{
    setListMarker(query, 0);
    deleteAllDraws(query, 1);

    // Adopt all markers
    queryExec(query, QStringLiteral("DELETE FROM markers"));
    for (const auto &marker : markers) {
        addMarker(query, marker);
    }

    for (const auto &data : playersData) {
        // Adopt the players number
        setNumber(query, data.name, data.number);

        // Adopt the marker
        setMarker(query, data.name, data.marker);

        // Adopt the disqualification
        setDisqualified(query, data.name, data.disqualified);

        // Adopt the draw
        QMap<int, Draw::Seat>::const_iterator it;
        for (it = data.draw.constBegin(); it != data.draw.constEnd(); it++) {
            setDraw(query, it.key(), data.name, it.value(), -1);
        }

        // Adopt the booking checksum
        setBookingChecksum(query, data.name, data.bookingChecksum);
    }
}

bool Database::assemblePair(const QString &player1, const QString &player2,
                            const QString &separator, int assignedMarker)
{
    return writeHelper([this, &player1, &player2, &separator, assignedMarker](QSqlQuery &query) {
        const QString pairName = player1 + separator + player2;
        renamePlayers(query, player1, pairName, assignedMarker);
        deletePlayers(query, player2);
    });
}

bool Database::setDraw(int round, const QString &name, const Draw::Seat &seat, int marker)
{
    return writeHelper([this, round, &name, &seat, marker](QSqlQuery &query) {
        setDraw(query, round, name, seat, marker);
    });
}

void Database::setDraw(QSqlQuery &query, int round, const QString &name, const Draw::Seat &seat,
                       int marker)
{
    // Cache the pair's/player's id (sadly, we have to do it this way, because if we're on a
    // network, the ids of the registrations can differ. Thus, we have to query for the name)
    queryPrepare(query, QStringLiteral("SELECT id from players WHERE name = :name"));
    query.bindValue(QStringLiteral(":name"), name);
    queryExec(query);
    query.next();
    const int id = query.value(0).toInt();

    // Remove a possibly existing draw
    queryPrepare(query, QStringLiteral("DELETE FROM draw "
                                       "WHERE round = :round AND pair_or_player = :id"));
    query.bindValue(QStringLiteral(":round"), round);
    query.bindValue(QStringLiteral(":id"),    id);
    queryExec(query);

    // If the given seat was empty, this was a delete request and we're done here
    if (seat.isEmpty()) {
        return;
    }

    // Save the given seat
    Q_ASSERT_X(seat.isValid(), "Database::setDraw", "Tried to save an invalid seat!");
    queryPrepare(query, QStringLiteral(
        "INSERT INTO draw(round, pair_or_player, table_number, pair_number, player_number) "
        "VALUES(:round, :id, :table_number, :pair_number, :player_number)"));
    query.bindValue(QStringLiteral(":round"),        round);
    query.bindValue(QStringLiteral(":id"),           id);
    query.bindValue(QStringLiteral(":table_number"), seat.table);
    query.bindValue(QStringLiteral(":pair_number"),  seat.pair);
    query.bindValue(QStringLiteral(":player_number"),seat.player);
    queryExec(query);

    // Update the pair's or player's marker if requested
    if (marker != -1) {
        setMarker(query, name, marker);
    }
}

bool Database::setListDraw(int round, const QVector<QString> &names,
                           const QVector<Draw::Seat> &seats)
{
    return writeHelper([this, round, &names, &seats](QSqlQuery &query) {
        // We have to get all IDs first. Operations on players have to be queried via their name,
        // because the ID can differ between machines if we're on a network.
        QHash<QString, int> idMap;
        queryPrepare(query, QStringLiteral("SELECT name, id FROM players;"));
        queryExec(query);
        while (query.next()) {
            idMap.insert(query.value(0).toString(), query.value(1).toInt());
        }

        // Remove all draws
        deleteAllDraws(query, round);

        // Create a QVariantList of all values we want to insert

        QVariantList rounds;
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
        for (int i = 0; i < names.count(); i++) {
            rounds.append(round);
        }
#else
        rounds.fill(round, names.count());
#endif

        QVariantList ids;
        QVariantList tables;
        QVariantList pairs;
        QVariantList players;
        for (int i = 0; i < names.count(); i++) {
            ids.append(idMap.value(names.at(i)));
            tables.append(seats.at(i).table);
            pairs.append(seats.at(i).pair);
            players.append(seats.at(i).player);
        }

        // Insert the data
        queryPrepare(query, QStringLiteral(
            "INSERT INTO draw("
                "round, "
                "pair_or_player, "
                "table_number, "
                "pair_number, "
                "player_number"
            ") "
            "VALUES("
                ":rounds, "
                ":ids, "
                ":tables, "
                ":pairs, "
                ":players"
            ")"));
        query.bindValue(QStringLiteral(":rounds"),  rounds);
        query.bindValue(QStringLiteral(":ids"),     ids);
        query.bindValue(QStringLiteral(":tables"),  tables);
        query.bindValue(QStringLiteral(":pairs"),   pairs);
        query.bindValue(QStringLiteral(":players"), players);

        queryExecBatch(query);
    });
}

bool Database::deleteAllDraws(int round)
{
    return writeHelper([this, round](QSqlQuery &query) {
        deleteAllDraws(query, round);
    });
}

void Database::deleteAllDraws(QSqlQuery &query, int round)
{
    queryPrepare(query, QStringLiteral("DELETE FROM draw WHERE round = :round"));
    query.bindValue(QStringLiteral(":round"), round);
    queryExec(query);
}

bool Database::setDisqualified(const QString &name, int round)
{
    return writeHelper([this, &name, round](QSqlQuery &query) {
        setDisqualified(query, name, round);
    });
}

void Database::setDisqualified(QSqlQuery &query, const QString &name, int round)
{
    queryPrepare(query, QStringLiteral("UPDATE players "
                                        "SET disqualified = :disqualified_value "
                                        "WHERE name = :name"));
    query.bindValue(QStringLiteral(":disqualified_value"), round != 0 ? round : s_sqliteNull);
    query.bindValue(QStringLiteral(":name"), name);
    queryExec(query);
}

bool Database::setNumber(const QString &name, int number)
{
    return writeHelper([this, &name, number](QSqlQuery &query) {
        setNumber(query, name, number);
    });
}

void Database::setNumber(QSqlQuery &query, const QString &name, int number)
{
    queryPrepare(query, QStringLiteral("UPDATE players SET number = :number WHERE name = :name"));
    query.bindValue(QStringLiteral(":number"), number > 0 ? number : s_sqliteNull);
    query.bindValue(QStringLiteral(":name"), name);
    queryExec(query);
}

bool Database::setBookingChecksum(const QString &name, const QString &checksum)
{
    return writeHelper([this, &name, &checksum](QSqlQuery &query) {
        setBookingChecksum(query, name, checksum);
    });
}

void Database::setBookingChecksum(QSqlQuery &query, const QString &name, const QString &checksum)
{
    // If we have an already-saved checksum: Revoke the booking
    revokeBooking(query, name, Booking::NewCodeExported, checksum);

    // Set the booking checksum
    queryPrepare(query, QStringLiteral("UPDATE players SET booking_checksum = :checksum "
                                       "WHERE name = :name"));
    query.bindValue(QStringLiteral(":checksum"), checksum);
    query.bindValue(QStringLiteral(":name"), name);
    queryExec(query);

    // If the checksum we have now was revoked before: Clean it from the revocation list
    // This can happen if a previously deleted registration is registered again exactly like before
    queryPrepare(query, QStringLiteral("DELETE FROM revoked_bookings WHERE checksum = :checksum"));
    query.bindValue(QStringLiteral(":checksum"), checksum);
    queryExec(query);
}

void Database::revokeBooking(QSqlQuery &query, const QString &name,
                             Booking::RevocationReason reason, const QString &newChecksum)
{
    // Query for a possibly already-saved booking checksum
    queryPrepare(query, QStringLiteral("SELECT booking_checksum FROM players WHERE name = :name"));
    query.bindValue(QStringLiteral(":name"), name);
    queryExec(query);
    query.next();
    const auto checksum = query.value(0).toString();

    if (checksum.isEmpty() || (! newChecksum.isEmpty() && checksum == newChecksum)) {
        // We either have no booking or the checksum didn't change (re-export without changes)
        return;
    }

    // If we have one, add it to the "revoked bookings" list
    queryPrepare(query, QStringLiteral(
        "INSERT OR REPLACE INTO revoked_bookings(checksum, revoked, reason) "
        "VALUES(:checksum, :revoked, :reason)"));
    query.bindValue(QStringLiteral(":checksum"), checksum);
    query.bindValue(QStringLiteral(":revoked"), timeToString(QDateTime::currentDateTime()));
    query.bindValue(QStringLiteral(":reason"), Booking::revocationReasonMap.value(reason));
    queryExec(query);
    qCDebug(DatabaseLog) << "Revoked booking checksum" << checksum << "for registration" << name;
}

// =======================
// Functions not using SQL
// =======================

const QString &Database::errorMessage() const
{
    return m_errorMessage;
}

bool Database::isOpen() const
{
    return m_db.isOpen();
}

bool Database::isWritable() const
{
    return m_isWritable;
}

bool Database::isChangeable() const
{
    return m_isWritable && m_tournamentSettings->tournamentOpen();
}

const QString &Database::dbFile() const
{
    return m_dbFileName;
}

const QString &Database::dbPath() const
{
    return m_dbFilePath;
}

int Database::dbVersion() const
{
    return s_dbVersion;
}

void Database::setNetworkTournamentStarted(bool state)
{
    m_networkTournamentStarted = state;
}

bool Database::networkTournamentStarted() const
{
    return m_networkTournamentStarted;
}

bool Database::tournamentStarted() const
{
    return m_tournamentStarted || m_networkTournamentStarted;
}

bool Database::localTournamentStarted() const
{
    return m_tournamentStarted;
}

int Database::necessaryDivisor() const
{
    return m_tournamentSettings->isPairMode() ? 2 : 4;
}

bool Database::playersDividable() const
{
    return playersDividable(m_pairsOrPlayersCount);
}

bool Database::playersDividable(int count) const
{
    return count > 0 && count % necessaryDivisor() == 0;
}

void Database::setPairsOrPlayersCount(int count)
{
    m_pairsOrPlayersCount = count;
    m_scoresPerRound = m_pairsOrPlayersCount * m_tournamentSettings->boogersPerRound();
    if (playersDividable()) {
        m_tablesCount = m_pairsOrPlayersCount / necessaryDivisor();
    }
}

int Database::pairsOrPlayersCount() const
{
    return m_pairsOrPlayersCount;
}

int Database::tablesCount() const
{
    return m_tablesCount;
}

bool Database::ranksDiffer(const RankData &currentRank, const RankData &lastRank)
{
    if (m_tournamentSettings->includeOpponentGoals()) {
        return currentRank.boogers != lastRank.boogers
               || currentRank.goals != lastRank.goals
               || currentRank.opponentGoals != lastRank.opponentGoals;
    } else {
        return currentRank.boogers != lastRank.boogers
               || currentRank.goals != lastRank.goals;
    }
}

const QMap<int, bool> &Database::roundsState() const
{
    return m_roundsState;
}

bool Database::scoresPresent(int round) const
{
    return m_scoresPresent.value(round, false);
}

bool Database::allRoundsFinished() const
{
    if (! tournamentStarted()) {
        return true;
    }

    const auto roundsStateValues = m_roundsState.values();
    for (const auto finished : roundsStateValues) {
        if (! finished) {
            return false;
        }
    }

    return true;
}

int Database::allRounds() const
{
    return m_allRounds;
}

Database::GoalsImportant Database::goalsImportance(const QVector<Database::RankData> &ranking) const
{
    QVector<bool> goalsImportance;
    QVector<bool> opponentGoalsImportance;

    for (int index = 0; index < ranking.count(); index++) {
        // Get the boogers and goals of the previous rank

        int prevRankBoogers = -1;
        int prevRankGoals = -1;
        int prevRankIndex = index - 1;

        while (prevRankIndex > 0 && ranking.at(prevRankIndex).rank == ranking.at(index).rank) {
            prevRankIndex--;
        }

        if (prevRankIndex >= 0 && ranking.at(prevRankIndex).rank != ranking.at(index).rank) {
            prevRankBoogers = ranking.at(prevRankIndex).boogers;
            prevRankGoals = ranking.at(prevRankIndex).goals;
        }

        // Get the boogers and goals of the next rank

        int nextRankBoogers = -1;
        int nextRankGoals = -1;
        int nextRankIndex = index + 1;

        while (nextRankIndex < ranking.count()
               && ranking.at(nextRankIndex).rank == ranking.at(index).rank) {

            nextRankIndex++;
        }

        if (nextRankIndex < ranking.count()
            && ranking.at(nextRankIndex).rank != ranking.at(index).rank) {

            nextRankBoogers = ranking.at(nextRankIndex).boogers;
            nextRankGoals = ranking.at(nextRankIndex).goals;
        }

        // Check what's important

        bool goalsImportant = true;
        bool opponentGoalsImportant = true;

        if (ranking.at(index).boogers != prevRankBoogers
            && ranking.at(index).boogers != nextRankBoogers) {

            goalsImportant = false;
            opponentGoalsImportant = false;

        } else if (ranking.at(index).goals != prevRankGoals
                   && ranking.at(index).goals != nextRankGoals) {

            opponentGoalsImportant = false;
        }

        goalsImportance.append(goalsImportant);
        opponentGoalsImportance.append(opponentGoalsImportant);
    }

    return { goalsImportance, opponentGoalsImportance };
}

bool Database::saveScheduleTimes(int round, const QString &start, const QString &end)
{
    return writeHelper([this, round, &start, &end](QSqlQuery &query) {
        queryPrepare(query, QStringLiteral("INSERT OR REPLACE INTO schedule(round, start, end) "
                                           "VALUES(:round, :start, :end)"));
        query.bindValue(QStringLiteral(":round"), round);
        query.bindValue(QStringLiteral(":start"), start);
        query.bindValue(QStringLiteral(":end"), end);
        queryExec(query);
    });
}

TSuccess<QVector<QString>> Database::getScheduleData()
{
    QVector<QString> result;
    bool success;

    readHelper([this, &result](QSqlQuery &query) {
        queryExec(query, QStringLiteral("SELECT round, start, end FROM schedule ORDER BY round"));
        while (query.next()) {
            result.append(query.value(0).toString());
            result.append(query.value(1).toString());
            result.append(query.value(2).toString());
        }
    }, success);

    return { result, success };
}

bool Database::resetScheduleTimes(int fromRound)
{
    return writeHelper([this, fromRound](QSqlQuery &query) {
        queryPrepare(query, QStringLiteral("DELETE FROM schedule WHERE round >= :start"));
        query.bindValue(QStringLiteral(":start"), fromRound);
        queryExec(query);
    });
}

TSuccess<QVector<int>> Database::getDrawnRounds()
{
    QVector<int> result;
    bool success;

    readHelper([this, &result](QSqlQuery &query) {
        queryExec(query, QStringLiteral("SELECT DISTINCT round FROM draw ORDER BY round"));
        while (query.next()) {
            result.append(query.value(0).toInt());
        }
    }, success);

    return { result, success };
}

TSuccess<bool> Database::round2PlusDrawsPresent()
{
    int result;
    bool success;

    readHelper([this, &result](QSqlQuery &query) {
        queryExec(query, QStringLiteral("SELECT COUNT(round) FROM draw WHERE NOT ROUND = 1"));
        query.next();
        result = query.value(0).toInt();
    }, success);

    return { result > 0, success };
}

TSuccess<bool> Database::bookingCodesExported()
{
    int result;
    bool success;

    readHelper([this, &result](QSqlQuery &query) {
        queryExec(query, QStringLiteral("SELECT COUNT(id) FROM players "
                                        "WHERE booking_checksum IS NOT NULL;"));
        query.next();
        result = query.value(0).toInt();
    }, success);

    return { result > 0, success };
}

TSuccess<Booking::RevocationState> Database::revocationState(const QString &checksum)
{
    Booking::RevocationState result;
    bool success;

    readHelper([this, &checksum, &result](QSqlQuery &query) {
        queryPrepare(query, QStringLiteral("SELECT revoked, reason FROM revoked_bookings "
                                           "WHERE checksum = :checksum"));
        query.bindValue(QStringLiteral(":checksum"), checksum);
        queryExec(query);
        if (query.next()) {
            result.date = timeFromString(query.value(0).toString());
            const auto reason = query.value(1).toString();
            result.reason = Booking::revocationReasonMap.key(reason,
                                                             Booking::UnknownRevocationReason);
            if (result.reason == Booking::UnknownRevocationReason) {
                qCWarning(DatabaseLog) << "Invalid revocation reason" << reason
                                       << "for booking checksum" << checksum;
            }
        }
    }, success);

    return { result, success };
}

void Database::updateFixtures()
{
    if (! m_tournamentSettings->autoSelectPairs()
        || m_tournamentSettings->selectByLastRound()) {

        // No need to update fixtures
        return;
    }

    bool success; // Not used here, as we invoke this as a slot most of the time

    readHelper([this](QSqlQuery &query) {
        const auto isSinglePlayerMode = m_tournamentSettings->isSinglePlayerMode();

        // Prepare the team mates and opponents hashes
        m_opponents.clear();
        m_teamMates.clear();
        queryExec(query, QStringLiteral("SELECT id FROM players ORDER BY id"));
        while (query.next()) {
            const auto id = query.value(0).toInt();
            m_opponents[id] = QVector<int>();
            if (isSinglePlayerMode) {
                m_teamMates[id] = QVector<int>();
            }
        }

        // Get all tables
        queryExec(query, QStringLiteral(
            "SELECT "
                "round || ' ' || table_number AS table_id, "
                "pair_number, "
                "player_number, "
                "pair_or_player "
            "FROM assignment "
            "UNION "
            "SELECT "
                "round || ' ' || table_number AS table_id, "
                "pair_number, "
                "player_number, "
                "pair_or_player "
            "FROM draw "
            "WHERE NOT EXISTS ("
                "SELECT TRUE FROM assignment "
                "WHERE assignment.round = draw.round "
                "AND assignment.pair_or_player = draw.pair_or_player "
            ") "
            "ORDER BY table_id, pair_number, player_number"
        ));

        QHash<QString, QVector<int>> allTables;
        while (query.next()) {
            const auto tableId = query.value(0).toString();
            if (! allTables.contains(tableId)) {
                allTables[tableId] = QVector<int> { 0, 0, 0, 0 };
            }
            const auto pair = query.value(1).toInt();
            const auto player = query.value(2).toInt();
            const auto id = query.value(3).toInt();
            allTables[tableId][Draw::indexForPlayer(pair, player)] = id;
        }

        // Add all pairs/players to the respective team mates and opponents lists

        QHash<QString, QVector<int>>::const_iterator it;

        for (it = allTables.constBegin(); it != allTables.constEnd(); it++) {
            const auto &players = it.value();
            const auto p0 = players.at(0);
            const auto p2 = players.at(2);

            // Add pair 1 or pair 1 player 1 and pair 2 or pair 2 player 1 opponents
            if (p0 > 0 && p2 > 0) {
                m_opponents[p0].append(p2);
                m_opponents[p2].append(p0);
            }

            if (isSinglePlayerMode) {
                const auto p1 = players.at(1);
                const auto p3 = players.at(3);

                // Also add pair 1 player 2 and pair 2 player 2 opponents
                if (p0 > 0 && p3 > 0) {
                    m_opponents[p0].append(p3);
                    m_opponents[p3].append(p0);
                }
                if (p2 > 0 && p1 > 0) {
                    m_opponents[p2].append(p1);
                    m_opponents[p1].append(p2);
                }
                if (p3 > 0 && p1 > 0) {
                    m_opponents[p3].append(p1);
                    m_opponents[p1].append(p3);
                }

                // Add team mates
                if (p0 > 0 && p1 > 0) {
                    m_teamMates[p0].append(p1);
                    m_teamMates[p1].append(p0);
                }
                if (p2 > 0 && p3 > 0) {
                    m_teamMates[p2].append(p3);
                    m_teamMates[p3].append(p2);
                }
            }
        }
    }, success);
}

const QHash<int, QVector<int>> &Database::opponents() const
{
    return m_opponents;
}

const QHash<int, QVector<int>> &Database::teamMates() const
{
    return m_teamMates;
}

QString Database::timeToString(const QDateTime &time) const
{
    return time.toUTC().toString(Qt::ISODate);
}

QDateTime Database::timeFromString(const QString &time) const
{
    return QDateTime::fromString(time, Qt::ISODate).toLocalTime();
}
