﻿// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef DATABASEUPDATER_H
#define DATABASEUPDATER_H

// Local includes
#include "SqLiteDatabase.h"

class DatabaseUpdater : public SqLiteDatabase
{
    Q_OBJECT

public:
    explicit DatabaseUpdater(QObject *parent = nullptr);
    void updateFromV3(QSqlQuery &query);
    void updateFromV4(QSqlQuery &query);
    void updateFromV5(QSqlQuery &query);
    void updateFromV6(QSqlQuery &query);
    void updateFromV7(QSqlQuery &query);
    void updateFromV8(QSqlQuery &query);
    void updateFromV9(QSqlQuery &query);
    void updateFromV10(QSqlQuery &query);
    void updateFromV11(QSqlQuery &query);
    void updateFromV12(QSqlQuery &query);
    void updateFromV13(QSqlQuery &query);

};

#endif // DATABASEUPDATER_H
