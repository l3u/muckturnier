// SPDX-FileCopyrightText: 2010-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef DATABASE_H
#define DATABASE_H

// Local includes

#include "SqLiteDatabase.h"

#include "shared/StructTemplate.h"
#include "shared/Markers.h"
#include "shared/Draw.h"
#include "shared/Scores.h"
#include "shared/Players.h"
#include "shared/Booking.h"

// Qt includes
#include <QMap>
#include <QHash>
#include <QDateTime>

// Local classes
class SharedObjects;
class StringTools;
class TournamentSettings;

// Qt classes
class QLockFile;
class QSqlQuery;

using namespace StructTemplate;

class Database : public SqLiteDatabase
{
    Q_OBJECT

public:
    enum Error {
        NoError,
        CouldNotOpen,
        DbVersion3,
        DbVersion4,
        DbVersion5,
        DbVersion6,
        DbVersion7,
        DbVersion8,
        DbVersion9,
        DbVersion10,
        DbVersion11,
        DbVersion12,
        DbVersion13
    };

    struct BoogerResult
    {
        int pairOrPlayer;
        int score;
    };

    struct RankData
    {
        int rank;
        QString name;
        int number;
        int boogers;
        int goals;
        int opponentGoals;
        int playedBoogers;
    };

    struct GoalsImportant
    {
        QVector<bool> goals;
        QVector<bool> opponentGoals;
    };

    explicit Database(QObject *parent, SharedObjects *sharedObjects);

    // Underlying database handling
    QString checkForLockFile(const QString &dbFile) const;
    bool create(const QString &dbFile);
    bool saveSettings();
    Database::Error openDatabase(const QString &dbFile);
    Database::Error checkDatabase(const QString &dbFile);
    bool updateDatabase(const QString &dbFileName, Error openError);
    void closeDatabaseConnection();
    bool reset();

    // Settings
    void setNetworkTournamentStarted(bool state);
    bool networkTournamentStarted() const;
    bool localTournamentStarted() const;
    void setPairsOrPlayersCount(int count);

    // Functions returning const values/references or boolean operations on them

    bool isWritable() const;
    bool isChangeable() const;
    bool tournamentStarted() const;
    int dbVersion() const;
    int necessaryDivisor() const;
    bool playersDividable() const;
    bool playersDividable(int count) const;
    int pairsOrPlayersCount() const;
    int tablesCount() const;
    int allRounds() const;
    bool isOpen() const;
    bool allRoundsFinished() const;
    GoalsImportant goalsImportance(const QVector<Database::RankData> &ranking) const;
    bool scoresPresent(int round) const;

    const QString &errorMessage() const;
    const QString &dbFile() const;
    const QString &dbPath() const;
    const QMap<int, bool> &roundsState() const;
    const QHash<int, QVector<int>> &opponents() const;
    const QHash<int, QVector<int>> &teamMates() const;

    QString timeToString(const QDateTime &time) const;
    QDateTime timeFromString(const QString &time) const;

    // Functions using SQL to write to the database

    bool addMarkers(const QVector<Markers::Marker> &markers);
    bool registerPlayers(const QString &name, int markerId);
    bool registerPlayers(const QStringList &names);
    bool renamePlayers(const QString &oldName, const QString &newName, int newMarker);
    bool renamePlayers(const QStringList &oldNames, const QStringList &newNames);
    bool setDraw(int round, const QString &name, const Draw::Seat &seat, int marker);
    bool setListDraw(int round, const QVector<QString> &names, const QVector<Draw::Seat> &seats);
    bool assemblePair(const QString &player1, const QString &player2, const QString &separator,
                      int assignedMarker);
    bool deletePlayers(const QString &name);
    bool deleteMarkedPlayers(int markerId);
    bool deleteAllPlayers();
    bool deleteAllDraws(int round);

    bool setMarker(const QString &name, int markerId);
    bool setListMarker(int markerId);
    bool removeListMarker(int markerId);

    bool addMarker(const Markers::Marker &marker);
    bool editMarker(int id, const QString &name, const QColor &color,
                    Markers::MarkerSorting sorting);
    bool deleteMarker(int id, int sequence);
    bool moveMarker(int id, int sequence, int direction);

    bool saveScore(int round, int table, const QVector<Database::BoogerResult> &score);
    bool deleteScore(int round, int table);

    bool swapPairs(int round, int table);
    bool swapPlayers(int round, int table, int pair);

    bool adoptAllData(const QVector<Players::Data> &playersData,
                      const QVector<Markers::Marker> &markers);
    bool adoptPlayersMetadata(const QVector<Players::Data> &playersData,
                              const QVector<Markers::Marker> &markers);

    bool setDisqualified(const QString &name, int round);

    bool setNumber(const QString &name, int number);

    bool setBookingChecksum(const QString &name, const QString &checksum);

    bool saveScheduleTimes(int round, const QString &start, const QString &end);
    bool resetScheduleTimes(int fromRound);

    // Functions using SQL to read from the database
    TSuccess<QStringList> players();
    TSuccess<QVector<Players::Data>> playersData();
    TSuccess<QVector<int>> missingTables(int round);
    TSuccess<QVector<Players::DisplayData>> missingPlayers(int round);
    TSuccess<QHash<int, Draw::Seat>> draw(int round);
    TSuccess<QHash<int, Draw::Seat>> assignment(int round);
    TSuccess<Scores::Score> scores(int round);
    TSuccess<QVector<BoogerResult>> singleScore(int round, int table);
    TSuccess<QVector<RankData>> ranking(int upToRound);
    TSuccess<QVector<Markers::Marker>> markers();
    TSuccess<QMap<int, QVector<QString>>> disqualifiedPlayers(int round);
    TSuccess<QVector<QString>> getScheduleData();
    TSuccess<QVector<int>> getDrawnRounds();
    TSuccess<bool> round2PlusDrawsPresent();
    TSuccess<bool> bookingCodesExported();
    TSuccess<Booking::RevocationState> revocationState(const QString &checksum);

Q_SIGNALS:
    void roundsStateChanged();

public Q_SLOTS:
    void updateFixtures();

private: // Functions
    // Connecting helpers
    bool connectDatabase(const QString &dbFile);
    bool lockDatabase();
    QString lockFileName(const QString &dbFile) const;
    void updateDbFileInfo();
    bool checkIfDbIsLocked();

    bool checkIfTournamentStarted();
    bool updateRoundsState();
    bool ranksDiffer(const RankData &currentRank, const RankData &lastRank);
    void updateAssignment(QSqlQuery &query,
                          int round, int pairOrPlayer, int pairNumber, int playerNumber, int table);
    void setDraw(QSqlQuery &query, int round, const QString &name, const Draw::Seat &seat,
                 int marker);
    void addMarker(QSqlQuery &query, const Markers::Marker &marker);
    void registerPlayers(QSqlQuery &query, const QStringList &names);
    void setMarker(QSqlQuery &query, const QString &name, int markerId);
    void setListMarker(QSqlQuery &query, int markerId);
    void renamePlayers(QSqlQuery &query, const QString &oldName, const QString &newName,
                       int newMarker);
    void deletePlayers(QSqlQuery &query, const QString &name);
    void deleteAllDraws(QSqlQuery &query, int round);
    void adoptPlayersMetadata(QSqlQuery &query,
                              const QVector<Players::Data> &playersData,
                              const QVector<Markers::Marker> &markers);
    void setDisqualified(QSqlQuery &query, const QString &name, int round);
    void setNumber(QSqlQuery &query, const QString &name, int number);
    void setBookingChecksum(QSqlQuery &query, const QString &name, const QString &checksum);

    void revokeBooking(QSqlQuery &query, const QString &name, Booking::RevocationReason reason,
                       const QString &newChecksum = QString());

private: // Variables
    TournamentSettings *m_tournamentSettings;
    StringTools *m_stringTools;

    QLockFile *m_lockFile = nullptr;
    QString m_errorMessage;
    QString m_dbFileName;
    QString m_dbFilePath;

    bool m_tournamentStarted = false;
    bool m_networkTournamentStarted = false;
    int m_pairsOrPlayersCount = 0;
    int m_scoresPerRound = 0;
    int m_tablesCount = 0;
    QMap<int, bool> m_roundsState;
    QHash<int, bool> m_scoresPresent;
    int m_allRounds = 0;
    QHash<int, QVector<int>> m_opponents;
    QHash<int, QVector<int>> m_teamMates;

};

#endif // DATABASE_H
