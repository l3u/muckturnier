// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "SqLiteDatabase.h"

// Qt includes
#include <QSqlError>

SqLiteDatabase::SqLiteDatabase(QObject *parent) : QObject(parent)
{
}

QString SqLiteDatabase::sqlErrorText() const
{
    const auto error = m_db.lastError();
    return tr("<p><b>Die Fehlermeldung war:</b><br/>%1<br/>%2</p>").arg(
              error.driverText(), error.databaseText());
}

QString SqLiteDatabase::sqlErrorText(const QSqlQuery &query) const
{
    const auto error = query.lastError();
    auto message = tr("<p><b>Die Fehlermeldung war:</b><br/>%1<br/>%2</p>").arg(
                      error.driverText(), error.databaseText());
    const auto executedQuery = query.executedQuery();
    if (! executedQuery.isEmpty()) {
        message.append(tr("<p><b>Die ausgeführte Datenbankabfrage war:</b><br/>%3</p>").arg(
                          executedQuery));
    }
    return message;
}

void SqLiteDatabase::emitSqlError(const QString &text)
{
    if (! m_emitSqlErrors) {
        return;
    }

    // This prevents saveSettings() to do a write attempt when triggered by the MainWindow
    // closing the database connection after displaying an error message
    m_isWritable = false;

    Q_EMIT sqlError(text);
}

bool SqLiteDatabase::startTransaction()
{
    const auto success = m_db.transaction();
    if (! success) {
        emitSqlError(tr("<p><b>Fehlgeschlagene Funktion:</b><br/>QSqlDatabase::transaction</p>")
                     + sqlErrorText());
        throw false;
    }
    return success;
}

bool SqLiteDatabase::commitTransaction()
{
    const auto success = m_db.commit();
    if (! success) {
        emitSqlError(tr("<p><b>Fehlgeschlagene Funktion:</b><br/>QSqlDatabase::commit</p>")
                     + sqlErrorText());
        throw false;
    }
    return success;
}

bool SqLiteDatabase::rollbackTransaction(QSqlQuery &query)
{
    query.clear();
    return m_db.rollback();
}

bool SqLiteDatabase::queryPrepare(QSqlQuery &query, const QString &text)
{
    const auto success = query.prepare(text);
    if (! success) {
        emitSqlError(tr("<p><b>Fehlgeschlagene Funktion:</b><br/>QSqlQuery::prepare</p>")
                     + sqlErrorText(query));
        throw false;
    }
    return success;
}

bool SqLiteDatabase::queryExec(QSqlQuery &query)
{
    const auto success = query.exec();
    if (! success) {
        emitSqlError(tr("<p><b>Fehlgeschlagene Funktion:</b><br/>QSqlQuery::exec</p>")
                     + sqlErrorText(query));
        throw false;
    }
    return success;
}

bool SqLiteDatabase::queryExec(QSqlQuery &query, const QString &text)
{
    const auto success = query.exec(text);
    if (! success) {
        emitSqlError(tr("<p><b>Fehlgeschlagene Funktion:</b><br/>QSqlQuery::exec</p>")
                     + sqlErrorText(query));
        throw false;
    }
    return success;
}

bool SqLiteDatabase::queryExecBatch(QSqlQuery &query)
{
    const auto success = query.execBatch();
    if (! success) {
        emitSqlError(tr("<p><b>Fehlgeschlagene Funktion:</b><br/>QSqlQuery::execBatch</p>")
                     + sqlErrorText(query));
        throw false;
    }
    return success;
}
