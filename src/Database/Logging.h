// SPDX-FileCopyrightText: 2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef LOGGING_DATABASE_H
#define LOGGING_DATABASE_H

#include <QLoggingCategory>

Q_DECLARE_LOGGING_CATEGORY(DatabaseLog)

#endif // LOGGING_DATABASE_H
