// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "Application.h"

#include "shared/Logging.h"

// Qt includes

#include <QApplication>
#include <QDebug>
#include <QPalette>

#ifdef Q_OS_MACOS
#include <QFileOpenEvent>
#endif

Application::Application(int &argc, char **argv) : QApplication(argc, argv)
{
    updateColorMode();
}

bool Application::event(QEvent *event)
{
    if (event->type() == QEvent::ApplicationPaletteChange) {
        qCDebug(MuckturnierLog) << "The color scheme has been changed";
        setStyleSheet(styleSheet());
        updateColorMode();
        Q_EMIT paletteChanged();
    }
#ifdef Q_OS_MACOS
    else if (event->type() == QEvent::FileOpen) {
        auto *openEvent = static_cast<QFileOpenEvent *>(event);
        Q_EMIT fileOpenRequested(openEvent->file());
    }
#endif

    return QApplication::event(event);
}

void Application::updateColorMode()
{
    const QPalette palette;
    const bool isDarkMode = palette.base().color().lightness()
                            < palette.windowText().color().lightness();
    setProperty("DARK_MODE", isDarkMode);

    qCDebug(MuckturnierLog) << (isDarkMode ? "Dark" : "Light") << "color scheme detected";
}
