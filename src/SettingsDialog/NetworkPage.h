// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef NETWORKPAGE_H
#define NETWORKPAGE_H

// Local includes
#include "shared/AbstractSettingsPage.h"

// Local classes
class Settings;

// Qt classes
class QSpinBox;
class QCheckBox;
class QLineEdit;

class NetworkPage : public AbstractSettingsPage
{
    Q_OBJECT

public:
    explicit NetworkPage(Settings *settings, QWidget *parent = nullptr);
    void saveSettings() override;

private Q_SLOTS:
    void restoreDefaultValues();

private: // Variables
    Settings *m_settings;
    QCheckBox *m_preferIpv6;
    QSpinBox *m_serverPort;
    QSpinBox *m_connectTimeout;
    QSpinBox *m_requestTimeout;
    QSpinBox *m_discoverPort;
    QSpinBox *m_discoverTimeout;
    QSpinBox *m_discoverAttempts;
    QSpinBox *m_wlanCodeScannerServerPort;
    QLineEdit *m_wlanCodeScannerServerKey;

};

#endif // NETWORKPAGE_H
