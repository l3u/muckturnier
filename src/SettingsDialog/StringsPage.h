// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef STRINGSPAGE_H
#define STRINGSPAGE_H

// Local includes
#include "shared/AbstractSettingsPage.h"

// Local classes
class Settings;
class SpacesLineEdit;

// Qt classes
class QLabel;
class QComboBox;

class StringsPage : public AbstractSettingsPage
{
    Q_OBJECT

public:
    explicit StringsPage(Settings *settings, bool networkInUse, QWidget *parent = nullptr);
    void saveSettings() override;

private Q_SLOTS:
    void updateSeparatorExample();
    void resetStrings();

private: // Variables
    Settings *m_settings;
    const bool m_networkInUse;

    QComboBox *m_boogerSymbol;
    SpacesLineEdit *m_namesSeparator;
    QLabel *m_separatorExample;

};

#endif // STRINGSPAGE_H
