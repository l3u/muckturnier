// SPDX-FileCopyrightText: 2019-2020 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SPACESLINEEDIT_H
#define SPACESLINEEDIT_H

// Qt includes
#include <QPlainTextEdit>

class SpacesLineEdit : public QPlainTextEdit
{
    Q_OBJECT

public:
    explicit SpacesLineEdit(QWidget *parent = nullptr);
    void setText(const QString &text);
    QString text() const;

protected:
    void keyPressEvent(QKeyEvent *event) override;
    QSize sizeHint() const override;
    QSize minimumSizeHint() const override;
    void insertFromMimeData(const QMimeData *source) override;

private: // Variables
    QSize m_sizeHint;

};

#endif // SPACESLINEEDIT_H
