// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef MARKERSTEMPLATEPAGE_H
#define MARKERSTEMPLATEPAGE_H

// Local includes
#include "shared/AbstractSettingsPage.h"
#include "shared/Markers.h"

// Local classes
class SharedObjects;
class Database;
class Settings;
class TournamentSettings;
class MarkersDisplay;

// Qt classes
class QGroupBox;
class QLabel;

class MarkersTemplatePage : public AbstractSettingsPage
{
    Q_OBJECT

public:
    explicit MarkersTemplatePage(SharedObjects *sharedObjects, QWidget *parent = nullptr);
    void saveSettings() override;

private Q_SLOTS:
    void restoreTemplate();
    void adoptCurrentSettings();

private: // Variables
    Database *m_db;
    Settings *m_settings;
    TournamentSettings *m_tournamentSettings;

    QVector<Markers::Marker> m_markersTemplate;
    QHash<Markers::Type, int> m_specialMarkers;
    Markers::SinglesManagement m_singlesManagement;

    MarkersDisplay *m_markersDisplay;

};

#endif // MARKERSTEMPLATEPAGE_H
