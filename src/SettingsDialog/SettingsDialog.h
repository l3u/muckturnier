// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

// Local includes
#include "shared/TitleDialog.h"

// Local classes
class SharedObjects;
class StackedWidgetList;

class SettingsDialog : public TitleDialog
{
    Q_OBJECT

public:
    explicit SettingsDialog(QWidget *parent, SharedObjects *sharedObjects, bool networkInUse);

protected Q_SLOTS:
    void accept() override;

private: // Variables
    StackedWidgetList *m_list;

};

#endif // SETTINGSDIALOG_H
