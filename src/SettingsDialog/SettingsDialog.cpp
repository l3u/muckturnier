// SPDX-FileCopyrightText: 2019-2022 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "SettingsDialog.h"
#include "FileNameTemplatePage.h"
#include "TournamentTemplatePage.h"
#include "MarkersTemplatePage.h"
#include "StringsPage.h"
#include "SaveOnClosePage.h"
#include "NetworkPage.h"

#include "SharedObjects/SharedObjects.h"

#include "shared/StackedWidgetList.h"
#include "shared/AbstractSettingsPage.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QListWidget>
#include <QStackedWidget>

SettingsDialog::SettingsDialog(QWidget *parent, SharedObjects *sharedObjects, bool networkInUse)
    : TitleDialog(parent)
{
    setTitle(tr("Einstellungen"));
    addButtonBox(QDialogButtonBox::Save | QDialogButtonBox::Close);
    buttonBox()->button(QDialogButtonBox::Save)->setText(tr("Speichern und schließen"));

    m_list = new StackedWidgetList;
    layout()->addWidget(m_list);

    const QVector<AbstractSettingsPage *> pages {
        new FileNameTemplatePage(sharedObjects->settings()),
        new TournamentTemplatePage(sharedObjects),
        new MarkersTemplatePage(sharedObjects),
        new StringsPage(sharedObjects->settings(), networkInUse),
        new SaveOnClosePage(sharedObjects->settings()),
        new NetworkPage(sharedObjects->settings())
    };

    for (AbstractSettingsPage *page : pages) {
        m_list->addWidget(page);
    }

    m_list->setCurrentIndex(0);
}

void SettingsDialog::accept()
{
    for (int i = 0; i < m_list->count(); i++) {
        qobject_cast<AbstractSettingsPage *>(m_list->widget(i))->saveSettings();
    }
    TitleDialog::accept();
}
