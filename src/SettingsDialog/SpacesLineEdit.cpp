// SPDX-FileCopyrightText: 2019-2020 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "SpacesLineEdit.h"

// Qt includes
#include <QDebug>
#include <QLineEdit>
#include <QMimeData>

SpacesLineEdit::SpacesLineEdit(QWidget *parent) : QPlainTextEdit(parent)
{
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setLineWrapMode(QPlainTextEdit::NoWrap);
    setTabChangesFocus(true);

    QTextOption option = document()->defaultTextOption();
    option.setFlags(option.flags() | QTextOption::ShowTabsAndSpaces);
    document()->setDefaultTextOption(option);

    // Stealing the sizeHint from a plain QLineEdit will do for now :-P
    QLineEdit lineEdit;
    m_sizeHint = lineEdit.sizeHint();
}

QSize SpacesLineEdit::minimumSizeHint() const
{
    return m_sizeHint;
}

QSize SpacesLineEdit::sizeHint() const
{
    return m_sizeHint;
}

void SpacesLineEdit::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Return || event->key() == Qt::Key_Enter) {
        event->ignore();
        return;
    }
    QPlainTextEdit::keyPressEvent(event);
}

void SpacesLineEdit::insertFromMimeData(const QMimeData *source)
{
    QString text = source->text();
    // Remove Windows newlines
    text.replace(QLatin1String("\r\n"), QLatin1String(" "));
    // Remove Linux newlines
    text.replace(QLatin1Char('\n'), QLatin1String(" "));
    // Remove Mac newlines (who knows?! ;-)
    text.replace(QLatin1Char('\r'), QLatin1String(" "));

    QMimeData processedSource;
    processedSource.setText(text);
    QPlainTextEdit::insertFromMimeData(&processedSource);
}

void SpacesLineEdit::setText(const QString &text)
{
    setPlainText(text);
}

QString SpacesLineEdit::text() const
{
    return toPlainText();
}
