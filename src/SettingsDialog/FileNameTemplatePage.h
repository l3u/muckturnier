// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef FILENAMETEMPLATEPAGE_H
#define FILENAMETEMPLATEPAGE_H

// Local includes
#include "shared/AbstractSettingsPage.h"

// Local classes
class Settings;

// Qt classes
class QLineEdit;
class QLabel;

class FileNameTemplatePage : public AbstractSettingsPage
{
    Q_OBJECT

public:
    explicit FileNameTemplatePage(Settings *settings, QWidget *parent = nullptr);
    void saveSettings() override;

private Q_SLOTS:
    void updateParsedFileName(const QString &text);
    void restoreTemplate();

private: // Variables
    Settings *m_settings;
    QLineEdit *m_template;
    QLabel *m_parsedFileName;

};

#endif // FILENAMETEMPLATEPAGE_H
