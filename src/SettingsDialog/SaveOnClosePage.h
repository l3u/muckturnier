// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SAVEONCLOSEPAGE_H
#define SAVEONCLOSEPAGE_H

// Local includes

#include "shared/AbstractSettingsPage.h"

#include "SharedObjects/Settings.h"

// Qt includes
#include <QHash>

// Local classes
class Settings;

// Qt classes
class QCheckBox;

class SaveOnClosePage : public AbstractSettingsPage
{
    Q_OBJECT

public:
    explicit SaveOnClosePage(Settings *settings, QWidget *parent = nullptr);
    void saveSettings() override;

private Q_SLOTS:
    void restoreDefaultValues();

private: // Variables
    Settings *m_settings;
    QHash<Settings::SaveOnCloseOption, QCheckBox *> m_options;

};

#endif // SAVEONCLOSEPAGE_H
