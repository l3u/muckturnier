//* SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "SaveOnClosePage.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QCheckBox>
#include <QPushButton>
#include <QMessageBox>

SaveOnClosePage::SaveOnClosePage(Settings *settings, QWidget *parent)
    : AbstractSettingsPage(tr("Beim Beenden speichern"), parent),
      m_settings(settings)
{
    QLabel *saveOnCloseLabel = new QLabel(tr("Beim Beenden des können folgende Einstellungen "
                                             "gespeichert und beim nächsten Start "
                                             "wiederhergestellt werden:"));
    saveOnCloseLabel->setWordWrap(true);
    layout()->addWidget(saveOnCloseLabel);

    for (int i = 0; i < Settings::SaveOnCloseCount; i++) {
        const auto optionId = static_cast<Settings::SaveOnCloseOption>(i);

        QString text;
        switch (optionId) {
        case Settings::SaveWindowGeometry:
            text = tr("Position und Größe des Programmfensters");
            break;
        case Settings::SaveWindowState:
            text = tr("Darstellung und Position der Seiten");
            break;
        case Settings::SaveLastDbPath:
            text = tr("Verzeichnis der letzten benutzen Turnierdatenbank\n"
                      "(als Startpunkt des „Turnier öffnen“-Dateidialogs)");
            break;
        case Settings::SaveStopWatchState:
            text = tr("Countdown-Zeit, Titel, Größe und Position der Stoppuhr");
            break;
        case Settings::SavePhoneticSearchState:
            text = tr("Auswahl zur phonetischen Suche");
            break;
        case Settings::SaveRegistrationOptionsState:
            text = tr("Auswahl der Eingabeoptionen auf der Anmeldeseite");
            break;
        case Settings::SaveBookingCodesDir:
            text = tr("Verzeichnis zum Speichern von Anmeldecodes");
            break;
        case Settings::SaveBookingCodesScaleFactor:
            text = tr("Skalierungsfaktor für Anmeldecodes");
            break;
        case Settings::SaveNotesSettings:
            text = tr("Einstellungen des „Zettel drucken“-Dialogs außer\n"
                      "den Abständen (die werden manuell gespeichert)");
            break;
        case Settings::SaveOnCloseCount:
            Q_ASSERT_X(false, "SaveOnClosePage::SaveOnClosePage",
                              "Requested text for the \"Count\" value");
            break;
        }

        auto *checkBox = new QCheckBox(text);
        m_options[optionId] = checkBox;
        layout()->addWidget(checkBox);
        checkBox->setChecked(m_settings->saveOnCloseEnabled(optionId));
    }

    layout()->addStretch();

    QHBoxLayout *resetLayout = new QHBoxLayout;
    layout()->addLayout(resetLayout);
    resetLayout->addStretch();
    QPushButton *resetButton = new QPushButton(tr("Zurücksetzen"));
    connect(resetButton, &QPushButton::clicked, this, &SaveOnClosePage::restoreDefaultValues);
    resetLayout->addWidget(resetButton);
}

void SaveOnClosePage::saveSettings()
{
    for (int i = 0; i < Settings::SaveOnCloseCount; i++) {
        const auto optionId = static_cast<Settings::SaveOnCloseOption>(i);
        m_settings->saveSaveOnCloseState(optionId, m_options[optionId]->isChecked());
    }
}

void SaveOnClosePage::restoreDefaultValues()
{
    for (int i = 0; i < Settings::SaveOnCloseCount; i++) {
        m_options[static_cast<Settings::SaveOnCloseOption>(i)]->setChecked(true);
    }
}
