// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "MarkersTemplatePage.h"

#include "Database/Database.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/Settings.h"
#include "SharedObjects/TournamentSettings.h"

#include "shared/MarkersDisplay.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QPushButton>
#include <QMessageBox>

MarkersTemplatePage::MarkersTemplatePage(SharedObjects *sharedObjects, QWidget *parent)
    : AbstractSettingsPage(tr("Vorlage Markierungen"), parent),
      m_db(sharedObjects->database()),
      m_settings(sharedObjects->settings()),
      m_tournamentSettings(sharedObjects->tournamentSettings())
{
    m_markersTemplate = m_settings->markersTemplate(m_tournamentSettings->tournamentMode());
    m_specialMarkers = m_settings->specialMarkers();
    m_singlesManagement = m_settings->singlesManagementTemplate();

    m_markersDisplay = new MarkersDisplay(m_tournamentSettings->tournamentMode(),
                                          &m_markersTemplate, &m_specialMarkers,
                                          &m_singlesManagement);
    layout()->addWidget(m_markersDisplay);

    layout()->addStretch();

    auto *controlLayout = new QHBoxLayout;
    layout()->addLayout(controlLayout);
    controlLayout->addStretch();

    auto *adoptCurrentSettingsButton = new QPushButton(tr("Aktuelle Einstellungen übernehmen"));
    connect(adoptCurrentSettingsButton, &QPushButton::clicked,
            this, &MarkersTemplatePage::adoptCurrentSettings);
    controlLayout->addWidget(adoptCurrentSettingsButton);
    adoptCurrentSettingsButton->setEnabled(m_db->isOpen());

    auto *restoreTemplateButton = new QPushButton(tr("Zurücksetzen"));
    connect(restoreTemplateButton, &QPushButton::clicked,
            this, &MarkersTemplatePage::restoreTemplate);
    controlLayout->addWidget(restoreTemplateButton);

    m_markersDisplay->refresh();
}

void MarkersTemplatePage::restoreTemplate()
{
    m_specialMarkers = Markers::defaultSpecialMarkerValues;
    m_markersTemplate = m_settings->defaultMarkersTemplate(m_tournamentSettings->tournamentMode());
    m_singlesManagement = m_settings->defaultSinglesManagementTemplate();

    m_markersDisplay->refresh();
}

void MarkersTemplatePage::adoptCurrentSettings()
{
    const auto [ markers, success ] = m_db->markers();
    if (! success) {
        return;
    }
    m_markersTemplate = markers;

    m_singlesManagement = m_tournamentSettings->singlesManagement();
    m_specialMarkers = m_tournamentSettings->specialMarkers();
    m_markersDisplay->refresh();
}

void MarkersTemplatePage::saveSettings()
{
    m_settings->saveMarkers(m_specialMarkers);
    m_settings->saveMarkersTemplate(m_tournamentSettings->tournamentMode(), m_markersTemplate);
    m_settings->saveSinglesManagementTemplate(m_singlesManagement);
}
