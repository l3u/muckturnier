// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "TournamentTemplatePage.h"

#include "Database/Database.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/Settings.h"

#include "shared/TableDelegate.h"
#include "shared/Tournament.h"
#include "shared/OptionalPages.h"
#include "shared/TournamentTemplateHelper.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QLabel>
#include <QTableWidget>
#include <QPushButton>
#include <QMessageBox>
#include <QHeaderView>
#include <QScrollBar>
#include <QTimer>
#include <QApplication>

TournamentTemplatePage::TournamentTemplatePage(SharedObjects *sharedObjects, QWidget *parent)
    : AbstractSettingsPage(tr("Vorlage Turniereinstellungen"), parent),
      m_db(sharedObjects->database()),
      m_settings(sharedObjects->settings()),
      m_tournamentSettings(sharedObjects->tournamentSettings())
{
    QLabel *tournamentTemplateLabel = new QLabel(tr("Folgende Turniereinstellungen werden für "
                                                    "neue Turnierdatenbanken voreingestellt:"));
    tournamentTemplateLabel->setWordWrap(true);
    layout()->addWidget(tournamentTemplateLabel);

    m_templateDisplay = new QTableWidget;
    m_templateDisplay->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_templateDisplay->horizontalHeader()->hide();
    m_templateDisplay->verticalHeader()->hide();
    m_templateDisplay->setShowGrid(false);
    m_templateDisplay->setSelectionMode(QTableWidget::NoSelection);
    m_templateDisplay->setFocusPolicy(Qt::NoFocus);
    m_templateDisplay->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_templateDisplay->setItemDelegate(new TableDelegate(m_templateDisplay));
    m_templateDisplay->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    m_templateDisplay->setColumnCount(2);
    m_templateDisplay->setRowCount(Row::Count);
    layout()->addWidget(m_templateDisplay);

    m_templateDisplay->setItem(Row::TournamentMode, 0,
                               new QTableWidgetItem(tr("Turniermodus:")));
    m_templateDisplay->setItem(Row::TournamentMode, 1, new QTableWidgetItem);

    m_templateDisplay->setItem(Row::BoogerScore, 0,
                               new QTableWidgetItem(tr("Punkte pro Bobbl:")));
    m_templateDisplay->setItem(Row::BoogerScore, 1, new QTableWidgetItem);

    m_templateDisplay->setItem(Row::BoogersPerRound, 0,
                               new QTableWidgetItem(tr("Bobbl pro Runde:")));
    m_templateDisplay->setItem(Row::BoogersPerRound, 1, new QTableWidgetItem);

    m_templateDisplay->setItem(Row::RegistrationColumns, 0,
                               new QTableWidgetItem(tr("Spalten der Anmeldeliste:")));
    m_templateDisplay->setItem(Row::RegistrationColumns, 1, new QTableWidgetItem);

    m_templateDisplay->setItem(Row::DrawSettings, 0,
                               new QTableWidgetItem(tr("Einstellungen für die\n"
                                                       "Auslosung der 1. Runde:")));
    m_templateDisplay->setItem(Row::DrawSettings, 1, new QTableWidgetItem);

    m_templateDisplay->setItem(Row::ScheduleSettings, 0,
                               new QTableWidgetItem(tr("Einstellungen für\n"
                                                       "den Turnier-Zeitplan:")));
    m_templateDisplay->setItem(Row::ScheduleSettings, 1, new QTableWidgetItem);

    m_templateDisplay->setItem(Row::RegistrationListHeaderHidden, 0,
                               new QTableWidgetItem(tr("Kopfzeile der Anmelde-\n"
                                                       "liste ausblenden:")));
    m_templateDisplay->setItem(Row::RegistrationListHeaderHidden, 1, new QTableWidgetItem);

    m_templateDisplay->setItem(Row::ScoreType, 0,
                               new QTableWidgetItem(tr("Spielstandzettel:")));
    m_templateDisplay->setItem(Row::ScoreType, 1, new QTableWidgetItem);

    m_templateDisplay->setItem(Row::AutoSelectMode, 0,
                               new QTableWidgetItem(tr("Modus für die Auslosung\n"
                                                       "bzw. automatische Auswahl:")));
    m_templateDisplay->setItem(Row::AutoSelectMode, 1, new QTableWidgetItem);

    m_templateDisplay->setItem(Row::IncludeOpponentGoals, 0,
                               new QTableWidgetItem(tr("Gegnerische Tore\nberücksichtigen:")));
    m_templateDisplay->setItem(Row::IncludeOpponentGoals, 1, new QTableWidgetItem);

    m_templateDisplay->setItem(Row::GrayOutUnimportantGoals, 0,
                               new QTableWidgetItem(tr("Nicht entscheidende\nTore ausgrauen:")));
    m_templateDisplay->setItem(Row::GrayOutUnimportantGoals, 1, new QTableWidgetItem);

    m_templateDisplay->setItem(Row::OptionalPages, 0,
                               new QTableWidgetItem(tr("Optionale Seiten:")));
    m_templateDisplay->setItem(Row::OptionalPages, 1, new QTableWidgetItem);

    QHBoxLayout *controlLayout = new QHBoxLayout;
    layout()->addLayout(controlLayout);
    controlLayout->addStretch();

    QPushButton *adoptCurrentSettingsButton = new QPushButton(tr("Aktuelle Einstellungen "
                                                                 "übernehmen"));
    connect(adoptCurrentSettingsButton, &QPushButton::clicked,
            this, &TournamentTemplatePage::adoptCurrentSettings);
    controlLayout->addWidget(adoptCurrentSettingsButton);
    adoptCurrentSettingsButton->setEnabled(m_db->isOpen());

    QPushButton *restoreTemplateButton = new QPushButton(tr("Zurücksetzen"));
    connect(restoreTemplateButton, &QPushButton::clicked,
            this, &TournamentTemplatePage::restoreTemplate);
    controlLayout->addWidget(restoreTemplateButton);

    m_template = m_settings->tournamentTemplate();

    QTimer::singleShot(0, this, &TournamentTemplatePage::updateTemplateDisplay);
}

QTableWidgetItem *TournamentTemplatePage::itemFor(Row row) const
{
    return m_templateDisplay->item(row, 1);
}

QString TournamentTemplatePage::boolText(bool state) const
{
    return state ? tr("Ja") : tr("Nein");
}

void TournamentTemplatePage::updateTemplateDisplay()
{
    itemFor(Row::TournamentMode)->setText(m_template.tournamentMode == Tournament::FixedPairs
                                         ? tr("Feste Paare") : tr("Einzelne Spieler"));
    itemFor(Row::ScoreType)->setText(m_template.scoreType == Tournament::HorizontalScore
                                         ? tr("horizontal") : tr("vertikal"));
    itemFor(Row::BoogerScore)->setText(QString::number(m_template.boogerScore));
    itemFor(Row::BoogersPerRound)->setText(QString::number(m_template.boogersPerRound));

    QString drawSettings;
    if (m_template.drawSettings.consecutive) {
        drawSettings = tr("Forlaufende Vergabe");
    } else {
        if (m_template.drawSettings.randomDraw != -1) {
            drawSettings = tr("zufällig bis Tisch %1").arg(
                              m_template.drawSettings.randomDraw);
        }
        if (! drawSettings.isEmpty()) {
            drawSettings += tr("\ndann ");
        }
        drawSettings += tr("innerhalb (von) %n Tisches/n", "",
                           m_template.drawSettings.windowDraw + 1);
        if (m_template.drawSettings.consecutiveDraw != -1) {
            drawSettings += tr("\nab Tisch %1 fortlaufend").arg(
                               m_template.drawSettings.consecutiveDraw);
        }
    }
    itemFor(Row::DrawSettings)->setText(drawSettings);

    itemFor(Row::ScheduleSettings)->setText(
        tr("Geplante Runden: %1\n"
           "Zeit pro Runde: %2 min\n"
           "Pause zwischen den Runden: %3 min").arg(
           QString::number(m_template.scheduleSettings.plannedRounds),
           QString::number(m_template.scheduleSettings.roundDuration),
           QString::number(m_template.scheduleSettings.breakDuration)));

    QString registrationColumns(tr("Name"));
    for (const RegistrationColumns::Column column : RegistrationColumns::allColumns) {
        if (m_template.registrationColumns.contains(column)) {
            registrationColumns.append(tr(",\n%1").arg(
                                          RegistrationColumns::checkBoxText.value(column)));
        }
    }
    itemFor(Row::RegistrationColumns)->setText(registrationColumns);

    itemFor(Row::RegistrationListHeaderHidden)->setText(
        boolText(m_template.registrationListHeaderHidden));

    QString autoSelectMode;
    if (! m_template.autoSelectPairs) {
        autoSelectMode = tr("Keine Auslosung oder\n"
                            "automatische Auswahl");
    } else {
        if (m_template.selectByLastRound) {
            autoSelectMode = tr("Paar 1 bleibt sitzen\n"
                                "Paar 2 rückt weiter");
        } else {
            autoSelectMode = tr("Jede Runde wird\n"
                                "neu ausgelost");
        }
    }
    itemFor(Row::AutoSelectMode)->setText(autoSelectMode);

    itemFor(Row::IncludeOpponentGoals)->setText(boolText(m_template.includeOpponentGoals));
    itemFor(Row::GrayOutUnimportantGoals)->setText(boolText(m_template.grayOutUnimportantGoals));

    QString optionalPages;
    if (m_template.optionalPages.isEmpty()) {
        optionalPages = tr("keine");
    } else {
        const auto &pagesList = m_template.optionalPages;
        for (OptionalPages::Page page : pagesList) {
            if (! optionalPages.isEmpty()) {
                optionalPages.append(QStringLiteral("\n"));
            }
            optionalPages.append(OptionalPages::pageTitle.value(page));
        }
    }
    itemFor(Row::OptionalPages)->setText(optionalPages);

    m_templateDisplay->resizeColumnsToContents();
    m_templateDisplay->resizeRowsToContents();
    m_templateDisplay->setMinimumWidth(
        m_templateDisplay->columnWidth(0) + m_templateDisplay->columnWidth(1)
        + qApp->style()->pixelMetric(QStyle::PM_ScrollBarExtent)
        + qApp->style()->pixelMetric(QStyle::PM_DefaultFrameWidth)
    );
}

void TournamentTemplatePage::restoreTemplate()
{
    m_template = TournamentTemplate::defaultSettings;
    updateTemplateDisplay();
}

void TournamentTemplatePage::adoptCurrentSettings()
{
    m_template = TournamentTemplateHelper::assembleTemplate(m_tournamentSettings);
    updateTemplateDisplay();
}

void TournamentTemplatePage::saveSettings()
{
    m_settings->saveTournamentTemplate(m_template);
}
