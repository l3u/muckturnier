// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "NetworkPage.h"

#include "SharedObjects/Settings.h"

#include "shared/DefaultValues.h"

// Qt includes
#include <QGroupBox>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QLabel>
#include <QSpinBox>
#include <QHBoxLayout>
#include <QPushButton>
#include <QCheckBox>
#include <QScrollArea>
#include <QLineEdit>
#include <QRegularExpressionValidator>

NetworkPage::NetworkPage(Settings *settings, QWidget *parent)
    : AbstractSettingsPage(tr("Netzwerk"), parent),
      m_settings(settings)
{
    int row = -1;

    auto *wrapper = new QWidget;
    wrapper->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Preferred);
    auto *wrapperLayout = new QVBoxLayout(wrapper);

    QGroupBox *ipBox = new QGroupBox(tr("IP-Adressen"));
    auto *ipBoxLayout = new QVBoxLayout(ipBox);
    wrapperLayout->addWidget(ipBox);
    m_preferIpv6 = new QCheckBox(tr("IPv6-Adressen bevorzugen"));
    m_preferIpv6->setChecked(m_settings->preferIpv6());
    ipBoxLayout->addWidget(m_preferIpv6);

    QGroupBox *serverBox = new QGroupBox(tr("Muckturnier-Server und -Client"));
    QGridLayout *serverBoxLayout = new QGridLayout(serverBox);
    wrapperLayout->addWidget(serverBox);

    serverBoxLayout->addWidget(new QLabel(tr("Standardport (TCP):")), ++row, 0);
    m_serverPort = new QSpinBox;
    m_serverPort->setMinimum(1024);
    m_serverPort->setMaximum(65535);
    m_serverPort->setValue(m_settings->serverPort());
    serverBoxLayout->addWidget(m_serverPort, row, 1);

    serverBoxLayout->addWidget(new QLabel(tr("Verbindungs-Timeout (ms):")), ++row, 0);
    m_connectTimeout = new QSpinBox;
    m_connectTimeout->setMinimum(100);
    m_connectTimeout->setMaximum(60000);
    m_connectTimeout->setSingleStep(100);
    m_connectTimeout->setValue(m_settings->connectTimeout());
    serverBoxLayout->addWidget(m_connectTimeout, row, 1);

    serverBoxLayout->addWidget(new QLabel(tr("Anfrage-Timeout (ms):")), ++row, 0);
    m_requestTimeout = new QSpinBox;
    m_requestTimeout->setMinimum(100);
    m_requestTimeout->setMaximum(60000);
    m_requestTimeout->setSingleStep(100);
    m_requestTimeout->setValue(m_settings->requestTimeout());
    serverBoxLayout->addWidget(m_requestTimeout, row, 1);

    row = -1;

    QGroupBox *discoverBox = new QGroupBox(tr("Discover-Server und -Client"));
    QGridLayout *discoverBoxLayout = new QGridLayout(discoverBox);
    wrapperLayout->addWidget(discoverBox);

    discoverBoxLayout->addWidget(new QLabel(tr("Port (UDP):")), ++row, 0);
    m_discoverPort = new QSpinBox;
    m_discoverPort->setMinimum(1024);
    m_discoverPort->setMaximum(65535);
    m_discoverPort->setValue(m_settings->discoverPort());
    discoverBoxLayout->addWidget(m_discoverPort, row, 1);

    discoverBoxLayout->addWidget(new QLabel(tr("Discover-Timeout (ms):")), ++row, 0);
    m_discoverTimeout = new QSpinBox;;
    m_discoverTimeout->setMinimum(100);
    m_discoverTimeout->setMaximum(60000);
    m_discoverTimeout->setSingleStep(100);
    m_discoverTimeout->setValue(m_settings->discoverTimeout());
    discoverBoxLayout->addWidget(m_discoverTimeout, row, 1);

    discoverBoxLayout->addWidget(new QLabel(tr("Anzahl Versuche:")), ++row, 0);
    m_discoverAttempts = new QSpinBox;;
    m_discoverAttempts->setMinimum(1);
    m_discoverAttempts->setMaximum(999);
    m_discoverAttempts->setValue(m_settings->discoverAttempts());
    discoverBoxLayout->addWidget(m_discoverAttempts, row, 1);

    row = -1;

    QGroupBox *scannerBox = new QGroupBox(tr("WLAN-Code-Scanner-Server"));
    QGridLayout *scannerBoxLayout = new QGridLayout(scannerBox);
    wrapperLayout->addWidget(scannerBox);

    scannerBoxLayout->addWidget(new QLabel(tr("Standardport (TCP):")), ++row, 0);
    m_wlanCodeScannerServerPort = new QSpinBox;
    m_wlanCodeScannerServerPort->setMinimum(1024);
    m_wlanCodeScannerServerPort->setMaximum(65535);
    m_wlanCodeScannerServerPort->setValue(m_settings->wlanCodeScannerServerPort());
    scannerBoxLayout->addWidget(m_wlanCodeScannerServerPort, row, 1);

    scannerBoxLayout->addWidget(new QLabel(tr("Parameter für den Datensatz:")), ++row, 0);
    m_wlanCodeScannerServerKey = new QLineEdit(m_settings->wlanCodeScannerServerKey());
    QRegularExpression regularExpression(QStringLiteral("[a-zA-Z0-9]+"));
    QValidator *validator = new QRegularExpressionValidator(regularExpression, this);
    m_wlanCodeScannerServerKey->setValidator(validator);
    scannerBoxLayout->addWidget(m_wlanCodeScannerServerKey, row, 1);

    auto *scroll = new QScrollArea;
    scroll->setWidgetResizable(true);
    scroll->setWidget(wrapper);
    layout()->addWidget(scroll);

    QHBoxLayout *resetLayout = new QHBoxLayout;
    layout()->addLayout(resetLayout);
    resetLayout->addStretch();
    QPushButton *restoreButton = new QPushButton(tr("Zurücksetzen"));
    resetLayout->addWidget(restoreButton);
    connect(restoreButton, &QPushButton::clicked, this, &NetworkPage::restoreDefaultValues);
}

void NetworkPage::restoreDefaultValues()
{
    m_preferIpv6->setChecked(false);
    m_serverPort->setValue(DefaultValues::serverPort);
    m_connectTimeout->setValue(DefaultValues::connectTimeout);
    m_requestTimeout->setValue(DefaultValues::requestTimeout);
    m_discoverPort->setValue(DefaultValues::discoverServerPort);
    m_discoverTimeout->setValue(DefaultValues::discoverTimeout);
    m_discoverAttempts->setValue(DefaultValues::discoverAttempts);
    m_wlanCodeScannerServerPort->setValue(DefaultValues::wlanCodeScannerServerPort);
    m_wlanCodeScannerServerKey->setText(DefaultValues::wlanCodeScannerServerKey);
}

void NetworkPage::saveSettings()
{
    m_settings->savePreferIpv6(m_preferIpv6->isChecked());
    m_settings->saveServerPort(m_serverPort->value());
    m_settings->saveConnectTimeout(m_connectTimeout->value());
    m_settings->saveRequestTimeout(m_requestTimeout->value());
    m_settings->saveDiscoverPort(m_discoverPort->value());
    m_settings->saveDiscoverTimeout(m_discoverTimeout->value());
    m_settings->saveDiscoverAttempts(m_discoverAttempts->value());
    m_settings->saveWlanCodeScannerServerPort(m_wlanCodeScannerServerPort->value());
    const auto key = m_wlanCodeScannerServerKey->text();
    if (! key.isEmpty()) {
        m_settings->saveWlanCodeScannerServerKey(key);
    }
}
