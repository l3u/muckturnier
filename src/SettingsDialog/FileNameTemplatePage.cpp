// SPDX-FileCopyrightText: 2019-2022 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "FileNameTemplatePage.h"

#include "SharedObjects/Settings.h"

#include "shared/DefaultValues.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QDateTime>
#include <QMessageBox>

FileNameTemplatePage::FileNameTemplatePage(Settings *settings, QWidget *parent)
    : AbstractSettingsPage(tr("Vorlage Datenbankdateiname"), parent),
      m_settings(settings)
{
    QLabel *label;

    label = new QLabel(tr("Für neue Turnierdatenbanken wird der Dateiname aufgrund folgender "
                          "Vorlage voreingestellt:"));
    label->setWordWrap(true);
    layout()->addWidget(label);

    m_template = new QLineEdit;
    connect(m_template, &QLineEdit::textChanged, this, &FileNameTemplatePage::updateParsedFileName);
    layout()->addWidget(m_template);

    m_parsedFileName = new QLabel;
    layout()->addWidget(m_parsedFileName);

    m_template->setText(m_settings->dbFileNameTemplate());

    label = new QLabel(tr(
        "<p><b>Syntax:</b></p>"
        "<ul>"
        "<li>Alles in einfachen Anführungszeichen (<kbd>'</kbd>) wird unverändert ausgegeben</li>"
        "<li>Zwei aufeinanderfolgende einfache Anführungszeichen werden als ein einfaches "
            "Anführungszeichen ausgegeben</li>"
        "<li>Alle Datums- und Zeitkürzel werden durch die passenden Werte ersetzt. Beispiel: "
            "„yyyy“ mit der vierstelligen Jahreszahl, „yy“ mit der zweistelligen. "
            "Für alle möglichen Werte vgl. die Qt-Dokumentation zu "
            "<a href=\"https://doc.qt.io/qt-5/qdate.html#toString-2\">QDate</a> und "
            "<a href=\"https://doc.qt.io/qt-5/qtime.html#toString\">QTime</a>.</li>"
        "</ul>"));
    label->setWordWrap(true);
    label->setTextInteractionFlags(Qt::TextBrowserInteraction | Qt::TextSelectableByMouse);
    label->setOpenExternalLinks(true);
    layout()->addWidget(label);

    layout()->addStretch();

    QHBoxLayout *controlLayout = new QHBoxLayout;
    layout()->addLayout(controlLayout);
    controlLayout->addStretch();
    QPushButton *restoreTemplateButton = new QPushButton(tr("Zurücksetzen"));
    connect(restoreTemplateButton, &QPushButton::clicked,
            this, &FileNameTemplatePage::restoreTemplate);
    controlLayout->addWidget(restoreTemplateButton);
}

void FileNameTemplatePage::updateParsedFileName(const QString &text)
{
    m_parsedFileName->setText(tr("Geparste Vorlage: „%1.mtdb“").arg(
                                 QLocale::system().toString(QDateTime::currentDateTime(),
                                                            text.simplified())));
}

void FileNameTemplatePage::restoreTemplate()
{
    m_template->setText(DefaultValues::dbFileNameTemplate);
}

void FileNameTemplatePage::saveSettings()
{
    const QString templateText = m_template->text().simplified();
    m_settings->saveDbFileNameTemplate(templateText.isEmpty() ? DefaultValues::dbFileNameTemplate
                                                              : templateText);
}
