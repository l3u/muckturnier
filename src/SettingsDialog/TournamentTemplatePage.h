// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef TOURNAMENTTEMPPLATEPAGE_H
#define TOURNAMENTTEMPPLATEPAGE_H

// Local includes
#include "shared/AbstractSettingsPage.h"
#include "shared/TournamentTemplate.h"

// Local classes
class SharedObjects;
class Database;
class TournamentSettings;
class Settings;

// Qt classes
class QTableWidget;
class QTableWidgetItem;

class TournamentTemplatePage : public AbstractSettingsPage
{
    Q_OBJECT

public:
    explicit TournamentTemplatePage(SharedObjects *sharedObjects, QWidget *parent = nullptr);
    void saveSettings() override;

public Q_SLOTS:
    void adoptCurrentSettings();

private Q_SLOTS:
    void restoreTemplate();

private: // Enums
    enum Row {
        TournamentMode,
        BoogerScore,
        BoogersPerRound,
        DrawSettings,
        ScheduleSettings,
        RegistrationColumns,
        RegistrationListHeaderHidden,
        ScoreType,
        AutoSelectMode,
        IncludeOpponentGoals,
        GrayOutUnimportantGoals,
        OptionalPages,
        Count
    };

private: // Functions
    QTableWidgetItem *itemFor(Row row) const;
    QString boolText(bool state) const;
    void updateTemplateDisplay();

private: // Variables
    Database *m_db;
    Settings *m_settings;
    TournamentSettings *m_tournamentSettings;
    TournamentTemplate::Settings m_template;
    QTableWidget *m_templateDisplay;

};

#endif // TOURNAMENTTEMPPLATEPAGE_H
