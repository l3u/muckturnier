// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "StringsPage.h"
#include "SpacesLineEdit.h"

#include "SharedObjects/Settings.h"

#include "shared/DefaultValues.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QLabel>
#include <QComboBox>
#include <QMessageBox>
#include <QPushButton>
#include <QLineEdit>

static const QLatin1Char s_tabulator('\t');

StringsPage::StringsPage(Settings *settings, bool networkInUse, QWidget *parent)
    : AbstractSettingsPage(tr("Zeichenketten"), parent),
      m_settings(settings),
      m_networkInUse(networkInUse)
{
    QLabel *stringsLabel = new QLabel(tr("Folgende Zeichenketten für die Anzeige können "
                                         "eingestellt werden:"));
    stringsLabel->setWordWrap(true);
    layout()->addWidget(stringsLabel);

    QGridLayout *settingsLayout = new QGridLayout;
    layout()->addLayout(settingsLayout);

    int row = 0;

    settingsLayout->addWidget(new QLabel(tr("Bobbl-Symbol:")), row, 0);

    m_boogerSymbol = new QComboBox;
    m_boogerSymbol->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    m_boogerSymbol->addItem(QStringLiteral("\u2022")); // "BULLET"
    m_boogerSymbol->addItem(QStringLiteral("\u26AB")); // "MEDIUM BLACK CIRCLE"
    m_boogerSymbol->addItem(QStringLiteral("\u25CF")); // "BLACK CIRCLE", the default
    m_boogerSymbol->addItem(QStringLiteral("\u2B24")); // "BLACK LARGE CIRCLE"
    m_boogerSymbol->addItem(QStringLiteral("\u204E")); // "LOW ASTERISK"
    m_boogerSymbol->addItem(QStringLiteral("\u2055")); // "FLOWER PUNCTUATION MARK"
    m_boogerSymbol->addItem(QStringLiteral("\u274B")); // "HEAVY EIGHT TEARDROP-SPOKED PROPELLER"
    m_boogerSymbol->addItem(QStringLiteral("\u273D")); // "HEAVY TEARDROP-SPOKED ASTERISK"
    m_boogerSymbol->setToolTip(tr("<p>Es kann jedes sichtbare Unicode-Zeichen als Bobbl-Symbol "
                                  "benutzt werden. In der Liste finden sich ein paar Vorschläge "
                                  "für geeignete Zeichen.</p>"));
    m_boogerSymbol->setEditable(true);
    m_boogerSymbol->lineEdit()->setMaxLength(1);
    m_boogerSymbol->setEditText(m_settings->boogerSymbol());
    settingsLayout->addWidget(m_boogerSymbol, row, 1);

    settingsLayout->addWidget(new QLabel(tr("Trenner für Paarnamen:")), ++row, 0);

    m_namesSeparator = new SpacesLineEdit;
    m_namesSeparator->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    m_namesSeparator->setText(m_settings->namesSeparator());
    m_namesSeparator->setToolTip(tr("<p>Bei Turnieren mit festen Paaren wird diese Zeichenkette "
                                    "als Voreinstellung für die Suche nach allein gekommenen "
                                    "Spielern benutzt.</p>"
                                    "<p>Bei Einzelspieler-Turnieren werden die Paarnamen mit "
                                    "dieser Zeichenkette als Trenner generiert.</p>"));
    m_namesSeparator->setEnabled(! m_networkInUse);
    settingsLayout->addWidget(m_namesSeparator, row, 1);
    connect(m_namesSeparator, &SpacesLineEdit::textChanged,
            this, &StringsPage::updateSeparatorExample);

    QLabel *exampleLabel = new QLabel(tr("Beispiel:"));
    exampleLabel->setAlignment(Qt::AlignRight);
    settingsLayout->addWidget(exampleLabel, ++row, 0);

    m_separatorExample = new QLabel;
    settingsLayout->addWidget(m_separatorExample, row, 1);

    if (m_networkInUse) {
        auto *networkLabel = new QLabel(tr("<i>Im Netzwerkbetrieb kann der Trenner für Paarnamen "
                                           "nicht verändert werden</i>"));
        networkLabel->setWordWrap(true);
        settingsLayout->addWidget(networkLabel, ++row, 0, 1, 2);
    }

    layout()->addStretch();
    QHBoxLayout *controlLayout = new QHBoxLayout;
    layout()->addLayout(controlLayout);
    controlLayout->addStretch();

    QPushButton *resetStringsButton = new QPushButton(tr("Zurücksetzen"));
    connect(resetStringsButton, &QPushButton::clicked, this, &StringsPage::resetStrings);
    controlLayout->addWidget(resetStringsButton);

    updateSeparatorExample();
}

void StringsPage::updateSeparatorExample()
{
    QString text = m_namesSeparator->text();
    if (text.contains(s_tabulator)) {
        QMessageBox::warning(this,
            tr("Trenner für Paarnamen"),
            tr("<p>Das Verwenden von Tabulatoren im Trenner für Paarnamen verursacht Probleme "
               "beim Export der Turnierdaten als CSV, da hier Tabulatoren als Feldtrenner benutzt "
               "werden.</p>"
               "<p> Es können daher keine Tabulatoren im Trenner benutzt werden.</p>"));
        m_namesSeparator->setText(text.remove(s_tabulator));
        return;
    }
    m_separatorExample->setText(tr("„Müller Andreas%1Meier Bernd“").arg(text));
}

void StringsPage::resetStrings()
{
    m_boogerSymbol->setCurrentText(DefaultValues::boogerSymbol);

    if (! m_networkInUse) {
        m_namesSeparator->setText(DefaultValues::namesSeparator);
    } else {
        if (m_namesSeparator->text() != DefaultValues::namesSeparator) {
            QMessageBox::information(this, tr("Trenner für Paarnamen"),
                tr("<p><b>Trenner für Paarnamen</b></p>"
                   "<p>Im Netzwerkbetrieb kann der Trenner für Paarnamen nicht verändert werden. "
                   "Der eingestellte Trenner wird deswegen beibehalten und nicht auf den "
                   "Standardwert zurückgesetzt.</p>"));
        }
    }
}

void StringsPage::saveSettings()
{
    const QString boogerSymbol = m_boogerSymbol->currentText().simplified();
    m_settings->saveBoogerSymbol(boogerSymbol.isEmpty() ? DefaultValues::boogerSymbol
                                                        : boogerSymbol);
    m_settings->saveNamesSeparator(m_namesSeparator->text().isEmpty()
                                       ? DefaultValues::namesSeparator
                                       : m_namesSeparator->text());
}
