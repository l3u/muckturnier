// SPDX-FileCopyrightText: 2018-2021 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef CHECKSUMSDIALOG_H
#define CHECKSUMSDIALOG_H

// Local includes
#include "shared/TitleDialog.h"

// Local classes
class Database;

class ChecksumsDialog : public TitleDialog
{
    Q_OBJECT

public:
    explicit ChecksumsDialog(QWidget *parent, Database *db, bool isPairMode);

private: // Functions
    QString formatChecksum(QString checksum) const;

};

#endif // CHECKSUMSDIALOG_H
