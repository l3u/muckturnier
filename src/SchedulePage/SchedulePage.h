// SPDX-FileCopyrightText: 2023-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SCHEDULEPAGE_H
#define SCHEDULEPAGE_H

// Local includes
#include "shared/OptionalPage.h"

// Qt includes
#include <QDateTime>
#include <QMap>
#include <QPointer>

// Local classes
class SharedObjects;
class Database;
class NumbersComboBox;
class StopWatchEngine;
class StopWatch;
class ScheduleSettingsWidget;
class TournamentSettings;
class Settings;
class ScheduleDisplay;
class FullScreenMenu;
class DisplayButton;

// Qt classes
class QTimerEvent;
class QLabel;
class QPushButton;
class QTableWidget;
class QCheckBox;
class QMenu;

class SchedulePage : public OptionalPage
{
    Q_OBJECT

public:
    explicit SchedulePage(SharedObjects *sharedObjects, StopWatchEngine *stopWatchEngine,
                          QWidget *parent);
    void reload();
    void checkStopRound();
    void reset();

Q_SIGNALS:
    void raiseMe();

    void updateClock(const QString &text = QString());
    void updateCurrentRound(int round = 0);
    void updateRoundStart(const QString &text = QString());
    void updateRoundTimeLapsed(const QString &text = QString());
    void updateRoundTimeLeft(const QString &text = QString());
    void updatePenalty(const QString &text = QString());

    void updateNextRound(int round = 0);
    void updateNextRoundStart(const QString &text = QString());
    void updateTimeToNextRound(const QString &text = QString());

public Q_SLOTS:
    bool closePage() override;
    void closeDisplay();

protected:
    void timerEvent(QTimerEvent *) override;

private Q_SLOTS:
    void roundsStateChanged();
    void adjustTimer();
    void startRound();
    void prepareStopWatch();
    void stopRound(bool force = false);
    void displayTimes();
    void resetFromRound();
    void askResetAll();
    void showScheduleDisplay();

private: // Structs
    struct RoundTimes
    {
        QDateTime start;
        QDateTime end;
    };

private: // Functions
    int getFirstOpenRound() const;
    QString formatTime(const QDateTime &time) const;
    QString formatTime(int secs) const;
    QString formatDeviation(int secs) const;
    QString currentTime() const;
    QString dockTitle() const;
    void cacheSecs();
    void generateSchedule();
    void resetAll();

private: // Variables
    Database *m_db;
    Settings *m_settings;
    TournamentSettings *m_tournamentSettings;
    StopWatchEngine *m_stopWatchEngine;
    const QMap<int, bool> *m_roundsState;
    const QString m_displayLogo;

    int m_timerId = -1;
    bool m_adjustOngoing = false;
    bool m_checkStartRoundOngoing = false;

    ScheduleSettingsWidget *m_settingsWidget;

    NumbersComboBox *m_round;

    QPushButton *m_startRound;
    QPushButton *m_stopRound;
    QPushButton *m_extras;
    QCheckBox *m_startStopWatch;

    QPushButton *m_preview;

    QTableWidget *m_schedule;

    bool m_scheduleGenerated = false;

    int m_roundSecs = 0.0;
    double m_boogerSecs = 0.0;
    int m_breakSecs = 0;

    int m_scheduledRounds = 0;
    int m_scheduleStartRound = 0;
    int m_currentRound = 0;

    int m_roundTimeLeft = 0;

    QDateTime m_currentTime;
    QDateTime m_nextRoundStart;

    int m_dbScheduleStartRound = 0;
    QDateTime m_dbScheduleStartTime;

    QMap<int, RoundTimes> m_scheduleTimes;
    QMap<int, RoundTimes> m_roundsTimes;

    QPointer<StopWatch> m_stopWatch;
    bool m_triggerStartStopWatch = false;

    bool m_triggerStopWatchByReload = false;

    DisplayButton *m_displayButton;
    QPointer<ScheduleDisplay> m_scheduleDisplay;
    FullScreenMenu *m_fullScreenMenu;

};

#endif // SCHEDULEPAGE_H
