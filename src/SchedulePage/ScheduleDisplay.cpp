// SPDX-FileCopyrightText: 2023-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "ScheduleDisplay.h"
#include "ResizableLabel.h"
#include "ImageWidget.h"
#include "MuckturnierLabel.h"
#include "FontWidgetAction.h"
#include "Logging.h"

#include "SharedObjects/Settings.h"

#include "shared/Json.h"
#include "shared/ColorHelper.h"

// Qt includes
#include <QStackedLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDebug>
#include <QPushButton>
#include <QMenu>
#include <QLabel>
#include <QWidgetAction>
#include <QPalette>
#include <QColorDialog>
#include <QJsonObject>
#include <QJsonArray>
#include <QMessageBox>
#include <QWindow>
#include <QScreen>
#include <QCursor>
#include <QFileDialog>
#include <QCloseEvent>
#include <QGridLayout>
#include <QSpinBox>
#include <QInputDialog>

// C++ includes
#include <functional>

static constexpr int s_settingsVersion = 2;

// Global settings

static const QLatin1String s_colorsSettings("colors");
static const QLatin1String s_background("background");
static const QLatin1String s_negativeTime("negative time");
static const QLatin1String s_text("text");

static const QLatin1String s_margins("margins");

// Images
static const QLatin1String s_imagesSettings("images");
static const QLatin1String s_welcome("welcome");
static const QLatin1String s_schedule("schedule");
static const QLatin1String s_goodBye("good bye");

// Per-label settings
static const QLatin1String s_labelSettings("label settings");
static const QLatin1String s_alignment("alignment");
static const QLatin1String s_fontFamily("font family");
static const QLatin1String s_color("color");
static const QLatin1String s_bold("bold");
static const QLatin1String s_maxHeight("max height");
static const QLatin1String s_stretch("stretch");

static const QHash<Qt::AlignmentFlag, QString> s_alignmentToString {
    { Qt::AlignLeft,   QLatin1String("left")   },
    { Qt::AlignCenter, QLatin1String("center") },
    { Qt::AlignRight,  QLatin1String("right")  }
};

static const QHash<QString, Qt::AlignmentFlag> s_alignmentFromString {
    { QLatin1String("left"),   Qt::AlignLeft   },
    { QLatin1String("center"), Qt::AlignCenter },
    { QLatin1String("right"),  Qt::AlignRight  }
};

static const QLatin1String s_clock("clock");
static const QLatin1String s_roundOrBreak("round or break");
static const QLatin1String s_roundOrBreakTimeLeft("round or break time left");
static const QLatin1String s_nextRoundStart("next round start");
static const QLatin1String s_logo("logo");

static const QVector<QString> s_allLabelIds {
    s_clock,
    s_roundOrBreak,
    s_roundOrBreakTimeLeft,
    s_nextRoundStart
};

ScheduleDisplay::ScheduleDisplay(QWidget *parent, Settings *settings, const QString &dbPath,
                                 const QString &displayLogo)
    : FullScreenDialog(parent),
      m_settings(settings),
      m_dbPath(dbPath)
{
    setWindowTitle(tr("Zeitplan-Display"));
    setAutoFillBackground(true);

    m_fontWidgetAction = new FontWidgetAction(this);

    // Setup the context menu

    m_menu = new QMenu(this);

    m_menu->addAction(fullScreenAction());

    auto *showMuckturnier = m_menu->addAction(tr("Logo ausblenden"));
    showMuckturnier->setCheckable(true);
    connect(showMuckturnier, &QAction::toggled,
            this, [this](bool checked)
            {
                m_logo->setVisible(! checked);
                for (const auto &id : s_allLabelIds) {
                    m_labels[id]->resizeText();
                }
            });

    m_menu->addSeparator();

    auto *loadImageMenu = m_menu->addMenu(tr("Hintergrundbild laden"));

    auto *loadWelcome = loadImageMenu->addAction(tr("Willkommen-Seite"));
    connect(loadWelcome, &QAction::triggered,
            this, std::bind(&ScheduleDisplay::loadImage, this, Page::Welcome, QString()));

    auto *loadSchedule = loadImageMenu->addAction(tr("Zeit-Anzeige-Seite"));
    connect(loadSchedule, &QAction::triggered,
            this, std::bind(&ScheduleDisplay::loadImage, this, Page::Schedule, QString()));

    auto *loadGoodBye = loadImageMenu->addAction(tr("Auf-Wiedersehen-Seite"));
    connect(loadGoodBye, &QAction::triggered,
            this, std::bind(&ScheduleDisplay::loadImage, this, Page::GoodBye, QString()));

    auto *removeImageMenu = m_menu->addMenu(tr("Bild entfernen"));

    auto *removeWelcome = removeImageMenu->addAction(tr("Willkommen-Seite"));
    connect(removeWelcome, &QAction::triggered,
            this, std::bind(&ScheduleDisplay::removeImage, this, Page::Welcome));

    auto *removeSchedule = removeImageMenu->addAction(tr("Zeit-Anzeige-Seite"));
    connect(removeSchedule, &QAction::triggered,
            this, std::bind(&ScheduleDisplay::removeImage, this, Page::Schedule));

    auto *removeGoodBye = removeImageMenu->addAction(tr("Auf-Wiedersehen-Seite"));
    connect(removeGoodBye, &QAction::triggered,
            this, std::bind(&ScheduleDisplay::removeImage, this, Page::GoodBye));

    m_menu->addSeparator();

    auto *pageMenu = m_menu->addMenu(tr("Angezeigte Seite"));

    auto *showWelcome = pageMenu->addAction(tr("Willkommen-Seite"));
    connect(showWelcome, &QAction::triggered,
            this, std::bind(&ScheduleDisplay::showPage, this, Page::Welcome));

    auto *showSchedule = pageMenu->addAction(tr("Zeit-Anzeige"));
    connect(showSchedule, &QAction::triggered,
            this, std::bind(&ScheduleDisplay::showPage, this, Page::Schedule));

    auto *showGoodBye = pageMenu->addAction(tr("Auf-Wiedersehen-Seite"));
    connect(showGoodBye, &QAction::triggered,
            this, std::bind(&ScheduleDisplay::showPage, this, Page::GoodBye));

    m_menu->addSeparator();

    auto *colorsMenu = m_menu->addMenu(tr("Farben Zeit-Anzeige-Seite"));

    auto *background = colorsMenu->addAction(tr("Hintergrund"));
    connect(background, &QAction::triggered,
            this, std::bind(&ScheduleDisplay::chooseColor, this, s_background));

    auto *allLabelsColor = colorsMenu->addAction(tr("Alle Texte"));
    connect(allLabelsColor, &QAction::triggered,
            this, std::bind(&ScheduleDisplay::chooseColor, this, s_text));

    auto *negativeTime = colorsMenu->addAction(tr("Negative Zeit"));
    connect(negativeTime, &QAction::triggered,
            this, std::bind(&ScheduleDisplay::chooseColor, this, s_negativeTime));

    m_fontMenu = m_menu->addMenu(tr("Schriftart für alle Texte"));
    connect(m_fontMenu, &QMenu::aboutToShow, this, &ScheduleDisplay::startFontMenu);
    connect(m_fontMenu, &QMenu::aboutToHide, this, &ScheduleDisplay::finishFontMenu);

    m_menu->addSeparator();

    m_setFormattingVisible = m_menu->addAction(tr("Formatierungsoptionen anzeigen"));
    m_setFormattingVisible->setCheckable(true);
    connect(m_setFormattingVisible, &QAction::toggled, this,
            &ScheduleDisplay::setFormattingVisible);

    auto *marginsWidget = new QWidget(this);
    auto *margingsLayout = new QGridLayout(marginsWidget);
    int marginRow = -1;
    for (const auto pos : { Qt::AlignTop, Qt::AlignRight, Qt::AlignBottom, Qt::AlignLeft }) {
        if (pos == Qt::AlignTop) {
            margingsLayout->addWidget(new QLabel(tr("Oben:")), ++marginRow, 0);
        } else if (pos == Qt::AlignRight) {
            margingsLayout->addWidget(new QLabel(tr("Rechts:")), ++marginRow, 0);
        } else if (pos == Qt::AlignBottom) {
            margingsLayout->addWidget(new QLabel(tr("Unten:")), ++marginRow, 0);
        } else if (pos == Qt::AlignLeft) {
            margingsLayout->addWidget(new QLabel(tr("Links:")), ++marginRow, 0);
        }

        auto *spinBox = new QSpinBox;
        spinBox->setMinimum(0);
        spinBox->setMaximum(999);
        spinBox->setValue(0);
        connect(spinBox, &QSpinBox::valueChanged,
                this, std::bind(&ScheduleDisplay::setMargin, this, pos, std::placeholders::_1));
        m_margins.insert(pos, spinBox);
        margingsLayout->addWidget(spinBox, marginRow, 1);
    }

    auto *marginsAction = new QWidgetAction(this);
    marginsAction->setDefaultWidget(marginsWidget);

    auto *marginsMenu = m_menu->addMenu(tr("Ränder"));
    marginsMenu->addAction(marginsAction);

    m_menu->addSeparator();

    auto *settingsMenu = m_menu->addMenu(tr("Einstellungen"));

    m_loadSettingsMenu = settingsMenu->addMenu(tr("Laden …"));
    settingsMenu->addSeparator();

    auto *saveSettingsAction = settingsMenu->addAction(tr("Speichern"));
    connect(saveSettingsAction, &QAction::triggered, this, [this]
            {
                saveSettings(m_currentSettingsId);
            });
    m_saveSettingsMenu = settingsMenu->addMenu(tr("Speichern als …"));
    settingsMenu->addSeparator();

    auto *resetSettingsAction = settingsMenu->addAction(tr("Zurücksetzen"));
    connect(resetSettingsAction, &QAction::triggered, this, &ScheduleDisplay::resetSettings);

    m_deleteSettingsMenu = settingsMenu->addMenu(tr("Löschen …"));

    m_menu->addSeparator();

    auto *closeAction = m_menu->addAction(tr("Fenster schließen"));
    connect(closeAction, &QAction::triggered, this, &QDialog::accept);

    setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, &QWidget::customContextMenuRequested,
            this, [this]
            {
                m_menu->exec(QCursor::pos());
            });

    // Setup the layout

    m_layout = new QStackedLayout(this);
    m_layout->setContentsMargins(0, 0, 0, 0);

    m_welcome = new ImageWidget(tr("<span style=\"font-size: x-large;\">"
                                   "Bitte ein<br/>"
                                   "Willkommen-Hintergrundbild<br/>"
                                   "auswählen"
                                   "</span>"));
    m_pagesMap.insert(Page::Welcome, m_welcome);
    m_layout->addWidget(m_welcome);

    m_schedule = new ImageWidget;
    m_pagesMap.insert(Page::Schedule, m_schedule);
    m_layout->addWidget(m_schedule);

    m_goodBye = new ImageWidget(tr("<span style=\"font-size: x-large;\">"
                                   "Bitte ein<br/>"
                                   "Auf-Wiedersehen-Hintergrundbild<br/>"
                                   "auswählen"
                                   "</span>"));
    m_pagesMap.insert(Page::GoodBye, m_goodBye);
    m_layout->addWidget(m_goodBye);

    auto *wrapperLayout = new QHBoxLayout(m_schedule);
    wrapperLayout->setContentsMargins(0, 0, 0, 0);
    const auto originalSpacing = wrapperLayout->spacing();
    wrapperLayout->setSpacing(0);

    auto *leftSpacerLayout = new QVBoxLayout;
    wrapperLayout->addLayout(leftSpacerLayout);
    auto *leftSpacer = new QLabel;
    leftSpacer->setMaximumWidth(0);
    leftSpacer->setMinimumWidth(0);
    leftSpacer->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Ignored);
    m_spacers.insert(Qt::AlignLeft, leftSpacer);
    leftSpacerLayout->addWidget(leftSpacer);

    m_labelsWrapper = new QWidget;
    auto *labelsLayout = new QVBoxLayout(m_labelsWrapper);
    labelsLayout->setContentsMargins(0, 0, 0, 0);
    labelsLayout->setSpacing(originalSpacing);
    wrapperLayout->addWidget(m_labelsWrapper);

    auto *topSpacer = new QLabel;
    topSpacer->setMaximumHeight(0);
    topSpacer->setMinimumHeight(0);
    m_spacers.insert(Qt::AlignTop, topSpacer);
    topSpacer->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Maximum);
    labelsLayout->addWidget(topSpacer);

    // All labels

    auto *clock = new ResizableLabel(m_fontWidgetAction);
    m_labels.insert(s_clock, clock);
    connect(clock, &ResizableLabel::stretchFactorChanged,
            labelsLayout, std::bind(QOverload<QWidget *, int>::of(&QVBoxLayout::setStretchFactor),
                                    labelsLayout, clock, std::placeholders::_1));
    labelsLayout->addWidget(clock);

    auto *roundOrBreak = new ResizableLabel(m_fontWidgetAction);
    m_labels.insert(s_roundOrBreak, roundOrBreak);
    connect(roundOrBreak, &ResizableLabel::stretchFactorChanged,
            labelsLayout, std::bind(QOverload<QWidget *, int>::of(&QVBoxLayout::setStretchFactor),
                                    labelsLayout, roundOrBreak, std::placeholders::_1));
    labelsLayout->addWidget(roundOrBreak);

    auto *roundOrBreakTimeLeft = new ResizableLabel(m_fontWidgetAction);
    m_labels.insert(s_roundOrBreakTimeLeft, roundOrBreakTimeLeft);
    connect(roundOrBreakTimeLeft, &ResizableLabel::stretchFactorChanged,
            labelsLayout, std::bind(QOverload<QWidget *, int>::of(&QVBoxLayout::setStretchFactor),
                                    labelsLayout, roundOrBreakTimeLeft, std::placeholders::_1));
    labelsLayout->addWidget(roundOrBreakTimeLeft);

    auto *nextRoundStart = new ResizableLabel(m_fontWidgetAction);
    m_labels.insert(s_nextRoundStart, nextRoundStart);
    connect(nextRoundStart, &ResizableLabel::stretchFactorChanged,
            labelsLayout, std::bind(QOverload<QWidget *, int>::of(&QVBoxLayout::setStretchFactor),
                                    labelsLayout, nextRoundStart, std::placeholders::_1));
    labelsLayout->addWidget(nextRoundStart);

    // The "Muckturnier.org" label
    m_logo = new MuckturnierLabel(displayLogo);
    connect(m_logo, &MuckturnierLabel::stretchFactorChanged,
            labelsLayout, std::bind(QOverload<QWidget *, int>::of(&QVBoxLayout::setStretchFactor),
                                    labelsLayout, m_logo, std::placeholders::_1));
    labelsLayout->addWidget(m_logo);

    auto *bottomSpacer = new QLabel;
    bottomSpacer->setMaximumHeight(0);
    bottomSpacer->setMinimumHeight(0);
    bottomSpacer->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Maximum);
    m_spacers.insert(Qt::AlignBottom, bottomSpacer);
    labelsLayout->addWidget(bottomSpacer);

    auto *rightSpacerLayout = new QVBoxLayout;
    wrapperLayout->addLayout(rightSpacerLayout);
    auto *rightSpacer = new QLabel;
    rightSpacer->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Ignored);
    m_spacers.insert(Qt::AlignRight, rightSpacer);
    rightSpacerLayout->addWidget(rightSpacer);

    updateSettingsMenus();
    m_currentSettingsId = m_settings->lastUsedScheduleDisplaySettings();
    loadSettings(m_currentSettingsId);
    showPage(Page::Schedule);
}

void ScheduleDisplay::updateSettingsMenus()
{
    m_loadSettingsMenu->clear();
    m_saveSettingsMenu->clear();
    m_deleteSettingsMenu->clear();

    auto *newEntry = m_saveSettingsMenu->addAction(tr("Neuer Eintrag"));
    connect(newEntry, &QAction::triggered, this, &ScheduleDisplay::saveNewSettings);
    m_saveSettingsMenu->addSeparator();

    QAction *action;

    const auto settings = m_settings->savedScheduleDisplaySettings();
    for (const auto &id : settings) {
        const QString text = id.isEmpty() ? tr("(unbenannt)") : id;

        action = m_loadSettingsMenu->addAction(text);
        connect(action, &QAction::triggered,
                this, std::bind(&ScheduleDisplay::loadSettings, this, id));

        action = m_saveSettingsMenu->addAction(text);
        connect(action, &QAction::triggered,
                this, std::bind(&ScheduleDisplay::saveSettings, this, id));

        action = m_deleteSettingsMenu->addAction(text);
        connect(action, &QAction::triggered,
                this, std::bind(&ScheduleDisplay::deleteSettings, this, id));
    }

    if (settings.isEmpty()) {
        for (auto *menu : { m_loadSettingsMenu, m_saveSettingsMenu, m_deleteSettingsMenu }) {
            action = menu->addAction(tr("Keine Einstellungen gespeichert"));
            action->setEnabled(false);
        }
    }
}

void ScheduleDisplay::closeEvent(QCloseEvent *event)
{
    if (currentSettings() != m_savedSettings) {
        const auto reply = QMessageBox::question(this, tr("Einstellungen geändert"),
            tr("<p>Die Einstellungen für das Info-Display wurden geändert.</p>"
               "<p>Sollen die Änderungen gespeichert werden?</p>"),
            QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel, QMessageBox::Cancel);

        if (reply == QMessageBox::Cancel) {
            event->ignore();
            return;
        } else if (reply == QMessageBox::Yes) {
            saveSettings(m_currentSettingsId);
        }
    }

    QDialog::closeEvent(event);
}

void ScheduleDisplay::updateClock(const QString &text)
{
    m_labels[s_clock]->setText(text);
}

void ScheduleDisplay::updateCurrentRound(int round)
{
    m_currentRound = round;

    if (! m_scheduleStarted) {
        m_labels[s_roundOrBreak]->setText(tr("Keine laufende Runde"));
    } else if (m_currentRound == 0) {
        m_labels[s_roundOrBreak]->setText(tr("Pause"));
    } else {
        m_labels[s_roundOrBreak]->setText(tr("Runde %1").arg(m_currentRound));
    }
}

void ScheduleDisplay::updateNextRound(int round)
{
    m_nextRound = round;
}

void ScheduleDisplay::setTimeToNextRoundText(const QString &text)
{
    const auto strippedText = tr("Verbleibend: %1").arg(text.isEmpty() ? tr("–") : text);
    auto displayText = strippedText;
    if (text.startsWith(tr("- "))) {
        displayText = tr("Verbleibend: <span style=\"color: %1;\">%2</span>").arg(
                         m_colors[s_negativeTime].name(), text);
    }
    m_labels[s_roundOrBreakTimeLeft]->setText(displayText, strippedText);
}

void ScheduleDisplay::updateRoundTimeLeft(const QString &text)
{
    if (m_currentRound != 0) {
        setTimeToNextRoundText(text);
    }
}

void ScheduleDisplay::updateTimeToNextRound(const QString &text)
{
    if (m_currentRound == 0) {
        setTimeToNextRoundText(text);
    }
}

void ScheduleDisplay::updateNextRoundStart(const QString &text)
{
    m_labels[s_nextRoundStart]->setText(
        tr("Beginn der nächsten Runde: %1").arg(text.isEmpty() ? tr("–") : text));
}

void ScheduleDisplay::setFormattingVisible(bool state)
{
    for (auto *label : m_labels.values()) {
        label->setFormattingVisible(state);
    }
    m_logo->setFormattingVisible(state);

    for (auto pos : { Qt::AlignTop, Qt::AlignRight, Qt::AlignBottom, Qt::AlignLeft }) {
        m_spacers[pos]->setStyleSheet(state ? QStringLiteral("background-color: red;")
                                            : QStringLiteral("background-color: transparent;"));
    }
}

void ScheduleDisplay::setMargin(Qt::Alignment pos, int margin)
{
    auto *spacer = m_spacers[pos];
    if (pos == Qt::AlignLeft || pos == Qt::AlignRight) {
        spacer->setMinimumWidth(margin);
        spacer->setMaximumWidth(margin);
    } else {
        spacer->setMinimumHeight(margin);
        spacer->setMaximumHeight(margin);
    }
}

void ScheduleDisplay::chooseColor(const QString &name)
{
    Q_ASSERT(name == s_background || name == s_text || name == s_negativeTime);

    QColor preset;
    if (name == s_text) {
        preset = m_labels[s_allLabelIds.first()]->color();
        for (int i = 1; i < s_allLabelIds.count(); i++) {
            const auto color = m_labels[s_allLabelIds.at(i)]->color();
            if (color != preset) {
                preset = Qt::black;
                break;
            }
        }
    } else {
        preset = m_colors.value(name);
    }

    const auto color = QColorDialog::getColor(preset);
    if (! color.isValid()) {
        return;
    }

    if (name == s_negativeTime || name == s_background) {
        m_colors[name] = color;
    } else if (name == s_text) {
        for (const auto &id : s_allLabelIds) {
            m_labels[id]->setColor(color);
        }
    }

    if (name == s_background) {
        m_schedule->setBackgroundColor(m_colors.value(s_background));
    }
}

QJsonObject ScheduleDisplay::currentSettings() const
{
    QJsonObject settings;

    // Colors
    QJsonObject colorsObject;
    for (const auto &colorName : { s_background, s_negativeTime }) {
        colorsObject.insert(colorName, ColorHelper::colorToString(m_colors.value(colorName)));
    };
    settings.insert(s_colorsSettings, colorsObject);

    // Margins

    // Just like with CSS's shorthand margin: top right bottom left
    settings.insert(s_margins, QJsonArray { m_spacers.value(Qt::AlignTop)->maximumHeight(),
                                            m_spacers.value(Qt::AlignRight)->maximumWidth(),
                                            m_spacers.value(Qt::AlignBottom)->maximumHeight(),
                                            m_spacers.value(Qt::AlignLeft)->maximumWidth() });

    // Label settings

    QJsonObject labelSettings;

    for (const auto &id : s_allLabelIds) {
        QJsonObject currentSettings;
        const auto *label = m_labels.value(id);
        currentSettings.insert(s_alignment,  s_alignmentToString.value(label->alignment()));
        currentSettings.insert(s_fontFamily, label->fontFamily());
        currentSettings.insert(s_bold,       label->isBold());
        currentSettings.insert(s_maxHeight,  label->maxHeight());
        currentSettings.insert(s_stretch,    label->stretch());
        currentSettings.insert(s_color,      ColorHelper::colorToString(label->color()));
        labelSettings.insert(id, currentSettings);
    }

    // Logo settings
    QJsonObject logoSettings;
    logoSettings.insert(s_maxHeight,  m_logo->maxHeight());
    logoSettings.insert(s_stretch,    m_logo->stretch());
    labelSettings.insert(s_logo, logoSettings);

    settings.insert(s_labelSettings, labelSettings);

    // Images settings
    const auto welcomeFile = m_welcome->backgroundImagePath();
    const auto scheduleFile = m_schedule->backgroundImagePath();
    const auto goodByeFile = m_goodBye->backgroundImagePath();
    if (! welcomeFile.isEmpty() || ! scheduleFile.isEmpty() || ! goodByeFile.isEmpty()) {
        QJsonObject imagesSettings;
        if (! welcomeFile.isEmpty()) {
            imagesSettings.insert(s_welcome, welcomeFile);
        }
        if (! scheduleFile.isEmpty()) {
            imagesSettings.insert(s_schedule, scheduleFile);
        }
        if (! goodByeFile.isEmpty()) {
            imagesSettings.insert(s_goodBye, goodByeFile);
        }
        settings.insert(s_imagesSettings, imagesSettings);
    }

    return settings;
}

void ScheduleDisplay::saveNewSettings()
{
    bool ok = false;
    const auto id = QInputDialog::getText(this, tr("Einstellungen speichern als"),
        tr("Bitte einen Namen für die Einstellungen eingeben"), QLineEdit::Normal, QString(), &ok);
    if (! ok) {
        return;
    }

    if (id.toLower() == QStringLiteral("version")) {
        QMessageBox::warning(this, tr("Einstellungen speichern als"),
            tr("<p>Die Bezeichnung „Version“ (unabhängig von Groß- und Kleinschreibung) kann aus "
               "technischen Gründen nicht vergeben werden.</p>"
               "<p>Bitte einen anderen Namen wählen!</p>"));
        return;
    }

    if (id == tr("(unbenannt)")
        && QMessageBox::warning(this, tr("Einstellungen speichern als"),
               tr("<p>Die Bezeichnung „(unbenannt)“ wird für Einstellungen angezeigt, für die kein "
                  "Name vergeben wurde. Wenn die Einstellungen jetzt als „(unbenannt)“ gespeichert "
                  "werden, dann sind sie von Einstellungen ohne Namen nicht zu unterscheiden, und "
                  "der Eintrag „(unbenannt)“ taucht evtl. doppelt auf.</p>"
                  "<p>Sollen die Einstellungen wirklich als „(unbenannt)“ gespeichert werden?"
                  "</p>"),
                  QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Cancel)
           == QMessageBox::Cancel) {

        return;

    }

    if (! m_settings->scheduleDisplaySettings(id).isEmpty()
        && QMessageBox::warning(this, tr("Einstellungen speichern als"),
               tr("<p>Es sind bereits Einstellungen unter dem Namen „%1“ gespeichert.</p>"
                  "<p>Sollen diese Einstellungen überschrieben werden?</p>").arg(
                  id.isEmpty() ? tr("(unbenannt)") : id.toHtmlEscaped()),
               QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Cancel)
           == QMessageBox::Cancel) {

        return;
    }

    m_currentSettingsId = id;
    saveSettings(id);
}

void ScheduleDisplay::saveSettings(const QString &id)
{
    if (id != m_currentSettingsId
        && QMessageBox::warning(this, tr("Einstellungen speichern"),
               tr("<p>Es sind bereits Einstellungen unter dem Namen „%1“ gespeichert.</p>"
                  "<p>Sollen diese Einstellungen überschrieben werden?</p>").arg(
                  id.isEmpty() ? tr("(unbenannt)") : id.toHtmlEscaped()),
               QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Cancel)
           == QMessageBox::Cancel) {

        return;
    }

    m_savedSettings = currentSettings();
    m_settings->saveScheduleDisplaySettings(id, Json::serialize(m_savedSettings),
                                            s_settingsVersion);
    qCDebug(ScheduleDisplayLog) << "Saved settings with ID" << id;
    QMessageBox::information(this, tr("Einstellungen speichern"),
        tr("Einstellungen für „%1“ gespeichert!").arg(id.isEmpty() ? tr("(unbenannt)") : id));

    updateSettingsMenus();
}

void ScheduleDisplay::deleteSettings(const QString &id)
{
    if (QMessageBox::warning(this, tr("Einstellungen löschen"),
            tr("Sollen die Einstellungen mit dem Namen „%1“ wirklich gelöscht werden?").arg(
               id.isEmpty() ? tr("(unbenannt)") : id.toHtmlEscaped()),
               QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Cancel)
           == QMessageBox::Cancel) {

        return;
    }

    m_settings->saveScheduleDisplaySettings(id, QString(), s_settingsVersion);
    if (m_currentSettingsId == id) {
        m_currentSettingsId.clear();
        m_settings->saveLastUsedDisplaySettings(QString());
        resetSettings();
    }

    qCDebug(ScheduleDisplayLog) << "Deleted settings with ID" << id;
    updateSettingsMenus();
}

void ScheduleDisplay::resetSettings()
{
    m_colors[s_background]   = QColor(47,  135, 76);
    m_colors[s_negativeTime] = QColor(228, 62,  57);
    m_schedule->setBackgroundColor(m_colors.value(s_background));

    m_margins[Qt::AlignTop]->setValue(20);
    m_margins[Qt::AlignRight]->setValue(50);
    m_margins[Qt::AlignBottom]->setValue(20);
    m_margins[Qt::AlignLeft]->setValue(50);

    ResizableLabel *label;
    const auto fontFamily = font().family();

    label = m_labels.value(s_clock);
    label->setAlignment(Qt::AlignRight);
    label->setBold(false);
    label->setMaxHeight(70);
    label->setStretch(2);
    label->setColor(QColor(238, 202, 67));
    m_colors[s_clock] = QColor(238, 202, 67);
    label->setFontFamily(fontFamily);

    label = m_labels.value(s_roundOrBreak);
    label->setAlignment(Qt::AlignCenter);
    label->setBold(true);
    label->setMaxHeight(230);
    label->setStretch(6);
    label->setColor(QColor(255, 255, 255));
    m_colors[s_roundOrBreak] = QColor(255, 255, 255);
    label->setFontFamily(fontFamily);

    label = m_labels.value(s_roundOrBreakTimeLeft);
    label->setAlignment(Qt::AlignCenter);
    label->setBold(false);
    label->setMaxHeight(190);
    label->setStretch(5);
    label->setColor(QColor(255, 255, 255));
    m_colors[s_roundOrBreakTimeLeft] = QColor(255, 255, 255);
    label->setFontFamily(fontFamily);

    label = m_labels.value(s_nextRoundStart);
    label->setAlignment(Qt::AlignCenter);
    label->setBold(false);
    label->setMaxHeight(120);
    label->setStretch(5);
    label->setColor(QColor(238, 202, 67));
    m_colors[s_nextRoundStart] = QColor(238, 202, 67);
    label->setFontFamily(fontFamily);

    m_logo->setMaxHeight(70);
    m_logo->setStretch(2);

    removeImage(Page::Welcome);
    removeImage(Page::Schedule);
    removeImage(Page::GoodBye);

    showPage(Page::Schedule);
}

void ScheduleDisplay::loadSettings(const QString &id)
{
    qCDebug(ScheduleDisplayLog) << "Loading settings for ID" << id;

    const auto version = m_settings->scheduleDisplaySettingsVersion(id);
    const auto settings = m_settings->scheduleDisplaySettings(id);
    bool fallBack = false;

    if (settings.isEmpty() && version == 0) {
        qCDebug(ScheduleDisplayLog) << "No schedule display settings saved yet"
                                    << "- falling back to defaults";
        fallBack = true;
    } else if (version > s_settingsVersion) {
        qCDebug(ScheduleDisplayLog) << "Schedule display settings version is" << version
                                    << "expected:" << s_settingsVersion
                                    << "- falling back to defaults";
        fallBack = true;
    } else if (settings.isEmpty()) {
        qCDebug(ScheduleDisplayLog) << "No schedule display settings found"
                                    << "- falling back to defaults";
        fallBack = true;
    }

    if (fallBack) {
        resetSettings();
        m_savedSettings = currentSettings();
        return;
    }

    m_savedSettings = Json::objectFromString(settings);

    // Colors

    const auto colors = m_savedSettings.value(s_colorsSettings).toObject();

    for (const auto &colorName : { s_background, s_negativeTime }) {
        const auto color = colors.value(colorName);
        if (color.isUndefined()) {
            qCDebug(ScheduleDisplayLog) << "color for" << colorName
                                        << "is not defined - falling back to defaults";
            resetSettings();
            return;
        }
        m_colors[colorName] = ColorHelper::colorFromString(color.toString());
    }

    m_schedule->setBackgroundColor(m_colors.value(s_background));

    if (version == 1) {
        const auto color = colors.value(s_text);
        if (color.isUndefined()) {
            qCDebug(ScheduleDisplayLog) << "text color is not defined - falling back to defaults";
            resetSettings();
            return;
        }
        const auto textColor = ColorHelper::colorFromString(color.toString());
        for (const auto &id : s_allLabelIds) {
            m_colors[id] = textColor;
        }
        qCDebug(ScheduleDisplayLog) << "Using the version 1 global text color for all labels";
    }

    // Margins

    const auto marginsValue = m_savedSettings.value(s_margins);
    if (marginsValue.isUndefined()) {
        qCDebug(ScheduleDisplayLog) << "Margins not defined - falling back to defaults";
        resetSettings();
        return;
    }

    const auto margins = marginsValue.toArray();
    if (margins.count() != 4) {
        qCDebug(ScheduleDisplayLog) << "Margins definition invalid - falling back to defaults";
        resetSettings();
        return;
    }

    m_margins[Qt::AlignTop]->setValue(margins.at(0).toInt());
    m_margins[Qt::AlignRight]->setValue(margins.at(1).toInt());
    m_margins[Qt::AlignBottom]->setValue(margins.at(2).toInt());
    if (version == 1) {
        qCDebug(ScheduleDisplayLog) << "Adjusting version 1 left margin by adding 50px";
        m_margins[Qt::AlignLeft]->setValue(margins.at(3).toInt() + 50);
    } else {
        m_margins[Qt::AlignLeft]->setValue(margins.at(3).toInt());
    }

    // Labels

    const auto labelSettingsValue = m_savedSettings.value(s_labelSettings);
    if (labelSettingsValue.isUndefined()) {
        qCDebug(ScheduleDisplayLog) << "Label settings not found - falling back to defaults";
        resetSettings();
        return;
    }

    const auto labelSettings = labelSettingsValue.toObject();

    for (const auto &id : s_allLabelIds) {
        const auto settingsValue = labelSettings.value(id);
        if (settingsValue.isUndefined()) {
            qCDebug(ScheduleDisplayLog) << "Label settings for" << id
                                        << "not found - falling back to defaults";
            resetSettings();
            return;
        }

        const auto settings = settingsValue.toObject();
        auto *label = m_labels.value(id);

        // Color
        QColor color;
        if (! m_colors.contains(id)) {
            // We're using version > 1
            const auto colorValue = settings.value(s_color);
            if (colorValue.isUndefined()) {
                qCDebug(ScheduleDisplayLog) << "No color setting found for" << id
                                            << "- falling back to defaults";
                resetSettings();
                return;
            }
            color = ColorHelper::colorFromString(colorValue.toString());
            m_colors[id] = color;
        } else {
            color = m_colors[id];
        }
        label->setColor(color);

        // Alignment

        const auto alignmentValue = settings.value(s_alignment);
        if (alignmentValue.isUndefined()) {
            qCDebug(ScheduleDisplayLog) << "No alignment setting found for" << id
                                        << "- falling back to defaults";
            resetSettings();
            return;
        }

        const auto alignment = alignmentValue.toString();
        if (! s_alignmentFromString.contains(alignment)) {
            qCDebug(ScheduleDisplayLog) << "Alignment setting" << alignment << "for" << id
                                        << "is invalid - falling back to defaults";
            resetSettings();
            return;
        }
        label->setAlignment(s_alignmentFromString.value(alignment));

        // Font family
        if (version > 1) {
            const auto fontFamilyValue = settings.value(s_fontFamily);
            if (fontFamilyValue.isUndefined()) {
                qCDebug(ScheduleDisplayLog) << "No font family setting found for" << id
                                            << "- falling back to defaults";
                resetSettings();
                return;
            }
            label->setFontFamily(fontFamilyValue.toString());
        }

        // Bold
        const auto boldValue = settings.value(s_bold);
        if (boldValue.isUndefined()) {
            qCDebug(ScheduleDisplayLog) << "No bold setting found for" << id
                                        << "- falling back to defaults";
            resetSettings();
            return;
        }
        label->setBold(boldValue.toBool());

        // Max height
        const auto maxHeightValue = settings.value(s_maxHeight);
        if (maxHeightValue.isUndefined()) {
            qCDebug(ScheduleDisplayLog) << "No max height setting found for" << id
                                        << "- falling back to defaults";
            resetSettings();
            return;
        }
        label->setMaxHeight(maxHeightValue.toInt());

        // Stretch
        const auto stretchValue = settings.value(s_stretch);
        if (stretchValue.isUndefined()) {
            qCDebug(ScheduleDisplayLog) << "No stretch setting found for" << id
                                        << "- falling back to defaults";
            resetSettings();
            return;
        }
        label->setStretch(stretchValue.toInt());
    }

    // Logo settings
    if (version > 1) {
        const auto settingsValue = labelSettings.value(s_logo);
        if (settingsValue.isUndefined()) {
            qCDebug(ScheduleDisplayLog) << "Label settings for the logo"
                                        << "not found - falling back to defaults";
            resetSettings();
            return;
        }

        const auto settings = settingsValue.toObject();

        // Max height
        const auto maxHeightValue = settings.value(s_maxHeight);
        if (maxHeightValue.isUndefined()) {
            qCDebug(ScheduleDisplayLog) << "No max height setting found for the logo"
                                        << "- falling back to defaults";
            resetSettings();
            return;
        }
        m_logo->setMaxHeight(maxHeightValue.toInt());

        // Stretch
        const auto stretchValue = settings.value(s_stretch);
        if (stretchValue.isUndefined()) {
            qCDebug(ScheduleDisplayLog) << "No stretch setting found for the logo"
                                        << "- falling back to defaults";
            resetSettings();
            return;
        }
        m_logo->setStretch(stretchValue.toInt());

    } else {
        m_logo->setMaxHeight(70);
        m_logo->setStretch(2);
    }

    // Images
    const auto imagesObject = m_savedSettings.value(s_imagesSettings).toObject();
    const auto welcomeImage = imagesObject.value(s_welcome).toString();
    const auto scheduleImage = imagesObject.value(s_schedule).toString();
    const auto goodByeImage = imagesObject.value(s_goodBye).toString();
    if (! welcomeImage.isEmpty()) {
        loadImage(Page::Welcome, welcomeImage);
    }
    if (! scheduleImage.isEmpty()) {
        loadImage(Page::Schedule, scheduleImage);
    }
    if (! goodByeImage.isEmpty()) {
        loadImage(Page::GoodBye, goodByeImage);
    }

    qCDebug(ScheduleDisplayLog) << "Successfully loaded the saved display settings";
}

void ScheduleDisplay::setScheduleStarted(bool state)
{
    m_scheduleStarted = state;
}

void ScheduleDisplay::showPage(Page page)
{
    m_layout->setCurrentIndex(static_cast<int>(page));
}

void ScheduleDisplay::loadImage(ScheduleDisplay::Page page, const QString &file)
{
    QString fileToLoad;
    if (! file.isEmpty()) {
        fileToLoad = file;
    } else {
        const auto selected = QFileDialog::getOpenFileName(this,
            tr("Bitte ein Bild auswählen"), m_dbPath, tr("Bilder (*.png *.jpg)"));
        if (selected.isEmpty()) {
            return;
        }
        fileToLoad = selected;
    }

    if (! m_pagesMap[page]->setBackgroundImage(fileToLoad)) {
        QMessageBox::warning(this, tr("Hintergrund setzen"),
            tr("<p>Das Laden der Bilddatei <kbd>%1</kbd> ist fehlgeschlagen!</p>"
               "<p>Bitte die Zugriffsrechte und ggf. das Bild selbst überprüfen!</p>").arg(
               QDir::toNativeSeparators(fileToLoad).toHtmlEscaped()));
        return;
    }

    showPage(page);
}

void ScheduleDisplay::removeImage(ScheduleDisplay::Page page)
{
    m_pagesMap[page]->removeBackgroundImage();
    showPage(Page::Schedule);
}


void ScheduleDisplay::startFontMenu()
{
    m_fontMenu->addAction(m_fontWidgetAction);
    connect(m_fontWidgetAction, &FontWidgetAction::familySelected, this,
        [this](const QString &family)
        {
            for (const auto &id : s_allLabelIds) {
                m_labels[id]->setFontFamily(family);
            }
        });

    QString commonFamily;
    for (const auto &id : s_allLabelIds) {
        if (commonFamily.isEmpty()) {
            commonFamily = m_labels[id]->fontFamily();
        } else {
            const auto family = m_labels[id]->fontFamily();
            if (family != commonFamily) {
                commonFamily = font().family();
                break;
            }
        }
    }

    if (m_fontWidgetAction->selectFamily(commonFamily)) {
        return;
    } else {
        m_fontWidgetAction->selectFamily(font().family());
    }
}

void ScheduleDisplay::finishFontMenu()
{
    m_fontWidgetAction->disconnect();
    m_fontMenu->removeAction(m_fontWidgetAction);
}
