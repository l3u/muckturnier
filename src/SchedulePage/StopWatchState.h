// SPDX-FileCopyrightText: 2020-2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef STOPWATCHSTATE_H
#define STOPWATCHSTATE_H

#include <QString>
#include <QByteArray>

namespace StopWatchState
{

constexpr int version = 1;

struct State
{
    int hours;
    int minutes;
    int seconds;
    QString title;
    QByteArray geometry;
};

const State defaultState {
    0,           // Hours
    0,           // Minutes
    0,           // Seconds
    QString(),   // Title
    QByteArray() // Geometry
};

}

#endif // STOPWATCHSTATE_H
