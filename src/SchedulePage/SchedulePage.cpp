// SPDX-FileCopyrightText: 2023-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "SchedulePage.h"
#include "StopWatchEngine.h"
#include "StopWatch.h"
#include "ScheduleSettingsWidget.h"
#include "ScheduleDisplay.h"
#include "ScheduleLabel.h"

#include "Database/Database.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/Settings.h"
#include "SharedObjects/TournamentSettings.h"
#include "SharedObjects/ResourceFinder.h"

#include "shared/Logging.h"
#include "shared/TableDelegate.h"
#include "shared/SharedStyles.h"
#include "shared/NumbersComboBox.h"
#include "shared/MinimumViewportWidget.h"
#include "shared/MinimumViewportScroll.h"
#include "shared/FullScreenMenu.h"
#include "shared/DisplayButton.h"

// Qt includes
#include <QTimer>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QApplication>
#include <QGroupBox>
#include <QStyle>
#include <QGridLayout>
#include <QLabel>
#include <QSpinBox>
#include <QTime>
#include <QDebug>
#include <QMessageBox>
#include <QTableWidget>
#include <QHeaderView>
#include <QFontMetrics>
#include <QMenu>
#include <QCheckBox>
#include <QScreen>

// C++ includes
#include <algorithm>
#include <cmath>

SchedulePage::SchedulePage(SharedObjects *sharedObjects, StopWatchEngine *stopWatchEngine,
                           QWidget *parent)
    : OptionalPage(parent),
      m_db(sharedObjects->database()),
      m_settings(sharedObjects->settings()),
      m_tournamentSettings(sharedObjects->tournamentSettings()),
      m_stopWatchEngine(stopWatchEngine),
      m_roundsState(&m_db->roundsState()),
      m_displayLogo(sharedObjects->resourceFinder()->find(QStringLiteral("display_logo.png")))
{
    connect(m_db, &Database::roundsStateChanged, this, &SchedulePage::roundsStateChanged);

    auto *topLayout = new QHBoxLayout;
    layout()->addLayout(topLayout);

    // Round

    // Close button
    auto *closeButton = new QPushButton(tr("Schließen"));
    closeButton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    closeButton->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogCloseButton));
    connect(closeButton, &QPushButton::clicked, this, &SchedulePage::closePage);
    topLayout->addStretch();
    topLayout->addWidget(closeButton);

    // Settings

    auto *settingsBox = new QGroupBox(tr("Einstellungen/Steuerung"));
    settingsBox->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
    auto *settingsBoxLayout = new QHBoxLayout(settingsBox);
    layout()->addWidget(settingsBox);

    m_settingsWidget = new ScheduleSettingsWidget;
    m_settingsWidget->adoptSettings(m_tournamentSettings->scheduleSettings());
    connect(m_settingsWidget, &ScheduleSettingsWidget::valuesChanged,
            this, [this]
            {
                m_tournamentSettings->setScheduleSettings(m_settingsWidget->settings());
            });
    settingsBoxLayout->addWidget(m_settingsWidget);

    m_preview = new QPushButton(tr("Zeitplan-Vorschau"));
    connect(m_preview, &QPushButton::clicked,
            this, [this]
            {
                cacheSecs();
                generateSchedule();
                m_scheduleGenerated = false;
            });
    settingsBoxLayout->addWidget(m_preview);

    // Control

    settingsBoxLayout->addStretch();

    auto *controlLayout = new QVBoxLayout;
    settingsBoxLayout->addLayout(controlLayout);

    auto *startStopLayout = new QHBoxLayout;
    controlLayout->addLayout(startStopLayout);

    startStopLayout->addWidget(new QLabel(tr("Runde")));
    m_round = new NumbersComboBox;
    startStopLayout->addWidget(m_round);

    m_startRound = new QPushButton(tr("Starten"));
    connect(m_round, &NumbersComboBox::currentNumberChanged,
            this, [this](int round)
            {
                m_startRound->setEnabled(! m_roundsState->value(round) && m_db->isChangeable());
            });
    connect(m_startRound, &QPushButton::clicked, this, &SchedulePage::startRound);
    startStopLayout->addWidget(m_startRound);

    roundsStateChanged();

    m_stopRound = new QPushButton(tr("Beenden"));
    connect(m_stopRound, &QPushButton::clicked, this, &SchedulePage::stopRound);
    m_stopRound->hide();
    startStopLayout->addWidget(m_stopRound);

    m_extras = new QPushButton(tr("…"));
    startStopLayout->addWidget(m_extras);

    auto *extrasMenu = new QMenu(this);

    auto *resetFromRound = extrasMenu->addAction(tr("Zeiten ab Runde zurücksetzen"));
    connect(resetFromRound, &QAction::triggered, this, &SchedulePage::resetFromRound);

    extrasMenu->addSeparator();

    auto *resetAll = extrasMenu->addAction(tr("Alle Zeiten zurücksetzen"));
    connect(resetAll, &QAction::triggered, this, &SchedulePage::askResetAll);

    m_extras->setMenu(extrasMenu);

    m_startStopWatch = new QCheckBox(tr("Zusätzliche Stoppuhr automatisch starten"));
    m_startStopWatch->setChecked(m_settings->scheduleStartStopWatch());
    connect(m_startStopWatch, &QCheckBox::toggled,
            m_settings, &Settings::saveScheduleStartStopWatch);
    controlLayout->addWidget(m_startStopWatch);

    // Status

    auto *statusBox = new QGroupBox(tr("Status"));
    auto *statusWrapperLayout = new QHBoxLayout(statusBox);
    layout()->addWidget(statusBox);

    auto *currentTimesBox = new QGroupBox(tr("Aktuelle Zeiten"));
    currentTimesBox->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Ignored);
    currentTimesBox->setStyleSheet(SharedStyles::normalGroupBoxTitle);
    auto *currentTimesBoxLayout = new QVBoxLayout(currentTimesBox);

    auto *currentTimesScroll = new MinimumViewportScroll(Qt::Horizontal);
    currentTimesScroll->setWidgetResizable(true);
    currentTimesScroll->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    currentTimesBoxLayout->addWidget(currentTimesScroll);
    statusWrapperLayout->addWidget(currentTimesBox);

    auto *currentTimesWidget = new MinimumViewportWidget;
    currentTimesWidget->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Ignored);
    auto *currentTimesWidgetLayout = new QVBoxLayout(currentTimesWidget);

    // Using another widget inside the MinimumViewportWidget is a bit more complicated, but it looks
    // nicer in this case concerning spacing. So we add another wrapper to the wrapper wrapper ;-)
    auto *spaceWrapper = new QWidget;
    currentTimesWidgetLayout->addWidget(spaceWrapper);

    auto *currentTimesWrapperLayout = new QVBoxLayout(spaceWrapper);
    auto *currentTimesLayout = new QGridLayout;
    currentTimesWrapperLayout->addLayout(currentTimesLayout);

    m_currentTime = QDateTime::currentDateTime();

    int row = -1;

    currentTimesLayout->addWidget(new QLabel(tr("Uhrzeit:")), ++row, 0);
    auto *clock = new ScheduleLabel(currentTime());

    // Reserve enough space for the clock label
    const QFontMetrics fontMetrics(font());
    clock->setMinimumWidth(fontMetrics.boundingRect(QStringLiteral("- 88 m 88 s ")).width());

    connect(this, &SchedulePage::updateClock, clock, &ScheduleLabel::setText);
    currentTimesLayout->addWidget(clock, row, 1);

    currentTimesLayout->addWidget(new QLabel(QStringLiteral(" ")), ++row, 0);

    currentTimesLayout->addWidget(new QLabel(tr("Laufende Runde:")), ++row, 0);
    auto *currentRound = new ScheduleLabel;
    connect(this, &SchedulePage::updateCurrentRound, currentRound, &ScheduleLabel::setText);
    currentTimesLayout->addWidget(currentRound, row, 1);

    currentTimesLayout->addWidget(new QLabel(tr("Start der Runde:")), ++row, 0);
    auto *roundStart = new ScheduleLabel;
    connect(this, &SchedulePage::updateRoundStart, roundStart, &ScheduleLabel::setText);
    currentTimesLayout->addWidget(roundStart, row, 1);

    currentTimesLayout->addWidget(new QLabel(tr("Spielzeit bisher:")), ++row, 0);
    auto *roundTimeLapsed = new ScheduleLabel;
    connect(this, &SchedulePage::updateRoundTimeLapsed, roundTimeLapsed, &ScheduleLabel::setText);
    currentTimesLayout->addWidget(roundTimeLapsed, row, 1);

    currentTimesLayout->addWidget(new QLabel(tr("Verbleibende Zeit:")), ++row, 0);
    auto *roundTimeLeft = new ScheduleLabel;
    roundTimeLeft->setTimeMode(true);
    connect(this, &SchedulePage::updateRoundTimeLeft, roundTimeLeft, &ScheduleLabel::setText);
    currentTimesLayout->addWidget(roundTimeLeft, row, 1);

    currentTimesLayout->addWidget(new QLabel(tr("Strafpunkte bisher:")), ++row, 0);
    auto *penalty = new ScheduleLabel;
    penalty->setWordWrap(true);
    connect(this, &SchedulePage::updatePenalty, penalty, &ScheduleLabel::setText);
    currentTimesLayout->addWidget(penalty, row, 1);

    currentTimesLayout->addWidget(new QLabel(QStringLiteral(" ")), ++row, 0);

    currentTimesLayout->addWidget(new QLabel(tr("Nächste Runde:")), ++row, 0);
    auto *nextRound = new ScheduleLabel;
    connect(this, &SchedulePage::updateNextRound, nextRound, &ScheduleLabel::setText);
    currentTimesLayout->addWidget(nextRound, row, 1);

    currentTimesLayout->addWidget(new QLabel(tr("Start der nächsten Runde:")), ++row, 0);
    auto *nextRoundStart = new ScheduleLabel;
    connect(this, &SchedulePage::updateNextRoundStart, nextRoundStart, &ScheduleLabel::setText);
    currentTimesLayout->addWidget(nextRoundStart, row, 1);

    currentTimesLayout->addWidget(new QLabel(tr("Zeit bis zur nächsten Runde:")), ++row, 0);
    auto *timeToNextRound = new ScheduleLabel;
    timeToNextRound->setTimeMode(true);
    connect(this, &SchedulePage::updateTimeToNextRound, timeToNextRound, &ScheduleLabel::setText);
    currentTimesLayout->addWidget(timeToNextRound, row, 1);

    currentTimesLayout->addWidget(new QLabel(QStringLiteral(" ")), ++row, 0);

    currentTimesWrapperLayout->addStretch();

    auto *remoteMenu = new QMenu(this);

    m_fullScreenMenu = new FullScreenMenu(this);
    remoteMenu->addMenu(m_fullScreenMenu);

    auto *showDisplayHereAction = remoteMenu->addAction(tr("Display hier anzeigen"));
    connect(showDisplayHereAction, &QAction::triggered,
            this, [this]
            {
                m_scheduleDisplay->initializeGeometry(screen());
            });

    remoteMenu->addSeparator();

    auto *pageMenu = remoteMenu->addMenu(tr("Angezeigte Seite"));
    auto *welcomePage = pageMenu->addAction(tr("Willkommen-Seite"));
    connect(welcomePage, &QAction::triggered,
            this, [this]
            {
                m_scheduleDisplay->showPage(ScheduleDisplay::Welcome);
            });
    auto *schedulePage = pageMenu->addAction(tr("Zeitanzeige"));
    connect(schedulePage, &QAction::triggered,
            this, [this]
            {
                m_scheduleDisplay->showPage(ScheduleDisplay::Schedule);
            });
    auto *goodByePage = pageMenu->addAction(tr("Auf-Wiedersehen-Seite"));
    connect(goodByePage, &QAction::triggered,
            this, [this]
            {
                m_scheduleDisplay->showPage(ScheduleDisplay::GoodBye);
            });

    remoteMenu->addSeparator();

    auto *closeDisplay = remoteMenu->addAction(tr("Display schließen"));
    connect(closeDisplay, &QAction::triggered, this, &SchedulePage::closeDisplay);

    m_displayButton = new DisplayButton(tr("Zeitplan-Display"), remoteMenu);
    connect(m_displayButton, &DisplayButton::displayRequested,
            this, &SchedulePage::showScheduleDisplay);
    currentTimesWrapperLayout->addWidget(m_displayButton);

    currentTimesScroll->setMaximumWidth(currentTimesWidget->sizeHint().width());
    currentTimesScroll->setWidget(currentTimesWidget);

    // Schedule

    auto *scheduleBox = new QGroupBox(tr("Zeitplan"));
    scheduleBox->setStyleSheet(SharedStyles::normalGroupBoxTitle);
    auto *scheduleLayout = new QVBoxLayout(scheduleBox);
    statusWrapperLayout->addWidget(scheduleBox);

    m_schedule = new QTableWidget;
    m_schedule->setSortingEnabled(false);
    m_schedule->setSelectionMode(QTableWidget::NoSelection);
    m_schedule->setFocusPolicy(Qt::NoFocus);
    m_schedule->setContextMenuPolicy(Qt::NoContextMenu);
    m_schedule->setColumnCount(4);
    m_schedule->setHorizontalHeaderLabels({ QString(),
                                            tr("Start soll"),
                                            tr("Start ist"),
                                            tr("Abweichung") });
    m_schedule->verticalHeader()->hide();
    m_schedule->setShowGrid(false);
    m_schedule->setItemDelegate(new TableDelegate(this));

    scheduleLayout->addWidget(m_schedule);

    reload();
}

void SchedulePage::roundsStateChanged()
{
    auto selectedRound = m_round->currentNumber();
    m_round->blockSignals(true);
    m_round->clear();

    for (auto it = m_roundsState->constBegin(); it != m_roundsState->constEnd(); it++) {
        m_round->addItem(tr("Runde %1 (%2)").arg(QString::number(it.key()),
                                                 it.value() ? tr("abgeschlossen") : tr("offen")),
                         it.key());
    }

    if (m_db->isChangeable()) {
        const auto newRound = m_roundsState->lastKey() + 1;
        m_round->addItem(tr("Runde %1 (neue Runde)").arg(newRound), newRound);
    }

    if (m_currentRound == 0 || ! m_round->setCurrentNumber(selectedRound)) {
        m_round->setCurrentNumber(getFirstOpenRound());
    }

    selectedRound = m_round->currentNumber();
    if (m_roundsState->contains(selectedRound)) {
        m_startRound->setEnabled(! m_roundsState->value(selectedRound) && m_db->isChangeable());
    }

    m_round->blockSignals(false);

    if (m_currentRound > 0 && m_roundsState->value(m_currentRound)) {
        QTimer::singleShot(0, this, [this]
        {
            if (QMessageBox::question(this, dockTitle(),
                tr("<p>Es wurde gerade das letzte Ergebnis aus Runde %1 eingegeben.</p>"
                   "<p>Soll die Zeitmessung für die Runde jetzt gestoppt werden?</p>").arg(
                   m_currentRound),
                QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes) == QMessageBox::Yes) {

                qobject_cast<QWidget *>(parent())->raise();
                stopRound();
            }
        });
    }
}

void SchedulePage::reload()
{
    adjustTimer();

    m_settingsWidget->adoptSettings(m_tournamentSettings->scheduleSettings());

    const auto isChangeable = m_db->isChangeable();
    m_settingsWidget->setEnabled(isChangeable);
    m_preview->setEnabled(isChangeable);
    m_round->setEnabled(isChangeable);
    m_startRound->setEnabled(isChangeable);
    m_stopRound->setEnabled(isChangeable);
    m_extras->setEnabled(isChangeable);
    m_startStopWatch->setEnabled(isChangeable);

    if (! isChangeable) {
        Q_EMIT updateNextRound();
        Q_EMIT updateNextRoundStart();
        Q_EMIT updateTimeToNextRound();
    }

    roundsStateChanged();

    const auto [ scheduleData, success ] = m_db->getScheduleData();
    if (! success) {
        return;
    }

    m_roundsTimes.clear();
    int processedRound = -1;
    bool roundIsOpen = false;

    QVector<QString>::const_iterator it;
    for (it = scheduleData.begin(); it != scheduleData.end(); it++) {
        const auto round = (*it).toInt();
        const auto start = *++it;
        const auto end = *++it;

        if (round == 0) {
            // The special "round 0" contains the start round and time of the schedule
            m_dbScheduleStartRound = start.toInt();
            m_dbScheduleStartTime = m_db->timeFromString(end);
        } else {
            m_roundsTimes[round].start = m_db->timeFromString(start);
            m_roundsTimes[round].end = end.isEmpty() ? QDateTime() : m_db->timeFromString(end);
            processedRound = round;
            roundIsOpen = end.isEmpty();
        }
    }

    if (! m_roundsTimes.isEmpty()) {
        cacheSecs();
        m_settingsWidget->setChangeable(false);
        m_preview->hide();
        generateSchedule();

        if (roundIsOpen) {
            QTimer::singleShot(0, this, [this, processedRound]
            {
                if (m_checkStartRoundOngoing) {
                    // We have to do this, because we can't prevent this being called twice when
                    // the program ist started and a database is loaded using a commmand line
                    // parameter.
                    // This prevents doubled message boxes -- we only want to ask once.
                    return;
                }

                m_checkStartRoundOngoing = true;
                bool startRound = true;

                const auto secsLapsed =
                    m_roundsTimes.value(processedRound).start.secsTo(QDateTime::currentDateTime());

                if (secsLapsed > m_roundSecs) {
                    raiseMe();
                    startRound = QMessageBox::question(this, tr("Turnier-Zeitplan"),
                        tr("<p>Runde %1 läuft gerade, ist aber laut Zeitplan bereits seit %2 "
                           "vorbei.</p>"
                           "<p>Soll die Runde weiterlaufen?</p>").arg(
                           QString::number(processedRound), formatTime(secsLapsed - m_roundSecs)),
                        QMessageBox::Yes | QMessageBox::No,
                        QMessageBox::No) == QMessageBox::Yes;
                }

                if (startRound) {
                    m_currentRound = processedRound;

                    if (processedRound < m_scheduledRounds) {
                        Q_EMIT updateNextRound(processedRound + 1);
                        m_nextRoundStart = m_roundsTimes[processedRound].start.addSecs(
                            m_roundSecs + m_breakSecs);
                        Q_EMIT updateNextRoundStart(formatTime(m_nextRoundStart));
                    } else {
                        Q_EMIT updateNextRound();
                        m_nextRoundStart = QDateTime();
                        Q_EMIT updateNextRoundStart();
                    }

                    Q_EMIT updateCurrentRound(processedRound);
                    Q_EMIT updateRoundStart(formatTime(m_roundsTimes[processedRound].start));

                    m_round->setEnabled(false);
                    m_round->setCurrentIndex(m_round->findData(processedRound));

                    m_startRound->hide();
                    m_stopRound->show();
                    m_startStopWatch->setEnabled(false);

                    if (m_startStopWatch->isChecked() && ! m_triggerStopWatchByReload) {
                        m_triggerStopWatchByReload = true;
                        prepareStopWatch();
                    }

                } else {
                    m_currentRound = 0;
                    m_roundsTimes[processedRound].start = QDateTime();

                    if (processedRound == 1) {
                        resetAll();
                    }
                }
            });

        } else {
            if (processedRound < m_scheduledRounds) {
                Q_EMIT updateNextRound(processedRound + 1);
                m_nextRoundStart = m_roundsTimes[processedRound].end.addSecs(m_breakSecs);
                Q_EMIT updateNextRoundStart(formatTime(m_nextRoundStart));
            } else {
                Q_EMIT updateNextRound();
                Q_EMIT updateNextRoundStart();
            }
        }
    }
}

QString SchedulePage::currentTime() const
{
    return m_currentTime.toString(tr("hh:mm:ss"));
}

bool SchedulePage::closePage()
{
    if (m_scheduleDisplay != nullptr
        && QMessageBox::warning(this, tr("Seite schließen"),
               tr("<p>Es wird momentan das Zeitplan-Display angezeigt. Wenn diese Seite "
                  "geschlossen wird, dann wird auch das Display geschlossen.</p>"
                  "<p>Sollen wirklich das Display und diese Seite geschlossen werden?</p>"),
               QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Cancel)
           == QMessageBox::Cancel) {

        return false;
    }

    qCDebug(MuckturnierLog) << "Stopping the timer";
    if (m_timerId != -1) {
        killTimer(m_timerId);
    }
    return OptionalPage::closePage();
}

void SchedulePage::closeDisplay()
{
    if (m_scheduleDisplay != nullptr) {
        m_scheduleDisplay->close();
        QApplication::processEvents();
    }
}

void SchedulePage::adjustTimer()
{
    if (m_adjustOngoing) {
        return;
    }

    m_adjustOngoing = true;

    if (m_timerId != -1) {
        killTimer(m_timerId);
    }

    const auto now = QTime::currentTime();
    const auto offset = 1000 - now.msec() + 200;
    qCDebug(MuckturnierLog) << "Adjusting the timer by" << offset << "msec";
    QTimer::singleShot(offset, this, [this]
    {
        qCDebug(MuckturnierLog) << "(Re-)Starting the timer";
        m_timerId = startTimer(1000, Qt::PreciseTimer);
        m_adjustOngoing = false;
    });
}

void SchedulePage::timerEvent(QTimerEvent *)
{
    m_currentTime = QDateTime::currentDateTime();
    const auto msec = m_currentTime.time().msec();
    if (msec < 100 || msec > 300) {
        adjustTimer();
    } else if (m_triggerStartStopWatch && m_stopWatch != nullptr) {
        m_stopWatch->setScheduleMode(
            tr("Runde %1").arg(m_currentRound),
            m_roundsTimes.value(m_currentRound).start.addSecs(m_roundSecs));
        m_stopWatch->start(-1);
        m_triggerStartStopWatch = false;
        m_triggerStopWatchByReload = false;
    }

    Q_EMIT updateClock(currentTime());

    if (m_currentRound != 0) {
        const auto secsLapsed = m_roundsTimes.value(m_currentRound).start.secsTo(m_currentTime);
        Q_EMIT updateRoundTimeLapsed(formatTime(secsLapsed));
        m_roundTimeLeft = m_roundSecs - secsLapsed;
        Q_EMIT updateRoundTimeLeft(formatTime(m_roundTimeLeft));

        if (m_currentRound < m_scheduledRounds) {
            Q_EMIT updateTimeToNextRound(formatTime(m_roundTimeLeft + m_breakSecs));
        }

        QString penalty;
        const auto lapsedRounds = std::min(int(std::floor(secsLapsed / m_boogerSecs)),
                                           m_tournamentSettings->boogersPerRound());
        for (int i = 0; i < lapsedRounds; i++) {
            if (! penalty.isEmpty()) {
                penalty.append(tr(" + "));
            }
            penalty.append(QString::number(m_tournamentSettings->boogerScore()));
        }

        if (lapsedRounds < m_tournamentSettings->boogersPerRound()) {
            const auto currentBoogerSecs = secsLapsed - m_boogerSecs * lapsedRounds;
            const auto currentPenalty = std::min(
                int(std::round(m_tournamentSettings->boogerScore()
                               / m_boogerSecs * currentBoogerSecs)),
                m_tournamentSettings->boogerScore());

            if (currentPenalty > 0) {
                if (! penalty.isEmpty()) {
                    penalty.append(tr(" + "));
                }
                penalty.append(QString::number(currentPenalty));
            }
        }

        if (! penalty.isEmpty()) {
            Q_EMIT updatePenalty(penalty);
        }

    } else {
        if (m_nextRoundStart.isValid() && m_db->isChangeable()) {
            Q_EMIT updateTimeToNextRound(
                formatTime(std::round(m_currentTime.msecsTo(m_nextRoundStart) / 1000.0)));
        }
    }

    if (m_stopWatch != nullptr) {
        m_stopWatch->update();
    }
}

int SchedulePage::getFirstOpenRound() const
{
    for (auto it = (*m_roundsState).constBegin(); it != (*m_roundsState).constEnd(); it++) {
        if (! it.value()) {
            return it.key();
        }
    }

    return m_roundsState->count() + 1;
}

void SchedulePage::cacheSecs()
{
    m_roundSecs = m_settingsWidget->roundDuration() * 60;
    m_boogerSecs = double(m_roundSecs) / m_tournamentSettings->boogersPerRound();
    m_breakSecs = m_settingsWidget->breakDuration() * 60;
}

void SchedulePage::startRound()
{
    const auto round = m_round->currentNumber();
    const auto firstOpen = getFirstOpenRound();

    const auto measuredRounds = m_roundsTimes.keys();
    for (const auto measuredRound : measuredRounds) {
        if (measuredRound > round) {
            QMessageBox::warning(this, tr("Runde starten"),
                                 tr("<p>Für Runde %1 kann keine Zeitmessung gestartet werden:</p>"
                                    "<p>Es wurde bereits eine nachfolgende Runde gemessen!"
                                    "</p>").arg(round));
            return;
        }
    }

    if (round > firstOpen
        && QMessageBox::warning(this, tr("Runde starten"),
               tr("<p>Die momentan erste offene Runde ist Runde %1.</p>"
                  "<p>Soll wirklich Runde %2 gestartet werden?</p>").arg(
                  QString::number(firstOpen), QString::number(round)),
               QMessageBox::Yes | QMessageBox::No,
               QMessageBox::No) == QMessageBox::No) {

        return;
    }

    bool roundRestarted = false;

    if (m_roundsTimes.contains(round)) {
        if (QMessageBox::warning(this, tr("Runde starten"),
                tr("<p>Für Runde %1 wurde bereits eine Zeitmessung durchgeführt.</p>"
                   "<p>Soll wirklich Runde %1 nochmals gestartet werden?</p>").arg(round),
                QMessageBox::Yes | QMessageBox::No,
                QMessageBox::No) == QMessageBox::No) {

            return;
        }

        roundRestarted = true;
        m_roundsTimes[round].end = QDateTime();
        if (m_settingsWidget->breakDuration() > 0) {
            const int row = (round - 1) * 2 + 1;
            m_schedule->setItem(row, 2, new QTableWidgetItem());
            m_schedule->setItem(row, 3, new QTableWidgetItem());
        }
    }

    if ((m_currentTime < m_nextRoundStart) && (! roundRestarted)
        && QMessageBox::warning(this, tr("Runde starten"),
               tr("<p>Der Start der nächsten Runde ist für %1 Uhr geplant. Bis dahin fehlen noch "
                  "%2 Minuten.</p>"
                  "<p>Soll die Runde trotzdem schon gestartet werden?</p>").arg(
                  m_nextRoundStart.time().toString(tr("hh.mm")),
                  QString::number(m_currentTime.secsTo(m_nextRoundStart) / 60)),
               QMessageBox::Yes | QMessageBox::No,
               QMessageBox::No) == QMessageBox::No) {

        return;
    }

    m_round->setEnabled(false);
    m_startRound->hide();
    m_stopRound->show();
    m_startStopWatch->setEnabled(false);

    if (m_settingsWidget->isChangeable()) {
        cacheSecs();
        m_settingsWidget->setChangeable(false);
        m_preview->hide();
    }

    m_roundsTimes[round].start = m_currentTime;
    m_currentRound = round;

    if (! m_db->saveScheduleTimes(round, m_db->timeToString(m_currentTime), QString())) {
        return;
    }

    if (round > m_scheduledRounds) {
        m_scheduleGenerated = false;
    }

    if (! m_scheduleGenerated) {
        generateSchedule();
    } else {
        displayTimes();
    }

    if (m_scheduleDisplay != nullptr) {
        m_scheduleDisplay->setScheduleStarted(true);
    }

    if (round < m_scheduledRounds) {
        Q_EMIT updateNextRound(round + 1);
        m_nextRoundStart = m_currentTime.addSecs(m_roundSecs + m_breakSecs);
        Q_EMIT updateNextRoundStart(formatTime(m_nextRoundStart));
        Q_EMIT updateTimeToNextRound(formatTime(m_currentTime.secsTo(m_nextRoundStart)));
    } else {
        Q_EMIT updateNextRound();
        Q_EMIT updateNextRoundStart();
        Q_EMIT updateTimeToNextRound();
        m_nextRoundStart = QDateTime();
    }

    Q_EMIT updateCurrentRound(round);
    Q_EMIT updateRoundStart(formatTime(m_currentTime));
    Q_EMIT updateRoundTimeLapsed(formatTime(0));
    Q_EMIT updateRoundTimeLeft(formatTime(m_roundSecs));

    if (m_startStopWatch->isChecked()) {
        prepareStopWatch();
    }
}

void SchedulePage::prepareStopWatch()
{
    m_stopWatch = m_stopWatchEngine->newStopWatch(true);
    m_stopWatch->setStartingMode();
    m_triggerStartStopWatch = true;
}

void SchedulePage::stopRound(bool force)
{
    const auto round = m_round->currentNumber();
    if (! force && (
        (m_roundsState->contains(round) && ! m_roundsState->value(round))
        || ! m_roundsState->contains(round))) {

        if (QMessageBox::warning(this, tr("Runde beenden"),
                tr("<p>Für Runde %1 wurden noch nicht alle Ergebnisse eingegeben!</p>"
                   "<p>Soll die Runde wirklich beendet (also abgebrochen) werden?</p>").arg(round),
                QMessageBox::Yes | QMessageBox::No, QMessageBox::No)
            == QMessageBox::No) {

            return;
        }
    }

    if (! m_db->saveScheduleTimes(m_currentRound,
                                  m_db->timeToString(m_roundsTimes[round].start),
                                  m_db->timeToString(m_currentTime))) {
        return;
    }

    m_roundsTimes[round].end = m_currentTime;

    if (m_currentRound >= m_scheduledRounds) {
        Q_EMIT updateNextRound();
        Q_EMIT updateNextRoundStart();
        Q_EMIT updateTimeToNextRound();
    }

    m_currentRound = 0;

    m_round->setEnabled(true);
    m_round->setCurrentNumber(getFirstOpenRound());
    m_startStopWatch->setEnabled(true);

    m_startRound->show();
    m_stopRound->hide();

    Q_EMIT updateCurrentRound();
    Q_EMIT updateRoundStart();
    Q_EMIT updateRoundTimeLapsed();
    m_roundTimeLeft = 0;
    Q_EMIT updateRoundTimeLeft();
    Q_EMIT updatePenalty();

    displayTimes();

    if (m_stopWatch != nullptr) {
        m_stopWatch->stop();
        m_stopWatch->close();
    }
}

QString SchedulePage::formatTime(const QDateTime &time) const
{
    return time.time().toString(tr("hh:mm"));
}

QString SchedulePage::formatTime(int secs) const
{
    const auto m = std::abs(secs / 60);
    const auto s = std::abs(secs % 60);

    QString time;
    if (m > 0) {
        time = tr("%1 m ").arg(QString::number(m));
    }
    time.append(tr("%2 s").arg(QString::number(s)));

    if (secs < 0 ) {
        time.prepend(tr("- "));
    }

    return time;
}

QString SchedulePage::formatDeviation(int secs) const
{
    const auto minutes = std::round(secs / 60.0);
    if (minutes == 0) {
        return tr("-");
    } else {
        return tr("%2%3").arg(minutes > 0 ? tr("+") : tr("-"),
                              tr("%n Minute(n)", "", std::abs(minutes)));
    }
}

void SchedulePage::generateSchedule()
{
    auto rounds = std::max({ m_settingsWidget->plannedRounds(),
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
                             m_roundsState->count(),
#else
                             int(m_roundsState->count()),
#endif
                             (m_roundsTimes.isEmpty() ? 0 : m_roundsTimes.lastKey()) });
    const auto rows = rounds * (m_breakSecs != 0 ? 2 : 1) + (m_breakSecs == 0 ? 1 : 0);

    if (m_scheduleStartRound == 0) {
        if (m_dbScheduleStartRound != 0) {
            m_scheduleStartRound = m_dbScheduleStartRound;
        } else {
            m_scheduleStartRound = getFirstOpenRound();
        }
    }

    m_schedule->setRowCount(rows);
    m_scheduleTimes.clear();

    // Planned schedule

    QDateTime time;

    if (m_dbScheduleStartRound != 0) {
        time = m_dbScheduleStartTime;

    } else {
        if (! m_roundsTimes.isEmpty()) {
            time = m_roundsTimes.first().start;
        } else {
            time = m_currentTime;
        }

        // Save the special "round 0" entry to indicate the start round and time of the schedule
        if (! m_db->saveScheduleTimes(0,
                                      QString::number(m_scheduleStartRound),
                                      m_db->timeToString(time))) {
            return;
        }
    }

    for (int round = 1; round <= rounds; round++) {
        int row = (round - 1) * (m_breakSecs != 0 ? 2 : 1) + (m_breakSecs == 0 ? 1 : 0);
        const auto includeRound = round >= m_scheduleStartRound;

        m_schedule->setItem(row, 0, new QTableWidgetItem(tr("Runde %1").arg(round)));
        m_schedule->setItem(row, 1, new QTableWidgetItem(includeRound ? formatTime(time)
                                                                      : tr("–")));

        if (includeRound) {
            m_scheduleTimes[round].start = time;
            time = time.addSecs(m_roundSecs);
            m_scheduleTimes[round].end = time;
        }

        if (m_breakSecs != 0 && round < rounds) {
            row++;
            m_schedule->setItem(row, 0, new QTableWidgetItem(tr("Pause")));
            m_schedule->setItem(row, 1, new QTableWidgetItem(includeRound ? formatTime(time)
                                                                          : tr("–")));
            if (includeRound) {
                time = time.addSecs(m_breakSecs);
            }
        }
    }

    m_schedule->setItem(rows - 1, 0, new QTableWidgetItem(tr("Ende")));
    m_schedule->setItem(rows - 1, 1, new QTableWidgetItem(formatTime(time)));

    m_schedule->resizeColumnsToContents();

    m_scheduleGenerated = true;
    m_scheduledRounds = rounds;
    displayTimes();
}

void SchedulePage::displayTimes()
{
    for (auto it = m_roundsTimes.constBegin(); it != m_roundsTimes.constEnd(); it++) {
        const auto round = it.key();
        const auto &times = it.value();
        int row = (round - 1) * (m_breakSecs != 0 ? 2 : 1) + (m_breakSecs == 0 ? 1 : 0);

        m_schedule->setItem(row, 2, new QTableWidgetItem(formatTime(times.start)));
        m_schedule->setItem(row, 3, new QTableWidgetItem(formatDeviation(
            m_scheduleTimes.contains(round)
                ? m_scheduleTimes.value(round).start.secsTo(times.start)
                : 0)));

        if (m_breakSecs > 0 && times.end.isValid()) {
            row++;
            m_schedule->setItem(row, 2, new QTableWidgetItem(formatTime(times.end)));
            m_schedule->setItem(row, 3, new QTableWidgetItem(formatDeviation(
                m_scheduleTimes.value(round).end.secsTo(times.end))));
        }
    }

    m_schedule->resizeColumnsToContents();
}

void SchedulePage::checkStopRound()
{
    if (m_currentRound != 0) {
        const auto round = m_currentRound;
        stopRound(true);
        QMessageBox::information(this, dockTitle(),
            tr("Die Zeitmessung für die aktuell laufende Runde %1 wurde automatisch beendet.").arg(
               round));
    }
}

void SchedulePage::resetFromRound()
{
    const auto round = m_round->currentNumber();
    if (round == 1) {
        askResetAll();
        return;
    }

    auto text = tr("<p>Sollen wirklich alle Zeiten für alle Runden ab Runde %1 zurückgesetzt "
                   "werden?</p>").arg(round);
    if (getFirstOpenRound() > 1) {
        text.append(tr("<p>Bereits abgeschlossene Runden können nicht neu gestartet werden!</p>"));
    }

    if (QMessageBox::question(this, dockTitle(),
                              text, QMessageBox::Yes | QMessageBox::No, QMessageBox::No)
        == QMessageBox::No) {

        return;
    }

    if (m_currentRound != 0) {
        stopRound(true);
    }

    if (! m_db->resetScheduleTimes(round)) {
        return;
    }

    int row = 0;
    if (m_breakSecs == 0) {
        row = round - 1;
    } else {
        row = round * 2 - 2;
    }

    // This has to be cached here, otherwise m_roundsTimes can be empty in the following for loop,
    // causing an assertion (because when QMap::lastKey is used, the QMap must not be empty).
    int lastRound = 0;
    if (! m_roundsTimes.isEmpty()) {
        lastRound = m_roundsTimes.lastKey();
    }

    for (int i = round; i <= lastRound; i++) {
        m_roundsTimes.remove(i);
        m_schedule->setItem(row, 2, new QTableWidgetItem());
        m_schedule->setItem(row, 3, new QTableWidgetItem());
        if (m_breakSecs > 0) {
            row++;
            m_schedule->setItem(row, 2, new QTableWidgetItem());
            m_schedule->setItem(row, 3, new QTableWidgetItem());
        }
        row++;
    }

    m_currentRound = 0;
    m_roundTimeLeft = 0;

    Q_EMIT updateCurrentRound();
    Q_EMIT updateRoundStart();
    Q_EMIT updateRoundTimeLapsed();
    Q_EMIT updateRoundTimeLeft();
    Q_EMIT updatePenalty();
}

void SchedulePage::askResetAll()
{
    auto text = tr("<p>Sollen wirklich alle Zeiten für alle Runden zurückgesetzt werden?</p>");
    if (getFirstOpenRound() > 1) {
        text.append(tr("<p>Bereits abgeschlossene Runden können nicht neu gestartet werden!</p>"));
    }

    if (QMessageBox::question(this, dockTitle(),
                              text, QMessageBox::Yes | QMessageBox::No, QMessageBox::No)
        == QMessageBox::No) {

        return;

    }

    if (m_currentRound != 0) {
        stopRound(true);
    }

    resetAll();
}

void SchedulePage::resetAll()
{
    if (m_currentRound != 0) {
        stopRound(true);
    }

    if (! m_db->resetScheduleTimes(0)) {
        return;
    }

    reset();
}

void SchedulePage::reset()
{
    m_scheduleTimes.clear();
    m_roundsTimes.clear();
    m_schedule->setRowCount(0);
    m_nextRoundStart = QDateTime();
    m_scheduleGenerated = false;
    m_scheduleStartRound = 0;
    m_currentRound = 0;
    m_dbScheduleStartRound = 0;
    m_roundTimeLeft = 0;

    m_settingsWidget->setChangeable(true);
    m_preview->show();

    if (m_scheduleDisplay != nullptr) {
        m_scheduleDisplay->setScheduleStarted(false);
    }

    Q_EMIT updateNextRound();
    Q_EMIT updateNextRoundStart();
    Q_EMIT updateTimeToNextRound();

    Q_EMIT updateCurrentRound();
    Q_EMIT updateRoundStart();
    Q_EMIT updateRoundTimeLapsed();
    Q_EMIT updateRoundTimeLeft();
    Q_EMIT updatePenalty();
}

QString SchedulePage::dockTitle() const
{
    return qobject_cast<QWidget *>(parent())->windowTitle();
}

void SchedulePage::showScheduleDisplay()
{
    m_scheduleDisplay = new ScheduleDisplay(this, m_settings, m_db->dbPath(), m_displayLogo);
    connect(m_scheduleDisplay, &QDialog::finished, m_displayButton, &DisplayButton::displayClosed);

    m_scheduleDisplay->setScheduleStarted(! m_roundsTimes.isEmpty());

    connect(this, &SchedulePage::updateClock,
            m_scheduleDisplay, &ScheduleDisplay::updateClock);
    m_scheduleDisplay->updateClock(currentTime());

    connect(this, &SchedulePage::updateNextRound,
            m_scheduleDisplay, &ScheduleDisplay::updateNextRound);
    m_scheduleDisplay->updateNextRound(m_nextRoundStart.isValid() ? m_currentRound + 1 : 0);

    connect(this, &SchedulePage::updateCurrentRound,
            m_scheduleDisplay, &ScheduleDisplay::updateCurrentRound);
    m_scheduleDisplay->updateCurrentRound(m_currentRound);

    connect(this, &SchedulePage::updateRoundTimeLeft,
            m_scheduleDisplay, &ScheduleDisplay::updateRoundTimeLeft);
    m_scheduleDisplay->updateRoundTimeLeft(m_currentRound == 0
                                               ? QString() : formatTime(m_roundTimeLeft));

    connect(this, &SchedulePage::updateTimeToNextRound,
            m_scheduleDisplay, &ScheduleDisplay::updateTimeToNextRound);
    m_scheduleDisplay->updateTimeToNextRound(m_nextRoundStart.isValid()
                                             ? formatTime(m_currentTime.secsTo(m_nextRoundStart))
                                             : QString());

    connect(this, &SchedulePage::updateNextRoundStart,
            m_scheduleDisplay, &ScheduleDisplay::updateNextRoundStart);
    m_scheduleDisplay->updateNextRoundStart(m_nextRoundStart.isValid()
                                            ? formatTime(m_nextRoundStart) : QString());

    connect(m_fullScreenMenu, &FullScreenMenu::fullScreenRequested,
            m_scheduleDisplay, &ScheduleDisplay::showFullScreen);

    m_scheduleDisplay->show();
}
