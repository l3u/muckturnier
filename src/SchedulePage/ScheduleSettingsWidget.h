// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SCHEDULESETTINGSWIDGET_H
#define SCHEDULESETTINGSWIDGET_H

// Qt includes
#include <QWidget>

// Local classes
namespace Schedule
{
struct Settings;
}

// Qt classes
class QLabel;
class QSpinBox;

class ScheduleSettingsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ScheduleSettingsWidget(QWidget *parent = nullptr);
    void setChangeable(bool state);
    bool isChangeable() const;

    int plannedRounds() const;
    int roundDuration() const;
    int breakDuration() const;

    Schedule::Settings settings() const;
    void adoptSettings(const Schedule::Settings &settings);

Q_SIGNALS:
    void valuesChanged();

private: // Variables
    QSpinBox *m_roundDuration;
    QLabel *m_roundDurationValue;
    QSpinBox *m_breakDuration;
    QLabel *m_breakDurationValue;
    QSpinBox *m_plannedRounds;
    QLabel *m_plannedRoundsValue;

};

#endif // SCHEDULESETTINGSWIDGET_H
