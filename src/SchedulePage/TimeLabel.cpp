// SPDX-FileCopyrightText: 2017-2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "TimeLabel.h"

// Qt includes
#include <QDebug>

static const QString s_styleDefault = QStringLiteral("font-weight: bold; color:black;");
static const QString s_styleExpired = QStringLiteral("font-weight: bold; color:red;");
static const QLatin1Char s_adjustmentChar('0');
static const QLatin1Char s_separatorChar (':');

TimeLabel::TimeLabel(QWidget *parent) : ExpandingLabel(parent)
{
    setStyleSheet(s_styleDefault);
}

void TimeLabel::setSeconds(qint64 seconds)
{
    if (seconds < 0 && ! m_displayIsRed) {
        setStyleSheet(s_styleExpired);
        m_displayIsRed = true;
    }
    if (seconds > 0 && m_displayIsRed) {
        setStyleSheet(s_styleDefault);
        m_displayIsRed = false;
    }

    m_seconds = seconds;
    qint64 secondsLeft = seconds;
    qint64 displaySeconds = abs(secondsLeft % 60);
    secondsLeft /= 60;
    qint64 minutes = abs(secondsLeft % 60);
    secondsLeft /= 60;
    qint64 hours = abs(secondsLeft);

    setText((m_displayIsRed ? tr("-") : QString())
            +                   QString::number(hours         ).rightJustified(2, s_adjustmentChar)
            + s_separatorChar + QString::number(minutes       ).rightJustified(2, s_adjustmentChar)
            + s_separatorChar + QString::number(displaySeconds).rightJustified(2, s_adjustmentChar));
}

qint64 TimeLabel::seconds() const
{
    return m_seconds;
}
