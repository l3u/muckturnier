// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef IMAGEWIDGET_H
#define IMAGEWIDGET_H

// Qt includes
#include <QWidget>
#include <QImage>
#include <QPixmap>

// Qt classes
class QTimer;
class QLabel;

class ImageWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ImageWidget(const QString &message = QString(), QWidget *parent = nullptr);
    bool setBackgroundImage(const QString &path);
    const QString &backgroundImagePath() const;
    void removeBackgroundImage();
    void setBackgroundColor(const QColor &color);
    void removeBackgroundColor();

protected: // Functions
    void paintEvent(QPaintEvent *event) override;

private Q_SLOTS:
    void setSmoothImage();

private: // Variables
    QImage m_backgroundImage;
    QString m_backgroundImagePath;
    QPixmap m_scaledBackgroundImage;
    QTimer *m_smoothTimer;
    QColor m_backgroundColor;
    QBrush m_backgroundBrush;
    QLabel *m_message = nullptr;

};

#endif // IMAGEWIDGET_H
