// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "MuckturnierLabel.h"
#include "ImageWidget.h"

// Qt includes
#include <QHBoxLayout>
#include <QLabel>
#include <QSpinBox>

MuckturnierLabel::MuckturnierLabel(const QString &logo, QWidget *parent) : QWidget(parent)
{
    auto *layout = new QHBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);

    m_formatting = new QWidget;
    m_formatting->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Preferred);
    auto *formatLayout = new QHBoxLayout(m_formatting);
    formatLayout->setContentsMargins(0, 0, 0, 0);

    auto *maxHeightLabel = new QLabel(tr("Max. H"));
    maxHeightLabel->setToolTip(tr("Maximale Höhe in Pixeln"));
    formatLayout->addWidget(maxHeightLabel);

    m_maxHeight = new QSpinBox;
    m_maxHeight->setToolTip(tr("Maximale Höhe in Pixeln"));
    m_maxHeight->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    m_maxHeight->setMinimum(20);
    m_maxHeight->setValue(70);
    m_maxHeight->setMaximum(999);
    connect(m_maxHeight, &QSpinBox::valueChanged, this, &MuckturnierLabel::setMaxHeight);
    formatLayout->addWidget(m_maxHeight);

    auto *stretchLabel = new QLabel(tr("H rel."));
    stretchLabel->setToolTip(tr("Relative Höhe im Vergleich zu den anderen Labeln"));
    formatLayout->addWidget(stretchLabel);

    m_stretch = new QSpinBox;
    m_stretch->setToolTip(tr("Relative Höhe im Vergleich zu den anderen Labeln"));
    m_stretch->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    m_stretch->setMinimum(0);
    m_stretch->setMaximum(999);
    connect(m_stretch, &QSpinBox::valueChanged, this, &MuckturnierLabel::stretchFactorChanged);
    formatLayout->addWidget(m_stretch);

    m_formatting->hide();
    layout->addWidget(m_formatting);

    m_label = new ImageWidget(tr("<i>Muckturnier.org</i>"));
    m_label->setBackgroundImage(logo);
    m_label->setMinimumSize(20, 20);
    layout->addWidget(m_label);
}

void MuckturnierLabel::setFormattingVisible(bool state)
{
    m_formatting->setVisible(state);
}

void MuckturnierLabel::setStretch(int stretch)
{
    m_stretch->setValue(stretch);
}

void MuckturnierLabel::setMaxHeight(int height)
{
    m_label->setMaximumHeight(height);

    if (m_maxHeight->value() != height) {
        m_maxHeight->blockSignals(true);
        m_maxHeight->setValue(height);
        m_maxHeight->blockSignals(false);
    }
}

int MuckturnierLabel::maxHeight() const
{
    return m_maxHeight->value();
}

int MuckturnierLabel::stretch() const
{
    return m_stretch->value();
}
