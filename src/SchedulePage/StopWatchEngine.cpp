// SPDX-FileCopyrightText: 2022-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "StopWatchEngine.h"
#include "StopWatch.h"

#include "ServerPage/Server.h"
#include "ClientPage/Client.h"

#include "network/StopWatchSync.h"

// Qt includes
#include <QWidget>
#include <QDebug>
#include <QMessageBox>
#include <QApplication>
#include <QTimer>
#include <QTime>

// C++ includes
#include <utility>

StopWatchEngine::StopWatchEngine(QObject *parent, SharedObjects *sharedObjects)
    : QObject(parent),
      m_mainWindow(qobject_cast<QWidget *>(parent)),
      m_sharedObjects(sharedObjects)
{
}

StopWatch *StopWatchEngine::newStopWatch(bool force)
{
    if (! force && m_stopWatches.count() > 0
        && QMessageBox::question(m_mainWindow,
               tr("Stoppuhr anzeigen"),
               tr("Es ist bereits mindestens eine Stoppuhr geöffnet. Soll wirklich eine weitere "
                  "angezeigt werden?"),
               QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Cancel)
           == QMessageBox::Cancel) {

        return nullptr;
    }

    m_stopWatchCounter++;

    auto *stopWatch = new StopWatch(m_mainWindow, m_sharedObjects, m_stopWatchCounter);
    if (m_isServer) {
        stopWatch->setIsServer(true);
    } else if (m_isClient) {
        stopWatch->setIsClient(true);
    }

    if (m_stopWatches.isEmpty()) {
        m_synchronizeWatch = stopWatch;
        stopWatch->setSynchronizeChecked(true);
    } else {
        stopWatch->setSynchronizeChecked(false);
    }

    m_stopWatches.append(stopWatch);
    m_stopWatchIdMap.insert(m_stopWatchCounter, stopWatch);

    connect(stopWatch, &StopWatch::closed, this, &StopWatchEngine::stopWatchClosed);
    connect(stopWatch, &StopWatch::synchronizeWatchSet,
            this, &StopWatchEngine::synchronizeWatchSet);
    connect(stopWatch, &StopWatch::requestSynchronization,
            this, &StopWatchEngine::requestSynchronization);

    stopWatch->show();

    if (m_isServer && m_stopWatches.count() > 1) {
        for (auto *stopWatch : std::as_const(m_stopWatches)) {
            stopWatch->setSynchronizeVisible(true);
        }
    }

    return stopWatch;
}

void StopWatchEngine::stopWatchClosed(StopWatch *stopWatch)
{
    m_stopWatches.removeOne(stopWatch);
    m_stopWatchIdMap.remove(stopWatch->id());

    if (m_synchronizeWatch == stopWatch) {
        m_synchronizeWatch = nullptr;

        if (! m_stopWatches.isEmpty()) {
            m_synchronizeWatch = m_stopWatches.first();
            m_synchronizeWatch->setSynchronizeChecked(true);
        }
    }

    if (m_isServer && m_stopWatches.count() == 1) {
        m_stopWatches.first()->setSynchronizeVisible(false);
    }
}

bool StopWatchEngine::stopWatchRunning() const
{
    for (const auto *stopWatch : std::as_const(m_stopWatches)) {
        if (stopWatch->isRunning()) {
            return true;
        }
    }
    return false;
}

void StopWatchEngine::closeAll()
{
    // We make a copy of m_stopWatches here, because we operate on it when closing a stop watch
    const auto allStopWatches = m_stopWatches;
    for (auto *stopWatch : allStopWatches) {
        stopWatch->stop();
        stopWatch->close();
    }
}

void StopWatchEngine::serverStarted(Server *server)
{
    m_server = server;
    m_isServer = true;
    const auto showSynchronize = m_stopWatches.count() > 1;
    for (auto *stopWatch : std::as_const(m_stopWatches)) {
        stopWatch->setIsServer(true);
        stopWatch->setSynchronizeVisible(showSynchronize);
    }
}

void StopWatchEngine::serverStopped()
{
    m_isServer = false;
    for (auto *stopWatch : std::as_const(m_stopWatches)) {
        stopWatch->setIsServer(false);
    }
}

void StopWatchEngine::clientConnected(Client *client)
{
    m_client = client;
    m_isClient = true;
    for (auto *stopWatch : std::as_const(m_stopWatches)) {
        stopWatch->setIsClient(true);
    }
}

void StopWatchEngine::clientDisconnected()
{
    m_isClient = false;
    for (auto *stopWatch : std::as_const(m_stopWatches)) {
        stopWatch->setIsClient(false);
    }
}

void StopWatchEngine::synchronizeWatchSet(StopWatch *stopWatch, bool state)
{
    if (state) {
        if (m_synchronizeWatch != nullptr) {
            m_synchronizeWatch->setSynchronizeChecked(false);
        }
        m_synchronizeWatch = stopWatch;
    } else {
        m_synchronizeWatch = nullptr;
    }
}

void StopWatchEngine::requestSynchronization(int id)
{
    m_client->requestStopWatchSynchronization(id);
}

void StopWatchEngine::setProcessingRequest(bool state)
{
    if (state) {
        QApplication::setOverrideCursor(Qt::BusyCursor);
    } else {
        QApplication::restoreOverrideCursor();
    }

    for (auto *stopWatch : m_stopWatches) {
        stopWatch->setEnabled(! state);
    }
}

StopWatchSync::Data StopWatchEngine::syncData() const
{
    if (m_synchronizeWatch == nullptr) {
        return StopWatchSync::Data();
    } else {
        return m_synchronizeWatch->syncData();
    }
}

void StopWatchEngine::noServerData() const
{
    QTimer::singleShot(0, this, [this]
    {
        QMessageBox::warning(m_mainWindow,
            tr("Stoppuhr synchronisieren"),
            tr("<p>Die Stoppuhr konnte nicht synchronisiert werden:</p>"
               "<p>Der Server stellt momentan keine Stoppuhrdaten zur Verfügung.</p>")
        );
    });
}

void StopWatchEngine::synchronize(const StopWatchSync::Data &data, int id)
{
    if (! m_stopWatchIdMap.contains(id)) {
        QTimer::singleShot(0, this, [this]
        {
            QMessageBox::warning(m_mainWindow,
                tr("Stoppuhr synchronisieren"),
                tr("<p>Die Stoppuhr konnte nicht synchronisiert werden:</p>"
                   "<p>Die anfragende Stoppuhr wurde während der Anfrage geschlossen.</p>")
            );
        });
        return;
    }

    auto *stopWatch = m_stopWatchIdMap.value(id);
    stopWatch->adoptSyncData(data);

    if (data.isRunning) {
        stopWatch->stop();

        const auto localOffset = QTime::currentTime().msec();
        auto delay = data.startOffset - localOffset;
        if (delay < 0) {
            delay += 1000;
        }

        QApplication::setOverrideCursor(Qt::WaitCursor);
        setProcessingRequest(true);
        const auto targetTime = data.targetTime;
        QTimer::singleShot(delay, this, [this, stopWatch, targetTime]
        {
            stopWatch->start(targetTime);
            setProcessingRequest(false);
        });
    }
}
