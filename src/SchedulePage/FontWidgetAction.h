// SPDX-FileCopyrightText: 2025 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef FONTWIDGETACTION_H
#define FONTWIDGETACTION_H

// Qt includes
#include <QWidgetAction>

// Qt classes
class QFontComboBox;

class FontWidgetAction : public QWidgetAction
{
    Q_OBJECT

public:
    explicit FontWidgetAction(QObject *parent);
    bool selectFamily(const QString &familyName);

Q_SIGNALS:
    void familySelected(const QString &familyName);

private Q_SLOTS:
    void setFamily();

private: // Variables
    QFontComboBox *m_family;

};

#endif // FONTWIDGETACTION_H
