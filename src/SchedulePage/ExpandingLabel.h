// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef EXPANDINGLABEL_H
#define EXPANDINGLABEL_H

// Qt includes
#include <QLabel>

class ExpandingLabel : public QLabel
{
    Q_OBJECT

public:
    explicit ExpandingLabel(QWidget *parent = nullptr);
    void resizeText();
    void setText(const QString &text);
    void setText(const QString &displayText, const QString &strippedText);

protected:
    void resizeEvent(QResizeEvent *event) override;

private: // Variables
    QString m_strippedText;

};

#endif // EXPANDINGLABEL_H
