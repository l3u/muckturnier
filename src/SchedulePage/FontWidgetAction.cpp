// SPDX-FileCopyrightText: 2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "FontWidgetAction.h"

// Qt includes
#include <QVBoxLayout>
#include <QFontComboBox>
#include <QPushButton>

FontWidgetAction::FontWidgetAction(QObject *parent) : QWidgetAction(parent)
{
    auto *widget = new QWidget;
    auto *layout = new QVBoxLayout(widget);

    m_family = new QFontComboBox;
    layout->addWidget(m_family);

    auto *button = new QPushButton(tr("Schriftart setzen"));
    connect(button, &QPushButton::clicked, this, &FontWidgetAction::setFamily);
    layout->addWidget(button);

    setDefaultWidget(widget);
}

bool FontWidgetAction::selectFamily(const QString &familyName)
{
    const auto index = m_family->findText(familyName);
    if (index != -1) {
        m_family->setCurrentIndex(index);
        return true;
    } else {
        return false;
    }
}

void FontWidgetAction::setFamily()
{
    const auto familyName = m_family->currentText();

    if (m_family->findText(familyName) != -1) {
        Q_EMIT familySelected(familyName);
    }

    Q_EMIT triggered();
}
