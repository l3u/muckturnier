// SPDX-FileCopyrightText: 2017-2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef TIMELABEL_H
#define TIMELABEL_H

// Local includes
#include "ExpandingLabel.h"

class TimeLabel : public ExpandingLabel
{
    Q_OBJECT

public:
    explicit TimeLabel(QWidget *parent = nullptr);
    void setSeconds(qint64 seconds);
    qint64 seconds() const;

private:
    qint64 m_seconds;
    bool m_displayIsRed = false;

};

#endif // TIMELABEL_H
