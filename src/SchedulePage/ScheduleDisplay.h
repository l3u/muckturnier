// SPDX-FileCopyrightText: 2023-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SCHEDULEDISPLAY_H
#define SCHEDULEDISPLAY_H

// Local includes
#include "shared/FullScreenDialog.h"

// Qt includes
#include <QHash>
#include <QJsonObject>

// Local classes
class Settings;
class ResizableLabel;
class ImageWidget;
class MuckturnierLabel;
class FontWidgetAction;

// Qt classes
class QLabel;
class QScreen;
class QStackedLayout;
class QMenu;
class QCloseEvent;
class QSpinBox;

class ScheduleDisplay : public FullScreenDialog
{
    Q_OBJECT

public:
    enum Page {
        Welcome,
        Schedule,
        GoodBye
    };

    explicit ScheduleDisplay(QWidget *parent, Settings *settings, const QString &dbPath,
                             const QString &displayLogo);
    void setScheduleStarted(bool state);

public Q_SLOTS:
    void updateClock(const QString &text);
    void updateCurrentRound(int round);
    void updateNextRound(int round);
    void updateRoundTimeLeft(const QString &text);
    void updateTimeToNextRound(const QString &text);
    void updateNextRoundStart(const QString &text);
    void showPage(Page page);

protected:
    void closeEvent(QCloseEvent *event) override;

private Q_SLOTS:
    void setFormattingVisible(bool state);
    void setMargin(Qt::Alignment pos, int margin);
    void chooseColor(const QString &name);
    void saveNewSettings();
    void saveSettings(const QString &id);
    void deleteSettings(const QString &id);
    void resetSettings();
    void loadImage(Page page, const QString &file);
    void removeImage(Page page);
    void startFontMenu();
    void finishFontMenu();

private: // Functions
    void loadSettings(const QString &id);
    void setTimeToNextRoundText(const QString &text);
    QJsonObject currentSettings() const;
    void updateSettingsMenus();

private: // Variables
    Settings *m_settings;
    const QString m_dbPath;
    QHash<Page, ImageWidget *> m_pagesMap;
    QJsonObject m_savedSettings;

    QStackedLayout *m_layout;
    ImageWidget *m_welcome;
    ImageWidget *m_schedule;
    ImageWidget *m_goodBye;

    QMenu *m_menu;
    QMenu *m_fontMenu;
    FontWidgetAction *m_fontWidgetAction;
    QMenu *m_loadSettingsMenu;
    QMenu *m_saveSettingsMenu;
    QMenu *m_deleteSettingsMenu;

    QWidget *m_labelsWrapper;
    QAction *m_setFormattingVisible;

    QHash<QString, ResizableLabel *> m_labels;
    MuckturnierLabel *m_logo;
    QHash<Qt::Alignment, QSpinBox *> m_margins;
    QHash<Qt::Alignment, QLabel *> m_spacers;

    QHash<QString, QColor> m_colors;

    QFlags<Qt::WindowState> m_lastWindowState = Qt::WindowNoState;

    bool m_scheduleStarted = false;
    int m_currentRound = 0;
    int m_nextRound = 0;
    bool m_negativeTime = false;
    QString m_currentSettingsId;

};

#endif // SCHEDULEDISPLAY_H
