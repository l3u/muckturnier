// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "ImageWidget.h"

// Qt includes
#include <QDebug>
#include <QStyleOption>
#include <QPainter>
#include <QTimer>
#include <QVBoxLayout>
#include <QLabel>

ImageWidget::ImageWidget(const QString &message, QWidget *parent) : QWidget(parent)
{
    m_smoothTimer = new QTimer(this);
    m_smoothTimer->setSingleShot(true);
    connect(m_smoothTimer, &QTimer::timeout, this, &ImageWidget::setSmoothImage);

    if (! message.isEmpty()) {
        auto *layout = new QVBoxLayout(this);
        m_message = new QLabel(message);
        m_message->setAlignment(Qt::AlignCenter);
        layout->addWidget(m_message);
    }
}

bool ImageWidget::setBackgroundImage(const QString &path)
{
    if (! m_backgroundImage.load(path)) {
        return false;
    }

    m_backgroundImagePath = path;

    if (m_message != nullptr) {
        m_message->hide();
    }

    setSmoothImage();
    update();

    return true;
}

const QString &ImageWidget::backgroundImagePath() const
{
    return m_backgroundImagePath;
}

void ImageWidget::removeBackgroundImage()
{
    m_backgroundImage = QImage();
    m_scaledBackgroundImage = QPixmap();
    m_backgroundImagePath.clear();

    if (m_message != nullptr) {
        m_message->show();
    }

    update();
}

void ImageWidget::setBackgroundColor(const QColor &color)
{
    m_backgroundColor = color;
    m_backgroundBrush = QBrush(m_backgroundColor);
    update();
}

void ImageWidget::removeBackgroundColor()
{
    m_backgroundColor = QColor();
    update();
}

void ImageWidget::paintEvent(QPaintEvent *event)
{
    QStyleOption opt;
    opt.initFrom(this);
    QPainter painter(this);

    if (m_backgroundColor.isValid()) {
        painter.fillRect(opt.rect, m_backgroundBrush);
    }

    if (! m_backgroundImage.isNull()) {
        const auto cr = contentsRect();

        const auto crSize = cr.size();
        const auto bgSize = m_scaledBackgroundImage.size();

        if (m_scaledBackgroundImage.isNull()
            || crSize.width() < bgSize.width()
            || crSize.height() < bgSize.height()
            || (crSize.width() > bgSize.width() && crSize.height() > bgSize.height())) {

            m_scaledBackgroundImage = QPixmap::fromImage(
                m_backgroundImage.scaled(cr.size(), Qt::KeepAspectRatio, Qt::FastTransformation));
            m_smoothTimer->start(200);
        }

        style()->drawItemPixmap(&painter, cr, Qt::AlignCenter | Qt::AlignVCenter,
                                m_scaledBackgroundImage);

    }

    QWidget::paintEvent(event);
}

void ImageWidget::setSmoothImage()
{
    m_scaledBackgroundImage = QPixmap::fromImage(
        m_backgroundImage.scaled(size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
    update();
}
