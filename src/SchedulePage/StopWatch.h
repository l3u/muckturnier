// SPDX-FileCopyrightText: 2017-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef STOPWATCH_H
#define STOPWATCH_H

// Local includes
#include "shared/TitleDialog.h"

// Qt includes
#include <QDateTime>

// Local classes

class SharedObjects;
class Settings;
class TimeLabel;
class ExpandingLabel;
class IconButton;

namespace StopWatchSync
{
struct Data;
}

// Qt classes
class QTimer;
class QStackedLayout;
class QSpinBox;
class QPushButton;
class QLineEdit;
class QCloseEvent;
class QKeyEvent;

class StopWatch : public TitleDialog
{
    Q_OBJECT

public:
    explicit StopWatch(QWidget *parent, SharedObjects *sharedObjects, int id);
    bool isRunning() const;
    void setIsServer(bool state);
    void setIsClient(bool state);
    void setSynchronizeVisible(bool state);
    void setSynchronizeChecked(bool state);
    int id() const;
    StopWatchSync::Data syncData() const;
    void adoptSyncData(const StopWatchSync::Data &data);

    void setStartingMode();
    void setScheduleMode(const QString &title, const QDateTime &target);

Q_SIGNALS:
    void closed(StopWatch *stopWatch);
    void synchronizeWatchSet(StopWatch *stopWatch, bool state);
    void requestSynchronization(int id);

public Q_SLOTS:
    void stop();
    void start(qint64 targetTime);
    void update();

protected: // Functions
    void closeEvent(QCloseEvent *event) override;
    void keyPressEvent(QKeyEvent *event) override;

private Q_SLOTS:
    void pause();
    void synchronizeClicked();

private: // Functions
    void setTargetTime(qint64 seconds);

private: // Variables
    const int m_id;
    Settings *m_settings;
    QTimer *m_timer;
    QDateTime m_targetTime;
    bool m_isRunning = false;
    QByteArray m_geometry;

    QStackedLayout *m_layout;

    QWidget *m_settingsWidget;
    QLineEdit *m_titleInput;
    QSpinBox *m_hours;
    QSpinBox *m_minutes;
    QSpinBox *m_seconds;

    QLabel *m_startingLabel;

    QWidget *m_stopWatchWidget;
    ExpandingLabel *m_titleDisplay;
    TimeLabel *m_timeDisplay;
    QPushButton *m_start;
    QPushButton *m_pause;
    QPushButton *m_stop;

    IconButton *m_synchronize;
    bool m_isServer = false;
    bool m_isClient = false;
    int m_startOffset = 0;

    bool m_scheduleMode = false;

};

#endif // STOPWATCH_H
