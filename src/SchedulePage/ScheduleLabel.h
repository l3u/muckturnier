// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SCHEDULELABEL_H
#define SCHEDULELABEL_H

// Qt includes
#include <QLabel>
#include <QVariant>

class ScheduleLabel : public QLabel
{
    Q_OBJECT

public:
    explicit ScheduleLabel(const QVariant &text = QVariant(), QWidget *parent = nullptr);
    void setTimeMode(bool state);

public Q_SLOTS:
    void setText(const QVariant &text = QVariant());

private: // Variables
    bool m_timeMode = false;

};

#endif // SCHEDULELABEL_H
