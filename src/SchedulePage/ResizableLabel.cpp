// SPDX-FileCopyrightText: 2023-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "ResizableLabel.h"
#include "ExpandingLabel.h"
#include "FontWidgetAction.h"

#include "shared/Logging.h"

// Qt includes
#include <QLabel>
#include <QHBoxLayout>
#include <QFontMetrics>
#include <QDebug>
#include <QToolButton>
#include <QSpinBox>
#include <QColorDialog>
#include <QMenu>

// C++ includes
#include <functional>

ResizableLabel::ResizableLabel(FontWidgetAction *fontWidgetAction, QWidget *parent)
    : QWidget(parent),
      m_fontWidgetAction(fontWidgetAction)
{
    auto *layout = new QHBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);

    m_formatting = new QWidget;
    m_formatting->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Preferred);
    auto *formatLayout = new QVBoxLayout(m_formatting);
    formatLayout->setContentsMargins(0, 0, 0, 0);
    formatLayout->addStretch();

    auto *upperLayout = new QHBoxLayout;
    formatLayout->addLayout(upperLayout);

    auto *color = new QToolButton;
    color->setToolTip(tr("Textfarbe auswählen"));
    color->setText(QStringLiteral("\u273D"));
    connect(color, &QToolButton::clicked, this, &ResizableLabel::selectColor);
    upperLayout->addWidget(color);

    auto *left = new QToolButton;
    left->setToolTip(tr("Text linksbündig"));
    m_alignment.insert(left, Qt::AlignLeft);
    left->setText(tr("←"));
    left->setCheckable(true);
    connect(left, &QToolButton::toggled,
            this, std::bind(&ResizableLabel::setAlignment, this, Qt::AlignLeft));
    upperLayout->addWidget(left);

    auto *center = new QToolButton;
    center->setToolTip(tr("Text zentriert"));
    m_alignment.insert(center, Qt::AlignCenter);
    center->setText(tr("↔"));
    center->setCheckable(true);
    center->setChecked(true);
    connect(center, &QToolButton::toggled,
            this, std::bind(&ResizableLabel::setAlignment, this, Qt::AlignCenter));
    upperLayout->addWidget(center);

    auto *right = new QToolButton;
    right->setToolTip(tr("Text rechtsbündig"));
    m_alignment.insert(right, Qt::AlignRight);
    right->setText(tr("→"));
    right->setCheckable(true);
    connect(right, &QToolButton::toggled,
            this, std::bind(&ResizableLabel::setAlignment, this, Qt::AlignRight));
    upperLayout->addWidget(right);

    auto *family = new QToolButton;
    family->setPopupMode(QToolButton::InstantPopup);
    family->setToolTip(tr("Schriftart auswählen"));
    family->setText(tr("S"));
    m_fontMenu = new QMenu(this);
    connect(m_fontMenu, &QMenu::aboutToShow, this, &ResizableLabel::startFontMenu);
    connect(m_fontMenu, &QMenu::aboutToHide, this, &ResizableLabel::finishFontMenu);
    family->setMenu(m_fontMenu);
    upperLayout->addWidget(family);

    m_bold = new QToolButton;
    m_bold->setToolTip(tr("Text fett"));
    m_bold->setText(tr("F"));
    m_bold->setCheckable(true);
    connect(m_bold, &QToolButton::toggled, this, &ResizableLabel::setBold);
    upperLayout->addWidget(m_bold);

    auto *lowerLayout = new QHBoxLayout;
    formatLayout->addLayout(lowerLayout);

    const QFontMetrics fontMetrics(font());
    const auto rect = fontMetrics.boundingRect(QStringLiteral("XXXXXXXXXXXXXXXXXXXXXXXXX"));

    auto *maxHeightLabel = new QLabel(tr("Max. H"));
    maxHeightLabel->setToolTip(tr("Maximale Höhe in Pixeln"));
    lowerLayout->addWidget(maxHeightLabel);

    m_maxHeight = new QSpinBox;
    m_maxHeight->setToolTip(tr("Maximale Höhe in Pixeln"));
    m_maxHeight->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    m_maxHeight->setMinimum(rect.height());
    m_maxHeight->setValue(rect.height() * 6);
    m_maxHeight->setMaximum(999);
    connect(m_maxHeight, &QSpinBox::valueChanged, this, &ResizableLabel::setMaxHeight);
    lowerLayout->addWidget(m_maxHeight);

    auto *stretchLabel = new QLabel(tr("H rel."));
    stretchLabel->setToolTip(tr("Relative Höhe im Vergleich zu den anderen Labeln"));
    lowerLayout->addWidget(stretchLabel);

    m_stretch = new QSpinBox;
    m_stretch->setToolTip(tr("Relative Höhe im Vergleich zu den anderen Labeln"));
    m_stretch->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    m_stretch->setMinimum(0);
    m_stretch->setMaximum(999);
    connect(m_stretch, &QSpinBox::valueChanged, this, &ResizableLabel::stretchFactorChanged);
    lowerLayout->addWidget(m_stretch);

    formatLayout->addStretch();

    m_formatting->hide();
    layout->addWidget(m_formatting);

    m_label = new ExpandingLabel;
    m_label->setMinimumWidth(rect.width());
    m_label->setMinimumHeight(rect.height());

    layout->addWidget(m_label);

    reset();
}

void ResizableLabel::setText(const QString &text)
{
    m_label->setText(text);
}

void ResizableLabel::setText(const QString &displayText, const QString &strippedText)
{
    m_label->setText(displayText, strippedText);
}

void ResizableLabel::reset()
{
    m_label->setMaximumHeight(m_label->minimumHeight() * 6);
    auto labelFont = m_label->font();
    labelFont.setBold(false);
    m_label->setFont(labelFont);
    m_label->setAlignment(Qt::AlignCenter);
}

void ResizableLabel::setBold(bool state)
{
    auto labelFont = m_label->font();
    labelFont.setBold(state);
    m_label->setFont(labelFont);

    if (m_bold->isChecked() != state) {
        m_bold->blockSignals(true);
        m_bold->setChecked(state);
        m_bold->blockSignals(false);
    }
}

void ResizableLabel::setAlignment(Qt::AlignmentFlag alignment)
{
    Q_ASSERT(alignment == Qt::AlignLeft
             || alignment == Qt::AlignCenter
             || alignment == Qt::AlignRight);

    QHash<QToolButton *, Qt::AlignmentFlag>::const_iterator it;
    for (it = m_alignment.constBegin(); it != m_alignment.constEnd(); it++) {
        auto *button = it.key();
        auto buttonAlignment = it.value();
        button->blockSignals(true);
        button->setChecked(buttonAlignment == alignment);
        button->blockSignals(false);
    }

    m_label->setAlignment(alignment);
}

void ResizableLabel::setMaxHeight(int height)
{
    m_label->setMaximumHeight(height);

    if (m_maxHeight->value() != height) {
        m_maxHeight->blockSignals(true);
        m_maxHeight->setValue(height);
        m_maxHeight->blockSignals(false);
    }
}

void ResizableLabel::setStretch(int stretch)
{
    m_stretch->setValue(stretch);
}

void ResizableLabel::setFormattingVisible(bool state)
{
    m_formatting->setVisible(state);
}

Qt::AlignmentFlag ResizableLabel::alignment() const
{
    QHash<QToolButton *, Qt::AlignmentFlag>::const_iterator it;
    for (it = m_alignment.constBegin(); it != m_alignment.constEnd(); it++) {
        if (it.key()->isChecked()) {
            return it.value();
        }
    }

    // We shouldn't reach here
    qCWarning(MuckturnierLog) << "No alignment set, falling back to centered";
    return Qt::AlignCenter;
}

bool ResizableLabel::isBold() const
{
    return m_bold->isChecked();
}

int ResizableLabel::maxHeight() const
{
    return m_maxHeight->value();
}

int ResizableLabel::stretch() const
{
    return m_stretch->value();
}

void ResizableLabel::setColor(const QColor &color)
{
    // We have to cache the color ...
    m_color = color;
    auto palette = m_label->palette();
    palette.setColor(QPalette::WindowText, color);
    m_label->setPalette(palette);
}

const QColor &ResizableLabel::color() const
{
    // ... because if the color is requested from the round or break time left label, and it's
    // currently set to the negative time text, this one will be returned instead of the actual
    // label color
    return m_color;
}

void ResizableLabel::setFontFamily(const QString &family)
{
    auto labelFont = m_label->font();
    labelFont.setFamily(family);
    m_label->setFont(labelFont);
    m_label->resizeText();
}

QString ResizableLabel::ResizableLabel::fontFamily() const
{
    return m_label->font().family();
}

void ResizableLabel::startFontMenu()
{
    m_fontMenu->addAction(m_fontWidgetAction);
    connect(m_fontWidgetAction, &FontWidgetAction::familySelected,
            this, &ResizableLabel::setFontFamily);
}

void ResizableLabel::finishFontMenu()
{
    m_fontWidgetAction->disconnect();
    m_fontMenu->removeAction(m_fontWidgetAction);
}

void ResizableLabel::selectColor()
{
    const auto selectedColor = QColorDialog::getColor(m_color);
    if (! selectedColor.isValid()) {
        return;
    }
    setColor(selectedColor);
}

void ResizableLabel::resizeText()
{
    m_label->resizeText();
}
