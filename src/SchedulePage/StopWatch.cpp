// SPDX-FileCopyrightText: 2017-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "StopWatch.h"
#include "TimeLabel.h"
#include "StopWatchState.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/Settings.h"

#include "shared/IconButton.h"

#include "network/StopWatchSync.h"

// Qt includes
#include <QDebug>
#include <QTimer>
#include <QStackedLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QSpinBox>
#include <QPushButton>
#include <QLineEdit>
#include <QMessageBox>
#include <QCloseEvent>

// C++ includes
#include <functional>

StopWatch::StopWatch(QWidget *parent, SharedObjects *sharedObjects, int id)
    : TitleDialog(parent),
      m_id(id),
      m_settings(sharedObjects->settings())
{
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowTitle(tr("Muckturnier: Stoppuhr"));

    m_timer = new QTimer(this);
    m_timer->setTimerType(Qt::PreciseTimer);
    connect(m_timer, &QTimer::timeout, this, &StopWatch::update);

    const StopWatchState::State state = m_settings->stopWatchState();
    m_geometry = state.geometry;

    m_layout = new QStackedLayout;
    layout()->addLayout(m_layout);

    // Settings Widget

    m_settingsWidget = new QWidget;
    auto *settingsLayout = new QVBoxLayout(m_settingsWidget);

    auto *titleLayout = new QHBoxLayout;
    settingsLayout->addLayout(titleLayout);

    titleLayout->addWidget(new QLabel(tr("Titel:")));

    m_titleInput = new QLineEdit;
    m_titleInput->setText(state.title);
    titleLayout->addWidget(m_titleInput);

    auto *timeSpanLayout = new QHBoxLayout;
    settingsLayout->addLayout(timeSpanLayout);

    timeSpanLayout->addStretch();

    m_hours = new QSpinBox;
    m_hours->setMinimum(0);
    m_hours->setMaximum(99);
    m_hours->setValue(state.hours);
    timeSpanLayout->addWidget(m_hours);
    timeSpanLayout->addWidget(new QLabel(tr("h")));

    m_minutes = new QSpinBox;
    m_minutes->setMinimum(0);
    m_minutes->setMaximum(59);
    m_minutes->setValue(state.minutes);
    timeSpanLayout->addWidget(m_minutes);
    timeSpanLayout->addWidget(new QLabel(tr("m")));

    m_seconds = new QSpinBox;
    m_seconds->setMinimum(0);
    m_seconds->setMaximum(59);
    m_seconds->setValue(state.seconds);
    timeSpanLayout->addWidget(m_seconds);
    timeSpanLayout->addWidget(new QLabel(tr("s")));

    timeSpanLayout->addStretch();

    m_layout->addWidget(m_settingsWidget);

    // "Starting" label
    m_startingLabel = new QLabel(tr("Starte Stopppuhr …"));
    m_startingLabel->setAlignment(Qt::AlignCenter);
    m_layout->addWidget(m_startingLabel);

    // Stopwatch widget

    m_stopWatchWidget = new QWidget;
    auto *stopWatchLayout = new QVBoxLayout(m_stopWatchWidget);

    m_titleDisplay = new ExpandingLabel;
    stopWatchLayout->addWidget(m_titleDisplay);

    m_timeDisplay = new TimeLabel;
    stopWatchLayout->addWidget(m_timeDisplay);

    m_layout->addWidget(m_stopWatchWidget);

    // Control buttons

    auto *controlLayout = new QHBoxLayout;
    layout()->addLayout(controlLayout);

    m_start = new QPushButton(tr("Start"));
    m_start->setFocusPolicy(Qt::NoFocus);
    connect(m_start, &QPushButton::clicked,
            this, std::bind(&StopWatch::start, this, -1));
    controlLayout->addWidget(m_start);

    m_pause = new QPushButton(tr("Pause"));
    m_pause->setFocusPolicy(Qt::NoFocus);
    m_pause->setCheckable(true);
    connect(m_pause, &QPushButton::clicked, this, &StopWatch::pause);
    m_pause->hide();
    controlLayout->addWidget(m_pause);

    m_stop = new QPushButton(tr("Stop"));
    m_stop->setFocusPolicy(Qt::NoFocus);
    connect(m_stop, &QPushButton::clicked, this, &StopWatch::stop);
    m_stop->hide();
    controlLayout->addWidget(m_stop);

    m_synchronize = new IconButton(sharedObjects, QStringLiteral("synchronize.png"));
    connect(m_synchronize, &QPushButton::clicked, this, &StopWatch::synchronizeClicked);
    m_synchronize->hide();
    controlLayout->addWidget(m_synchronize);
}

void StopWatch::setStartingMode()
{
    m_layout->setCurrentWidget(m_startingLabel);
    m_start->hide();
    restoreGeometry(m_geometry);
}

void StopWatch::setScheduleMode(const QString &title, const QDateTime &target)
{
    m_scheduleMode = true;
    m_titleInput->setText(title);
    m_targetTime = target;
}

void StopWatch::setTargetTime(qint64 seconds)
{
    m_targetTime = QDateTime::currentDateTime().addSecs(seconds);
}

void StopWatch::start(qint64 targetTime)
{
    m_layout->setCurrentWidget(m_stopWatchWidget);

    m_start->hide();
    m_pause->setVisible(! m_scheduleMode);
    m_stop->setVisible(! m_scheduleMode);

    if (targetTime == -1) {
        if (! m_scheduleMode) {
            setTargetTime(m_hours->value() * 3600 + m_minutes->value() * 60 + m_seconds->value());
        }
    } else {
        m_targetTime = QDateTime::fromMSecsSinceEpoch(targetTime);
    }

    const auto title = m_titleInput->text().simplified();
    if (title.isEmpty()) {
        m_titleDisplay->hide();
    } else {
        m_titleDisplay->setText(title);
        m_titleDisplay->show();
        m_titleDisplay->resizeText();
    }

    if (! m_scheduleMode) {
        m_timer->start(1000);
    }

    m_startOffset = QTime::currentTime().msec();

    m_isRunning = true;
    update();
    restoreGeometry(m_geometry);
    m_timeDisplay->resizeText();
}

void StopWatch::update()
{
    QDateTime currentDateTime = QDateTime::currentDateTime();
    // We use msecsTo here instead of secsTo to avoid rounding problems.
    // If we use secsTo, we get "0" both for the currentDateTime == m_targetTime
    // and also for one second before.
    m_timeDisplay->setSeconds(qRound64(currentDateTime.msecsTo(m_targetTime) / 1000.0));
}

void StopWatch::stop()
{
    m_timer->stop();
    m_pause->setChecked(false);
    m_isRunning = false;
    m_geometry = saveGeometry();

    m_layout->setCurrentWidget(m_settingsWidget);

    m_start->show();
    m_pause->hide();
    m_stop->hide();

    resizeToContents();
}

void StopWatch::pause()
{
    if (m_pause->isChecked()) {
        m_timer->stop();
    } else {
        setTargetTime(m_timeDisplay->seconds());
        m_timer->start(1000);
        m_pause->clearFocus();
    }
}

bool StopWatch::isRunning() const
{
    return m_isRunning;
}


void StopWatch::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Escape) {
        // Don't close on ESC, as the closeEvent won't be processed in this case
        event->ignore();
        return;
    }

    QDialog::keyPressEvent(event);
}

void StopWatch::closeEvent(QCloseEvent *event)
{
    if (m_isRunning
        && QMessageBox::warning(this,
               tr("Stoppuhr schließen"),
               (m_scheduleMode
                    ? tr("<p>Diese Stoppuhr wurde automatisch von der Turnier-Zeitplan-Seite "
                         "geöffnet.</p>")
                    : tr("<p>Die Stoppuhr läuft gerade!</p>")
               ).append(tr("<p>Soll sie wirklich geschlossen werden?</p>")),
            QMessageBox::Close | QMessageBox::Cancel, QMessageBox::Cancel)
        == QMessageBox::Cancel) {

        event->ignore();
        return;
    }

    if (m_settings->saveOnCloseEnabled(Settings::SaveStopWatchState)) {
        m_settings->saveStopWatchState(
            StopWatchState::State { m_hours->value(),
                                    m_minutes->value(),
                                    m_seconds->value(),
                                    m_titleInput->text(),
                                    m_isRunning ? saveGeometry() : m_geometry });
    }

    Q_EMIT closed(this);
    QDialog::closeEvent(event);
}

void StopWatch::setIsServer(bool state)
{
    m_isServer = state;
    if (! state) {
        m_synchronize->hide();
    }
    m_synchronize->setCheckable(true);
    m_synchronize->setToolTip(tr("Diese Stoppuhr zum Synchronisieren mit Clients festlegen"));
}

void StopWatch::setIsClient(bool state)
{
    m_isClient = state;
    m_synchronize->setVisible(state);
    m_synchronize->setCheckable(false);
    m_synchronize->setToolTip(tr("Diese Stoppuhr mit der des Servers synchronisieren"));
}

void StopWatch::setSynchronizeVisible(bool state)
{
    m_synchronize->setVisible(state);
}

void StopWatch::setSynchronizeChecked(bool state)
{
    m_synchronize->blockSignals(true);
    m_synchronize->setChecked(state);
    m_synchronize->blockSignals(false);
}

int StopWatch::id() const
{
    return m_id;
}

void StopWatch::synchronizeClicked()
{
    if (m_isServer) {
        Q_EMIT synchronizeWatchSet(this, m_synchronize->isChecked());
    } else if (m_isClient) {
        Q_EMIT requestSynchronization(m_id);
    }
}

StopWatchSync::Data StopWatch::syncData() const
{
    return StopWatchSync::Data {
        true, // syncPossible
        m_titleInput->text(),
        m_hours->value(),
        m_minutes->value(),
        m_seconds->value(),
        m_isRunning,
        m_startOffset,
        m_isRunning ? m_targetTime.toMSecsSinceEpoch() : 0
    };
}

void StopWatch::adoptSyncData(const StopWatchSync::Data &data)
{
    m_titleInput->setText(data.title);
    m_hours->setValue(data.hours);
    m_minutes->setValue(data.minutes);
    m_seconds->setValue(data.seconds);
}
