// SPDX-FileCopyrightText: 2023-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef RESIZABLELABEL_H
#define RESIZABLELABEL_H

// Qt includes
#include <QWidget>
#include <QHash>
#include <QColor>

// Local classes
class ExpandingLabel;
class FontWidgetAction;

// Qt classes
class QLabel;
class QToolButton;
class QSpinBox;
class QMenu;

class ResizableLabel : public QWidget
{
    Q_OBJECT

public:
    explicit ResizableLabel(FontWidgetAction *fontWidgetAction, QWidget *parent = nullptr);
    Qt::AlignmentFlag alignment() const;
    bool isBold() const;
    int maxHeight() const;
    int stretch() const;
    void setColor(const QColor &color);
    const QColor &color() const;
    void setFontFamily(const QString &family);
    QString fontFamily() const;
    void resizeText();
    void setText(const QString &displayText, const QString &strippedText);
    void setFormattingVisible(bool state);
    void reset();
    void setText(const QString &text);
    void setStretch(int stretch);

Q_SIGNALS:
    void stretchFactorChanged(int factor);

public Q_SLOTS:
    void setBold(bool state);
    void setAlignment(Qt::AlignmentFlag alignment);
    void setMaxHeight(int height);
    void startFontMenu();
    void finishFontMenu();
    void selectColor();

private: // Variables
    FontWidgetAction *m_fontWidgetAction;
    QWidget *m_formatting;
    QToolButton *m_bold;
    QHash<QToolButton *, Qt::AlignmentFlag> m_alignment;
    QSpinBox *m_maxHeight;
    QSpinBox *m_stretch;
    ExpandingLabel *m_label;
    QColor m_color;
    QMenu *m_fontMenu;

};

#endif // RESIZABLELABEL_H
