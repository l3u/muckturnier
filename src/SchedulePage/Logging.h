// SPDX-FileCopyrightText: 2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef LOGGING_SCHEDULE_H
#define LOGGING_SCHEDULE_H

#include <QLoggingCategory>

Q_DECLARE_LOGGING_CATEGORY(ScheduleDisplayLog)

#endif // LOGGING_SCHEDULE_H
