// SPDX-FileCopyrightText: 2023-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "ScheduleLabel.h"

#include "shared/SharedStyles.h"

ScheduleLabel::ScheduleLabel(const QVariant &text, QWidget *parent) : QLabel(parent)
{
    setText(text);
}

void ScheduleLabel::setTimeMode(bool state)
{
    m_timeMode = state;
}

void ScheduleLabel::setText(const QVariant &text)
{
    QString processedText;

    if (text.isNull() || (
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
        text.type() == QVariant::Int
#else
        text.typeId() == QMetaType::Int
#endif
        && text.toInt() == 0)) {

        processedText = tr("–");
    } else {
        processedText = text.toString();
    }

    if (m_timeMode && processedText.startsWith(tr("- "))) {
        processedText = QStringLiteral("<span style=\"%1\">%2</span>").arg(
                                       SharedStyles::redText, processedText);
    }

    QLabel::setText(processedText);
}
