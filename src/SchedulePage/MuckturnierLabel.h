// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef MUCKTURNIERLABEL_H
#define MUCKTURNIERLABEL_H

// Qt includes
#include <QWidget>

// Local classes
class ImageWidget;

// Qt classes
class QResizeEvent;
class QSpinBox;

class MuckturnierLabel : public QWidget
{
    Q_OBJECT

public:
    explicit MuckturnierLabel(const QString &logo, QWidget *parent = nullptr);
    int maxHeight() const;
    int stretch() const;

Q_SIGNALS:
    void stretchFactorChanged(int factor);

public Q_SLOTS:
    void setFormattingVisible(bool state);
    void setMaxHeight(int height);
    void setStretch(int stretch);

private: // Variables
    QWidget *m_formatting;
    QSpinBox *m_maxHeight;
    QSpinBox *m_stretch;
    ImageWidget *m_label;

};

#endif // MUCKTURNIERLABEL_H
