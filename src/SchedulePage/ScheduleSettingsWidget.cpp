// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "ScheduleSettingsWidget.h"

#include "shared/DefaultValues.h"
#include "shared/Schedule.h"

// Qt includes
#include <QGridLayout>
#include <QLabel>
#include <QSpinBox>
#include <QDebug>

ScheduleSettingsWidget::ScheduleSettingsWidget(QWidget *parent) : QWidget(parent)
{
    auto *layout = new QGridLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);

    layout->addWidget(new QLabel(tr("Geplante Runden:")), 0, 0);

    m_plannedRoundsValue = new QLabel;
    m_plannedRoundsValue->hide();
    layout->addWidget(m_plannedRoundsValue, 0, 1);

    m_plannedRounds = new QSpinBox;
    m_plannedRounds->setMinimum(1);
    m_plannedRounds->setMaximum(DefaultValues::maximumRounds);
    m_plannedRounds->setValue(DefaultValues::plannedRounds);
    connect(m_plannedRounds, &QSpinBox::valueChanged, this, &ScheduleSettingsWidget::valuesChanged);
    layout->addWidget(m_plannedRounds, 0, 2);

    layout->addWidget(new QLabel(tr("Zeit pro Runde:")), 1, 0);

    m_roundDurationValue = new QLabel;
    m_roundDurationValue->hide();
    layout->addWidget(m_roundDurationValue, 1, 1);

    m_roundDuration = new QSpinBox;
    m_roundDuration->setMinimum(1);
    m_roundDuration->setMaximum(DefaultValues::maximumRoundDuration);
    m_roundDuration->setValue(DefaultValues::roundDuration);
    m_roundDuration->setSuffix(tr(" min"));
    connect(m_roundDuration, &QSpinBox::valueChanged, this, &ScheduleSettingsWidget::valuesChanged);
    layout->addWidget(m_roundDuration, 1, 2);

    layout->addWidget(new QLabel(tr("Pause zwischen den Runden:")), 2, 0);

    m_breakDurationValue = new QLabel;
    m_breakDurationValue->hide();
    layout->addWidget(m_breakDurationValue, 2, 1);

    m_breakDuration = new QSpinBox;
    m_breakDuration->setMinimum(0);
    m_breakDuration->setMaximum(DefaultValues::maximumBreakDuration);
    m_breakDuration->setValue(DefaultValues::breakDuration);
    m_breakDuration->setSuffix(tr(" min"));
    connect(m_breakDuration, &QSpinBox::valueChanged, this, &ScheduleSettingsWidget::valuesChanged);
    layout->addWidget(m_breakDuration, 2, 2);
}

void ScheduleSettingsWidget::setChangeable(bool state)
{
    if (! state) {
        m_roundDurationValue->setText(tr("%1 min").arg(roundDuration()));
        m_breakDurationValue->setText(tr("%1 min").arg(breakDuration()));
        m_plannedRoundsValue->setText(QString::number(plannedRounds()));
    }

    m_roundDurationValue->setVisible(! state);
    m_roundDuration->setVisible(state);
    m_breakDurationValue->setVisible(! state);
    m_breakDuration->setVisible(state);
    m_plannedRoundsValue->setVisible(! state);
    m_plannedRounds->setVisible(state);
}

bool ScheduleSettingsWidget::isChangeable() const
{
    return m_roundDuration->isVisible();
}

int ScheduleSettingsWidget::plannedRounds() const
{
    return m_plannedRounds->value();
}

int ScheduleSettingsWidget::roundDuration() const
{
    return m_roundDuration->value();
}

int ScheduleSettingsWidget::breakDuration() const
{
    return m_breakDuration->value();
}

void ScheduleSettingsWidget::adoptSettings(const Schedule::Settings &settings)
{
    blockSignals(true);

    m_plannedRounds->clearFocus();
    m_roundDuration->clearFocus();
    m_breakDuration->clearFocus();

    m_plannedRounds->setValue(settings.plannedRounds);
    m_roundDuration->setValue(settings.roundDuration);
    m_breakDuration->setValue(settings.breakDuration);

    blockSignals(false);
}

Schedule::Settings ScheduleSettingsWidget::settings() const
{
    return Schedule::Settings {
        plannedRounds(),
        roundDuration(),
        breakDuration()
    };
}
