// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "ExpandingLabel.h"

// Qt includes
#include <QDebug>

ExpandingLabel::ExpandingLabel(QWidget *parent) : QLabel(parent)
{
    setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    setMinimumHeight(15);
}

void ExpandingLabel::setText(const QString &text)
{
    setText(text, text);
}

void ExpandingLabel::setText(const QString &displayText, const QString &strippedText)
{
    QLabel::setText(displayText);
    m_strippedText = strippedText;
    resizeText();
}

void ExpandingLabel::resizeText()
{
    if (m_strippedText.isEmpty()) {
        return;
    }

    auto labelSpace = contentsRect();
    labelSpace.setWidth(labelSpace.width() * 0.95);
    auto font = this->font();

    int size = 0;
    const auto steps = { 100, 50, 25, 10, 5, 1 };
    for (const int step : steps) {
        while (true) {
            size += step;
            font.setPointSize(size);
            const QFontMetrics fontMetrics(font);
            const auto targetSpace = fontMetrics.boundingRect(m_strippedText);

            if (targetSpace.height() >= labelSpace.height()
                || targetSpace.width() >= labelSpace.width()) {

                size -= step;
                break;
            }
        }
    }

    setFont(font);
}

void ExpandingLabel::resizeEvent(QResizeEvent *event)
{
    QLabel::resizeEvent(event);
    resizeText();
}
