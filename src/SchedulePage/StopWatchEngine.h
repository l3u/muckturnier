// SPDX-FileCopyrightText: 2022-2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef STOPWATCHENGINE_H
#define STOPWATCHENGINE_H

// Qt includes
#include <QObject>
#include <QHash>

// Qt classes
class m_mainWindow;

// Local classes

class SharedObjects;
class StopWatch;
class Server;
class Client;

namespace StopWatchSync
{
struct Data;
}

class StopWatchEngine : public QObject
{
    Q_OBJECT

public:
    explicit StopWatchEngine(QObject *parent, SharedObjects *sharedObjects);
    bool stopWatchRunning() const;
    void closeAll();

    void serverStarted(Server *server);
    void serverStopped();
    void clientConnected(Client *client);
    void clientDisconnected();
    void setProcessingRequest(bool state);
    StopWatchSync::Data syncData() const;
    void noServerData() const;
    void synchronize(const StopWatchSync::Data &data, int id);

public Q_SLOTS:
    StopWatch *newStopWatch(bool force = false);

private Q_SLOTS:
    void stopWatchClosed(StopWatch *stopWatch);
    void synchronizeWatchSet(StopWatch *stopWatch, bool state);
    void requestSynchronization(int id);

private: // Variables
    QWidget *m_mainWindow;
    SharedObjects *m_sharedObjects;
    QVector<StopWatch *> m_stopWatches;
    int m_stopWatchCounter = 0;
    QHash<int, StopWatch *> m_stopWatchIdMap;

    Server *m_server = nullptr;
    Client *m_client = nullptr;
    bool m_isServer = false;
    bool m_isClient = false;
    StopWatch *m_synchronizeWatch = nullptr;

};

#endif // STOPWATCHENGINE_H
