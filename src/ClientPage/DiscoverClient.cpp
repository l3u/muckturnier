// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "DiscoverClient.h"

#include "network/DiscoverProtocol.h"

// Qt includes
#include <QDebug>
#include <QTimer>

namespace dp = DiscoverProtocol;

DiscoverClient::DiscoverClient(QObject *parent, int port, int discoverTimeout)
    : AbstractDiscoverEngine(parent, port)
{
    m_timer = new QTimer(this);
    m_timer->setInterval(discoverTimeout);
    m_timer->setSingleShot(true);
    connect(m_timer, &QTimer::timeout, this, &DiscoverClient::discoverTimeout);
}

bool DiscoverClient::discoverServer()
{
    if (! listen()) {
        return false;
    }

    m_timer->start();
    broadcastData(dp::searchingServerMessage);
    return true;
}

void DiscoverClient::discoverTimeout()
{
    close();
    Q_EMIT discoverResult(QString(), -1);
}

void DiscoverClient::processData(const QJsonObject &jsonObject, const QHostAddress &)
{
    if (jsonObject.value(dp::action) != dp::announcingServer) {
        return;
    }

    m_timer->stop();
    close();
    Q_EMIT discoverResult(jsonObject.value(dp::ip).toString(),
                          jsonObject.value(dp::port).toInt());
}
