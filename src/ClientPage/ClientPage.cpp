// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "ClientPage.h"
#include "Client.h"
#include "DifferencesDelegate.h"
#include "DiscoverClient.h"

#include "Database/Database.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/StringTools.h"
#include "SharedObjects/Settings.h"

#include "network/SocketAddressWidget.h"
#include "network/LogDisplay.h"

#include "shared/SharedStyles.h"
#include "shared/Urls.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QMessageBox>
#include <QApplication>
#include <QGroupBox>
#include <QTableWidget>
#include <QHeaderView>
#include <QTimer>
#include <QScrollBar>
#include <QUdpSocket>
#include <QNetworkInterface>

// C++ includes
#include <functional>

static const QChar s_space = QLatin1Char(' ');

ClientPage::ClientPage(QWidget *parent, SharedObjects *sharedObjects,
                       RegistrationPage *registrationPage, ScorePage *scorePage,
                       StopWatchEngine* stopWatchEngine, const QString &ip, int port)
    : OptionalPage(parent),
      m_stringTools(sharedObjects->stringTools()),
      m_db(sharedObjects->database())
{
    Settings *settings = sharedObjects->settings();
    m_discoverAttempts = settings->discoverAttempts();
    m_connecting = false;

    m_discoverClient = new DiscoverClient(this, settings->discoverPort(),
                                          settings->discoverTimeout());
    connect(m_discoverClient, &DiscoverClient::discoverResult,
            this, &ClientPage::processDiscoverResult);

    m_client = new Client(this, settings->connectTimeout(), settings->requestTimeout(),
                          sharedObjects, registrationPage, scorePage, stopWatchEngine);
    connect(m_client, &Client::protocolError, this, &ClientPage::connectionError);
    connect(m_client, &Client::connectResult, this, &ClientPage::checkConnectResult);
    connect(m_client, &Client::connectionError, this, &ClientPage::connectionError);
    connect(m_client, &Client::requestRejected, this, &ClientPage::requestRejected);
    connect(m_client, &Client::statusUpdate, this, &ClientPage::statusUpdate);
    connect(m_client, &Client::namesDiffer, this, &ClientPage::showDifferingNames);

    // Settings box

    m_settingsBox = new QGroupBox(tr("Socket-Adresse des Servers"));
    auto *mainSettingsLayout = new QVBoxLayout(m_settingsBox);
    auto *settingsLayout = new QHBoxLayout;
    mainSettingsLayout->addLayout(settingsLayout);
    layout()->addWidget(m_settingsBox);

    m_notConnectedMessage = tr("<span style=\"%1\">Nicht<br/>verbunden</span>").arg(
                               SharedStyles::redText);
    m_connectedLabel = new QLabel(m_notConnectedMessage);
    settingsLayout->addWidget(m_connectedLabel);

    m_searchServerButton = new QPushButton(tr("Server suchen"));
    connect(m_searchServerButton, &QPushButton::clicked, this, &ClientPage::searchServer);
    settingsLayout->addWidget(m_searchServerButton);

    m_socketAddress = new SocketAddressWidget(SocketAddressWidget::Mode::Client,
                                              settings->serverPort(),
                                              settings->preferIpv6());
    connect(m_socketAddress, &SocketAddressWidget::returnPressed,
            this, &ClientPage::connectToServer);
    settingsLayout->addWidget(m_socketAddress);

    m_connectButton = new QPushButton(tr("Verbinden"));
    m_connectButton->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogYesButton));
    settingsLayout->addWidget(m_connectButton);
    connect(m_socketAddress, &SocketAddressWidget::settingsOkay,
            m_connectButton, &QPushButton::setEnabled);
    connect(m_connectButton, &QPushButton::clicked, this, &ClientPage::connectToServer);
    if (! ip.isEmpty()) {
        // This will also enable the connect button
        m_socketAddress->setSocketAddress(ip, port);
    }

    m_disconnectButton = new QPushButton(tr("Verbindung beenden"));
    m_disconnectButton->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogCancelButton));
    settingsLayout->addWidget(m_disconnectButton);
    m_disconnectButton->hide();
    connect(m_disconnectButton, &QPushButton::clicked, this, &ClientPage::closeConnection);

    settingsLayout->addStretch();

    m_closeButton = new QPushButton(tr("Schließen"));
    m_closeButton->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogCloseButton));
    settingsLayout->addWidget(m_closeButton);
    connect(m_closeButton, &QPushButton::clicked, this, &ClientPage::closePage);

    m_warningLabel = new QLabel(tr(
        "<span style=\"%1 %2\">Achtung!</span> Wenn eine Verbindung hergestellt wird, werden "
        "<span style=\"%2\">alle lokalen Anmeldungen, Auslosungen und Markierungen "
        "verworfen</span> und der Datenbestand des Servers übernommen!").arg(
        SharedStyles::boldText, SharedStyles::redText));
    m_warningLabel->setWordWrap(true);
    mainSettingsLayout->addWidget(m_warningLabel);

    // Differences box

    m_differencesBox = new QGroupBox(tr("Unterschiede der Anmeldungsliste"));
    auto *differencesLayout = new QVBoxLayout(m_differencesBox);
    layout()->addWidget(m_differencesBox);

    m_differences = new QTableWidget;
    m_differences->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_differences->setItemDelegate(new DifferencesDelegate(this));
    m_differences->verticalHeader()->hide();
    m_differences->setShowGrid(false);
    m_differences->setWordWrap(false);
    m_differences->setSelectionMode(QTableWidget::NoSelection);
    m_differences->setFocusPolicy(Qt::NoFocus);
    m_differences->setContextMenuPolicy(Qt::NoContextMenu);
    m_differences->setColumnCount(2);
    m_differences->setHorizontalHeaderLabels({ tr("Name hier"), tr("Name Server") });
    differencesLayout->addWidget(m_differences);

    auto *differencesButtonsLayout = new QHBoxLayout;
    differencesLayout->addLayout(differencesButtonsLayout);

    differencesButtonsLayout->addStretch();

    m_adoptListButton = new QPushButton(tr("Namen vom Server übernehmen und neu verbinden"));
    m_adoptListButton->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogYesButton));
    differencesButtonsLayout->addWidget(m_adoptListButton);
    connect(m_adoptListButton, &QPushButton::clicked, this, &ClientPage::adoptList);

    m_cancelButton = new QPushButton(tr("Abbrechen"));
    m_cancelButton->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogCancelButton));
    differencesButtonsLayout->addWidget(m_cancelButton);
    connect(m_cancelButton, &QPushButton::clicked, this, &ClientPage::cancelAdoption);

    m_differencesBox->hide();

    // Display box

    m_displayBox = new QGroupBox(tr("Netzwerkinformationen"));
    auto *connectedLayout = new QVBoxLayout(m_displayBox);
    layout()->addWidget(m_displayBox);

    m_display = new LogDisplay;
    connect(m_client, &Client::message, m_display, &LogDisplay::displayMessage);
    connect(m_client, &AbstractNetworkEngine::protocolError,
            m_display, &LogDisplay::displayProtocolError);
    connectedLayout->addWidget(m_display);

    m_disconnectButton->hide();

    // We have to do this via QTimer call so that the page shows up before we continue
    QTimer::singleShot(0, this, std::bind(&ClientPage::finishConstruction, this, ! ip.isEmpty()));
}

void ClientPage::finishConstruction(bool ipGiven)
{
    if (ipGiven) {
        if (QMessageBox::question(this,
            tr("Mit Server verbinden"),
            tr("Soll die letzte benutzte Verbindung zu dem Muckturnier-Server unter IP %1 Port %2 "
               "wiederhergestellt werden?").arg(m_socketAddress->ip(),
                                                QString::number(m_socketAddress->port())),
            QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes) == QMessageBox::Yes) {

            connectToServer();
        } else {
            m_connectButton->setFocus();
        }
    } else {
        searchServer();
    }
}

void ClientPage::connectToServer()
{
    if (! m_connectButton->isEnabled()) {
        return;
    }

    m_display->clear();
    Q_EMIT connectionRequested(m_socketAddress->ip(), m_socketAddress->port());
    m_connecting = true;
    QApplication::setOverrideCursor(Qt::WaitCursor);
    m_settingsBox->setEnabled(false);
    m_searchServerButton->setEnabled(false);
    m_connectButton->setEnabled(false);
    m_closeButton->setEnabled(false);
    m_client->connectToServer(QHostAddress(m_socketAddress->ip()), m_socketAddress->port());
}

void ClientPage::resetInterface()
{
    QApplication::restoreOverrideCursor();
    m_connecting = false;
    m_settingsBox->setEnabled(true);
    m_searchServerButton->setEnabled(true);
    m_connectButton->setEnabled(true);
    m_closeButton->setEnabled(true);
}

void ClientPage::checkConnectResult(bool success, const QString &error)
{
    if (success) {
        resetInterface();
        m_warningLabel->hide();
        m_socketAddress->hide();
        m_connectedLabel->setText(tr("<span style=\"%1\">Verbindung hergestellt</span><br/>"
                                     "Server: <b>%2</b> Port: <b>%3</b><br/>"
                                     "Client-ID: <b>%4</b>").arg(SharedStyles::greenText,
                                                          m_client->serverIp(),
                                                          QString::number(m_client->serverPort()),
                                                          QString::number(m_client->id())));
        m_searchServerButton->hide();
        m_connectButton->hide();
        m_disconnectButton->show();
        m_closeButton->show();
        m_closeButton->clearFocus();
        Q_EMIT connected();
        Q_EMIT statusUpdate(tr("Verbindung mit Server unter IP %1 Port %2 hergestellt").arg(
                             m_client->serverIp(), QString::number(m_client->serverPort())));
        if (m_stringTools->localeChanged()) {
            QTimer::singleShot(0, this, [this]
            {
                QMessageBox::information(this,
                    tr("Verbindung hergestellt"),
                    tr("<p><b>Abweichendes Gebietsschema</b></p>"
                       "<p>Der Server benutzt eine anderes Gebietsschema („%1“) als die hier "
                       "ausgeführte Instanz von Muckturnier („%2“). Für die Zeit der Verbindung "
                       "wird das Server-Gebietsschema übernommen. Das führt evtl. zu einer anderen "
                       "Sortierung von Namen; alle Teilnehmer müssen gleich sortieren.</p>"
                       "<p>Nach dem Beenden der Verbindung wird die lokale Einstellung für das "
                       "Gebietsschema wiederhergestellt.</p>").arg(m_stringTools->locale(),
                                                                   m_stringTools->defaultLocale()));
            });
        }
    } else {
        m_client->abortHandshake();
        m_display->displayMessage(tr("Herstellen der Verbindung ist fehlgeschlagen"));
        resetInterface();
        QMessageBox::warning(this, tr("Mit Server verbinden"),
            tr("<p><b>Es kann keine Verbindung mit dem Server %1 hergestellt werden:</p>"
               "<p>%2</p>").arg(m_socketAddress->ip(), error));
    }
}

void ClientPage::connectionError(const QString &text)
{
    Q_EMIT raiseMe();
    m_display->displayMessage(tr("Verbindungsfehler: %1").arg(text));
    m_client->closeConnection();
    closeConnection();
    resetInterface();
    QMessageBox::warning(this, tr("Verbindungsfehler"),
                         tr("<p>Es ist ein Fehler in der Serververbindung aufgetreten:</p>"
                            "<p>%1</p>").arg(text));
}

bool ClientPage::closeConnection()
{
    if (m_client->connected()
        && QMessageBox::warning(this, tr("Serververbindung schließen"),
               tr("<p>Wenn die Serververbindung getrennt wird, dann werden keine Änderungen mehr "
                  "an die anderen Netzwerkteilnehmer gesendet oder von ihnen empfangen!</p>"
                  "<p>Trotzdem schließen?</p>"),
               QMessageBox::Ok | QMessageBox::Cancel,
               QMessageBox::Cancel) == QMessageBox::Cancel) {

               return false;
    }

    m_client->closeConnection();
    m_display->displayMessage(tr("Serververbindung beendet"));
    m_connectedLabel->setText(m_notConnectedMessage);
    m_searchServerButton->show();
    m_connectButton->show();
    m_disconnectButton->hide();
    m_socketAddress->show();
    m_connectButton->setFocus();

    Q_EMIT connectionClosed();
    Q_EMIT statusUpdate(tr("Serververbindung beendet"));

    return true;
}

void ClientPage::requestRejected(ServerProtocol::RejectReason reason)
{
    m_display->displayMessage(tr("Anfrage vom Server zurückgewiesen"));
    QString message;
    switch (reason) {
    case ServerProtocol::RejectReasonOutdatedRequest:
        message = tr("<p>Der Server hat die letzte Anfrage zurückgewiesen, da der Datenbestand "
                     "sich während der Anfrage verändert hat. Die Anfrage wird ignoriert.</p>"
                     "<p>Bitte die Änderung erneut anfragen!</p>");
        break;
    case ServerProtocol::RejectReasonUnknownAction:
        message = tr("<p>Die Anfrage wurde vom Server nicht verstanden. Das sollte eigentlich "
                     "nicht passieren, bitte mit möglichst vielen Informationen (wie kann man das "
                     "reproduzieren?) als Fehler melden!</p>"
                     "<p>Der Bugtracker ist unter <a href=\"%1\">%1</a> zu finden.</p>").arg(
                     Urls::bugtracker);
        break;
    }
    QMessageBox::warning(qobject_cast<QWidget *>(parent()),
                         tr("Anfrage vom Server zurückgewiesen"), message);
}

bool ClientPage::closePage()
{
    // This will only do something if a handshake is currently being processed
    client()->abortHandshake();

    if (! closeConnection()) {
        return false;
    }

    return OptionalPage::closePage();
}

void ClientPage::showDifferingNames(QStringList &localNames, QStringList &remoteNames)
{
    Q_EMIT namesDifferencesDisplayed(true);

    removeIntersection(localNames, remoteNames);
    QHash<int, int> assignment = matchLists(localNames, remoteNames);

    m_settingsBox->hide();
    m_differencesBox->show();
    m_displayBox->hide();

    m_differences->clearContents();
    m_differences->setRowCount(localNames.count());
    for (int i = 0; i < localNames.count(); i++) {
        const QString lcs = longestCommonSubsequence(localNames.at(i),
                                                     remoteNames.at(assignment[i]));
        QTableWidgetItem *localName = new QTableWidgetItem(localNames.at(i));
        QTableWidgetItem *remoteName = new QTableWidgetItem(remoteNames.at(assignment[i]));
        localName->setData(Qt::UserRole, QVariant(diffList(localNames.at(i), lcs)));
        remoteName->setData(Qt::UserRole, QVariant(diffList(remoteNames.at(assignment[i]), lcs)));
        m_differences->setItem(i, 0, localName);
        m_differences->setItem(i, 1, remoteName);
    }
    m_differences->resizeColumnsToContents();
}

void ClientPage::adoptList()
{
    QStringList names;
    QStringList newNames;
    for (int i = 0; i < m_differences->rowCount(); i++) {
        names.append(m_differences->item(i, 0)->text());
        newNames.append(m_differences->item(i, 1)->text());
    }
    m_client->adoptList(names, newNames);
    cancelAdoption();
    connectToServer();
}

void ClientPage::cancelAdoption()
{
    Q_EMIT namesDifferencesDisplayed(false);
    m_settingsBox->show();
    m_differencesBox->hide();
    m_displayBox->show();
    m_display->clear();
}

Client *ClientPage::client() const
{
    return m_client;
}

void ClientPage::searchServer()
{
    if (m_currentDiscoverAttempts == 0) {
        m_currentDiscoverAttempts = m_discoverAttempts;
    }

    if (! m_discoverClient->discoverServer()) {
        QMessageBox::warning(this,
            tr("Muckturnier-Server suchen"),
            tr("<p><b>Der Discover-Client konnte nicht gestartet werden</b></p>"
               "<p>Die Fehlermeldung war: %1</p>"
               "<p>Es kann nicht automatisch nach einem Muckturnier-Server gesucht werden. Bitte "
               "die Socket-Adresse (IP-Adresse und Port) des Servers manuell eingeben.</p>").arg(
               m_discoverClient->errorString()));
        return;
    }

    m_settingsBox->setEnabled(false);
    m_display->displayMessage(tr("Suche nach einem Muckturnier-Server via UDP-Port %1 (%n "
                                 "Versuch(e) verbleiben) …", "", m_currentDiscoverAttempts).arg(
                                 m_discoverClient->port()));
    QApplication::setOverrideCursor(Qt::WaitCursor);
}

void ClientPage::processDiscoverResult(const QString &ip, int port)
{
    m_settingsBox->setEnabled(true);
    QApplication::restoreOverrideCursor();

    if (ip.isEmpty()) {
        m_display->displayMessage(tr("Es wurde kein Muckturnier-Server gefunden"));
        if (--m_currentDiscoverAttempts > 0) {
            searchServer();
        } else {
            m_socketAddress->focusIp();
        }

    } else {
        m_currentDiscoverAttempts = 0;
        m_display->displayMessage(tr("Muckturnier-Server mit IP %1 auf Port %2 gefunden").arg(
                                     ip, QString::number(port)));

        bool scopeIdChanged = false;
        QString scopeId;
        QHostAddress address(ip);
        if (address.protocol() == QAbstractSocket::IPv6Protocol && ! address.scopeId().isEmpty()) {
            // Get the possibly correct scope id for an IPv6 link-local address
            const auto allInterfaces = QNetworkInterface::allInterfaces();
            for (const QNetworkInterface &interface : allInterfaces) {
                const QNetworkInterface::InterfaceFlags flags = interface.flags();
                if (flags & QNetworkInterface::IsUp
                    && flags & QNetworkInterface::IsRunning
                    && flags & QNetworkInterface::CanBroadcast) {

                    scopeId = interface.name();
                    break;
                }
            }
            address.setScopeId(scopeId);
            scopeIdChanged = true;
        }

        m_socketAddress->setSocketAddress(address.toString(), port);
        QString message = tr("<p>Es wurde ein Muckturnier-Server mit der <b>IP %1</b> und "
                             "<b>Port %2</b> gefunden.").arg(ip, QString::number(port));
        if (scopeIdChanged) {
            message += tr("</p><p>Diese Adresse ist eine <b>IPv6-Link-Local-Adresse</b>, die die "
                          "passende <b>Scope-ID</b> enthalten muss. Diese wurde auf <b>„%1“</b> "
                          "gesetzt. Sollte das nicht korrekt sein, dann bitte korrigieren und "
                          "manuell verbinden.</p><p>").arg(scopeId);
        }
        message += tr("Soll jetzt eine Verbindung mit diesem Server hergestellt werden?</p>");

        if (QMessageBox::question(this, tr("Mit Server verbinden"), message,
            QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes) == QMessageBox::Yes) {

            connectToServer();
        } else {
            QTimer::singleShot(0, this, [this]
            {
                m_connectButton->setFocus();
            });
        }
    }
}

QHash<int, int> ClientPage::matchLists(QStringList &source, QStringList &target) const
{
    QHash<int, QHash<int, int>> allDistances;
    QVector<int> remainingTargetIndices;
    QHash<int, int> assignment;

    // Calculate all distances for all possible pairings
    for (int i = 0; i < source.count(); i++) {
        allDistances[i] = combinedLevenshteinDistance(source.at(i), target);
        remainingTargetIndices.append(i);
    }

    // Calculate the best pairing
    while (! allDistances.isEmpty()) {
        int distance = -1;
        int bestSourceIndex;
        const auto allDistancesKeys = allDistances.keys();
        for (const int sourceIndex : allDistancesKeys) {
            for (int targetIndex : allDistances[sourceIndex].keys()) {
                if (distance == -1 || allDistances[sourceIndex][targetIndex] < distance) {
                    distance = allDistances[sourceIndex][targetIndex];
                    bestSourceIndex = sourceIndex;
                }
            }
        }

        while (true) {
            int matchDistance = -1;
            int bestTargetIndex;
            const auto allDistancesBestSourceIndexKeys = allDistances[bestSourceIndex].keys();
            for (int targetIndex : allDistancesBestSourceIndexKeys) {
                if (matchDistance == -1
                    || allDistances[bestSourceIndex][targetIndex] < matchDistance) {

                    matchDistance = allDistances[bestSourceIndex][targetIndex];
                    bestTargetIndex = targetIndex;
                }
            }

            if (remainingTargetIndices.contains(bestTargetIndex)) {
                assignment[bestSourceIndex] = bestTargetIndex;
                remainingTargetIndices.removeOne(bestTargetIndex);
                break;
            } else {
                allDistances[bestSourceIndex].remove(bestTargetIndex);
            }
        }

        allDistances.remove(bestSourceIndex);
    }

    return assignment;
}

int ClientPage::levenshteinDistance(const QString &source, const QString &target) const
{
    // Mostly stolen from https://qgis.org/api/2.14/qgsstringutils_8cpp_source.html

    if (source == target) {
        return 0;
    }

    const int sourceCount = source.size();
    const int targetCount = target.size();
    if (source.isEmpty()) {
        return targetCount;
    }
    if (target.isEmpty()) {
        return sourceCount;
    }
    if (sourceCount > targetCount) {
        return levenshteinDistance(target, source);
    }

    QVector<int> column;
    column.fill(0, targetCount + 1);
    QVector<int> previousColumn;
    previousColumn.reserve(targetCount + 1);
    for (int i = 0; i < targetCount + 1; i++) {
        previousColumn.append(i);
    }

    for (int i = 0; i < sourceCount; i++) {
        column[0] = i + 1;
        for (int j = 0; j < targetCount; j++) {
            column[j + 1]
                = std::min({ 1 + column.at(j),
                             1 + previousColumn.at(1 + j),
                             previousColumn.at(j) + ((source.at(i) == target.at(j)) ? 0 : 1) });
        }
        column.swap(previousColumn);
    }

    return previousColumn.at(targetCount);
}

QHash<int, int> ClientPage::combinedLevenshteinDistance(const QString &text,
                                                        const QStringList &candidates) const
{
    QHash<int, int> combinedDistances;
    const auto words = text.split(s_space);

    // Calculate a combined distance for each candidate

    for (int i = 0; i < candidates.count(); i++) {
        auto textWords = words;
        auto candidateWords = candidates.at(i).split(s_space);
        removeIntersection(textWords, candidateWords);

        const int textWordsCount = textWords.count();
        const int candidateWordsCount = candidateWords.count();
        int distance = 0;
        int biggestDistance = 0;
        QHash<int, QHash<int, int>> distances;

        // Calculate a distance matrix for each word pair
        for (int i = 0; i < textWordsCount; i++) {
            for (int j = 0; j < candidateWordsCount; j++) {
                const int currentDistance = levenshteinDistance(textWords.at(i),
                                                                candidateWords.at(j));
                distances[i][j] = currentDistance;
                if (biggestDistance < currentDistance) {
                    biggestDistance = currentDistance;
                }
            }
        }

        // Search for the smallest distance of each word and remove it from the candidate list

        while (! distances.isEmpty()) {
            int smallestDistance = biggestDistance + 1;

            // Those two are initialized in each case by the below loop, because in the first
            // iteration, distances[textWordIndex][candidateWordIndex] will be smaller than
            // smallestDistance (initialized to biggestDistance + 1)
            int smallestDistanceWordIndex;
            int smallestDistanceCandidateIndex;

            const auto distancesKeys = distances.keys();
            for (const int textWordIndex : distancesKeys) {
                const auto distancesTextWordIndexKeys = distances.value(textWordIndex).keys();
                for (const int candidateWordIndex : distancesTextWordIndexKeys) {
                    const int distance = distances.value(textWordIndex).value(candidateWordIndex);
                    if (distance < smallestDistance) {
                        smallestDistance = distance;
                        smallestDistanceWordIndex = textWordIndex;
                        smallestDistanceCandidateIndex = candidateWordIndex;
                    }
                }
            }

            distance += smallestDistance;
            distances.remove(smallestDistanceWordIndex);
            const auto processedDistancesKeys = distances.keys();
            for (const int textWordIndex : processedDistancesKeys) {
                distances[textWordIndex].remove(smallestDistanceCandidateIndex);
                if (distances.value(textWordIndex).isEmpty()) {
                    distances.remove(textWordIndex);
                }
            }
        }

        // This is the smallest possible distance of the respective name and the current candidate
        combinedDistances[i] = distance;
    }

    return combinedDistances;
}

void ClientPage::removeIntersection(QStringList &list1, QStringList &list2) const
{
    const QStringList workList(list1);
    for (const QString &entry : workList) {
        if (list2.removeOne(entry)) {
            list1.removeOne(entry);
        }
    }
}

QString ClientPage::longestCommonSubsequence(const QString &source, const QString &target) const
{
    // Mostly stolen from https://www.geeksforgeeks.org/printing-longest-common-subsequence/

    const int sourceCount = source.size();
    const int targetCount = target.size();

    QHash<int, QHash<int, int>> l;
    for (int i = 0; i <= sourceCount; i++) {
        for (int j = 0; j <= targetCount; j++) {
            if (i == 0 || j == 0) {
                l[i][j] = 0;
            } else if (source.at(i - 1) == target.at(j - 1)) {
                l[i][j] = l.value(i - 1).value(j - 1) + 1;
            } else {
                l[i][j] = std::max(l.value(i - 1).value(j), l.value(i).value(j - 1));
            }
        }
    }

    int i = sourceCount;
    int j = targetCount;
    int index = l.value(sourceCount).value(targetCount);
    auto lcs = QStringLiteral().leftJustified(index, QLatin1Char(' '));
    while (i > 0 && j > 0) {
        if (source.at(i - 1) == target.at(j - 1)) {
            lcs[index - 1] = source.at(i - 1);
            i--;
            j--;
            index--;
        } else if (l.value(i - 1).value(j) > l.value(i).value(j - 1)) {
            i--;
        } else {
            j--;
        }
    }

    return lcs;
}

QStringList ClientPage::diffList(QString text, QString lcs) const
{
    if (text.isEmpty()) {
        return QStringList { text };
    }

    if (lcs.isEmpty()) {
        return QStringList { QString(), text };
    }

    QStringList allParts;
    QString currentPart;
    bool matching = true;
    if (text.at(0) != lcs.at(0)) {
        allParts.append(QString());
        matching = false;
    }

    while (! text.isEmpty()) {
        if (matching) {
            while (! text.isEmpty() && ! lcs.isEmpty()
                   && text.at(0) == lcs.at(0)) {

                currentPart.append(text.at(0));
                text.remove(0, 1);
                lcs.remove(0, 1);
            }

            allParts.append(currentPart);
            currentPart.clear();
            matching = false;

        } else {
            if (lcs.isEmpty()) {
                allParts.append(text);
                text.clear();
            } else {
                while (! text.isEmpty() && text.at(0) != lcs.at(0)) {
                    currentPart.append(text.at(0));
                    text.remove(0, 1);
                }
            }

            if (! currentPart.isEmpty()) {
                allParts.append(currentPart);
                currentPart.clear();
            }
            matching = true;
        }
    }

    return allParts;
}
