// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef DISCOVERCLIENT_H
#define DISCOVERCLIENT_H

// Local includes
#include "network/AbstractDiscoverEngine.h"

// Qt includes
#include <QJsonObject>

// Qt classes
class QTimer;

class DiscoverClient : public AbstractDiscoverEngine
{
    Q_OBJECT

public:
    explicit DiscoverClient(QObject *parent, int port, int discoverTimeout);
    bool discoverServer();

Q_SIGNALS:
    void discoverResult(const QString &ip, int port);

protected:
    void processData(const QJsonObject &jsonObject, const QHostAddress &) override;

private Q_SLOTS:
    void discoverTimeout();

private: // Variables
    QTimer *m_timer;

};

#endif // DISCOVERCLIENT_H
