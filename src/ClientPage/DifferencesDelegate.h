// SPDX-FileCopyrightText: 2019-2020 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef DIFFERENCESDELEGATE_H
#define DIFFERENCESDELEGATE_H

// Local includes
#include "shared/TableDelegate.h"

class DifferencesDelegate : public TableDelegate
{
    Q_OBJECT

public:
    explicit DifferencesDelegate(QObject *parent);

protected:
    void drawText(QPainter *painter, const QStyleOptionViewItem &option,
                  const QModelIndex &index) const override;

};

#endif // DIFFERENCESDELEGATE_H
