// SPDX-FileCopyrightText: 2018-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "Client.h"

#include "network/ScoreHelper.h"
#include "network/StopWatchSync.h"

#include "RegistrationPage/RegistrationPage.h"
#include "RegistrationPage/MarkersWidget.h"

#include "ScorePage/ScorePage.h"

#include "SchedulePage/StopWatchEngine.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/StringTools.h"
#include "SharedObjects/Settings.h"
#include "SharedObjects/TournamentSettings.h"

#include "shared/Tournament.h"
#include "shared/JsonHelper.h"
#include "shared/Players.h"
#include "shared/ColorHelper.h"

// Qt includes
#include <QDebug>
#include <QHostAddress>
#include <QTcpSocket>
#include <QTimer>
#include <QJsonObject>
#include <QJsonArray>
#include <QColor>
#include <QApplication>
#include <QLocale>

namespace sp = ServerProtocol;

Client::Client(QObject *parent, int connectTimeout, int requestTimeout,
               SharedObjects *sharedObjects, RegistrationPage *registrationPage,
               ScorePage *scorePage, StopWatchEngine *stopWatchEngine)
    : AbstractNetworkEngine(parent),
      m_connectTimeout(connectTimeout),
      m_db(sharedObjects->database()),
      m_settings(sharedObjects->settings()),
      m_tournamentSettings(sharedObjects->tournamentSettings()),
      m_stringTools(sharedObjects->stringTools()),
      m_registrationPage(registrationPage),
      m_markersWidget(registrationPage->markersWidget()),
      m_scorePage(scorePage),
      m_stopWatchEngine(stopWatchEngine),

      m_requestHandlers {
          // Registration changes

          { sp::registerPlayers, &Client::processRegisterPlayers },
          { sp::renamePlayers,   &Client::processRenamePlayers },
          { sp::assemblePair,    &Client::processAssemblePair },

          { sp::deletePlayers,         &Client::processDeletePlayers },
          { sp::deleteMarkedPlayers,   &Client::processDeleteMarkedPlayers },
          { sp::deleteAllPlayers,      &Client::processDeleteAllPlayers },

          { sp::setMarker,        &Client::processSetMarker },
          { sp::setListMarker,    &Client::processSetListMarker },
          { sp::removeListMarker, &Client::processRemoveListMarker },

          { sp::setNumber, &Client::processSetNumber },

          { sp::setBookingChecksum, &Client::processSetBookingChecksum },

          { sp::setDisqualified, &Client::processSetDisqualified },

          // Draw changes
          { sp::setDraw,        &Client::processSetDraw },
          { sp::setListDraw,    &Client::processSetListDraw },
          { sp::deleteAllDraws, &Client::processDeleteAllDraws },

          // Markers definition changes
          { sp::addMarker,    &Client::processAddMarker },
          { sp::deleteMarker, &Client::processDeleteMarker },
          { sp::moveMarker,   &Client::processMoveMarker },
          { sp::editMarker,   &Client::processEditMarker },

          // Special markers changes
          { sp::setBookedMarker,         &Client::processSetBookedMarker },
          { sp::singlesManagementChange, &Client::processSinglesManagementChange },

          // Finishing the registration
          { sp::finishRegistration, &Client::processFinishRegistration },

          // Score comparison
          { sp::scoreChecksumChange, &Client::processScoreChecksumChange },
          { sp::scoreRequest, &Client::processScoreRequest },

          // Stop watch synchronization
          { sp::stopWatchSynchronization, &Client::processStopWatchSynchronization }
      }
{
    connect(socket(), &QAbstractSocket::errorOccurred, this, &Client::error);
    connect(socket(), &QAbstractSocket::connected, this, &Client::socketConnected);

    m_connectionTimer = new QTimer(this);
    m_connectionTimer->setInterval(connectTimeout);
    m_connectionTimer->setSingleShot(true);
    connect(m_connectionTimer, &QTimer::timeout, this, &Client::connectionTimeout);

    m_requestTimer = new QTimer(this);
    m_requestTimer->setInterval(requestTimeout);
    m_requestTimer->setSingleShot(true);
    connect(m_requestTimer, &QTimer::timeout, this, &Client::requestTimeout);
}

void Client::setChecksum(const QString *checksum)
{
    m_checksum = checksum;
}

void Client::connectToServer(const QHostAddress &address, quint16 port)
{
    Q_EMIT message(tr("Verbinde mit Server %1").arg(address.toString()));
    m_connectionPending = true;
    m_connectionTimer->start();
    Q_EMIT message(tr("Stelle Socketverbindung her …"));
    socket()->connectToHost(address, port);
}

QString Client::serverIp() const
{
    return socket()->peerAddress().toString();
}

int Client::serverPort() const
{
    return socket()->peerPort();
}

void Client::socketConnected()
{
    Q_EMIT message(tr("Socketverbindung hergestellt, initialisiere Handshake …"));
    m_connectionPending = false;
    m_handshakePending = true;
}

void Client::abortHandshake()
{
    if (! m_handshakePending) {
        return;
    }

    closeConnection();
    m_connectionTimer->stop();
    m_handshakePending = false;
}

void Client::error(QAbstractSocket::SocketError)
{
    closeConnection();

    if (m_connectionPending || m_handshakePending) {
        m_connectionTimer->stop();
        m_handshakePending = false;
        m_connectionPending = false;
        Q_EMIT connectResult(false, socket()->errorString());
        return;
    }

    Q_EMIT connectionError(socket()->errorString());
}

void Client::connectionTimeout()
{
    if (m_connectionPending) {
        Q_EMIT connectionError(tr("Konnte keine Socketverbindung herstellen."));
        return;
    }

    closeConnection();
    QLocale locale;
    Q_EMIT message(tr("Timeout: Keine Antwort nach %1 Sekunden").arg(
                      locale.toString(float(m_connectTimeout) / 1000.0)));
    Q_EMIT connectResult(false, tr("Der Server hat zu lange keine Antwort geschickt."));
}

void Client::closeConnection()
{
    socket()->abort();
    m_connected = false;
}

void Client::sendRequest(QJsonObject &jsonObject, ProcessingTarget target,
                         const QString &request, const QString &scrollToTarget)
{
    Q_EMIT tx();
    Q_EMIT message(tr("Frage %1 an").arg(request));
    Q_EMIT statusUpdate(tr("Frage %1 beim Server an …").arg(request));

    switch (target) {
    case ProcessingTarget::NoTarget:
        break;
    case ProcessingTarget::TargetRegistration:
        m_registrationPage->setProcessingRequest(true, scrollToTarget);
        break;
    case ProcessingTarget::TargetMarkers:
        m_markersWidget->setProcessingRequest(true);
        break;
    case ProcessingTarget::TargetScore:
        m_scorePage->setProcessingRequest(true);
        break;
    case ProcessingTarget::TargetStopWatch:
        m_stopWatchEngine->setProcessingRequest(true);
        break;
    }

    if (JsonHelper::doChecksumCheck(jsonObject)) {
        jsonObject.insert(sp::checksum, *m_checksum);
    }

    jsonObject.insert(sp::clientId, m_id);
    sendData(jsonObject);

    m_processingTarget.append(target);
    m_requestTimer->start();
}

int Client::id() const
{
    return m_id;
}

void Client::requestTimeout()
{
    m_registrationPage->setProcessingRequest(false);
    m_markersWidget->setProcessingRequest(false);
    m_scorePage->setProcessingRequest(false);
    m_stopWatchEngine->setProcessingRequest(false);
    Q_EMIT connectionError(tr("Der Server hat zu lange nicht auf eine Anfrage reagiert. Bitte die "
                              "Netzwerkverbindung überprüfen und neu verbinden."));
    closeConnection();
}

bool Client::connected() const
{
    return m_connected;
}

void Client::stopProcessingRequest()
{
    m_requestTimer->stop();
    switch (m_processingTarget.takeFirst()) {
    case ProcessingTarget::NoTarget:
        break;
    case ProcessingTarget::TargetRegistration:
        m_registrationPage->setProcessingRequest(false);
        break;
    case ProcessingTarget::TargetMarkers:
        m_markersWidget->setProcessingRequest(false);
        break;
    case ProcessingTarget::TargetScore:
        m_scorePage->setProcessingRequest(false);
        break;
    case ProcessingTarget::TargetStopWatch:
        m_stopWatchEngine->setProcessingRequest(false);
        break;
    }
}

void Client::verifyChecksum(const QString &checksum)
{
    if (checksum != *m_checksum) {
        // This should not happen. But who knows?!
        while (! m_processingTarget.isEmpty()) {
            stopProcessingRequest();
        }
        Q_EMIT connectionError(tr("Die Synchronisation mit dem Server ist fehlgeschlagen, die "
                                  "Daten weichen ab. Die Verbindung wird beendet. Bitte neu "
                                  "verbinden, um den Datenbestand wieder mit dem Server "
                                  "abzugleichen!"));
    }
}

// ====================
// Handshake processing
// ====================

void Client::processHandshake(const QJsonObject &jsonObject)
{
    const QJsonValue phaseValue = jsonObject.value(sp::phase);
    if (phaseValue.type() != QJsonValue::String) {
        Q_EMIT connectResult(false, tr("Der Server hat eine ungültige Begrüßung geschickt"));
        return;
    }

    const QString handshakePhase = phaseValue.toString();

    // Handshake Initialization
    // ========================

    if (handshakePhase == sp::initialization) {

        const QJsonValue serverSignatureValue = jsonObject.value(sp::serverSignatureKey);
        if (serverSignatureValue.type() != QJsonValue::String
            || serverSignatureValue.toString() != sp::serverSignature) {

            Q_EMIT message(tr("Der Server hat eine ungültige Begrüßung geschickt."));
            Q_EMIT connectResult(false, tr("Der Server hat eine ungültige Begrüßung geschickt."));
            return;
        }

        const QJsonValue protocolVersionValue = jsonObject.value(sp::protocolVersionKey);
        if (protocolVersionValue.type() != QJsonValue::Double) {
            Q_EMIT message(tr("Der Server hat eine ungültige Begrüßung geschickt."));
            Q_EMIT connectResult(false, tr("Der Server hat eine ungültige Begrüßung geschickt."));
            return;
        } else {
            const int protocolVersion = protocolVersionValue.toInt();
            if (protocolVersion != sp::protocolVersion) {
                Q_EMIT message(tr("Falsche Protokollrevision (ist %1, müsste %2 sein)").arg(
                                  QString::number(protocolVersion),
                                  QString::number(sp::protocolVersion)));
                Q_EMIT connectResult(false, tr(
                    "Der Server verwendet eine andere Protokollrevision (ist %1, müsste %2 sein). "
                    "Es kann keine Verbindung hergestellt werden.").arg(
                    QString::number(protocolVersion), QString::number(sp::protocolVersion)));
                return;
            }
        }

        // The counterpart seems to be a Muckturnier server and we speak the same protocol revision
        Q_EMIT message(tr("Begrüßung empfangen, frage Synchronisation an …"));
        sendData(sp::synchronizationRequest);

    // Data Synchronization
    // ===================

    } else if (handshakePhase == sp::synchronization) {

        // Check the settings and state
        // ----------------------------

        const QJsonObject settings          = jsonObject.value(sp::settings).toObject();
        const int boogerScore               = settings.value(sp::boogerScore).toInt();
        const int boogersPerRound           = settings.value(sp::boogersPerRound).toInt();
        const bool networkTournamentStarted = settings.value(sp::tournamentStarted).toBool();
        const Tournament::Mode tournamentMode
            = static_cast<Tournament::Mode>(settings.value(sp::tournamentMode).toInt());

        // Tournament settings
        if (m_tournamentSettings->tournamentMode() != tournamentMode
            || m_tournamentSettings->boogerScore() != boogerScore
            || m_tournamentSettings->boogersPerRound() != boogersPerRound) {

            Q_EMIT message(tr("Die Datenbank des Servers hat andere Einstellungen als die lokale"));
            Q_EMIT connectResult(false, tr(
                "Die Datenbank des Servers hat andere Einstellungen als die lokale:</p>"
                "<table>"
                "<tr>"
                    "<td></td>"
                    "<td>Server</td>"
                    "<td>Hier</td>"
                "</tr>"
                "<tr>"
                    "<td>Turniermodus:</td>"
                    "<td><i>%1</i>&nbsp;&nbsp;</td>"
                    "<td><i>%2</i></td>"
                "</tr>"
                "<tr>"
                    "<td>Punkte pro Bobbl:&nbsp;&nbsp;</td>"
                    "<td><i>%3</i></td>"
                    "<td><i>%4</i></td>"
                "</tr>"
                "<tr>"
                    "<td>Bobbl pro Runde:</td>"
                    "<td><i>%5</i></td>"
                    "<td><i>%6</i></td>"
                "</tr>"
                "</table>"
                "<p>Es kann keine Verbindung hergestellt werden.").arg(
                tournamentMode == Tournament::FixedPairs ? tr("Feste Paare")
                                                         : tr("Einzelne Spieler"),
                m_tournamentSettings->isPairMode() ? tr("Feste Paare") : tr("Einzelne Spieler"),
                QString::number(boogerScore),
                QString::number(m_tournamentSettings->boogerScore()),
                QString::number(boogersPerRound),
                QString::number(m_tournamentSettings->boogersPerRound())));
            return;
        }

        // Registration phase state
        if (m_db->localTournamentStarted() && ! networkTournamentStarted) {
            Q_EMIT message(tr("Es wurden bereits Ergebnisse eingegeben, aber nicht auf dem "
                              "Server"));
            Q_EMIT connectResult(false,
                     tr("Es wurden bereits Ergebnisse eingegeben, aber der Server hat die "
                        "Anmeldung noch nicht beendet.</p>"
                        "<p>Bitte nach Beendigung der Anmeldung erneut verbinden, oder die bisher "
                        "eingegebenen Ergebnisse löschen und erneut verbinden!"));
            return;
        }

        // Process the Server's data
        // -------------------------

        // Extract the markers
        QVector<Markers::Marker> markers;
        for (const auto &value : jsonObject.value(sp::markersList).toArray()) {
            const auto entry = value.toObject();
            const auto id = entry.value(sp::markerId).toInt();
            if (id != 0) {
                markers.append( {
                    id,                                           // id
                    entry.value(sp::markerName).toString(),       // name
                    ColorHelper::colorFromString(
                        entry.value(sp::markerColor).toString()), // color
                    static_cast<Markers::MarkerSorting>(
                        entry.value(sp::markerSorting).toInt())   // sorting
                } );
            } else {
                markers.append(Markers::unmarkedMarker);
            }
        }

        // Extract the players data

        QStringList players;
        QVector<Players::Data> playersData;
        for (const auto &value : jsonObject.value(sp::playersList).toArray()) {
            const auto entry = value.toObject();
            const auto name = entry.value(sp::playersName).toString();

            const auto jsonDraw = entry.value(sp::playersDraw).toObject();
            QMap<int, Draw::Seat> draw;
            QJsonObject::const_iterator it;
            for (it = jsonDraw.constBegin(); it != jsonDraw.constEnd(); it++) {
                const auto jsonSeat = it.value().toObject();
                draw.insert(it.key().toInt(),
                            Draw::Seat { jsonSeat.value(sp::drawnTable).toInt(),
                                         jsonSeat.value(sp::drawnPair).toInt(),
                                         jsonSeat.value(sp::drawnPlayer).toInt() });
            }

            playersData.append({
                0,                                          // id (not used here)
                name,                                       // name
                entry.value(sp::playersNumber).toInt(),     // number
                entry.value(sp::markerId).toInt(),          // marker
                draw,                                       // draw
                entry.value(sp::disqualified).toInt(),      // disqualified
                entry.value(sp::bookingChecksum).toString() // bookingChecksum
            });

            players.append(name);
        }

        // Extract the booking settings
        const auto bookingSettings = JsonHelper::parseBookingSettings(
            jsonObject.value(sp::bookingSettings).toObject());

        // Adopt the data
        // --------------

        // Adopt the locale
        const QString serverLocale = settings.value(sp::locale).toString();
        if (m_stringTools->locale() != serverLocale) {
            Q_EMIT message(tr("Übernehme abweichendes Server-Gebietsschema „%1“ (die lokale "
                              "Einstellung ist „%2“)").arg(serverLocale, m_stringTools->locale()));
            m_stringTools->setLocale(serverLocale);
        }

        // Adopt the registration phase state
        m_db->setNetworkTournamentStarted(networkTournamentStarted);

        // If we don't have results yet, we can simply adopt all data
        if (! m_db->localTournamentStarted()) {
            if (! m_db->adoptAllData(playersData, markers)) {
                return;
            }
            m_registrationPage->reload();

        // Otherwise, the local players list has to be matched and probably updated
        } else {
            auto [ localPlayers, success ] = m_db->players();
            if (! success) {
                return;
            }

            if (localPlayers.count() != players.count()) {
                Q_EMIT message(tr("Es wurden bereits Ergebnisse eingetragen, und es sind "
                                  "unterschiedlich viele %1 angemeldet").arg(
                                  m_tournamentSettings->isPairMode() ? tr("Paare")
                                                                     : tr("Spieler")));
                Q_EMIT connectResult(false, tr(
                    "Es wurden bereits Ergebnisse eingetragen, und es sind unterschiedlich viele "
                    "%1 angemeldet (am Server %2, hier %3). Es kann keine Verbindung hergestellt "
                    "werden!").arg(m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler"),
                                   QString::number(players.count()),
                                   QString::number(localPlayers.count())));
                return;
            }

            m_stringTools->sort(localPlayers);
            m_stringTools->sort(players);
            if (localPlayers == players) {
                // The lists match, we can leave it as-is and adopt the metadata
                if (! m_db->adoptPlayersMetadata(playersData, markers)) {
                    return;
                }
                m_registrationPage->reload();
            } else {
                // The names differ, so we have to match the lists first
                Q_EMIT namesDiffer(localPlayers, players);
                Q_EMIT connectResult(false, tr(
                    "Es wurden bereits Ergebnisse eingetragen, und die %1 des Servers "
                    "unterscheidet sich von der lokalen. Bevor eine Verbindung hergestellt werden "
                    "kann, müssen alle Namen übereinstimmen.</p>"
                    "<p>Bitte die Liste überprüfen, die Namen übernehmen bzw. manuell korrigieren "
                    "und erneut verbinden!").arg(
                    m_tournamentSettings->isPairMode() ? tr("Paarliste") : tr("Spielerliste")));
                return;
            }
        }

        // Adopt the "booked" marker
        const int bookedMarkerId = jsonObject.value(sp::bookedMarker).toInt();
        m_tournamentSettings->setSpecialMarker(Markers::Booked, bookedMarkerId);
        m_markersWidget->setBookedMarker(bookedMarkerId);

        if (m_tournamentSettings->isPairMode()) {
            // Adopt the singles management settings
            const Markers::SinglesManagement singlesManagement
                = JsonHelper::parseSinglesManagement(jsonObject.value(
                      sp::singlesManagement).toObject());
            m_markersWidget->setSinglesManagement(singlesManagement);
            m_tournamentSettings->setSinglesManagement(singlesManagement);
        }

        if (m_tournamentSettings->isSinglePlayerMode()) {
            // Adopt the server's names separator
            m_settings->setTransientNamesSeparator(settings.value(sp::namesSeparator).toString());
        }

        // Update the players list to show the adopted settings
        m_registrationPage->updatePlayersList();

        // We have to invoke RegistrationPage::updateChecksum() here manually, because as of now,
        // the registration page does not know yet that this instance is a client and won't cache
        // the checksum whilst running updatePlayersList()
        m_registrationPage->updateChecksum();

        // Adopt the server's initial score checksums
        const QJsonObject scoreChecksums = jsonObject.value(sp::scoreChecksumsList).toObject();
        const auto rounds = scoreChecksums.keys();
        for (const auto &round : rounds) {
            m_scorePage->setServerChecksum(round.toInt(), scoreChecksums.value(round).toString());
        }

        // Adopt the booking settings
        m_tournamentSettings->setBookingSettings(bookingSettings);

        // Here, the data should be in sync
        if (jsonObject.value(sp::checksum).toString() == *m_checksum) {
            // We are in sync with the server and everything seems to be fine, so we want an ID
            Q_EMIT message(tr("Synchronisation war erfolgreich, übermittle Ergebnis-Prüfsummen und "
                              "frage ID an …"));
            m_scorePage->initializeChecksums();

            QJsonObject scoreChecksums;

            QMap<int, QString>::const_iterator it;
            for (it = m_scorePage->checksums().constBegin();
                 it != m_scorePage->checksums().constEnd(); it++) {

                scoreChecksums.insert(QString::number(it.key()), it.value());
            }

            sendData(QJsonObject {
                { sp::action, sp::handshake },
                { sp::phase,  sp::idAllocation },
                { sp::scoreChecksumsList, scoreChecksums }
            });

        } else {
            // This should not happen, but who knows?!
            Q_EMIT message(tr("Die Übernahme der Daten des Servers ist fehlgeschlagen!"));
            Q_EMIT connectResult(false,
                                 tr("Die Übernahme der Daten des Servers ist fehlgeschlagen!"));
            return;
        }

    // Finish the handshake
    // ====================

    } else if (handshakePhase == sp::idAllocation) {
        m_connectionTimer->stop();
        m_handshakePending = false;
        m_id = jsonObject.value(sp::clientId).toInt();
        Q_EMIT message(tr("Verbindung mit %1 Port %2 hergestellt, Client-ID: %3").arg(
                          serverIp(), QString::number(serverPort()), QString::number(m_id)));
        Q_EMIT connectResult(true);
        m_connected = true;
    }
}

void Client::statusMessage(const QString &message, int clientId)
{
    if (clientId == m_id) {
        Q_EMIT statusUpdate(message);
    } else {
        Q_EMIT statusUpdate(tr("Netzwerkänderung: ") + message);
    }
}

void Client::adoptList(const QStringList &names, const QStringList &newNames)
{
    // This is invoked if a client wants to connect, scores have been entered already but the
    // registration list differs to adopt all names from the server. No connection is established at
    // this point, so no server request is queued for this, this is a local-only change.
    if (! m_db->renamePlayers(names, newNames)) {
        return;
    }
    Q_EMIT m_registrationPage->nameEdited();
}

// ======================
// Requests to the server
// ======================

// Registration changes
// ====================

void Client::requestRegisterPlayers(const QString &name, int marker)
{
    QJsonObject message { { sp::action,      sp::registerPlayers },
                          { sp::playersName, name },
                          { sp::markerId,    marker } };
    sendRequest(message, ProcessingTarget::TargetRegistration,
                tr("Anmelden von „%1“").arg(name),
                name);
}

void Client::requestRenamePlayers(const QString &name, const QString &newName, int markerId)
{
    QJsonObject message { { sp::action,         sp::renamePlayers },
                          { sp::playersName,    name },
                          { sp::newPlayersName, newName },
                          { sp::markerId,       markerId } };

    QString markerChange;
    if (markerId != -1) {
        if (markerId == 0) {
            markerChange = tr(" und Entfernen der Markierung");
        } else {
            markerChange = tr(" und Setzen der Markierung auf „%1“").arg(
                              m_markersWidget->marker(markerId).name);
        }
    }
    sendRequest(message, ProcessingTarget::TargetRegistration,
                tr("Umbenennen von „%1“ zu „%2“%3").arg(name, newName, markerChange),
                newName);
}

void Client::requestAssemblePair(const QString &player1, const QString &player2,
                                 const QString &separator, int assignedMarker)
{
    QJsonObject message { { sp::action,      sp::assemblePair },
                          { sp::playersName, QJsonArray { player1, player2, separator } },
                          { sp::markerId,    assignedMarker } };
    sendRequest(message, ProcessingTarget::TargetRegistration,
                tr("Zusammenfügen von „%1“ und „%2“ zu einem Paar").arg(player1, player2),
                player1 + separator + player2);
}

void Client::requestDeletePlayers(const QString &name)
{
    QJsonObject message { { sp::action,      sp::deletePlayers },
                          { sp::playersName, name } };
    sendRequest(message, ProcessingTarget::TargetRegistration,
                tr("Löschen von „%1“").arg(name));
}

void Client::requestDeleteMarkedPlayers(int markerId)
{
    QJsonObject message { { sp::action,     sp::deleteMarkedPlayers },
                          { sp::markerId,   markerId } };

    const auto &marker = m_markersWidget->marker(markerId);
    sendRequest(message, ProcessingTarget::TargetRegistration, markerId != 0
        ? tr("Löschen aller mit „%1“ markierten %2").arg(
             marker.name, m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler"))
        : tr("Löschen aller unmarkierten %1").arg(
             m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler")));
}

void Client::requestDeleteAllPlayers()
{
    QJsonObject message { { sp::action, sp::deleteAllPlayers } };
    sendRequest(message, ProcessingTarget::TargetRegistration,
                tr("Löschen aller %1 an").arg(
                   m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler")));
}

void Client::requestSetNumber(const QString &name, int number)
{
    QJsonObject message { { sp::action,        sp::setNumber },
                          { sp::playersName,   name },
                          { sp::playersNumber, number } };
    if (number > 0) {
        sendRequest(message, ProcessingTarget::TargetRegistration,
                    (m_tournamentSettings->isPairMode()
                         ? tr("Vergeben der Paarnummer %1 für „%2“")
                         : tr("Vergeben der Spielernummer %1 für „%2“")).arg(
                     QString::number(number), name),
                    name);
    } else {
        sendRequest(message, ProcessingTarget::TargetRegistration,
                    (m_tournamentSettings->isPairMode()
                         ? tr("Löschen der Paarnummer von „%1“")
                         : tr("Löschen der Spielernummer von „%1“")).arg(name),
                    name);
    }
}

void Client::requestSetBookingChecksum(const QString &name, const QString &checksum)
{
    QJsonObject message { { sp::action,          sp::setBookingChecksum },
                          { sp::playersName,     name },
                          { sp::bookingChecksum, checksum } };
    sendRequest(message, ProcessingTarget::TargetRegistration,
                tr("Registrieren des Voranmeldungscodes für „%1“").arg(name));
}

void Client::requestSetDisqualified(const QString &name, int round)
{
    QJsonObject message { { sp::action,      sp::setDisqualified },
                          { sp::playersName, name },
                          { sp::round,       round } };
    if (round != 0) {
        sendRequest(message, ProcessingTarget::TargetRegistration,
                    tr("Disqualifikation von „%1“ ab Runde %2").arg(
                       name, QString::number(round)),
                    name);
    } else {
        sendRequest(message, ProcessingTarget::TargetRegistration,
                    tr("Zurückziehen der Disqualifikation von „%1“").arg(name),
                    name);
    }
}

void Client::requestSetMarker(const QString &name, int markerId)
{
    QJsonObject message { { sp::action,      sp::setMarker },
                          { sp::playersName, name },
                          { sp::markerId,    markerId } };

    if (markerId != 0) {
        sendRequest(message, ProcessingTarget::TargetRegistration,
                    tr("Markieren von „%1“ als „%2“").arg(
                        name, m_markersWidget->marker(markerId).name),
                    name);
    } else {
        sendRequest(message, ProcessingTarget::TargetRegistration,
                    tr("Entfernen der Markierung von „%1“").arg(
                       name),
                    name);
    }
}

void Client::requestSetListMarker(int markerId)
{
    QJsonObject message { { sp::action,     sp::setListMarker },
                          { sp::markerId,   markerId } };

    if (markerId != 0) {
        sendRequest(message, ProcessingTarget::TargetRegistration,
                    tr("Markieren aller %1 als „%2“ an").arg(
                       m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler"),
                       m_markersWidget->marker(markerId).name));
    } else {
        sendRequest(message, ProcessingTarget::TargetRegistration,
                    tr("Entfernen aller Markierungen"));
    }
}

void Client::requestRemoveListMarker(int markerId)
{
    QJsonObject message { { sp::action,     sp::removeListMarker },
                          { sp::markerId,   markerId } };
    sendRequest(message, ProcessingTarget::TargetRegistration,
                tr("Entfernen aller „%1“-Markierungen").arg(
                   m_markersWidget->marker(markerId).name));
}

// Draw changes
// ============

void Client::requestSetDraw(int round, const QString &name, const Draw::Seat &seat, int marker)
{
    QJsonObject message { { sp::action,      sp::setDraw },
                          { sp::round,       round },
                          { sp::playersName, name },
                          { sp::drawnTable,  seat.table },
                          { sp::drawnPair,   seat.pair },
                          { sp::drawnPlayer, seat.player },
                          { sp::markerId,    marker } };
    const QString text = seat.isEmpty()
        ? tr("Löschen der Auslosung für „%1“ für Runde %2").arg(name, QString::number(round))
        : tr("Speichern der Auslosung für „%1“ für Runde %2").arg(name, QString::number(round));
    sendRequest(message, ProcessingTarget::TargetRegistration, text, name);
}

void Client::requestSetListDraw(int round, const QVector<QString> &names,
                                const QVector<Draw::Seat> &draw)
{
    QJsonArray namesList;
    QJsonArray tables;
    QJsonArray pairs;
    QJsonArray players;
    for (int i = 0; i < names.count(); i++) {
        namesList.append(names.at(i));
        tables.append(draw.at(i).table);
        pairs.append(draw.at(i).pair);
        players.append(draw.at(i).player);
    }

    QJsonObject message { { sp::action,           sp::setListDraw },
                          { sp::round,            round },
                          { sp::playersList,      namesList },
                          { sp::drawnTablesList,  tables },
                          { sp::drawnPairsList,   pairs },
                          { sp::drawnPlayersList, players } };

    sendRequest(message, ProcessingTarget::TargetRegistration,
                tr("Setzen der Auslosung aller Plätze für Runde %1").arg(round));
}

void Client::requestDeleteAllDraws(int round)
{
    QJsonObject message { { sp::action, sp::deleteAllDraws },
                          { sp::round,  round} };
    sendRequest(message, ProcessingTarget::TargetRegistration,
                tr("Löschen aller Auslosungen für Runde %1").arg(round));
}

// Markers definition changes
// ==========================

void Client::requestAddMarker(const Markers::Marker &marker)
{
    QJsonObject message { { sp::action,        sp::addMarker },
                          { sp::markerName,    marker.name },
                          { sp::markerColor,   ColorHelper::colorToString(marker.color) },
                          { sp::markerSorting, marker.sorting } };
    sendRequest(message, ProcessingTarget::TargetMarkers,
                tr("Hinzufügen der Markierung „%1“").arg(marker.name));
}

void Client::requestDeleteMarker(const QString &name, int id, int sequence)
{
    QJsonObject message { { sp::action,         sp::deleteMarker },
                          { sp::markerName,     name },
                          { sp::markerId,       id },
                          { sp::markerSequence, sequence } };
    sendRequest(message, ProcessingTarget::TargetMarkers,
                tr("Frage Löschen der Markierung „%1“").arg(name));
}

void Client::requestMoveMarker(const QString &name, int id, int sequence, int direction)
{
    QJsonObject message { { sp::action,          sp::moveMarker },
                          { sp::markerName,      name },
                          { sp::markerId,        id },
                          { sp::markerSequence,  sequence },
                          { sp::markerDirection, direction } };
    sendRequest(message, ProcessingTarget::TargetMarkers,
                tr("Verschieben der Markierung „%1“ %2").arg(
                   name, direction == 1 ? tr("nach unten") : tr("nach oben")));
}

void Client::requestEditMarker(const QString &name, int id, const QString &newName,
                               const QColor &color, Markers::MarkerSorting sorting)
{
    QJsonObject message { { sp::action,        sp::editMarker },
                          { sp::markerName,    name },
                          { sp::markerId,      id },
                          { sp::newMarkerName, newName },
                          { sp::markerColor,   ColorHelper::colorToString(color) },
                          { sp::markerSorting, sorting } };
    sendRequest(message, ProcessingTarget::TargetMarkers,
                tr("Bearbeiten der Markierung „%1“").arg(name));
}

// Special markers changes
// =======================

void Client::requestSetBookedMarker(const QString &name, int id)
{
    QJsonObject message { { sp::action,     sp::setBookedMarker },
                          { sp::markerName, name },
                          { sp::markerId,   id } };
    sendRequest(message, ProcessingTarget::TargetMarkers,
                id == -1 ? tr("Deaktivierung der Markierung für Voranmeldungen")
                         : tr("Setzen der Markierung für Voranmeldungen auf „%1“").arg(name));
}

void Client::requestSinglesManagementChange(const Markers::SinglesManagement &data)
{
    QJsonObject message { { sp::action,            sp::singlesManagementChange },
                          { sp::singlesManagement, JsonHelper::singlesManagementToJson(data) } };
    sendRequest(message, ProcessingTarget::TargetMarkers,
                tr("Änderung der Einstellungen für das Behandlen allein gekommener Spieler"));
}

// Finishing the registration
// ==========================

void Client::requestFinishRegistration()
{
    QJsonObject message { { sp::action, sp::finishRegistration } };
    sendRequest(message, ProcessingTarget::TargetRegistration,
                tr("Beenden der Anmeldung"));
}

// Score comparison
// ================

void Client::requestScore(int round)
{
    QJsonObject message { { sp::action,      sp::scoreRequest },
                          { sp::requestType, sp::clientRequest },
                          { sp::round,       round } };
    sendRequest(message, ProcessingTarget::NoTarget,
                tr("Ergebnisse für Runde %1").arg(round));
}

void Client::requestScoreChecksumChange(int round)
{
    QJsonObject message { { sp::action,        sp::scoreChecksumChange },
                          { sp::round,         round },
                          { sp::scoreChecksum, m_scorePage->checksums().value(round) } };
    sendRequest(message, ProcessingTarget::TargetScore,
                tr("Änderung der Ergebnisse für Runde %1").arg(round));
}

// Stop watch synchronization
// ==========================

void Client::requestStopWatchSynchronization(int id)
{
    QJsonObject message { { sp::action,      sp::stopWatchSynchronization },
                          { sp::stopWatchId, id } };
    JsonHelper::setNoChecksumCheck(message);
    sendRequest(message, ProcessingTarget::TargetStopWatch,
                tr("Synchronisation der Stoppuhr #%1 mit der des Servers").arg(id));
}

// ====================================
// Change announcements from the server
// ====================================

void Client::dataReceived(const QString &action, const QJsonObject &jsonObject)
{
    Q_EMIT rx();

    if (action == sp::handshake) {
        if (! m_handshakePending) {
            return;
        }
        processHandshake(jsonObject);
        return;
    }

    const int clientId = jsonObject.value(sp::clientId).toInt();
    auto target = ProcessingTarget::NoTarget;
    if (clientId == m_id) {
        // We cache the last processing target here, because it's removed by
        // stopProcessingRequest(), but we may need it below if the request was rejected
        target = m_processingTarget.first();
        stopProcessingRequest();
    } else {
        QApplication::setOverrideCursor(Qt::BusyCursor);
    }

    // Check if the request has been accepted

    if (action == sp::actionRejected) {
        QApplication::restoreOverrideCursor();

        if (target != ProcessingTarget::TargetScore) {
            // We have a registration or marker collision that we actually have to handle
            Q_EMIT statusUpdate(tr("Anfrage vom Server zurückgewiesen"));
            Q_EMIT requestRejected(
                static_cast<sp::RejectReason>(jsonObject.value(sp::rejectReason).toInt()));

        } else {
            // The only request the score page does is to announce score checksum changes.
            // If a checksum change happens whilst doing so, somebody else renamed a pair/player
            // meanwhile. This will be broadcasted and handled, and the client will automatically
            // re-announce the current score checksum. So we can ignore this.
            Q_EMIT statusUpdate(tr("Anfrage automatisch korrigiert"));
            Q_EMIT message(tr("Während des Übermittelns der neuen Ergebnis-Prüfsumme wurde die "
                              "Anmeldeliste geändert. Die neue Prüfsumme wurde automatisch "
                              "übermittelt."));
        }

        return;
    }

    // Pass the data to the respective handler function
    if (m_requestHandlers.contains(action)) {
        (this->*m_requestHandlers.value(action))(jsonObject, clientId);
    } else {
        // This should not happen
        QApplication::restoreOverrideCursor();
        while (! m_processingTarget.isEmpty()) {
            stopProcessingRequest();
        }
        Q_EMIT statusUpdate(tr("Aktion „%1“ ist unbekannt, konnte die Antwort des Servers nicht "
                               "verarbeiten!").arg(action));
        Q_EMIT connectionError(tr("Aktion „%1“ ist unbekannt, konnte die Antwort des Servers nicht "
                                  "verarbeiten!").arg(action));
        return;
    }

    QApplication::restoreOverrideCursor();

    if (JsonHelper::doChecksumCheck(jsonObject)) {
        verifyChecksum(jsonObject.value(sp::checksum).toString());
    }
}

// Registration changes
// ====================

void Client::processRegisterPlayers(const QJsonObject &jsonObject, int clientId)
{
    const QString name = jsonObject.value(sp::playersName).toString();
    const int marker   = jsonObject.value(sp::markerId).toInt();
    Q_EMIT message(tr("Melde „%1“ an").arg(name));

    if (! m_db->registerPlayers(name, marker)) {
       return;
    }
    statusMessage(tr("%1 „%2“ angemeldet").arg(
                     m_tournamentSettings->isPairMode() ? tr("Paar") : tr("Spieler"), name),
                  clientId);

    m_registrationPage->processNetworkChange(clientId);
}

void Client::processRenamePlayers(const QJsonObject &jsonObject, int clientId)
{
    const auto name       = jsonObject.value(sp::playersName).toString();
    const auto newName    = jsonObject.value(sp::newPlayersName).toString();
    const auto markerId   = jsonObject.value(sp::markerId).toInt();
    const auto markerName = markerId == -1 ? QString() : m_markersWidget->marker(markerId).name;

    QString markerChangeStart;
    QString markerChangeFinished;
    if (markerId != -1) {
        if (markerId == 0) {
            markerChangeStart = tr(" und entferne die Markierung");
            markerChangeFinished = tr(" und die Markierung entfernt");
        } else {
            markerChangeStart = tr(" und setze die Markierung auf „%1“").arg(markerName);
            markerChangeFinished = tr(" und die Markierung auf „%1“ gesetzt").arg(markerName);
        }
    }

    Q_EMIT message(tr("Benenne „%1“ zu „%2“ um%3").arg(name, newName, markerChangeStart));

    if (! m_db->renamePlayers(name, newName, markerId)) {
        return;
    }

    statusMessage(tr("%1 „%2“ zu „%3“ umbenannt%4").arg(
                     m_tournamentSettings->isPairMode() ? tr("Paar") : tr("Spieler"),
                     name, newName, markerChangeFinished),
                  clientId);

    m_registrationPage->processNetworkChange(clientId);
    Q_EMIT m_registrationPage->requestUpdateDrawOverviewPage(0, clientId == m_id ? newName
                                                                                 : QString());
    Q_EMIT m_registrationPage->nameEdited();
}

void Client::processAssemblePair(const QJsonObject &jsonObject, int clientId)
{
    const QJsonArray namesData = jsonObject.value(sp::playersName).toArray();
    const QString player1      = namesData.at(0).toString();
    const QString player2      = namesData.at(1).toString();
    const QString separator    = namesData.at(2).toString();
    const int marker           = jsonObject.value(sp::markerId).toInt();
    Q_EMIT message(tr("Füge „%1“ und „%2“ zu einem Paar zusammen").arg(player1, player2));

    if (! m_db->assemblePair(player1, player2, separator, marker)) {
        return;
    }

    statusMessage(tr("„%1“ und „%2“ zu einem Paar zusammengefügt").arg(player1, player2),
                  clientId);

    m_registrationPage->processNetworkChange(clientId);
    Q_EMIT m_registrationPage->requestUpdateDrawOverviewPage(0,
                                                             clientId == m_id
                                                                 ? player1 + separator + player2
                                                                 : QString());
}

void Client::processDeletePlayers(const QJsonObject &jsonObject, int clientId)
{
    const QString name = jsonObject.value(sp::playersName).toString();
    Q_EMIT message(tr("Lösche „%1“").arg(name));

    if (! m_db->deletePlayers(name)) {
        return;
    }

    statusMessage(tr("%1 „%2“ gelöscht").arg(
                     m_tournamentSettings->isPairMode() ? tr("Paar") : tr("Spieler"), name),
                  clientId);

    m_registrationPage->processNetworkChange(clientId);
    Q_EMIT m_registrationPage->requestUpdateDrawOverviewPage(0, QString());
}

void Client::processDeleteMarkedPlayers(const QJsonObject &jsonObject, int clientId)
{
    const auto &marker = m_markersWidget->marker(jsonObject.value(sp::markerId).toInt());

    Q_EMIT message(marker.id != 0
        ? tr("Lösche alle als „%1“ markierten %2").arg(
             marker.name, m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler"))
        : tr("Lösche alle unmarkierten %1").arg(
             m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler")));

    if (! m_db->deleteMarkedPlayers(marker.id)) {
        return;
    }

    statusMessage(marker.id != 0
        ? tr("Alle als „%1“ markierten %2 gelöscht").arg(
             marker.name, m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler"))
        : tr("Alle unmarkierten %1 gelöscht").arg(
             m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler")),
        clientId);

    m_registrationPage->processNetworkChange(clientId);
    Q_EMIT m_registrationPage->requestUpdateDrawOverviewPage(0, QString());
}

void Client::processDeleteAllPlayers(const QJsonObject &, int clientId)
{
    Q_EMIT message(tr("Lösche alle %1").arg(m_tournamentSettings->isPairMode() ? tr("Paare")
                                                                               : tr("Spieler")));

    if (! m_db->deleteAllPlayers()) {
        return;
    }

    statusMessage(tr("Alle %1 gelöscht").arg(
                     m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler")),
                  clientId);

    m_registrationPage->processNetworkChange(clientId);
    Q_EMIT m_registrationPage->requestUpdateDrawOverviewPage(0, QString());
}

void Client::processSetNumber(const QJsonObject &jsonObject, int clientId)
{
    const QString name = jsonObject.value(sp::playersName).toString();
    const int number   = jsonObject.value(sp::playersNumber).toInt();

    if (number > 0) {
        Q_EMIT message((m_tournamentSettings->isPairMode()
                            ? tr("Vergebe Paarnummer %1 für „%2“")
                            : tr("Vergebe Spielernummer %1 für „%2“")).arg(
                        QString::number(number), name));
    } else {
        Q_EMIT message((m_tournamentSettings->isPairMode()
                            ? tr("Lösche Paarnummer von „%1“")
                            : tr("Lösche Spielernummer von „%1“")).arg(name));
    }

    if (! m_db->setNumber(name, number)) {
        return;
    }

    if (number > 0) {
        statusMessage((m_tournamentSettings->isPairMode()
                           ? tr("Paarnummer %1 für „%2“ vergeben")
                           : tr("Spielernummer %1 für „%2“ vergeben")).arg(
                       QString::number(number), name),
                      clientId);
    } else {
        statusMessage((m_tournamentSettings->isPairMode()
                           ? tr("Paarnummer von „%1“ gelöscht")
                           : tr("Spielernummer von „%1“ gelöscht")).arg(name),
                      clientId);
    }

    m_registrationPage->processNetworkChange(clientId);
    Q_EMIT m_registrationPage->requestUpdateDrawOverviewPage(0,
                                                             clientId == m_id ? name : QString());
}

void Client::processSetBookingChecksum(const QJsonObject &jsonObject, int clientId)
{
    const auto name     = jsonObject.value(sp::playersName).toString();
    const auto checksum = jsonObject.value(sp::bookingChecksum).toString();

    Q_EMIT message(tr("Registriere Voranmeldungscode für „%1“").arg(name));

    if (! m_db->setBookingChecksum(name, checksum)) {
        return;
    }

    statusMessage(tr("Voranmeldungscode für „%1“ registriert").arg(name), clientId);

    m_registrationPage->processNetworkChange(clientId);
    if (clientId == m_id) {
        QTimer::singleShot(0, m_registrationPage, &RegistrationPage::requestSaveBookingPng);
    }
}

void Client::processSetDisqualified(const QJsonObject &jsonObject, int clientId)
{
    const QString name = jsonObject.value(sp::playersName).toString();
    const int round    = jsonObject.value(sp::round).toInt();

    if (round != 0) {
        Q_EMIT message(tr("Disqualifiziere „%1“ ab Runde %2").arg(name, QString::number(round)));
    } else {
        Q_EMIT message(tr("Nehme die Disqualifikation von „%1“ zurück").arg(name));
    }

    if (! m_db->setDisqualified(name, round)) {
        return;
    }

    if (round != 0) {
        statusMessage(tr("%1 „%2“ ab Runde %3 disqualifiziert").arg(
                         m_tournamentSettings->isPairMode() ? tr("Paar") : tr("Spieler"),
                         name, QString::number(round)),
                      clientId);
    } else {
        statusMessage(tr("Disqualifikation von %1 „%2“ zurückgenommen").arg(
                         m_tournamentSettings->isPairMode() ? tr("Paar") : tr("Spieler"), name),
                      clientId);
    }

    m_registrationPage->processNetworkChange(clientId);
    Q_EMIT m_registrationPage->requestUpdateDrawOverviewPage(0,
                                                             clientId == m_id ? name : QString());
    Q_EMIT m_registrationPage->disqualificationChanged();
}

void Client::processSetMarker(const QJsonObject &jsonObject, int clientId)
{
    const auto name    = jsonObject.value(sp::playersName).toString();
    const auto &marker = m_markersWidget->marker(jsonObject.value(sp::markerId).toInt());

    if (marker.id != 0) {
        Q_EMIT message(tr("Markiere „%1“ als „%2“").arg(name, marker.name));
    } else {
        Q_EMIT message(tr("Entferne Markierung von „%1“").arg(name));
    }

    if (! m_db->setMarker(name, marker.id)) {
        return;
    }

    if (marker.id != 0) {
        statusMessage(tr("%1 „%2“ als „%3“ markiert").arg(
                         m_tournamentSettings->isPairMode() ? tr("Paar") : tr("Spieler"),
                         name, marker.name),
                      clientId);
    } else {
        statusMessage(tr("Markierung von %1 „%2“ entfernt").arg(
                         m_tournamentSettings->isPairMode() ? tr("Paar") : tr("Spieler"), name),
                      clientId);
    }

    m_registrationPage->processNetworkChange(clientId);
}

void Client::processSetListMarker(const QJsonObject &jsonObject, int clientId)
{
    const auto &marker = m_markersWidget->marker(jsonObject.value(sp::markerId).toInt());

    if (marker.id != 0) {
        Q_EMIT message(tr("Markiere alle %1 als „%2“").arg(
                          m_tournamentSettings->isPairMode() ? tr("Paar") : tr("Spieler"),
                          marker.name));
    } else {
        Q_EMIT message(tr("Entferne alle Markierungen"));
    }

    if (! m_db->setListMarker(marker.id)) {
        return;
    }

    if (marker.id != 0) {
        statusMessage(tr("Alle %1 als „%2“ markiert").arg(
                         m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler"),
                         marker.name),
                      clientId);
    } else {
        statusMessage(tr("Alle Markierungen entfernt"), clientId);
    }

    m_registrationPage->processNetworkChange(clientId);
}

void Client::processRemoveListMarker(const QJsonObject &jsonObject, int clientId)
{
    const auto &marker = m_markersWidget->marker(jsonObject.value(sp::markerId).toInt());

    Q_EMIT message(tr("Entferne alle „%1“-Markierungen").arg(marker.name));

    if (! m_db->removeListMarker(marker.id)) {
        return;
    }

    statusMessage(tr("Alle „%1“-Markierungen entfernt").arg(marker.name), clientId);

    m_registrationPage->processNetworkChange(clientId);
}

// Draw changes
// ============

void Client::processSetDraw(const QJsonObject &jsonObject, int clientId)
{
    const int round       = jsonObject.value(sp::round).toInt();
    const QString name    = jsonObject.value(sp::playersName).toString();
    const Draw::Seat seat = { jsonObject.value(sp::drawnTable).toInt(),
                              jsonObject.value(sp::drawnPair).toInt(),
                              jsonObject.value(sp::drawnPlayer).toInt() };
    Q_EMIT message(seat.isEmpty()
        ? tr("Lösche Auslosung für „%1“ für Runde %2").arg(name, QString::number(round))
        : tr("Speichere Auslosung für „%1“ für Runde %2").arg(name, QString::number(round)));

    if (! m_db->setDraw(round, name, seat, jsonObject.value(sp::markerId).toInt())) {
        return;
    }

    if (seat.isEmpty()) {
        statusMessage(
            tr("Auslosung für „%1“ für Runde %2 gelöscht").arg(name, QString::number(round)),
            clientId);
    } else {
        if (m_tournamentSettings->isPairMode()) {
            statusMessage(tr("Auslosung für „%1“ für Runde %2 gespeichert: Tisch %3 Paar %4").arg(
                             name, QString::number(round), QString::number(seat.table),
                             QString::number(seat.pair)),
                          clientId);
        } else {
            statusMessage(tr("Auslosung für „%1“ für Runde %2 gespeichert: Tisch %3 Paar %4 "
                             "(%5)").arg(
                             name, QString::number(round), QString::number(seat.table),
                             QString::number(seat.pair), QString::number(seat.player)),
                          clientId);
        }
    }

    m_registrationPage->processNetworkChange(clientId);
    Q_EMIT m_registrationPage->requestUpdateDrawOverviewPage(round,
        (clientId == m_id && seat.isEmpty()) ? QString() : name);
    Q_EMIT m_registrationPage->drawChanged();
}

void Client::processSetListDraw(const QJsonObject &jsonObject, int clientId)
{
    const int round = jsonObject.value(sp::round).toInt();
    Q_EMIT message(tr("Setze Auslosung aller Plätze für Runde %1").arg(round));

    const QJsonArray namesData   = jsonObject.value(sp::playersList).toArray();
    const QJsonArray tablesData  = jsonObject.value(sp::drawnTablesList).toArray();
    const QJsonArray pairsData   = jsonObject.value(sp::drawnPairsList).toArray();
    const QJsonArray playersData = jsonObject.value(sp::drawnPlayersList).toArray();

    QVector<QString> names;
    QVector<Draw::Seat> draw;
    for (int i = 0; i < namesData.count(); i++) {
        names.append(namesData.at(i).toString());
        draw.append({ tablesData.at(i).toInt(),
                      pairsData.at(i).toInt(),
                      playersData.at(i).toInt() });
    }

    if (! m_db->setListDraw(round, names, draw)) {
        return;
    }

    statusMessage(tr("Auslosung aller Plätze für Runde %1 gesetzt").arg(round), clientId);
    m_registrationPage->processNetworkChange(clientId);
    Q_EMIT m_registrationPage->requestUpdateDrawOverviewPage(round, QString());
    Q_EMIT m_registrationPage->drawChanged();
}

void Client::processDeleteAllDraws(const QJsonObject &jsonObject, int clientId)
{
    const int round = jsonObject.value(sp::round).toInt();
    Q_EMIT message(tr("Lösche alle Auslosungen für Runde %1").arg(round));

    if (! m_db->deleteAllDraws(round)) {
        return;
    }

    statusMessage(tr("Alle Auslosungen für Runde %1 gelöscht").arg(round), clientId);

    m_registrationPage->processNetworkChange(clientId);
    Q_EMIT m_registrationPage->requestUpdateDrawOverviewPage(round, QString());
    Q_EMIT m_registrationPage->drawChanged();
}

// Marker definition changes
// =========================

void Client::processAddMarker(const QJsonObject &jsonObject, int clientId)
{
    const QString name = jsonObject.value(sp::markerName).toString();
    Q_EMIT message(tr("Füge Markierung „%1“ hinzu").arg(name));

    Markers::Marker marker;
    marker.name    = name;
    marker.color   = ColorHelper::colorFromString(jsonObject.value(sp::markerColor).toString());
    marker.sorting = static_cast<Markers::MarkerSorting>(
                         jsonObject.value(sp::markerSorting).toInt());

    if (! m_db->addMarker(marker)) {
        return;
    }

    statusMessage(tr("Markierung „%1“ hinzugefügt").arg(name), clientId);

    m_markersWidget->processNetworkChange();
}

void Client::processDeleteMarker(const QJsonObject &jsonObject, int clientId)
{
    const QString name = jsonObject.value(sp::markerName).toString();
    Q_EMIT message(tr("Lösche Markierung „%1“").arg(name));

    if (! m_db->deleteMarker(jsonObject.value(sp::markerId).toInt(),
                             jsonObject.value(sp::markerSequence).toInt())) {
        return;
    }

    statusMessage(tr("Markierung „%1“ gelöscht").arg(name), clientId);

    m_markersWidget->processNetworkChange();
}

void Client::processMoveMarker(const QJsonObject &jsonObject, int clientId)
{
    const QString name  = jsonObject.value(sp::markerName).toString();
    const int direction = jsonObject.value(sp::markerDirection).toInt();
    Q_EMIT message(tr("Verschiebe Markierung „%1“ %2").arg(
                      name, direction == 1 ? tr("nach unten") : tr("nach oben")));

    if (! m_db->moveMarker(jsonObject.value(sp::markerId).toInt(),
                           jsonObject.value(sp::markerSequence).toInt(),
                           direction)) {
        return;
    }

    statusMessage(tr("Markierung „%1“ %2 verschoben").arg(
                     name, direction == 1 ? tr("nach unten") : tr("nach oben")),
                  clientId);

    m_markersWidget->processNetworkChange();
}

void Client::processEditMarker(const QJsonObject &jsonObject, int clientId)
{
    const QString name = jsonObject.value(sp::markerName).toString();
    Q_EMIT message(tr("Speichere Änderungen an Markierung „%1“").arg(name));

    if (! m_db->editMarker(jsonObject.value(sp::markerId).toInt(),
                           jsonObject.value(sp::newMarkerName).toString(),
                           ColorHelper::colorFromString(
                               jsonObject.value(sp::markerColor).toString()),
                           static_cast<Markers::MarkerSorting>(
                               jsonObject.value(sp::markerSorting).toInt()))) {
        return;
    }

    statusMessage(tr("Änderungen an Markierung „%1“ gespeichert").arg(name), clientId);

    m_markersWidget->processNetworkChange();
}

// Special markers changes
// =======================

void Client::processSetBookedMarker(const QJsonObject &jsonObject, int clientId)
{
    const QString name = jsonObject.value(sp::markerName).toString();
    const int markerId = jsonObject.value(sp::markerId).toInt();

    Q_EMIT message(markerId == -1 ? tr("Deaktiviere Markierung für Voranmeldungen")
                                  : tr("Setze Markierung für Voranmeldungen auf „%1“").arg(name));

    m_tournamentSettings->setSpecialMarker(Markers::Booked, markerId);
    m_markersWidget->setBookedMarker(markerId);

    statusMessage(markerId == -1 ? tr("Markierung für Voranmeldungen deaktiviert")
                                 : tr("Markierung für Voranmeldungen auf „%1“ gesetzt").arg(name),
                  clientId);

    m_markersWidget->processNetworkChange();

    if (markerId != -1) {
        m_markersWidget->warnAboutSpecialMarkers();
    }
}

void Client::processSinglesManagementChange(const QJsonObject &jsonObject, int clientId)
{
    Q_EMIT message(tr("Übernehme Änderungen an den Einstellungen für das Behandlen allein "
                      "gekommener Spieler"));

    const Markers::SinglesManagement data = JsonHelper::parseSinglesManagement(
        jsonObject.value(sp::singlesManagement).toObject());
    m_markersWidget->setSinglesManagement(data);
    m_tournamentSettings->setSinglesManagement(data);
    statusMessage(tr("Änderungen an den Einstellungen für das Behandeln allein gekommener Spieler "
                     "übernommen"),
                  clientId);

    m_markersWidget->processNetworkChange();

    if (data.enabled) {
        m_markersWidget->warnAboutSpecialMarkers();
    }
}

// Finishing the registration
// ==========================

void Client::processFinishRegistration(const QJsonObject &, int clientId)
{
    Q_EMIT message(tr("Beende die Anmeldung"));
    m_registrationPage->networkFinishRegistration(clientId);
    statusMessage(tr("Anmeldung beendet"), clientId);
}

// Score comparison
// ================

void Client::processScoreChecksumChange(const QJsonObject &jsonObject, int remoteId)
{
    const auto round = jsonObject.value(sp::round).toInt();

    if (remoteId == 0) {
        const auto checksum = jsonObject.value(sp::scoreChecksum).toString();
        Q_EMIT message(tr("Änderung der Ergebnisse für Runde %1 am Server").arg(round));
        m_scorePage->setServerChecksum(round, checksum);
        Q_EMIT remoteScoreChecksumChanged(round);
    } else {
        const auto text = tr("Änderung der Ergebnisse für Runde %1 wurde vom Server "
                             "verarbeitet").arg(round);
        Q_EMIT message(text);
        Q_EMIT statusUpdate(text);
    }
}

void Client::processScoreRequest(const QJsonObject &jsonObject, int remoteId)
{
    const auto requestType = jsonObject.value(sp::requestType).toString();

    if (requestType == sp::clientRequest) {
        // We asked for a score
        const auto score = ScoreHelper::jsonToScore(jsonObject);
        const auto text = tr("Rundenergebnisse des Servers erhalten");
        Q_EMIT message(text);
        Q_EMIT statusUpdate(text);
        Q_EMIT compareScore(score);

    } else if (requestType == sp::serverRequest) {
        const auto round = jsonObject.value(sp::round).toInt();

        if (remoteId == 0) {
            // The server asked for a score and we have to send our data
            Q_EMIT message(tr("Der Server fragt die Ergebnisse für Runde %1 an").arg(round));

            const auto [ score, success ] = m_db->scores(round);
            if (! success) {
                return;
            }

            auto data = ScoreHelper::scoreToJson(score);
            data.insert(sp::requestType, sp::serverRequest);
            data.insert(sp::round, round);

            sendRequest(data, ProcessingTarget::NoTarget,
                        tr("Antwort auf das Senden der Ergebnisse für Runde %1").arg(round));

        } else {
            // The server asked for a score, we sent the data and received the okay response
            const auto text = tr("Die Ergebnisse für Runde %1 wurden vom Server verarbeitet").arg(
                                 round);
            Q_EMIT message(text);
            Q_EMIT statusUpdate(text);
        }

    } else {
        // This should not happen
        qWarning("Received a score request that's neither a client nor a server request!");
        const auto text = tr("Ungültige Spielstandanfrage empfangen!");
        Q_EMIT message(text);
        Q_EMIT statusUpdate(text);
    }
}

// Stop watch synchronization
// ==========================

void Client::processStopWatchSynchronization(const QJsonObject &jsonObject, int)
{
    if (! jsonObject.value(sp::syncPossible).toBool()) {
        const auto text = tr("Der Server stellt momentan keine Stoppuhrdaten zur Verfügung");
        Q_EMIT message(text);
        Q_EMIT statusUpdate(text);
        m_stopWatchEngine->noServerData();
        return;
    }

    const auto stopWatchId = jsonObject.value(sp::stopWatchId).toInt();
    m_stopWatchEngine->synchronize(
        StopWatchSync::Data {
            true, // syncPossible
            jsonObject.value(sp::title).toString(),
            jsonObject.value(sp::hours).toInt(),
            jsonObject.value(sp::minutes).toInt(),
            jsonObject.value(sp::seconds).toInt(),
            jsonObject.value(sp::isRunning).toBool(),
            jsonObject.value(sp::startOffset).toInt(),
            jsonObject.value(sp::targetTime).toString().toLongLong()
        }, stopWatchId);

    const auto text = tr("Stoppuhrdaten des Servers zur Synchronisation von Stoppuhr #%1 "
                         "erhalten").arg(stopWatchId);
    Q_EMIT message(text);
    Q_EMIT statusUpdate(text);
}
