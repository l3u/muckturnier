// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef CLIENTPAGE_H
#define CLIENTPAGE_H

// Local includes

#include "shared/OptionalPage.h"

#include "network/ServerProtocol.h"

// Local classes
class Database;
class RegistrationPage;
class ScorePage;
class StopWatchEngine;
class Client;
class LogDisplay;
class SocketAddressWidget;
class SharedObjects;
class StringTools;
class Database;
class DiscoverClient;

// Qt classes
class QPushButton;
class QLabel;
class QGroupBox;
class QTableWidget;

class ClientPage : public OptionalPage
{
    Q_OBJECT

public:
    explicit ClientPage(QWidget *parent, SharedObjects *sharedObjects,
                        RegistrationPage *registrationPage, ScorePage *scorePage,
                        StopWatchEngine *stopWatchEngine, const QString &ip, int port);
    Client *client() const;

Q_SIGNALS:
    void connectionRequested(const QString &ip, int port);
    void connected();
    void connectionClosed();
    void statusUpdate(const QString &text);
    void namesDifferencesDisplayed(bool state);
    void raiseMe();

public Q_SLOTS:
    bool closePage();
    void showDifferingNames(QStringList &localNames, QStringList &remoteNames);

private Q_SLOTS:
    bool closeConnection();
    void finishConstruction(bool ipGiven);
    void connectToServer();
    void checkConnectResult(bool success, const QString &error);
    void connectionError(const QString &text);
    void requestRejected(ServerProtocol::RejectReason reason);
    void adoptList();
    void cancelAdoption();
    void searchServer();
    void processDiscoverResult(const QString &ip, int port);

private: // Functions
    void resetInterface();
    void closeClient();

    QHash<int, int> matchLists(QStringList &source, QStringList &target) const;
    int levenshteinDistance(const QString &source, const QString &target) const;
    QHash<int, int> combinedLevenshteinDistance(const QString &text,
                                                const QStringList &candidates) const;
    void removeIntersection(QStringList &list1, QStringList &list2) const;
    QString longestCommonSubsequence(const QString &source, const QString &target) const;
    QStringList diffList(QString text, QString longestCommonSubsequence) const;

private: // Variables
    StringTools *m_stringTools;
    Database *m_db;
    Client *m_client;
    QGroupBox *m_settingsBox;
    SocketAddressWidget *m_socketAddress;
    QLabel *m_warningLabel;
    QPushButton *m_searchServerButton;
    QPushButton *m_connectButton;
    QPushButton *m_disconnectButton;
    QPushButton *m_closeButton;
    QPushButton *m_adoptListButton;
    QPushButton *m_cancelButton;
    QGroupBox *m_displayBox;
    LogDisplay *m_display;
    QLabel *m_connectedLabel;
    bool m_connecting;
    QGroupBox *m_differencesBox;
    QTableWidget *m_differences;
    QString m_notConnectedMessage;
    DiscoverClient *m_discoverClient;
    int m_discoverAttempts;
    int m_currentDiscoverAttempts = 0;

};

#endif // CLIENTPAGE_H
