// SPDX-FileCopyrightText: 2019-2020 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "DifferencesDelegate.h"

// Qt includes
#include <QPainter>
#include <QDebug>
#include <QApplication>

static constexpr int s_offset = 3;
static const QString s_ellipsis = QStringLiteral("…");

DifferencesDelegate::DifferencesDelegate(QObject *parent) : TableDelegate(parent)
{
}

void DifferencesDelegate::drawText(QPainter *painter, const QStyleOptionViewItem &option,
                                   const QModelIndex &index) const
{
    if (! index.data(Qt::UserRole).isValid()) {
        TableDelegate::drawText(painter, option, index);
        return;
    }

    painter->save();

    const QPen defaultPen = painter->pen();

    QStyleOptionViewItem opt = option;
    initStyleOption(&opt, index);
    opt.text.clear();

    QStyle *style = opt.widget ? opt.widget->style() : QApplication::style();
    style->drawControl(QStyle::CE_ItemViewItem, &opt, painter, opt.widget);

    const int ellipsisWidth = painter->fontMetrics().boundingRect(s_ellipsis).width();
    const int rightBorder = opt.rect.left() + opt.rect.width() - s_offset;

    QRect drawRect;
    int color = 1;
    int partWidth;
    bool overflow = false;

    opt.rect.moveRight(opt.rect.right() + s_offset);

    const QStringList parts = index.data(Qt::UserRole).toStringList();
    const int partsCount = parts.count();
    for (int i = 0; i < partsCount; i++) {
        color++;
        color = color % 2;
        if (color) {
            painter->setPen(Qt::red);
        } else {
            painter->setPen(defaultPen);
        }

        partWidth = painter->fontMetrics().horizontalAdvance(parts.at(i));

        drawRect = opt.rect;
        if (drawRect.left() + partWidth + (i == partsCount - 1 ? 0 : ellipsisWidth) > rightBorder) {
            drawRect.setWidth(rightBorder - drawRect.left() - ellipsisWidth);
            overflow = true;
        }

        style->drawItemText(painter, drawRect, opt.displayAlignment, opt.palette, true,
                            parts.at(i));

        if (overflow) {
            drawRect.setLeft(rightBorder - ellipsisWidth);
            drawRect.setWidth(ellipsisWidth);
            style->drawItemText(painter, drawRect, opt.displayAlignment, opt.palette, true,
                                s_ellipsis);
            break;
        }

        opt.rect.moveRight(opt.rect.right() + partWidth);
    }

    painter->restore();
}
