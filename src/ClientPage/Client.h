// SPDX-FileCopyrightText: 2018-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef CLIENT_H
#define CLIENT_H

// Local includes

#include "network/AbstractNetworkEngine.h"
#include "network/ServerProtocol.h"

#include "Database/Database.h"

#include "shared/Markers.h"
#include "shared/Draw.h"
#include "shared/Scores.h"

// Qt includes
#include <QAbstractSocket>

// Local classes
class SharedObjects;
class Settings;
class TournamentSettings;
class StringTools;
class RegistrationPage;
class MarkersWidget;
class ScorePage;
class StopWatchEngine;

// Qt classes
class QHostAddress;
class QTimer;
class QJsonObject;

class Client : public AbstractNetworkEngine
{
    Q_OBJECT

public:
    explicit Client(QObject *parent, int connectTimeout, int requestTimeout,
                    SharedObjects *sharedObjects, RegistrationPage *registrationPage,
                    ScorePage *scorePage, StopWatchEngine *stopWatchEngine);
    void setChecksum(const QString *checksum);
    void connectToServer(const QHostAddress &address, quint16 port);
    void closeConnection();
    QString serverIp() const;
    int serverPort() const;
    int id() const;
    bool connected() const;
    void abortHandshake();
    void checkAbortHandshake();

    // Needed for names syncing before connecting
    void adoptList(const QStringList &names, const QStringList &newNames);

    // Change request functions
    // ========================

    // Registration changes

    void requestRegisterPlayers(const QString &name, int marker);
    void requestRenamePlayers(const QString &name, const QString &newName, int markerId);
    void requestAssemblePair(const QString &player1, const QString &player2,
                             const QString &separator, int assignedMarker);

    void requestDeletePlayers(const QString &name);
    void requestDeleteMarkedPlayers(int markerId);
    void requestDeleteAllPlayers();

    void requestSetMarker(const QString &name, int markerId);
    void requestSetListMarker(int markerId);
    void requestRemoveListMarker(int markerId);

    void requestSetNumber(const QString &name, int number);

    void requestSetBookingChecksum(const QString &name, const QString &checskum);

    void requestSetDisqualified(const QString &name, int round);

    // Draw changes
    void requestSetDraw(int round, const QString &name, const Draw::Seat &seat, int Marker);
    void requestSetListDraw(int round, const QVector<QString> &names,
                            const QVector<Draw::Seat> &draw);
    void requestDeleteAllDraws(int round);

    // Markers definition changes
    void requestAddMarker(const Markers::Marker &marker);
    void requestDeleteMarker(const QString &name, int id, int sequence);
    void requestMoveMarker(const QString &name, int id, int sequence, int direction);
    void requestEditMarker(const QString &name, int id, const QString &newName,
                           const QColor &color, Markers::MarkerSorting sorting);

    // Special markers changes
    void requestSetBookedMarker(const QString &name, int id);
    void requestSinglesManagementChange(const Markers::SinglesManagement &data);

    // Finishing the registration
    void requestFinishRegistration();

    // Score comparison
    void requestScoreChecksumChange(int round);

    // Stop watch synchronization
    void requestStopWatchSynchronization(int id);

public Q_SLOTS:
    // Score comparison
    void requestScore(int round);

Q_SIGNALS:
    void tx();
    void rx();

    void message(const QString &text);
    void connectResult(bool success, const QString &error = QString());
    void connectionError(const QString &text);
    void requestRejected(ServerProtocol::RejectReason reason);
    void statusUpdate(const QString &text);
    void namesDiffer(QStringList &localNames, QStringList &remoteNames);
    void remoteScoreChecksumChanged(int round);

    void compareRanking(const QVector<Database::RankData> &ranking);
    void compareScore(const Scores::Score &score);

private Q_SLOTS:
    void socketConnected();
    void error(QAbstractSocket::SocketError);
    void connectionTimeout();
    void requestTimeout();

private: // Enums
    enum ProcessingTarget {
        NoTarget,
        TargetRegistration,
        TargetMarkers,
        TargetScore,
        TargetStopWatch
    };

private: // Functions
    void statusMessage(const QString &message, int clientId);
    void sendRequest(QJsonObject &jsonObject, ProcessingTarget target,
                     const QString &request, const QString &scrollToTarget = QString());
    void processHandshake(const QJsonObject &jsonObject);
    void verifyChecksum(const QString &checksum);
    void stopProcessingRequest();

    void dataReceived(const QString &action, const QJsonObject &jsonObject) override;

    // Server change announcement handlers
    // ===================================

    // Helper macro to keep the header a bit more tidy
    #define CHANGE_HANDLER(name) void process##name(const QJsonObject &jsonObject, int clientId)

    // Registration changes

    CHANGE_HANDLER(RegisterPlayers);
    CHANGE_HANDLER(RenamePlayers);
    CHANGE_HANDLER(AssemblePair);

    CHANGE_HANDLER(DeletePlayers);
    CHANGE_HANDLER(DeleteMarkedPlayers);
    CHANGE_HANDLER(DeleteAllPlayers);

    CHANGE_HANDLER(SetNumber);

    CHANGE_HANDLER(SetBookingChecksum);

    CHANGE_HANDLER(SetDisqualified);

    CHANGE_HANDLER(SetMarker);
    CHANGE_HANDLER(SetListMarker);
    CHANGE_HANDLER(RemoveListMarker);

    // Draw changes
    CHANGE_HANDLER(SetDraw);
    CHANGE_HANDLER(SetListDraw);
    CHANGE_HANDLER(DeleteAllDraws);

    // Markers definition changes
    CHANGE_HANDLER(AddMarker);
    CHANGE_HANDLER(DeleteMarker);
    CHANGE_HANDLER(MoveMarker);
    CHANGE_HANDLER(EditMarker);

    // Special markers changes
    CHANGE_HANDLER(SetBookedMarker);
    CHANGE_HANDLER(SinglesManagementChange);

    // Finishing the registration
    CHANGE_HANDLER(FinishRegistration);

    // Score comparison
    CHANGE_HANDLER(ScoreChecksumChange);
    CHANGE_HANDLER(ScoreRequest);

    // Stop watch synchronization
    CHANGE_HANDLER(StopWatchSynchronization);

    #undef CHANGE_HANDLER

private: // Variables
    const int m_connectTimeout;
    Database *m_db;
    Settings *m_settings;
    TournamentSettings *m_tournamentSettings;
    StringTools *m_stringTools;

    RegistrationPage *m_registrationPage;
    MarkersWidget *m_markersWidget;
    ScorePage *m_scorePage;
    StopWatchEngine *m_stopWatchEngine;

    const QHash<QString, void(Client::*)(const QJsonObject &, int)> m_requestHandlers;
    const QString *m_checksum;

    int m_id;
    bool m_connected = false;
    bool m_connectionPending = false;
    bool m_handshakePending = false;
    QTimer *m_connectionTimer;
    QTimer *m_requestTimer;
    QVector<ProcessingTarget> m_processingTarget;

};

#endif // CLIENT_H
