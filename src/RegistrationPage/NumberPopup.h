// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef NUMBERPOPUP_H
#define NUMBERPOPUP_H

// Local includes
#include "shared/SelectionPopup.h"

// Qt classes
class QWidget;
class QLabel;
class QSpinBox;
class QPushButton;

class NumberPopup : public SelectionPopup
{
    Q_OBJECT

public:
    explicit NumberPopup(QWidget *parent, const QVector<int> *numbers);
    void show(int number = 0, const QPoint &position = QPoint(-1, -1));
    void setIsPairMode(bool state);
    void checkCurrentNumber();

Q_SIGNALS:
    void requestSetNumber(int number);

private Q_SLOTS:
    void checkNumber(int number);

private: // Variables
    const QVector<int> *m_numbers;
    bool m_everShown = false;
    bool m_numberAssigned = false;
    bool m_isPairMode = true;
    QWidget *m_main;
    QLabel *m_label;
    QLabel *m_currentNumber;
    QSpinBox *m_number;
    QLabel *m_taken;
    QPushButton *m_save;
    QPushButton *m_delete;

};

#endif // NUMBERPOPUP_H
