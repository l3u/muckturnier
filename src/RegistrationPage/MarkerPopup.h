// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef MARKERPOPUP_H
#define MARKERPOPUP_H

// Local includes
#include "shared/SelectionPopup.h"
#include "shared/Markers.h"

class MarkerPopup : public SelectionPopup
{
    Q_OBJECT

public:
    explicit MarkerPopup(QWidget *parent, const QVector<Markers::Marker> &markers);
    void show(int currentMarker, const QPoint &position = QPoint(-1, -1));
    void updateMarkers();

Q_SIGNALS:
    void requestSetMarker(int markerId);

private Q_SLOTS:
    void markerSelected(QPushButton *button);

private: // Variables
    const QVector<Markers::Marker> *m_markers;
    int m_markersCount = -1;
    int m_currentMarker = -1;

};

#endif // MARKERPOPUP_H
