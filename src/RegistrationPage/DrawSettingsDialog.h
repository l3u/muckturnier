// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef DRAWSETTINGSDIALOG_H
#define DRAWSETTINGSDIALOG_H

// Local includes
#include "shared/TitleDialog.h"
#include "shared/Draw.h"

// Local classes
class DrawSettingsWidget;

class DrawSettingsDialog : public TitleDialog
{
    Q_OBJECT

public:
    explicit DrawSettingsDialog(QWidget *parent);
    void updateSettings(const Draw::Settings &settings);
    Draw::Settings settings() const;

private Q_SLOTS:
    void resetValues();

private: // Variables
    DrawSettingsWidget *m_drawSettingsWidget;
    Draw::Settings m_initialSettings;

};

#endif // DRAWSETTINGSDIALOG_H
