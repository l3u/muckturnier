// SPDX-FileCopyrightText: 2023-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef REGISTRATIONSVIEW_H
#define REGISTRATIONSVIEW_H

// Local includes
#include "shared/TableView.h"
#include "shared/RegistrationColumns.h"
#include "shared/Players.h"

// Qt includes
#include <QPointer>

// Local classes
class SharedObjects;
class RegistrationsModel;
class PlayersEditor;

class RegistrationsView : public TableView
{
    Q_OBJECT

public:
    explicit RegistrationsView(SharedObjects *sharedObjects, QWidget *parent = nullptr);
    void setModel(QAbstractItemModel *model) override;
    void selectName(const QString &name, int originalVisibleRow = -1);
    void selectRow(int row, int originalVisibleRow);
    const Players::Data &selectedData() const;
    const QString &selectedName() const;
    void deselectRow();
    int selectedRow() const;
    int selectedVisibleRow() const;
    int visibleRow(int row) const;
    bool isEditing() const;
    void saveEditorState();
    void abortEdit();
    void restoreEditorState();
    void hoverSelectedRowAndKeepHoveredRow();

Q_SIGNALS:
    void requestPopup(RegistrationColumns::Column column);

public Q_SLOTS:
    void editorCreated(QWidget *editor, const QString &name);
    void indexClicked(const QModelIndex &index);
    void editCurrentName();

protected:
    void mousePressEvent(QMouseEvent *event) override;

private: // Variables
    const int m_columnCount;
    RegistrationsModel *m_model;
    Qt::MouseButton m_lastPressedButton;
    int m_lastSelectedRow = -1;
    int m_selectedRow = -1;
    QPointer<PlayersEditor> m_editor;
    QString m_lastEditedName;
    Players::Data m_selectedData;

    // Editor state cache
    QString m_enteredText;
    int m_selectionStart = -1;
    int m_selectionLength = -1;
    int m_cursorPosition = -1;

};

#endif // REGISTRATIONSVIEW_H
