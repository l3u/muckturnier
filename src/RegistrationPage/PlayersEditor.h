// SPDX-FileCopyrightText: 2019-2020 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef PLAYERSEDITOR_H
#define PLAYERSEDITOR_H

// Qt includes
#include <QLineEdit>

class PlayersEditor : public QLineEdit
{
    Q_OBJECT

public:
    explicit PlayersEditor(const QString &text, QWidget *parent);
    bool enterPressed() const;

protected:
    void keyPressEvent(QKeyEvent *event) override;

private: // Variables
    bool m_enterPressed = false;

};

#endif // PLAYERSEDITOR_H
