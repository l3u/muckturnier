// SPDX-FileCopyrightText: 2023-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef REGISTRATIONSMODEL_H
#define REGISTRATIONSMODEL_H

// Local includes

#include "shared/Draw.h"
#include "shared/Players.h"

#include "Database/Database.h"

// Qt includes
#include <QAbstractTableModel>
#include <QIcon>

// Local classes
class SharedObjects;
class ResourceFinder;
class StringTools;
class SearchEngine;
class TournamentSettings;
class MarkersModel;

class RegistrationsModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit RegistrationsModel(QObject *parent, SharedObjects *sharedObjects,
                                const MarkersModel *markersModel);
    int rowCount(const QModelIndex &index = QModelIndex()) const override;
    int columnCount(const QModelIndex & = QModelIndex()) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;

    void clear();
    void refresh();
    const QHash<int, int> &markerCounts() const;
    int markerCount(int marker) const;
    const QVector<int> &assignedTables(int pair) const;
    const QVector<int> &incompleteTables() const;
    const QVector<int> &tableGaps() const;
    const Players::Data &data(int row) const;
    const Players::Data &data(const QString &name) const;
    const QVector<Players::Data> &playersData() const;
    bool hasExactMatch(const QString &name) const;
    bool hasCaseInsensitiveMatch(const QString &name) const;
    QString hasFuzzyMatch(const QString &text, bool phoneticSearch,
                          const QString &exclude = QString()) const;
    int rowForName(const QString &name) const;
    QModelIndex indexForName(const QString &name) const;
    bool drawsPresent() const;
    int drawsCount() const;
    void updateSearchHidden(const QString &text, bool phoneticSearch);
    bool isSearchHidden(int row) const;
    bool isMarkerHidden(int row) const;
    const QString &searchTerm() const;
    const QString &formattedSearchTerm() const;
    const QVector<int> &numbers() const;
    const QString &nameForBookingChecksum(const QString &checksum) const;
    void setDrawRound(int round);
    QString exactOrMatchString(const QVector<QString> &names) const;
    const QHash<int, QVector<int>> &currentDraw() const;
    QVector<int> allIds() const;

private Q_SLOTS:
    void initializeIcons();

private: // Variables
    const int m_columnCount;
    Database *m_db;
    ResourceFinder *m_resourceFinder;
    StringTools *m_stringTools;
    SearchEngine *m_searchEngine;
    TournamentSettings *m_tournamentSettings;
    const MarkersModel *m_markersModel;

    QIcon m_markerIcon;
    QIcon m_drawIcon;
    QIcon m_disqualifiedIcon;
    QString m_searchTerm;
    QString m_formattedSearchTerm;
    bool m_phoneticSearch = false;
    int m_drawRound = 1;

    QVector<Players::Data> m_playersData;
    int m_rowCount = 0;
    QHash<int, QColor> m_markerColors;
    QHash<int, QString> m_markerNames;
    QHash<int, int> m_markerCounts;
    int m_drawsCount = 0;
    bool m_drawsPresent = false;
    QVector<int> m_drawnPair1Tables;
    QVector<int> m_drawnPair2Tables;
    QVector<int> m_incompleteTables;
    QVector<int> m_tableGaps;
    QVector<bool> m_searchHidden;
    QVector<int> m_numbers;
    QHash<int, QVector<int>> m_currentDraw;

};

#endif // REGISTRATIONSMODEL_H
