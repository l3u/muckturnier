// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "NumberPopup.h"

#include "shared/DefaultValues.h"
#include "shared/SharedStyles.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QSpinBox>
#include <QPushButton>
#include <QTimer>
#include <QMessageBox>

NumberPopup::NumberPopup(QWidget *parent, const QVector<int> *numbers)
    : SelectionPopup(parent),
      m_numbers(numbers)
{
    m_main = new QWidget;

    auto *layout = new QVBoxLayout(m_main);
    auto *numberLayout = new QHBoxLayout;
    numberLayout->setContentsMargins(10, 5, 10, 0);
    layout->addLayout(numberLayout);

    m_label = new QLabel;
    numberLayout->addWidget(m_label);

    m_currentNumber = new QLabel;
    m_currentNumber->setAlignment(Qt::AlignCenter);
    numberLayout->addWidget(m_currentNumber);

    m_number = new QSpinBox;
    m_number->setMinimum(1);
    m_number->setMaximum(DefaultValues::maximumPlayersNumber);
    connect(m_number, &QSpinBox::valueChanged, this, &NumberPopup::checkNumber);
    numberLayout->addWidget(m_number);

    m_taken = new QLabel(tr("<span style=\"%1\">Bereits vergeben!</span>").arg(
                            SharedStyles::redText));
    m_taken->setAlignment(Qt::AlignCenter);
    m_taken->hide();
    layout->addWidget(m_taken);

    m_save = textButton(tr("Nummer speichern"));
    connect(m_save, &QPushButton::clicked, this, &SelectionPopup::accept);
    layout->addWidget(m_save);

    m_delete = textButton(tr("Nummer löschen"));
    connect(m_delete, &QPushButton::clicked, this, &SelectionPopup::accept);
    layout->addWidget(m_delete);

    setScrollWidget(m_main);

    connect(this, &QDialog::accepted,
            this, [this]
            {
                Q_EMIT requestSetNumber(m_numberAssigned ? 0 : m_number->value());
            });
}

void NumberPopup::show(int number, const QPoint &position)
{
    m_numberAssigned = number > 0;

    if (m_numberAssigned) {
        m_currentNumber->setText(QString::number(number));
    } else {
        int max = 0;
        for (const auto i : *m_numbers) {
            if (i > max) {
                max = i;
            }
        }
        m_number->setValue(max + 1);
        m_number->clearFocus();
        checkCurrentNumber();
    }

    m_currentNumber->setVisible(m_numberAssigned);
    m_number->setVisible(! m_numberAssigned);
    m_save->setVisible(! m_numberAssigned);
    m_delete->setVisible(m_numberAssigned);

    m_taken->hide();

    if (position == QPoint(-1, -1)) {
        SelectionPopup::show();
    } else {
        SelectionPopup::show(position);
    }

    if (! m_everShown) {
        // Try to get the geometry right, even the first time the whole thing is shown
        QTimer::singleShot(0, this, [this, position]
        {
            m_number->show();
            m_number->adjustSize();
            m_currentNumber->setMinimumSize(m_number->sizeHint());
            m_number->setVisible(! m_numberAssigned);
            m_main->show();
            m_main->adjustSize();
            setScrollWidget(m_main);
            if (position == QPoint(-1, -1)) {
                SelectionPopup::show();
            } else {
                SelectionPopup::show(position);
            }
        });
        m_everShown = true;
    }
}

void NumberPopup::setIsPairMode(bool state)
{
    m_isPairMode = state;
    m_label->setText(m_isPairMode ? tr("Paarnummer:") : tr("Spielernummer:"));
    m_everShown = false;
}

void NumberPopup::checkCurrentNumber()
{
    checkNumber(m_number->value());
}

void NumberPopup::checkNumber(int number)
{
    const auto taken = m_numbers->contains(number);
    m_taken->setVisible(taken);
    m_save->setVisible(! taken);
}
