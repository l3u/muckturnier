// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "MarkerPopup.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QPushButton>
#include <QVariant>

// C++ includes
#include <functional>

MarkerPopup::MarkerPopup(QWidget *parent, const QVector<Markers::Marker> &markers)
    : SelectionPopup(parent),
      m_markers(&markers)
{
}

void MarkerPopup::show(int currentMarker, const QPoint &position)
{
    m_currentMarker = currentMarker;

    // Only "unmarked" is defined --> we can't set any marker
    if (m_markersCount == 1) {
        reject();
        return;
    }

    // Hide the current marker
    for (auto *button : findChildren<QPushButton *>()) {
        button->setVisible(button->property("id").toInt() != m_currentMarker);
    }

    if (position == QPoint(-1, -1)) {
        SelectionPopup::show();
    } else {
        SelectionPopup::show(position);
    }
}

void MarkerPopup::updateMarkers()
{
    m_markersCount = m_markers->count();

    auto *widget = new QWidget;
    auto *layout = new QVBoxLayout(widget);
    layout->setSpacing(0);
    layout->setContentsMargins(10, 2, 12, 0);

    for (const Markers::Marker &marker : *m_markers) {
        auto *button = textButton(marker.name);
        button->setStyleSheet(QStringLiteral("color:") + marker.color.name());
        button->setProperty("id", QVariant(marker.id));
        layout->addWidget(button);
        connect(button, &QPushButton::clicked,
                this, std::bind(&MarkerPopup::markerSelected, this, button));
    }

    widget->adjustSize();
    setScrollWidget(widget);

    if (isVisible()) {
        show(m_currentMarker, geometry().topLeft());
    }
}

void MarkerPopup::markerSelected(QPushButton *button)
{
    Q_EMIT requestSetMarker(button->property("id").toInt());
    accept();
}
