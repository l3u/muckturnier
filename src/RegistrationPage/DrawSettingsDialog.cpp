// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "DrawSettingsDialog.h"

#include "shared/DrawSettingsWidget.h"

// Qt includes
#include <QVBoxLayout>
#include <QDebug>

DrawSettingsDialog::DrawSettingsDialog(QWidget *parent) : TitleDialog(parent, DeleteMode::NoDelete)
{
    setTitle(tr("Parameter Auslosung"));
    addButtonBox(QDialogButtonBox::Save | QDialogButtonBox::Close | QDialogButtonBox::Reset);
    buttonBox()->button(QDialogButtonBox::Save)->setText(tr("Speichern und schließen"));

    connect(buttonBox()->button(QDialogButtonBox::Reset), &QPushButton::clicked,
            this, &DrawSettingsDialog::resetValues);

    m_drawSettingsWidget = new DrawSettingsWidget;
    layout()->addWidget(m_drawSettingsWidget);
}

void DrawSettingsDialog::updateSettings(const Draw::Settings &settings)
{
    m_drawSettingsWidget->updateSettings(settings);
    m_initialSettings = settings;
}

Draw::Settings DrawSettingsDialog::settings() const
{
    return m_drawSettingsWidget->settings();
}

void DrawSettingsDialog::resetValues()
{
    m_drawSettingsWidget->updateSettings(m_initialSettings);
}
