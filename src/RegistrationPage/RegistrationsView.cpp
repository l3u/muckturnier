// SPDX-FileCopyrightText: 2023-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "RegistrationsView.h"
#include "RegistrationsModel.h"
#include "PlayersEditor.h"

#include "shared/Logging.h"
#include "shared/RegistrationColumns.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/IconSizeEngine.h"

// Qt includes
#include <QDebug>
#include <QMouseEvent>

// C++ includes
#include <functional>

RegistrationsView::RegistrationsView(SharedObjects *sharedObjects, QWidget *parent)
    : TableView(parent),
      m_columnCount(RegistrationColumns::columnIndex.count())
{
    setIconSize(sharedObjects->iconSizeEngine()->relativeSize(1.35));
    setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);

    connect(this, &QTableView::clicked, this, &RegistrationsView::indexClicked);
}

void RegistrationsView::setModel(QAbstractItemModel *model)
{
    m_model = qobject_cast<RegistrationsModel *>(model);
    QTableView::setModel(model);
}

void RegistrationsView::mousePressEvent(QMouseEvent *event)
{
    QTableView::mousePressEvent(event);
    m_lastPressedButton = event->button();
    const auto clickedIndex = indexAt(event->pos());
    if (clickedIndex.isValid() && m_lastPressedButton == Qt::RightButton) {
        // Also select the row with a right mouse button click
        indexClicked(clickedIndex);
    } else if (! clickedIndex.isValid()) {
        // Deselect the row
        indexClicked(QModelIndex());
    }
}

void RegistrationsView::indexClicked(const QModelIndex &index)
{
    if (editTriggers() == QAbstractItemView::NoEditTriggers) {
        return;
    }

    const auto row = index.isValid() ? index.row() : -1;

    // Update the selection

    if (row != m_selectedRow) {
        m_selectedData = index.isValid() ? m_model->data(index.row()) : Players::noData;

        m_lastSelectedRow = m_selectedRow;
        m_selectedRow = row;
        tableDelegate()->setSelectedRow(m_selectedRow);

        if (m_lastSelectedRow != -1) {
            for (int i = 0; i < m_columnCount; i++) {
                update(m_model->index(m_lastSelectedRow, i));
            }
            if (m_lastSelectedRow + 1 < m_model->rowCount()) {
                for (int i = 0; i < m_columnCount; i++) {
                    update(m_model->index(m_lastSelectedRow + 1, i));
                }
            }
        }

        if (m_selectedRow != -1) {
            for (int i = 0; i < m_columnCount; i++) {
                update(m_model->index(m_selectedRow, i));
            }
            if (m_selectedRow + 1 < m_model->rowCount()) {
                for (int i = 0; i < m_columnCount; i++) {
                    update(m_model->index(m_selectedRow + 1, i));
                }
            }
        }

        // If no other row is selected and an edit is currently going on: Abort it
        if (m_selectedRow == -1 && m_editor != nullptr) {
            m_editor->clearFocus();
        }
    }

    // Possibly request a popup
    if (m_lastPressedButton == Qt::LeftButton && index.isValid()) {
        const auto column = RegistrationColumns::columnIndex.key(index.column());
        if (   column == RegistrationColumns::Number
            || column == RegistrationColumns::Mark
            || column == RegistrationColumns::Draw) {

            Q_EMIT requestPopup(column);
        }
    }
}

void RegistrationsView::deselectRow()
{
    indexClicked(QModelIndex());
}

void RegistrationsView::editorCreated(QWidget *editor, const QString &name)
{
    hoverSelectedRowAndKeepHoveredRow();
    connect(editor, &QObject::destroyed,
            this, std::bind(&RegistrationsView::setKeepHoveredRow, this, false));

    m_editor = qobject_cast<PlayersEditor *>(editor);
    m_lastEditedName = name;
}

void RegistrationsView::selectName(const QString &name, int originalVisibleRow)
{
    if (name.isEmpty()) {
        qCDebug(MuckturnierLog) << "Requested selecting an empty name";
        deselectRow();
        return;
    }

    const auto row = m_model->rowForName(name);
    if (row == -1) {
        // This should not happen
        qCWarning(MuckturnierLog) << "Requested selecting" << name
                                  << "which is not present in the model!";
        return;
    }

    selectRow(row, originalVisibleRow);
}

void RegistrationsView::selectRow(int row, int originalVisibleRow)
{
    if (isRowHidden(row)) {
        deselectRow();
        return;
    }

    const auto target = m_model->index(row, 0);

    if (originalVisibleRow == -1 || visibleRow(row) != originalVisibleRow) {
        if (row == m_model->rowCount() - 1) {
            scrollToBottom();
        } else {
            scrollTo(target, QAbstractItemView::PositionAtCenter);
        }
    }

    m_selectedRow = -1; // Force updating when calling indexClicked()
    indexClicked(target);
}

const Players::Data &RegistrationsView::selectedData() const
{
    return m_selectedData;
}

const QString &RegistrationsView::selectedName() const
{
    return m_selectedData.name;
}

int RegistrationsView::selectedRow() const
{
    return m_selectedRow;
}

int RegistrationsView::selectedVisibleRow() const
{
    return visibleRow(m_selectedRow);
}

void RegistrationsView::editCurrentName()
{
    edit(m_model->index(m_selectedRow,
                        RegistrationColumns::columnIndex.value(RegistrationColumns::Name)));
}

bool RegistrationsView::isEditing() const
{
    return m_editor != nullptr;
}

void RegistrationsView::saveEditorState()
{
    if (! isEditing()) {
        return;
    }

    m_enteredText = m_editor->text();

    if (m_editor->hasSelectedText()) {
        if (m_editor->cursorPosition() == m_editor->selectionStart()) {
            m_selectionStart = m_editor->selectionStart() + m_editor->selectedText().length();
            m_selectionLength = m_editor->selectedText().length() * -1;
        } else {
            m_selectionStart = m_editor->selectionStart();
            m_selectionLength = m_editor->selectedText().length();
        }
    } else {
        m_selectionStart = -1;
        m_cursorPosition = m_editor->cursorPosition();
    }

    m_editor->clearFocus();
}

void RegistrationsView::abortEdit()
{
    if (! isEditing()) {
        qCWarning(MuckturnierLog) << "abortEdit() requested but no editing going on!";
        return;
    }
    m_editor->clearFocus();
}

void RegistrationsView::restoreEditorState()
{
    if (m_selectedData.name.isEmpty()) {
        qCWarning(MuckturnierLog) << "restoreEditorState() requested, but no name is selected!";
        return;
    }

    const auto index = m_model->indexForName(m_selectedData.name);
    if (! index.isValid()) {
        qCWarning(MuckturnierLog) << "restoreEditorState() for" << m_selectedData.name
                                  << "requested, but the model didn't return a valid index for it!";
        return;
    }

    edit(index);
    m_editor->setText(m_enteredText);
    if (m_selectionStart != -1) {
        m_editor->setSelection(m_selectionStart, m_selectionLength);
    } else {
        m_editor->setCursorPosition(m_cursorPosition);
    }
}

int RegistrationsView::visibleRow(int row) const
{
    if (row == -1) {
        return -1;
    }

    // Get the index at the topmost position
    const auto firstIndex = indexAt(QPoint(0, 0));
    if (! firstIndex.isValid()) {
        return -1;
    }

    const int firstRow = firstIndex.row();
    // Assume all items are visible, without scrollbars
    int lastRow = m_model->rowCount();

    const int viewportHeight = viewport()->height();
    const auto lastIndex = indexAt(QPoint(0, viewportHeight - 1));
    if (lastIndex.isValid()) {
        // If we have an item at the lowest possible position,
        // this one's row is the last possible one
        const bool completelyVisible = visualRect(lastIndex).bottomLeft().y() <= viewportHeight;
        lastRow = lastIndex.row() + completelyVisible;
    }

    // Count all rows that are actually shown above the row we query
    int visibleRow = 0;
    for (int i = firstRow; i < lastRow; i++) {
        if (m_model->index(i, 0).row() == row) {
            return visibleRow;
        }
        visibleRow += ! isRowHidden(i);
    }

    // The row is outside of the viewport
    return -1;
}

void RegistrationsView::hoverSelectedRowAndKeepHoveredRow()
{
    updateHoveredRow(model()->index(m_selectedRow, 0));
    setKeepHoveredRow(true);
}
