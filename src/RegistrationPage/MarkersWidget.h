// SPDX-FileCopyrightText: 2018-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef MARKERSWIDGET_H
#define MARKERSWIDGET_H

// Local includes
#include "shared/Markers.h"

// Qt includes
#include <QWidget>
#include <QHash>
#include <QVector>

// Local classes
class SharedObjects;
class Database;
class Settings;
class TournamentSettings;
class Server;
class Client;
class ChecksumHelper;
class MinimumViewportWidget;
class MinimumViewportScroll;
class MarkersModel;
class MarkersView;
class NumbersComboBox;

// Qt classes
class QComboBox;
class QLineEdit;
class QGroupBox;
class QCheckBox;
class QLabel;
class QStackedLayout;

class MarkersWidget : public QWidget
{
    Q_OBJECT

public:
    enum MarkerData {
        Id = Qt::UserRole,
        Sorting
    };

    explicit MarkersWidget(QWidget *parent, SharedObjects *sharedObjects);
    void updateTournamentMode();
    void reload();
    void reset();
    void updateCounters(const QHash<int, int> &counter);
    int newRegistrationMarker(const QString &name) const;
    int drawnMarker() const;
    const Markers::Marker &markerAfterEdit(const QString &oldName, const QString &name) const;
    void addChecksumData(ChecksumHelper &checksumHelper) const;
    void serverStarted(Server *server);
    void serverStopped();
    void clientConnected(Client *client);
    void clientDisconnected();
    void processNetworkChange();
    void setProcessingRequest(bool processing);
    void updateTournamentState();
    int singleMarker() const;
    int bookedMarker() const;
    int assignedMarker() const;
    void resetSingleConditionString();
    QString singleConditionString() const;
    void setDrawnMarker(int markerId);
    void setBookedMarker(int markerId);
    void setSinglesManagement(const Markers::SinglesManagement &data);
    void warnAboutSpecialMarkers();
    const QVector<Markers::Marker> &markers() const;
    const Markers::Marker &marker(int id) const;
    const MarkersModel *markersModel() const;
    void setShowSpecialMarkersWarning(bool state);

Q_SIGNALS:
    void markersChanged();
    void statusUpdate(const QString &text);
    void visibilityChanged();
    void updatePlayersList();
    void bookedMarkerChange();

private Q_SLOTS:
    void addMarker();
    void saveNewMarker(const Markers::Marker &marker);
    void moveMarker(int direction);
    void editMarker();
    void saveEdit(const Markers::Marker &marker);
    void deleteMarker();
    void toggleBookedMarker(bool state);
    void bookedMarkerChanged();
    void singlesManagementToggled(bool state);
    void singlesManagementChanged();
    void toggleDrawnMarker(bool state);
    void drawnMarkerChanged();
    void updateBookedMarkerBox();

private: // Functions
    bool checkSelectedMarkerChanged(const QString &name, const QString &action);
    void setMarkersComboBoxSelection(QComboBox *comboBox, const int markerId);

private: // Variables
    Database *m_db;
    Settings *m_settings;
    TournamentSettings *m_tournamentSettings;

    MarkersModel *m_markersModel;
    MarkersView *m_markersView;

    QHash<Markers::Type, NumbersComboBox *> m_markersComboBoxes;
    QCheckBox *m_enableDrawnMarker;

    QGroupBox *m_automaticMarkersBox;
    QLabel *m_defaultMarkerLabel;

    QGroupBox *m_bookedMarkerBox;
    QStackedLayout *m_bookedLayout;
    QWidget *m_bookingModeWidget;
    QLabel *m_bookedMarkerLabel;
    QWidget *m_bookedSettingsWidget;

    QGroupBox *m_singlesManagementBox;
    QCheckBox *m_markSingles;
    QComboBox *m_singleCondition;
    QLineEdit *m_singleConditionString;
    MinimumViewportWidget *m_markerSettingsWidget;
    MinimumViewportScroll *m_settingsScroll;

    bool m_showSpecialMarkersWarning = true;
    bool m_bookedMarkerInitialized = false;
    bool m_singleMarkerInitialized = false;

    Server *m_server = nullptr;
    Client *m_client = nullptr;
    bool m_isServer = false;
    bool m_isClient = false;
};

#endif // MARKERSWIDGET_H
