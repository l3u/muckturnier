// SPDX-FileCopyrightText: 2023-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef MARKERSMODEL_H
#define MARKERSMODEL_H

// Local includes
#include "shared/Markers.h"

// Qt includes
#include <QAbstractTableModel>
#include <QIcon>

// Local classes
class SharedObjects;
class Database;
class ResourceFinder;
class StringTools;

class MarkersModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit MarkersModel(QObject *parent, SharedObjects *sharedObjects);
    int rowCount(const QModelIndex &index = QModelIndex()) const override;
    int columnCount(const QModelIndex &index = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;

    void updateMarkersCounts(const QHash<int, int> &count);
    void clear();
    void refresh();
    void toggleVisible(int row);
    void showOnly(int row);
    bool isVisible(int id) const;
    bool allVisible() const;
    const Markers::Marker &data(int row) const;
    const Markers::Marker &marker(int id) const;
    const QVector<Markers::Marker> &markers() const;
    const QString &unmarkedName() const;
    bool isUnmarkedName(const QString &name) const;
    bool isMarkerNamePresent(const QString &name, int skipId = -1) const;

Q_SIGNALS:
    void visibilityChanged();

private Q_SLOTS:
    void initializeIcons();

private: // Variables
    Database *m_db;
    ResourceFinder *m_resourceFinder;
    StringTools *m_stringTools;
    const int m_columnCount = 4;

    QIcon m_unused;
    QIcon m_visible;
    QIcon m_hidden;

    int m_rowCount = 0;
    const QString *m_unmarkedName = nullptr;
    QVector<Markers::Marker> m_markers;
    QHash<int, int> m_rowForId;
    QHash<int, bool> m_isVisible;
    QHash<int, int> m_markersCounts;

};

#endif // MARKERSMODEL_H
