// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "DrawPopup.h"

#include "shared/DefaultValues.h"
#include "shared/SharedStyles.h"

#include "SharedObjects/TournamentSettings.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QSpinBox>
#include <QRadioButton>
#include <QPushButton>
#include <QCheckBox>
#include <QApplication>
#include <QStyle>
#include <QTimer>
#include <QRandomGenerator>
#include <QMessageBox>

// C++ includes
#include <algorithm>

static const QString s_iconButtonStyle = QStringLiteral("QPushButton { padding: 8px; }");
static const QVector<int> s_noData = { 0, 0, 0, 0 };

DrawPopup::DrawPopup(QWidget *parent,
                     TournamentSettings *tournamentSettings,
                     const QVector<int> &assignedPair1Tables,
                     const QVector<int> &assignedPair2Tables,
                     const QHash<int, QVector<int>> &currentDraw,
                     const QHash<int, QVector<int>> &opponents,
                     const QHash<int, QVector<int>> &teamMates)
    : SelectionPopup(parent),
      m_tournamentSettings(tournamentSettings),
      m_drawSettings(&m_tournamentSettings->drawSettings()),
      m_assignedPair1Tables(&assignedPair1Tables),
      m_assignedPair2Tables(&assignedPair2Tables),
      m_currentDraw(&currentDraw),
      m_allOpponents(&opponents),
      m_allTeamMates(&teamMates)
{
    connect(m_tournamentSettings, &TournamentSettings::drawSettingsChanged, this, [this]
            {
                if (m_drawRound == 1) {
                    setUpdateNeeded();
                }
            });

    connect(this, &QDialog::accepted, this, &DrawPopup::emitRequestSetDraw);

    m_main = new QWidget;
    QVBoxLayout *layout = new QVBoxLayout(m_main);
    layout->setContentsMargins(8, 2, 10, 8);

    m_selection = new QWidget;
    auto *selectionLayout = new QHBoxLayout(m_selection);

    m_isSinglePlayer = new QLabel(tr("<span style=\"%1\">Allein gekommener Spieler!</span>").arg(
                                     SharedStyles::redText));
    m_isSinglePlayer->setStyleSheet(QStringLiteral("QLabel { margin-top: 12px; }"));
    m_isSinglePlayer->setAlignment(Qt::AlignCenter);
    layout->addWidget(m_isSinglePlayer);

    selectionLayout->addWidget(new QLabel(tr("Tisch")));

    m_table = new QSpinBox;
    m_table->setMinimum(1);
    m_table->setMaximum(DefaultValues::maximumDrawTables);
    connect(m_table, QOverload<int>::of(&QSpinBox::valueChanged),
            this, &DrawPopup::checkSelection);
    selectionLayout->addWidget(m_table);

    auto *pairLayout = new QVBoxLayout;
    selectionLayout->addLayout(pairLayout);

    m_pair1 = new QRadioButton(tr("Paar 1"));
    connect(m_pair1, &QRadioButton::toggled, this, &DrawPopup::checkSelection);
    pairLayout->addWidget(m_pair1);

    m_pair2 = new QRadioButton(tr("Paar 2"));
    pairLayout->addWidget(m_pair2);

    auto *buttonLayout = new QVBoxLayout;
    selectionLayout->addLayout(buttonLayout);

    m_newDraw = new QPushButton;
    m_newDraw->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    m_newDraw->setIcon(QApplication::style()->standardIcon(QStyle::SP_BrowserReload));
    m_newDraw->setStyleSheet(s_iconButtonStyle);
    m_newDraw->setToolTip(tr("Neu auslosen"));
    connect(m_newDraw, &QPushButton::clicked, this, &DrawPopup::drawSeat);
    buttonLayout->addWidget(m_newDraw);

    auto *setToFirstFree = new QPushButton;
    setToFirstFree->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    setToFirstFree->setIcon(QApplication::style()->standardIcon(QStyle::SP_ArrowLeft));
    setToFirstFree->setStyleSheet(s_iconButtonStyle);
    setToFirstFree->setToolTip(tr("Auf ersten freien Platz setzen"));
    connect(setToFirstFree, &QPushButton::clicked, this, &DrawPopup::selectFirstFreeSeat);
    buttonLayout->addWidget(setToFirstFree);

    layout->addWidget(m_selection);

    m_taken = new QLabel(tr("<span style=\"%1\">Bereits vergeben!</span>").arg(
                            SharedStyles::redText));
    m_taken->setAlignment(Qt::AlignCenter);
    layout->addWidget(m_taken);

    m_beyondLimit = new QLabel(tr("<span style=\"%1\">Tischvergabe außerhalb des Limits!"
                                  "</span>").arg(SharedStyles::redText));
    m_beyondLimit->setAlignment(Qt::AlignCenter);
    m_beyondLimit->hide();
    layout->addWidget(m_beyondLimit);

    m_teamMateCollision = new QLabel(tr("<span style=\"%1\">Partner schon einmal zugelost!"
                                        "</span>").arg(SharedStyles::redText));
    m_teamMateCollision->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    m_teamMateCollision->setAlignment(Qt::AlignCenter);
    m_teamMateCollision->hide();
    layout->addWidget(m_teamMateCollision);

    m_opponentCollision = new QLabel(tr("<span style=\"%1\">Gegner schon einmal zugelost!"
                                        "</span>").arg(SharedStyles::redText));
    m_opponentCollision->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    m_opponentCollision->setAlignment(Qt::AlignCenter);
    m_opponentCollision->hide();
    layout->addWidget(m_opponentCollision);

    m_save = textButton(tr("Auslosung speichern"));
    connect(m_save, &QPushButton::clicked, this, &SelectionPopup::accept);
    layout->addWidget(m_save);
    m_taken->setMinimumHeight(m_save->minimumHeight());

    m_delete = textButton(tr("Auslosung löschen"));
    connect(m_delete, &QPushButton::clicked, this, &SelectionPopup::accept);
    layout->addWidget(m_delete);

    setScrollWidget(m_main);
}

int DrawPopup::assignmentsPerPair() const
{
    return m_tournamentSettings->isPairMode() ? 1 : 2;
}

void DrawPopup::setUpdateNeeded()
{
    // Book an update for the next show()
    m_updateNeeded = true;

    if (isVisible()) {
        // This was a network change. We have to update the seats and re-check our selection.
        updateDrawableSeats();
        if (m_save->isVisible()) {
            updateCollisionFreeSeats();
        }
        checkSelection();
    }
}

int DrawPopup::findFirstIncompleteTable(const QVector<int> *list) const
{
    int tableNumber = 1;

    if (assignmentsPerPair() == 1) {
        // It's a bit easier for fixed pairs tournaments ...
        for (const int i : *list) {
            if (i != tableNumber) {
                return tableNumber;
            }
            tableNumber++;
        }
    } else {
        // ... and a bit more complicated for single players ;-)
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
        QVectorIterator<int> table(*list);
#else
        QListIterator<int> table(*list);
#endif
        while (table.hasNext()) {
            table.next();
            if (table.hasNext() && table.next() == tableNumber) {
                tableNumber++;
            } else {
                return tableNumber;
            }
        }
    }

    return tableNumber;
}

void DrawPopup::checkCollision(int table, int pair,
                               bool *teamMateCollision, bool *opponentCollision) const
{
    const auto tableDraw = m_currentDraw->value(table, s_noData);

    *teamMateCollision = m_tournamentSettings->isPairMode()
                             ? false
                             : m_teamMates.contains(tableDraw.at(Draw::indexForPlayer(pair, 1)));

    const auto otherPair = Draw::otherPair(pair);
    *opponentCollision = m_opponents.contains(tableDraw.at(Draw::indexForPlayer(otherPair, 1)))
                         || m_opponents.contains(tableDraw.at(Draw::indexForPlayer(otherPair, 2)));
}

bool DrawPopup::isCollisionFree(int table, int pair) const
{
    bool teamMateCollision;
    bool opponentCollision;
    checkCollision(table, pair, &teamMateCollision, &opponentCollision);

    return    (   (   m_drawSettings->avoidTeamMateCollision
                   && m_drawSettings->avoidOpponentCollision)
               && (   ! teamMateCollision
                   && ! opponentCollision))

           || (   (   m_drawSettings->avoidTeamMateCollision
                   && ! m_drawSettings->avoidOpponentCollision)
               && ! teamMateCollision)

           || (   (   ! m_drawSettings->avoidTeamMateCollision
                   && m_drawSettings->avoidOpponentCollision)
               && ! opponentCollision);
}

void DrawPopup::updateTeamMatesAndOpponents()
{
    m_opponents = m_allOpponents->value(m_id, QVector<int>());
    if (m_tournamentSettings->isSinglePlayerMode()) {
        m_teamMates = m_tournamentSettings->isSinglePlayerMode()
                          ? m_allTeamMates->value(m_id, QVector<int>())
                          : QVector<int>();
    }
}

void DrawPopup::updateDrawableSeats()
{
    if (! m_updateNeeded) {
        // No data change since the last call. Everything up to date, nothing to do.
        return;
    }

    // If we're drawing round 2+, we cache the current ID's past opponents and
    // possibly team mates if we're in single player mode -- if we do have data:
    if (m_drawRound > 1) {
        updateTeamMatesAndOpponents();
    }

    // Find the first seat we can select at all. This is either an incomplete table somewhere
    // between other already (partially) assigned tables, or the next one all are complete.

    const int firstIncompletePair1Table = findFirstIncompleteTable(m_assignedPair1Tables);
    const int firstIncompletePair2Table = findFirstIncompleteTable(m_assignedPair2Tables);

    // Set the first free table and pair
    if (firstIncompletePair1Table <= firstIncompletePair2Table) {
        m_firstFreeSeat.table = firstIncompletePair1Table;
        m_firstFreeSeat.pair = 1;
    } else {
        m_firstFreeSeat.table = firstIncompletePair2Table;
        m_firstFreeSeat.pair = 2;
    }

    // Define the player
    m_firstFreeSeat.player = getPlayer(m_firstFreeSeat);

    // Check if we can draw at all

    m_drawableSeats.clear();
    int maximumTable = 0;

    if (m_drawRound > 1) {
        // Round 2+ --> We want a complete random draw
        m_drawingPossible = true;
        maximumTable = m_allTables;

    } else if (m_drawSettings->consecutive) {
        // We want to do a consecutive assignment
        m_drawingPossible = false;

    } else {
        // We want to do a random draw

        // Check if we can draw at all and get all seats we can draw

        if (m_drawSettings->consecutiveDraw != -1) {
            // We do have a limit for drawing

            if (m_firstFreeSeat.table >= m_drawSettings->consecutiveDraw) {
                // We're already above it --> no drawing anymore
                m_drawingPossible = false;

            } else {
                // We can draw

                if (m_drawSettings->randomDraw != -1
                    && m_firstFreeSeat.table <= m_drawSettings->randomDraw) {

                    // There's a limit up to which we can freely draw and we are below it
                    m_drawingPossible = true;
                    maximumTable = m_drawSettings->randomDraw;

                } else {
                    // Either, there's no range for drawing freely or we're above it, so we do
                    // a close table drawing up to the consecutive drawing limit
                    m_drawingPossible = true;
                    maximumTable = std::min(m_firstFreeSeat.table + m_drawSettings->windowDraw,
                                            m_drawSettings->consecutiveDraw - 1);
                }
            }

        } else {
            // There's no limit for drawing

            if (m_drawSettings->randomDraw != -1
                && m_firstFreeSeat.table <= m_drawSettings->randomDraw) {
                // There's a limit up to which we can freely draw and we are below it

                m_drawingPossible = true;
                maximumTable = m_drawSettings->randomDraw;

            } else {
                // Either, there's no range for drawing freely or we're above it, so we do
                // a close table drawing up to the consecutive drawing limit
                m_drawingPossible = true;
                maximumTable = m_firstFreeSeat.table + m_drawSettings->windowDraw;
            }
        }

        if (m_drawSettings->tablesLimit != -1 && maximumTable > m_drawSettings->tablesLimit) {
            maximumTable = m_drawSettings->tablesLimit;
        }
    }

    // Compile all possible seats if we can draw
    if (m_drawingPossible) {
        for (int table = m_firstFreeSeat.table; table <= maximumTable; table++) {
            for (int pair = 1; pair < 3; pair++) {
                const auto seat = Draw::Seat {
                    table, // table
                    pair,  // pair
                    0      // player is not set yet
                };
                if (! isSeatTaken(seat)) {
                    m_drawableSeats.append(seat);
                }
            }
        }
    }

    // If all drawable seats are empty here, we can't draw
    if (m_drawableSeats.isEmpty()) {
        m_drawingPossible = false;
    }

    // Disable updating until the next model change is announced
    m_updateNeeded = false;
}

void DrawPopup::updateCollisionFreeSeats()
{
    if (! m_tournamentSettings->autoSelectPairs() || m_tournamentSettings->selectByLastRound()
        || (! m_drawSettings->avoidTeamMateCollision && ! m_drawSettings->avoidOpponentCollision)) {

        // We don't auto-select, or we select by last round, or we neither want to prevent team mate
        // collisions, nor do we want to prevent opponent collisions. Thus, the "collision-free"
        // seats are simply all seats we have:
        m_collisionFreeSeats = m_drawableSeats;
        return;
    }

    m_collisionFreeSeats.clear();

    for (const auto &seat : std::as_const(m_drawableSeats)) {
        if (isCollisionFree(seat.table, seat.pair)) {
            m_collisionFreeSeats.append(seat);
        }
    }
}

void DrawPopup::show(int id, Draw::Seat seat, bool isSinglePlayer, const QPoint &position)
{
    m_id = id;
    updateDrawableSeats();

    // Possibly compile a list of collision-free seats, according to the settings
    if (seat.isEmpty()) {
        // If the seat is empty, it's not set, so we want to draw. This will simply mark all
        // drawable seats as "collision free" if we don't check for collisions.
        updateCollisionFreeSeats();
    }

    m_isSinglePlayer->setVisible(isSinglePlayer);
    m_newDraw->setEnabled(m_drawingPossible);

    selectSeat(seat.isEmpty() ? (m_drawingPossible ? getRandomSeat() : m_firstFreeSeat)
                              : seat);

    m_beyondLimit->setVisible(m_drawSettings->tablesLimit != -1
                              && m_table->value() > m_drawSettings->tablesLimit);

    const auto empty = seat.isEmpty();
    m_selection->setEnabled(empty);
    m_teamMateCollision->hide();
    m_opponentCollision->hide();
    m_taken->hide();
    m_save->setVisible(empty);
    m_delete->setVisible(! empty);

    if (empty) {
        checkSelection();
    }

    m_main->adjustSize();

    if (position == QPoint(-1, -1)) {
        SelectionPopup::show();
    } else {
        SelectionPopup::show(position);
    }
}

int DrawPopup::getPlayer(const Draw::Seat &seat) const
{
    if (assignmentsPerPair() == 1) {
        // In fixed pair mode, the "player" is always 1
        // (as there's only one assignment per table and pair number)
        return 1;

    } else {
        // In single players mode, the player depends on if this table and pair number has not been
        // assigned to a player yet (count == 0 --> player = 1) or not (count == 1 --> player = 2)
        if (seat.pair == 1) {
            return m_assignedPair1Tables->count(seat.table) + 1;
        } else {
            return m_assignedPair2Tables->count(seat.table) + 1;
        }
    }
}

Draw::Seat DrawPopup::getRandomSeat() const
{
    // If we have collision-free seats, draw from there. Otherwise from all drawable seats.
    const auto &seats = ! m_collisionFreeSeats.isEmpty() ? m_collisionFreeSeats
                                                         : m_drawableSeats;

    // Get the table and pair (with the player being initialized as 0)
    auto seat = seats.at(QRandomGenerator::global()->bounded(seats.count()));

    // Define the player
    seat.player = getPlayer(seat);

    return seat;
}

void DrawPopup::selectSeat(const Draw::Seat &seat)
{
    m_table->blockSignals(true);
    m_pair1->blockSignals(true);
    m_pair2->blockSignals(true);
    m_table->setValue(seat.table);
    m_pair1->setChecked(seat.pair == 1);
    m_pair2->setChecked(seat.pair == 2);
    m_table->blockSignals(false);
    m_pair1->blockSignals(false);
    m_pair2->blockSignals(false);
}

void DrawPopup::selectFirstFreeSeat()
{
    selectSeat(m_firstFreeSeat);
    checkSelection();
}

void DrawPopup::drawSeat()
{
    selectSeat(getRandomSeat());
    checkSelection();
}

bool DrawPopup::isSeatTaken(const Draw::Seat &seat) const
{
    return seat.pair == 1 ? m_assignedPair1Tables->count(seat.table) == assignmentsPerPair()
                          : m_assignedPair2Tables->count(seat.table) == assignmentsPerPair();
}

void DrawPopup::checkSelection()
{
    const auto table = m_table->value();
    const auto pair  = m_pair1->isChecked() ? 1 : 2;

    // Check if the chosen seat is already taken and if we can save at all
    const bool taken = isSeatTaken(Draw::Seat {
        table, // table
        pair,  // pair
        0      // player is not set yet
    });
    m_taken->setVisible(taken);
    m_save->setVisible(! taken);

    // Possibly check for a team mate and/or opponent collision

    if (m_drawRound > 1
        && ! taken
        && ((m_tournamentSettings->isSinglePlayerMode() && m_drawSettings->warnTeamMateCollision)
            || m_drawSettings->warnOpponentCollision)) {

        bool teamMateCollision;
        bool opponentCollision;
        checkCollision(table, pair, &teamMateCollision, &opponentCollision);
        m_teamMateCollision->setVisible(m_drawSettings->warnTeamMateCollision && teamMateCollision);
        m_opponentCollision->setVisible(m_drawSettings->warnOpponentCollision && opponentCollision);
    } else {
        m_teamMateCollision->hide();
        m_opponentCollision->hide();
    }

    m_main->adjustSize();
    resize(m_main->width(), m_main->height() + 2);
}

void DrawPopup::emitRequestSetDraw()
{
    // Prepare the seat from the selected table and pair
    Draw::Seat seat {
        m_selection->isEnabled() ? m_table->value() : 0, // table
        m_pair1->isChecked() ? 1 : 2,                    // pair
        0                                                // player is not set yet
    };

    // Define the player
    seat.player = getPlayer(seat);

    Q_EMIT requestSetDraw(seat);
}

QVector<Draw::Seat> DrawPopup::completeDraw(const QVector<int> &ids)
{
    // Generate a list of all seats we need
    QVector<Draw::Seat> allSeats;
    for (int table = 1; table <= ids.count() / 2 / assignmentsPerPair(); table++) {
        for (int pair = 1; pair <= 2; pair++) {
            for (int player = 1; player <= assignmentsPerPair(); player++) {
                allSeats.append(Draw::Seat { table, pair, player });
            }
        }
    }

    // Create a shuffled version of all seats
    QVector<Draw::Seat> shuffledSeats;
    while (allSeats.count() > 0) {
        const int i = QRandomGenerator::global()->bounded(allSeats.count());
        shuffledSeats.append(allSeats.at(i));
        allSeats.remove(i);
    }

    if (m_drawRound == 1 ||
        (! m_drawSettings->avoidTeamMateCollision && ! m_drawSettings->avoidOpponentCollision)) {

        // We're drawing for round 1 and/or we don't want to check for collisions.
        // Thus, we're done here and simply return all seats in a shuffled order:
        return shuffledSeats;
    }

    // Otherwise, it's a bit more complicated ;-)

    // We have to use a copy of all team mates and all opponents here,
    // with all draws that already have been set for this round removed

    // Cache the original pointers
    const auto *originalAllOpponents = m_allOpponents;
    const auto *originalAllTeamMates = m_allTeamMates;
    const auto *originalCurrentDraw = m_currentDraw;

    // Create the datasets we will be working on (copying the original opponents and team mates)
    QHash<int, QVector<int>> allOpponents(*m_allOpponents);
    QHash<int, QVector<int>> allTeamMates(*m_allTeamMates);
    QHash<int, QVector<int>> currentDraw; // We start with an empty dataset

    // Remove all team mates and opponents already drawn in this round

    const auto isSinglePlayerMode = m_tournamentSettings->isSinglePlayerMode();

    QHash<int, QVector<int>>::const_iterator it;

    for (it = m_currentDraw->constBegin(); it != m_currentDraw->constEnd(); it++) {
        const auto &players = it.value();
        const auto p0 = players.at(0);
        const auto p2 = players.at(2);

        // Remove pair 1 or pair 1 player 1 and pair 2 or pair 2 player 1 opponents
        if (p0 > 0 && p2 > 0) {
            allOpponents[p0].removeOne(p2);
            allOpponents[p2].removeOne(p0);
        }

        if (isSinglePlayerMode) {
            const auto p1 = players.at(1);
            const auto p3 = players.at(3);

            // Also remove pair 1 player 2 and pair 2 player 2 opponents
            if (p0 > 0 && p3 > 0) {
                allOpponents[p0].removeOne(p3);
                allOpponents[p3].removeOne(p0);
            }
            if (p2 > 0 && p1 > 0) {
                allOpponents[p2].removeOne(p1);
                allOpponents[p1].removeOne(p2);
            }
            if (p3 > 0 && p1 > 0) {
                allOpponents[p3].removeOne(p1);
                allOpponents[p1].removeOne(p3);
            }

            // Remove team mates
            if (p0 > 0 && p1 > 0) {
                allTeamMates[p0].removeOne(p1);
                allTeamMates[p1].removeOne(p0);
            }
            if (p2 > 0 && p3 > 0) {
                allTeamMates[p2].removeOne(p3);
                allTeamMates[p3].removeOne(p2);
            }
        }
    }

    // Update the pointers, so that our local version is used
    m_allOpponents = &allOpponents;
    m_allTeamMates = &allTeamMates;
    m_currentDraw = &currentDraw;

    // Generate a shuffled list of all IDs we draw for. We need to randomize this, because at the
    // end of the draw, it's more likely we will have collisions than at the beginning. Thus, it
    // would always be the last entires to experience collisions if we don't randomize the draw
    QVector<int> allIds(ids);
    QVector<int> shuffledIds;
    while (allIds.count() > 0) {
        const int i = QRandomGenerator::global()->bounded(allIds.count());
        shuffledIds.append(allIds.at(i));
        allIds.remove(i);
    }

    // Prepare a list of drawn seats
    QVector<Draw::Seat> draw(ids.count(), Draw::EmptySeat);

    // Draw a seat for each ID

    for (const int id : shuffledIds) {
        m_id = id;
        updateTeamMatesAndOpponents();

        // Search for a collision-free seat

        int drawnIndex = -1;

        for (int i = 0; i < shuffledSeats.count(); i++) {
            const auto table = shuffledSeats.at(i).table;
            const auto pair = shuffledSeats.at(i).pair;
            if (isCollisionFree(table, pair)) {
                drawnIndex = i;
                break;
            }
        }

        // If we could not find one, use the last index
        if (drawnIndex == -1) {
            drawnIndex = shuffledSeats.count() - 1;
        }

        const auto seat = shuffledSeats.at(drawnIndex);
        draw[ids.indexOf(id)] = seat;
        if (! currentDraw.contains(seat.table)) {
            currentDraw[seat.table] = { 0, 0, 0, 0 };
        }
        currentDraw[seat.table][Draw::indexForPlayer(seat.pair, seat.player)] = id;
        shuffledSeats.remove(drawnIndex);
    }

    // Restore the original pointers
    m_allOpponents = originalAllOpponents;
    m_allTeamMates = originalAllTeamMates;
    m_currentDraw = originalCurrentDraw;

    // Book an update for the next show()
    m_updateNeeded = true;

    return draw;
}

void DrawPopup::setDrawRound(int round, int tables)
{
    m_drawRound = round;
    m_allTables = tables;
    m_table->setMaximum(round == 1 ? DefaultValues::maximumDrawTables : tables);
}
