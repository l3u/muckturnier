// SPDX-FileCopyrightText: 2021 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef DISQUALIFICATIONDIALOG_H
#define DISQUALIFICATIONDIALOG_H

// Local includes
#include "shared/TitleDialog.h"

// Qt classes
class QSpinBox;

class DisqualificationDialog : public TitleDialog
{
    Q_OBJECT

public:
    explicit DisqualificationDialog(QWidget *parent, const QString &namem, bool pairMode,
                                    int rounds);
    int round() const;

private: // Variables
    QSpinBox *m_round;

};

#endif // DISQUALIFICATIONDIALOG_H
