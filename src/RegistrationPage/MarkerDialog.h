// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef MARKERDIALOG_H
#define MARKERDIALOG_H

// Local includes
#include "shared/TitleDialog.h"
#include "shared/Markers.h"

// Local classes
class MarkersModel;

// Qt classes
class QLineEdit;
class QComboBox;
class QLabel;

class MarkerDialog : public TitleDialog
{
    Q_OBJECT

public:
    explicit MarkerDialog(QWidget *parent, MarkersModel *model,
                          Markers::Marker editedMarker = Markers::unmarkedMarker);
    void disableSortingSelection();
    bool isEditing() const;

Q_SIGNALS:
    void markerSaved(const Markers::Marker &marker);

public Q_SLOTS:
    void setColor(const QColor &color);

private Q_SLOTS:
    void chooseColor();
    void accept() override;

private: // Functions
    qreal contrastRatio(const QColor &color1, const QColor &color2) const;
    qreal relativeLuminance(const QColor &color) const;
    qreal prepareColorComponent(qreal component) const;

private: // Variables
    MarkersModel *m_markersModel;
    Markers::Marker m_editedMarker;

    QColor m_background;
    QLineEdit *m_name;
    QLabel *m_badContrast;
    QComboBox *m_sorting;

};

#endif // MARKERDIALOG_H
