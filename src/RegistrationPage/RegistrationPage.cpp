// SPDX-FileCopyrightText: 2010-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "RegistrationPage.h"
#include "MarkersWidget.h"
#include "NumberPopup.h"
#include "MarkerPopup.h"
#include "DrawPopup.h"
#include "DrawSettingsDialog.h"
#include "DisqualificationDialog.h"
#include "Logging.h"

#include "RegistrationsModel.h"
#include "RegistrationsView.h"
#include "RegistrationsDelegate.h"

#include "ServerPage/Server.h"
#include "ClientPage/Client.h"
#include "network/ChecksumHelper.h"

#include "DrawOverviewPage/DrawOverviewPage.h"

#include "BookingPage/BookingEngine.h"

#include "Database/Database.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/StringTools.h"
#include "SharedObjects/SearchEngine.h"
#include "SharedObjects/Settings.h"
#include "SharedObjects/TournamentSettings.h"
#include "SharedObjects/DismissableMessage.h"

#include "shared/SearchWidget.h"
#include "shared/TitleMenu.h"
#include "shared/BackupEngine.h"
#include "shared/SharedStyles.h"
#include "shared/DefaultValues.h"
#include "shared/Tournament.h"
#include "shared/Draw.h"
#include "shared/PhoneticSearchButton.h"
#include "shared/Urls.h"
#include "shared/Players.h"
#include "shared/IconButton.h"
#include "shared/DrawModeMenu.h"
#include "shared/DrawModeHelper.h"
#include "shared/ScanLineEdit.h"
#include "shared/ReadyToScanLabel.h"
#include "shared/Booking.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QHeaderView>
#include <QScrollBar>
#include <QAction>
#include <QMessageBox>
#include <QApplication>
#include <QSplitter>
#include <QCheckBox>
#include <QDir>
#include <QTimer>
#include <QCursor>
#include <QPalette>
#include <QSpinBox>

// C++ includes
#include <functional>
#include <cmath>

static const QVector<QString> s_namePrefixes = {
    QStringLiteral("genannt"),
    QStringLiteral("von"),
    QStringLiteral("und"),
    QStringLiteral("zu"),
    QStringLiteral("van"),
    QStringLiteral("del"),
    QStringLiteral("de"),
    QStringLiteral("da"),
    QStringLiteral("d'"),
    QStringLiteral("d’") // We consider \u2019 here, a typographically correct apostrophe ("’")
};

static const QVector<QString> s_nameConnectors {
    QStringLiteral("-"),
    QStringLiteral("–"),
    QStringLiteral("'"),
    QStringLiteral("’")  // cf. above
};

static const QLatin1Char s_space(' ');

RegistrationPage::RegistrationPage(QWidget *parent, SharedObjects *sharedObjects)
    : QWidget(parent),
      m_db(sharedObjects->database()),
      m_settings(sharedObjects->settings()),
      m_tournamentSettings(sharedObjects->tournamentSettings()),
      m_stringTools(sharedObjects->stringTools()),
      m_searchEngine(sharedObjects->searchEngine()),
      m_dismissableMessage(sharedObjects->dismissableMessage()),
      m_bookingEngine(sharedObjects->bookingEngine())
{
    // Markers
    m_markersWidget = new MarkersWidget(this, sharedObjects);
    connect(m_markersWidget, &MarkersWidget::markersChanged,
            this, &RegistrationPage::markersChanged);
    connect(m_markersWidget, &MarkersWidget::statusUpdate, this, &RegistrationPage::statusUpdate);
    connect(m_markersWidget, &MarkersWidget::visibilityChanged,
            this, std::bind(&RegistrationPage::updatePlayersList, this, QString(), -1));
    connect(m_markersWidget, &MarkersWidget::updatePlayersList,
            this, std::bind(&RegistrationPage::updatePlayersList, this, QString(), -1));

    QVBoxLayout *layout = new QVBoxLayout(this);

    // Register new players/pairs

    auto *registerWidget = new QWidget;
    auto *registerLayout = new QVBoxLayout(registerWidget);
    registerLayout->setContentsMargins(0, 0, 0, 0);

    m_newPlayersBox = new QGroupBox;
    m_newPlayersBox->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Maximum);
    registerLayout->addWidget(m_newPlayersBox);

    auto *newPlayersBoxLayout = new QVBoxLayout(m_newPlayersBox);

    auto *nameLayout = new QHBoxLayout;
    newPlayersBoxLayout->addLayout(nameLayout);
    m_nameLabel = new QLabel;
    nameLayout->addWidget(m_nameLabel);

    m_playersName = new ScanLineEdit;
    nameLayout->addWidget(m_playersName);
    connect(m_playersName, &QLineEdit::textChanged, this, &RegistrationPage::playersNameChanged);
    connect(m_playersName, &QLineEdit::returnPressed, this, &RegistrationPage::registrationEntered);

    m_registerButton = new QPushButton(tr("Anmelden"));
    m_registerButton->setEnabled(false);
    nameLayout->addWidget(m_registerButton);
    connect(m_registerButton, &QPushButton::clicked, this, &RegistrationPage::registrationEntered);

    // Registration options

    auto *optionsLayout = new QHBoxLayout;
    optionsLayout->setContentsMargins(0, 0, 4, 0);
    newPlayersBoxLayout->addLayout(optionsLayout);

    m_registerPhoneticSearch = new PhoneticSearchButton(Settings::registrationPhoneticId,
                                                        sharedObjects);
    connect(m_registerPhoneticSearch, &QAbstractButton::toggled,
            this, &RegistrationPage::registerPhoneticSearchToggled);
    optionsLayout->addWidget(m_registerPhoneticSearch);

    m_searchAsYouType = new IconButton(sharedObjects, QStringLiteral("search_as_you_type.png"),
                                       this);
    m_searchAsYouType->setToolTip(tr("<p>Beim Eingeben die Anmeldeliste durchsuchen</p>"));
    connect(m_searchAsYouType, &QAbstractButton::toggled,
            this, &RegistrationPage::searchAsYouTypeToggled);
    optionsLayout->addWidget(m_searchAsYouType);

    m_autoCapitalize = new IconButton(sharedObjects, QStringLiteral("capitalize.png"), this);
    m_autoCapitalize->setToolTip(tr("<p>Automatische Groß- und Kleinschreibung "
                                    "(de-)aktivierien</p>"));
    m_autoCapitalize->setChecked(m_settings->autoCapitalizeState());
    connect(m_autoCapitalize, &QAbstractButton::toggled,
            m_settings, &Settings::saveAutoCapitalizeState);
    optionsLayout->addWidget(m_autoCapitalize);

    m_searchAsYouTypeResult = new QLabel;
    m_searchAsYouTypeResult->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    optionsLayout->addWidget(m_searchAsYouTypeResult);

    optionsLayout->addStretch();

    m_readyToScanLabel = new ReadyToScanLabel(m_playersName);
    connect(m_tournamentSettings, &TournamentSettings::bookingEnabledChanged,
            this, &RegistrationPage::bookingEnabledChanged);
    optionsLayout->addWidget(m_readyToScanLabel);

    // Registered players/pairs

    m_playersBox = new QGroupBox;
    QVBoxLayout *playersBoxLayout = new QVBoxLayout(m_playersBox);
    registerLayout->addWidget(m_playersBox);

    m_isSearchLabel = new QLabel;
    m_isSearchLabel->setAlignment(Qt::AlignCenter);
    m_isSearchLabel->setWordWrap(true);
    m_isSearchLabel->hide();
    playersBoxLayout->addWidget(m_isSearchLabel);

    m_visibilityDeviation = new QLabel;
    m_visibilityDeviation->setWordWrap(true);
    m_visibilityDeviation->setAlignment(Qt::AlignCenter);
    m_visibilityDeviation->setTextInteractionFlags(Qt::TextBrowserInteraction);
    connect(m_visibilityDeviation, &QLabel::linkActivated, this,
            &RegistrationPage::processStatusLinkClick);
    m_visibilityDeviation->hide();
    playersBoxLayout->addWidget(m_visibilityDeviation);

    m_registrationsModel = new RegistrationsModel(this, sharedObjects,
                                                  m_markersWidget->markersModel());

    m_registrationsDelegate = new RegistrationsDelegate(this);

    m_registrationsView = new RegistrationsView(sharedObjects);
    m_registrationsView->setModel(m_registrationsModel);
    m_registrationsView->setItemDelegate(m_registrationsDelegate);
    m_registrationsView->setContextMenuPolicy(Qt::CustomContextMenu);

    connect(m_registrationsDelegate, &RegistrationsDelegate::editorCreated,
            m_registrationsView, &RegistrationsView::editorCreated);
    connect(m_registrationsDelegate, &RegistrationsDelegate::requestRenamePlayers,
            this, &RegistrationPage::renamePlayers);
    connect(m_registrationsView, &RegistrationsView::requestPopup,
            this, std::bind(&RegistrationPage::popupRequested, this,
                            std::placeholders::_1, QPoint(-1, -1)));
    connect(m_registrationsModel, &QAbstractItemModel::dataChanged,
            m_registrationsView, &QTableView::resizeColumnsToContents);
    connect(m_markersWidget, &MarkersWidget::visibilityChanged,
            this, &RegistrationPage::updateVisibility);
    connect(m_registrationsView, &QTableView::customContextMenuRequested,
            this, &RegistrationPage::showContextMenu);

    m_registrationsView->setColumnHidden(rc::columnIndex.value(rc::Column::Mark), true);
    m_registrationsView->setColumnHidden(rc::columnIndex.value(rc::Column::Draw), true);
    m_registrationsView->setColumnHidden(rc::columnIndex.value(rc::Column::Marker), true);

    auto *header = m_registrationsView->horizontalHeader();
    header->setSectionsClickable(false);
    header->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(header, &QHeaderView::customContextMenuRequested,
            this, &RegistrationPage::showHeaderContextMenu);

    playersBoxLayout->addWidget(m_registrationsView);

    // Search widget

    auto *searchLayout = new QHBoxLayout;
    playersBoxLayout->addLayout(searchLayout);

    m_searchWidget = new SearchWidget(QStringLiteral("registration_page_search"), sharedObjects);
    connect(m_searchWidget, &SearchWidget::searchChanged, this, &RegistrationPage::updateSearch);
    searchLayout->addWidget(m_searchWidget);

    // Arrange the registering part and the markers part in a splitter

    QSplitter *splitter = new QSplitter(this);
    splitter->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Expanding);
    splitter->setHandleWidth(2);
    splitter->setStyleSheet(SharedStyles::visibleHorizontalSplitter);

    splitter->addWidget(registerWidget);
    splitter->setCollapsible(0, false);
    splitter->setStretchFactor(0, 100);

    splitter->addWidget(m_markersWidget);
    splitter->setCollapsible(1, false);
    splitter->setStretchFactor(1, 1);

    layout->addWidget(splitter);

    // Wrapper widget for the status label and the options settings menu button
    auto *statusWidget = new QWidget;
    statusWidget->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Maximum);
    auto *statusLayout = new QHBoxLayout(statusWidget);
    statusLayout->setContentsMargins(0, 0, 0, 0);
    layout->addWidget(statusWidget);

    // Draw round selection

    m_drawRoundWidget = new QWidget;
    m_drawRoundWidget->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Preferred);
    auto *drawRoundLayout = new QHBoxLayout(m_drawRoundWidget);
    statusLayout->addWidget(m_drawRoundWidget);

    auto *drawRoundLabel = new QLabel(tr("Auslosung\nfür Runde"));
    drawRoundLayout->addWidget(drawRoundLabel);

    m_drawRoundSpinBox = new QSpinBox;
    m_drawRoundSpinBox->setMinimum(1);
    m_drawRoundSpinBox->setMaximum(DefaultValues::maximumDrawRounds);
    connect(m_drawRoundSpinBox, &QSpinBox::valueChanged, this, &RegistrationPage::drawRoundChanged);
    drawRoundLayout->addWidget(m_drawRoundSpinBox);

    auto *line = new QFrame;
    line->setFrameShape(QFrame::VLine);
    line->setProperty("isseparator", true);
    drawRoundLayout->addWidget(line);

    // Status label
    m_statusLabel = new QLabel;
    m_statusLabel->setWordWrap(true);
    m_statusLabel->setTextInteractionFlags(Qt::TextBrowserInteraction);
    m_statusLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Maximum);
    connect(m_statusLabel, &QLabel::linkActivated, this, &RegistrationPage::processStatusLinkClick);
    statusLayout->addWidget(m_statusLabel);

    // Settings menu button

    m_menuButton = new QPushButton(tr("Einstellungen"));
    m_menuButton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    auto *settingsMenu = new QMenu(this);
    m_menuButton->setMenu(settingsMenu);

    m_columnsMenu = new TitleMenu(tr("Angezeigte Spalten"), this);
    m_columnsMenu->setTitleHidden(true);

    for (const rc::Column column : rc::allColumns) {
        m_showColumn.insert(column, m_columnsMenu->addAction(rc::checkBoxText.value(column)));
        m_showColumn[column]->setCheckable(true);
        connect(m_showColumn.value(column), &QAction::toggled,
                this, std::bind(&RegistrationPage::setColumnVisible, this, column));
    }

    m_columnsMenu->addSeparator();
    m_hideHeaderAction = m_columnsMenu->addAction(tr("Kopfzeile ausblenden"));
    m_hideHeaderAction->setCheckable(true);
    connect(m_hideHeaderAction, &QAction::toggled, this, &RegistrationPage::setHideHeader);

    settingsMenu->addMenu(m_columnsMenu);

    settingsMenu->addSeparator();

    m_drawModeMenu = new DrawModeMenu(DrawModeMenu::DrawTexts, m_tournamentSettings, this);
    connect(m_drawModeMenu, &DrawModeMenu::drawModeChanged, this, [this]
            {
                QMessageBox::information(this, tr("Auslosungsmodus geändert"),
                    tr("<p><b>Folgende Einstellungen gelten jetzt für die Auslosung:</b></p>")
                    + DrawModeHelper::description(m_tournamentSettings->autoSelectPairs(),
                                                  m_tournamentSettings->selectByLastRound()));
            });
    settingsMenu->addMenu(m_drawModeMenu);

    m_showDrawSettingsAction = settingsMenu->addAction(tr("Parameter Auslosung"));
    connect(m_showDrawSettingsAction, &QAction::triggered,
            this, &RegistrationPage::showDrawSettings);

    auto *menuButtonLayout = new QVBoxLayout;
    statusLayout->addLayout(menuButtonLayout);
    menuButtonLayout->addStretch();
    menuButtonLayout->addWidget(m_menuButton);

    // Number popup
    m_numberPopup = new NumberPopup(this, &m_registrationsModel->numbers());
    connect(m_numberPopup, &SelectionPopup::aboutToShow,
            m_registrationsView, &RegistrationsView::hoverSelectedRowAndKeepHoveredRow);
    connect(m_numberPopup, &QDialog::finished,
            m_registrationsView, std::bind(&RegistrationsView::setKeepHoveredRow,
                                           m_registrationsView, false));
    connect(m_numberPopup, &NumberPopup::requestSetNumber, this, &RegistrationPage::setNumber);

    // Marker popup
    m_markerPopup = new MarkerPopup(this, m_markersWidget->markers());
    connect(m_markerPopup, &SelectionPopup::aboutToShow,
            m_registrationsView, &RegistrationsView::hoverSelectedRowAndKeepHoveredRow);
    connect(m_markerPopup, &QDialog::finished,
            m_registrationsView, std::bind(&RegistrationsView::setKeepHoveredRow,
                                           m_registrationsView, false));
    connect(m_markerPopup, &MarkerPopup::requestSetMarker, this, &RegistrationPage::setMarker);

    // Draw popup
    m_drawPopup = new DrawPopup(this,
                                m_tournamentSettings,
                                m_registrationsModel->assignedTables(1),
                                m_registrationsModel->assignedTables(2),
                                m_registrationsModel->currentDraw(),
                                m_db->opponents(),
                                m_db->teamMates());
    connect(m_drawPopup, &SelectionPopup::aboutToShow,
            m_registrationsView, &RegistrationsView::hoverSelectedRowAndKeepHoveredRow);
    connect(m_drawPopup, &QDialog::finished,
            m_registrationsView, std::bind(&RegistrationsView::setKeepHoveredRow,
                                           m_registrationsView, false));
    connect(m_registrationsModel, &QAbstractItemModel::dataChanged,
            m_drawPopup, &DrawPopup::setUpdateNeeded);
    connect(m_drawPopup, &DrawPopup::requestSetDraw, this, &RegistrationPage::setDraw);

    // Context menu

    m_contextMenu = new TitleMenu(this);
    connect(m_contextMenu, &QMenu::aboutToShow,
            m_registrationsView, &RegistrationsView::hoverSelectedRowAndKeepHoveredRow);
    connect(m_contextMenu, &QMenu::aboutToHide,
            this, [this]
            {
                m_registrationsView->setKeepHoveredRow(false);
                m_lastContextMenuPos = m_contextMenu->geometry().topLeft();
            });

    m_assignMenu = m_contextMenu->addMenu(tr("Einzelspieler zuordnen"));
    m_assignMenu->menuAction()->setVisible(false);

    m_editAction = m_contextMenu->addAction(QString());
    connect(m_editAction, &QAction::triggered,
            m_registrationsView, &RegistrationsView::editCurrentName);

    m_deleteAction = m_contextMenu->addAction(QString());
    connect(m_deleteAction, &QAction::triggered, this, &RegistrationPage::deletePlayers);

    m_contextMenu->addSeparator();

    m_setNumberAction = m_contextMenu->addAction(QString());
    connect(m_setNumberAction, &QAction::triggered,
            this, [this]
            {
                popupRequested(rc::Number, m_lastContextMenuPos);
            });

    m_deleteNumberAction = m_contextMenu->addAction(QString());;
    connect(m_deleteNumberAction, &QAction::triggered,
            this, std::bind(&RegistrationPage::setNumber, this, 0));

    m_contextMenu->addSeparator();

    m_setOnlyMarkerAction = m_contextMenu->addAction(QString());
    m_setOnlyMarkerAction->setVisible(false);

    m_setMarkerMenu = m_contextMenu->addMenu(tr("Markieren als"));

    m_removeMarkerAction = m_contextMenu->addAction(tr("Markierung entfernen"));
    connect(m_removeMarkerAction, &QAction::triggered,
            this, std::bind(&RegistrationPage::setMarker, this, 0));

    m_contextMenu->addSeparator();

    m_setDrawAction = m_contextMenu->addAction(tr("Auslosung eingeben"));
    connect(m_setDrawAction, &QAction::triggered,
            this, [this]
            {
                popupRequested(rc::Draw, m_lastContextMenuPos);
            });

    m_deleteDrawAction = m_contextMenu->addAction(tr("Auslosung löschen"));
    connect(m_deleteDrawAction, &QAction::triggered,
            this, std::bind(&RegistrationPage::setDraw, this, Draw::EmptySeat));

    // Disqualify menu

    m_contextMenu->addSeparator();
    m_disqualificationMenu = m_contextMenu->addMenu(tr("Disqualifikation"));

    m_disqualifyAction = m_disqualificationMenu->addAction(tr("Disqualifizieren"));
    connect(m_disqualifyAction, &QAction::triggered, this, &RegistrationPage::setDisqualified);

    m_unDisqualifyAction = m_disqualificationMenu->addAction(tr("Disqualifikation zurücknehmen"));
    connect(m_unDisqualifyAction, &QAction::triggered, this, &RegistrationPage::setDisqualified);

    // Whole list menu

    m_listMenu = new TitleMenu(tr("Ganze Liste"), this);

    m_contextMenu->addSeparator();
    m_contextMenu->addMenu(m_listMenu);

    m_listSetOnlyMarkerAction = m_listMenu->addAction(QString());

    m_listSetMarkerMenu = m_listMenu->addMenu(tr("Markieren als"));

    m_listRemoveOnlyMarkerAction = m_listMenu->addAction(QString());

    m_listRemoveMarkerMenu = m_listMenu->addMenu(tr("Markierungen entfernen"));

    m_listRemoveAllMarkersAction = m_listMenu->addAction(tr("Alle Markierungen entfernen"));
    connect(m_listRemoveAllMarkersAction, &QAction::triggered,
            this, std::bind(&RegistrationPage::setListMarker, this, 0));

    m_listMenu->addSeparator();

    m_deleteOnlyMarkedAction = m_listMenu->addAction(QString());

    m_deleteMarkedMenu = m_listMenu->addMenu(QString());

    m_deleteUnmarkedAction = m_listMenu->addAction(QString());
    connect(m_deleteUnmarkedAction, &QAction::triggered,
            this, std::bind(&RegistrationPage::deleteMarkedPlayers, this, 0));

    m_deleteAllAction = m_listMenu->addAction(QString());
    connect(m_deleteAllAction, &QAction::triggered, this, &RegistrationPage::deleteAllPlayers);

    m_listMenu->addSeparator();

    auto *drawAllSeatsAction = m_listMenu->addAction(tr("Alle Plätze auslosen"));
    connect(drawAllSeatsAction, &QAction::triggered, this, &RegistrationPage::drawAllSeats);

    m_deleteAllDrawsAction = m_listMenu->addAction(tr("Alle Auslosungen löschen"));
    connect(m_deleteAllDrawsAction, &QAction::triggered, this, &RegistrationPage::deleteAllDraws);

    // Set the m_registerPhoneticSearch button's tool tip and style sheet
    registerPhoneticSearchToggled();

    // Restore saved settings
    m_searchAsYouType->setChecked(m_settings->searchAsYouTypeState());
}

void RegistrationPage::reload(bool updateMarkersWidget)
{
    m_newPlayersBox->show();

    switch (m_tournamentSettings->tournamentMode()) {
    case Tournament::FixedPairs:
        m_newPlayersBox->setTitle(tr("Neue Paare anmelden"));
        m_playersBox->setTitle(tr("Angemeldete Paare"));
        m_nameLabel->setText(tr("Name(n):"));
        m_editAction->setText(tr("Name(n) ändern"));
        m_deleteAction->setText(tr("Paar löschen"));
        m_setNumberAction->setText(tr("Paarnummer vergeben"));
        m_deleteNumberAction->setText(tr("Paarnummer löschen"));
        m_deleteMarkedMenu->menuAction()->setText(tr("Alle Paare mit Markierung löschen"));
        m_deleteUnmarkedAction->setText(tr("Alle unmarkierten Paare löschen"));
        m_deleteAllAction->setText(tr("Alle Paare löschen"));
        break;
    case Tournament::SinglePlayers:
        m_newPlayersBox->setTitle(tr("Neue Spieler anmelden"));
        m_playersBox->setTitle(tr("Angemeldete Spieler"));
        m_nameLabel->setText(tr("Name:"));
        m_editAction->setText(tr("Name ändern"));
        m_deleteAction->setText(tr("Spieler löschen"));
        m_setNumberAction->setText(tr("Spielernummer vergeben"));
        m_deleteNumberAction->setText(tr("Spielernummer löschen"));
        m_deleteMarkedMenu->menuAction()->setText(tr("Alle Spieler mit Markierung löschen"));
        m_deleteUnmarkedAction->setText(tr("Alle unmarkierten Spieler löschen"));
        m_deleteAllAction->setText(tr("Alle Spieler löschen"));
        break;
    }

    const auto dbIsChangeable = m_db->isChangeable();

    m_newPlayersBox->setEnabled(dbIsChangeable);
    m_showDrawSettingsAction->setEnabled(dbIsChangeable);

    if (! dbIsChangeable) {
        m_registrationsView->deselectRow();
    }
    m_registrationsView->setEditTriggers(dbIsChangeable ? QAbstractItemView::DoubleClicked
                                                        : QAbstractItemView::NoEditTriggers);
    m_registrationsView->setTrackHovering(dbIsChangeable);

    m_numberPopup->setIsPairMode(m_tournamentSettings->isPairMode());
    m_markersWidget->updateTournamentMode();

    // We have to call this via a QTimer call if the database is opened via command line argument
    // to be sure that everything is constructed and set up before we do this
    QTimer::singleShot(0, this, [this]
    {
        m_hideHeaderAction->setChecked(m_tournamentSettings->registrationListHeaderHidden());
    });

    // This will also trigger updatePlayersList() each
    if (updateMarkersWidget) {
        m_markersWidget->reload();
    } else {
        markersChanged();
    }

    if (! m_db->tournamentStarted() && m_drawRound != 1) {
        m_drawRoundSpinBox->setValue(1);
    }

    Q_EMIT requestUpdateDrawOverviewPage(0, QString());
}

void RegistrationPage::markersChanged()
{
    const auto &markers = m_markersWidget->markers();
    const int count = markers.count();

    // No need to process count < 2, because the actions will be hidden in this case

    if (count == 2) {
        // We have exactly one marker

        m_setOnlyMarkerAction->disconnect();
        m_listSetOnlyMarkerAction->disconnect();
        m_listRemoveOnlyMarkerAction->disconnect();
        m_deleteOnlyMarkedAction->disconnect();

        for (const Markers::Marker &marker : markers) {
            if (marker.id != 0) {
                m_setOnlyMarkerAction->setText(tr("Markieren als „%1“").arg(marker.name));
                connect(m_setOnlyMarkerAction, &QAction::triggered,
                        this, std::bind(&RegistrationPage::setMarker, this, marker.id));

                m_listSetOnlyMarkerAction->setText(tr("Markieren als „%1“").arg(marker.name));
                connect(m_listSetOnlyMarkerAction, &QAction::triggered,
                        this, std::bind(&RegistrationPage::setListMarker, this, marker.id));

                m_listRemoveOnlyMarkerAction->setText(tr("Markierung „%1“ entfernen").arg(
                                                          marker.name));
                connect(m_listRemoveOnlyMarkerAction, &QAction::triggered,
                        this, std::bind(&RegistrationPage::removeListMarker, this, marker.id));

                m_deleteOnlyMarkedAction->setText((m_tournamentSettings->isPairMode()
                    ? tr("Alle als „%1“ markierten Paare löschen")
                    : tr("Alle als „%1“ markierten Spieler löschen")).arg(marker.name));
                m_deleteOnlyMarkedAction->setData(marker.id);
                connect(m_deleteOnlyMarkedAction, &QAction::triggered,
                        this, std::bind(&RegistrationPage::deleteMarkedPlayers, this, marker.id));

                break;
            }
        }
    }

    if (count > 2) {
        // We have multiple markers
        m_setMarkerMenu->clear();
        m_listSetMarkerMenu->clear();
        m_listRemoveMarkerMenu->clear();
        m_deleteMarkedMenu->clear();

        for (const Markers::Marker &marker : markers) {
            if (marker.id == 0) {
                continue;
            }

            QAction *setMarkerAction = m_setMarkerMenu->addAction(marker.name);
            setMarkerAction->setData(marker.id);
            connect(setMarkerAction, &QAction::triggered,
                    this, std::bind(&RegistrationPage::setMarker, this, marker.id));

            QAction *listSetMarkerAction = m_listSetMarkerMenu->addAction(marker.name);
            listSetMarkerAction->setData(marker.id);
            connect(listSetMarkerAction, &QAction::triggered,
                    this, std::bind(&RegistrationPage::setListMarker, this, marker.id));

            QAction *listRemoveMarkerAction = m_listRemoveMarkerMenu->addAction(marker.name);
            listRemoveMarkerAction->setData(marker.id);
            connect(listRemoveMarkerAction, &QAction::triggered,
                    this, std::bind(&RegistrationPage::removeListMarker, this, marker.id));

            QAction *deleteMarkedAction = m_deleteMarkedMenu->addAction(marker.name);
            deleteMarkedAction->setData(marker.id);
            connect(deleteMarkedAction, &QAction::triggered,
                    this, std::bind(&RegistrationPage::deleteMarkedPlayers, this, marker.id));
        }
    }

    m_setOnlyMarkerAction->setVisible(count == 2);
    m_setMarkerMenu->menuAction()->setVisible(count > 2);
    m_removeMarkerAction->setVisible(count > 1);

    m_listSetOnlyMarkerAction->setVisible(count == 2);
    m_listSetMarkerMenu->menuAction()->setVisible(count > 2);
    m_listRemoveOnlyMarkerAction->setVisible(count == 2);
    m_listRemoveMarkerMenu->menuAction()->setVisible(count > 2);
    m_listRemoveAllMarkersAction->setVisible(count > 2);

    m_deleteOnlyMarkedAction->setVisible(count == 2);
    m_deleteMarkedMenu->menuAction()->setVisible(count > 2);
    m_deleteUnmarkedAction->setVisible(count > 2);

    const auto selectedName = m_registrationsView->selectedName();
    updatePlayersList();
    if (! selectedName.isEmpty()) {
        m_registrationsView->selectName(selectedName);
    }

    // Update the markers popup
    m_markerPopup->updateMarkers();
}

void RegistrationPage::updatePlayersList(const QString &changedSelection, int originalVisibleRow)
{
    const auto oldRegistrationsCount = m_registrationsModel->rowCount();
    m_registrationsModel->refresh();
    if (! changedSelection.isEmpty()) {
        m_registrationsView->selectName(changedSelection, originalVisibleRow);
    }
    const auto registrationsCount = m_registrationsModel->rowCount();

    const bool drawsSet = m_registrationsModel->drawsCount() > 0;
    m_deleteAllDrawsAction->setEnabled(drawsSet);
    updateVisibility();

    Q_EMIT listWasModified();

    // Update the context menus

    if (m_listSetMarkerMenu->menuAction()->isVisible()) {
        const auto listSetMarkerMenuActions = m_listSetMarkerMenu->actions();
        for (QAction *action : listSetMarkerMenuActions) {
            action->setEnabled(m_registrationsModel->markerCount(action->data().toInt())
                                   < registrationsCount);
        }
        const auto listRemoveMarkerMenuActions = m_listRemoveMarkerMenu->actions();
        for (QAction *action : listRemoveMarkerMenuActions) {
            action->setEnabled(m_registrationsModel->markerCount(action->data().toInt()) > 0);
        }
        const auto deleteMarkedMenuActions = m_deleteMarkedMenu->actions();
        for (QAction *action : deleteMarkedMenuActions) {
            action->setEnabled(m_registrationsModel->markerCount(action->data().toInt()) > 0);
        }
    }

    if (m_registrationsModel->markerCount(0) == registrationsCount) {
        m_listRemoveOnlyMarkerAction->setEnabled(false);
        m_listRemoveMarkerMenu->setEnabled(false);
        m_listRemoveAllMarkersAction->setEnabled(false);
    } else {
        m_listRemoveOnlyMarkerAction->setEnabled(true);
        m_listRemoveMarkerMenu->setEnabled(true);
        m_listRemoveAllMarkersAction->setEnabled(true);
    }

    if (m_db->tournamentStarted() || m_registrationsModel->markerCount(0) == registrationsCount) {
        m_deleteOnlyMarkedAction->setEnabled(false);
        m_deleteMarkedMenu->setEnabled(false);
        m_deleteUnmarkedAction->setEnabled(false);
    } else {
        m_deleteOnlyMarkedAction->setEnabled(
            m_registrationsModel->markerCount(m_deleteOnlyMarkedAction->data().toInt())
                < registrationsCount);
        m_deleteMarkedMenu->setEnabled(true);
        m_deleteUnmarkedAction->setEnabled(m_registrationsModel->markerCount(0) > 0);
    }

    // Update the GUI

    m_markersWidget->updateCounters(m_registrationsModel->markerCounts());

    m_searchWidget->setVisible(registrationsCount > 0
                               && (! m_searchAsYouType->isChecked() || m_db->tournamentStarted()));
    Q_EMIT registrationsAvailable(registrationsCount > 0);

    const bool oldSinglesPresent = m_singlesPresent;
    const auto singlesCount = m_registrationsModel->markerCount(m_markersWidget->singleMarker());
    m_singlesPresent = singlesCount > 0;
    const bool singlesPresentChanged = oldSinglesPresent != m_singlesPresent;

    const bool oldBookedPresent = m_bookedPresent;
    const auto bookedCount = m_registrationsModel->markerCount(m_markersWidget->bookedMarker());
    m_bookedPresent = bookedCount > 0;
    const bool bookedPresentChanged = oldBookedPresent != m_bookedPresent;

    const auto aboardRegistrationsWithoutSingles = registrationsCount - bookedCount - singlesCount;

    // Compile the registration status text
    // ====================================

    QString registrationStatus;

    registrationStatus.append(m_tournamentSettings->isPairMode()
                                  ? tr("%n Paar(e)", "", aboardRegistrationsWithoutSingles)
                                  : tr("%n Spieler", "", aboardRegistrationsWithoutSingles));

    if (m_singlesPresent) {
        registrationStatus.append(tr(" und "));
        registrationStatus.append(redSpan(tr("%n allein gekommene(r) Spieler", "", singlesCount)));
    }

    registrationStatus.append(tr(" angemeldet"));

    registrationStatus.append(formatNeededTables(
        aboardRegistrationsWithoutSingles * (m_tournamentSettings->isPairMode() ? 2 : 1)
        + singlesCount,
        false));

    if (m_bookedPresent) {
        registrationStatus.append(QStringLiteral("<br/>"));
        registrationStatus.append(redSpan(tr("%n Voranmeldung(en) fehlt/fehlen noch", "",
                                             bookedCount)));
        registrationStatus.append(formatNeededTables(
            (aboardRegistrationsWithoutSingles + bookedCount)
            * (m_tournamentSettings->isPairMode() ? 2 : 1)
            + singlesCount,
            true));
    }

    // Draw status line
    // ----------------

    m_drawAdjustmentNeeded = false;

    if (m_showColumn.value(rc::Column::Draw)->isChecked() && ! drawsSet) {
        registrationStatus.append(tr("<br/>Es wurde noch keine Auslosung eingegeben"));

    } else if (drawsSet) {
        const auto incompleteTables = m_registrationsModel->incompleteTables().count() > 0;
        const auto tableGaps = m_registrationsModel->tableGaps().count() > 0;

        if (m_drawOverviewPage == nullptr) {
            registrationStatus.append(tr("<br/>Auslosung (<a href=\"#drawOverview\">Übersicht"
                                         "</a>): "));
        } else {
            registrationStatus.append(tr("<br/>Auslosung: "));
        }

        const bool unfinished = m_registrationsModel->drawsCount() != registrationsCount;

        registrationStatus.append(unfinished
            ? tr("<a href=\"#unfinished\">Unvollständig</a>; ")
            : tr("Vollständig; "));

        if (incompleteTables) {
            registrationStatus.append(tr("<a href=\"#incompleteTables\">unvollständige Tische"
                                         "</a>"));
        }

        if (tableGaps) {
            registrationStatus.append(incompleteTables
                ? tr("; <a href=\"#tableGaps\">nicht fortlaufend</a>")
                : tr("<a href=\"#tableGaps\">nicht fortlaufend</a>"));
        }

        if (! incompleteTables && ! tableGaps) {
            registrationStatus.append(tr("alle Tische sind fortlaufend und vollständig vergeben"));
        }

        m_drawAdjustmentNeeded = ! unfinished && ! m_singlesPresent && ! m_bookedPresent
                                 && m_db->playersDividable(aboardRegistrationsWithoutSingles)
                                 && (incompleteTables || tableGaps)
                                 && m_drawRound == 1;
        if (m_drawAdjustmentNeeded) {
            registrationStatus.append(tr(" – <a href=\"#adjustDraw\">Auslosung bereinigen</a>"));
        }
    }

    m_statusLabel->setText(registrationStatus);

    // ====================================

    Q_EMIT drawAdjustmentNeeded(m_drawAdjustmentNeeded);

    if (registrationsCount != oldRegistrationsCount
        || singlesPresentChanged || bookedPresentChanged) {

        m_db->setPairsOrPlayersCount(registrationsCount);
        // Emitting the registrationsCountChanged signal will make the MainWindow check for single
        // players being present and hide/show the score dock accordingly, also if the number of
        // registered players wasn't changed by the previous action (e. g. marking an entry as
        // single)
        Q_EMIT playersCountChanged(registrationsCount);
    }

    if (m_db->tournamentStarted()) {
        m_newPlayersBox->hide();
    }

    scoreChanged();

    if (m_isServer || m_isClient) {
        updateChecksum();
    }
}

QString RegistrationPage::redSpan(const QString &text) const
{
    return QStringLiteral("<span style=\"%1\">%2</span>").arg(SharedStyles::redText, text);
}

QString RegistrationPage::formatNeededTables(int players, bool booked) const
{
    const auto maxTables = m_tournamentSettings->drawSettings().tablesLimit;
    const auto tables = int(std::ceil(players / 4.0));
    const auto missing = tables * 4 - players;

    QString text;

    text.append(tr(" – "));

    if (booked) {
        text.append(tr("damit wäre(n) ", "", tables));
    }

    if (maxTables == -1) {
        text.append(tr("%n Tisch(e) besetzt", "", tables));
    } else {
        const auto tableString = tr("%1/%n Tisch(en) besetzt", "", maxTables).arg(tables);
        if (tables <= maxTables) {
            text.append(tableString);
        } else {
            text.append(redSpan(tableString));
        }
    }

    if (missing > 0) {
        if (m_tournamentSettings->isPairMode()) {
            if (! booked) {
                text.append(tr(" – "));
                if (missing == 1) {
                    text.append(redSpan(tr("es fehlt noch ein Spieler!")));
                } else if (missing == 2) {
                    text.append(redSpan(tr("es fehlt noch ein Paar!")));
                } else { // missing == 3
                    text.append(redSpan(tr("es fehlen noch ein Spieler und ein Paar!")));
                }
            } else {
                text.append(tr(" – aber "));
                if (missing == 1) {
                    text.append(redSpan(tr("es würde noch ein Spieler fehlen!")));
                } else if (missing == 2) {
                    text.append(redSpan(tr("es würde noch ein Paar fehlen!")));
                } else { // missing == 3
                    text.append(redSpan(tr("es würden noch ein Spieler und ein Paar fehlen!")));
                }
            }
        } else {
            text.append(tr(" – "));
            if (! booked) {
                text.append(redSpan(tr("es fehlt/fehlen noch %n Spieler!", "", missing)));
            } else {
                text.append(redSpan(tr("es würde(n) noch %n Spieler fehlen!", "", missing)));
            }
        }
    } else if (m_singlesPresent) {
        text.append(tr(" – "));
        if (! booked) {
            text.append(redSpan(tr("die Einzelspieler müssen noch zugeordnet werden!")));
        } else {
            text.append(redSpan(tr("die Einzelspieler müssten noch zugeordnet werden!")));
        }
    }

    return text;
}

void RegistrationPage::scoreChanged()
{
    m_deleteAction->setEnabled(! m_db->tournamentStarted());
    m_deleteAllAction->setEnabled(! m_db->tournamentStarted());
    m_markersWidget->updateTournamentState();
}

void RegistrationPage::reset()
{
    m_registrationsView->deselectRow();
    m_registrationsModel->clear();
    m_newPlayersBox->show();
    m_playersName->clear();
    m_searchWidget->clear();
}

bool RegistrationPage::checkForMarkAsAboard()
{
    // If we're just registering a booking, we don't want to mark a registration as aboard
    if (m_registeringBooking) {
        return false;
    }

    // Check if a "booked" marker is defined
    const auto originalBookedId = m_tournamentSettings->specialMarker(Markers::Booked);
    if (originalBookedId == -1) {
        return false;
    }

    // Check if a possible current search only shows one match
    int onlyVisibleIndex = -1;
    for (int i = 0; i < m_registrationsModel->rowCount(); i++) {
        if (m_actuallyHiddenRow == i) {
            // This row would normally be hidden by the search and is only
            // visible because it's currently selected. Thus, we skip it.
            continue;
        }
        if (! m_registrationsView->isRowHidden(i)) {
            if (onlyVisibleIndex != -1) {
                return false;
            }
            onlyVisibleIndex = i;
        }
    }
    if (onlyVisibleIndex == -1) {
        return false;
    }

    // Check if that one match is currently marked as "booked"
    const auto playersData = m_registrationsModel->data(onlyVisibleIndex);
    if (playersData.marker != originalBookedId) {
        return false;
    }

    const auto originalBookedMarkerData = m_markersWidget->marker(originalBookedId);
    const auto defaultMarkerData = m_markersWidget->marker(m_tournamentSettings->specialMarker(
                                                               Markers::Default));

    if (QMessageBox::question(this,
            tr("Voranmeldung als „gekommen“ markieren"),
            tr("<p><b>Voranmeldung gefunden</b></p>"
               "<p>Die Eingabe „%1“ passt zu genau einer Voranmeldung, nämlich</p>"
               "<p><b>„%2“</b></p>"
               "<p>Soll %3 als „%4“ markiert werden?</p>").arg(
               m_playersName->text().simplified().toHtmlEscaped(),
               playersData.name.toHtmlEscaped(),
               m_tournamentSettings->isPairMode() ? tr("Das Paar") : tr("Der Spieler"),
               defaultMarkerData.name.toHtmlEscaped()),
               QMessageBox::Yes | QMessageBox::No)
        != QMessageBox::Yes) {

        return false;
    }

    if (m_isClient || m_isServer) {
        // We have to check for possible colliding network changes

        // Check for a name change or deletion
        if (! m_registrationsModel->hasExactMatch(playersData.name)) {
            QTimer::singleShot(0, this, [this, playersData]
            {
                QMessageBox::warning(this,
                    tr("Kollidierende Netzwerkänderung"),
                    tr("<p>%1 „%2“ wurde zwischenzeitlich von einem anderen Netzwerkteilnehmer "
                       "entweder umbenannt oder gelöscht. Die Änderung der Markierung ist nicht "
                       "mehr möglich.</p>"
                       "<p>Bitte die Änderung erneut anfragen!</p>").arg(
                       m_tournamentSettings->isPairMode() ? tr("Das Paar") : tr("Der Spieler"),
                       playersData.name.toHtmlEscaped()));
            });
            return true;
        }

        // Check for a change of the "default" marker or "booked" marker settings.
        // The "default marker" can't be changed via network, so we don't have to check it.

        if (originalBookedId != m_tournamentSettings->specialMarker(Markers::Booked)
            || (originalBookedId != -1
                && originalBookedMarkerData
                   != m_markersWidget->marker(m_tournamentSettings->specialMarker(
                                                  Markers::Booked)))) {

            QTimer::singleShot(0, this, [this]
            {
                QMessageBox::warning(this,
                    tr("Kollidierende Netzwerkänderung"),
                    tr("<p>Die Einstellung für die Markierung neuer bzw. abwesender Anmeldungen "
                       "wurde(n) zwischenzeitlich von einem anderen Netzwerkteilnehmer geändert. "
                       "Die Änderung der Markierung ist nicht mehr möglich.</p>"
                       "<p>Bitte die Änderung erneut anfragen!</p>"));
            });

            return true;
        }
    }

    m_playersName->clear();
    m_registrationsView->selectName(playersData.name);
    setMarker(defaultMarkerData.id);

    if (m_isServer) {
        // Be sure to scroll to the changed entry. In client mode, the change is
        // processed by processNetworkChange() anyway, and the scrolling is ensured.
        QTimer::singleShot(0, this, [this, playersData, onlyVisibleIndex]
        {
            m_registrationsView->selectName(playersData.name, onlyVisibleIndex);
        });
    }

    return true;
}

void RegistrationPage::registerBooking(const QString &name)
{
    m_registeringBooking = true;
    registerPlayers(name, m_tournamentSettings->specialMarker(Markers::Booked));
    m_registeringBooking = false;
}

void RegistrationPage::showBooking(const QString &name)
{
    if (! m_registrationsModel->hasExactMatch(name)) {
        qCDebug(RegistrationLog) << "RegistrationPage::showBooking: Requested showing" << name
                                 << "which could not be found";
        return;
    }

    m_registrationsView->selectName(name);
    Q_EMIT raiseMe();
}

void RegistrationPage::setBookingChecksum(const QString &name, const QString &checksum)
{
    if (m_isClient) {
        m_client->requestSetBookingChecksum(name, checksum);
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    Q_EMIT statusUpdate(tr("Registriere Anmeldungscode für „%1“ …").arg(name));

    if (! m_db->setBookingChecksum(name, checksum)) {
        return;
    }

    updatePlayersList();

    Q_EMIT statusUpdate(tr("Anmeldungscode für „%1“ registriert").arg(name));
    QTimer::singleShot(0, this, &RegistrationPage::requestSaveBookingPng);
    QApplication::restoreOverrideCursor();

    if (m_isServer) {
        QApplication::setOverrideCursor(Qt::BusyCursor);
        m_server->broadcastSetBookingChecksum(name, checksum);
        QApplication::restoreOverrideCursor();
    }
}

void RegistrationPage::registrationEntered()
{
    const auto text = m_playersName->text();

    if (m_tournamentSettings->bookingEnabled()) {
        if (m_bookingEngine->looksLikeBookingCode(text)) {
            m_playersName->clear();
            bookingCodeScanned(text);
            return;

        } else if (m_bookingEngine->containsBookingCodeHeader(text)) {
            if (QMessageBox::question(this, tr("Anmeldungscode gescannt?"),
                    tr("<p><b>Versehentlich Anmeldungscode gescannt?</b></p>"
                       "<p>Der eingegebene Name</p>"
                       "<p style=\"margin-left: 1em;\">„%1“</p>"
                       "<p>enthält den Header eines Anmeldungscodes. Evtl. wurde zusätzlich zu "
                       "einer normalen Eingabe versehentlich ein Code gescannt?</p>"
                       "<p><b>Soll die Anmeldung so weiterverarbietet werden?</b></p>").arg(
                       text.toHtmlEscaped()),
                    QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Cancel)
                    != QMessageBox::Yes) {

                return;
            }
        }
    }

    registerPlayers(text.simplified(), -1);
}

void RegistrationPage::registerPlayers(QString name, int marker)
{
    if (! m_registerButton->isEnabled() && ! m_registeringBooking) {
        return;
    }

    // If we have a "booked" marker defined, check if a possible search currently only
    // shows one match, and if that one is marked as "booked", ask to change the marker
    if (checkForMarkAsAboard()) {
        return;
    }

    // If requested, auto-capitalize the name(s)

    if (m_autoCapitalize->isChecked()) {
        for (const auto &connector : s_nameConnectors) {
            name = name.replace(connector, connector + s_space);
        }

        const auto &namesSeparator = m_settings->namesSeparator();
        const auto trimmedNamesSeparator = namesSeparator.trimmed();
        const auto namesSeparatorWithSpaces = QStringLiteral(" %1 ").arg(trimmedNamesSeparator);
        name = name.replace(trimmedNamesSeparator, namesSeparatorWithSpaces);

        const auto nameParts = name.split(s_space, Qt::SkipEmptyParts);

        QStringList processedNameParts;

        for (const auto &part : nameParts) {
            const auto lowerPart = m_stringTools->toLower(part);
            bool isNamePrefix = false;
            for (const auto &prefix : s_namePrefixes) {
                if (lowerPart == prefix) {
                    isNamePrefix = true;
                    break;
                }
            }

            if (isNamePrefix) {
                processedNameParts.append(part);
            } else {
                QString upperCasePart = part;
                upperCasePart.replace(0, 1, m_stringTools->toUpper(QString(upperCasePart[0])));
                processedNameParts.append(upperCasePart);
            }
        }

        name = processedNameParts.join(s_space);
        for (const auto &connector : s_nameConnectors) {
            name = name.replace(connector + s_space, connector);
        }

        name = name.replace(namesSeparatorWithSpaces, namesSeparator);
    }

    // Check for an exact match. In this case, we can't register the pair/player
    if (m_registrationsModel->hasCaseInsensitiveMatch(name)) {
        QMessageBox::warning(this, m_newPlayersBox->title(),
            tr("<p><b>%1 „%2“ kann nicht angemeldet werden</b></p>"
               "<p>Der Name ist bereits vergeben.</p>").arg(
               m_tournamentSettings->isPairMode() ? tr("Das Paar") : tr("Der Spieler"),
               name.toHtmlEscaped()));
        m_playersName->selectAll();
        m_playersName->setFocus();
        return;
    }

    // Check for a fuzzy match. If one is found, display a warning about a possible duplicate
    const auto fuzzyMatch = m_registrationsModel->hasFuzzyMatch(
                                name, m_registerPhoneticSearch->isChecked());
    if (! fuzzyMatch.isEmpty()
        && QMessageBox::warning(this,
            m_newPlayersBox->title(),
            tr("<p>Es ist bereits ein %1 mit einem ähnlichen Namen angemeldet:</p>"
               "<p><table>"
               "<tr><td>Eingegeben:&nbsp;</td><td><i>%2</i></td></tr>"
               "<tr><td>Vorhanden:</td><td><i>%3</i></td></tr>"
               "</table></p>"
               "<p>Soll %4 trotzdem angemeldet werden?</p>").arg(
               m_tournamentSettings->isPairMode() ? tr("Paar") : tr("Spieler"),
               name.toHtmlEscaped(),
               fuzzyMatch.toHtmlEscaped(),
               m_tournamentSettings->isPairMode() ? tr("das Paar") : tr("der Spieler")),
               QMessageBox::Yes | QMessageBox::No, QMessageBox::No)
        == QMessageBox::No) {

           m_playersName->selectAll();
           m_playersName->setFocus();
           return;
    }

    if (m_isClient || m_isServer) {
        // Someone else could have registered the exact name we want to register meanwhile
        if (m_registrationsModel->hasCaseInsensitiveMatch(name)) {
            QMessageBox::warning(this,
                tr("Kollidierende Netzwerkänderung"),
                tr("Es wurde mittlerweile von einem anderen Netzwerkteilnehmer ein %1 mit dem "
                   "Namen „%2“ angemeldet. Die Anmeldung ist nicht mehr möglich und wird "
                   "abgebrochen.").arg(
                   m_tournamentSettings->isPairMode() ? tr("Paar") : tr("Spieler"), name));
            m_playersName->clear();
            return;
        }
    }

    if (marker == -1) {
        marker = m_markersWidget->newRegistrationMarker(name);
    }

    if (m_isClient) {
        m_requestingRegistration = true;
        m_client->requestRegisterPlayers(name, marker);
        m_focusPlayersName = true;
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    Q_EMIT statusUpdate(m_tournamentSettings->isPairMode() ? tr("Melde Paar an …")
                                                           : tr("Melde Spieler an …"));

    if (! m_db->registerPlayers(name, marker)) {
        if (! m_registeringBooking) {
            m_playersName->clear();
        }
        return;
    }

    if (! m_registeringBooking) {
        m_playersName->clear();
        m_playersName->setFocus();
    }

    updatePlayersList();

    if (! m_searchAsYouType->isChecked()) {
        m_searchWidget->clear();
    }

    QTimer::singleShot(0, this, [this, name]
    {
        m_registrationsView->selectName(name);
    });

    if (m_tournamentSettings->bookingEnabled()
        && ! m_registeringBooking && marker
             == m_tournamentSettings->specialMarker(Markers::Booked)) {

        Q_EMIT requestShowBookingData(name);
    }

    Q_EMIT statusUpdate((m_tournamentSettings->isPairMode()
        ? tr("Paar „%1“ angemeldet") : tr("Spieler „%1“ angemeldet")).arg(name));

    QApplication::restoreOverrideCursor();

    if (m_isServer) {
        QApplication::setOverrideCursor(Qt::BusyCursor);
        m_server->broadcastRegisterPlayers(name, marker);
        QApplication::restoreOverrideCursor();
    }
}

void RegistrationPage::renamePlayers(const QString &originalName, const QString &name)
{
    // Check for an existing registration with the name we want to rename to.
    // In this case, we can't rename the pair/player.

    const auto pureCaseChange = m_stringTools->toLower(originalName)
                                    == m_stringTools->toLower(name);

    if (! pureCaseChange && m_registrationsModel->hasCaseInsensitiveMatch(name)) {
        QMessageBox::warning(this,
            m_tournamentSettings->isPairMode() ? tr("Paar umbenennen") : tr("Spieler umbenennen"),
            tr("<p><b>%1 „%2“ kann nicht umbenannt werden:</b></p>"
               "<p>Der Name „%3“ ist bereits vergeben.</p>").arg(
               m_tournamentSettings->isPairMode() ? tr("Das Paar") : tr("Der Spieler"),
               originalName.toHtmlEscaped(), name.toHtmlEscaped()));
        return;
    }

    // Seach for a fuzzy match

    const auto fuzzyMatch = m_registrationsModel->hasFuzzyMatch(name,
                                                       m_registerPhoneticSearch->isChecked(),
                                                       originalName);

    if (! fuzzyMatch.isEmpty()) {
        m_currentMessageBox = new QMessageBox(QMessageBox::Warning,
            m_tournamentSettings->isPairMode() ? tr("Paar umbenennen") : tr("Spieler umbenennen"),
            tr("<p>Umbenennen von %1 „%2“</p>"
               "<p>Es ist bereits ein %1 mit einem ähnlichen Namen angemeldet:</p>"
               "<p><table>"
               "<tr><td>Eingegeben:&nbsp;</td><td><i>%3</i></td></tr>"
               "<tr><td>Vorhanden:</td><td><i>%4</i></td></tr>"
               "</table></p>"
               "<p>Das könnte zu Verwechslungen führen. Soll %5 trotzdem umbenannt "
               "werden?</p>").arg(
               m_tournamentSettings->isPairMode() ? tr("Paar") : tr("Spieler"),
               originalName.toHtmlEscaped(),
               name.toHtmlEscaped(),
               fuzzyMatch.toHtmlEscaped(),
               m_tournamentSettings->isPairMode() ? tr("das Paar") : tr("Der Spieler")),
            QMessageBox::Yes | QMessageBox::No,
            this);
        m_currentMessageBox->setDefaultButton(QMessageBox::No);

        bool abort = ! m_currentMessageBox->exec()
                     || m_currentMessageBox->clickedButton()
                        != m_currentMessageBox->button(QMessageBox::Yes);
        m_currentMessageBox->deleteLater();

        if (abort) {
            return;
        }
    }

    // Check for colliding network changes

    if (m_isClient || m_isServer) {
        // Someone could have deleted or renamed or edited entry
        if (m_registrationsModel->rowForName(originalName) == -1) {
            QMessageBox::warning(this,
                tr("Kollidierende Netzwerkänderung"),
                tr("%1 „%2“ wurde mittlerweile von einem anderen Netzwerkteilnehmer entweder "
                   "umbenannt oder gelöscht. Das Umbenennen ist nicht mehr möglich und wird "
                   "abgebrochen.").arg(
                   m_tournamentSettings->isPairMode() ? tr("Das Paar") : tr("Der Spieler"),
                   originalName));
            return;
        }

        // Someone could have registered a new pair or player with the exact name we want to rename
        // our edited pair or player to or could have renamed another one to that exact name
        if (! pureCaseChange && m_registrationsModel->hasCaseInsensitiveMatch(name)) {
            QMessageBox::warning(this,
                tr("Kollidierende Netzwerkänderung"),
                tr("Es wurde mittlerweile von einem anderen Netzwerkteilnehmer %1 mit dem "
                   "Namen „%2“ angemeldet oder %1 entsprechend umbenannt. Das Umbenennen ist "
                   "nicht mehr möglich und wird abgebrochen.").arg(
                   m_tournamentSettings->isPairMode() ? tr("ein Paar") : tr("ein Spieler"), name));
            return;
        }
    }

    const auto &markerAfterEdit = m_markersWidget->markerAfterEdit(originalName, name);

    if (m_isClient) {
        m_client->requestRenamePlayers(originalName, name, markerAfterEdit.id);
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    QString changeMarkerStartMessage;
    QString changeMarkerFinishedMessage;
    if (markerAfterEdit.id != -1) {
        if (markerAfterEdit.id == 0) {
            const auto &originalMarkerName = m_markersWidget->marker(
                m_registrationsModel->data(originalName).marker).name;
            changeMarkerStartMessage = tr("und entferne die Markierung „%1“").arg(
                                          originalMarkerName);
            changeMarkerFinishedMessage = tr(" und die Markierung „%1“ entfernt").arg(
                                             originalMarkerName);
        } else {
            changeMarkerStartMessage = tr(" und setze die Markierung auf „%1“ ").arg(
                                          markerAfterEdit.name);
            changeMarkerFinishedMessage = tr(" und die Markierung auf „%1“ gesetzt").arg(
                                             markerAfterEdit.name);
        }
    }
    Q_EMIT statusUpdate((m_tournamentSettings->isPairMode() ? tr("Benenne Paar um %1…")
                                                            : tr("Benenne Spieler um %1…")).arg(
                                                                 changeMarkerStartMessage));

    if (! m_db->renamePlayers(originalName, name, markerAfterEdit.id)) {
        return;
    }

    const auto originalVisibleRow
        = m_registrationsView->visibleRow(m_registrationsModel->rowForName(originalName));
    updatePlayersList(name, originalVisibleRow);

    Q_EMIT nameEdited();

    const auto &data = m_registrationsModel->data(name);
    Q_EMIT requestUpdateDrawOverviewPage(0, data.draw.contains(m_drawRound) ? name : QString());

    if (m_tournamentSettings->bookingEnabled()
        &&  data.marker == m_tournamentSettings->specialMarker(Markers::Booked)) {

        Q_EMIT requestShowBookingData(name);
    }

    Q_EMIT statusUpdate((m_tournamentSettings->isPairMode()
        ? tr("Paar „%1“ umbenannt zu „%2“%3")
        : tr("Spieler „%1“ umbenannt zu „%2“%3")).arg(
             originalName, name, changeMarkerFinishedMessage));

    QApplication::restoreOverrideCursor();

    if (m_isServer) {
        QApplication::setOverrideCursor(Qt::BusyCursor);
        m_server->broadcastRenamePlayers(originalName, name, markerAfterEdit.id);
        QApplication::restoreOverrideCursor();
    }
}

void RegistrationPage::updateContextMenu()
{
    const auto &selectedData = m_registrationsView->selectedData();
    const auto singleMarker = m_markersWidget->singleMarker();
    const auto isSinglePlayer = selectedData.marker == singleMarker;

    m_assignMenu->menuAction()->setVisible(isSinglePlayer);
    m_contextMenu->setTitle((m_tournamentSettings->isPairMode() && ! isSinglePlayer
                                ? tr("Paar „%1“") : tr("Spieler „%1“")).arg(selectedData.name));
    if (isSinglePlayer) {
        m_assignMenu->clear();
        for (const auto &data : m_registrationsModel->playersData()) {
            if (data.marker == singleMarker &&  data.name != selectedData.name) {
                auto *action = m_assignMenu->addAction(data.name);
                connect(action, &QAction::triggered,
                        this, std::bind(&RegistrationPage::assignPlayer, this, data.name));
            }
        }
        if (m_assignMenu->actions().count() == 0) {
            auto *action = m_assignMenu->addAction(tr("keiner sonst angemeldet"));
            action->setEnabled(false);
        }

        m_editAction->setText(tr("Name ändern"));
        m_deleteAction->setText(tr("Spieler löschen"));

    } else if (m_tournamentSettings->isPairMode()) {
        m_editAction->setText(tr("Name(n) ändern"));
        m_deleteAction->setText(tr("Paar löschen"));
    }

    m_removeMarkerAction->setEnabled(selectedData.marker != 0);
    for (auto *action : m_setMarkerMenu->actions()) {
        action->setEnabled(action->data().toInt() != selectedData.marker);
    }

    m_setNumberAction->setVisible(selectedData.number <= 0);
    m_deleteNumberAction->setVisible(selectedData.number > 0);

    const bool drawSet = selectedData.draw.contains(m_drawRound);
    m_setDrawAction->setVisible(! drawSet);
    m_deleteDrawAction->setVisible(drawSet);

    m_disqualificationMenu->setEnabled(selectedData.disqualified || m_db->tournamentStarted());
    m_disqualifyAction->setEnabled(! selectedData.disqualified);
    m_unDisqualifyAction->setEnabled(selectedData.disqualified);
}

void RegistrationPage::showContextMenu(QPoint point)
{
    if (m_registrationsModel->rowCount() == 0 || ! m_db->isChangeable()) {
        return;
    }

    if (m_registrationsView->horizontalHeader()->isVisible()) {
        point.setY(point.y() + m_registrationsView->horizontalHeader()->height());
    }

    if (m_registrationsView->selectedName().isEmpty()) {
        m_listMenu->exec(m_registrationsView->mapToGlobal(point));
    } else {
        updateContextMenu();
        m_contextMenu->exec(m_registrationsView->mapToGlobal(point));
    }
}

void RegistrationPage::updateVisibility()
{
    // Get the row for the currently selected name. If no name is selected
    // and/or the selection isn't present anymore, we will get -1.
    const auto selectedRow = m_registrationsModel->rowForName(m_registrationsView->selectedName());

    if (! m_registrationsModel->searchTerm().isEmpty()) {
        int matches = 0;
        int hiddenMatches = 0;
        for (int row = 0; row < m_registrationsModel->rowCount(); row++) {
            if (! m_registrationsModel->isSearchHidden(row)) {
                matches++;
                if (m_registrationsModel->isMarkerHidden(row)) {
                    hiddenMatches++;
                }
            }
        }

        m_searchWidget->updateMatches(matches, hiddenMatches);
        m_registrationsView->resizeColumnsToContents();
        m_isSearchLabel->setText(tr("(Suchergebnis für %1)").arg(
                                    m_registrationsModel->formattedSearchTerm()));
        m_isSearchLabel->show();

        if (m_searchAsYouType->isChecked()) {
            m_searchAsYouTypeResult->setText(m_searchWidget->matchesText());
        } else {
            m_searchAsYouTypeResult->clear();
        }

    } else {
        m_isSearchLabel->hide();
        m_searchAsYouTypeResult->clear();
    }

    int hidden = 0;
    m_actuallyHiddenRow = -1;
    for (int row = 0; row < m_registrationsModel->rowCount(); row++) {
        auto hide = m_registrationsModel->isSearchHidden(row)
                    || m_registrationsModel->isMarkerHidden(row);
        if (hide && row == selectedRow) {
            hide = false;
            m_actuallyHiddenRow = row;
        }
        m_registrationsView->setRowHidden(row, hide);
        hidden += hide;
    }

    if (m_actuallyHiddenRow != -1) {
        m_visibilityDeviation->setText(tr(
            "Die Anmeldung „%1“ wäre jetzt eigentlich ausgeblendet! "
            "<a href=\"#updateVisibility\">ausblenden</a>").arg(
            m_registrationsView->selectedName()));
        m_visibilityDeviation->show();
    } else {
        m_visibilityDeviation->hide();
    }

    if (hidden > 0) {
        m_playersBox->setTitle(m_tournamentSettings->isPairMode()
                                   ? tr("Angemeldete Paare (%n ausgeblendet)", "", hidden)
                                   : tr("Angemeldete Spieler (%n ausgeblendet)", "", hidden));
    } else {
        m_playersBox->setTitle(m_tournamentSettings->isPairMode() ? tr("Angemeldete Paare")
                                                                  : tr("Angemeldete Spieler"));
    }

    m_registrationsView->resizeColumnsToContents();
}

void RegistrationPage::updateSearch(const QString &searchText)
{
    m_registrationsModel->updateSearchHidden(searchText,
        m_searchAsYouType->isChecked() ? m_registerPhoneticSearch->isChecked()
                                       : m_searchWidget->phoneticSearch());
    updateVisibility();
}

void RegistrationPage::resetSearch()
{
    m_searchWidget->clear();
}

void RegistrationPage::setMarker(int markerId)
{
    const auto &marker = m_markersWidget->marker(markerId);
    const auto name = m_registrationsView->selectedName();

    if (markerId == m_tournamentSettings->specialMarker(Markers::Booked)
        && m_db->tournamentStarted()) {

        QMessageBox::warning(this,
            m_tournamentSettings->isPairMode() ? tr("Paar als abwesend markieren")
                                               : tr("Spieler als abwesend markieren"),
            tr("<p>Es wurden bereits Ergebnisse eingegeben!</p>"
               "<p>Nach dem Turnierstart können keine Anmeldungen mehr als vorangemeldet (also"
               "abwesend) markiert werden.</p>"));
        return;
    }

    if (m_isClient) {
        m_client->requestSetMarker(name, marker.id);
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    if (marker.id != 0) {
        Q_EMIT statusUpdate((m_tournamentSettings->isPairMode()
                             ? tr("Markiere Paar „%1“ als „%2“ …")
                             : tr("Markiere Spieler „%1“ als „%2“ …")).arg(name, marker.name));
    } else {
        Q_EMIT statusUpdate((m_tournamentSettings->isPairMode()
                             ? tr("Entferne Markierung von Paar „%1“ …")
                             : tr("Entferne Markierung von Spieler „%1“ …")).arg(name));
    }

    if (! m_db->setMarker(name, marker.id)) {
        return;
    }

    const auto originalVisibleRow = m_registrationsView->visibleRow(
                                        m_registrationsModel->rowForName(name));
    updatePlayersList();
    m_registrationsView->selectName(name, originalVisibleRow);

    if (m_tournamentSettings->bookingEnabled()
        && marker.id == m_tournamentSettings->specialMarker(Markers::Booked)) {

        Q_EMIT requestShowBookingData(name);
    }

    if (marker.id != 0) {
        Q_EMIT statusUpdate((m_tournamentSettings->isPairMode()
                             ? tr("Paar „%1“ als „%2“ markiert")
                             : tr("Spieler „%1“ als „%2“ markiert")).arg(name, marker.name));
    } else {
        Q_EMIT statusUpdate((m_tournamentSettings->isPairMode()
                             ? tr("Markierung von Paar „%1“ entfernt")
                             : tr("Markierung von Spieler „%1“ entfernt")).arg(name));
    }

    QApplication::restoreOverrideCursor();

    if (m_isServer) {
        QApplication::setOverrideCursor(Qt::BusyCursor);
        m_server->broadcastSetMarker(name, marker.id);
        QApplication::restoreOverrideCursor();
    }
}

void RegistrationPage::setListMarker(int markerId)
{
    const auto &marker = m_markersWidget->marker(markerId);

    if (m_isClient) {
        m_client->requestSetListMarker(marker.id);
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    if (marker.id != 0) {
        Q_EMIT statusUpdate(tr("Markiere alle %1 als „%2“").arg(
                               m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler"),
                               marker.name));
    } else {
        Q_EMIT statusUpdate(tr("Entferne alle Markierungen …"));
    }

    if (! m_db->setListMarker(marker.id)) {
        return;
    }

    updatePlayersList();

    if (markerId != 0) {
        Q_EMIT statusUpdate(tr("Alle %1 als „%2“ markiert").arg(
                               m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler"),
                               marker.name));
    } else {
        Q_EMIT statusUpdate(tr("Alle Markierungen entfernt"));
    }
    QApplication::restoreOverrideCursor();

    if (m_isServer) {
        QApplication::setOverrideCursor(Qt::BusyCursor);
        m_server->broadcastSetListMarker(marker.id);
        QApplication::restoreOverrideCursor();
    }
}

void RegistrationPage::removeListMarker(int markerId)
{
    const auto &marker = m_markersWidget->marker(markerId);

    if (m_isClient) {
        m_client->requestRemoveListMarker(marker.id);
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    Q_EMIT statusUpdate(tr("Entferne alle „%1“-Markierungen …").arg(marker.name));

    if (! m_db->removeListMarker(marker.id)) {
        return;
    }

    updatePlayersList();

    Q_EMIT statusUpdate(tr("Alle „%1“-Markierungen entfernt").arg(marker.name));
    QApplication::restoreOverrideCursor();

    if (m_isServer) {
        QApplication::setOverrideCursor(Qt::BusyCursor);
        m_server->broadcastRemoveListMarker(marker.id);
        QApplication::restoreOverrideCursor();
    }
}

void RegistrationPage::deletePlayers()
{
    const auto name = m_registrationsView->selectedData().name;

    const auto isPair = m_tournamentSettings->isPairMode()
        ? (m_registrationsView->selectedData().marker != m_markersWidget->singleMarker())
        : false;

    auto text = (isPair ? tr("<p>Soll das Paar „%1“ wirklich gelöscht werden?</p>")
                        : tr("<p>Soll der Spieler „%1“ wirklich gelöscht werden?</p>")).arg(name);
    if (! m_registrationsModel->data(name).bookingChecksum.isEmpty()) {
        text.append(isPair
            ? tr("<p>Für das Paar wurde bereits ein Anmeldungscode exportiert! ")
            : tr("<p>Für den Spieler wurde bereits ein Anmeldungscode exportiert! "));
        text.append(tr("Wenn die Anmeldung gelöscht wird, dann wird der Code als widerrufen "
                       "hinterlegt und ungültig.</p>"));
    }

    m_currentMessageBox = new QMessageBox(QMessageBox::Question,
                                          isPair ? tr("Paar löschen") : tr("Spieler löschen"),
                                          text,
                                          QMessageBox::Yes | QMessageBox::Cancel,
                                          this);
    m_currentMessageBox->setDefaultButton(QMessageBox::Cancel);
    const auto abort = m_currentMessageBox->exec() != QMessageBox::Yes;
    m_currentMessageBox->deleteLater();

    // If we're a client or a server and another network participant deletes or renames the
    // registration we currently work on, processNetworkChange() will reject the message box.
    // We thus don't have to check again if the entry still exists, because if not, we will return
    // here anyway.
    if (abort) {
        return;
    }

    m_registrationsView->deselectRow();

    if (m_isClient) {
        m_client->requestDeletePlayers(name);
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    Q_EMIT statusUpdate((isPair ? tr("Lösche Paar „%1“ …")
                                : tr("Lösche Spieler „%1“ …")).arg(name));

    if (! m_db->deletePlayers(name)) {
        return;
    }

    updatePlayersList();
    Q_EMIT requestUpdateDrawOverviewPage(0, QString());

    Q_EMIT statusUpdate((isPair ? tr("Paar „%1“ gelöscht")
                                : tr("Spieler „%1“ gelöscht")).arg(name));
    QApplication::restoreOverrideCursor();

    if (m_isServer) {
        QApplication::setOverrideCursor(Qt::BusyCursor);
        m_server->broadcastDeletePlayers(name);
        QApplication::restoreOverrideCursor();
    }
}

void RegistrationPage::cacheChecksum()
{
    if (m_isServer || m_isClient) {
        m_cachedChecksum = m_checksum;
    }
}

bool RegistrationPage::checkCachedChecksum(const QString &title)
{
    if (! m_isClient && ! m_isServer) {
        return true;
    }

    if (m_checksum != m_cachedChecksum) {
        QMessageBox::warning(this, title,
            tr("<p><b>Kollidierende Netzwerkänderung</b></p>"
               "<p>Die Anmeldungsliste wurde zwischenzeitlich von einem anderen Netzwerkteilnehmer "
               "verändert. Die Aktion kann nicht mehr durchgeführt werden und wird abgebrochen."
               "</p>"));
        return false;
    }

    return true;
}

bool RegistrationPage::confirmListDeleteAction(const QString &title, const QString &text)
{
    m_backupFile.clear();

    QMessageBox messageBox(QMessageBox::Warning, title, text,
                           QMessageBox::Yes | QMessageBox::Cancel, this);
    messageBox.setDefaultButton(QMessageBox::Cancel);
    QCheckBox *createBackup = new QCheckBox(tr("Vorher ein Backup der Datenbank anlegen"));
    createBackup->setChecked(true);
    messageBox.setCheckBox(createBackup);

    if (messageBox.exec() == QMessageBox::Cancel) {
        return false;
    }

    if (createBackup->isChecked()) {
        const QString openedDb = m_db->dbFile();
        BackupEngine backupEngine(openedDb);
        if (! m_db->saveSettings()) {
            return false;
        }
        m_db->closeDatabaseConnection();
        bool success = backupEngine.createBackup();
        m_db->openDatabase(openedDb);

        if (! success) {
            QMessageBox::critical(this, title,
                tr("<p><b>Das Anlegen eines Backups ist fehlgeschlagen!</b></p>"
                   "<p>Die Datei konnte nicht angelegt werden. Bitte die Zugriffsrechte von „%1“ "
                   "bzw. dem Verzeichnis „%2“) überprüfen!</p>").arg(
                   backupEngine.backupFile().toHtmlEscaped(),
                   QDir::toNativeSeparators(backupEngine.path()).toHtmlEscaped()));
            return false;
        }

        m_backupFile = backupEngine.backupFile();
    }

    return true;
}

void RegistrationPage::checkInformAboutBackup(const QString &title)
{
    if (m_backupFile.isEmpty()) {
        return;
    }

    QTimer::singleShot(0, this, [this, title]
    {
        QMessageBox::information(this, title,
            tr("Es wurde erfolgreich ein Backup der Datenbank mit dem Dateinamen „%1“ "
               "erstellt.").arg(m_backupFile));
    });
}

void RegistrationPage::deleteMarkedPlayers(int markerId)
{
    const auto &marker = m_markersWidget->marker(markerId);

    cacheChecksum();
    const QString title = markerId != 0
        ? tr("Markierte %1 löschen").arg(
             m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler"))
        : tr("Unmarkierte %1 löschen").arg(
             m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler"));

    if (! confirmListDeleteAction(title, markerId != 0
            ? tr("Sollen wirklich alle als „%1“ markierten %2 gelöscht werden?").arg(
                 marker.name, m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler"))
            : tr("Sollen wirklich alle unmarkierten %1 gelöscht werden?").arg(
                 m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler")))
        || ! checkCachedChecksum(title)) {

        return;
    }

    if (m_isClient) {
        m_client->requestDeleteMarkedPlayers(marker.id);
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    Q_EMIT statusUpdate(markerId != 0
        ? tr("Lösche alle als „%1“ markierten %2 …").arg(
             marker.name, m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler"))
        : tr("Lösche alle unmarkierten %1 …").arg(
             m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler")));

    if (! m_db->deleteMarkedPlayers(marker.id)) {
        return;
    }

    updatePlayersList();
    Q_EMIT requestUpdateDrawOverviewPage(0, QString());
    checkInformAboutBackup(title);

    Q_EMIT statusUpdate(markerId != 0
        ? tr("Alle als „%1“ markierten %2 gelöscht").arg(
             marker.name, m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler"))
        : tr("Alle unmarkierten %1 gelöscht").arg(
             m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler")));
    QApplication::restoreOverrideCursor();

    if (m_isServer) {
        QApplication::setOverrideCursor(Qt::BusyCursor);
        m_server->broadcastDeleteMarkedPlayers(marker.id);
        QApplication::restoreOverrideCursor();
    }
}

void RegistrationPage::deleteAllPlayers()
{
    const QString title = tr("Alle %1 löschen").arg(
                             m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler"));

    if (! confirmListDeleteAction(title,
              tr("Sollen wirklich alle %1 gelöscht werden?").arg(
                 m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler")))) {

        return;
    }

    if (m_isClient) {
        m_client->requestDeleteAllPlayers();
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    Q_EMIT statusUpdate(tr("Lösche alle %1 …").arg(
                           m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler")));

    if (! m_db->deleteAllPlayers()) {
        return;
    }

    updatePlayersList();
    Q_EMIT requestUpdateDrawOverviewPage(0, QString());
    checkInformAboutBackup(title);

    Q_EMIT statusUpdate(tr("Alle %1 gelöscht").arg(
                           m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler")));
    QApplication::restoreOverrideCursor();

    if (m_isServer) {
        QApplication::setOverrideCursor(Qt::BusyCursor);
        m_server->broadcastDeleteAllPlayers();
        QApplication::restoreOverrideCursor();
    }
}

void RegistrationPage::setColumnVisible(rc::Column column)
{
    const bool state = m_showColumn.value(column)->isChecked();

    // If we're about to hide the number ot the draw column,
    // potentially ask if the user really wants this

    bool askAboutHiding = false;
    if (! state && ! m_tournamentSettings->loadingDb()) {
        if (column == rc::Number) {
            askAboutHiding =
                m_registrationsModel->numbers().count() > 0
                && ! m_dismissableMessage->dismissed(DismissableMessage::AskHideNumberColumn);
        } else if (column == rc::Draw) {
            askAboutHiding =
                m_registrationsModel->drawsPresent()
                && ! m_dismissableMessage->dismissed(DismissableMessage::AskHideDrawColumn);
        }
    }

    if (askAboutHiding) {
        DismissableMessage::SessionMessage messageId;
        QString datasetPresent;
        if (column == rc::Number) {
            messageId = DismissableMessage::AskHideNumberColumn;
            datasetPresent = tr("Es wurden bereits %1 eingegeben.").arg(
                                m_tournamentSettings->isPairMode() ? tr("Paarnummern")
                                                                   : tr("Spielernummern"));
        } else if (column == rc::Draw) {
            messageId = DismissableMessage::AskHideDrawColumn;
            datasetPresent = tr("Es wurden bereits Auslosungen eingegeben.");

        } else {
            // This should not happen
            qCWarning(RegistrationLog) << "RegistrationPage::setColumnVisible: Hide column warning"
                                       << "invoked for unhandled column" << column;
            messageId = DismissableMessage::NonExistingFallback;
        }

        if (m_dismissableMessage->question(messageId, this,
                tr("Spalte „%1“ ausblenden").arg(rc::checkBoxText.value(column)),
                tr("%1 Soll die Spalte „%2“ wirklich ausgeblendet werden?").arg(
                   datasetPresent, rc::checkBoxText.value(column)), QMessageBox::No)
            == QMessageBox::No) {

            // Re-check the respective context menu entry
            auto *action = m_showColumn[column];
            action->blockSignals(true);
            action->setChecked(true);
            action->blockSignals(false);

            return;
        }
    }

    m_registrationsView->setColumnHidden(rc::columnIndex.value(column), state == Qt::Unchecked);
    m_registrationsView->resizeColumnsToContents();

    if (column == rc::Draw) {
        if (! state) {
            m_drawRoundSpinBox->setValue(1);
        }
        m_drawRoundWidget->setVisible(state);
    }

    int lastVisibleColumn = 0;

    for (int i = 0; i < rc::columnIndex.count(); i++) {
        if (i == rc::columnIndex.value(column) && state == Qt::Unchecked) {
            continue;
        }

        if (! m_registrationsView->isColumnHidden(i)
            || (i == rc::columnIndex.value(column) && state)) {

            lastVisibleColumn = i;
        }
    }
    m_registrationsDelegate->setLastVisibleColumn(lastVisibleColumn);
    m_registrationsView->setColumnHidden(rc::columnIndex.value(column), state == Qt::Unchecked);
    m_registrationsView->resizeColumnsToContents();

    if (column == rc::Column::Draw && m_db->isOpen()) {
        // We need to call this to update the status
        updatePlayersList();
    }

    // Inform the database
    QVector<rc::Column> columns;
    for (const rc::Column column : rc::allColumns) {
        if (m_showColumn.value(column)->isChecked()) {
            columns.append(column);
        }
    }
    m_tournamentSettings->setRegistrationColumns(columns);

    // If the draw column is shown, ask if the draw overview page should also be shown
    if (column == rc::Column::Draw && state
        && ! m_dismissableMessage->dismissed(DismissableMessage::AskShowDrawOverview)
        && ! m_tournamentSettings->loadingDb()
        && m_drawOverviewPage == nullptr
        && m_dismissableMessage->question(DismissableMessage::AskShowDrawOverview, this,
               tr("Spalte „Auslosung“ anzeigen"),
               tr("Soll die Auslosungs-Übersichts-Seite ebenfalls angezeigt werden?"))
           == QMessageBox::Yes) {

        Q_EMIT requestShowDrawOverviewPage(m_drawRound);
        QTimer::singleShot(0, qobject_cast<QWidget *>(parent()), &QWidget::raise);
    }
}

void RegistrationPage::updateChecksum()
{
    ChecksumHelper checksumHelper;

    // Add the players data
    for (const auto &data : m_registrationsModel->playersData()) {
        checksumHelper.addData(data);
    }

    // Add the marker data
    m_markersWidget->addChecksumData(checksumHelper);

    // Add the tournament state
    checksumHelper.addData(m_db->networkTournamentStarted());

    m_checksum = checksumHelper.checksum();
}

const QString &RegistrationPage::checksum() const
{
    return m_checksum;
}

void RegistrationPage::serverStarted(Server *server)
{
    updateChecksum();
    m_server = server;
    m_isServer = true;
    m_markersWidget->serverStarted(server);
}

void RegistrationPage::serverStopped()
{
    m_isServer = false;
    m_markersWidget->serverStopped();
    markersChanged();
    m_checksum.clear();
}

void RegistrationPage::hideContextMenus()
{
    const auto contextMenus = findChildren<QMenu *>();
    for (QMenu *menu : contextMenus) {
        menu->hide();
    }
}

void RegistrationPage::requestRejected()
{
    if (m_requestingRegistration) {
        m_requestingRegistration = false;
    }
}

void RegistrationPage::processNetworkChange(int clientId)
{
    // Damn. This is one nasty function. But screw it. We need it :-P

    // Restore all override cursors. None should be set at this point, but whatever.
    while (QApplication::overrideCursor() != nullptr) {
        QApplication::restoreOverrideCursor();
    }

    if (m_isClient && clientId == m_client->id()) {
        // We're processing our own request. No edit can be going on, no context menu can be
        // visible, and no popup can be shown in this case. We can simply display the change.

        if (m_requestingRegistration) {
            m_requestingRegistration = false;
            m_playersName->clear();
        }

        if (m_focusPlayersName) {
            m_focusPlayersName = false;
            m_playersName->setFocus();
        }

        const auto originalVisibleRow = m_registrationsView->selectedVisibleRow();
        updatePlayersList();

        if (! m_scrollToTarget.isEmpty()) {
            const auto row = m_registrationsModel->rowForName(m_scrollToTarget);
            if (row == -1) {
                itemError(Q_FUNC_INFO, m_scrollToTarget);
                return;
            }
            m_registrationsView->selectRow(row, originalVisibleRow);

            // Possibly request to show the booking data for the entry we requested a change for
            if (m_tournamentSettings->bookingEnabled()) {
                const auto &data = m_registrationsModel->data(row);
                if (data.marker == m_tournamentSettings->specialMarker(Markers::Booked)) {
                    Q_EMIT requestShowBookingData(data.name);
                }
            }
        }

        return;
    }

    // We're processing a change from another client or the server. It's possibly a bit more
    // complicated in this case, as maybe, we're just about to do something.

    // Cache what we have

    const auto selectedName = m_registrationsView->selectedName();
    const auto selectedRow = m_registrationsView->selectedRow();
    const auto originalVisibleRow = m_registrationsView->selectedVisibleRow();
    const auto originalDraw = selectedRow != -1
        ? m_registrationsModel->data(selectedRow).draw.value(m_drawRound, Draw::EmptySeat)
        : Draw::EmptySeat;
    const auto originalNumber = selectedRow != -1
        ? m_registrationsModel->data(selectedRow).number
        : -1;

    const auto editorOpen = m_registrationsView->isEditing();
    if (editorOpen) {
        m_registrationsView->saveEditorState();
    }

    // Update the list
    updatePlayersList();

    // In each case, we try to re-select the previously selected name and try to keep it's position

    const auto targetRow = m_registrationsModel->rowForName(selectedName);
    const auto selectedNamePresent = ! selectedName.isEmpty()
                                     && targetRow != -1
                                     && ! m_registrationsView->isRowHidden(targetRow);

    if (selectedNamePresent) {
        // Re-select the previously selected name
        m_registrationsView->selectRow(targetRow, originalVisibleRow);
        const auto processedVisibleRow = m_registrationsView->selectedVisibleRow();

        // If we wanted to keep the hovered row: Update it
        if (m_registrationsView->keepHoveredRow()) {
            QTimer::singleShot(0, this, [this]
            {
                m_registrationsView->setKeepHoveredRow(false);
                m_registrationsView->hoverSelectedRowAndKeepHoveredRow();
            });
        }

        // Try to scroll so that it's at the same place on screen if the position is different
        if (processedVisibleRow != originalVisibleRow) {
            m_registrationsView->verticalScrollBar()->setValue(
                m_registrationsView->verticalScrollBar()->value()
                - originalVisibleRow + processedVisibleRow);
        }
    }

    // Check if we had the whole list menu and if so, if we can still display it

    if (m_listMenu->isVisible()) {
        // The only thing that would stop us is if somebody deleted all registrations
        if (m_registrationsModel->rowCount() == 0) {
            QTimer::singleShot(0, this, [this]
            {
                // Showing this warning will also close all context menus
                QMessageBox::warning(this,
                    tr("Kollidierende Netzwerkänderung"),
                    tr("Es wurden von einem anderen Netzwerkteilnehmer alle Anmeldungen "
                       "gelöscht. Ein Bearbeiten der ganzen Liste ist nicht mehr möglich."));
            });
            return;
        }

        if (m_contextMenu->isVisible() && ! selectedNamePresent) {
            // If the selected name isn't present anymore, we still can keep showing the whole list
            // menu. Hiding the context menu will also hide the whole list menu, so we have to show
            // it again afterwards
            QTimer::singleShot(0, this, [this]
            {
                const auto pos = m_listMenu->geometry().topLeft();
                m_contextMenu->hide();
                m_listMenu->exec(pos);
            });
        }

        return;
    }

    // If the list menu wasn't visible, and no name was selected before the change, no action was
    // going on. In this case, we're done here.
    if (selectedName.isEmpty()) {
        return;
    }

    // For everything else, we need the previously selected name to be still present.

    if (! selectedNamePresent) {
        // The selected name is not present anymore. We have to abort here.

        // Remove the selection
        m_registrationsView->deselectRow();

        // If we currently display some message box: close it
        if (m_currentMessageBox != nullptr) {
            qCDebug(RegistrationLog) << "Closing the currently displayed message box due to a "
                                        "network collision";
            m_currentMessageBox->reject();
        }

        // Inform the user
        QTimer::singleShot(0, this, [this, selectedName]
        {
            QMessageBox::warning(this,
                tr("Kollidierende Netzwerkänderung"),
                tr("<p>%1 „%2“ wurde durch eine Änderung eines anderen Netzwerkteilnehmers "
                   "entweder umbenannt oder gelöscht.</p>"
                   "<p>Die Auswahl bzw. die laufende Bearbeitung kann nicht wiederhergestellt "
                   "werden.</p>").arg(
                   m_tournamentSettings->isPairMode() ? tr("Das gerade bearbeitete Paar")
                                                      : tr("Der gerade bearbeitete Spieler"),
                   selectedName.toHtmlEscaped()));
        });

        return;
    }

    // If we were editing before the change, we restore the editor's state. In this case, no
    // context menu or popup could have been shown and we're done here.
    if (editorOpen) {
        m_registrationsView->restoreEditorState();
        return;
    }

    // If the context menu is visible, we have to update it. Markers could have changed etc.
    if (m_contextMenu->isVisible()) {
        updateContextMenu();
        return;
    }

    // Check if the number popup is visible
    if (m_numberPopup->isVisible()) {
        // Another network participant could have assigned a number to that very registration
        if (originalNumber
            != m_registrationsModel->data(m_registrationsView->selectedRow()).number) {

            QTimer::singleShot(0, this, [this, selectedName]
            {
                QMessageBox::warning(this, tr("Kollidierende Netzwerkänderung"),
                    tr("<p>Ein anderer Netzwerkteilnehmer hat gerade die %1 für „%2“ vergeben oder "
                       "gelöscht.</p>"
                       "<p>Die Bearbeitung wird abgebrochen.</p>").arg(
                    m_tournamentSettings->isPairMode() ? tr("Paarnummer") : tr("Spielernummer"),
                    selectedName.toHtmlEscaped()));
            });
            m_numberPopup->reject();
            return;
        }

        // Otherwise, we update the availability check for the currently entered number
        m_numberPopup->checkCurrentNumber();
        return;
    }

    // Update the markers popup if it's visible
    if (m_markerPopup->isVisible()) {
        popupRequested(rc::Mark, m_markerPopup->geometry().topLeft());
        return;
    }

    // Update the draw popup if it's visible
    if (m_drawPopup->isVisible()) {
        const auto &data = m_registrationsModel->data(m_registrationsView->selectedRow());

        // The entry could have been marked as "booked"
        if (data.marker == m_tournamentSettings->specialMarker(Markers::Booked)) {
            QTimer::singleShot(0, this, [this, selectedName]
            {
                m_drawPopup->reject();
                QMessageBox::warning(this,
                    tr("Kollidierende Netzwerkänderung"),
                    tr("<p>%1 „%2“ wurde durch eine Änderung eines anderen Netzwerkteilnehmers "
                       "als „abwesend“ markiert.</p>"
                       "<p>Das Eingeben einer Auslosung ist nicht mehr möglich und wurde "
                       "abgebrochen.</p>").arg(
                       m_tournamentSettings->isPairMode() ? tr("Das gerade bearbeitete Paar")
                                                          : tr("Der gerade bearbeitete Spieler"),
                       selectedName.toHtmlEscaped()));
            });
            return;
        }

        // If a draw has been entered by another network user for the processed name,
        // we have to re-show the dialog, with everything else being already processed

        if (m_registrationsModel->data(m_registrationsView->selectedRow())
                .draw.value(m_drawRound, Draw::EmptySeat) != originalDraw) {

            m_drawPopup->reject();
            QTimer::singleShot(0, this, [this, selectedName]
            {
                QMessageBox::warning(this,
                    tr("Kollidierende Netzwerkänderung"),
                    tr("<p>Für %1 „%2“ wurde mittlerweile von einem anderen Netzwerkteilnehmer "
                       "eine Auslosung eingegeben.</p>"
                       "<p>Sofern das wirklich gewollt ist, dann bitte die Änderung der Auslosung "
                       "erneut angfragen.</p>").arg(
                       m_tournamentSettings->isPairMode() ? tr("das Paar") : tr("den Spieler"),
                       selectedName.toHtmlEscaped()));
            });
        }

        return;
    }
}

void RegistrationPage::clientConnected(Client *client)
{
    m_isClient = true;
    m_client = client;
    m_markersWidget->clientConnected(client);
}

void RegistrationPage::clientDisconnected()
{
    m_isClient = false;
    m_markersWidget->clientDisconnected();
    markersChanged();
    m_checksum.clear();
    m_requestingRegistration = false;
}

void RegistrationPage::setProcessingRequest(bool processing, const QString &scrollToTarget)
{
    m_newPlayersBox->setEnabled(! processing);
    m_playersBox->setEnabled(! processing);
    if (processing) {
        m_scrollToTarget = scrollToTarget;
        QApplication::setOverrideCursor(Qt::WaitCursor);
    }
}

MarkersWidget *RegistrationPage::markersWidget() const
{
    return m_markersWidget;
}

void RegistrationPage::finishRegistration()
{
    if (QMessageBox::warning(this,
        tr("Anmeldung beenden"),
        tr("<p>Wenn die Anmeldung beendet ist, kann (mindestens während das Netzwerk läuft) kein "
           "Netzwerkteilnehmer mehr neue %1 anmelden oder löschen!</p>"
           "<p>Soll die Anmeldung wirklich beendet werden?</p>").arg(
           m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler")),
        QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Cancel) == QMessageBox::Cancel) {

        return;
    }

    if (m_isClient) {
        m_client->requestFinishRegistration();
        return;
    }

    // Otherwise, we're the server
    QApplication::setOverrideCursor(Qt::BusyCursor);
    networkFinishRegistration();
    m_server->broadcastFinishRegistration();
    QApplication::restoreOverrideCursor();
}

void RegistrationPage::networkFinishRegistration(int clientId)
{
    QString networkChange;
    if ((m_isClient && clientId != m_client->id()) || (m_isServer && clientId != 0)) {
        networkChange = tr(" von einem anderen Netzwerkteilnehmer");
        if (m_contextMenu->isVisible()) {
            hideContextMenus();
        }
        if (m_registrationsView->isEditing()) {
            m_registrationsView->abortEdit();
        }
    }

    m_db->setNetworkTournamentStarted(true);
    reload();
    Q_EMIT networkTournamentStarted(true);

    QTimer::singleShot(0, this, [this, networkChange]
    {
        QMessageBox::information(this,
            tr("Anmeldung beendet"),
            tr("<p>Die Anmeldung wurde%1 beendet. Es können ab jetzt keine neuen %2 mehr "
               "angemeldet oder bestehende Anmeldungen gelöscht werden.</p>"
               "<p>Sollte das trotzdem noch nötig sein, dann bitte den Server stoppen, neu "
               "starten und alle Clients neu verbinden. Sofern noch keine Ergebnisse eingegeben "
               "wurden, ist danach das komplette Bearbeiten der Anmeldungsliste wieder möglich."
               "</p>").arg(networkChange, m_tournamentSettings->isPairMode() ? tr("Paare")
                                                                             : tr("Spieler")));
    });
}

bool RegistrationPage::checkDrawIsIgnored()
{
    if (! m_tournamentSettings->autoSelectPairs()) {
        if (QMessageBox::warning(this, tr("Auslosung bearbeiten"),
                tr("<p><b>Auslosung bearbeiten</b></p>"
                   "<p>Die automatische Tisch- bzw. %1 ist deaktiviert.</p>"
                   "<p><b>Alle Auslosungen werden ignoriert.<br/>"
                   "Soll die Auslosung trotzdem bearbeitet werden?</b></p>"
                   "<p>Die automatische Auswahl kann auf der Ergebnisse-Seite unter "
                   "„Einstellungen“ → „Automatische Auswahl“ aktiviert werden.</p>").arg(
                   m_tournamentSettings->isPairMode() ? tr("Paarauswahl")
                                                      : tr("Spielerauswahl")),
                QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Cancel)
            != QMessageBox::Yes) {

            return false;
        }

    } else if (m_tournamentSettings->selectByLastRound() && m_drawRound != 1) {
        if (QMessageBox::warning(this, tr("Auslosung bearbeiten"),
                tr("<p><b>Auslosung bearbeiten</b></p>"
                   "<p>Für die automatische Tisch- bzw. %1 ist die Option  „Paar 2 rückt pro "
                   "Runde weiter“ aktiviert. Es wird nur die Auslosung für Runde 1 "
                   "berücksichtigt.</p>"
                   "<p><b>Eingaben für Runde %2 werden ignoriert.<br/>"
                   "Soll die Auslosung trotzdem bearbeitet werden?</b></p>"
                   "<p>Die Einstellungen für die automatische Auswahl können auf der "
                   "Ergebnisse-Seite unter „Einstellungen“ → „Automatische Auswahl“ gewählt "
                   "werden.</p>").arg(
                   m_tournamentSettings->isPairMode() ? tr("Paarauswahl")
                                                      : tr("Spielerauswahl"),
                   QString::number(m_drawRound)),
                QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Cancel)
            != QMessageBox::Yes) {

            return false;
        }
    }

    return true;
}

bool RegistrationPage::checkWarnDeleteDraw(const QString &title)
{
    if (! m_tournamentSettings->autoSelectPairs()
        || (m_tournamentSettings->selectByLastRound() && m_drawRound != 1)) {

        // The changes to the draw will be ignored anyway. So let the the user do whatever.
        return true;
    }

    if (m_db->scoresPresent(m_drawRound) && ! m_db->roundsState().value(m_drawRound, false)
        && QMessageBox::warning(this, title,
                tr("<p><b>%1</b></p>"
                   "<p>Für Runde %2 wurden bereits Ergebnisse eingegeben. Wenn jetzt die Auslosung "
                   "verändert wird, dann kann es sein, dass Ergebnisse nicht mehr laut Auslosung "
                   "eingegeben werden können, weil die neue Auslosung mit bestehenden Eingaben "
                   "kollidiert.</p>"
                   "<p>Soll die Auslosung trotzdem bearbeitet werden?</p>").arg(
                   title, QString::number(m_drawRound)),
                QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Cancel)
           != QMessageBox::Yes) {

        return false;
    }

    return true;
}

void RegistrationPage::popupRequested(rc::Column column, QPoint pos)
{
    if (! m_db->isChangeable()) {
        return;
    }

    if (pos == QPoint(-1, -1)) {
        pos = QCursor::pos();
    }

    if (column == rc::Number) {
        if (! m_showColumn.value(rc::Number)->isChecked()) {
            askShowColumn(rc::Number);
        }
        m_numberPopup->show(m_registrationsModel->data(m_registrationsView->selectedRow()).number,
                            pos);

    } else if (column == rc::Mark) {
        m_markerPopup->show(m_registrationsModel->data(m_registrationsView->selectedRow()).marker,
                            pos);

    } else if (column == rc::Draw) {
        if (! m_showColumn.value(rc::Draw)->isChecked()) {
            askShowColumn(rc::Draw);
        }

        const auto &data = m_registrationsModel->data(m_registrationsView->selectedRow());

        if (data.marker == m_tournamentSettings->specialMarker(Markers::Booked)
            && ! data.draw.contains(m_drawRound)) {

            QMessageBox::warning(this,
                tr("Auslosung eingeben"),
                tr("<p><b>Auslosung eingeben</b></p>")
                + (m_tournamentSettings->isPairMode()
                       ? tr("<p>Das Paar „%1“ ist momentan als abwesend markiert. Für ein "
                            "abwesendes Paar kann keine Auslosung eingegeben werden."
                            "</p>").arg(m_registrationsView->selectedName().toHtmlEscaped())
                       : tr("<p>Der Spieler „%1“ ist momentan als abwesend markiert. Für einen "
                            "abwesenden Spieler kann keine Auslosung eingegeben "
                            "werden.").arg(m_registrationsView->selectedName().toHtmlEscaped())));
            return;
        }

        if (! checkDrawIsIgnored()) {
            return;
        }

        if (data.draw.contains(m_drawRound) && ! checkWarnDeleteDraw(tr("Auslosung löschen"))) {
            return;
        }

        m_drawPopup->show(data.id,
                          data.draw.value(m_drawRound, Draw::EmptySeat),
                          data.marker == m_markersWidget->singleMarker(),
                          pos);
    }
}

void RegistrationPage::askShowColumn(rc::Column column)
{
    DismissableMessage::SessionMessage messageId;
    if (column == rc::Draw) {
        messageId = DismissableMessage::AskShowDrawColumn;
    } else if (column == rc::Number) {
        messageId = DismissableMessage::AskShowNumberColumn;
    } else {
        // This should not happen
        qCWarning(RegistrationLog) << "RegistrationPage::askShowColumn: Unhandled columnd ID"
                                   << column;
        return;
    }

    if (m_dismissableMessage->dismissed(messageId) || m_showColumn.value(column)->isChecked()) {
        return;
    }

    if (m_dismissableMessage->question(messageId, this,
            tr("Auslosung speichern"),
            tr("Momentan ist die Spalte „%1“ auf der Anmeldeseite ausgeblendet. Soll sie angezeigt "
               "werden?").arg(rc::checkBoxText.value(column))) == QMessageBox::Yes) {

        m_showColumn[column]->setChecked(true);
    }
}

void RegistrationPage::setDraw(const Draw::Seat &seat)
{
    // Check for a suspiciously high table number
    if (seat.table >= DefaultValues::drawTableWarning
        && QMessageBox::question(this,
               tr("Auslosung speichern"),
               tr("Soll wirklich eine Auslosung für Tisch Nr. %1 eingegeben werden (vielleicht "
                  "war es ein Tippfehler)?").arg(seat.table),
               QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Cancel)
        == QMessageBox::Cancel) {

        return;
    }

    const auto name = m_registrationsView->selectedName();

    if (m_isClient || m_isServer) {
        // If we're on a network, we have to check if somebody else did
        // an assignment for the registration we're working on meanwhile

        if (! seat.isEmpty() // We want to delete an assignment otherwise
            && m_registrationsModel->data(m_registrationsView->selectedRow())
                   .draw.contains(m_drawRound)) {

            QMessageBox::warning(this,
                tr("Kollidierende Netzwerkänderung"),
                tr("<p>Für %1 „%2“ wurde mittlerweile von einem anderen Netzwerkteilnehmer eine "
                   "Auslosung für Runde %3 eingegeben.</p>"
                   "<p>Sofern das wirklich gewollt ist, dann bitte eine Änderung der Auslosung "
                   "anfragen (also die Auslosung löschen und neu eingeben).</p>").arg(
                   m_tournamentSettings->isPairMode() ? tr("das Paar") : tr("den Spieler"),
                   name.toHtmlEscaped(), QString::number(m_drawRound)));

            return;
        }

        // ... and we also have to check if the very seat we're about to assign is still available
        if (m_drawPopup->isSeatTaken(seat)) {
            QMessageBox::warning(this,
                tr("Kollidierende Netzwerkänderung"),
                tr("<p>Die Auslosung „Tisch %1 Paar %2“ wurde mittlerweile durch einen anderen "
                   "Netzwerkteilnehmer für Runde %3 vergeben und kann nicht mehr zugewiesen werden."
                   "</p>"
                   "<p>Bitte die Auslosung neu eingeben.</p>").arg(
                   QString::number(seat.table), QString::number(seat.pair),
                   QString::number(m_drawRound)));
            return;
        }
    }

    const int marker = seat.isEmpty() ? -1 : m_markersWidget->drawnMarker();

    if (m_isClient) {
        m_client->requestSetDraw(m_drawRound, name, seat, marker);
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    Q_EMIT statusUpdate(seat.isEmpty() ? tr("Lösche Auslosung …") : tr("Speichere Auslosung …"));

    if (! m_db->setDraw(m_drawRound, name, seat, marker)) {
        return;
    }

    const auto originalVisibleRow
        = m_registrationsView->visibleRow(m_registrationsModel->rowForName(name));
    updatePlayersList();
    Q_EMIT requestUpdateDrawOverviewPage(m_drawRound, seat.isEmpty() ? QString() : name);
    m_registrationsView->selectName(name, originalVisibleRow);

    Q_EMIT drawChanged();

    if (seat.isEmpty()) {
        Q_EMIT statusUpdate(tr("Auslosung für „%1“ für Runde %2 gelöscht").arg(
                               name, QString::number(m_drawRound)));
    } else {
        if (m_tournamentSettings->isPairMode()) {
            Q_EMIT statusUpdate(
                tr("Auslosung für „%1“ für Runde %2 gespeichert: Tisch %3 Paar %4").arg(
                   name, QString::number(m_drawRound), QString::number(seat.table),
                   QString::number(seat.pair)));
        } else {
            Q_EMIT statusUpdate(
                tr("Auslosung für „%1“ für Runde %2 gespeichert: Tisch %3 Paar %4 (%5)").arg(
                   name, QString::number(m_drawRound), QString::number(seat.table),
                   QString::number(seat.pair), QString::number(seat.player)));
        }
    }
    QApplication::restoreOverrideCursor();

    if (m_isServer) {
        QApplication::setOverrideCursor(Qt::BusyCursor);
        m_server->broadcastSetDraw(m_drawRound, name, seat, marker);
        QApplication::restoreOverrideCursor();
    }
}

void RegistrationPage::assignPlayer(const QString &name)
{
    const auto selectedName = m_registrationsView->selectedName();
    const auto pairName = selectedName + m_settings->namesSeparator() + name;

    if (m_registrationsModel->hasCaseInsensitiveMatch(pairName)) {
        QMessageBox::warning(this, tr("Allein gekommene Spieler zuordnen"),
            tr("<p><b>Zuordnung nicht möglich</b></p>"
               "<p>Der Spieler „%1“ kann dem Spieler „%2“ nicht zugeordnet werden, da der Paarname "
               "„%3“ bereits vergeben ist.</p>"
               "<p>Bitte mindestestens einen Namen ändern, damit ein eindeutiger Paarname erstellt "
               "werden kann!</p>").arg(
               name.toHtmlEscaped(), selectedName.toHtmlEscaped(), pairName.toHtmlEscaped()));
        return;
    }

    const auto player2Data = m_registrationsModel->data(name);
    if (player2Data.draw.contains(1) || player2Data.number > 0) {
        QString text;
        if (! player2Data.draw.isEmpty() && player2Data.number <= 0) {
            text = tr("<p><b>Vorsicht: Auslosung wird verworfen</b></p>"
                      "<p>Für den Spieler „%1“ wurde eine Auslosung gespeichert. Beim Zuweisen zu "
                      "Spieler „%2“ wird diese Auslosung verworfen, und die evtl. vorhandene "
                      "Auslosung von „%2“ für das Paar gesetzt.</p>"
                      "<p>Soll der Spieler zugewiesen und die Auslosung verworfen werden?</p>").arg(
                      name.toHtmlEscaped(), selectedName.toHtmlEscaped());
        } else if (player2Data.draw.isEmpty() && player2Data.number > 0) {
            text = tr("<p><b>Vorsicht: Nummer wird verworfen</b></p>"
                      "<p>Für den Spieler „%1“ wurde eine Nummer vergeben. Beim Zuweisen zu "
                      "Spieler „%2“ wird diese Nummer verworfen, und die evtl. vorhandene "
                      "Nummer von „%2“ für das Paar gesetzt.</p>"
                      "<p>Soll der Spieler zugewiesen und die Nummer verworfen werden?</p>").arg(
                      name.toHtmlEscaped(), selectedName.toHtmlEscaped());
        } else {
            text = tr("<p><b>Vorsicht: Auslosung und Nummer werden verworfen</b></p>"
                      "<p>Für den Spieler „%1“ wurde sowohl eine Auslosung gespeichert als auch "
                      "eine Nummer vergeben. Beim Zuweisen zu Spieler „%2“ werden diese Auslosung "
                      "und auch die Nummer verworfen, und die evtl. vorhandene Auslosung und/oder "
                      "Nummer von „%2“ für das Paar gesetzt.</p>"
                      "<p>Soll der Spieler zugewiesen und die Auslosung und Nummer verworfen "
                      "werden?</p>").arg(name.toHtmlEscaped(), selectedName.toHtmlEscaped());
        }
        if (QMessageBox::warning(this, tr("Allein gekommene Spieler zuordnen"), text,
                QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Cancel)
            != QMessageBox::Yes) {

            return;
        }
    }

    if (m_isClient || m_isServer) {
        // Somebody could have deleted or renamed the involved players

        if (   ! m_registrationsModel->hasExactMatch(selectedName)
            || ! m_registrationsModel->hasExactMatch(name)) {

            QMessageBox::warning(this,
                tr("Kollidierende Netzwerkänderung"),
                tr("<p>Mindestens einer der beteiligten Spieler wurde zwischenzeitlich von einem "
                   "anderen Netzwerkteilnehmer umbenannt oder gelöscht.</p>"
                   "<p>Das Zuordnen ist nicht mehr möglich und wird abgebrochen.</p>"));
            return;
        }
    }

    if (m_isClient) {
        m_client->requestAssemblePair(selectedName, name, m_settings->namesSeparator(),
                                      m_markersWidget->assignedMarker());
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    Q_EMIT statusUpdate(tr("Erstelle Paar aus Einzelspielern …"));

    if (! m_db->assemblePair(selectedName, name, m_settings->namesSeparator(),
                             m_markersWidget->assignedMarker())) {
        return;
    }

    updatePlayersList();

    if (! m_registrationsModel->hasExactMatch(pairName)) {
        itemError(Q_FUNC_INFO, pairName);
        return;
    }

    if (m_registrationsModel->data(pairName).draw.contains(1)) {
        Q_EMIT requestUpdateDrawOverviewPage(0, pairName);
    }

    m_registrationsView->selectName(pairName);

    Q_EMIT statusUpdate(tr("Paar „%1“ erstellt").arg(pairName));
    QApplication::restoreOverrideCursor();

    if (m_isServer) {
        QApplication::setOverrideCursor(Qt::BusyCursor);
        m_server->broadcastAssemblePair(selectedName, name, m_settings->namesSeparator(),
                                        m_markersWidget->assignedMarker());
        QApplication::restoreOverrideCursor();
    }
}

bool RegistrationPage::bookedPresent() const
{
    return m_bookedPresent;
}

bool RegistrationPage::singlesPresent() const
{
    return m_singlesPresent;
}

void RegistrationPage::deleteAllDraws()
{
    const QString title = tr("Alle Auslosungen für Runde %1 löschen").arg(m_drawRound);
    if (! confirmListDeleteAction(title, tr("Sollen wirklich alle bisher eingegebenen Auslosungen "
                                            "für Runde %1 gelöscht werden?").arg(m_drawRound))) {
        return;
    }

    if (m_isClient) {
        m_client->requestDeleteAllDraws(m_drawRound);
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    Q_EMIT statusUpdate(tr("Lösche alle Auslosungen für Runde %1 …").arg(m_drawRound));

    if (! m_db->deleteAllDraws(m_drawRound)) {
        return;
    }

    Q_EMIT drawChanged();
    updatePlayersList();
    Q_EMIT requestUpdateDrawOverviewPage(m_drawRound, QString());
    checkInformAboutBackup(title);

    Q_EMIT statusUpdate(tr("Alle Auslosungen für Runde %1 gelöscht").arg(m_drawRound));
    QApplication::restoreOverrideCursor();

    if (m_isServer) {
        QApplication::setOverrideCursor(Qt::BusyCursor);
        m_server->broadcastDeleteAllDraws(m_drawRound);
        QApplication::restoreOverrideCursor();
    }
}

void RegistrationPage::processStatusLinkClick(const QString &anchor)
{
    if (anchor == QLatin1String("#updateVisibility")) {
        if (m_registrationsView->selectedRow() == m_actuallyHiddenRow) {
            m_registrationsView->deselectRow();
        }
        updateVisibility();
        return;
    }

    if (anchor == QLatin1String("#drawOverview")) {
        Q_EMIT requestShowDrawOverviewPage(m_drawRound);
        return;
    }

    if (anchor == QLatin1String("#adjustDraw")) {
        Q_EMIT requestShowAdjustDrawDialog();
        return;
    }

    QString text;

    if (anchor == QLatin1String("#unfinished")) {
        text = tr("<p><b>Unvollständige Auslosung</b></p>");

        const auto missingDraws = m_registrationsModel->playersData().count()
                                  - m_registrationsModel->drawsCount();

        if (missingDraws == 1) {
            for (const auto &data : m_registrationsModel->playersData()) {
                if (! data.draw.contains(m_drawRound)) {
                    text.append(m_tournamentSettings->isPairMode()
                        ? tr("<p>Das Paar „%1“ hat noch keine Auslosung.</p>").arg(
                             data.name.toHtmlEscaped())
                        : tr("<p>Der Spieler „%1“ hat noch keine Auslosung.</p>").arg(
                             data.name.toHtmlEscaped()));
                    break;
                }
            }

        } else {
            QString list;
            int listCount = 0;
            for (const auto &data : m_registrationsModel->playersData()) {
                if (! data.draw.contains(m_drawRound)) {
                    list.append(QStringLiteral("<li>%1</li>").arg(data.name.toHtmlEscaped()));
                    if (++listCount >= 10) {
                        break;
                    }
                }
            }
            text.append(m_tournamentSettings->isPairMode()
                ? tr("<p>Die folgenden Paare haben noch keine Auslosung:</p>"
                     "<ul>%1</ul>").arg(list)
                : tr("<p>Die folgenden Spieler haben noch keine Auslosung:</p>"
                     "<ul>%1</ul>").arg(list));
            if (missingDraws > 10) {
                text.append(m_tournamentSettings->isPairMode()
                    ? tr("<p>und %n weitere(s)</p>", "für Paare", missingDraws - 10)
                    : tr("<p>und %n weitere(r)</p>", "für Spieler", missingDraws - 10));
            }
        }

    } else if (anchor == QLatin1String("#incompleteTables")) {
        text = tr("<p><b>Tische mit unvollständiger Auslosung</b></p>");
        const int incompleteCount = m_registrationsModel->incompleteTables().count();
        if (incompleteCount == 1) {
            text.append(tr("<p>Zu Tisch <b>%1</b> wurde bisher nur ein Paar 1 oder ein Paar 2 "
                           "zugelost%2.</p>").arg(
                           QString::number(m_registrationsModel->incompleteTables().at(0)),
                           m_tournamentSettings->isPairMode()
                               ? QString() : tr(" (bzw. die Paare sind unvollständig)")));
        } else if (incompleteCount == 2) {
            text.append(tr("<p>Zu den Tischen <b>%1</b> und <b>%2</b> wurden bisher nur ein Paar 1 "
                           "oder ein Paar 2 zugelost%3.</p>").arg(
                           QString::number(m_registrationsModel->incompleteTables().at(0)),
                           QString::number(m_registrationsModel->incompleteTables().at(1)),
                           m_tournamentSettings->isPairMode()
                               ? QString() : tr(" (bzw. die Paare sind unvollständig)")));
        } else {
            text.append(tr("<p>Zu folgenden Tischen wurden bisher nur ein Paar 1 oder ein Paar 2 "
                           "zugelost%2:</p><p>").arg(
                           m_tournamentSettings->isPairMode()
                               ? QString() : tr(" (bzw. die Paare sind unvollständig)")));
            for (int i = 0; i < incompleteCount - 1; i++) {
                text.append(tr("<b>%1</b>, ").arg(m_registrationsModel->incompleteTables().at(i)));
            }
            text = text.left(text.size() - 2);
            text.append(tr(" und <b>%1</b>.</p>").arg(
                           m_registrationsModel->incompleteTables().at(incompleteCount - 1)));
        }

    } else if (anchor == QLatin1String("#tableGaps")) {
        text = tr("<p><b>Tische, die in der Auslosung fehlen</b></p>");
        const int gapsCount = m_registrationsModel->tableGaps().count();
        if (gapsCount == 1) {
            text.append(tr("<p>Tisch <b>%1</b> ist bisher nicht ausgelost, müsste es aber sein (da "
                           "es sonst Lücken gibt)</p>").arg(
                           m_registrationsModel->tableGaps().at(0)));
        } else if (gapsCount == 2) {
            text.append(tr("<p>Die Tische <b>%1</b> und <b>%2</b> sind bisher nicht zugewiesen, "
                           "müssten es aber sein (da es sonst Lücken gibt).</p>").arg(
                           QString::number(m_registrationsModel->tableGaps().at(0)),
                           QString::number(m_registrationsModel->tableGaps().at(1))));
        } else {
            text.append(tr("<p>Folgende Tische sind bisher nicht ausgelost, müssten es aber "
                           "sein (da es sonst Lücken gibt):</p><p>"));
            for (int i = 0; i < gapsCount - 1; i++) {
                text.append(tr("<b>%1</b>, ").arg(m_registrationsModel->tableGaps().at(i)));
            }
            text = text.left(text.size() - 2);
            text.append(tr(" und <b>%1</b>.</p>").arg(
                           m_registrationsModel->tableGaps().at(gapsCount - 1)));
        }
    }

    QMessageBox::information(this, tr("Auslosung"), text);
}

void RegistrationPage::setDrawOverviewPage(DrawOverviewPage *drawOverviewPage)
{
    m_drawOverviewPage = drawOverviewPage;
    updatePlayersList();
}

void RegistrationPage::setColumnsVisible(const QVector<rc::Column> &columns)
{
    // First hide all columns except the name column and deselect the respective actions
    for (const auto column : rc::allColumns) {
        auto *action = m_showColumn[column];
        action->blockSignals(true);
        action->setChecked(false);
        action->blockSignals(false);
        m_registrationsView->hideColumn(rc::columnIndex.value(column));
    }

    // Also hide the drawRoundWidget
    // (won't happen automatically because above, we block the signals)
    m_drawRoundWidget->hide();

    // We have to call this via a QTimer call if the database is opened via command line argument
    // to be sure that everything is constructed and set up before we do this
    QTimer::singleShot(0, this, [this, columns]
    {
        // Re-show all requested columns
        for (const auto column : rc::allColumns) {
            m_showColumn[column]->setChecked(columns.contains(column));
        }
    });
}

void RegistrationPage::showDrawSettings()
{
    auto dialog = DrawSettingsDialog(this);
    dialog.updateSettings(m_tournamentSettings->drawSettings());
    if (dialog.exec()) {
        m_tournamentSettings->setDrawSettings(dialog.settings());
        updatePlayersList();
        if (m_registrationsView->isColumnHidden(rc::columnIndex.value(rc::Draw))) {
            askShowColumn(rc::Draw);
        }
    }
}

void RegistrationPage::drawAllSeats()
{
    const QString title = tr("Alle Plätze für Runde %1 auslosen").arg(m_drawRound);

    // First check if we can actually do a draw for all seats and tell the user why not if not

    if (! m_db->playersDividable() || m_singlesPresent || m_bookedPresent) {
        QString whyNot;
        if (! m_db->playersDividable()) {
            whyNot.append(tr("<li>Es ist nicht die passende Anzahl %1 angemeldet</li>").arg(
                m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler")));
        }
        if (m_singlesPresent) {
            whyNot.append(tr("<li>Es sind noch Spieler als „allein da“ markiert</li>"));
        }
        if (m_bookedPresent) {
            whyNot.append(tr("<li>Es noch %1 als „abwesend“ markiert</li>").arg(
                m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler")));
        }

        QMessageBox::warning(this, title,
            tr("<p><b>%1</b></p>"
               "<p>Das Auslosen aller Plätze ist derzeit nicht möglich:</p>"
               "<ul>%2</ul>").arg(title, whyNot));

        return;
    }

    // From here on, another network participant could alter something
    // whilst some message box is shown. We thus cache the checksum now.
    cacheChecksum();

    // Ask if we should show the draw column if it's currently hidden
    if (m_registrationsView->isColumnHidden(rc::columnIndex.value(rc::Draw))) {
        askShowColumn(rc::Draw);
    }

    // Warn about the draw being ignored anyway if appropriate
    if (! checkDrawIsIgnored() || ! checkCachedChecksum(title)) {
        return;
    }

    // Check if we would need more tables than the defined maximum (if any)

    const int tablesLimit = m_tournamentSettings->drawSettings().tablesLimit;
    const int neededTables = m_registrationsModel->rowCount() / m_db->necessaryDivisor();
    if (tablesLimit != -1 && neededTables > tablesLimit) {
        if (QMessageBox::warning(this, title,
                tr("<p><b>%1</b></p>"
                   "<p>Die Anzahl der Tische, die momentan für eine komplette Auslosung benötigt "
                   "werden (%2), ist höher, als das Tisch-Limit, das in den Einstellungen für die "
                   "Auslosung festgelegt wurde (%3)!</p>"
                   "<p>Sollen wirklich alle Plätze ausgelost werden?</p>").arg(
                   title, QString::number(neededTables), QString::number(tablesLimit)),
                QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Cancel)
                == QMessageBox::Cancel
            || ! checkCachedChecksum(title)) {

            return;
        }
    }

    // Warn about already entered draws being overwritten

    if (m_deleteAllDrawsAction->isEnabled()) {
        // We have existing entered draws
        if (QMessageBox::warning(this, title,
                tr("<p><b>%1</b></p>"
                   "<p>Es wurden bereits Auslosungen für Runde %2 eingegeben. Wenn jetzt alle "
                   "Plätze ausgelost werden, dann werden alle bestehenden Auslosungen "
                   "überschrieben und neu vergeben.</p>"
                   "<p>Sollen wirklich alle Plätze ausgelost werden?</p>").arg(
                   title, QString::number(m_drawRound)),
                QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Cancel)
                == QMessageBox::Cancel
            || ! checkCachedChecksum(title)) {

            return;
        }
    }

    // Warn about already entered scores and a possible mess up
    if (! checkWarnDeleteDraw(title) || ! checkCachedChecksum(title)) {
        return;
    }

    const auto draw = m_drawPopup->completeDraw(m_registrationsModel->allIds());
    setListDraw(m_drawRound, draw);
}

void RegistrationPage::setListDraw(int round, const QVector<Draw::Seat> &draw)
{
    // Here, we need a list of all names
    QVector<QString> playersNames;
    for (const auto &data : m_registrationsModel->playersData()) {
        playersNames.append(data.name);
    }

    if (m_isClient) {
        m_client->requestSetListDraw(round, playersNames, draw);
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    Q_EMIT statusUpdate(tr("Setze Auslosung aller Plätze für Runde %1 …").arg(round));

    if (! m_db->setListDraw(round, playersNames, draw)) {
        return;
    }

    Q_EMIT drawChanged();
    updatePlayersList();
    Q_EMIT requestUpdateDrawOverviewPage(round, QString());

    Q_EMIT statusUpdate(tr("Auslosung aller Plätze für Runde %1 gesetzt").arg(round));
    QApplication::restoreOverrideCursor();

    if (m_isServer) {
        QApplication::setOverrideCursor(Qt::BusyCursor);
        m_server->broadcastSetListDraw(round, playersNames, draw);
        QApplication::restoreOverrideCursor();
    }
}

bool RegistrationPage::registerPhoneticSearchEnabled() const
{
    return m_registerPhoneticSearch->isChecked();
}

void RegistrationPage::adoptDrawAdjustment(const QVector<QString> &names,
                                           const QVector<Draw::Seat> &seats)
{
    QVector<Draw::Seat> newDraw;
    for (int i = 0; i < m_registrationsModel->rowCount(); i++) {
        const auto &data = m_registrationsModel->data(i);
        if (names.contains(data.name)) {
            newDraw.append(seats.at(names.indexOf(data.name)));
        } else {
            // The draw for round 1 is always set because the draw adjustment
            // can only be called if each registration has a draw set
            newDraw.append(data.draw.value(1));
        }
    }

    // Draw adjustments are always for round 1
    setListDraw(1, newDraw);
}

bool RegistrationPage::drawAdjustmentNeededState() const
{
    return m_drawAdjustmentNeeded;
}

void RegistrationPage::adoptSearchText()
{
    const QString text = m_searchWidget->searchText().simplified();
    const auto words = text.split(QStringLiteral(" "));
    QString newText;
    for (const auto &word : words) {
        newText += QStringLiteral("%1%2 ").arg(m_stringTools->toUpper(word.at(0)),
                                               word.mid(1, word.length()));
    }

    m_playersName->setText(newText.simplified());
    m_searchWidget->clear();
    m_playersName->setFocus();
}

void RegistrationPage::showHeaderContextMenu(const QPoint &point)
{
    m_showHideHeaderHint = true;
    m_menuButton->clearFocus();
    m_columnsMenu->setTitleHidden(false);
    m_columnsMenu->exec(m_registrationsView->mapToGlobal(point));
    m_columnsMenu->setTitleHidden(true);
    m_showHideHeaderHint = false;
}

void RegistrationPage::setHideHeader(bool state)
{
    if (m_showHideHeaderHint) {
        QMessageBox::information(this, tr("Kopfzeile ausblenden"),
            tr("Die Kopfzeile der Anmeldungsliste kann über den „Einstellungen“-Knopf (unter "
               "„Angezeigte Spalten“) wieder eingeblendet werden!"));
    }

    m_registrationsView->horizontalHeader()->setVisible(! state);

    m_tournamentSettings->setRegistrationListHeaderHidden(state);
}

void RegistrationPage::itemError(const char *funcInfoText, const QString &text)
{
    qCCritical(RegistrationLog) << "Error:" << funcInfoText << "\n   Item not found:" << text;
    QTimer::singleShot(0, this, [this, funcInfoText, text]
    {
        QMessageBox::critical(this, tr("Unerwarteter Fehler"),
            tr("<p><b>Unerwarteter Fehler in<br/>"
               "%1</b></p>"
               "<p>Der Eintrag „%2“ konnte nicht gefunden werden. Bestenfalls funktioniert "
               "alles weiter, aber womöglich müssen die Netzwerkverbindung erneut gestartet "
               "und die Daten neu synchronisiert werden.</p>"
               "<p><b>Das sollte nicht passieren!</b></p>"
               "<p>Bitte einen Bugreport auf "
               "<a href=\"%3\">%3</a> schreiben!</p>").arg(
               QString::fromUtf8(funcInfoText).toHtmlEscaped(),
               text.toHtmlEscaped(),
               Urls::bugtracker));
    });
}

void RegistrationPage::setDisqualified()
{
    int round = -1;
    const auto selectedName = m_registrationsView->selectedName();

    if (m_registrationsModel->data(m_registrationsView->selectedRow()).disqualified == 0) {
        auto dialog = DisqualificationDialog(this, selectedName,
                                                m_tournamentSettings->isPairMode(),
                                                m_db->allRounds());
        if (! dialog.exec()) {
            return;
        }
        round = dialog.round();

    } else {
        if (QMessageBox::question(this,
                tr("Disqualifikation zurücknehmen"),
                tr("Soll die Disqualifikation für %1 „%2“ wirklich zurückgenommen werden?").arg(
                m_tournamentSettings->isPairMode() ? tr("das Paar") : tr("den Spieler"),
                selectedName),
                QMessageBox::Yes | QMessageBox::Cancel,
                QMessageBox::Cancel) == QMessageBox::Cancel) {

            return;
        }
        round = 0;
    }

    Q_ASSERT(round != -1);

    // Someone could have deleted or renamed or edited the entry
    if ((m_isClient || m_isServer ) && ! m_registrationsModel->hasExactMatch(selectedName)) {
        QMessageBox::warning(this,
            tr("Kollidierende Netzwerkänderung"),
            tr("%1 „%2“ wurde mittlerweile von einem anderen Netzwerkteilnehmer entweder "
                "umbenannt oder gelöscht. Die Disqualifikation ist nicht mehr möglich und wird "
                "abgebrochen.").arg(
                m_tournamentSettings->isPairMode() ? tr("Das Paar") : tr("Der Spieler"),
                selectedName));
        return;
    }

    if (m_isClient) {
        m_client->requestSetDisqualified(selectedName, round);
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    if (round != 0) {
        Q_EMIT statusUpdate((m_tournamentSettings->isPairMode()
                             ? tr("Disqualifiziere Paar „%1“ ab Runde %2 …")
                             : tr("Disqualifiziere Spieler „%1“ ab Runde %2 …")).arg(
                                  selectedName, QString::number(round)));
    } else {
        Q_EMIT statusUpdate((m_tournamentSettings->isPairMode()
                             ? tr("Nehme die Disqualifikation von Paar „%1“ zurück …")
                             : tr("Nehme die Disqualifikation von Spieler „%1“ zurück …")).arg(
                                  selectedName));
    }

    if (! m_db->setDisqualified(selectedName, round)) {
        return;
    }

    updatePlayersList();
    Q_EMIT requestUpdateDrawOverviewPage(0, selectedName);

    Q_EMIT disqualificationChanged();

    if (round != 0) {
        Q_EMIT statusUpdate((m_tournamentSettings->isPairMode()
                             ? tr("Paar „%1“ ab Runde %2 disqualifiziert")
                             : tr("Spieler „%1“ ab Runde %2 disqualifiziert")).arg(
                                  selectedName, QString::number(round)));
    } else {
        Q_EMIT statusUpdate((m_tournamentSettings->isPairMode()
                             ? tr("Disqualifikation von Paar „%1“ zurückgenommen")
                             : tr("Disqualifikation von Spieler „%1“ zurückgenommen")).arg(
                                  selectedName));
    }
    QApplication::restoreOverrideCursor();

    if (m_isServer) {
        QApplication::setOverrideCursor(Qt::BusyCursor);
        m_server->broadcastSetDisqualified(selectedName, round);
        QApplication::restoreOverrideCursor();
    }
}

void RegistrationPage::requestSetColumnVisible(rc::Column column, bool state)
{
    m_showColumn.value(column)->setChecked(state);
}

const QVector<Players::Data> &RegistrationPage::playersData() const
{
    return m_registrationsModel->playersData();
}

void RegistrationPage::searchAsYouTypeToggled(bool state)
{
    m_settings->saveSearchAsYouTypeState(state);
    m_playersName->setClearButtonEnabled(state);
    m_searchWidget->setVisible(! state);
    if (! state) {
        m_searchWidget->clear();
    } else {
        m_searchWidget->setSearchText(m_playersName->text());
    }
}

void RegistrationPage::playersNameChanged(const QString &text)
{
    m_registerButton->setEnabled(! text.simplified().isEmpty());

    if (m_tournamentSettings->bookingEnabled() && m_bookingEngine->looksLikeBookingCode(text)) {
        if (m_searchAsYouType->isChecked() && ! m_searchWidget->searchText().isEmpty()) {
            m_searchWidget->clear();
        }
        return;
    }

    if (m_searchAsYouType->isChecked()) {
        m_searchWidget->setSearchText(text);
    }
}

void RegistrationPage::registerPhoneticSearchToggled()
{
    if (m_searchAsYouType->isChecked()) {
        updateSearch(m_playersName->text());
    }

    if (m_registerPhoneticSearch->isChecked()) {
        m_registerPhoneticSearch->setStyleSheet(QStringLiteral("QToolButton { padding: 2px; }"));
        m_registerPhoneticSearch->setToolTip(
            tr("<p>Phonetische Suche für die Suche nach Duplikaten bei der Anmeldung "
            "(de)aktivieren</p>")
            + PhoneticSearchButton::description());
    } else {
        m_registerPhoneticSearch->setStyleSheet(QStringLiteral(
            "QToolButton { padding: 2px; background: rgba(255, 0, 0, 50) };"));
        m_registerPhoneticSearch->setToolTip(tr(
            "<p>Phonetische Suche für die Suche nach Duplikaten bei der Anmeldung (de)aktivieren"
            "</p>"
            "<p><b>Vorsicht: Die phonetische Suche ist deaktiviert!</b></p>"
            "<p>Identische Anmeldungen mit kleinen Tippfehlern bzw. anderer Schreibweise des "
            "Namens werden womöglich nicht als Duplikat gemeldet!</p>"
            "<p>Z. B. würde die phonetische Suche „Schmidt Yannick / Meier Stefan“ als "
            "potentielles Duplikat von „Stephan Meyer / Jannik Schmitt“ finden. Ohne phonetische "
            "Suche wird die Eingabe ohne Warnung als neues Paar akzeptiert.</p>"));
    }
}

void RegistrationPage::setNumber(int number)
{
    const auto name = m_registrationsView->selectedName();

    if (number == 0 && QMessageBox::question(this,
            m_tournamentSettings->isPairMode() ? tr("Paarnummer löschen")
                                               : tr("Spielernummer löschen"),
            tr("Soll %1 für „%2“ wirklich gelöscht werden?").arg(
               m_tournamentSettings->isPairMode() ? tr("die Paarnummer")
                                                  : tr("die Spielernummer"),
               name.toHtmlEscaped()),
            QMessageBox::Yes | QMessageBox::Cancel,
            QMessageBox::Cancel)

        != QMessageBox::Yes) {

        return;
    }

    if (m_isClient || m_isServer) {
        // Check if the requested number is still available.
        // Another network participant could have assigned it meanwhile.
        if (number > 0 && m_registrationsModel->numbers().contains(number)) {
            QTimer::singleShot(0, [this, number]
            {
                QMessageBox::warning(this, tr("Kollidierende Netzwerkänderung"),
                    tr("<p>Die %1 %2 wurde zwischenzeitlich von einem anderen Netzwerkteilnehmer "
                       "vergeben.</p>"
                       "<p>Die Änderung wird abgebrochen.</p>").arg(
                    m_tournamentSettings->isPairMode() ? tr("Paarnummer") : tr("Spielernummer"),
                    QString::number(number)));
            });
            return;
        }
    }

    if (m_isClient) {
        m_client->requestSetNumber(name, number);
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    if (number != 0) {
        Q_EMIT statusUpdate((m_tournamentSettings->isPairMode()
                                 ? tr("Vergebe Paarnummer %1 für „%2“  …")
                                 : tr("Vergebe Spielernummer %1 für „%2“ …")).arg(
                             QString::number(number), name));
    } else {
        Q_EMIT statusUpdate((m_tournamentSettings->isPairMode()
                                 ? tr("Lösche Paarnummer für „%1“ …")
                                 : tr("Lösche Spielernummer für „%1“ …")).arg(name));
    }

    if (! m_db->setNumber(name, number)) {
        return;
    }

    const auto originalVisibleRow = m_registrationsView->visibleRow(
                                        m_registrationsModel->rowForName(name));
    updatePlayersList();
    Q_EMIT requestUpdateDrawOverviewPage(0, name);
    Q_EMIT playersNumberChanged();

    m_registrationsView->selectName(name, originalVisibleRow);

    if (number != 0) {
        Q_EMIT statusUpdate((m_tournamentSettings->isPairMode()
                                 ? tr("Paarnummer %1 für „%2“ vergeben")
                                 : tr("Spielernummer %1 für „%2“ vergeben")).arg(
                             QString::number(number), name));
    } else {
        Q_EMIT statusUpdate((m_tournamentSettings->isPairMode()
                                 ? tr("Paarnummer für „%1“ gelöscht")
                                 : tr("Spielernummer für „%1“ gelöscht")).arg(name));
    }
    QApplication::restoreOverrideCursor();

    if (m_isServer) {
        QApplication::setOverrideCursor(Qt::BusyCursor);
        m_server->broadcastSetNumber(name, number);
        QApplication::restoreOverrideCursor();
    }
}

void RegistrationPage::bookingCodeScanned(const QString &scannedText)
{
    // In case this is triggered by the WLAN code scanner server, we
    // have to check if we want to parse data at all at the moment:
    if (! m_playersName->readyToScan()) {
        qCDebug(RegistrationLog) << "RegistrationPage::bookingCodeScanned: Not ready to scan."
                                 << "Refusing to process booking code.";
        return;
    }

    // Parse the read data ...
    const auto data = m_bookingEngine->parse(scannedText);

    // ... and check it for problems

    if (! data.isValid && ! data.error.isEmpty()) {
        // Parsing failed, we can't process the code
        QMessageBox::warning(this, tr("Anmeldungscode scannen"),
            tr("<p>Der Code konnte nicht verarbeitet werden! Der Fehler war:</p>"
               "<p>%1</p>").arg(data.error));
        return;
    }

    if (! data.isValid) {
        // Parsing succeeded, but the checksum check failed. This is no valid registration code.

        QString text = tr("<p><b>Die Prüsumme ist ungültig!</b></p>"
                          "<p style=\"%1\"><b>Das ist kein gültiger Anmeldungscode!</b></p>").arg(
                          SharedStyles::redText);
        if (data.tournament != m_bookingEngine->tournamentName()) {
            text += tr("<p>Der Anmeldungscode wurde für ein anderes Turnier erstellt:<br/>"
                       "Ist „%1“, sollte aber „%2“ sein!</p>").arg(
                       data.tournament.toHtmlEscaped(),
                       m_bookingEngine->tournamentName().toHtmlEscaped());
        }
        text += tr("<p>Soll die Anmeldung für „%1“ trotzdem weiterverarbeitet werden?</p>").arg(
                   data.name.toHtmlEscaped());

        if (QMessageBox::warning(this, tr("Anmeldungscode scannen"), text,
                                 QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Cancel)
            != QMessageBox::Yes) {

            return;
        }
    }

    // If we reach here, the code contains valid and checked data,
    // or the user explicitely wants to process invalid data.

    QString targetName;
    if (m_registrationsModel->hasExactMatch(data.name)) {
        targetName = data.name;
    } else {
        // Using the checksum as a secondary identification feature besides the name itself should
        // be totally safe: It's only used if the name has been edited and thus can't be found to
        // identify the original registration. And independently of the dataset being valid or not,
        // being used despite of checksum failures, wrong tournament name or whatever, it's
        // virtually impossible that we get an MD5 sum collision. Even if so, the user can still
        // refuse to process this very code.
        targetName = m_registrationsModel->nameForBookingChecksum(data.checksum);
    }

    // Check if the booking has been revoked
    const auto [ revocationState, success ] = m_db->revocationState(data.checksum);
    if (! success) {
        return;
    }

    if (revocationState.isRevoked()) {
        targetName.clear(); // We will handle the revocation below
    }

    if (! targetName.isEmpty()) {
        // We do have this exact registration

        if (m_registrationsModel->data(targetName).marker
            == m_tournamentSettings->specialMarker(Markers::Booked)) {

            // It's also marked as "booked"

            const auto &marker = m_markersWidget->marker(
                                     m_markersWidget->newRegistrationMarker(targetName));

            if (targetName == data.name) {
                if (QMessageBox::question(this, tr("Anmeldungscode scannen"),
                        tr("<p><b>Voranmeldung gefunden!</b></p>"
                           "<p>Der Code enthält die Voranmeldung von<br/>"
                           "<b>%1</b></p>"
                           "<p>Soll %2 jetzt als „%3“ markiert werden?</p>").arg(
                        targetName.toHtmlEscaped(),
                        m_tournamentSettings->isPairMode() ? tr("das Paar") : tr("der Spieler"),
                        marker.name.toHtmlEscaped()),
                        QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Yes)
                    == QMessageBox::Cancel) {

                    return;
                }

            } else {
                if (QMessageBox::question(this, tr("Anmeldungscode scannen"),
                        tr("<p><b>Voranmeldung gefunden</b></p>"
                           "<p>Der Anmeldungscode passt auf eine Voranmeldung, aber der Name "
                           "wurde geändert:</p>"
                           "<p><table>"
                           "<tr><td><i>Name im Code:</i></td><td>%1</td></tr>"
                           "<tr><td><i>Passende Voranmeldung:</i></td><td><b>%2</b></td></tr>"
                           "</table></p>"
                           "<p>Soll %3 jetzt als „%4“ markiert werden?</p>").arg(
                           data.name.toHtmlEscaped(), targetName.toHtmlEscaped(),
                           m_tournamentSettings->isPairMode() ? tr("das Paar") : tr("der Spieler"),
                           marker.name.toHtmlEscaped()),
                        QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Yes)
                    == QMessageBox::Cancel) {

                    return;
                }
            }

            // If we're on a network: Check for a collision
            if ((m_isClient || m_isServer)
                && (! m_registrationsModel->hasExactMatch(targetName)
                    || m_registrationsModel->data(targetName).marker
                       != m_tournamentSettings->specialMarker(Markers::Booked))) {

                QMessageBox::warning(this, tr("Kollidierende Netzwerkänderung"),
                    tr("<p>Ein anderer Netzwerkteilnehmer hat die Voranmeldung „%1“ entweder "
                       "umbenannt, gelöscht, oder die „Vorangemeldet“-Markierung geändert.</p>"
                       "<p>Das Markieren ist nicht mehr möglich und wird abgebrochen.</p>").arg(
                       targetName.toHtmlEscaped()));
                return;
            }

            // Select the name
            m_registrationsView->selectName(targetName);
            // Update the marker
            setMarker(marker.id);

        } else {
            // The registration isn't "booked" (anymore)
            QMessageBox::warning(this, tr("Anmeldungscode scannen"),
                tr("<p>%1 „%2“ ist bereits regulär angemeldet.</p>"
                   "<p>Evtl. wurde der Code bereits verarbeitet, oder die Anmeldung schon manuell "
                   "durchgeführt?</p>").arg(
                   m_tournamentSettings->isPairMode() ? tr("Das Paar") : tr("Der Spieler"),
                   targetName.toHtmlEscaped()));
            return;
        }

    } else {
        // The registration has been revoked or we don't have it

        if (revocationState.isRevoked()) {
            // This code has been marked as "revoked"

            QString reason;
            switch (revocationState.reason) {
            case Booking::UnknownRevocationReason:
                // This should not happen
                reason = tr("(Der Grund des Widerrufs konnte nicht ausgelesen werden)");
                break;
            case Booking::RegistrationDeleted:
                reason = tr("Die zugehörige Anmeldung wurde <b>gelöscht</b>");
                break;
            case Booking::NewCodeExported:
                reason = tr("Für die Anmeldung wurde nach der Änderung des angemeldeten Namens "
                            "oder des Turniernamens ein <b>neuer Anmeldungscode exportiert</b>");
            }

            if (QMessageBox::warning(this, tr("Anmeldungscode scannen"),
                    tr("<p><b>Anmeldungscode wurde widerrufen</b></p>"
                       "<p><table>"
                       "<tr><td><i>Grund:</i></td><td>%1</td></tr>"
                       "<tr><td><i>Zeitpunkt:</i></td><td>%2</td></tr>"
                       "</table></p>"
                       "<p style=\"%3\"><b>Das ist kein gültiger Anmeldungscode!</b></p>"
                       "<p>Soll die Anmeldung für „%4“ trotzdem weiterverarbeitet werden?</p>").arg(
                       reason,
                       revocationState.date.toString(tr("dd.MM.yyyy, HH:mm")),
                       SharedStyles::redText,
                       data.name.toHtmlEscaped()),
                    QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Cancel)
                == QMessageBox::Cancel) {

                return;
            }

        } else {
            // This should not happen unless the database has been modified by hand.
            // If we overwrite a booking or delete one, the respective checksum is added to the
            // "revoked" list. So we should catch this case above.
            if (QMessageBox::question(this, tr("Anmeldungscode scannen"),
                tr("<p><b>Keine passende Voranmeldung</b></p>"
                   "<p>Der Anmeldungscode enthält Daten für „%1“,<br/>"
                   "<b>aber es gibt keine entsprechende Voranmeldung!</b></p>"
                   "<p>Soll „%1“ regulär als %2 angemeldet werden?</p>").arg(
                   data.name.toHtmlEscaped(),
                   m_tournamentSettings->isPairMode() ? tr("Paar") : tr("Spieler")),
                QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Yes)
                == QMessageBox::Cancel) {

                return;
            }
        }

        if ((m_isClient || m_isServer)
            && m_registrationsModel->hasCaseInsensitiveMatch(data.name)) {

            QMessageBox::warning(this, tr("Kollidierende Netzwerkänderung"),
                tr("<p>Ein anderer Netzwerkteilnehmer hat zwischenzeitlich %1 „%2“ angemeldet.</p>"
                   "<p>Die Anmeldung ist nicht mehr möglich und wird abgebrochen.</p>").arg(
                   m_tournamentSettings->isPairMode() ? tr("das Paar") : tr("den Spieler"),
                   data.name.toHtmlEscaped()));
            return;
        }

        // Register the booking
        m_playersName->setText(data.name);
        registrationEntered();
    }
}

void RegistrationPage::drawRoundChanged(int round)
{
    if (round > 1 && ! m_db->tournamentStarted()) {
        m_drawRoundSpinBox->setValue(1);
        clearDrawRoundSpinBoxFocus();
        QMessageBox::warning(this, tr("Runde für Auslosung ändern"),
            tr("<p><b>Runde für Auslosung ändern</p>"
               "<p>Die Anmeldung wurde noch nicht beendet!</p>"
               "<p>Ab Runde 2 ist die Auslosung auf die besetzten Tische beschränkt. Bevor klar "
               "ist, wie viele Spieler teilnehmen, kann keine Auslosung für Runden nach der 1. "
               "Runde eingegeben werden.</p>"));
        return;
    }

    if (round > 1
        && (m_tournamentSettings->autoSelectPairs() && m_tournamentSettings->selectByLastRound())
        && QMessageBox::warning(this, tr("Runde für Auslosung ändern"),
               tr("<p><b>Runde für Auslosung ändern</p>"
                  "<p>Für die automatische Tisch- bzw. Paar-/Spielerauswahl ist die Option „Paar 2 "
                  "rückt pro Runde weiter“ aktiviert. Ab Runde 2 werden die Plätze von den "
                  "Ergebnissen der vorhergehenden Runde bestimmt.</p>"
                  "<p><b>Auslosungen für eine andere Runde als Runde 1 werden nicht "
                  "berücksichtigt!</b></p>"
                  "<p>Soll trotzdem die Auslosung für Runde %1 bearbeitet werden?</p>").arg(round),
               QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Cancel)
           == QMessageBox::Cancel) {

        m_drawRoundSpinBox->setValue(1);
        clearDrawRoundSpinBoxFocus();
        return;
    }

    m_drawRound = round;
    m_registrationsModel->setDrawRound(round);
    m_drawPopup->setDrawRound(round, m_db->tablesCount());
    updatePlayersList();
    clearDrawRoundSpinBoxFocus();
}

void RegistrationPage::clearDrawRoundSpinBoxFocus()
{
    QTimer::singleShot(0, this, [this]
    {
        auto *lineEdit = m_drawRoundSpinBox->findChild<QLineEdit *>();
        lineEdit->deselect();
        lineEdit->clearFocus();
        m_drawRoundSpinBox->clearFocus();
    });
}

void RegistrationPage::prepareDrawNextRound(const QVector<QString> &names, int round, bool raise)
{
    m_registrationsView->deselectRow();
    m_searchWidget->setSearchText(m_registrationsModel->exactOrMatchString(names));
    if (m_drawRound != round + 1) {
        m_drawRoundSpinBox->setValue(round + 1);
    }

    if (raise) {
        Q_EMIT raiseMe();
    }
}

void RegistrationPage::bookingEnabledChanged(bool state)
{
    m_readyToScanLabel->setVisible(state);
    m_playersName->setShowBlurAction(state);
    m_readyToScanLabel->updateText();
}
