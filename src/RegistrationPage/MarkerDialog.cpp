// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "MarkerDialog.h"
#include "MarkersModel.h"

#include "shared/SharedStyles.h"

// Qt includes
#include <QGridLayout>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>
#include <QComboBox>
#include <QDebug>
#include <QColorDialog>
#include <QTableWidget>
#include <QMessageBox>
#include <QTimer>

// C++ includes
#include <math.h> // pow

MarkerDialog::MarkerDialog(QWidget *parent, MarkersModel *model, Markers::Marker editedMarker)
    : TitleDialog(parent),
      m_markersModel(model),
      // We cache a deep copy of a possibly edited marker here. Who knows what happens to some
      // pointer if a network change is being applied.
      // Additionally, the default value is Markers::unmarkedMarker. We can't edit the "unmarked"
      // marker, so this indicates that we're not editing but we want to add a new marker.
      m_editedMarker(editedMarker)
{
    QPalette palette;
    m_background = palette.color(QPalette::Base);

    addButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    QGridLayout *settingsLayout = new QGridLayout;
    layout()->addLayout(settingsLayout);
    mainLayout()->setSizeConstraint(QLayout::SetFixedSize);

    settingsLayout->addWidget(new QLabel(tr("Name:")), 0, 0);
    m_name = new QLineEdit;
    settingsLayout->addWidget(m_name, 0, 1);

    m_badContrast = new QLabel(tr("<span style=\"%1\">Vorsicht: Schlechter Kontrast!</span>").arg(
                                  SharedStyles::redText));
    settingsLayout->addWidget(m_badContrast, 1, 1);
    m_badContrast->hide();

    setColor(QColor(Qt::red));

    settingsLayout->addWidget(new QLabel(tr("Farbe:")), 2, 0);
    QPushButton *chooseColorButton = new QPushButton(tr("…"));
    connect(chooseColorButton, &QPushButton::clicked, this, &MarkerDialog::chooseColor);
    settingsLayout->addWidget(chooseColorButton, 2, 1);

    settingsLayout->addWidget(new QLabel(tr("Sortierung:")), 3, 0);
    m_sorting = new QComboBox;
    m_sorting->addItem(tr("Neuer Block"), QVariant(Markers::MarkerSorting::NewBlock));
    m_sorting->addItem(tr("Fortlaufend"), QVariant(Markers::MarkerSorting::Merged));
    settingsLayout->addWidget(m_sorting, 3, 1);

    // Process the passed marker

    if (m_editedMarker == Markers::unmarkedMarker) {
        setTitle(tr("Neue Markierung"));

        const auto name = tr("Neue Markierung");
        int counter = 1;
        auto newName = name;
        while (m_markersModel->isMarkerNamePresent(newName)) {
            counter++;
            newName = QStringLiteral("%1 %2").arg(name, QString::number(counter));
        }
        m_name->setText(newName);

        setButtonText(QDialogButtonBox::Ok, tr("Hinzufügen"));

    } else {
        setTitle(tr("Markierung bearbeiten"));
        m_name->setText(m_editedMarker.name);
        setColor(m_editedMarker.color);
        m_sorting->setCurrentIndex(m_sorting->findData(m_editedMarker.sorting));
        setButtonText(QDialogButtonBox::Ok, tr("Speichern"));
    }

    // Finish the setup
    connect(m_name, &QLineEdit::textChanged,
            this, [this]
            {
                setOkButtonEnabled(! m_name->text().isEmpty());
            });
    m_name->setFocus();
    QTimer::singleShot(0, m_name, &QLineEdit::deselect);
}

void MarkerDialog::chooseColor()
{
    // We have to mess with pointer here because if this dialog is closed due to a colliding
    // network change, the color dialog would persist, and if it's deleted and the color dialog
    // is an object and not a pointer, the program crashes. With the color dialog being a pointer,
    // everything is fine on deletion ;-)

    QColorDialog *dialog = new QColorDialog(m_name->palette().color(QPalette::Text), this);
    connect(dialog, &QColorDialog::colorSelected, this, &MarkerDialog::setColor);

    dialog->setWindowTitle(tr("Markierungsfarbe wählen"));
    dialog->exec();
}

void MarkerDialog::setColor(const QColor &color)
{
    QPalette palette;
    palette.setColor(QPalette::Text, color);
    m_name->setPalette(palette);

    if (contrastRatio(m_background, color) >= 2.3) {
        m_badContrast->hide();
    } else {
        m_badContrast->show();
    }
}

void MarkerDialog::disableSortingSelection()
{
    m_sorting->setEnabled(false);
}

qreal MarkerDialog::contrastRatio(const QColor &color1, const QColor &color2) const
{
    // This computes the contrast ratio of two colors according to WCAG 2.0
    // See https://www.w3.org/TR/2008/REC-WCAG20-20081211/#contrast-ratiodef

    qreal l1;
    qreal l2;
    if (color1.lightness() >= color2.lightness()) {
        l1 = relativeLuminance(color1);
        l2 = relativeLuminance(color2);
    } else {
        l1 = relativeLuminance(color2);
        l2 = relativeLuminance(color1);
    }

    return (l1 + 0.05) / (l2 + 0.05);
}

qreal MarkerDialog::relativeLuminance(const QColor &color) const
{
    return   0.2126 * prepareColorComponent(color.redF())
           + 0.7152 * prepareColorComponent(color.greenF())
           + 0.0722 * prepareColorComponent(color.blueF());
}

qreal MarkerDialog::prepareColorComponent(qreal component) const
{
    if (component <= 0.03928) {
        return component / 12.92;
    } else {
        return pow(((component + 0.055) / 1.055), 2.4);
    }
}

void MarkerDialog::accept()
{
    const auto name = m_name->text().simplified();

    if (m_markersModel->isUnmarkedName(name)) {
        // No matter what, if the name is the "unmarked" marker's name, we display a warning

        auto text = tr("<p><b>Uneindeutiger Name für die Markierung</b></p>"
                       "<p>Die Bezeichnung „%1“ ist für unmarkierte Einträge reserviert!</p>").arg(
                       m_markersModel->unmarkedName().toHtmlEscaped());

        if (isEditing()) {
            if (name == m_editedMarker.name) {
                text.append(tr("<p>Soll der Name der Markierung wirklich bei „%1“ belassen werden?"
                               "</p>").arg(m_markersModel->unmarkedName().toHtmlEscaped()));
            } else {
                text.append(tr("<p>Soll der Name der Markierung wirklich auf „%1“ geändert werden?"
                               "</p>").arg(m_markersModel->unmarkedName().toHtmlEscaped()));
            }
        } else {
            text.append(tr("<p>Soll wirklich eine Markierung mit dem Namen „%1“ erstellt werden?"
                            "</p>").arg(m_markersModel->unmarkedName().toHtmlEscaped()));
        }

        if (QMessageBox::warning(this, windowTitle(),
                text,
                QMessageBox::Yes | QMessageBox::Cancel,
                QMessageBox::Cancel) == QMessageBox::Cancel) {

            return;
        }

    } else if (m_markersModel->isMarkerNamePresent(name, m_editedMarker.id)) {
        // If we have another name, we check if there's some collision. If we edit a marker, it's
        // ID will be skipped whilst searching. If we add a new marker, m_editedMarker.id will be
        // 0, the ID of the "unmarked" marker. And a collision with this one already has been
        // checked above.

        auto text = tr("<p><b>Uneindeutiger Name für die Markierung</b></p>"
                       "<p>Der Name „%1“ wird bereits für eine andere Markierung benutzt!</p>").arg(
                       name.toHtmlEscaped());

        if (isEditing()) {
            text.append(tr("<p>Soll der Name der Markierung „%1“ wirklich auf „%2“ geändert werden?"
                           "</p>").arg(m_editedMarker.name.toHtmlEscaped(), name.toHtmlEscaped()));
        } else {
            text.append(tr("<p>Soll wirklich eine Markierung mit dem Namen „%1“ erstellt werden?"
                           "</p>").arg(name.toHtmlEscaped()));
        }

        if (QMessageBox::warning(this, windowTitle(),
                text,
                QMessageBox::Yes | QMessageBox::Cancel,
                QMessageBox::Cancel) == QMessageBox::Cancel) {

            return;
        }
    }

    // Everything fine or let through on the nod, so emit our new or edited marker
    Markers::Marker marker;
    marker.name = name;
    marker.color = m_name->palette().color(QPalette::Text);
    marker.sorting = static_cast<Markers::MarkerSorting>(m_sorting->currentData().toInt());
    Q_EMIT markerSaved(marker);

    QDialog::accept();
}

bool MarkerDialog::isEditing() const
{
    return m_editedMarker != Markers::unmarkedMarker;
}
