// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef DRAWPOPUP_H
#define DRAWPOPUP_H

// Local includes
#include "shared/SelectionPopup.h"
#include "shared/Draw.h"

// Local classes
class TournamentSettings;

// Qt classes
class QSpinBox;
class QRadioButton;
class QLabel;
class QPushButton;

class DrawPopup : public SelectionPopup
{
    Q_OBJECT

public:
    explicit DrawPopup(QWidget *parent,
                       TournamentSettings *tournamentSettings,
                       const QVector<int> &assignedPair1Tables,
                       const QVector<int> &assignedPair2Tables,
                       const QHash<int, QVector<int>> &currentDraw,
                       const QHash<int, QVector<int>> &opponents,
                       const QHash<int, QVector<int>> &teamMates);
    void show(int id, Draw::Seat seat, bool isSinglePlayer, const QPoint &position);
    QVector<Draw::Seat> completeDraw(const QVector<int> &ids);
    bool isSeatTaken(const Draw::Seat &seat) const;
    void setDrawRound(int round, int tables);

Q_SIGNALS:
    void requestSetDraw(const Draw::Seat &seat);

public Q_SLOTS:
    void setUpdateNeeded();

private Q_SLOTS:
    void selectFirstFreeSeat();
    void drawSeat();
    void emitRequestSetDraw();

private: // Functions
    int assignmentsPerPair() const;
    void updateDrawableSeats();
    void updateCollisionFreeSeats();
    int findFirstIncompleteTable(const QVector<int> *list) const;
    void selectSeat(const Draw::Seat &seat);
    Draw::Seat getRandomSeat() const;
    int getPlayer(const Draw::Seat &seat) const;
    void checkCollision(int table, int pair,
                        bool *teamMateCollision, bool *opponentCollision) const;
    bool isCollisionFree(int table, int pair) const;
    void checkSelection();
    void updateTeamMatesAndOpponents();

private: // Variables
    TournamentSettings *m_tournamentSettings;
    const Draw::Settings *m_drawSettings;
    const QVector<int> *m_assignedPair1Tables;
    const QVector<int> *m_assignedPair2Tables;
    const QHash<int, QVector<int>> *m_currentDraw;
    const QHash<int, QVector<int>> *m_allOpponents;
    const QHash<int, QVector<int>> *m_allTeamMates;

    Draw::Seat m_firstFreeSeat;
    QVector<Draw::Seat> m_drawableSeats;
    QVector<Draw::Seat> m_collisionFreeSeats;
    bool m_drawingPossible = false;
    bool m_updateNeeded = false;
    int m_drawRound = 1;
    int m_allTables = 0;
    int m_id = 0;
    QVector<int> m_opponents;
    QVector<int> m_teamMates;

    QWidget *m_main;
    QWidget *m_selection;
    QLabel *m_isSinglePlayer;
    QSpinBox *m_table;
    QRadioButton *m_pair1;
    QRadioButton *m_pair2;
    QLabel *m_taken;
    QLabel *m_beyondLimit;
    QLabel *m_teamMateCollision;
    QLabel *m_opponentCollision;
    QPushButton *m_save;
    QPushButton *m_delete;
    QPushButton *m_newDraw;

};

#endif // DRAWPOPUP_H
