// SPDX-FileCopyrightText: 2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef LOGGING_REGISTRATION_H
#define LOGGING_REGISTRATION_H

#include <QLoggingCategory>

Q_DECLARE_LOGGING_CATEGORY(RegistrationLog)

#endif // LOGGING_REGISTRATION_H
