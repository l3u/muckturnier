// SPDX-FileCopyrightText: 2021-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "DisqualificationDialog.h"

// Qt includes
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QSpinBox>

DisqualificationDialog::DisqualificationDialog(QWidget *parent, const QString &name, bool pairMode,
                                               int rounds)
    : TitleDialog(parent, DeleteMode::NoDelete)
{
    setTitle(pairMode ? tr("Paar disqualifizieren") : tr("Spieler disqualifizieren"));

    auto *descriptionLabel = new QLabel(tr(
        "Ab der ausgewählten Runde taucht %1 nicht mehr in der Rangliste auf. Die Eingabe von "
        "Ergebnissen (mit einem Vertretungs-Paar der Turnierleitung) ist weiterhin möglich.").arg(
        pairMode ? tr("das Paar") : tr("der Spieler")));
    descriptionLabel->setWordWrap(true);
    layout()->addWidget(descriptionLabel);

    auto *roundLayout = new QHBoxLayout;
    layout()->addLayout(roundLayout);

    roundLayout->addWidget(new QLabel(tr("Disqualifizieren ab Runde")));

    m_round = new QSpinBox;
    m_round->setMinimum(1);
    m_round->setMaximum(rounds);
    m_round->setValue(rounds);
    roundLayout->addWidget(m_round);

    roundLayout->addStretch();

    auto *questionLabel =  new QLabel(
        tr("<b>Soll %1 „%2“ wirklich disqualifiziert werden?</b>").arg(
           pairMode ? tr("das Paar") : tr("der Spieler"), name.toHtmlEscaped()));
    questionLabel->setWordWrap(true);
    layout()->addWidget(questionLabel);

    addButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    setButtonText(QDialogButtonBox::Ok, tr("Disqualifizieren"));
}

int DisqualificationDialog::round() const
{
    return m_round->value();
}
