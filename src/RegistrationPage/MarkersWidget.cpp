// SPDX-FileCopyrightText: 2018-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "MarkersWidget.h"
#include "MarkerDialog.h"
#include "MarkersModel.h"
#include "MarkersView.h"

#include "ServerPage/Server.h"
#include "ClientPage/Client.h"
#include "network/ChecksumHelper.h"

#include "Database/Database.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/Settings.h"
#include "SharedObjects/TournamentSettings.h"
#include "SharedObjects/IconSizeEngine.h"

#include "shared/TitleMenu.h"
#include "shared/SharedStyles.h"
#include "shared/MinimumViewportWidget.h"
#include "shared/MinimumViewportScroll.h"
#include "shared/NumbersComboBox.h"

// Qt includes
#include <QVBoxLayout>
#include <QDebug>
#include <QGroupBox>
#include <QApplication>
#include <QMessageBox>
#include <QCheckBox>
#include <QComboBox>
#include <QTimer>
#include <QLabel>
#include <QLineEdit>
#include <QSplitter>
#include <QStackedLayout>

// C++ includes
#include <functional>

static const QVector<Markers::Type> s_allMarkerTypes = {
    Markers::Default,
    Markers::Drawn,
    Markers::Booked,
    Markers::Single,
    Markers::Assigned
};

static const Markers::Marker s_markerUnchanged {
    -1,               // id
    QString(),        // name (not used)
    QColor(0, 0, 0),  // color (not used)
    Markers::NewBlock // sorting (not used)
};

MarkersWidget::MarkersWidget(QWidget *parent, SharedObjects *sharedObjects)
    : QWidget(parent),
      m_db(sharedObjects->database()),
      m_settings(sharedObjects->settings()),
      m_tournamentSettings(sharedObjects->tournamentSettings())
{
    auto *mainLayout = new QVBoxLayout(this);
    // This eliminates additional space around this widget when displayed inside the players page
    mainLayout->setContentsMargins(0, 0, 0, 0);

    // Markers list
    // ============

    auto *markersBox = new QGroupBox(tr("Markierungen"));
    auto *markersBoxLayout = new QVBoxLayout(markersBox);

    m_markersModel = new MarkersModel(this, sharedObjects);
    m_markersView = new MarkersView;
    m_markersView->setModel(m_markersModel);
    m_markersView->setIconSize(sharedObjects->iconSizeEngine()->relativeSize(1.35));
    markersBoxLayout->addWidget(m_markersView);

    connect(m_markersModel, &MarkersModel::visibilityChanged,
            this, &MarkersWidget::visibilityChanged);

    connect(m_markersView, &MarkersView::requestMoveMarker, this, &MarkersWidget::moveMarker);
    connect(m_markersView, &MarkersView::requestEditMarker, this, &MarkersWidget::editMarker);
    connect(m_markersView, &MarkersView::requestDeleteMarker, this, &MarkersWidget::deleteMarker);
    connect(m_markersView, &MarkersView::requestAddMarker, this, &MarkersWidget::addMarker);

    // Settings part
    // =============

    m_markerSettingsWidget = new MinimumViewportWidget;
    auto *settingsLayout = new QVBoxLayout(m_markerSettingsWidget);
    settingsLayout->setContentsMargins(0, 0, 0, 0);
    m_markerSettingsWidget->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);

    // Automatic markers

    m_automaticMarkersBox = new QGroupBox(tr("Automatische Markierungen"));
    m_automaticMarkersBox->setStyleSheet(SharedStyles::normalGroupBoxTitle);
    settingsLayout->addWidget(m_automaticMarkersBox);
    auto *automaticMarkersLayout = new QGridLayout(m_automaticMarkersBox);
    m_automaticMarkersBox->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);

    m_defaultMarkerLabel = new QLabel(tr("Neue Anmeldungen\nmarkieren als:"));
    m_defaultMarkerLabel->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    automaticMarkersLayout->addWidget(m_defaultMarkerLabel, 0, 0);
    auto *defaultMarkerCombo = new NumbersComboBox;
    m_markersComboBoxes[Markers::Default] = defaultMarkerCombo;
    defaultMarkerCombo->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Maximum);
    connect(defaultMarkerCombo, &NumbersComboBox::currentNumberChanged,
            m_tournamentSettings, std::bind(
                &TournamentSettings::setSpecialMarker, m_tournamentSettings,
                Markers::Default, std::placeholders::_1));
    automaticMarkersLayout->addWidget(defaultMarkerCombo, 0, 1);

    m_enableDrawnMarker = new QCheckBox(tr("Nach Auslosung\nmarkieren als:"));
    automaticMarkersLayout->addWidget(m_enableDrawnMarker, 1, 0);
    connect(m_enableDrawnMarker, &QCheckBox::toggled, this, &MarkersWidget::toggleDrawnMarker);

    auto *drawnMarker = new NumbersComboBox;
    m_markersComboBoxes[Markers::Drawn] = drawnMarker;
    drawnMarker->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Maximum);
    connect(drawnMarker, &NumbersComboBox::currentNumberChanged,
            this, &MarkersWidget::drawnMarkerChanged);

    automaticMarkersLayout->addWidget(drawnMarker, 1, 1);
    drawnMarker->setEnabled(false);

    // Ignored registrations marker

    m_bookedMarkerBox = new QGroupBox(tr("Voranmeldungen berücksichtigen"));
    m_bookedMarkerBox->setStyleSheet(SharedStyles::normalGroupBoxTitle);
    m_bookedMarkerBox->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
    m_bookedMarkerBox->setCheckable(true);
    m_bookedMarkerBox->setChecked(false);
    connect(m_bookedMarkerBox, &QGroupBox::toggled, this, &MarkersWidget::toggleBookedMarker);
    settingsLayout->addWidget(m_bookedMarkerBox);

    m_bookedLayout = new QStackedLayout(m_bookedMarkerBox);

    // "Booked" marker settings widget

    m_bookedSettingsWidget = new QWidget;
    auto *bookedSettingsWidgetLayout = new QHBoxLayout(m_bookedSettingsWidget);
    bookedSettingsWidgetLayout->addWidget(new QLabel(tr("Markierung für „Vorangemeldet“:")));

    auto *bookedMarker = new NumbersComboBox;
    m_markersComboBoxes[Markers::Booked] = bookedMarker;
    bookedMarker->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Maximum);
    connect(bookedMarker, &NumbersComboBox::currentNumberChanged,
            this, &MarkersWidget::bookedMarkerChanged);
    bookedSettingsWidgetLayout->addWidget(bookedMarker);

    m_bookedLayout->addWidget(m_bookedSettingsWidget);

    // Booking mode label
    m_bookingModeWidget = new QWidget;
    auto *bookingModeWidgetLayout = new QVBoxLayout(m_bookingModeWidget);
    m_bookedMarkerLabel = new QLabel;
    m_bookedMarkerLabel->setWordWrap(true);
    m_bookedMarkerLabel->setAlignment(Qt::AlignCenter);
    m_bookedMarkerLabel->setTextInteractionFlags(Qt::TextBrowserInteraction);
    connect(m_bookedMarkerLabel, &QLabel::linkActivated,
            this, [this]
            {
                m_bookedMarkerLabel->clearFocus();
                QMessageBox::information(this, tr("Markierung für Voranmeldungen ändern"),
                    tr("<p>Nachdem der Voranmeldungsmodus gestartet wurde, kann die Markierung für "
                       "Voranmeldungen nicht mehr geändert werden.</p>"
                       "<p>Wenn hier eine Änderung gewünscht wird, dann muss der "
                       "Voranmeldungsmodus deaktiviert werden. Hierzu auf der Voranmeldungsseite "
                       "(„Zusatzfunktionen“ → „Voranmeldung“) Die Einstellungen einblenden und "
                       "dann löschen.</p>"));
            });
    bookingModeWidgetLayout->addWidget(m_bookedMarkerLabel);
    m_bookedLayout->addWidget(m_bookingModeWidget);

    connect(m_tournamentSettings, &TournamentSettings::bookingEnabledChanged,
            this, &MarkersWidget::updateBookedMarkerBox);

    // Singles management

    m_singlesManagementBox = new QGroupBox(tr("Allein gekommene Spieler berücksichtigen"));
    m_singlesManagementBox->setCheckable(true);
    m_singlesManagementBox->setChecked(false);
    settingsLayout->addWidget(m_singlesManagementBox);
    m_singlesManagementBox->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);

    auto *singlesManagementLayout = new QGridLayout(m_singlesManagementBox);
    connect(m_singlesManagementBox, &QGroupBox::toggled,
            this, &MarkersWidget::singlesManagementToggled);

    auto *singlesMarkerLabel = new QLabel(tr("Markierung für „Allein da“:"));
    singlesManagementLayout->addWidget(singlesMarkerLabel, 0, 0);
    auto *singleMarkerCombo = new NumbersComboBox;
    m_markersComboBoxes[Markers::Single] = singleMarkerCombo;
    connect(singleMarkerCombo, &NumbersComboBox::currentNumberChanged,
            this, &MarkersWidget::singlesManagementChanged);
    singleMarkerCombo->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Maximum);
    singlesManagementLayout->addWidget(singleMarkerCombo, 0, 1, 1, 2);

    m_markSingles = new QCheckBox(tr("automatisch setzen\nwenn der Name"));
    connect(m_markSingles, &QCheckBox::toggled,
            this, &MarkersWidget::singlesManagementChanged);
    singlesManagementLayout->addWidget(m_markSingles, 1, 0);

    m_singleConditionString = new QLineEdit;
    m_singleConditionString->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Maximum);
    m_singleConditionString->setMinimumWidth(50);
    connect(m_singleConditionString, &QLineEdit::editingFinished,
            this, &MarkersWidget::singlesManagementChanged);
    singlesManagementLayout->addWidget(m_singleConditionString, 1, 1);

    m_singleCondition = new QComboBox;
    m_singleCondition->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    m_singleCondition->addItem(tr("nicht enthält"));
    m_singleCondition->addItem(tr("enthält"));
    connect(m_singleCondition, QOverload<int>::of(&QComboBox::currentIndexChanged),
            this, &MarkersWidget::singlesManagementChanged);
    singlesManagementLayout->addWidget(m_singleCondition, 1, 2);

    auto *assignedMarkerlabel = new QLabel(tr("Markierung nach Zuordnung:"));
    singlesManagementLayout->addWidget(assignedMarkerlabel, 2, 0);
    auto *assignedMarkerCombo = new NumbersComboBox;
    m_markersComboBoxes[Markers::Assigned] = assignedMarkerCombo;
    assignedMarkerCombo->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Maximum);
    connect(assignedMarkerCombo, &NumbersComboBox::currentNumberChanged,
            this, &MarkersWidget::singlesManagementChanged);
    singlesManagementLayout->addWidget(assignedMarkerCombo, 2, 1, 1, 2);

    // Splitter arrangement
    // --------------------

    auto *splitter = new QSplitter(Qt::Vertical);
    splitter->setHandleWidth(2);
    splitter->setStyleSheet(SharedStyles::visibleVerticalSplitter);

    splitter->addWidget(markersBox);
    splitter->setCollapsible(0, false);
    splitter->setStretchFactor(0, 4);

    m_settingsScroll = new MinimumViewportScroll(Qt::Horizontal);
    m_settingsScroll->setFrameShape(QFrame::NoFrame);
    m_settingsScroll->setWidgetResizable(true);
    m_settingsScroll->setMaximumHeight(m_markerSettingsWidget->sizeHint().height());
    m_settingsScroll->setWidget(m_markerSettingsWidget);

    splitter->addWidget(m_settingsScroll);
    splitter->setCollapsible(1, false);
    splitter->setStretchFactor(1, 5);

    mainLayout->addWidget(splitter);
}

void MarkersWidget::setMarkersComboBoxSelection(QComboBox *comboBox, int markerId)
{
    const int index = comboBox->findData(markerId);
    comboBox->setCurrentIndex(index != -1 ? index : comboBox->findData(0));
}

void MarkersWidget::updateTournamentMode()
{
    if (m_tournamentSettings->isSinglePlayerMode()) {
        m_markSingles->setChecked(false);
        m_singlesManagementBox->setVisible(false);
    } else {
        m_singlesManagementBox->setVisible(true);
    }

    m_settingsScroll->setMaximumHeight(m_markerSettingsWidget->sizeHint().height());
}

void MarkersWidget::reload()
{
    m_markersModel->refresh();
    m_markersView->resizeColumnsToContents();
    m_markersView->setDbIsChangeable(m_db->isChangeable());

    // Cache the selected marker for all combo boxes and clear them
    QHash<Markers::Type, int> selectedMarkers;
    for (const auto comboId : s_allMarkerTypes) {
        auto *comboBox = m_markersComboBoxes.value(comboId);
        const auto data = comboBox->currentData();
        selectedMarkers.insert(comboId, data.isValid() ? data.toInt() : -1);
        comboBox->blockSignals(true);
        comboBox->clear();
    }

    // Fill in the current markers
    for (const auto &marker : m_markersModel->markers()) {
        for (const auto comboId : s_allMarkerTypes) {
            m_markersComboBoxes.value(comboId)->addItem(marker.name, marker.id);
        }
    }

    // Try to re-select the markers that were selected before
    for (const auto comboId : s_allMarkerTypes) {
        auto *comboBox = m_markersComboBoxes.value(comboId);
        setMarkersComboBoxSelection(comboBox, selectedMarkers.value(comboId));
        comboBox->blockSignals(false);
    }

    Q_EMIT markersChanged();
}

void MarkersWidget::addMarker()
{
    MarkerDialog *dialog = new MarkerDialog(this, m_markersModel);
    connect(dialog, &MarkerDialog::markerSaved, this, &MarkersWidget::saveNewMarker);
    dialog->exec();
}

void MarkersWidget::saveNewMarker(const Markers::Marker &marker)
{
    if (m_isClient) {
        m_client->requestAddMarker(marker);
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);

    Q_EMIT statusUpdate(tr("Lege neue Markierung an …"));

    if (! m_db->addMarker(marker)) {
        return;
    }

    reload();

    if (m_isServer) {
        m_server->broadcastAddMarker(marker);
    }

    QApplication::restoreOverrideCursor();
    Q_EMIT statusUpdate(tr("Markierung „%1“ hinzugefügt").arg(marker.name));
}

void MarkersWidget::deleteMarker()
{
    const auto &selectedMarker = m_markersView->selectedMarker();
    const auto selectedRow = m_markersView->selectedRow();

    if (selectedMarker.id == bookedMarker()) {
        QMessageBox::warning(this, tr("Markierung löschen"),
            tr("<p><b>Löschen der Markierung nicht möglich</b></p>"
               "<p>Die Markierung „%1“ wird momentan für Voranmeldungen benutzt und kann deswegen "
               "nicht gelöscht werden.</p>"
               "<p>Um die Markierung zu löschen bitte vorher das Berücksichtigen von "
               "Voranmeldungen deaktivieren oder eine andere Markierung dafür auswählen.</p>").arg(
               selectedMarker.name.toHtmlEscaped()));
        return;
    } else if (selectedMarker.id == singleMarker()) {
        QMessageBox::warning(this, tr("Markierung löschen"),
            tr("<p><b>Löschen der Markierung nicht möglich</b></p>"
               "<p>Die Markierung „%1“ wird momentan für allein gekommene Spieler benutzt und kann "
               "deswegen nicht gelöscht werden.</p>"
               "<p>Um die Markierung zu löschen bitte vorher das Berücksichtigen allein gekommener "
               "Spieler deaktivieren oder eine andere Markierung dafür auswählen.</p>").arg(
               selectedMarker.name.toHtmlEscaped()));
        return;
    }

    if (QMessageBox::question(this, tr("Markierung löschen"),
            tr("Soll die Markierung „%1“ wirklich gelöscht werden?").arg(
               selectedMarker.name.toHtmlEscaped()),
            QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Cancel) == QMessageBox::Cancel) {

        return;
    }

    if (m_isClient) {
        m_client->requestDeleteMarker(selectedMarker.name, selectedMarker.id, selectedRow);
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);

    Q_EMIT statusUpdate(tr("Lösche Markierung „%1“ …").arg(selectedMarker.name));

    if (! m_db->deleteMarker(selectedMarker.id, selectedRow)) {
        return;
    }

    reload();

    if (m_isServer) {
        m_server->broadcastDeleteMarker(selectedMarker.name, selectedMarker.id, selectedRow);
    }

    QApplication::restoreOverrideCursor();
    Q_EMIT statusUpdate(tr("Markierung „%1“ gelöscht").arg(selectedMarker.name));
}

void MarkersWidget::editMarker()
{
    MarkerDialog *dialog = new MarkerDialog(this, m_markersModel, m_markersView->selectedMarker());
    connect(dialog, &MarkerDialog::markerSaved, this, &MarkersWidget::saveEdit);
    if (m_markersView->selectedRow() == 0) {
        dialog->disableSortingSelection();
    }
    dialog->exec();
}

void MarkersWidget::saveEdit(const Markers::Marker &marker)
{
    const auto &selectedMarker = m_markersView->selectedMarker();

    if (   marker.name    == selectedMarker.name
        && marker.color   == selectedMarker.color
        && marker.sorting == selectedMarker.sorting) {

        Q_EMIT statusUpdate(tr("Markierung „%1“ wurde nicht verändert").arg(selectedMarker.name));
        return;
    }

    if (m_isClient) {
        m_client->requestEditMarker(selectedMarker.name, selectedMarker.id,
                                    marker.name, marker.color, marker.sorting);
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    Q_EMIT statusUpdate(tr("Speichere Änderungen an Markierung „%1“ …").arg(selectedMarker.name));

    if (! m_db->editMarker(selectedMarker.id, marker.name, marker.color, marker.sorting)) {
        return;
    }

    reload();

    if (selectedMarker.id == m_tournamentSettings->specialMarker(Markers::Booked)
        && m_tournamentSettings->bookingEnabled()) {

        updateBookedMarkerBox();
    }

    if (m_isServer) {
        m_server->broadcastEditMarker(selectedMarker.name, selectedMarker.id,
                                      marker.name, marker.color, marker.sorting);
    }

    QApplication::restoreOverrideCursor();
    Q_EMIT statusUpdate(tr("Änderungen an Markierung „%1“ gespeichert").arg(selectedMarker.name));
}

void MarkersWidget::moveMarker(int direction)
{
    const auto &selectedMarker = m_markersView->selectedMarker();
    const auto selectedRow = m_markersView->selectedRow();

    if (m_isClient) {
        m_client->requestMoveMarker(selectedMarker.name, selectedMarker.id, selectedRow, direction);
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    Q_EMIT statusUpdate(tr("Verschiebe Markierung „%1“ %2 …").arg(
                           selectedMarker.name, direction == 1 ? tr("nach unten")
                                                               : tr("nach oben")));

    if (! m_db->moveMarker(selectedMarker.id, selectedRow, direction)) {
        return;
    }

    reload();

    if (m_isServer) {
        m_server->broadcastMoveMarker(selectedMarker.name, selectedMarker.id, selectedRow,
                                      direction);
    }

    QApplication::restoreOverrideCursor();
    Q_EMIT statusUpdate(tr("Markierung „%1“ %2 verschoben").arg(
                           selectedMarker.name, direction == 1 ? tr("nach unten")
                                                               : tr("nach oben")));
}

const QVector<Markers::Marker> &MarkersWidget::markers() const
{
    return m_markersModel->markers();
}

void MarkersWidget::reset()
{
    m_markersModel->showOnly(-1);

    setBookedMarker(m_tournamentSettings->specialMarker(Markers::Booked));
    setDrawnMarker(m_tournamentSettings->specialMarker(Markers::Drawn));

    setSinglesManagement(m_tournamentSettings->singlesManagement());
    setMarkersComboBoxSelection(m_markersComboBoxes.value(Markers::Default),
                                m_tournamentSettings->specialMarker(Markers::Default));

    m_bookedMarkerBox->setEnabled(m_db->isChangeable());
    m_singlesManagementBox->setEnabled(m_db->isChangeable());
    m_automaticMarkersBox->setEnabled(m_db->isChangeable());

    m_bookedMarkerInitialized = false;
    m_singleMarkerInitialized = false;
}

void MarkersWidget::updateCounters(const QHash<int, int> &counter)
{
    m_markersModel->updateMarkersCounts(counter);
    m_markersView->resizeColumnsToContents();
}

int MarkersWidget::newRegistrationMarker(const QString &name) const
{
    if (m_markSingles->isEnabled() && m_markSingles->isChecked()) {
        // The first m_singleCondition entry is "doesn't contain" (index 0), the second is
        // "contains" (index 1), so we can simply use the selected index here, because it will be
        // (per official C++ behavior) silently casted to a bool value in the following comparison

        if (name.contains(m_tournamentSettings->singlesManagement().conditionString)
            == m_singleCondition->currentIndex()) {

            return m_markersComboBoxes.value(Markers::Single)->currentNumber();
        }
    }

    return m_markersComboBoxes.value(Markers::Default)->currentNumber();
}

void MarkersWidget::toggleDrawnMarker(bool state)
{
    m_markersComboBoxes.value(Markers::Drawn)->setEnabled(state);
    drawnMarkerChanged();
}

void MarkersWidget::drawnMarkerChanged()
{
    m_tournamentSettings->setSpecialMarker(Markers::Drawn, drawnMarker());
}

int MarkersWidget::drawnMarker() const
{
    auto *combo = m_markersComboBoxes.value(Markers::Drawn);
    return combo->isEnabled() ? combo->currentNumber() : -1;
}

const Markers::Marker &MarkersWidget::markerAfterEdit(const QString &oldName,
                                                      const QString &name) const
{
    if (! m_markSingles->isEnabled() || ! m_markSingles->isChecked()) {
        return s_markerUnchanged;
    }

    const QString &condition = m_tournamentSettings->singlesManagement().conditionString;
    const bool oldContains = oldName.contains(condition);
    const bool contains = name.contains(condition);

    if (oldContains == contains) {
        // No change in the defined singles state --> leave the marker untouched
        return s_markerUnchanged;
    }

    // See above about the logic used here
    if (contains == m_singleCondition->currentIndex()) {
        return marker(m_markersComboBoxes.value(Markers::Single)->currentNumber());
    } else {
        return marker(m_markersComboBoxes.value(Markers::Assigned)->currentNumber());
    }
}

void MarkersWidget::addChecksumData(ChecksumHelper &checksumHelper) const
{
    // Add the markers data
    for (const auto &marker : m_markersModel->markers()) {
        checksumHelper.addData(marker);
    }

    // Add the "booked" marker
    checksumHelper.addData(bookedMarker());

    if (m_tournamentSettings->isPairMode()) {
        // Add the singles management data
        checksumHelper.addData(m_singlesManagementBox->isChecked());
        checksumHelper.addData(singleMarker());
        checksumHelper.addData(m_markSingles->isChecked());
        checksumHelper.addData(m_singleConditionString->text());
        checksumHelper.addData(m_singleCondition->currentData().toInt());
        checksumHelper.addData(assignedMarker());
    }
}

void MarkersWidget::serverStarted(Server *server)
{
    m_server = server;
    m_isServer = true;
}

void MarkersWidget::clientConnected(Client *client)
{
    m_client = client;
    m_isClient = true;
}

void MarkersWidget::serverStopped()
{
    m_isServer = false;
}

void MarkersWidget::clientDisconnected()
{
    m_isClient = false;
}

void MarkersWidget::processNetworkChange()
{
    const auto &selectedMarker = m_markersView->selectedMarker();
    const auto selectedRow = m_markersView->selectedRow();
    m_markersView->hideMarkerMenu();

    QString bookedName;
    const auto checkBookedName = m_tournamentSettings->bookingEnabled();
    if (checkBookedName) { // ... which means we do have a "booked" marker defined
        bookedName = m_markersModel->marker(
            m_tournamentSettings->specialMarker(Markers::Booked)).name;
    }

    reload();

    if (checkBookedName
        && bookedName != m_markersModel->marker(
                             m_tournamentSettings->specialMarker(Markers::Booked)).name) {

        updateBookedMarkerBox();
    }

    MarkerDialog *dialog = findChild<MarkerDialog *>();
    if (dialog != nullptr && dialog->isEditing()) {
        Markers::Marker currentMarker;
        if (selectedRow < m_markersModel->rowCount()) {
            currentMarker = m_markersModel->data(selectedRow);
        }

        if (currentMarker != selectedMarker) {
            dialog->reject();
            dialog->deleteLater();

            // This is invoked via a QTimer::singleShot call so that the function returns
            // immediately and the network processing is not blocked until the dialog is closed
            QTimer::singleShot(0, this, [this, selectedMarker]
            {
                QMessageBox::warning(this,
                    tr("Kollidierende Netzwerkänderung"),
                    tr("<p>Die Markierung „%1“ wurde zwischenzeitlich von einem anderen "
                       "Netzwerkteilnehmer entweder gelöscht, umbenannt oder geändert.</p>"
                       "<p>Das Ändern ist nicht mehr möglich und wird abgebrochen.</p>").arg(
                       selectedMarker.name.toHtmlEscaped()));
            });
            return;
        }
    }
}

void MarkersWidget::setProcessingRequest(bool processing)
{
    setEnabled(! processing);
    if (processing) {
        QApplication::setOverrideCursor(Qt::WaitCursor);
    }
}

void MarkersWidget::updateTournamentState()
{
    m_defaultMarkerLabel->setEnabled(! m_db->tournamentStarted());
    m_markersComboBoxes.value(Markers::Default)->setEnabled(! m_db->tournamentStarted());
    m_bookedMarkerBox->setEnabled(! m_db->tournamentStarted());
    m_singlesManagementBox->setEnabled(m_tournamentSettings->isPairMode()
                                       && ! m_db->tournamentStarted());
}

void MarkersWidget::singlesManagementToggled(bool state)
{
    if (state) {
        auto *singleCombo = m_markersComboBoxes[Markers::Single];

        if (! m_singleMarkerInitialized) {
            // We enable the singles management for the first time. The default selection is
            // "unmarked". This is probably not what the user wants. So we check if we can select
            // "the "Allein da" marker from the default markers template:
            int singleId = -1;
            for (const auto &marker : m_markersModel->markers()) {
                if (marker.name == tr("Allein da")) {
                    singleId = marker.id;
                    break;
                }
            }
            if (singleId != -1) {
                singleCombo->blockSignals(true);
                singleCombo->setCurrentNumber(singleId);
                singleCombo->blockSignals(false);
            }
            m_singleMarkerInitialized = true;
        }

        // Now, we have to check if we actually can leave the selection.
        // The "booked" marker can't be the same as the "single" marker.
        if (singleMarker() == bookedMarker()) {
            // Check if we can select another marker
            QVector<int> ids;
            for (const auto &marker : m_markersModel->markers()) {
                ids.append(marker.id);
            }
            ids.remove(singleMarker());
            if (! ids.isEmpty()) {
                singleCombo->blockSignals(true);
                singleCombo->setCurrentNumber(ids.first());
                singleCombo->blockSignals(false);

            } else {
                // The only available marker is already set for "booked".
                // The singles Management can't be activated.
                singleCombo->blockSignals(true);
                singleCombo->setCurrentNumber(0);
                singleCombo->blockSignals(false);
                m_singlesManagementBox->setChecked(false);
                QMessageBox::warning(this, tr("Allein gekommene Spieler"),
                    tr("<p><b>Aktivieren nicht möglich</b></p>"
                       "<p>Die Markierung für allein gekommene Spieler kann nicht dieselbe sein "
                       "wie die für Voranmeldungen. Die einzige verfügbare Markierung ist momentan "
                       "für Voranmeldungen gesetzt.</p>"
                       "<p>Bitte entweder eine weitere Markierung erstellen oder das "
                       "Berücksichtigen von Voranmeldungen deaktivieren!</p>"));
                return;
            }
        }
    }

    singlesManagementChanged();
}

void MarkersWidget::singlesManagementChanged()
{
    if (! m_tournamentSettings->singlesManagement().enabled
        && m_singlesManagementBox->isChecked()) {

        warnAboutSpecialMarkers();
    }

    const auto currentSinglesManagement = m_tournamentSettings->singlesManagement();

    if (m_singlesManagementBox->isChecked()
        && currentSinglesManagement.singleMarker != -1
        && singleMarker() != currentSinglesManagement.singleMarker
        && singleMarker() == bookedMarker()) {

        // The selected single marker was changed. We have to check
        // if it collides with the possibly selected "booked" marker.
        setSinglesManagement(currentSinglesManagement);
        QMessageBox::warning(this, tr("Allein gekommene Spieler"),
            tr("<p><b>Auswahl nicht möglich</b></p>"
               "<p>Es kann nicht dieselbe Markierung für allein gekommene Spieler <i>und</i> "
               "Voranmeldungen benutzt werden!</p>"));
        return;
    }

    // First, we cache the currently entered data
    const Markers::SinglesManagement singlesManagement {
        m_singlesManagementBox->isChecked(),          // enabled
        singleMarker(),                               // singleMarker
        m_markSingles->isChecked(),                   // markSingles
        m_singleConditionString->text().simplified(), // conditionString
        m_singleCondition->currentIndex(),            // condition
        assignedMarker()                              // assignedMarker
    };

    if (m_isClient) {
        // Reset the GUI to the last state in case the request fails
        setSinglesManagement(currentSinglesManagement);
        // Request the change
        m_client->requestSinglesManagementChange(singlesManagement);
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    m_tournamentSettings->setSinglesManagement(singlesManagement);
    Q_EMIT updatePlayersList();

    if (m_isServer) {
        m_server->broadcastSinglesManagementChange(singlesManagement);
    }

    QApplication::restoreOverrideCursor();
}

int MarkersWidget::singleMarker() const
{
    return m_singlesManagementBox->isChecked()
               ? m_markersComboBoxes.value(Markers::Single)->currentNumber() : -1;
}

int MarkersWidget::bookedMarker() const
{
    auto *bookedCombo = m_markersComboBoxes.value(Markers::Booked);
    return bookedCombo->isEnabled() ? bookedCombo->currentNumber() : -1;
}

int MarkersWidget::assignedMarker() const
{
    const auto &assignedMarker = m_markersComboBoxes.value(Markers::Assigned);
    return assignedMarker->isEnabled() ? assignedMarker->currentNumber() : -1;
}

void MarkersWidget::setSinglesManagement(const Markers::SinglesManagement &data)
{
    auto *assignedMarker = m_markersComboBoxes.value(Markers::Assigned);
    auto *singleMarker = m_markersComboBoxes.value(Markers::Single);

    m_singlesManagementBox->blockSignals(true);
    singleMarker->blockSignals(true);
    m_markSingles->blockSignals(true);
    m_singleConditionString->blockSignals(true);
    m_singleCondition->blockSignals(true);
    assignedMarker->blockSignals(true);

    m_singlesManagementBox->setChecked(data.enabled);
    setMarkersComboBoxSelection(singleMarker, data.singleMarker);
    m_markSingles->setChecked(data.markSingles);
    m_singleConditionString->setText(data.conditionString);
    m_singleCondition->setCurrentIndex(data.condition == 0 ? 0 : 1);
    setMarkersComboBoxSelection(assignedMarker, data.assignedMarker);

    m_singlesManagementBox->blockSignals(false);
    singleMarker->blockSignals(false);
    m_markSingles->blockSignals(false);
    m_singleConditionString->blockSignals(false);
    m_singleCondition->blockSignals(false);
    assignedMarker->blockSignals(false);

    if (data.enabled && ! m_tournamentSettings->loadingDb()) {
        warnAboutSpecialMarkers();
    }
}

void MarkersWidget::resetSingleConditionString()
{
    m_singleConditionString->setText(m_settings->namesSeparator().simplified());
    singlesManagementChanged();
}

QString MarkersWidget::singleConditionString() const
{
    return m_singleConditionString->text();
}

void MarkersWidget::setDrawnMarker(int markerId)
{
    auto *drawnCombo = m_markersComboBoxes.value(Markers::Drawn);
    m_enableDrawnMarker->blockSignals(true);
    drawnCombo->blockSignals(true);

    m_enableDrawnMarker->setChecked(markerId != -1);
    drawnCombo->setEnabled(markerId != -1);

    if (markerId != -1) {
        setMarkersComboBoxSelection(m_markersComboBoxes.value(Markers::Drawn), markerId);
    }

    m_enableDrawnMarker->blockSignals(false);
    drawnCombo->blockSignals(false);
}

void MarkersWidget::setBookedMarker(int markerId)
{
    auto *combo = m_markersComboBoxes.value(Markers::Booked);

    m_bookedMarkerBox->blockSignals(true);
    combo->blockSignals(true);

    m_bookedMarkerBox->setChecked(markerId != -1);
    if (markerId != -1) {
        setMarkersComboBoxSelection(combo, markerId);

        if (! m_tournamentSettings->loadingDb()) {
            warnAboutSpecialMarkers();
        }
    }

    m_bookedMarkerBox->blockSignals(false);
    combo->blockSignals(false);

    Q_EMIT bookedMarkerChange();
}

void MarkersWidget::toggleBookedMarker(bool state)
{
    if (! state && m_tournamentSettings->bookingEnabled()) {
        m_bookedMarkerBox->blockSignals(true);
        m_bookedMarkerBox->setChecked(true);
        m_bookedMarkerBox->blockSignals(false);
        QMessageBox::information(this, tr("Voranmeldungsmodus aktiv"),
            tr("<p>Das Berücksichtigen von Voranmeldungen kann im Voranmeldungsmodus nicht "
               "deaktiviert werden.</p>"
               "<p>Wenn keine Voranmeldungen berücksichtigt werden sollen, muss der "
               "Voranmeldungsmodus deaktiviert werden. Hierzu auf der Voranmeldungsseite "
               "(„Zusatzfunktionen“ → „Voranmeldung“) die Einstellungen einblenden und dann "
               "löschen.</p>"));
        return;
    }

    auto *bookedCombo = m_markersComboBoxes.value(Markers::Booked);

    if (state) {
        if (! m_bookedMarkerInitialized) {
            // We're enabling the "booked" marker and it was not enabled before. The default
            // selection is "(unmarked)". This is possibly not what the user wants. Thus, we check
            // if we can set the "Vorangemeldet" marker from the default markers template.
            int preregisteredId = -1;
            for (const auto &marker : m_markersModel->markers()) {
                if (marker.name == tr("Vorangemeldet")) {
                    preregisteredId = marker.id;
                    break;
                }
            }
            if (preregisteredId != -1) {
                bookedCombo->blockSignals(true);
                bookedCombo->setCurrentNumber(preregisteredId);
                bookedCombo->blockSignals(false);
            }
            m_bookedMarkerInitialized = true;
        }

        // Now, we have to check if we actually can leave the selection as-is.
        // The "booked" marker can't be the same as the "single" marker.
        if (bookedMarker() == singleMarker()) {
            // Check if we can select another marker
            QVector<int> ids;
            for (const auto &marker : m_markersModel->markers()) {
                ids.append(marker.id);
            }
            ids.remove(bookedMarker());
            if (! ids.isEmpty()) {
                bookedCombo->blockSignals(true);
                bookedCombo->setCurrentNumber(ids.first());
                bookedCombo->blockSignals(false);

            } else {
                // The only available marker is already set for "single".
                // "Booked" can't be activated.
                bookedCombo->blockSignals(true);
                bookedCombo->setCurrentNumber(0);
                m_bookedMarkerBox->setChecked(false);
                bookedCombo->blockSignals(false);
                QMessageBox::warning(this, tr("Voranmeldungen berücksichtigen"),
                    tr("<p><b>Aktivieren nicht möglich</b></p>"
                       "<p>Die Markierung für Voranmeldungen kann nicht dieselbe sein wie die für "
                       "allein gekommene Spieler. Die einzige verfügbare Markierung ist momentan "
                       "für allein gekommene Spieler gesetzt.</p>"
                       "<p>Bitte entweder eine weitere Markierung erstellen oder das "
                       "Berücksichtigen allein gekommener Spieler deaktivieren!</p>"));
                return;
            }
        }
    }

    bookedMarkerChanged();
}

void MarkersWidget::bookedMarkerChanged()
{
    if (m_tournamentSettings->specialMarker(Markers::Booked) == -1) {
        warnAboutSpecialMarkers();
    }

    // Cache the "booked" marker combo box
    auto *bookedCombo = m_markersComboBoxes.value(Markers::Booked);

    // Check if we just selected the same marker which is currently set as the "single" marker
    if (bookedCombo->isEnabled() && bookedMarker() == singleMarker()) {
        bookedCombo->blockSignals(true);
        bookedCombo->setCurrentNumber(m_tournamentSettings->specialMarker(Markers::Booked));
        bookedCombo->blockSignals(false);
        QMessageBox::warning(this, tr("Voranmeldungen"),
            tr("<p><b>Auswahl nicht möglich</b></p>"
               "<p>Es kann nicht dieselbe Markierung für Voranmeldungen <i>und</i> allein "
               "gekommene Spieler benutzt werden!</p>"));
        return;
    }

    QString bookedMarkerName;
    int bookedMarkerId = -1;
    if (bookedCombo->isEnabled()) {
        bookedMarkerName = bookedCombo->currentText();
        bookedMarkerId = bookedCombo->currentNumber();
    }

    if (m_isClient) {
        // First reset the GUI to the last setting in case the request fails
        setBookedMarker(m_tournamentSettings->specialMarker(Markers::Booked));
        // Then request the change
        m_client->requestSetBookedMarker(bookedMarkerName, bookedMarkerId);
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    m_tournamentSettings->setSpecialMarker(Markers::Booked, bookedMarkerId);

    Q_EMIT updatePlayersList();
    Q_EMIT bookedMarkerChange();

    if (m_isServer) {
        m_server->broadcastSetBookedMarker(bookedMarkerName, bookedMarkerId);
    }

    QApplication::restoreOverrideCursor();
}

void MarkersWidget::warnAboutSpecialMarkers()
{
    if (! m_showSpecialMarkersWarning || m_db->tournamentStarted()) {
        return;
    }

    QTimer::singleShot(0, this, [this]
    {
        QMessageBox::information(this, tr("Beenden der Anmeldung"),
            tr("<p><b>Bitte beachten:</b></p>"
               "<p>Solang es Anmeldungen gibt, die als „Vorangemeldet“ und/oder als „Allein da“ "
               "markiert sind, kann das Turnier nicht gestartet werden.<p>"
               "<p>Bitte zum Starten des Turniers alle entsprechenden Markierungen entfernen, "
               "die so markierten Anmeldungen löschen oder die entsprechende(n) Option(en) "
               "deaktivieren.</p>"));
    });

    m_showSpecialMarkersWarning = false;
}

const MarkersModel *MarkersWidget::markersModel() const
{
    return m_markersModel;
}

const Markers::Marker &MarkersWidget::marker(int id) const
{
    return m_markersModel->marker(id);
}

void MarkersWidget::updateBookedMarkerBox()
{
    if (m_tournamentSettings->bookingEnabled()) {
        m_bookedMarkerLabel->setText(tr(
            "<p><i>Voranmeldungsmodus aktiv</i>: "
            "Für Voranmeldungen wird die Markierung <i>„%1“</i> benutzt. "
            "<a href=\"#\">Ändern</a></p>").arg(
            m_markersComboBoxes[Markers::Booked]->currentText().toHtmlEscaped()));
        m_bookedLayout->setCurrentWidget(m_bookingModeWidget);
    } else {
        m_bookedLayout->setCurrentWidget(m_bookedSettingsWidget);
    }
}

void MarkersWidget::setShowSpecialMarkersWarning(bool state)
{
    m_showSpecialMarkersWarning = state;
}
