// SPDX-FileCopyrightText: 2023-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef REGISTRATIONSDELEGATE_H
#define REGISTRATIONSDELEGATE_H

// Local includes
#include "shared/TableDelegate.h"

class RegistrationsDelegate : public TableDelegate
{
    Q_OBJECT

public:
    explicit RegistrationsDelegate(QObject *parent);

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &,
                          const QModelIndex &) const override;
    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option,
                              const QModelIndex &index) const override;
    void setEditorData(QWidget *editor, const QModelIndex &index) const override;
    void setModelData(QWidget *editor, QAbstractItemModel *,
                      const QModelIndex &index) const override;

Q_SIGNALS:
    void editorCreated(QWidget *editor, const QString &name) const;
    void requestRenamePlayers(const QString &originalName, const QString &name) const;

};

#endif // REGISTRATIONSDELEGATE_H
