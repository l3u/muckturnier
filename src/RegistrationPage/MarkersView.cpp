// SPDX-FileCopyrightText: 2023-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "MarkersView.h"
#include "MarkersModel.h"

#include "shared/TitleMenu.h"

// Qt includes
#include <QDebug>
#include <QMenu>

// C++ includes
#include <functional>

MarkersView::MarkersView(QWidget *parent) : TableView(parent)
{
    setEditTriggers(QAbstractItemView::NoEditTriggers);
    setContextMenuPolicy(Qt::CustomContextMenu);

    connect(this, &QTableView::clicked, this, &MarkersView::indexClicked);
    connect(this, &QTableView::doubleClicked, this, &MarkersView::indexDoubleClicked);
    connect(this, &QTableView::customContextMenuRequested, this, &MarkersView::showContextMenu);

    m_setVisibilityMenu = new QMenu(this);
    m_showOnlyAction = m_setVisibilityMenu->addAction(QString());
    m_showAllAction = m_setVisibilityMenu->addAction(tr("Alle anzeigen"));

    m_markersMenu = new TitleMenu(this);

    m_moveUpAction = m_markersMenu->addAction(tr("Nach oben bewegen"));
    connect(m_moveUpAction, &QAction::triggered,
            this, std::bind(&MarkersView::requestMoveMarker, this, -1));

    m_moveDownAction = m_markersMenu->addAction(tr("Nach unten bewegen"));
    connect(m_moveDownAction, &QAction::triggered,
            this, std::bind(&MarkersView::requestMoveMarker, this, 1));

    m_markersMenu->addSeparator();

    m_editMarkerAction = m_markersMenu->addAction(tr("Bearbeiten"));
    connect(m_editMarkerAction, &QAction::triggered, this, &MarkersView::requestEditMarker);

    m_deleteMarkerAction = m_markersMenu->addAction(tr("Löschen"));
    connect(m_deleteMarkerAction, &QAction::triggered, this, &MarkersView::requestDeleteMarker);

    m_markersMenu->addSeparator();

    m_newMarkerAction = m_markersMenu->addAction(tr("Neue Markierung"));
    connect(m_newMarkerAction, &QAction::triggered, this, &MarkersView::requestAddMarker);

    m_newMarkerMenu = new QMenu(this);
    m_newMarkerMenu->addAction(m_newMarkerAction);
}

void MarkersView::setModel(QAbstractItemModel *model)
{
    QTableView::setModel(model);
    m_model = qobject_cast<MarkersModel *>(model);
    connect(m_showAllAction, &QAction::triggered,
            m_model, std::bind(&MarkersModel::showOnly, m_model, -1));
}

void MarkersView::setDbIsChangeable(bool state)
{
    m_dbIsChangeable = state;
    setTrackHovering(state);
}

void MarkersView::indexClicked(const QModelIndex &index)
{
    if (index.column() != 3) {
        return;
    }
    m_model->toggleVisible(index.row());
}

void MarkersView::indexDoubleClicked(const QModelIndex &index)
{
    if (index.column() == 3) {
        return;
    }
    showContextMenu(viewport()->mapFromGlobal(QCursor::pos()));
}

void MarkersView::showContextMenu(const QPoint &position)
{
    const auto index = indexAt(position);
    const auto target = viewport()->mapToGlobal(position);

    if (! index.isValid()) {
        if (m_dbIsChangeable) {
            m_newMarkerMenu->exec(target);
            return;
        } else {
            return;
        }
    }

    const auto row = index.row();
    const auto column = index.column();

    // Show the "Show only/show all" menu
    if (column == 3) {
        m_showOnlyAction->setText(tr("Nur „%1“ anzeigen").arg(m_model->data(row).name));
        m_showOnlyAction->disconnect();
        connect(m_showOnlyAction, &QAction::triggered,
                m_model, std::bind(&MarkersModel::showOnly, m_model, row));
        m_showAllAction->setEnabled(! m_model->allVisible());
        m_setVisibilityMenu->exec(target);
    }

    // All other actions require write access, so return here if the database if ro
    if (! m_dbIsChangeable) {
        return;
    }

    if (column == 1 || column == 2) {
        m_selectedRow = row;
        m_selectedMarker = m_model->data(row);

        m_markersMenu->setTitle(tr("Markierung „%1“").arg(m_model->data(row).name));
        m_moveUpAction->setEnabled(m_selectedRow > 0);
        m_moveDownAction->setEnabled(m_selectedRow < m_model->rowCount() - 1);
        m_editMarkerAction->setEnabled(m_selectedMarker.id != 0);
        m_deleteMarkerAction->setEnabled(m_selectedMarker.id != 0);

        updateHoveredRow(index);
        setKeepHoveredRow(true);
        m_markersMenu->exec(target);
        setKeepHoveredRow(false);
    }
}

const Markers::Marker &MarkersView::selectedMarker() const
{
    return m_selectedMarker;
}

int MarkersView::selectedRow() const
{
    return m_selectedRow;
}

void MarkersView::hideMarkerMenu()
{
    m_markersMenu->hide();
}
