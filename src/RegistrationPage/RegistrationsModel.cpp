// SPDX-FileCopyrightText: 2023-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "Application.h"

#include "RegistrationsModel.h"
#include "MarkersModel.h"

#include "shared/Logging.h"
#include "shared/RegistrationColumns.h"
#include "shared/Json.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/ResourceFinder.h"
#include "SharedObjects/StringTools.h"
#include "SharedObjects/SearchEngine.h"
#include "SharedObjects/TournamentSettings.h"

// Qt includes
#include <QDebug>
#include <QApplication>
#include <QStyle>
#include <QJsonArray>

// C++ includes
#include <algorithm>
#include <utility>

namespace rc = RegistrationColumns;

static const QString s_noName;
static const QLatin1String s_exactOr("@EXACT_OR");

RegistrationsModel::RegistrationsModel(QObject *parent, SharedObjects *sharedObjects,
                                       const MarkersModel *markersModel)
    : QAbstractTableModel(parent),
      m_columnCount(rc::columnIndex.count()),
      m_db(sharedObjects->database()),
      m_resourceFinder(sharedObjects->resourceFinder()),
      m_stringTools(sharedObjects->stringTools()),
      m_searchEngine(sharedObjects->searchEngine()),
      m_tournamentSettings(sharedObjects->tournamentSettings()),
      m_markersModel(markersModel)
{
    initializeIcons();
    connect(qobject_cast<Application *>(qApp), &Application::paletteChanged,
            this, &RegistrationsModel::initializeIcons);
}

void RegistrationsModel::initializeIcons()
{
    m_markerIcon = QIcon(m_resourceFinder->find(QStringLiteral("marker.png")));
    m_drawIcon = QIcon(m_resourceFinder->find(QStringLiteral("draw.png")));
    m_disqualifiedIcon = QApplication::style()->standardIcon(QStyle::SP_DialogCloseButton);
}

int RegistrationsModel::rowCount(const QModelIndex &) const
{
    return m_rowCount;
}

int RegistrationsModel::columnCount(const QModelIndex &) const
{
    return m_columnCount;
}

Qt::ItemFlags RegistrationsModel::flags(const QModelIndex &index) const
{
    if (index.column() == rc::columnIndex.value(rc::Column::Name)) {
        return Qt::ItemIsEnabled | Qt::ItemIsEditable;
    } else {
        return Qt::ItemIsEnabled;
    }
}

QVariant RegistrationsModel::data(const QModelIndex &index, int role) const
{
    if (! index.isValid()) {
        return QVariant();
    }

    const auto row = index.row();
    const auto column = index.column();
    if (row >= rowCount() || column > rc::columnIndex.count()) {
        return QVariant();
    }
    const auto &data = m_playersData.at(row);

    if (role == Qt::DisplayRole) {
        if (column == rc::columnIndex.value(rc::Column::Name)) {
            return data.name;

        } else if (column == rc::columnIndex.value(rc::Column::Number)) {
            return data.number != 0 ? QString::number(data.number) : tr("…");

        } else if (column == rc::columnIndex.value(rc::Column::Draw)) {
            if (data.draw.contains(m_drawRound)) {
                const auto &seat = data.draw[m_drawRound];
                if (m_tournamentSettings->isPairMode()) {
                    return tr("Tisch %1 Paar %2").arg(QString::number(seat.table),
                                                      QString::number(seat.pair));
                } else {
                    return tr("Tisch %1 Paar %2 (%3)").arg(QString::number(seat.table),
                                                           QString::number(seat.pair),
                                                           QString::number(seat.player));
                }
            }

        } else if (column == rc::columnIndex.value(rc::Column::Marker)) {
            return data.marker != 0 ? m_markerNames.value(data.marker) : QVariant();
        }

    } else if (role == Qt::ForegroundRole && column == rc::columnIndex.value(rc::Column::Name)) {
        return data.marker != 0 ? m_markerColors.value(data.marker) : QVariant();

    } else if (role == Qt::DecorationRole) {
        if (column == rc::columnIndex.value(rc::Column::Name) && data.disqualified != 0) {
            return m_disqualifiedIcon;

        } else if (column == rc::columnIndex.value(rc::Column::Mark)) {
            return m_markerIcon;

        } else if (column == rc::columnIndex.value(rc::Column::Draw)
                   && ! data.draw.contains(m_drawRound)) {

            return m_drawIcon;
        }

    } else if (role == Qt::ToolTipRole) {
        if (column == rc::columnIndex.value(rc::Column::Name) && data.disqualified != 0) {
            return tr("Disqualifiziert in Runde %1").arg(data.disqualified);
        }

    }

    return QVariant();
}

QVariant RegistrationsModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole || orientation == Qt::Vertical) {
        return QVariant();
    }

    if (section == rc::columnIndex.value(rc::Column::Name)) {
        return tr("Name(n)");
    } else if (section == rc::columnIndex.value(rc::Column::Number)) {
        return tr("#");
    } else if (section == rc::columnIndex.value(rc::Column::Mark)) {
        return QString();
    } else if (section == rc::columnIndex.value(rc::Column::Draw)) {
        return tr("Auslosung");
    } else if (section == rc::columnIndex.value(rc::Column::Marker)) {
        return tr("Markierung");
    }

    // We should not reach here
    qCWarning(MuckturnierLog) << "RegistrationsModel::headerData: Unknown section:" << section;
    return QVariant();
}

void RegistrationsModel::clear()
{
    m_searchEngine->clearSearchCache();

    const auto lastRow = m_rowCount - 1;
    beginRemoveRows(QModelIndex(), 0, lastRow);

    m_playersData.clear();
    m_rowCount = 0;
    m_markerColors.clear();
    m_markerNames.clear();
    m_markerCounts.clear();
    m_drawnPair1Tables.clear();
    m_drawnPair2Tables.clear();
    m_incompleteTables.clear();
    m_tableGaps.clear();
    m_searchHidden.clear();
    m_numbers.clear();
    m_currentDraw.clear();

    endRemoveRows();
    Q_EMIT dataChanged(index(0, 0), index(lastRow, m_columnCount - 1), { Qt::DisplayRole });
}

void RegistrationsModel::refresh()
{
    clear();

    // Get the players data
    auto [ playersData, success ] = m_db->playersData();
    if (! success) {
        return;
    }

    m_rowCount = playersData.count();
    const auto lastRow = m_rowCount - 1;
    beginInsertRows(QModelIndex(), 0, lastRow);

    // Extract a list of all names and sort it
    QHash<QString, int> nameIndex;
    QVector<QString> names;
    for (int i = 0; i < m_rowCount; i++) {
        const auto &name = playersData.at(i).name;
        nameIndex[name] = i;
        names.append(name);
    }
    m_stringTools->sort(names);

    // Get all markers
    const auto &markers = m_markersModel->markers();
    for (const auto &marker : markers) {
        m_markerColors[marker.id] = marker.color;
        m_markerNames[marker.id] = marker.name;
    }

    // Be sure to not refer to a non-existant marker. This should not happen, but however, it could
    // happen if the database was manupulated by hand. Thus, we have to handle this:
    const auto markerIds = m_markerColors.keys();
    for (const auto &name : std::as_const(names)) {
        if (! markerIds.contains(playersData.at(nameIndex.value(name)).marker)) {
            qCWarning(MuckturnierLog)
                << "Pair or player" << name << "references non-existant marker"
                << playersData.at(nameIndex.value(name)).marker << "- falling back to \"unmarked\"";
            playersData[nameIndex.value(name)].marker = 0;
        }
    }

    // Sort all registrations according to their marker

    // Create a list of all blocks we need
    QHash<int, int> targetBlock;
    QVector<QVector<QString>> block;
    for (const auto &marker : markers) {
        switch (marker.sorting) {
        case Markers::MarkerSorting::NewBlock:
            block.append(QVector<QString>());
            targetBlock[marker.id] = block.count() - 1;
            break;
        case Markers::MarkerSorting::Merged:
            targetBlock[marker.id] = block.count() - 1;
            break;
        }
    }

    // Iterate through all players:
    // - Add them to their respective blocks
    // - Update the SearchEngine
    // - Count the markers
    // - Get the assigned tables
    // - Get the assigned players numbers

    m_drawsCount = 0;
    m_drawsPresent = false;

    for (const QString &name : std::as_const(names)) {
        const auto &data = playersData.at(nameIndex.value(name));

        block[targetBlock.value(data.marker)].append(name);
        m_searchEngine->updateSearchCache(name);
        m_markerCounts[data.marker]++;

        if (! m_drawsPresent && ! data.draw.isEmpty()) {
            m_drawsPresent = true;
        }

        if (data.draw.contains(m_drawRound)) {
            const auto &seat = data.draw[m_drawRound];
            if (seat.pair == 1) {
                m_drawnPair1Tables.append(seat.table);
            } else {
                m_drawnPair2Tables.append(seat.table);
            }
            m_drawsCount++;

            if (m_drawRound > 1) {
                if (! m_currentDraw.contains(seat.table)) {
                    m_currentDraw[seat.table] = QVector<int> { 0, 0, 0, 0 };
                }
                m_currentDraw[seat.table][Draw::indexForPlayer(seat.pair, seat.player)] = data.id;
            }
        }

        if (data.number > 0) {
            m_numbers.append(data.number);
        }
    }

    // Generate a sorted copy of playersData
    for (const auto &list : std::as_const(block)) {
        for (const auto &name : list) {
            m_playersData.append(playersData.at(nameIndex.value(name)));
        }
    }

    // Process the assigned tables

    std::sort(m_drawnPair1Tables.begin(), m_drawnPair1Tables.end());
    std::sort(m_drawnPair2Tables.begin(), m_drawnPair2Tables.end());

    // Generate a sorted list of all drawn tables
    QSet<int> assignedTablesSet;
    for (const int table : m_drawnPair1Tables) {
        assignedTablesSet.insert(table);
    }
    for (const int table : m_drawnPair2Tables) {
        assignedTablesSet.insert(table);
    }
    QVector<int> allAssignedTables(assignedTablesSet.begin(), assignedTablesSet.end());
    std::sort(allAssignedTables.begin(), allAssignedTables.end());

    // Check for tables with incomplete (missing pair 1 or 2 or incomplete pairs) draw
    const int neededCount = m_tournamentSettings->isPairMode() ? 1 : 2;
    for (const int table : std::as_const(allAssignedTables)) {
        if (   m_drawnPair1Tables.count(table) != neededCount
            || m_drawnPair2Tables.count(table) != neededCount) {

            m_incompleteTables.append(table);
        }
    }

    // Search for gaps in the draw
    int consecutiveNumber = 1;
    for (int i = 0; i < allAssignedTables.count(); i++) {
        if (consecutiveNumber < allAssignedTables.at(i)) {
            for (int j = consecutiveNumber; j < allAssignedTables.at(i); j++) {
                m_tableGaps.append(j);
            }
            consecutiveNumber = allAssignedTables.at(i) + 1;
        } else {
            consecutiveNumber++;
        }
    }

    // Update the rows matching a possibly set search term
    updateSearchHidden(m_searchTerm, m_phoneticSearch);

    endInsertRows();
    Q_EMIT dataChanged(index(0, 0), index(lastRow, m_columnCount - 1), { Qt::DisplayRole });
}

const QHash<int, int> &RegistrationsModel::markerCounts() const
{
    return m_markerCounts;
}

int RegistrationsModel::markerCount(int marker) const
{
    // This will also return 0 for non-existant markers. We need this for e.g. non-set markers
    // like the single players marker, and when creating a new database.
    return m_markerCounts.value(marker);
}

const QVector<int> &RegistrationsModel::assignedTables(int pair) const
{
    Q_ASSERT(pair == 1 || pair == 2);
    return pair == 1 ? m_drawnPair1Tables : m_drawnPair2Tables;
}

const QVector<int> &RegistrationsModel::incompleteTables() const
{
    return m_incompleteTables;
}

const QVector<int> &RegistrationsModel::tableGaps() const
{
    return m_tableGaps;
}

const Players::Data &RegistrationsModel::data(int row) const
{
    Q_ASSERT(row < m_rowCount);
    if (row != -1) {
        return m_playersData.at(row);
    } else {
        qCWarning(MuckturnierLog) << "RegistrationsModel::data: Requested data for invalid row "
                                  << "-1. Returning an empty dataset";
        return Players::noData;
    }
}

const Players::Data &RegistrationsModel::data(const QString &name) const
{
    const auto row = rowForName(name);
    if (row != -1) {
        return data(row);
    } else {
        qCWarning(MuckturnierLog) << "RegistrationsModel::data: Requested data for name" << name
                                  << "which could not be found. Returning an empty dataset";
        return Players::noData;
    }
}

const QVector<Players::Data> &RegistrationsModel::playersData() const
{
    return m_playersData;
}

bool RegistrationsModel::hasExactMatch(const QString &name) const
{
    return rowForName(name) != -1;
}

bool RegistrationsModel::hasCaseInsensitiveMatch(const QString &name) const
{
    const QString lowerName = m_stringTools->toLower(name);
    for (const auto &data : m_playersData) {
        if (lowerName == m_stringTools->toLower(data.name)) {
            return true;
        }
    }
    return false;
}

QString RegistrationsModel::hasFuzzyMatch(const QString &text, bool phoneticSearch,
                                          const QString &exclude) const
{
    m_searchEngine->setSearchTerm(text);

    for (const auto &data : m_playersData) {
        if (data.name == exclude) {
            continue;
        }
        if (m_searchEngine->checkMatch(data.name, SearchEngine::DefaultSearch, phoneticSearch)) {
            return data.name;
        }
    }
    return QString();
}

int RegistrationsModel::rowForName(const QString &name) const
{
    if (name.isEmpty()) {
        return -1;
    }

    for (int i = 0; i < m_rowCount; i++) {
        if (m_playersData.at(i).name == name) {
            return i;
        }
    }

    return -1;
}

QModelIndex RegistrationsModel::indexForName(const QString &name) const
{
    const auto row = rowForName(name);
    if (row == -1) {
        return QModelIndex();
    }
    return index(row, rc::columnIndex.value(rc::Name));
}

bool RegistrationsModel::drawsPresent() const
{
    return m_drawsPresent;
}

int RegistrationsModel::drawsCount() const
{
    return m_drawsCount;
}

void RegistrationsModel::updateSearchHidden(const QString &text, bool phoneticSearch)
{
    m_searchTerm = text;
    m_formattedSearchTerm.clear();
    bool isNumber = false;
    const auto number = text.toInt(&isNumber);
    m_phoneticSearch = phoneticSearch;

    m_searchHidden.clear();

    if (m_searchTerm.isEmpty()) {
        // Search term is empty --> each row is visible
        m_searchHidden.fill(false, m_rowCount);

    } else if (isNumber && number > 0) {
        // We search for a pure number --> check for a matching players number
        m_formattedSearchTerm = tr("Nummer %1").arg(number);
        m_searchHidden.fill(true, m_rowCount);
        for (int i = 0; i < m_rowCount; i++) {
            if (m_playersData.at(i).number == number) {
                m_searchHidden[i] = false;
                break;
            }
        }

    } else if (text.startsWith(s_exactOr)) {
        // We want to do an exact OR search for a list of names

        // Get the JSON array from the search string and parse it
        QVector<QString> names;
        for (const auto &value : Json::arrayFromString(text.mid(9))) {
            const auto name = value.toString();
            if (name.isEmpty()) {
                continue;
            }
            names.append(name);
            if (m_formattedSearchTerm.isEmpty()) {
                m_formattedSearchTerm = tr("„%1“").arg(name);
            } else {
                m_formattedSearchTerm.append(tr(" oder „%1“").arg(name));
            }
        }

        if (names.isEmpty()) {
            // Parsing failed or the array only contained empty strings
            m_formattedSearchTerm = tr("<i>ungültige „oder“-Definition</i>");
            m_searchHidden.fill(true, m_rowCount);
            return;
        }

        // Match all rows with the names
        for (const auto &data : std::as_const(m_playersData)) {
            bool matches = false;
            for (const auto &name : names) {
                if (data.name == name) {
                    matches = true;
                    break;
                }
            }
            m_searchHidden.append(! matches);
        }

    } else {
        // We query the search engine
        m_formattedSearchTerm = tr("„%1“").arg(m_searchTerm.simplified());
        m_searchEngine->setSearchTerm(m_searchTerm);
        for (const auto &data : std::as_const(m_playersData)) {
            m_searchHidden.append(! m_searchEngine->checkMatch(data.name,
                                                               SearchEngine::DefaultSearch,
                                                               m_phoneticSearch));
        }
    }
}

bool RegistrationsModel::isSearchHidden(int row) const
{
    Q_ASSERT(row < m_rowCount);
    return m_searchHidden.at(row);
}

bool RegistrationsModel::isMarkerHidden(int row) const
{
    Q_ASSERT(row < m_rowCount);
    return ! m_markersModel->isVisible(m_playersData.at(row).marker);
}

const QString &RegistrationsModel::searchTerm() const
{
    return m_searchTerm;
}

const QString &RegistrationsModel::formattedSearchTerm() const
{
    return m_formattedSearchTerm;
}

const QVector<int> &RegistrationsModel::numbers() const
{
    return m_numbers;
}

const QString &RegistrationsModel::nameForBookingChecksum(const QString &checksum) const
{
    for (const auto &data : m_playersData) {
        if (data.bookingChecksum == checksum) {
            return data.name;
        }
    }
    return s_noName;
}

void RegistrationsModel::setDrawRound(int round)
{
    m_drawRound = round;
    // Actually refreshing the data set is triggerd via RegistrationPage::updatePlayersList()
    // which is called directly after setting the new draw round to refresh the draws view
}

QString RegistrationsModel::exactOrMatchString(const QVector<QString> &names) const
{
    QJsonArray jsonNames;
    for (const auto &name : names) {
        jsonNames.append(name);
    }
    return s_exactOr + Json::serialize(jsonNames);
}

const QHash<int, QVector<int>> &RegistrationsModel::currentDraw() const
{
    return m_currentDraw;
}

QVector<int> RegistrationsModel::allIds() const
{
    QVector<int> ids;
    for (const auto &data : m_playersData) {
        ids.append(data.id);
    }
    return ids;
}
