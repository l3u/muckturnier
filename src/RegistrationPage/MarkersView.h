// SPDX-FileCopyrightText: 2023-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef MARKERSVIEW_H
#define MARKERSVIEW_H

// Local includes
#include "shared/Markers.h"
#include "shared/TableView.h"

// Local classes
class MarkersModel;
class TitleMenu;

// Qt classes
class QMenu;
class QAction;

class MarkersView : public TableView
{
    Q_OBJECT

public:
    explicit MarkersView(QWidget *parent = nullptr);
    void setModel(QAbstractItemModel *model) override;
    void setDbIsChangeable(bool state);
    const Markers::Marker &selectedMarker() const;
    int selectedRow() const;
    void hideMarkerMenu();

Q_SIGNALS:
    void requestMoveMarker(int direction);
    void requestEditMarker();
    void requestDeleteMarker();
    void requestAddMarker();

private Q_SLOTS:
    void indexClicked(const QModelIndex &index);
    void indexDoubleClicked(const QModelIndex &index);
    void showContextMenu(const QPoint &position);

private: // Variables
    MarkersModel *m_model;

    bool m_dbIsChangeable = false;

    QMenu *m_setVisibilityMenu;
    QAction *m_showOnlyAction;
    QAction *m_showAllAction;

    TitleMenu *m_markersMenu;
    QAction *m_moveUpAction;
    QAction *m_moveDownAction;
    QAction *m_editMarkerAction;
    QAction *m_deleteMarkerAction;
    QAction *m_newMarkerAction;

    QMenu *m_newMarkerMenu;

    // This is not a reference but a deep copy, in case the model changes due to a network change
    Markers::Marker m_selectedMarker;
    int m_selectedRow = -1;

};

#endif // MARKERSVIEW_H
