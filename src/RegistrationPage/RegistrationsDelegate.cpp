// SPDX-FileCopyrightText: 2023-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "RegistrationsDelegate.h"
#include "PlayersEditor.h"

#include "shared/Logging.h"
#include "shared/RegistrationColumns.h"

// Qt includes
#include <QDebug>
#include <QPainter>
#include <QTimer>

RegistrationsDelegate::RegistrationsDelegate(QObject *parent) : TableDelegate(parent)
{
    setFirstVisibleColumn(RegistrationColumns::columnIndex.value(RegistrationColumns::Name));
}

QWidget *RegistrationsDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &,
                                             const QModelIndex &index) const
{
    auto *editor = new PlayersEditor(QString(), parent);
    Q_EMIT editorCreated(editor, index.data().toString());
    return editor;
}

void RegistrationsDelegate::updateEditorGeometry(QWidget *editor,
                                                 const QStyleOptionViewItem &option,
                                                 const QModelIndex &index) const
{
    int x = 2;
    int w = option.rect.width() - 2;
    if (index.data(Qt::DecorationRole).isValid()) {
        x += option.decorationSize.width();
        w -= option.decorationSize.width();
    }
    editor->setGeometry(x, option.rect.top() + 2,
                        w, option.rect.height() - 4);
}

void RegistrationsDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    qobject_cast<PlayersEditor *>(editor)->setText(index.data().toString());
}

void RegistrationsDelegate::setModelData(QWidget *editor, QAbstractItemModel *,
                                         const QModelIndex &index) const
{
    const auto *playersEditor = qobject_cast<PlayersEditor *>(editor);

    if (! playersEditor->enterPressed()) {
        qCDebug(MuckturnierLog) << "Enter was not pressed, discarding edit";
        return;
    }

    const auto oldName = index.data().toString();
    const auto newName = playersEditor->text().simplified();
    if (newName == oldName) {
        qCDebug(MuckturnierLog) << "Name was not changed, discarding edit";
        return;
    }

    // Call RegistrationPage::renamePlayers() after everything is done here. Otherwise, we get
    // a segfault if renamePlayers() shows a message and a network change happens meanwhile
    QTimer::singleShot(0, this, [this, oldName, newName]
    {
        Q_EMIT requestRenamePlayers(oldName, newName);
    });
}
