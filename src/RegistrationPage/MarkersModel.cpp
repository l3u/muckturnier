// SPDX-FileCopyrightText: 2023-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "Application.h"

#include "MarkersModel.h"

#include "Database/Database.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/StringTools.h"
#include "SharedObjects/ResourceFinder.h"

#include "shared/Logging.h"

// Qt includes
#include <QDebug>

MarkersModel::MarkersModel(QObject *parent, SharedObjects *sharedObjects)
    : QAbstractTableModel(parent),
      m_db(sharedObjects->database()),
      m_resourceFinder(sharedObjects->resourceFinder()),
      m_stringTools(sharedObjects->stringTools())
{
    initializeIcons();
    connect(qobject_cast<Application *>(qApp), &Application::paletteChanged,
            this, &MarkersModel::initializeIcons);
}

void MarkersModel::initializeIcons()
{
    m_unused = QIcon(m_resourceFinder->find(QStringLiteral("unused.png")));
    m_visible = QIcon(m_resourceFinder->find(QStringLiteral("visible.png")));
    m_hidden = QIcon(m_resourceFinder->find(QStringLiteral("hidden.png")));
}

int MarkersModel::rowCount(const QModelIndex &) const
{
    return m_rowCount;
}

int MarkersModel::columnCount(const QModelIndex &) const
{
    return m_columnCount;
}

QVariant MarkersModel::data(const QModelIndex &index, int role) const
{
    if (! index.isValid()) {
        return QVariant();
    }

    const auto row = index.row();
    const auto column = index.column();

    if (row > m_rowCount || column >= m_columnCount) {
        return QVariant();
    }

    const auto &marker = m_markers.at(row);

    if (column == 0 && role == Qt::DisplayRole) {
        return m_markersCounts.value(marker.id);

    } else if (column == 1) {
        if (role == Qt::DisplayRole) {
            return marker.name;
        } else if (role == Qt::ForegroundRole) {
            return marker.id != 0 ? marker.color : QVariant();
        }

    } else if (column == 2 && role == Qt::DisplayRole) {
        switch (marker.sorting) {
        case Markers::NewBlock:
            return tr("Neuer Block");
        case Markers::Merged:
            return tr("Fortlaufend");
        }

    } else if (column == 3 && role == Qt::DecorationRole) {
        if (! m_isVisible.value(marker.id)) {
            return m_hidden;
        } else {
            if (m_markersCounts.value(marker.id) == 0) {
                return m_unused;
            } else {
                return m_visible;
            }
        }
    }

    return QVariant();
}

QVariant MarkersModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole || orientation == Qt::Vertical) {
        return QVariant();
    }

    if (section == 0) {
        return tr("n", "Anzahl");
    } else if (section == 1) {
        return tr("Name");
    } else if (section == 2) {
        return tr("Sortierung");
    } else if (section == 3) {
        return QString();
    }

    // We can't reach here
    return QVariant();
}

void MarkersModel::clear()
{
    const auto count = rowCount();
    beginRemoveRows(QModelIndex(), 0, count - 1);

    m_markers.clear();

    endRemoveRows();
    Q_EMIT dataChanged(index(0, 0), index(count, m_columnCount - 1), { Qt::DisplayRole });
}

void MarkersModel::refresh()
{
    clear();

    const auto [ markers, success ] = m_db->markers();
    if (! success) {
        return;
    }

    m_markers = markers;
    m_rowCount = m_markers.count();
    const auto lastRow = m_rowCount - 1;
    beginInsertRows(QModelIndex(), 0, lastRow);

    // Cache the row for each id and setup the "unmarked" name and name pointer
    m_rowForId.clear();
    for (int row = 0; row < m_markers.count(); row++) {
        auto &marker = m_markers[row];
        m_rowForId[marker.id] = row;
        if (marker.id == 0) {
            marker.name = tr("(unmarkiert)");
            m_unmarkedName = &marker.name;
            break;
        }
    }

    // Set new markers visible
    QVector<int> ids;
    ids.reserve(m_rowCount);
    for (const auto &marker : m_markers) {
        ids.append(marker.id);
        if (! m_isVisible.contains(marker.id)) {
            m_isVisible[marker.id] = true;
        }
    }

    // Remove visibility entries for deleted markers
    for (const auto id : m_isVisible.keys()) {
        if (! ids.contains(id)) {
            m_isVisible.remove(id);
        }
    }

    endInsertRows();
    Q_EMIT dataChanged(index(0, 0), index(lastRow, m_columnCount - 1), { Qt::DisplayRole });
}

void MarkersModel::updateMarkersCounts(const QHash<int, int> &count)
{
    m_markersCounts = count;
    const auto lastRow = m_rowCount - 1;
    Q_EMIT dataChanged(index(0, 0), index(lastRow, 0), { Qt::DisplayRole });
    Q_EMIT dataChanged(index(0, 3), index(lastRow, 3), { Qt::DecorationRole });
}

void MarkersModel::toggleVisible(int row)
{
    const auto id = m_markers.at(row).id;
    m_isVisible[id] = ! m_isVisible[id];
    Q_EMIT dataChanged(index(row, 3), index(row, 3), { Qt::DecorationRole });
    Q_EMIT visibilityChanged();
}

void MarkersModel::showOnly(int row)
{
    Q_ASSERT(row < m_rowCount);
    const auto targetId = row == -1 ? -1 : m_markers.at(row).id;
    for (const auto id : m_isVisible.keys()) {
        m_isVisible[id] = id == targetId || targetId == -1;
    }
    Q_EMIT dataChanged(index(0, 3), index(m_rowCount - 1, 3), { Qt::DecorationRole });
    Q_EMIT visibilityChanged();
}

bool MarkersModel::isVisible(int id) const
{
    return m_isVisible.value(id);
}

bool MarkersModel::allVisible() const
{
    QHash<int, bool>::const_iterator it;
    for (it = m_isVisible.constBegin(); it != m_isVisible.constEnd(); it++) {
        if (! it.value()) {
            return false;
        }
    }
    return true;
}

const Markers::Marker &MarkersModel::data(int row) const
{
    Q_ASSERT(row < m_rowCount);
    return m_markers.at(row);
}

const Markers::Marker &MarkersModel::marker(int id) const
{
    if (! m_rowForId.contains(id)) {
        // This should not happen
        qCWarning(MuckturnierLog) << "MarkersModel::marker: Requested marker data for id" << id
                                  << "which could not be found!";
        // Return the "unmarked" marker, which is definitely always present
        return m_markers.at(m_rowForId.value(0));
    }
    return m_markers.at(m_rowForId.value(id));
}

const QVector<Markers::Marker> &MarkersModel::markers() const
{
    return m_markers;
}

const QString &MarkersModel::unmarkedName() const
{
    return *m_unmarkedName;
}

bool MarkersModel::isUnmarkedName(const QString &name) const
{
    return m_stringTools->toLower(name) == m_stringTools->toLower(*m_unmarkedName);
}

bool MarkersModel::isMarkerNamePresent(const QString &name, int skipId) const
{
    const auto lowerName = m_stringTools->toLower(name);
    for (const auto &marker : m_markers) {
        if (marker.id == skipId) {
            continue;
        }
        if (lowerName == m_stringTools->toLower(marker.name)) {
            return true;
        }
    }

    return false;
}
