// SPDX-FileCopyrightText: 2019-2020 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "PlayersEditor.h"

// Qt includes
#include <QDebug>
#include <QKeyEvent>

PlayersEditor::PlayersEditor(const QString &text, QWidget *parent) : QLineEdit(text, parent)
{
    setStyleSheet(QStringLiteral("QLineEdit { border:none; margin-top: 1px; margin-left: 1px; }"));
}

void PlayersEditor::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Return || event->key() == Qt::Key_Enter) {
        m_enterPressed = true;
    }
    QLineEdit::keyPressEvent(event);
}

bool PlayersEditor::enterPressed() const
{
    return m_enterPressed;
}
