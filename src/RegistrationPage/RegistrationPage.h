// SPDX-FileCopyrightText: 2010-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef REGISTRATIONPAGE_H
#define REGISTRATIONPAGE_H

// Local includes
#include "shared/RegistrationColumns.h"
#include "shared/Draw.h"
#include "shared/Players.h"

// Qt includes
#include <QWidget>
#include <QPointer>

// Local classes
class SharedObjects;
class StringTools;
class Database;
class TournamentSettings;
class SearchEngine;
class TitleMenu;
class SearchWidget;
class MarkersWidget;
class MarkerPopup;
class Settings;
class Server;
class Client;
class DrawPopup;
class DrawOverviewPage;
class PhoneticSearchButton;
class DismissableMessage;
class RegistrationsModel;
class RegistrationsDelegate;
class RegistrationsView;
class IconButton;
class NumberPopup;
class BookingEngine;
class ScanLineEdit;
class DrawModeMenu;
class ReadyToScanLabel;

// Qt classes
class QGroupBox;
class QLabel;
class QLineEdit;
class QPushButton;
class QAction;
class QMenu;
class QMessageBox;
class QCheckBox;
class QSpinBox;

namespace rc = RegistrationColumns;

class RegistrationPage : public QWidget
{
    Q_OBJECT

public:
    explicit RegistrationPage(QWidget *parent, SharedObjects *sharedObjects);
    void reload(bool updateMarkersWidget = true);
    void reset();
    bool bookedPresent() const;
    bool singlesPresent() const;
    void setDrawOverviewPage(DrawOverviewPage *drawOverviewPage);
    void setColumnsVisible(const QVector<rc::Column> &columns);
    void requestSetColumnVisible(rc::Column column, bool state);
    bool registerPhoneticSearchEnabled() const;
    bool drawAdjustmentNeededState() const;
    void askShowColumn(rc::Column column);
    void registerBooking(const QString &name);
    void showBooking(const QString &name);
    void setBookingChecksum(const QString &name, const QString &checksum);

    void serverStarted(Server *server);
    void serverStopped();
    void clientConnected(Client *client);
    void clientDisconnected();
    void updateChecksum();
    MarkersWidget *markersWidget() const;
    void processNetworkChange(int clientId = 0);
    void setProcessingRequest(bool processing, const QString &scrollToTarget = QString());
    void networkFinishRegistration(int clientId = 0);

    const QVector<Players::Data> &playersData() const;
    const QString &checksum() const;

Q_SIGNALS:
    void statusUpdate(const QString &message);
    void playersCountChanged(int count);
    void registrationsAvailable(bool state);
    void nameEdited();
    void networkTournamentStarted(bool state);
    void drawChanged();
    void requestShowDrawOverviewPage(int round);
    void requestShowAdjustDrawDialog();
    void drawAdjustmentNeeded(bool state) const;
    void listWasModified();
    void disqualificationChanged();
    void requestUpdateDrawOverviewPage(int changedRound, const QString &scrollTarget);
    void raiseMe();
    void playersNumberChanged();
    void requestShowBookingData(const QString &name);
    void requestSaveBookingPng();

public Q_SLOTS:
    void updatePlayersList(const QString &changedSelection = QString(),
                           int originalVisibleRow = -1);
    void scoreChanged();
    void finishRegistration();
    void resetSearch();
    void adoptDrawAdjustment(const QVector<QString> &names, const QVector<Draw::Seat> &seats);
    void requestRejected();
    void bookingCodeScanned(const QString &scannedText);
    void prepareDrawNextRound(const QVector<QString> &names, int round, bool raise);

private Q_SLOTS:
    void registrationEntered();
    void renamePlayers(const QString &originalName, const QString &name);
    void showContextMenu(QPoint point);
    void deletePlayers();
    void updateSearch(const QString &searchText);
    void markersChanged();
    void setMarker(int markerId);
    void setListMarker(int markerId);
    void removeListMarker(int markerId);
    void deleteMarkedPlayers(int markerId);
    void deleteAllPlayers();
    void updateVisibility();
    void setColumnVisible(rc::Column column);
    void popupRequested(rc::Column column, QPoint pos);
    void assignPlayer(const QString &name);
    void setDraw(const Draw::Seat &seat);
    void drawAllSeats();
    void deleteAllDraws();
    void processStatusLinkClick(const QString &anchor);
    void showDrawSettings();
    void adoptSearchText();
    void showHeaderContextMenu(const QPoint &point);
    void setHideHeader(bool state);
    void setDisqualified();
    void searchAsYouTypeToggled(bool state);
    void playersNameChanged(const QString &text);
    void registerPhoneticSearchToggled();
    void setNumber(int number);
    void drawRoundChanged(int round);
    void bookingEnabledChanged(bool state);

private: // Functions
    void registerPlayers(QString name, int marker);
    bool confirmListDeleteAction(const QString &title, const QString &text);
    void checkInformAboutBackup(const QString &title);
    void cacheChecksum();
    bool checkCachedChecksum(const QString &title);
    void hideContextMenus();
    void setListDraw(int round, const QVector<Draw::Seat> &draw);
    void itemError(const char *funcInfoText, const QString &text);
    void updateContextMenu();
    bool checkForMarkAsAboard();
    bool checkDrawIsIgnored();
    bool checkWarnDeleteDraw(const QString &title);
    void clearDrawRoundSpinBoxFocus();
    QString redSpan(const QString &text) const;
    QString formatNeededTables(int players, bool booked) const;

private: // Variables
    Database *m_db;
    Settings *m_settings;
    TournamentSettings *m_tournamentSettings;
    StringTools *m_stringTools;
    SearchEngine *m_searchEngine;
    DismissableMessage *m_dismissableMessage;
    BookingEngine *m_bookingEngine;

    QGroupBox *m_newPlayersBox;
    QLabel *m_nameLabel;
    ScanLineEdit *m_playersName;
    IconButton *m_autoCapitalize;
    PhoneticSearchButton *m_registerPhoneticSearch;
    QPushButton *m_registerButton;
    ReadyToScanLabel *m_readyToScanLabel;

    IconButton *m_searchAsYouType;
    QLabel *m_searchAsYouTypeResult;

    QGroupBox *m_playersBox;
    RegistrationsModel *m_registrationsModel;
    RegistrationsDelegate *m_registrationsDelegate;
    RegistrationsView *m_registrationsView;

    QWidget *m_drawRoundWidget;
    QSpinBox *m_drawRoundSpinBox;
    int m_drawRound = 1;

    QLabel *m_statusLabel;
    QLabel *m_isSearchLabel;
    QLabel *m_visibilityDeviation;
    int m_actuallyHiddenRow = -1;
    QHash<rc::Column, QAction *> m_showColumn;
    SearchWidget *m_searchWidget;

    MarkersWidget *m_markersWidget;
    NumberPopup *m_numberPopup;
    MarkerPopup *m_markerPopup;
    DrawPopup *m_drawPopup;
    DrawOverviewPage *m_drawOverviewPage = nullptr;
    QPointer<QMessageBox> m_currentMessageBox;

    bool m_drawAdjustmentNeeded = false;
    bool m_showHideHeaderHint = false;

    bool m_registeringBooking = false;

    // Context menus

    QPushButton *m_menuButton;
    TitleMenu *m_columnsMenu;
    QAction *m_hideHeaderAction;
    QAction *m_showDrawSettingsAction;
    DrawModeMenu *m_drawModeMenu;

    TitleMenu *m_contextMenu;

    QMenu *m_assignMenu;

    QAction *m_editAction;
    QAction *m_deleteAction;

    QAction *m_setNumberAction;
    QAction *m_deleteNumberAction;

    QAction *m_setOnlyMarkerAction;
    QMenu *m_setMarkerMenu;
    QAction *m_removeMarkerAction;

    QAction *m_setDrawAction;
    QAction *m_deleteDrawAction;
    QPoint m_lastContextMenuPos;

    QMenu *m_disqualificationMenu;
    QAction *m_disqualifyAction;
    QAction *m_unDisqualifyAction;

    TitleMenu *m_listMenu;

    QAction *m_listSetOnlyMarkerAction;
    QMenu *m_listSetMarkerMenu;
    QAction *m_listRemoveOnlyMarkerAction;
    QMenu *m_listRemoveMarkerMenu;
    QAction *m_listRemoveAllMarkersAction;

    QAction *m_deleteOnlyMarkedAction;
    QMenu *m_deleteMarkedMenu;
    QAction *m_deleteUnmarkedAction;
    QAction *m_deleteAllAction;

    QAction *m_deleteAllDrawsAction;

    // Network
    Server *m_server = nullptr;
    Client *m_client = nullptr;
    bool m_isServer = false;
    bool m_isClient = false;
    QString m_scrollToTarget;
    QString m_checksum;
    QString m_cachedChecksum;
    QString m_backupFile;
    bool m_bookedPresent = false;
    bool m_singlesPresent = false;
    bool m_focusPlayersName = false;
    bool m_requestingRegistration = false;

};

#endif // REGISTRATIONPAGE_H
