// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef DRAWNTABLESLAYOUT_H
#define DRAWNTABLESLAYOUT_H

// Qt includes
#include <QLayout>

class DrawnTablesLayout : public QLayout
{

public:
    explicit DrawnTablesLayout(QWidget *parent);
    ~DrawnTablesLayout();
    void addItem(QLayoutItem *item) override;
    QLayoutItem *takeAt(int index) override;
    QLayoutItem *itemAt(int index) const override;
    int count() const override;
    QSize sizeHint() const override;
    QSize minimumSize() const override;
    void setGeometry(const QRect &rect) override;
    void positionItems();

private: // Variables
    QList<QLayoutItem *> m_items;
    QSize m_cellSize;
    QSize m_fullSize;

};

#endif // DRAWNTABLESLAYOUT_H
