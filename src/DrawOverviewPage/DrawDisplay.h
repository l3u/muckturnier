// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef DRAWDISPLAY_H
#define DRAWDISPLAY_H

// Local includes
#include "shared/FullScreenDialog.h"

// Local classes
class DrawnTablesLayout;
class DrawnTableWidget;
class SizeFactorLabel;

// Qt classes
class QLabel;
class QMenu;
class QStackedLayout;

class DrawDisplay : public FullScreenDialog
{
    Q_OBJECT

public:
    explicit DrawDisplay(QWidget *parent, const QString &logo, int baseSize, const int *round,
                         const QVector<QString> *pairs1, const QVector<QString> *pairs2);
    void update();

Q_SIGNALS:
    void baseSizeChanged(int size);

private Q_SLOTS:
    void setBaseSize(int size);

private: // Variables
    int m_baseSize = -1;

    const int *m_round;
    const QVector<QString> *m_pairs1;
    const QVector<QString> *m_pairs2;

    QMenu *m_menu;
    QAction *m_hideLogo;

    SizeFactorLabel *m_header;
    SizeFactorLabel *m_noDraw;
    QStackedLayout *m_stackedLayout;
    DrawnTablesLayout *m_tablesLayout;
    QVector<DrawnTableWidget *> m_tableWidgets;
    QImage m_logo;
    QLabel *m_footer;

};

#endif // DRAWDISPLAY_H
