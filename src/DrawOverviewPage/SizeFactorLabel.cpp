// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "SizeFactorLabel.h"

// Qt includes
#include <QVBoxLayout>
#include <QLabel>
#include <QGridLayout>
#include <QFrame>
#include <QDebug>

SizeFactorLabel::SizeFactorLabel(int baseSize, double scaleFactor, QWidget *parent)
    : QLabel(parent),
      m_scaleFactor(scaleFactor)
{
    initializeBaseSize(baseSize);
}

SizeFactorLabel::SizeFactorLabel(const QString &text, int baseSize, double scaleFactor,
                                 QWidget *parent)
    : QLabel(text, parent),
      m_scaleFactor(scaleFactor)
{
    initializeBaseSize(baseSize);
}

void SizeFactorLabel::initializeBaseSize(int baseSize)
{
    m_baseSize = baseSize == -1 ? font().pointSize() : baseSize;
    setBaseSize(m_baseSize);
}

void SizeFactorLabel::setBaseSize(int size)
{
    m_baseSize = size;
    auto currentFont = font();
    currentFont.setPointSize(m_baseSize * m_scaleFactor);
    setFont(currentFont);
}
