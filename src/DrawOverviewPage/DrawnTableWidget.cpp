// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "DrawnTableWidget.h"
#include "SizeFactorLabel.h"

// Qt includes
#include <QVBoxLayout>
#include <QGridLayout>
#include <QFrame>

// C++ includes
#include <algorithm>

DrawnTableWidget::DrawnTableWidget(int table, int baseSize, QWidget *parent) : QWidget(parent)
{
    auto *wrapperLayout = new QVBoxLayout(this);
    m_wrapper = new QWidget;
    m_wrapper->setObjectName(QStringLiteral("wrapper"));
    wrapperLayout->addWidget(m_wrapper);

    m_layout = new QVBoxLayout(m_wrapper);

    m_table = new SizeFactorLabel(tr("<b>Tisch %1</b>").arg(table), baseSize, 1.1);
    m_table->setAlignment(Qt::AlignCenter);
    m_layout->addWidget(m_table);

    m_hr = new QWidget;
    m_hr->setObjectName(QStringLiteral("hr"));
    m_layout->addWidget(m_hr);

    auto *pairLayout = new QGridLayout;
    m_layout->addLayout(pairLayout);

    m_pair1Label = new SizeFactorLabel(tr("<b>Paar 1:</b>&nbsp;&nbsp;"));
    m_pair1Label->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    pairLayout->addWidget(m_pair1Label, 0, 0);

    m_pair1 = new SizeFactorLabel;
    pairLayout->addWidget(m_pair1, 0, 1);

    m_pair2Label = new SizeFactorLabel(tr("<b>Paar 2:</b>"));
    m_pair2Label->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    pairLayout->addWidget(m_pair2Label, 1, 0);

    m_pair2 = new SizeFactorLabel;
    pairLayout->addWidget(m_pair2, 1, 1);

    setBaseSize(baseSize);
}

void DrawnTableWidget::setPairs(const QString &pair1, const QString &pair2)
{
    m_pair1->setText(pair1.isEmpty() ? tr("–") : pair1);
    m_pair2->setText(pair2.isEmpty() ? tr("–") : pair2);
}

void DrawnTableWidget::setBaseSize(int size)
{
    m_table->setBaseSize(size);
    m_pair1Label->setBaseSize(size);
    m_pair1->setBaseSize(size);
    m_pair2Label->setBaseSize(size);
    m_pair2->setBaseSize(size);

    const auto margin = int(std::max(size / 1.6, 2.0));
    m_layout->setContentsMargins(margin, margin, margin, margin);

    const auto hrHeight = int(std::max(size / 7.0, 1.0));
    m_hr->setMinimumHeight(hrHeight);
    m_hr->setMaximumHeight(hrHeight);
}
