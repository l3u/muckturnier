// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef DRAWOVERVIEWPAGE_H
#define DRAWOVERVIEWPAGE_H

// Local includes
#include "shared/Players.h"
#include "shared/OptionalPage.h"
#include "shared/Draw.h"

// Qt includes
#include <QVector>
#include <QPointer>

// Local classes
class SharedObjects;
class Database;
class TournamentSettings;
class SearchWidget;
class SearchEngine;
class DrawModel;
class DrawView;
class NumbersComboBox;
class DrawDisplay;
class FullScreenMenu;
class DisplayButton;

// Qt classes
class QLabel;

class DrawOverviewPage : public OptionalPage
{
    Q_OBJECT

public:
    explicit DrawOverviewPage(QWidget *parent, SharedObjects *sharedObjects,
                              const QVector<Players::Data> *playersData, bool drawAdjustmentNeeded);

Q_SIGNALS:
    void requestShowAdjustDrawDialog();

public Q_SLOTS:
    void resetSearch();
    void drawAdjustmentNeeded(bool state);
    bool closePage();
    void updateDraw(int changedRound, const QString &scrollTarget);
    void setRound(int round);
    void closeDisplay();

private Q_SLOTS:
    void updateSearch(const QString &searchText);
    void showDrawDisplay();

private: // Variables
    Database *m_db;
    TournamentSettings *m_tournamentSettings;
    SearchEngine *m_searchEngine;

    int m_round = 1;
    QVector<int> m_drawnRounds;

    FullScreenMenu *m_fullScreenMenu;

    NumbersComboBox *m_roundSelect;
    QLabel *m_uncompletedTablesLabel;
    QLabel *m_noDraw;
    QLabel *m_isSearch;
    DrawModel *m_drawModel;
    DrawView *m_drawView;
    QLabel *m_adjustmentLabel;
    SearchWidget *m_searchWidget;

    int m_baseSize = -1;
    DisplayButton *m_displayButton;
    const QString m_displayLogo;
    QPointer<DrawDisplay> m_drawDisplay;

};

#endif // DRAWOVERVIEWPAGE_H
