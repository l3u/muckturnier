// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "DrawModel.h"

#include "shared/DrawMapper.h"
#include "shared/Logging.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/StringTools.h"

// Qt includes
#include <QDebug>
#include <QColor>

// C++ includes
#include <utility>

static const QColor s_red = QColor(Qt::red);

DrawModel::DrawModel(QObject *parent, const QVector<Players::Data> *playersData,
                     SharedObjects *sharedObjects)
    : QAbstractTableModel(parent),
      m_playersData(playersData),
      m_stringTools(sharedObjects->stringTools()),
      m_searchEngine(sharedObjects->searchEngine())
{
}

int DrawModel::rowCount(const QModelIndex &) const
{
    return m_rowCount;
}

int DrawModel::columnCount(const QModelIndex &) const
{
    return m_columnCount;
}

QVariant DrawModel::data(const QModelIndex &index, int role) const
{
    if (! index.isValid()) {
        return QVariant();
    }

    const auto row = index.row();
    const auto column = index.column();
    if (row >= m_rowCount || column >= m_columnCount) {
        return QVariant();
    }

    if (column == 0 && role == Qt::DisplayRole) {
        return row % 2 == 0 ? QString::number(row / 2 + 1) : QString();
    }

    const auto drawIndex = row / 2;
    const auto drawPresent = m_draw.at(drawIndex) != DrawMapper::emptyTable;

    if (column == 1) {
        if (role == Qt::DisplayRole) {
            if (row % 2 == 0) {
                return drawPresent ? tr("Paar 1:") : tr("Noch keine Paare zugelost");
            } else {
                return drawPresent ? tr("Paar 2:") : QString();
            }

        } else if (role == Qt::ForegroundRole) {
            return drawPresent ? QVariant() : s_red;
        }

    } else if (column == 2) {
        if (! drawPresent && role == Qt::DisplayRole) {
            return QString();
        }

        const auto &pairName = row % 2 == 0 ? m_displayPairs1.at(drawIndex)
                                            : m_displayPairs2.at(drawIndex);
        if (role == Qt::DisplayRole) {
            return pairName.isEmpty() ? tr("Noch kein Paar zugelost") : pairName;

        } else if (role == Qt::ForegroundRole) {
            if (pairName.isEmpty()
                || (! m_isPairMode && m_draw.at(drawIndex).at(row % 2 == 0 ? 1 : 3).isEmpty())) {

                return s_red;
            }
        }
    }

    return QVariant();
}

QVariant DrawModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole || orientation == Qt::Vertical) {
        return QVariant();
    }

    if (section == 0) {
        return tr("Tisch");
    } else if (section == 1) {
        return QString();
    } else if (section == 2) {
        return tr("Namen");
    }

    return QVariant();
}

void DrawModel::refresh(int round, bool isPairMode)
{
    beginRemoveRows(QModelIndex(), 0, m_rowCount - 1);
    m_displayPairs1.clear();
    m_searchPairs1.clear();
    m_displayPairs2.clear();
    m_searchPairs2.clear();
    m_numbers.clear();
    m_rowCount = 0;
    endRemoveRows();
    Q_EMIT dataChanged(index(0, 0), index(m_rowCount - 1, m_columnCount - 1), { Qt::DisplayRole });

    m_isPairMode = isPairMode;
    m_searchType = m_isPairMode ? SearchEngine::DefaultSearch
                                : SearchEngine::DrawnPairsDrawSearch;

    QHash<QString, int> dataIndex;
    for (int i = 0; i < m_playersData->count(); i++) {
        dataIndex[m_playersData->at(i).name] = i;
    }

    // Get the complete draw representation
    m_draw = DrawMapper::mapDraw(*m_playersData, round);

    // Generate the empty and incomplete tables lists
    m_emptyTables.clear();
    m_incompleteTables.clear();
    const int neededCount = m_isPairMode ? 2 : 4;
    for (int i = 0; i < m_draw.count(); i++) {
        int count = 0;
        for (const auto &name : std::as_const(m_draw.at(i))) {
            count += ! name.isEmpty();
        }
        if (count == 0) {
            m_emptyTables.append(i + 1);
        } else if (count != neededCount) {
            m_incompleteTables.append(i + 1);
        }
    }

    // Update our internal lists

    m_rowCount = m_draw.count() * 2;

    beginInsertRows(QModelIndex(), 0, m_rowCount - 1);

    if (! m_isPairMode) {
        m_searchEngine->clearSearchCache(SearchEngine::DrawnPairsDrawSearch);
    }

    for (const auto &table : m_draw) {
        m_numbers.append(QVector<int>());
        auto &currentNumbers = m_numbers.last();

        if (m_isPairMode) {
            const auto &pair1 = table.at(0);
            if (! pair1.isEmpty()) {
                const auto pair1Data = playersData(dataIndex, pair1);
                m_displayPairs1.append(m_stringTools->pairOrPlayerName(pair1Data,
                                                                       StringTools::FormattedName));
                m_searchPairs1.append(pair1);
                if (pair1Data.number > 0) {
                    currentNumbers.append(pair1Data.number);
                }
            } else {
                m_displayPairs1.append(QString());
                m_searchPairs1.append(QString());
            }

            const auto &pair2 = table.at(2);
            if (! pair2.isEmpty()) {
                const auto pair2Data = playersData(dataIndex, pair2);
                m_displayPairs2.append(m_stringTools->pairOrPlayerName(pair2Data,
                                                                       StringTools::FormattedName));
                m_searchPairs2.append(pair2);
                if (pair2Data.number > 0) {
                    currentNumbers.append(pair2Data.number);
                }
            } else {
                m_displayPairs2.append(QString());
                m_searchPairs2.append(QString());
            }

        } else {
            const auto &pair1Player1 = table.at(0);
            const auto &pair1Player2 = table.at(1);
            if (! pair1Player1.isEmpty()) {
                const auto player1Data = playersData(dataIndex, pair1Player1);
                const auto player2Data = playersData(dataIndex, pair1Player2);
                m_displayPairs1.append(m_stringTools->pairName(player1Data, player2Data,
                                                               StringTools::FormattedName));
                m_searchPairs1.append(pair1Player2.isEmpty()
                    ? pair1Player1
                    : m_stringTools->pairName(player1Data, player2Data, StringTools::BareName));
                if (player1Data.number > 0) {
                    currentNumbers.append(player1Data.number);
                }
                if (player2Data.number > 0) {
                    currentNumbers.append(player2Data.number);
                }
                m_searchEngine->updateSearchCache(m_searchPairs1.last(),
                                                  SearchEngine::DrawnPairsDrawSearch);
            } else {
                m_displayPairs1.append(QString());
                m_searchPairs1.append(QString());
            }

            const auto &pair2Player1 = table.at(2);
            const auto &pair2Player2 = table.at(3);
            if (! pair2Player1.isEmpty()) {
                const auto player1Data = playersData(dataIndex, pair2Player1);
                const auto player2Data = playersData(dataIndex, pair2Player2);
                m_displayPairs2.append(m_stringTools->pairName(player1Data, player2Data,
                                                               StringTools::FormattedName));
                m_searchPairs2.append(pair2Player2.isEmpty()
                    ? pair2Player1
                    : m_stringTools->pairName(player1Data, player2Data, StringTools::BareName));
                m_searchEngine->updateSearchCache(m_searchPairs2.last(),
                                                  SearchEngine::DrawnPairsDrawSearch);
                if (player1Data.number > 0) {
                    currentNumbers.append(player1Data.number);
                }
                if (player2Data.number > 0) {
                    currentNumbers.append(player2Data.number);
                }
            } else {
                m_displayPairs2.append(QString());
                m_searchPairs2.append(QString());
            }
        }
    }

    endInsertRows();

    m_round = round;

    Q_EMIT dataChanged(index(0, 0), index(m_rowCount - 1, m_columnCount - 1), { Qt::DisplayRole });
}

Players::DisplayData DrawModel::playersData(const QHash<QString, int> &dataIndex,
                                            const QString &name) const
{
    if (! name.isEmpty()) {
        const auto &dataSet = m_playersData->at(dataIndex.value(name));
        return {
            dataSet.name,             // DisplayData.name
            dataSet.number,           //            .number
            dataSet.disqualified > 0, //            .disqualified
            -1                        //            .id is not used here
        };

    } else {
        return {
            tr("(noch kein zweiter Spieler zugelost)"), // DisplayData.name
            0,                                          //            .number
            false,                                      //            .disqualified
            -1                                          //            .id is not used here
        };
    }
}

bool DrawModel::isTableEmpty(int row) const
{
    Q_ASSERT(row % 2 == 0);
    return m_draw.at(row / 2) == DrawMapper::emptyTable;
}

bool DrawModel::tableMatchesNumber(int row, int number) const
{
    Q_ASSERT(row % 2 == 0);
    return m_numbers.at(row / 2).contains(number);
}

bool DrawModel::tableMatchesSearchTerm(int row, bool phoneticSearch) const
{
    Q_ASSERT(row % 2 == 0);
    return    m_searchEngine->checkMatch(m_searchPairs1.at(row / 2), m_searchType, phoneticSearch)
           || m_searchEngine->checkMatch(m_searchPairs2.at(row / 2), m_searchType, phoneticSearch);
}

QModelIndex DrawModel::indexForName(const QString &name) const
{
    for (int i = 0; i < m_draw.count(); i++) {
        if (   m_searchPairs1.at(i) == name
            || m_searchPairs2.at(i) == name
            || m_draw.at(i).contains(name)) {

            return index(i * 2, 0);
        }
    }

    qCWarning(MuckturnierLog) << "DrawModel: Requested index for" << name
                              << "which could not be found";
    return QModelIndex();
}

const QVector<int> &DrawModel::incompleteTables() const
{
    return m_incompleteTables;
}

const QVector<int> &DrawModel::emptyTables() const
{
    return m_emptyTables;
}

const QVector<QString> *DrawModel::displayPairs1() const
{
    return &m_displayPairs1;
}

const QVector<QString> *DrawModel::displayPairs2() const
{
    return &m_displayPairs2;
}

const int *DrawModel::round() const
{
    return &m_round;
}
