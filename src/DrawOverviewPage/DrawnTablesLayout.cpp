// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "DrawnTablesLayout.h"

// Qt includes
#include <QDebug>
#include <QTimer>

// C++ includes
#include <utility>
#include <cmath>

DrawnTablesLayout::DrawnTablesLayout(QWidget *parent) : QLayout(parent)
{
}

DrawnTablesLayout::~DrawnTablesLayout()
{
    while (auto *item = takeAt(0)) {
        delete item;
    }
}

void DrawnTablesLayout::addItem(QLayoutItem *item)
{
    m_items.append(item);
}

QLayoutItem *DrawnTablesLayout::takeAt(int index)
{
    if (index >= 0 && index < m_items.size()) {
        return m_items.takeAt(index);
    } else {
        return nullptr;
    }
}

QLayoutItem *DrawnTablesLayout::itemAt(int index) const
{
    return m_items.value(index);
}

int DrawnTablesLayout::count() const
{
    return m_items.size();
}

QSize DrawnTablesLayout::sizeHint() const
{
    return m_fullSize;
}

QSize DrawnTablesLayout::minimumSize() const
{
    return QSize(m_cellSize.width(), m_fullSize.height());
}

void DrawnTablesLayout::setGeometry(const QRect &rect)
{
    QLayout::setGeometry(rect);
    QTimer::singleShot(0, this, &DrawnTablesLayout::positionItems);
}

void DrawnTablesLayout::positionItems()
{
    if (count() == 0) {
        return;
    }

    // Find the largest DrawnTableWidget
    m_cellSize.setWidth(0);
    m_cellSize.setHeight(0);
    for (auto *item : std::as_const(m_items)) {
        m_cellSize = m_cellSize.expandedTo(item->sizeHint());
    }

    // Calculate the best grid positioning

    const auto rect = geometry();

    auto cols = rect.width() / m_cellSize.width();
    if (cols > count()) {
        cols = count();
    }
    const int xStretch = (rect.width() - cols * m_cellSize.width()) / (cols + 1);

    const auto rows = std::ceil(double(count()) / cols);
    int yStretch = 0;
    if (rect.height() > rows * m_cellSize.height()) {
        yStretch = (rect.height() - rows * m_cellSize.height()) / (rows + 1);
    }

    int x = 0;
    int y = 0;
    int col = 1;
    int row = 1;
    for (auto *item : std::as_const(m_items)) {
        item->setGeometry(QRect(QPoint(x + col * xStretch, y + row * yStretch),
                                QSize(m_cellSize.width(), m_cellSize.height())));
        x += m_cellSize.width();
        col++;
        if (col > cols) {
            col = 1;
            row++;
            x = 0;
            y += m_cellSize.height();
        }
    }

    m_fullSize = QSize(m_cellSize.width() * cols, m_cellSize.height() * rows);
}
