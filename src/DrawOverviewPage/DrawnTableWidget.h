// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef DRAWNTABLEWIDGET_H
#define DRAWNTABLEWIDGET_H

// Qt includes
#include <QWidget>

// Local classes
class SizeFactorLabel;

// Qt classes
class QVBoxLayout;

class DrawnTableWidget : public QWidget
{
    Q_OBJECT

public:
    explicit DrawnTableWidget(int table, int baseSize, QWidget *parent = nullptr);
    void setPairs(const QString &pair1, const QString &pair2);
    void setBaseSize(int size);

private: // Variables
    QWidget *m_wrapper;
    QVBoxLayout *m_layout;

    SizeFactorLabel *m_table;
    QWidget *m_hr;
    SizeFactorLabel *m_pair1Label;
    SizeFactorLabel *m_pair1;
    SizeFactorLabel *m_pair2Label;
    SizeFactorLabel *m_pair2;

};

#endif // DRAWNTABLEWIDGET_H
