// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef DRAWVIEW_H
#define DRAWVIEW_H

// Local includes
#include "shared/TableView.h"

// Local classes
class DrawModel;

class DrawView : public TableView
{
    Q_OBJECT

public:
    explicit DrawView(QWidget *parent = nullptr);
    void setModel(QAbstractItemModel *model) override;
    void scrollToName(const QString &name);

protected Q_SLOTS:
    void dataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight,
                     const QVector<int> &roles = QVector<int>()) override;

private: // Variables
    DrawModel *m_model;

};

#endif // DRAWVIEW_H
