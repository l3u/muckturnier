// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "Application.h"

#include "DrawDisplay.h"
#include "DrawnTablesLayout.h"
#include "DrawnTableWidget.h"
#include "SizeFactorLabel.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QLabel>
#include <QScrollArea>
#include <QMenu>
#include <QHBoxLayout>
#include <QSpinBox>
#include <QWidgetAction>
#include <QFontMetrics>
#include <QStyle>
#include <QStackedLayout>

DrawDisplay::DrawDisplay(QWidget *parent, const QString &logo, int baseSize, const int *round,
                         const QVector<QString> *pairs1, const QVector<QString> *pairs2)
    : FullScreenDialog(parent),
      m_baseSize(baseSize),
      m_round(round),
      m_pairs1(pairs1),
      m_pairs2(pairs2)
{
    setWindowTitle(tr("Auslosungs-Display"));

    m_menu = new QMenu(this);

    auto *sizeWidget = new QWidget;
    auto *sizeWidgetLayout = new QHBoxLayout(sizeWidget);
    sizeWidgetLayout->setContentsMargins(
        style()->pixelMetric(QStyle::PM_LayoutHorizontalSpacing),
        style()->pixelMetric(QStyle::PM_LayoutVerticalSpacing) / 2,
        style()->pixelMetric(QStyle::PM_LayoutHorizontalSpacing) / 2,
        style()->pixelMetric(QStyle::PM_LayoutVerticalSpacing) / 2);
    sizeWidgetLayout->addWidget(new QLabel(tr("Schriftgröße:")));
    auto *sizeSpinBox = new QSpinBox;
    sizeSpinBox->setMinimum(6);
    sizeSpinBox->setMaximum(60);
    sizeSpinBox->setValue(m_baseSize);
    connect(sizeSpinBox, &QSpinBox::valueChanged, this, &DrawDisplay::setBaseSize);
    sizeWidgetLayout->addWidget(sizeSpinBox);
    auto *sizeAction = new QWidgetAction(this);
    sizeAction->setDefaultWidget(sizeWidget);
    m_menu->addAction(sizeAction);

    m_menu->addSeparator();

    m_menu->addAction(fullScreenAction());

    m_hideLogo = m_menu->addAction(tr("Logo ausblenden"));
    m_hideLogo->setCheckable(true);
    m_hideLogo->setChecked(false);
    connect(m_hideLogo, &QAction::toggled,
            this, [this]
            {
                setBaseSize(m_baseSize);
            });

    m_menu->addSeparator();

    auto *closeAction = m_menu->addAction(tr("Fenster schließen"));
    connect(closeAction, &QAction::triggered, this, &QDialog::accept);

    setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, &QWidget::customContextMenuRequested,
            this, [this]
            {
                m_menu->exec(QCursor::pos());
            });

    auto *layout = new QVBoxLayout(this);

    m_header = new SizeFactorLabel(m_baseSize, 2.0);
    m_header->setAlignment(Qt::AlignCenter);
    m_header->setObjectName(QStringLiteral("header"));
    layout->addWidget(m_header);

    m_stackedLayout = new QStackedLayout;
    layout->addLayout(m_stackedLayout);

    m_noDraw = new SizeFactorLabel(m_baseSize, 1.5);
    m_noDraw->setAlignment(Qt::AlignCenter);
    m_stackedLayout->addWidget(m_noDraw);

    auto *tablesWrapper = new QWidget;
    m_tablesLayout = new DrawnTablesLayout(tablesWrapper);

    auto *scroll = new QScrollArea;
    scroll->setFrameShape(QFrame::NoFrame);
    scroll->setWidgetResizable(true);
    scroll->setWidget(tablesWrapper);

    m_stackedLayout->addWidget(scroll);

    update();

    m_logo.load(logo);
    m_footer = new QLabel;
    m_footer->setObjectName(QStringLiteral("footer"));
    m_footer->setAlignment(Qt::AlignCenter);
    layout->addWidget(m_footer);

    connect(qobject_cast<Application *>(qApp), &Application::paletteChanged,
            this, [this]
            {
                setBaseSize(m_baseSize);
            });
    setBaseSize(m_baseSize);
}

void DrawDisplay::update()
{
    m_header->setText(tr("<b>Auslosung für Runde %1</b>").arg(*m_round));
    m_noDraw->setText(tr("<i>Es wurde noch keine Auslosung für Runde %1 eingegeben</i>").arg(
                         *m_round));

    const auto tables = m_pairs1->size();
    const auto difference = tables - m_tableWidgets.size();

    if (difference > 0) {
        const auto lastTable = m_tableWidgets.size();
        for (int i = 0; i < difference; i++) {
            auto *tableWidget = new DrawnTableWidget(lastTable + 1 + i, m_baseSize);
            m_tableWidgets.append(tableWidget);
            m_tablesLayout->addWidget(tableWidget);
        }
    } else if (difference < 0) {
        for (int i = 0; i > difference; i--) {
            auto *tableWidget = m_tableWidgets.takeLast();
            tableWidget->deleteLater();
        }
    }

    for (int i = 0; i < tables; i++) {
        m_tableWidgets[i]->setPairs(m_pairs1->at(i), m_pairs2->at(i));
    }

    m_stackedLayout->setCurrentIndex(tables != 0);
}

void DrawDisplay::setBaseSize(int size)
{
    m_baseSize = size;

    const auto fontMetrics = QFontMetrics(m_header->font());
    const auto headerHeight = fontMetrics.height();
    const auto padding = int(headerHeight / 8.0);

    QString css;
    if (! m_hideLogo->isChecked()) {
        css.append(QStringLiteral(
            "QLabel#header {"
                "color: white;"
                "background-color: #2f874c;"
                "padding: %1px;"
            "}"
            "QLabel#footer {"
                "background-color: #2f874c;"
                "padding: %1px;"
            "}").arg(padding));
        m_footer->setPixmap(QPixmap::fromImage(m_logo.scaled(QSize(headerHeight, headerHeight),
                                               Qt::KeepAspectRatioByExpanding,
                                               Qt::SmoothTransformation)));
        m_footer->show();
    } else {
        m_footer->hide();
    }

    setStyleSheet(css);

    const auto tableCss = QStringLiteral(
        "QWidget#wrapper {"
            "border: %1px solid %2;"
            "border-radius: %3px;"
            "background: palette(base);"
        "}"
        "QWidget#hr {"
            "background-color: %2"
        "}").arg(QString::number(int(std::max(size / 5.0, 1.0))),
                 qApp->property("DARK_MODE").toBool() ? QStringLiteral("palette(Light)")
                                                      : QStringLiteral("palette(Mid)"),
                 QString::number(int(size / 1.25)));

    m_header->setBaseSize(size);
    m_noDraw->setBaseSize(size);
    for (auto *tableWidget : m_tableWidgets) {
        tableWidget->setBaseSize(size);
        tableWidget->setStyleSheet(tableCss);
    }

    m_tablesLayout->positionItems();

    Q_EMIT baseSizeChanged(size);
}
