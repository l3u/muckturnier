// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "DrawOverviewPage.h"
#include "DrawModel.h"
#include "DrawView.h"
#include "DrawDisplay.h"

#include "Database/Database.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/SearchEngine.h"
#include "SharedObjects/TournamentSettings.h"
#include "SharedObjects/ResourceFinder.h"

#include "shared/SearchWidget.h"
#include "shared/NumbersComboBox.h"
#include "shared/FullScreenMenu.h"
#include "shared/DisplayButton.h"

// Qt includes
#include <QGroupBox>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QPushButton>
#include <QApplication>
#include <QStyle>
#include <QLabel>
#include <QDebug>
#include <QScreen>
#include <QMessageBox>

// C++ includes
#include <functional>

DrawOverviewPage::DrawOverviewPage(QWidget *parent, SharedObjects *sharedObjects,
                                   const QVector<Players::Data> *playersData,
                                   bool drawAdjustmentNeeded)
    : OptionalPage(parent),
      m_db(sharedObjects->database()),
      m_tournamentSettings(sharedObjects->tournamentSettings()),
      m_searchEngine(sharedObjects->searchEngine()),
      m_displayLogo(sharedObjects->resourceFinder()->find(QStringLiteral("display_logo.png")))
{
    // Top layout

    auto *topWidget = new QWidget;
    topWidget->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
    layout()->addWidget(topWidget);
    auto *topLayout = new QGridLayout(topWidget);
    topLayout->setContentsMargins(0, 0, 0, 0);

    // Round selection
    auto *roundLayout = new QHBoxLayout;
    topLayout->addLayout(roundLayout, 0, 0);
    roundLayout->addWidget(new QLabel(tr("<b>Auslosung für Runde</b>")));
    m_roundSelect = new NumbersComboBox;
    connect(m_roundSelect, &NumbersComboBox::currentNumberChanged,
            this, &DrawOverviewPage::setRound);
    roundLayout->addWidget(m_roundSelect);
    roundLayout->addStretch();

    // Close button
    auto *closeButton = new QPushButton(tr("Schließen"));
    closeButton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    closeButton->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogCloseButton));
    connect(closeButton, &QPushButton::clicked, this, &DrawOverviewPage::closePage);
    topLayout->addWidget(closeButton, 0, 1, Qt::AlignRight);

    // Status labels

    auto *labelsLayout = new QVBoxLayout;
    topLayout->addLayout(labelsLayout, 1, 0);

    m_uncompletedTablesLabel = new QLabel;
    m_uncompletedTablesLabel->setWordWrap(true);
    labelsLayout->addWidget(m_uncompletedTablesLabel);

    m_adjustmentLabel = new QLabel(tr("Alle %1 haben eine Auslosung, aber die Vergabe ist nicht "
                                      "fortlaufend. <a href=\"#adjustDraw\">Auslosung bereinigen"
                                      "</a>").arg(
                                      m_tournamentSettings->isPairMode() ? tr("Paare")
                                                                         : tr("Spieler")));
    m_adjustmentLabel->setWordWrap(true);
    m_adjustmentLabel->setTextInteractionFlags(Qt::TextBrowserInteraction);
    connect(m_adjustmentLabel, &QLabel::linkActivated,
            this, &DrawOverviewPage::requestShowAdjustDrawDialog);
    labelsLayout->addWidget(m_adjustmentLabel);
    m_adjustmentLabel->setVisible(drawAdjustmentNeeded);

    // Draw display

    auto *displayMenu = new QMenu(this);

    m_fullScreenMenu = new FullScreenMenu(this);
    displayMenu->addMenu(m_fullScreenMenu);

    auto *showDisplayHereAction = displayMenu->addAction(tr("Display hier anzeigen"));
    connect(showDisplayHereAction, &QAction::triggered,
            this, [this]
            {
                m_drawDisplay->initializeGeometry(screen());
            });

    displayMenu->addSeparator();

    auto *closeDisplayAction = displayMenu->addAction(tr("Display schließen"));
    connect(closeDisplayAction, &QAction::triggered, this, &DrawOverviewPage::closeDisplay);

    m_displayButton = new DisplayButton(tr("Display"), displayMenu);
    connect(m_displayButton, &DisplayButton::displayRequested,
            this, &DrawOverviewPage::showDrawDisplay);
    topLayout->addWidget(m_displayButton, 1, 1, Qt::AlignTop);

    // Draw overview

    auto *box = new QGroupBox;
    auto *boxLayout = new QVBoxLayout(box);
    layout()->addWidget(box);

    m_noDraw = new QLabel;
    m_noDraw->setAlignment(Qt::AlignCenter);
    m_noDraw->setWordWrap(true);
    boxLayout->addWidget(m_noDraw);

    m_isSearch = new QLabel;
    m_isSearch->hide();
    boxLayout->addWidget(m_isSearch);

    m_drawModel = new DrawModel(this, playersData, sharedObjects);
    m_drawView = new DrawView;
    m_drawView->setModel(m_drawModel);
    boxLayout->addWidget(m_drawView);

    // Search widget

    m_searchWidget = new SearchWidget(QStringLiteral("draw_page_search"), sharedObjects);
    connect(m_searchWidget, &SearchWidget::searchChanged, this, &DrawOverviewPage::updateSearch);
    boxLayout->addWidget(m_searchWidget);

    // This forces an update of the round select after construction if no draw is set yet
    m_drawnRounds.append(0);

    m_baseSize = font().pointSize();
}

bool DrawOverviewPage::closePage()
{
    if (m_drawDisplay != nullptr
        && QMessageBox::warning(this, tr("Seite schließen"),
               tr("<p>Es wird momentan das Auslosungs-Display angezeigt. Wenn diese Seite "
                  "geschlossen wird, dann wird auch das Display geschlossen.</p>"
                  "<p>Sollen wirklich das Display und diese Seite geschlossen werden?</p>"),
               QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Cancel)
           == QMessageBox::Cancel) {

        return false;
    }

    if (m_tournamentSettings->isSinglePlayerMode()) {
        m_searchEngine->clearSearchCache(SearchEngine::DrawnPairsDrawSearch);
    }

    return OptionalPage::closePage();
}

void DrawOverviewPage::updateDraw(int changedRound, const QString &scrollTarget)
{
    // Update the round select combo box

    const auto [ drawnRounds, success ] = m_db->getDrawnRounds();
    if (! success) {
        return;
    }

    if (drawnRounds != m_drawnRounds) {
        m_drawnRounds = drawnRounds;

        m_roundSelect->blockSignals(true);
        m_roundSelect->clear();

        if (! m_drawnRounds.contains(1)) {
            m_roundSelect->addItem(QString::number(1), 1);
        }

        for (const auto round : m_drawnRounds) {
            m_roundSelect->addItem(QString::number(round), round);
        }

        m_roundSelect->blockSignals(false);

        if (! m_drawnRounds.contains(m_round)) {
            m_roundSelect->setCurrentNumber(1);
            setRound(1);
            return;
        }
    }

    if (changedRound != 0 && changedRound != m_round) {
        // A pure draw update in round we don't currently display was done. Nothing to do.
        return;
    }

    m_drawModel->refresh(m_round, m_tournamentSettings->isPairMode());
    if (m_drawDisplay != nullptr) {
        m_drawDisplay->update();
    }

    if (m_drawModel->rowCount() == 0) {
        m_noDraw->show();
        m_uncompletedTablesLabel->setText(QStringLiteral(" ")); // Act as a placeholder
        m_drawView->hide();
        m_searchWidget->clear();
        m_searchWidget->hide();
        m_adjustmentLabel->hide();
        return;
    } else {
        m_noDraw->hide();
        m_drawView->show();
        m_searchWidget->show();
    }

    QString text = QStringLiteral("<table>");

    const auto &emptyTables = m_drawModel->emptyTables();
    const int emptyTablesCount = emptyTables.count();
    if (emptyTablesCount > 0) {
        text.append(tr("<tr><td>Unbesetzte Tische:</td><td>"));
        for (int i = 0; i < emptyTablesCount; i++) {
            if (i > 0 && emptyTablesCount > 1) {
                if (i < emptyTablesCount - 1) {
                    text.append(tr(", "));
                } else {
                    text.append(tr(" und "));
                }
            }
            text.append(QString::number(emptyTables.at(i)));
        }
        text.append(QStringLiteral("</td></tr>"));
    }

    const auto &incompleteTables = m_drawModel->incompleteTables();
    const int incompleteTablesCount = incompleteTables.count();
    if (incompleteTablesCount > 0) {
        text.append(tr("<tr><td>Unvollständig besetzte Tische:</td><td>"));
        for (int i = 0; i < incompleteTablesCount; i++) {
            if (i > 0 && incompleteTablesCount > 1) {
                if (i < incompleteTablesCount - 1) {
                    text.append(tr(", "));
                } else {
                    text.append(tr(" und "));
                }
            }
            text.append(QString::number(incompleteTables.at(i)));
        }
        text.append(QStringLiteral("</td></tr>"));
    }

    if (emptyTablesCount == 0 && incompleteTablesCount == 0) {
        m_uncompletedTablesLabel->setText(tr("Die Auslosung ist vollständig und lückenlos."));
    } else {
        text.append(QStringLiteral("</table>"));
        m_uncompletedTablesLabel->setText(text);
    }

    if (! scrollTarget.isEmpty()) {
        m_drawView->scrollToName(scrollTarget);
    }
}

void DrawOverviewPage::updateSearch(const QString &searchText)
{
    if (searchText.isEmpty()) {
        for (int row = 0; row < m_drawModel->rowCount(); row++) {
            m_drawView->showRow(row);
        }
        m_drawView->resizeColumnsToContents();
        m_isSearch->hide();
        return;
    }

    int matches = 0;

    bool isNumber = false;
    const auto number = searchText.toInt(&isNumber);

    if (isNumber && number > 0) {
        // We do a pure number search and check for the players number only
        for (int row = 0; row < m_drawModel->rowCount(); row += 2) {
            if (m_drawModel->tableMatchesNumber(row, number)) {
                matches++; // This can actually only happen once
                m_drawView->showRow(row);
                m_drawView->showRow(row + 1);
            } else {
                m_drawView->hideRow(row);
                m_drawView->hideRow(row + 1);
            }
        }

    } else {
        // We query the search engine
        m_searchEngine->setSearchTerm(searchText);
        const auto phoneticSearch = m_searchWidget->phoneticSearch();

        for (int row = 0; row < m_drawModel->rowCount(); row += 2) {
            if (m_drawModel->tableMatchesSearchTerm(row, phoneticSearch)) {
                matches++;
                m_drawView->showRow(row);
                m_drawView->showRow(row + 1);
            } else {
                m_drawView->hideRow(row);
                m_drawView->hideRow(row + 1);
            }
        }
    }

    m_searchWidget->updateMatches(matches);
    m_drawView->resizeColumnsToContents();
    m_isSearch->setText(tr("Suchergebnis für „%1“").arg(searchText.simplified()));
    m_isSearch->show();
}

void DrawOverviewPage::resetSearch()
{
    m_searchWidget->clear();
}

void DrawOverviewPage::drawAdjustmentNeeded(bool state)
{
    m_adjustmentLabel->setVisible(state);
}

void DrawOverviewPage::setRound(int round)
{
    m_round = round;
    m_noDraw->setText(tr("<i>Es wurde noch keine Auslosung für Runde %1 eingegeben</i>").arg(
                         m_round));

    updateDraw(m_round, QString());

    m_roundSelect->blockSignals(true);
    if (! m_roundSelect->setCurrentNumber(round)) {
        m_round = 1;
        m_roundSelect->setCurrentNumber(1);
        updateDraw(m_round, QString());
    }
    m_roundSelect->blockSignals(false);
}

void DrawOverviewPage::showDrawDisplay()
{
    m_drawDisplay = new DrawDisplay(this, m_displayLogo, m_baseSize, m_drawModel->round(),
                                    m_drawModel->displayPairs1(), m_drawModel->displayPairs2());

    connect(m_drawDisplay, &QDialog::finished, m_displayButton, &DisplayButton::displayClosed);

    connect(m_drawDisplay, &DrawDisplay::baseSizeChanged,
            this, [this](int size)
            {
                m_baseSize = size;
            });

    connect(m_fullScreenMenu, &FullScreenMenu::fullScreenRequested,
            m_drawDisplay, &FullScreenDialog::showFullScreen);

    m_drawDisplay->show();
}

void DrawOverviewPage::closeDisplay()
{
    if (m_drawDisplay != nullptr) {
        m_drawDisplay->close();
        // Make sure m_drawDisplay will be reset to nullptr. If we omit this whilst closing the
        // whole program, m_drawDisplay will not be deleted yet and DrawOverviewPage::closePage()
        // will ask if we're sure we want to close the page.
        QApplication::processEvents();
    }
}
