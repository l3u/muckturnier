// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SIZEFACTORLABEL_H
#define SIZEFACTORLABEL_H

// Qt includes
#include <QLabel>

class SizeFactorLabel : public QLabel
{
    Q_OBJECT

public:
    explicit SizeFactorLabel(int baseSize = -1, double scaleFactor = 1.0,
                             QWidget *parent = nullptr);
    explicit SizeFactorLabel(const QString &text, int baseSize = -1,
                             double scaleFactor = 1.0, QWidget *parent = nullptr);

public Q_SLOTS:
    void setBaseSize(int size);

private: // Functions
    void initializeBaseSize(int size);

private: // Variables
    const double m_scaleFactor = 1.0;
    int m_baseSize = -1;

};

#endif // SIZEFACTORLABEL_H
