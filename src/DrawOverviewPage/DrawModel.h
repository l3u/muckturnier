// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef DRAWMODEL_H
#define DRAWMODEL_H

// Local includes

#include "shared/Players.h"

#include "SharedObjects/SearchEngine.h"

// Qt includes
#include <QAbstractTableModel>

// Local classes
class SharedObjects;
class StringTools;

class DrawModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit DrawModel(QObject *parent, const QVector<Players::Data> *playersData,
                       SharedObjects *sharedObjects);
    int rowCount(const QModelIndex & = QModelIndex()) const override;
    int columnCount(const QModelIndex & = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;

    void refresh(int round, bool isPairMode);
    bool isTableEmpty(int row) const;
    bool tableMatchesNumber(int row, int number) const;
    bool tableMatchesSearchTerm(int row, bool phoneticSearch) const;
    QModelIndex indexForName(const QString &name) const;
    const QVector<int> &incompleteTables() const;
    const QVector<int> &emptyTables() const;

    const QVector<QString> *displayPairs1() const;
    const QVector<QString> *displayPairs2() const;
    const int *round() const;

private: // Functions
    Players::DisplayData playersData(const QHash<QString, int> &dataIndex,
                                     const QString &name) const;

private: // Variables
    const int m_columnCount = 3;
    int m_rowCount = 0;
    int m_round = 0;

    const QVector<Players::Data> *m_playersData;
    StringTools *m_stringTools;
    SearchEngine *m_searchEngine;

    bool m_isPairMode = true;
    SearchEngine::SearchType m_searchType = SearchEngine::DefaultSearch;
    QVector<QVector<QString>> m_draw;

    QVector<QString> m_displayPairs1;
    QVector<QString> m_displayPairs2;

    QVector<QString> m_searchPairs1;
    QVector<QString> m_searchPairs2;
    QVector<QVector<int>> m_numbers;

    QVector<int> m_incompleteTables;
    QVector<int> m_emptyTables;

};

#endif // DRAWMODEL_H
