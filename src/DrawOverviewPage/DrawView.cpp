// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "DrawView.h"
#include "DrawModel.h"

#include "shared/TableDelegate.h"

// Qt includes
#include <QDebug>
#include <QTimer>

DrawView::DrawView(QWidget *parent) : TableView(parent, TableDelegate::AlternatingNRows)
{
}

void DrawView::setModel(QAbstractItemModel *model)
{
    m_model = qobject_cast<DrawModel *>(model);
    QTableView::setModel(model);
}

void DrawView::dataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight,
                           const QVector<int> &roles)
{
    // First clear all spans
    clearSpans();

    // Pass the update to the base class
    QTableView::dataChanged(topLeft, bottomRight, roles);

    // Update the spans
    for (int row = 0; row < m_model->rowCount(); row += 2) {
        // Set the table number spans
        setSpan(row, 0, 2, 1);

        // Set the "no pairs assigned" spans
        if (m_model->isTableEmpty(row)) {
            setSpan(row, 1, 2, 2);
        }
    }

    // Resize the columns
    QTimer::singleShot(0, this, &QTableView::resizeColumnsToContents);
}

void DrawView::scrollToName(const QString &name)
{
    const auto index = m_model->indexForName(name);
    if (! index.isValid()) {
        return;
    }

    const auto row = index.row();
    if (row == 0) {
        scrollToTop();
    } else if (row == m_model->rowCount() - 2) {
        scrollToBottom();
    } else {
        QTableView::scrollTo(index, QAbstractItemView::PositionAtCenter);
    }
}
