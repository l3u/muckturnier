// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef BOOKINGENGINE_H
#define BOOKINGENGINE_H

// Local includes
#include "shared/Booking.h"

// Qt includes
#include <QObject>
#include <QString>

// Local classes
class TournamentSettings;

class BookingEngine : public QObject
{
    Q_OBJECT

public:
    static constexpr int protocolVersion = 2;
    static constexpr int maxEscapedLength = 1295; // zz = 1295 in base 36

    struct QrData
    {
        bool isValid = false;
        QString name;
        QString tournament;
        QString checksum;
        QString error;
    };

    explicit BookingEngine(QObject *parent, TournamentSettings *tournamentSettings);
    void reload();
    const QString &tournamentName() const;
    const QString &securityKey() const;
    QString checksum(const QString &name, const QString &tournament) const;
    QByteArray serialize(const QString &name, const QString &checksum) const;
    QrData parse(const QString &data) const;
    void reset();
    QString escape(QString string) const;
    bool looksLikeBookingCode(const QString &string) const;
    bool containsBookingCodeHeader(const QString &string) const;

private: // Functions
    QString unEscape(QString string) const;
    QByteArray encodeInt(int value) const;
    int decodeInt(const QString &value) const;
    QByteArray encodeChecksum(const QString &checksum) const;
    QString decodeChecksum(const QString &text) const;
    QString swapReplacementChars(QString text) const;

private: // Variables
    TournamentSettings *m_tournamentSettings;

    Booking::Settings m_settings;
    QString m_escapedTournamentName;

};

#endif // BOOKINGENGINE_H
