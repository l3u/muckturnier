// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef QRCODE_H
#define QRCODE_H

// Qt includes
#include <QColor>
#include <QImage>

// Qt classes
class QByteArray;

namespace QrCode
{

constexpr auto border = 4;
constexpr auto black = qRgb(  0,   0,   0);
constexpr auto white = qRgb(255, 255, 255);

QImage generate(const QByteArray &data);

}

#endif // QRCODE_H
