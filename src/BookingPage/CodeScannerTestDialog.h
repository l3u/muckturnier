// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef CODESCANNERTESTDIALOG_H
#define CODESCANNERTESTDIALOG_H

// Local includes
#include "shared/TitleDialog.h"

// Qt includes
#include <QImage>

// Local classes
class ScanLineEdit;

// Qt classes
class QLabel;

class CodeScannerTestDialog : public TitleDialog
{
    Q_OBJECT

public:
    explicit CodeScannerTestDialog(QWidget *parent);

private Q_SLOTS:
    void displayCode(int scaleFactor);
    void checkScan();

private: // Variables
    const QImage m_code;
    QLabel *m_codeLabel;
    ScanLineEdit *m_scan;

};

#endif // CODESCANNERTESTDIALOG_H
