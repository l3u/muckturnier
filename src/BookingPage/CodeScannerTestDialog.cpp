// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "CodeScannerTestDialog.h"
#include "QrCode.h"
#include "DisplayDataHelper.h"

#include "shared/ScanLineEdit.h"
#include "shared/ReadyToScanLabel.h"
#include "shared/SharedStyles.h"

// Qt includes
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPixmap>
#include <QSpinBox>
#include <QMessageBox>
#include <QFrame>

// We don't use "`" and "^", as those could be interpreted as a combining character. Also, we leave
// out the backslash, as it could be interpreted as an escaping character. Since version 2 of the
// booking code serialization protocol, spaces aren't used anymore and thus also omitted here. All
// other printable ASCII chars are allowed and could possibly be used.
static const auto s_testData = QStringLiteral(
    "!\"#$%&'()*+,-./"
    "0123456789:;<=>?"
    "@ABCDEFGHIJKLMNO"
    "PQRSTUVWXYZ[]_"
    "abcdefghijklmno"
    "pqrstuvwxyz{|}~"
);

CodeScannerTestDialog::CodeScannerTestDialog(QWidget *parent)
    : TitleDialog(parent),
      m_code(QrCode::generate(s_testData.toUtf8()))
{
    setTitle(tr("Code-Scanner testen"));
    addButtonBox(QDialogButtonBox::Close);

    auto *description = new QLabel(tr(
        "<p>Ein Barcode- bzw. QR-Code-Scanner verhält sich wie eine Tastatur. Es muss das richtige "
        "Tastaturlayout eingestellt sein (sonst werden Zeichen vertauscht bzw. falsch eingegeben), "
        "und der Scanner muss eine Eingabe mit „Return“ beenden (CR+LF).</p>"
        "<p>Der folgende QR-Code enthält alle Zeichen, die in einem Anmeldungscode vorkommen "
        "können. Es wird geprüft, ob der Scanner die Daten korrekt dekodiert und eingibt.</p>"
    ));
    description->setWordWrap(true);
    layout()->addWidget(description);

    auto *line = new QFrame;
    line->setFrameShape(QFrame::HLine);
    line->setProperty("isseparator", true);
    layout()->addWidget(line);

    auto *scaleLayout = new QHBoxLayout;
    layout()->addLayout(scaleLayout);

    scaleLayout->addWidget(new QLabel(tr("Skalierungsfaktor für den QR-Code:")));

    auto *scaleFactor = new QSpinBox;
    scaleFactor->setMinimum(1);
    scaleFactor->setMaximum(10);
    scaleLayout->addWidget(scaleFactor);
    connect(scaleFactor, &QSpinBox::valueChanged, this, &CodeScannerTestDialog::displayCode);

    auto *scanLayout = new QHBoxLayout;
    layout()->addLayout(scanLayout);

    m_codeLabel = new QLabel;
    m_codeLabel->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    m_codeLabel->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
    // Plasma 6's Breeze refuses to draw a frame here, cf. KDE Bug #488195
    // This is a workaround to make that specific style draw a frame anyway:
    m_codeLabel->setProperty("_breeze_force_frame", true);
    scanLayout->addWidget(m_codeLabel);
    scaleFactor->setValue(4); // This initializes the code

    scaleLayout->addStretch();

    auto *inputLayout = new QVBoxLayout;
    scanLayout->addLayout(inputLayout);

    inputLayout->addStretch();

    auto *scanLabel = new QLabel(tr("Bitte das Eingabefeld fokussieren und den angezeigten QR-Code "
                                    "scannen:"));
    scanLabel->setWordWrap(true);
    inputLayout->addWidget(scanLabel);

    m_scan = new ScanLineEdit;
    connect(m_scan, &ScanLineEdit::focusChanged, m_scan, &QLineEdit::clear);
    connect(m_scan, &QLineEdit::returnPressed, this, &CodeScannerTestDialog::checkScan);
    inputLayout->addWidget(m_scan);

    auto *readyLabel = new ReadyToScanLabel(m_scan);
    inputLayout->addWidget(readyLabel);

    inputLayout->addStretch();

    m_scan->setFocus();
}

void CodeScannerTestDialog::displayCode(int scaleFactor)
{
    const auto size = m_code.size().width() * scaleFactor;
    m_codeLabel->setMinimumHeight(size);
    m_codeLabel->setMinimumWidth(size);
    m_codeLabel->resize(QSize(size, size));
    m_codeLabel->setPixmap(QPixmap::fromImage(m_code.scaled(size, size)));
    resizeToContents();
}

void CodeScannerTestDialog::checkScan()
{
    const auto data = m_scan->text().trimmed();
    m_scan->clear();

    if (data == s_testData) {
        QMessageBox::information(this, tr("Code-Scanner testen"),
            tr("<p><span style=\"%1\">Der gescannte Datensatz ist korrekt!</span></p>"
               "<p>Der Scanner ist richtig konfiguriert!</p>").arg(SharedStyles::greenText));
    } else {
        QMessageBox::warning(this, tr("Code-Scanner testen"),
            tr("<p><span style=\"%1\">Der gescannte Datensatz ist <b>nicht</b> korrekt!</span></p>"
               "<p>War das Eingabefeld leer? Bitte nochmal scannen. Ab und zu schlägt das "
               "„Eintippen“ fehl.</p>"
               "<p>Sollte das Scannen reproduzierbar fehlschlagen, dann bitte die "
               "Konfiguration des Scanners überprüfen. Vermutlich ist das falsche Tastaturlayout "
               "eingestellt.</p>"
               "<hr/>"
               "<p>Der kodierte Datensatz ist:</p>"
               "<p><kbd>%2</kbd></p>"
               "<hr/>"
               "<p>Die gescannten Daten waren:</p>"
               "<p><kbd>%3</kbd></p>").arg(
               SharedStyles::redText,
               DisplayDataHelper::formatDataset(s_testData),
               DisplayDataHelper::formatDataset(data)));
    }
}
