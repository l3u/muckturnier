// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef DISPLAYDATAHELPER_H
#define DISPLAYDATAHELPER_H

// Qt classes
class QString;

namespace DisplayDataHelper
{

void appendLine(QString &out, const QString &line);
QString formatDataset(const QString &data);

}

#endif // DISPLAYDATAHELPER_H
