// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "QrCode.h"

#include "qrcodegen/qrcodegen.hpp"

namespace QrCode
{

QImage generate(const QByteArray &data)
{
    const auto qrCode = qrcodegen::QrCode::encodeText(data.constData(),
                                                      qrcodegen::QrCode::Ecc::MEDIUM);
    const auto size = qrCode.getSize();

    QImage image(size + QrCode::border * 2, size + QrCode::border * 2, QImage::Format_RGB32);
    image.fill(QrCode::white); // This is needed for the quiet zone around the code
    for (int y = 0; y < size; y++) {
        for (int x = 0; x < size; x++) {
            image.setPixel(x + QrCode::border, y + QrCode::border,
                           qrCode.getModule(x, y) ? QrCode::black : QrCode::white);
        }
    }

    return image;
}

}
