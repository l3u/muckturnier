// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "BookingPage.h"
#include "BookingEngine.h"
#include "QrCode.h"
#include "DisplayDataHelper.h"
#include "Logging.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/Settings.h"
#include "SharedObjects/TournamentSettings.h"
#include "SharedObjects/StringTools.h"

#include "shared/SharedStyles.h"
#include "shared/PhoneticSearchButton.h"
#include "shared/Booking.h"

#include "RegistrationPage/RegistrationPage.h"

#include "ScorePage/PlayersComboBox.h"

#include "Database/Database.h"

// Qt includes
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QApplication>
#include <QStyle>
#include <QGroupBox>
#include <QStackedLayout>
#include <QLabel>
#include <QDebug>
#include <QGridLayout>
#include <QLineEdit>
#include <QMessageBox>
#include <QJsonObject>
#include <QSpinBox>
#include <QFileDialog>
#include <QFileInfo>
#include <QTextStream>
#include <QRegularExpression>
#include <QStandardItem>
#include <QCheckBox>
#include <QMenu>
#include <QTimer>
#include <QClipboard>

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
#include <QTextCodec>
#endif

// C++ includes
#include <cmath>

static const QLatin1String s_newLine("\n");

BookingPage::BookingPage(QWidget *parent, SharedObjects *sharedObjects,
                         RegistrationPage *registrationPage)
    : OptionalPage(parent),
      m_settings(sharedObjects->settings()),
      m_tournamentSettings(sharedObjects->tournamentSettings()),
      m_stringTools(sharedObjects->stringTools()),
      m_bookingEngine(sharedObjects->bookingEngine()),
      m_registrationPage(registrationPage),
      m_playersData(&m_registrationPage->playersData()),
      m_db(sharedObjects->database())
{
    // Close button
    auto *topLayout = new QHBoxLayout;
    layout()->addLayout(topLayout);
    topLayout->addStretch();
    auto *closeButton = new QPushButton(tr("Schließen"));
    closeButton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    closeButton->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogCloseButton));
    connect(closeButton, &QPushButton::clicked, this, &OptionalPage::closePage);
    topLayout->addWidget(closeButton);

    m_layout = new QStackedLayout;
    layout()->addLayout(m_layout);

    // "No booked marker defined" part

    m_noBookedMarker = new QGroupBox;
    m_layout->addWidget(m_noBookedMarker);
    auto *noBookedMarkerLayout = new QVBoxLayout(m_noBookedMarker);
    auto *noBookedMarkerLabel = new QLabel(tr(
        "<p><i><b>Momentan ist keine Voranmeldung möglich!</b><br/>"
        "Voranmeldungen werden derzeit nicht berücksichtigt.</i></p>"
        "<p><i>Bitte auf der Anmeldungsseite die Box „Voranmeldungen berücksichtigen“"
        "<br/>"
        "aktivieren und eine passende Markierung für Voranmeldungen auswählen.</i></p>"));
    noBookedMarkerLabel->setWordWrap(true);
    noBookedMarkerLabel->setAlignment(Qt::AlignCenter);
    noBookedMarkerLayout->addWidget(noBookedMarkerLabel);

    // Booking widget part

    m_bookingWidget = new QWidget;
    auto *bookingLayout = new QVBoxLayout(m_bookingWidget);
    bookingLayout->setContentsMargins(0, 0, 0, 0);
    m_layout->addWidget(m_bookingWidget);

    // Settings box

    auto *headerLayout = new QHBoxLayout;
    bookingLayout->addLayout(headerLayout);

    m_bookingActive = new QLabel;
    headerLayout->addWidget(m_bookingActive);

    headerLayout->addStretch();

    m_showSettings = new QCheckBox(tr("Einstellungen anzeigen"));
    connect(m_showSettings, &QCheckBox::toggled,
            this, [this](bool state)
            {
                m_settingsBox->setVisible(state);
            });
    headerLayout->addWidget(m_showSettings);

    m_settingsBox = new QGroupBox(tr("Einstellungen"));
    m_settingsBox->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
    bookingLayout->addWidget(m_settingsBox);
    auto *settingsBoxLayout = new QVBoxLayout(m_settingsBox);

    m_settingsWidget = new QWidget;
    auto *settingsLayoutWrapper = new QVBoxLayout(m_settingsWidget);
    settingsLayoutWrapper->setContentsMargins(0, 0, 0, 0);
    settingsBoxLayout->addWidget(m_settingsWidget);
    connect(m_settingsBox, &QGroupBox::toggled, m_settingsWidget, &QWidget::setVisible);

    m_networkBlocked = new QLabel(tr("Im Netzwerkmodus können die Einstellungen für die "
                                     "Voranmeldung nicht geändert werden"));
    m_networkBlocked->setStyleSheet(SharedStyles::redText);
    m_networkBlocked->setAlignment(Qt::AlignCenter);
    m_networkBlocked->setWordWrap(true);
    m_networkBlocked->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
    settingsLayoutWrapper->addWidget(m_networkBlocked);

    auto *settingsLayout = new QGridLayout;
    settingsLayoutWrapper->addLayout(settingsLayout);

    settingsLayout->addWidget(new QLabel(tr("Turniername:")), 0, 0);
    m_tournamentName = new QLineEdit;
    m_tournamentName->setToolTip(tr(
        "<p>Bitte hier einen eindeutigen Namen für das Turnier eingeben. Am besten eine Jahreszahl "
        "mit angeben, damit es keine Verwechlungen mit dem letzten Mal gibt. Und nicht nur "
        "„Muckturnier“ nehmen, damit es keine Verwechlungen mit anderen Turnieren gibt. Also "
        "z.\u202FB. „Muckturnier.org Konradsreuth 2024“"));
    connect(m_tournamentName, &QLineEdit::textEdited, this, &BookingPage::checkSavePossible);
    settingsLayout->addWidget(m_tournamentName, 0, 1);

    m_tournamentNameTooLong = new QLabel(tr("Der Turniername ist zu lang"));
    m_tournamentNameTooLong->setStyleSheet(SharedStyles::redText);
    m_tournamentNameTooLong->hide();
    settingsLayout->addWidget(m_tournamentNameTooLong, 1, 1);

    settingsLayout->addWidget(new QLabel(tr("Sicherheitsschlüssel:")), 2, 0);
    m_securityKey = new QLineEdit;
    m_securityKey->setToolTip(tr(
        "<p>Diese Zeichenkette wird mit in die Prüfsumme einbezogen, ist aber nicht in den "
        "QR-Code-Daten enthalten. Das verhindert, dass jemand seinen eigenen QR-Code generiert und "
        "dann behauptet, er wäre ja vorangemeldet gewesen.</p>"
        "<p>Am besten einfach den zufällig generierten Schlüssel so lassen. Und nicht verraten ;-)"
        "</p>"));
    connect(m_securityKey, &QLineEdit::textEdited, this, &BookingPage::checkSavePossible);
    settingsLayout->addWidget(m_securityKey, 2, 1);

    m_notSavedYet = new QLabel(tr("Die Änderungen wurden noch noch nicht gespeichert!"));
    m_notSavedYet->setStyleSheet(SharedStyles::redText);
    m_notSavedYet->setAlignment(Qt::AlignRight);
    m_notSavedYet->setVisible(false);
    settingsLayoutWrapper->addWidget(m_notSavedYet);

    auto *saveLayout = new QHBoxLayout;
    settingsLayoutWrapper->addLayout(saveLayout);

    m_exportSettings = new QLabel(tr("<a href=\"#export\">Einstellungen exportieren</a>"));
    m_exportSettings->setTextInteractionFlags(Qt::TextBrowserInteraction);
    connect(m_exportSettings, &QLabel::linkActivated, this, &BookingPage::exportSettings);
    saveLayout->addWidget(m_exportSettings);

    saveLayout->addStretch();

    m_saveSettings = new QPushButton(tr("Speichern"));
    connect(m_saveSettings, &QPushButton::clicked, this, &BookingPage::saveSettings);
    saveLayout->addWidget(m_saveSettings);

    m_resetSettings = new QPushButton(tr("Zurücksetzen"));
    connect(m_resetSettings, &QPushButton::clicked, this, &BookingPage::resetSettings);
    saveLayout->addWidget(m_resetSettings);

    m_deleteSettings = new QPushButton(tr("Einstellungen löschen"));
    connect(m_deleteSettings, &QPushButton::clicked, this, &BookingPage::deleteSettings);
    saveLayout->addWidget(m_deleteSettings);

    // Booking box

    m_saveSettingsLabel = new QLabel(tr("<i>Für die Voranmeldung müssen erst die Einstellungen "
                                        "gespeichert werden</i>"));
    m_saveSettingsLabel->setAlignment(Qt::AlignCenter);
    bookingLayout->addWidget(m_saveSettingsLabel);

    m_bookingBox = new QGroupBox;
    m_bookingBox->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);
    m_bookingBox->setEnabled(false);
    bookingLayout->addWidget(m_bookingBox);

    auto *bookingBoxLayout = new QVBoxLayout(m_bookingBox);

    auto *registrationBox = new QGroupBox(tr("Neue Voranmeldungen erfassen"));
    auto *playersLayout = new QGridLayout(registrationBox);
    bookingBoxLayout->addWidget(registrationBox);

    playersLayout->addWidget(new QLabel(tr("Name(n):")), 0, 0);
    m_playersName = new QLineEdit;
    connect(m_playersName, &QLineEdit::returnPressed, this, &BookingPage::saveNewBooking);
    playersLayout->addWidget(m_playersName, 0, 1);

    m_registerButton = new QPushButton(tr("Anmelden"));
    connect(m_playersName, &QLineEdit::textChanged, this, &BookingPage::checkSaveBookingPossible);
    connect(m_registerButton, &QPushButton::clicked, this, &BookingPage::saveNewBooking);
    playersLayout->addWidget(m_registerButton, 0, 2);

    m_playersNameTooLong = new QLabel(tr("Der Name ist zu lang"));
    m_playersNameTooLong->setStyleSheet(SharedStyles::redText);
    m_playersNameTooLong->hide();
    playersLayout->addWidget(m_playersNameTooLong, 1, 1);

    // Data box

    auto *dataBox = new QGroupBox(tr("Anmeldungscodes"));
    bookingBoxLayout->addWidget(dataBox);
    auto *dataBoxWrapper = new QHBoxLayout(dataBox);

    auto *dataBoxLayout = new QGridLayout;
    dataBoxLayout->setContentsMargins(8, 0, 8, 0);
    dataBoxWrapper->addLayout(dataBoxLayout);

    QLabel *label;
    int row = -1;

    label = new QLabel(tr("Voranmeldung:"));
    label->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Preferred);
    dataBoxLayout->addWidget(label, ++row, 0);

    auto *comboLayout = new QHBoxLayout;
    dataBoxLayout->addLayout(comboLayout, row, 1);

    m_allBookings = new PlayersComboBox(sharedObjects->searchEngine(), 0, this);
    connect(m_allBookings, &PlayersComboBox::selectionChanged, this, &BookingPage::showBookingData);
    comboLayout->addWidget(m_allBookings);

    auto *phoneticSearch = new PhoneticSearchButton(QStringLiteral("booking_page"), sharedObjects);
    connect(phoneticSearch, &PhoneticSearchButton::toggled,
            m_allBookings, &PlayersComboBox::setPhoneticSearch);
    comboLayout->addWidget(phoneticSearch);

    m_dataNameTooLong = new QLabel(tr("Der Name ist zu lang, um einen Anmeldungscode zu erzeugen.\n"
                                      "Bitte auf der Anmeldungsseite ändern!"));
    m_dataNameTooLong->setWordWrap(true);
    m_dataNameTooLong->setStyleSheet(SharedStyles::redText);
    m_dataNameTooLong->hide();
    dataBoxLayout->addWidget(m_dataNameTooLong, ++row, 1);

    m_showOnRegistrationPage = new QLabel;
    m_showOnRegistrationPage->setTextInteractionFlags(Qt::TextBrowserInteraction);
    connect(m_showOnRegistrationPage, &QLabel::linkActivated,
            this, [this]
            {
                m_registrationPage->showBooking(m_allBookings->currentName());
                m_showOnRegistrationPage->clearFocus();
            });
    dataBoxLayout->addWidget(m_showOnRegistrationPage, ++row, 1);

    label = new QLabel(tr("Turniername:"));
    label->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Preferred);
    dataBoxLayout->addWidget(label, ++row, 0);
    m_dataTournament = new QLabel(tr("–"));
    m_dataTournament->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Preferred);
    m_dataTournament->setWordWrap(true);
    dataBoxLayout->addWidget(m_dataTournament, row, 1);

    label = new QLabel(tr("Prüfsumme:"));
    label->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Preferred);
    dataBoxLayout->addWidget(label, ++row, 0);
    m_dataChecksum = new QLabel(tr("–"));
    dataBoxLayout->addWidget(m_dataChecksum, row, 1);

    m_checksumStatus = new QLabel(tr("–"));
    m_checksumStatus->setWordWrap(true);
    dataBoxLayout->addWidget(m_checksumStatus, ++row, 1);

    auto *line = new QFrame;
    line->setFrameShape(QFrame::VLine);
    line->setProperty("isseparator", true);
    dataBoxWrapper->addWidget(line);

    auto *spacingWrapper = new QVBoxLayout;
    dataBoxWrapper->addLayout(spacingWrapper);

    auto *codeWidget = new QWidget;
    codeWidget->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    auto *codeWidgetWrapper = new QVBoxLayout(codeWidget);

    m_exportStatus = new QLabel(tr("–"));
    codeWidgetWrapper->addWidget(m_exportStatus);

    auto *codeWidgetLayout = new QGridLayout;
    codeWidgetWrapper->addLayout(codeWidgetLayout);

    m_bookingCodeMenu = new QMenu(this);
    auto *showData = m_bookingCodeMenu->addAction(tr("Datensatz anzeigen"));
    connect(showData, &QAction::triggered,
            this, [this]
            {
                QMessageBox::information(this, tr("Anmeldungscode-Datensatz"),
                    tr("<p>In dem Anmeldungscode ist folgender Datensatz enthalten (ohne die "
                       "Zeilenumbrüche):</p>"
                       "<kbd>%1</kbd>").arg(DisplayDataHelper::formatDataset(m_bookinCodeData)));
            });
    m_bookingCodeMenu->addSeparator();
    auto *saveCode = m_bookingCodeMenu->addAction(tr("Code exportieren"));
    connect(saveCode, &QAction::triggered, this, &BookingPage::exportBookingCode);

    m_bookingCodeDisplay = new QLabel;
    m_bookingCodeDisplay->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(m_bookingCodeDisplay, &QWidget::customContextMenuRequested,
            this, &BookingPage::showContextMenu);
    m_bookingCodeDisplay->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    m_bookingCodeDisplay->setMinimumSize(100, 100);
    m_bookingCodeDisplay->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
    // Plasma 6's Breeze refuses to draw a frame here, cf. KDE Bug #488195
    // This is a workaround to make that specific style draw a frame anyway:
    m_bookingCodeDisplay->setProperty("_breeze_force_frame", true);
    codeWidgetLayout->addWidget(m_bookingCodeDisplay, 0, 0, 2, 1);

    m_saveCodeButton = new QPushButton(tr("Code exportieren"));
    connect(m_saveCodeButton, &QPushButton::clicked, this, &BookingPage::exportBookingCode);
    m_saveCodeButton->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Ignored);
    codeWidgetLayout->addWidget(m_saveCodeButton, 0, 1, 1, 2);

    codeWidgetLayout->addWidget(new QLabel(tr("Skalierungsfaktor:")), 1, 1);

    m_scaleFactor = new QSpinBox;
    m_scaleFactor->setMinimum(1);
    m_scaleFactor->setMaximum(20);
    m_scaleFactor->setValue(m_settings->bookingCodesScaleFactor());
    codeWidgetLayout->addWidget(m_scaleFactor, 1, 2);

    spacingWrapper->addWidget(codeWidget);
    spacingWrapper->addStretch();

    bookingBoxLayout->addStretch();

    m_bookingsCount = new QLabel;
    bookingLayout->addWidget(m_bookingsCount);

    reload();
    bookedMarkerChange();

    QTimer::singleShot(0, m_playersName, QOverload<>::of(&QLineEdit::setFocus));
}

void BookingPage::reload()
{
    bookedMarkerChange();

    const auto bookingEnabled = m_tournamentSettings->bookingEnabled();

    m_bookingActive->setText(bookingEnabled ? tr("<b>Voranmeldungsmodus aktiv</b>")
                                            : tr("<b>Voranmeldungsmodus nicht aktiv</b>"));

    m_notSavedYet->hide();

    m_tournamentName->setText(m_bookingEngine->tournamentName());
    m_securityKey->setText(m_bookingEngine->securityKey());

    m_showSettings->setVisible(bookingEnabled);
    m_showSettings->setChecked(false);

    m_settingsBox->setVisible(! bookingEnabled);
    m_exportSettings->setVisible(bookingEnabled);
    m_saveSettings->setEnabled(false);
    m_resetSettings->setEnabled(false);
    m_deleteSettings->setVisible(bookingEnabled);

    m_saveSettingsLabel->setVisible(! bookingEnabled);
    m_bookingBox->setEnabled(bookingEnabled);

    m_playersName->clear();
    m_registerButton->setEnabled(false);
}

void BookingPage::bookedMarkerChange()
{
    m_layout->setCurrentWidget(m_tournamentSettings->specialMarker(Markers::Booked) == -1
                                   ? m_noBookedMarker : m_bookingWidget);
}

void BookingPage::checkSavePossible()
{
    m_resetSettings->setEnabled(true);
    m_notSavedYet->show();

    const auto tournamentName = m_tournamentName->text().simplified();
    const auto tournamentNameCount = m_bookingEngine->escape(tournamentName).size();
    const auto possible =    ! tournamentName.isEmpty()
                          && ! m_securityKey->text().simplified().isEmpty()
                          && tournamentNameCount <= BookingEngine::maxEscapedLength;
    m_saveSettings->setEnabled(possible);
    m_tournamentNameTooLong->setVisible(tournamentNameCount > BookingEngine::maxEscapedLength);
}

void BookingPage::saveSettings()
{
    const auto bookingWasEnabled = m_tournamentSettings->bookingEnabled();
    const auto tournamentName = m_tournamentName->text().simplified();
    const auto securityKey = m_securityKey->text().simplified();

    if (   tournamentName == m_bookingEngine->tournamentName()
        && securityKey == m_bookingEngine->securityKey()) {

        QMessageBox::information(this, tr("Voranmeldung"),
            tr("Die Einstellungen für die Voranmeldung wurden nicht verändert!"));
        resetSettings();
        return;
    }

    const auto [ bookingCodesExported, success ] = m_db->bookingCodesExported();
    if (! success) {
        return;
    }

    if (bookingWasEnabled && bookingCodesExported) {
        if (   tournamentName != m_bookingEngine->tournamentName()
            && securityKey == m_bookingEngine->securityKey()) {

            if (QMessageBox::warning(this, tr("Voranmeldung"),
                    tr("<p><b>Einstellungen für die Voranmeldung speichern</b></p>"
                       "<p>Wenn der Turniername geändert wird, dann ändert sich die Prüfsumme für "
                       "alle Anmeldungen, für die bereits ein Anmeldungscode erstellt wurde.</p>"
                       "<p>Es wird hier eine entsprechender Hinweis angezeigt, aber <b>die Codes "
                       "sind weiterhin gültig.</b></p>"
                       "<p>Sollen die Einstellungen wirklich geändert werden?</p>"),
                    QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Cancel)
                != QMessageBox::Yes) {

                resetSettings();
                return;
            }

        } else {
            if (QMessageBox::warning(this, tr("Voranmeldung"),
                    tr("<p><b>Einstellungen für die Voranmeldung speichern</b></p>"
                       "<p>Wenn der Sicherheitsschlüssel geändert wird, dann <b>werden alle bisher "
                       "erstellten Anmeldungscodes ungültig!</b></p>"
                       "<p>Sollen die Einstellungen wirklich geändert werden?</p>"),
                    QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Cancel)
                != QMessageBox::Yes) {

                resetSettings();
                return;
            }
        }
    }

    m_tournamentName->setText(tournamentName);
    m_securityKey->setText(securityKey);

    m_tournamentSettings->setBookingSettings(Booking::Settings {
        tournamentName,
        securityKey
    });
    m_tournamentSettings->setBookingEnabled(true);
    m_bookingEngine->reload();

    reload();
    showBookingData();

    if (! bookingWasEnabled) {
        QMessageBox::information(this, tr("Voranmeldung"),
            tr("<p><b>Voranmeldungseinstellungen gespeichert!</b></p>"
               "<p>Der Voranmeldungsmodus ist jetzt aktiv.</p>"
               "<p>Die Markierung, die für abwesende (also vorangemeldete) Anmeldungen benutzt "
               "wird, kann im Voranmeldungsmodus nicht geändert werden.</p>"
               "<p>Über „Einstellungen löschen“ kann der Voranmeldungsmodus deaktiviert werden."
               "</p>"));
    }

    if (QMessageBox::question(this, tr("Einstellungen sichern"),
            tr("<p><b>Einstellungen sichern</b></p>"
               "<p>Wenn die Einstellungen oder die Datenbank versehentlich gelöscht werden, kann "
               "kein bereits erstellter Anmeldungscode mehr als „gültig“ validiert werden.</p>"
               "<p>Sollen die Einstellungen sicherheitshalber in eine UTF-8-kodierte Textdatei "
               "gesichert werden (später ist das auch noch über „Einstellungen exportieren“ oder "
               "manuell möglich)?</p>"),
            QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes)
        == QMessageBox::Yes) {

        exportSettings();
    }
}

void BookingPage::checkSaveBookingPossible(const QString &text)
{
    const auto escapedName = m_bookingEngine->escape(text.simplified());
    const auto count = escapedName.size();
    m_registerButton->setEnabled(count > 0 && count <= BookingEngine::maxEscapedLength);
    m_playersNameTooLong->setVisible(count > BookingEngine::maxEscapedLength);
}

void BookingPage::saveNewBooking()
{
    if (! m_registerButton->isEnabled()) {
        return;
    }

    m_registrationPage->registerBooking(m_playersName->text().simplified());
    m_playersName->setFocus();
}

void BookingPage::updateBookings()
{
    const auto currentName = m_allBookings->currentText();
    const auto lowerName = m_stringTools->toLower(m_playersName->text().simplified());

    m_allBookings->blockSignals(true);

    m_allBookings->clear();
    m_bookingChecksums.clear();

    const auto bookedMarker = m_tournamentSettings->specialMarker(Markers::Booked);
    int index = -1;
    int row = 0;

    for (const auto &data : *m_playersData) {
        if (data.marker != bookedMarker) {
            continue;
        }

        if (index == -1 && m_stringTools->toLower(data.name) == lowerName) {
            index = row;
        }

        const auto displayData = Players::DisplayData {
            data.name,         // DisplayData::name
            data.number,       // DisplayData::number
            false,             // DisplayData::disqualified (not used here)
            -1                 // DisplayData::id (not used here)
        };
        m_allBookings->addEntry(displayData,
            m_stringTools->pairOrPlayerName(displayData, StringTools::FormattedName));

        m_bookingChecksums.append(data.bookingChecksum);

        row++;
    }

    m_bookingsCount->setText(tr("%n Voranmeldung(en) erfasst", "", row));

    if (index != -1) {
        m_allBookings->setCurrentIndex(index);
        m_playersName->clear();
        m_playersName->setFocus();
    } else {
        m_allBookings->setCurrentIndex(m_allBookings->findText(currentName));
    }

    if (m_allBookings->currentIndex() == -1 && m_allBookings->count() > 0) {
        m_allBookings->setCurrentIndex(0);
    }

    m_allBookings->blockSignals(false);

    showBookingData();
}

void BookingPage::setNetworkBlocked(bool state)
{
    if (state) {
        reload();
    }

    m_settingsWidget->setEnabled(! state);
    m_networkBlocked->setVisible(state);
}

void BookingPage::showBookingData()
{
    QApplication::setOverrideCursor(Qt::WaitCursor);

    const auto name = m_allBookings->currentName();
    const auto count = m_bookingEngine->escape(name).size();

    int size;

    if (m_tournamentSettings->bookingEnabled() &&
        ! name.isEmpty() && count <= BookingEngine::maxEscapedLength) {

        m_dataTournament->setText(m_bookingEngine->tournamentName());

        const auto checksum = m_bookingEngine->checksum(name, m_bookingEngine->tournamentName());
        const auto &bookedChecksum = m_bookingChecksums.at(m_allBookings->currentIndex());

        m_dataChecksum->setText(checksum);

        if (bookedChecksum.isEmpty()) {
            m_exportStatus->setText(tr("<span style=\"%1\">Bisher wurde kein Anmeldungscode "
                                       "exportiert</span>").arg(SharedStyles::redText));
            m_checksumStatus->setText(tr("<i>Die Prüfsumme wird in der Datenbank gespeichert, "
                                         "sobald ein Anmeldungscode exportiert wurde</i>"));

        } else {
            m_exportStatus->setText(tr("<span style=\"%1\">Es wurde bereits ein Anmeldungscode "
                                       "exportiert</span>").arg(SharedStyles::greenText));

            if (bookedChecksum == checksum) {
                m_checksumStatus->setText(tr("<span style=\"%1\">Die Prüfsumme in der Datenbank "
                                             "ist korrekt</span>").arg(SharedStyles::greenText));
            } else {
                m_checksumStatus->setText(tr(
                    "<p style=\"%1\">Die Prüfsumme weicht von der in der Datenbank gespeicherten "
                    "ab!</p>"
                    "<p>Wenn nur der Anmeldungs- und/oder Turniername nach dem Export des "
                    "Anmeldungscodes editiert wurde(n), ist der Code trotzdem gültig und wird "
                    "korrekt zugewiesen.</p>").arg(SharedStyles::redText));
            }
        }

        const auto data = m_bookingEngine->serialize(name, checksum);
        m_bookingCode = QrCode::generate(data);
        qCDebug(BookingLog) << "QR code data for the currently displayed code:";
        qCDebug(BookingLog).noquote() << data;
        m_bookinCodeData = QString::fromUtf8(data);

        // The maximum size of a QR code with both the tournament name and the pair/player name
        // being 255 chars long should be 97 x 97 px. We still want to display it at least with
        // a scale factor of 2. The resulting ~ 200 x 200 px is still acceptable. However, A nice
        // display is around 120 px. So:
        int qrWidth = m_bookingCode.width();
        if (qrWidth * 2 > 120) {
            qrWidth *= 2;
        } else {
            qrWidth *= std::round(120.0 / qrWidth);
        }
        m_bookingCodeDisplay->setPixmap(QPixmap::fromImage(m_bookingCode.scaled(qrWidth, qrWidth)));
        m_saveCodeButton->setEnabled(true);
        size = qrWidth;

    } else {
        m_dataTournament->setText(tr("–"));
        m_dataChecksum->setText(tr("–"));
        m_showOnRegistrationPage->setText(tr("–"));
        m_exportStatus->setText(tr("–"));
        m_bookingCodeDisplay->setPixmap(QPixmap());
        m_saveCodeButton->setEnabled(false);
        size = 100;
    }

    m_bookingCodeDisplay->setMinimumSize(size, size);

    if (! name.isEmpty()) {
        m_showOnRegistrationPage->setText(tr("<a href=\"#show\">Eintrag auf der Anmeldungsseite "
                                             "zeigen</a>"));
    }

    m_dataNameTooLong->setVisible(count > BookingEngine::maxEscapedLength);

    QApplication::restoreOverrideCursor();
}

QString BookingPage::getSaveFileName(const QString &dir, const QString &defaultFileName,
                                     const QString &wildcard)
{
    const auto fileName = QFileDialog::getSaveFileName(this,
        tr("Bitte einen Dateinamen auswählen"),
        dir + QStringLiteral("/") + defaultFileName,
        wildcard + tr("Alle Dateien") + QStringLiteral(" (*)"));

    if (fileName == m_db->dbFile()) {
        QMessageBox::warning(this, tr("Offene Turnierdatenbank ausgewählt"),
            tr("Als Datei wurde (vermutlich versehentlich) die momentan geöffnete "
               "Turnierdatenbank ausgewählt. Diese wird nicht überschrieben!"));
        return QString();
    }

    return fileName;
}

void BookingPage::exportBookingCode()
{
    // Derive an email-save file name from the pair/player name
    const auto name = m_allBookings->currentText();

    // Check if we would overwrite an existing checksum with a changed one
    const auto &savedChecksum = m_bookingChecksums.at(m_allBookings->currentIndex());
    if (! savedChecksum.isEmpty() && m_dataChecksum->text() != savedChecksum
        && QMessageBox::warning(this, tr("Anmeldungscode speichern"), tr(
               "<p>Für die Voranmeldung „%1“ wurde bereits ein Anmeldungscode exportiert, aber "
               "der momentane Datensatz enthält eine andere Prüfsumme. Das passiert, wenn der "
               "angemeldete Name, der Turniername und/oder der Sicherheitsschlüssel nach dem "
               "Export geändert wurde(n).</p>"
               "<p>Wurde nur der angemeldete Name oder der Turniername geändert, <b>dann bleibt "
               "der alte Anmeldungscode weiterhin gültig</b> und wird korrekt zugewiesen!</p>"
               "<p>Wenn jetzt ein neuer Code für den geänderten Datensatz exportiert wird, <b>dann "
               "wird der alte Code als widerrufen hinterlegt</b> und damit ungültig.</p>"
               "<p>Soll wirklich ein neuer Anmeldungscode exportiert werden?</p>").arg(
               name.toHtmlEscaped()),
               QMessageBox::Yes | QMessageBox::Cancel) != QMessageBox::Yes) {

        return;
    }

    // Replace umlauts
    auto fileName = m_stringTools->replaceUmlauts(name);
    // Replace "ß". "ß" has to be represented as a QStringLiteral, as it's not Latin-1
    fileName = fileName.replace(QStringLiteral("ß"), QLatin1String("ss"));
    // Strip remaining diacritics
    fileName = m_stringTools->stripDiacritics(fileName);
    // Replace all remaining non-alphanumerical characters with a dash
    fileName.replace(QRegularExpression(QStringLiteral("[^0-9a-zA-Z]")), QLatin1String("-"));
    // Combine all dash sequences
    fileName.replace(QRegularExpression(QStringLiteral("-+")), QLatin1String("-"));
    // Use lower-case
    fileName = m_stringTools->toLower(fileName);
    // Add prefix and suffix
    fileName = tr("anmeldungscode_") + fileName + QLatin1String(".png");

    auto dir = m_settings->bookingCodesDir();
    if (dir.isEmpty()) {
        const QFileInfo info(m_db->dbFile());
        dir = info.dir().path();
    }

    fileName = getSaveFileName(dir, fileName, tr("PNG-Dateien") + QStringLiteral(" (*.png);;"));
    if (fileName.isEmpty()) {
        return;
    }

    // We first save the booking code's checksum for the respective pair/player.
    // RegistrationPage::setBookingChecksum will then trigger the execution of savePngFile().
    m_selectedFileName = fileName;
    m_registrationPage->setBookingChecksum(m_allBookings->currentName(), m_dataChecksum->text());
}

void BookingPage::savePngFile()
{
    QApplication::setOverrideCursor(Qt::WaitCursor);

    const auto size = m_bookingCode.width() * m_scaleFactor->value();
    const auto qr = m_bookingCode.scaled(size, size);
    const QFileInfo info(m_selectedFileName);
    if (qr.save(m_selectedFileName, "PNG")) {
        m_settings->saveBookingCodesScaleFactor(m_scaleFactor->value());
        const auto path = QDir::toNativeSeparators(m_selectedFileName);
        QGuiApplication::clipboard()->setText(m_selectedFileName);
        QApplication::restoreOverrideCursor();
        QMessageBox::information(this, tr("Anmeldungscode speichern"),
            tr("<p>Der Anmeldungscode wurde erfolgreich als</p>"
               "<p><kbd>%1</kbd></p>"
               "<p>gespeichert! Der Pfad der Datei wurde auch in die Zwischenablage kopiert."
               "</p>").arg(path.toHtmlEscaped()));
        m_settings->saveBookingCodesDir(info.dir().path());
    } else {
        QApplication::restoreOverrideCursor();
        QMessageBox::warning(this, tr("Anmeldungscode speichern"),
            tr("Das Speichern des Anmeldungscodes ist fehlgeschlagen! Bitte die Zugangsrechte "
               "für <kbd>%1</kbd> überprüfen!").arg(
               QDir::toNativeSeparators(info.dir().path()).toHtmlEscaped()));
    }

    showBookingData();
    m_playersName->setFocus();
}

void BookingPage::resetSettings()
{
    m_tournamentName->setText(m_bookingEngine->tournamentName());
    m_securityKey->setText(m_bookingEngine->securityKey());
    m_saveSettings->setEnabled(false);
    m_resetSettings->setEnabled(false);
    m_bookingBox->setEnabled(m_tournamentSettings->bookingEnabled());
    m_notSavedYet->hide();
    m_tournamentNameTooLong->hide();
}

void BookingPage::deleteSettings()
{
    const auto [ bookingCodesExported, success ] = m_db->bookingCodesExported();
    if (! success) {
        return;
    }

    if (bookingCodesExported
        && QMessageBox::warning(this, tr("Voranmeldung"),
               tr("<p><b>Einstellungen für die Voranmeldung löschen</b></p>"
                  "<p>Wenn die Einstellungen für die Voranmeldung gelöscht werden, <b>dann werden "
                  "alle bisher erstellten Anmeldungscodes ungültig</b>, sofern sie nicht erneut "
                  "mit dem exakt selben Turniernamen und Sicherheitsschlüssel gespeichert werden!"
                  "</p>"
                  "<p>Sollen die Einstellungen wirklich gelöscht werden?</p>"),
               QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Cancel)
           != QMessageBox::Yes) {

        return;
    }

    m_bookingEngine->reset();
    reload();
    showBookingData();
}

void BookingPage::exportSettings()
{
    const QFileInfo info(m_db->dbFile());
    auto fileName = info.baseName() + tr(" Voranmeldungseinstellungen.txt");
    fileName = getSaveFileName(info.dir().path(), fileName,
                               tr("Textdateien") + QStringLiteral(" (*.txt);;"));
    if (fileName.isEmpty()) {
        return;
    }

    QFile exportFile(fileName);
    if (! exportFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
        QMessageBox::warning(this, tr("Einstellungen exportieren"),
            tr("Die Datei <kbd>%1</kbd> konnte nicht geöffnet werden! Bitte die Zugriffsrechte "
               "überprüfen!").arg(QDir::toNativeSeparators(fileName).toHtmlEscaped()));
        return;
    }

    QTextStream outStream(&exportFile);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    outStream.setCodec(QTextCodec::codecForName("UTF-8"));
#endif

    outStream << tr("Diese Datei ist UTF-8-kodiert.\n"
                    "Folgende Einstellungen für die Voranmeldung wurden gespeichert:\n\n")

              << tr("Turniername         : ") << m_bookingEngine->tournamentName() << s_newLine
              << tr("Sicherheitsschlüssel: ") << m_bookingEngine->securityKey() << s_newLine
              << s_newLine

              << tr("Sollte die Datenbank z. B. gelöscht oder beschädigt werden, können diese\n"
                    "Einstellungen für die Voranmeldung in einer neuen Datenbank gesetzt werden.\n"
                    "So bleiben alle vorher generierten Anmeldungs-Codes gültig.\n");

    exportFile.close();

    QMessageBox::information(this, tr("Einstellungen exportieren"),
                             tr("Einstellungen exportiert!"));
}

void BookingPage::showContextMenu(const QPoint &pos)
{
    m_bookingCodeMenu->setEnabled(m_saveCodeButton->isEnabled());
    m_bookingCodeMenu->exec(m_bookingCodeDisplay->mapToGlobal(pos));
}

void BookingPage::selectBookingEntry(const QString &name)
{
    if (name == m_allBookings->currentName()) {
        return;
    }
    m_allBookings->selectName(name);
}
