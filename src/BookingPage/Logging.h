// SPDX-FileCopyrightText: 2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef LOGGING_BOOKING_H
#define LOGGING_BOOKING_H

#include <QLoggingCategory>

Q_DECLARE_LOGGING_CATEGORY(BookingLog)

#endif // LOGGING_BOOKING_H
