// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef BOOKINGPAGE_H
#define BOOKINGPAGE_H

// Local includes
#include "shared/OptionalPage.h"
#include "shared/Players.h"

// Qt includes
#include <QImage>

// Local classes
class SharedObjects;
class Settings;
class TournamentSettings;
class StringTools;
class BookingEngine;
class RegistrationPage;
class PlayersComboBox;
class Database;

// Qt classes
class QStackedLayout;
class QGroupBox;
class QLineEdit;
class QPushButton;
class QLabel;
class QSpinBox;
class QCheckBox;
class QMenu;

class BookingPage : public OptionalPage
{
    Q_OBJECT

public:
    explicit BookingPage(QWidget *parent, SharedObjects *sharedObjects,
                         RegistrationPage *registrationPage);
    void reload();
    void setNetworkBlocked(bool state);

public Q_SLOTS:
    void bookedMarkerChange();
    void checkSavePossible();
    void saveSettings();
    void saveNewBooking();
    void updateBookings();
    void selectBookingEntry(const QString &name);
    void savePngFile();

private Q_SLOTS:
    void resetSettings();
    void deleteSettings();
    void showBookingData();
    void exportBookingCode();
    void exportSettings();
    void checkSaveBookingPossible(const QString &text);
    void showContextMenu(const QPoint &pos);

private: // Functions
    QString getSaveFileName(const QString &dir, const QString &defaultFileName,
                            const QString &wildcard);

private: // Variables
    Settings *m_settings;
    TournamentSettings *m_tournamentSettings;
    StringTools *m_stringTools;
    BookingEngine *m_bookingEngine;
    RegistrationPage *m_registrationPage;
    const QVector<Players::Data> *m_playersData;
    Database *m_db;

    QVector<QString> m_bookingChecksums;

    QStackedLayout *m_layout;
    QGroupBox *m_noBookedMarker;
    QWidget *m_bookingWidget;
    QLabel *m_networkBlocked;

    QLabel *m_bookingActive;
    QCheckBox *m_showSettings;
    QGroupBox *m_settingsBox;
    QWidget *m_settingsWidget;
    QLineEdit *m_tournamentName;
    QLabel *m_tournamentNameTooLong;
    QLineEdit *m_securityKey;
    QLabel *m_notSavedYet;
    QLabel *m_exportSettings;
    QPushButton *m_saveSettings;
    QPushButton *m_resetSettings;
    QPushButton *m_deleteSettings;

    QLabel *m_saveSettingsLabel;
    QGroupBox *m_bookingBox;
    QLineEdit *m_playersName;
    QLabel *m_playersNameTooLong;
    QPushButton *m_registerButton;
    PlayersComboBox *m_allBookings;

    QLabel *m_bookingCodeDisplay;
    QMenu *m_bookingCodeMenu;
    QLabel *m_dataTournament;
    QLabel *m_dataChecksum;
    QLabel *m_checksumStatus;
    QLabel *m_dataNameTooLong;
    QLabel *m_showOnRegistrationPage;
    QLabel *m_exportStatus;

    QImage m_bookingCode;
    QString m_bookinCodeData;
    QSpinBox *m_scaleFactor;
    QPushButton *m_saveCodeButton;
    QString m_selectedFileName;

    QLabel *m_bookingsCount;

};

#endif // BOOKINGPAGE_H
