// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "DisplayDataHelper.h"

// Qt includes
#include <QString>

namespace DisplayDataHelper
{

void appendLine(QString &out, const QString &line)
{
    if (line.isEmpty()) {
        return;
    }

    if (out.isEmpty()) {
        out = line.toHtmlEscaped();
    } else {
        out.append(QStringLiteral("<br/>") + line.toHtmlEscaped());
    }
}

QString formatDataset(const QString &data)
{
    QString out;
    QString line;
    int count = 0;
    for (const auto &c : data) {
        line.append(c);
        if (++count == 32) {
            appendLine(out, line);
            line.clear();
            count = 0;
        }
    }
    appendLine(out, line);
    return out;
}

}
