// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "BookingEngine.h"
#include "Logging.h"

#include "SharedObjects/TournamentSettings.h"

#include "network/ChecksumHelper.h"

// Qt includes
#include <QRandomGenerator>
#include <QJsonObject>
#include <QDebug>

// =================================================================================================
// QR data protocol definition
// =================================================================================================

static const QString s_codeId = QLatin1String("MTBC"); // "MuckTurnier Booking Code"

static constexpr int s_checksumLength = 22; // MD5 hash, base64 encoded

/*
   Escaping considerations
   -----------------------

   To ascertain maximum hardware barcode scanner compatibility, we only use the ASCII charset,
   with some exceptions.

   The ASCII dataset is:

       !"#$%&'()*+,-./
      0123456789:;<=>?
      @ABCDEFGHIJKLMNO
      PQRSTUVWXYZ[\]^_
      `abcdefghijklmno
      pqrstuvwxyz{|}~

   We leave out the following chars:

    - " ": Some scanners simply skip spaces, so we don't use them
    - "_": Used to replace spaces
    - "%": Used by the percent-encoding
    - "^" and "`": Could be interpreted as a combining character to produce e.g. "ê" or "è"
    - "\": May be interpreted as an escaping character

   We thus have the following non-alphanumerical chars left:

      !"#$&'()*+,-./:;<=>?@[]{|}~

   Some of those characters could be used in pair names or player names:

    - "&": Like in "Player 1 & Player 2"
    - "'": Like in "Petro D'Angelo"
    - "+": Like in "Player 1 + Player 2"
    - "-": Like in "Müller-Lüdenscheidt"
    - ".": Like in "Dr. No"
    - "/": Like in "Player 1 / Player 2"

    In one string:

      &'+-./

   We want to use those as-is. Thus, the following chars remain:

      !"#$()*,:;<=>?@[]{|}~

   Those can be used to replace non-ASCII chars frequently found in names so we cheap out some
   percent encoding and keep the QR codes as small as possible.

   A possible test string for the serialization would be:

      &'+-./ _ !"#$()*,:;<=>?@[]{|}~ äöüÄÖÜßéèêëáàâóòñÉÈÊ’

   which consists of:

    - All chars we use as-is: &'+-./
    - Some spaces (that have to be replaced by underscores)
    - An underscore (that has to be percent-encoded)
    - All "unused" ASCII chars
    - All non-ASCII name components to be swapped with the "unused" ASCII chars
*/

// Common non-ASCII name chars to be swapped with "unused" ASCII chars
static const QString s_charsToReplace   = QStringLiteral(R"(äöüÄÖÜßéèêëáàâóòñÉÈÊ’)");
static const QString s_replacementChars = QStringLiteral(R"(!"#$()*,:;<=>?@[]{|}~)");
static constexpr int s_replacementsCount = 21;

// We exclude the following chars from percent encoding and use them as-is:
static const QByteArray s_escapeExcludes = QStringLiteral(
    " "                      // Space, to be replaced by "_" later
    "!\"#$()*,:;<=>?@[]{|}~" // The replacement chars we use
    "&'+-./"                 // The remaining printable ASCII characters
).toUtf8();

// QByteArray::toPercentEncoding by default leaves all alphanumerical characters (a-z, A-Z, 0-9)
// as well as "-", ".", "_" and "~" unencoded. We thus have to request having "_" encoded
// explicitely:
static const QByteArray s_escapeIncludes = QStringLiteral("_").toUtf8();

// We want to swap " " with "_" after percent-encoding and before percent-unencoding:
static const QChar s_space      = QChar::fromLatin1(' ');
static const QChar s_underscore = QChar::fromLatin1('_');

// =================================================================================================

// Security key
// ------------

// For the security key, we use the whole ASCII set, except for:
// space, single quotes, double quotes, and backslash
// (because those might cause problems in a file containing the key as a backup)
static const QString s_keySource = QLatin1String(
    "!#$%&()*+,-./"    // No space and no quoues
    "0123456789:;<=>?"
    "@ABCDEFGHIJKLMNO"
    "PQRSTUVWXYZ[]^_"  // No backslash
    "`abcdefghijklmno"
    "pqrstuvwxyz{|}~"
);

BookingEngine::BookingEngine(QObject *parent, TournamentSettings *tournamentSettings)
    : QObject(parent),
      m_tournamentSettings(tournamentSettings)
{
}

void BookingEngine::reload()
{
    m_settings.clear();
    m_escapedTournamentName.clear();

    const auto settings = m_tournamentSettings->bookingSettings();
    const auto escapedTournamentName = escape(settings.tournamentName);
    if (escapedTournamentName.size() > BookingEngine::maxEscapedLength) {
        // This should not happen, unless the database was modified by hand
        qCWarning(BookingLog) << "Corrupted dataset: Escaped tournament name is too long."
                              << "Falling back to defaults!";
    } else {
        m_settings = settings;
        m_escapedTournamentName = escapedTournamentName;
    }

    if (! settings.isValid()) {
        qCDebug(BookingLog) << "Initializing new security key";
        QString securityKey;
        const auto keySourceCount = s_keySource.size();
        for (int i = 0; i < 20; i++) {
            securityKey.append(s_keySource.at(
                QRandomGenerator::global()->bounded(keySourceCount)));
        }
        m_settings.securityKey = securityKey;
    }

    m_tournamentSettings->setBookingEnabled(m_settings.isValid());

    qCDebug(BookingLog) << "Now using the following booking settings:";
    qCDebug(BookingLog) << "    Tournament name:" << m_settings.tournamentName;
    qCDebug(BookingLog) << "    Security key   :" << m_settings.securityKey;
}

const QString &BookingEngine::tournamentName() const
{
    return m_settings.tournamentName;
}

const QString &BookingEngine::securityKey() const
{
    return m_settings.securityKey;
}

QString BookingEngine::checksum(const QString &name, const QString &tournament) const
{
    if (tournament.isEmpty()) {
        qCWarning(BookingLog) << "BookingEngine::checksum: Corrupdated dataset:"
                              << "Empty tournament name passed";
        return QString();
    }

    if (name.isEmpty()) {
        qCWarning(BookingLog) << "BookingEngine::checksum: Corrupdated dataset:"
                              << "Empty pair/player name passed";
        return QString();
    }

    if (m_settings.securityKey.isEmpty()) {
        qCWarning(BookingLog) << "BookingEngine::checksum: No security key present! "
                                 "This should not happen!";
        return QString();
    }

    ChecksumHelper checksumHelper;
    checksumHelper.addData(tournament);
    checksumHelper.addData(name);
    checksumHelper.addData(m_settings.securityKey);
    return checksumHelper.checksum();
}

QString BookingEngine::swapReplacementChars(QString text) const
{
    for (int i = 0; i < text.size(); i++) {
        auto currentChar = text.at(i);
        for (int j = 0; j < s_replacementsCount; j++) {
            if (currentChar == s_charsToReplace.at(j)) {
                text[i] = s_replacementChars.at(j);
            } else if (currentChar == s_replacementChars.at(j)) {
                text[i] = s_charsToReplace.at(j);
            }
        }
    }
    return text;
}

QString BookingEngine::escape(QString string) const
{
    // Swap possible name components with directly usable but "unused" ASCII chars.
    // Convert the result to a byte array so that we can percent-encode it.
    const auto swapped = swapReplacementChars(string).toUtf8();

    // Percent-encode the swapped string
    string = QString::fromUtf8(swapped.toPercentEncoding(s_escapeExcludes, s_escapeIncludes));

    // Replace all spaces with underscores
    string.replace(s_space, s_underscore);

    // Return the escaped string
    return string;
}

QString BookingEngine::unEscape(QString string) const
{
    // Replace all underscores with spaces
    string.replace(s_underscore, s_space);

    // Percent-unencode the string
    string = QString::fromUtf8(QByteArray::fromPercentEncoding(string.toUtf8()));

    // Swap the "unused" ASCII chars with their possible name component counterpart
    string = swapReplacementChars(string);

    // Return the unescaped string
    return string;
}

QByteArray BookingEngine::encodeInt(int value) const
{
    Q_ASSERT_X(value <= BookingEngine::maxEscapedLength,
               "BookingEngine::encodeInt", "Value bigger than maxEscapedLength passed");
    return QString::number(value, 36).rightJustified(2, QChar::fromLatin1('0')).toUtf8();
}

int BookingEngine::decodeInt(const QString &value) const
{
    Q_ASSERT_X(value.size() == 2, "BookingEngine::decodeInt", "Invalid value passed");
    bool okay;
    const auto result = value.toInt(&okay, 36);
    return okay ? result : -1;
}

QByteArray BookingEngine::encodeChecksum(const QString &checksum) const
{
    return QByteArray::fromHex(checksum.toUtf8()).toBase64(QByteArray::Base64Encoding
                                                           | QByteArray::OmitTrailingEquals);
}

QString BookingEngine::decodeChecksum(const QString &text) const
{
    const auto result = QByteArray::fromBase64Encoding(text.toUtf8());
    if (result.decodingStatus != QByteArray::Base64DecodingStatus::Ok) {
        return QString();
    }
    const auto checksum = QString::fromUtf8(result.decoded.toHex());
    if (checksum.size() != 32) { // The hex MD5 hash is 32 chars long
        return QString();
    }
    return checksum;
}

QByteArray BookingEngine::serialize(const QString &name, const QString &checksum) const
{
    QByteArray rawData;

    // Code ID header (4 chars)
    rawData.append(s_codeId.toUtf8());

    // Protocol version as hex (2 chars)
    rawData.append(encodeInt(BookingEngine::protocolVersion));

    // Length of the escaped tournament name as hex (2 chars)
    rawData.append(encodeInt(m_escapedTournamentName.size()));

    // Length of the escaped pair/player name as hex (2 chars)
    const auto escapedName = escape(name);
    Q_ASSERT_X(escapedName.size() <= BookingEngine::maxEscapedLength,
               "BookingEngine::serialize", "Escaped name too long!");
    rawData.append(encodeInt(escapedName.size()));

    // The escaped tournament name
    rawData.append(m_escapedTournamentName.toUtf8());

    // The esacped pair/player name
    rawData.append(escapedName.toUtf8());

    // The checksum
    rawData.append(encodeChecksum(checksum));

    return rawData;
}

BookingEngine::QrData BookingEngine::parse(const QString &data) const
{
    const auto dataCount = data.size();

    QrData parsed;

    // Check the data to be valid

    // The smallest possible dataset is something like this:
    //
    // "MTBC 01 01 01 A B nNPitDtB4OEFJ8Sp9od5MA" (without the spaces)
    //
    // Header = 4 chars
    // Version = 2 chars
    // Length of tournament name data = 2 chars
    // Length of pair/player name data = 2 chars
    // Tournament name = at least 1 char
    // Pair/Player name = at least 1 char
    // Checksum = 22 chars
    //
    // In sum: 34 chars

    // Thus, the data is invalid if it's shorter than 34 chars:
    if (dataCount < 34) {
        parsed.error = tr("Der Datensatz ist zu kurz");
        return parsed;
    }

    // Check for our header (first four chars)
    if (data.left(4) != s_codeId) {
        parsed.error = tr("Der Datensatz enthält keinen gültigen Header");
        return parsed;
    }

    // Extract and check all numbers

    // Extract the version
    const auto version = decodeInt(data.mid(4, 2));
    if (version == -1) {
        parsed.error = tr("Die Protokoll-Versionsnummer konnte nicht ausgelesen werden");
        return parsed;
    }

    // Check for the version to be correct
    if (version != BookingEngine::protocolVersion) {
        parsed.error = tr("Die Protokoll-Versionsnummer ist falsch "
                          "(erwartet: %1, gelesen: %2)").arg(
                          QString::number(BookingEngine::protocolVersion),
                          QString::number(version));
        return parsed;
    }

    // Extract the tournament name length
    const auto tournamentNameLength = decodeInt(data.mid(6, 2));
    if (tournamentNameLength == -1) {
        parsed.error = tr("Die Länge des Turniernamens konnte nicht ausgelesen werden");
        return parsed;
    }

    // Extract the players name length
    const auto nameLength = decodeInt(data.mid(8, 2));
    if (nameLength == -1) {
        parsed.error = tr("Die Länge des Paar- bzw. Spielernamens konnte nicht ausgelesen werden");
        return parsed;
    }

    // Do a plausibility check for the found lengths.
    // The header and the lengths are 10 chars long.
    // The checksum has s_checksumLength chars.
    // The rest has to be the tournament and the pair/player name.
    // Thus:
    if (dataCount - 10 - tournamentNameLength - nameLength != s_checksumLength) {
        parsed.error = tr("Die Längenangaben im Datensatz stimmen nicht mit der Datensatzlänge "
                          "überein");
        return parsed;
    }

    // Extract the tournament name
    const auto tournament = unEscape(data.mid(10, tournamentNameLength));

    // Extract the pair/player name
    const auto name = unEscape(data.mid(10 + tournamentNameLength, nameLength));

    // Extract the checksum
    const auto dataChecksum = decodeChecksum(data.right(s_checksumLength));
    if (dataChecksum.isEmpty()) {
        parsed.error = tr("Die Prüfsumme konnte nicht ausgelesen werden");
        return parsed;
    }

    // Fill in the data
    parsed.name = name;
    parsed.tournament = tournament;
    parsed.checksum = dataChecksum;
    parsed.isValid = dataChecksum == checksum(name, tournament);

    return parsed;
}

void BookingEngine::reset()
{
    m_tournamentSettings->resetBookingSettings();
    m_tournamentSettings->setBookingEnabled(false);
    reload();
}

bool BookingEngine::looksLikeBookingCode(const QString &string) const
{
    return string.startsWith(s_codeId);
}

bool BookingEngine::containsBookingCodeHeader(const QString &string) const
{
    return string.contains(s_codeId);
}
