// SPDX-FileCopyrightText: 2010-2020 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef ABOUTDIALOG_H
#define ABOUTDIALOG_H

// Local includes
#include "shared/TitleDialog.h"

// Local classes
class SharedObjects;

class AboutDialog : public TitleDialog
{
    Q_OBJECT

public:
    explicit AboutDialog(QWidget *parent, SharedObjects *sharedObjects);

};

#endif // ABOUTDIALOG_H
