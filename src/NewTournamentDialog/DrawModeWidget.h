// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef DRAWMODEWIDGET_H
#define DRAWMODEWIDGET_H

// Qt includes
#include <QWidget>

// Qt classes
class QRadioButton;

class DrawModeWidget : public QWidget
{
    Q_OBJECT

public:
    enum TextMode {
        BasicTexts,
        ScoreTexts
    };

    explicit DrawModeWidget(TextMode textMode, QWidget *parent = nullptr);
    bool autoSelectPairs() const;
    bool selectByLastRound() const;

Q_SIGNALS:
    void drawModeChanged(bool autoSelectPairs, bool selectByLastRound);

public Q_SLOTS:
    void setDrawMode(bool autoSelectPairs, bool selectByLastRound);
    void emitDrawModeChanged();

private: // Variables
    QRadioButton *m_pairs2Rotate;
    QRadioButton *m_drawEachRound;
    QRadioButton *m_ignoreDraw;

};

#endif // DRAWMODEWIDGET_H
