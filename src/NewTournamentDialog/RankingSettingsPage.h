// SPDX-FileCopyrightText: 2020 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef RANKINGSETTINGSPAGE_H
#define RANKINGSETTINGSPAGE_H

// Local includes
#include "shared/AbstractSettingsPage.h"

// Local classes

class TournamentSettings;

namespace TournamentTemplate
{
struct Settings;
}

// Qt classes
class QLabel;
class QCheckBox;

class RankingSettingsPage : public AbstractSettingsPage
{
    Q_OBJECT

public:
    explicit RankingSettingsPage(const TournamentTemplate::Settings &tournamentTemplate,
                                 TournamentSettings *tournamentSettings,
                                 QWidget *parent = nullptr);
    void saveSettings() override;

private: // Variables
    TournamentSettings *m_tournamentSettings;
    QCheckBox *m_includeOpponentGoals;
    QCheckBox *m_grayOutUnimportantGoals;

};

#endif // RANKINGSETTINGSPAGE_H
