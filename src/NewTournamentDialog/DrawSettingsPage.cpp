// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "DrawSettingsPage.h"

#include "SharedObjects/TournamentSettings.h"

#include "shared/TournamentTemplate.h"
#include "shared/DrawSettingsWidget.h"

// Qt includes
#include <QVBoxLayout>
#include <QLabel>
#include <QScrollArea>

DrawSettingsPage::DrawSettingsPage(const TournamentTemplate::Settings &tournamentTemplate,
                                   TournamentSettings *tournamentSettings,
                                   QWidget *parent)
    : AbstractSettingsPage(tr("Auslosung"), parent),
      m_tournamentSettings(tournamentSettings)
{
    auto *scrollArea = new QScrollArea;
    layout()->addWidget(scrollArea);

    auto *content = new QWidget;
    auto *contentLayout = new QVBoxLayout(content);
    contentLayout->setContentsMargins(0, 0, 0, 0);

    QLabel *drawSettingsLabel = new QLabel(tr(
        "Die hier einstellbaren Optionen können auf der „Anmeldung“-Seite (via „Einstellungen“ → "
        "„Auslosung“) beliebig verändert werden, auch während des Turniers."));
    drawSettingsLabel->setWordWrap(true);
    contentLayout->addWidget(drawSettingsLabel);

    m_drawSettingsWidget = new DrawSettingsWidget;
    m_drawSettingsWidget->updateSettings(tournamentTemplate.drawSettings);
    contentLayout->addWidget(m_drawSettingsWidget);

    auto *label = new QLabel(tr("Für die „Übersicht Auslosung“-Seite siehe auch "
                                "<a href=\"#raiseOptionalPages\">Optionale Seiten</a>!"));
    label->setWordWrap(true);
    label->setTextInteractionFlags(Qt::TextBrowserInteraction);
    connect(label, &QLabel::linkActivated, this, &AbstractSettingsPage::linkActivated);
    contentLayout->addWidget(label);

    scrollArea->setWidget(content);
}

void DrawSettingsPage::saveSettings()
{
    m_tournamentSettings->setDrawSettings(m_drawSettingsWidget->settings());
}
