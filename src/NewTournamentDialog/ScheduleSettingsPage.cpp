// SPDX-FileCopyrightText: 2023-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "ScheduleSettingsPage.h"

#include "SharedObjects/TournamentSettings.h"

#include "shared/TournamentTemplate.h"

#include "SchedulePage/ScheduleSettingsWidget.h"

// Qt includes
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>

ScheduleSettingsPage::ScheduleSettingsPage(const TournamentTemplate::Settings &tournamentTemplate,
                                           TournamentSettings *tournamentSettings,
                                           QWidget *parent)
    : AbstractSettingsPage(tr("Turnier-Zeitplan"), parent),
      m_tournamentSettings(tournamentSettings)
{
    auto *settingsLabel = new QLabel(tr(
        "Die hier einstellbaren Optionen können auf der optionalen „Turnier-Zeitplan“-Seite "
        "(einblendbar via „Extras“ → „Turnier-Zeitplan“) beliebig verändert werden, auch während "
        "des Turniers."));
    settingsLabel->setWordWrap(true);
    layout()->addWidget(settingsLabel);

    auto *wrapperLayout = new QHBoxLayout;
    layout()->addLayout(wrapperLayout);
    m_settings = new ScheduleSettingsWidget;
    m_settings->adoptSettings(tournamentTemplate.scheduleSettings);
    wrapperLayout->addWidget(m_settings);
    wrapperLayout->addStretch();

    auto *label = new QLabel(tr("Für die „Turnier-Zeitplan“-Seite siehe auch "
                                "<a href=\"#raiseOptionalPages\">Optionale Seiten</a>!"));

    layout()->addStretch();

    label->setWordWrap(true);
    label->setTextInteractionFlags(Qt::TextBrowserInteraction);
    connect(label, &QLabel::linkActivated, this, &AbstractSettingsPage::linkActivated);
    layout()->addWidget(label);
}

void ScheduleSettingsPage::saveSettings()
{
    m_tournamentSettings->setScheduleSettings(m_settings->settings());
}
