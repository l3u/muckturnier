// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "RegistrationSettingsPage.h"

#include "SharedObjects/TournamentSettings.h"

#include "shared/TournamentTemplate.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QCheckBox>
#include <QGroupBox>
#include <QMessageBox>
#include <QFrame>

RegistrationSettingsPage::RegistrationSettingsPage(
    const TournamentTemplate::Settings &tournamentTemplate,
    TournamentSettings *tournamentSettings,
    QWidget *parent)
    : AbstractSettingsPage(tr("Spalten Anmeldeliste"), parent),
      m_tournamentSettings(tournamentSettings)
{
    QLabel *label;

    label = new QLabel(tr("Die hier einstellbaren Optionen können auf der „Anmeldung“-Seite "
                          "beliebig verändert werden, auch während des Turniers."));
    label->setWordWrap(true);
    layout()->addWidget(label);

    auto *columnsBox = new QGroupBox;

    auto *columnsBoxLayout = new QVBoxLayout(columnsBox);
    label = new QLabel(tr("Neben der „Name“-Spalte werden zu Beginn folgende Spalten "
                          "der Anmeldeliste eingeblendet:"));
    label->setWordWrap(true);
    columnsBoxLayout->addWidget(label);
    layout()->addWidget(columnsBox);

    for (const RegistrationColumns::Column column : RegistrationColumns::allColumns) {
        auto *checkBox = new QCheckBox(RegistrationColumns::checkBoxText.value(column));
        m_column[column] = checkBox;
        checkBox->setChecked(tournamentTemplate.registrationColumns.contains(column));
        columnsBoxLayout->addWidget(checkBox);
    }
    connect(m_column.value(RegistrationColumns::Column::Draw), &QCheckBox::toggled,
            this, &RegistrationSettingsPage::drawColumnShown);

    auto *line = new QFrame;
    line->setFrameShape(QFrame::HLine);
    line->setProperty("isseparator", true);
    columnsBoxLayout->addWidget(line);

    m_headerHidden = new QCheckBox(tr("Spalten-Kopfzeile ausblenden"));
    m_headerHidden->setChecked(tournamentTemplate.registrationListHeaderHidden);
    columnsBoxLayout->addWidget(m_headerHidden);

    label = new QLabel(tr("Für die „Übersicht Auslosung“-Seite siehe "
                          "<a href=\"#raiseOptionalPages\">Optionale Seiten</a>!"));
    label->setWordWrap(true);
    label->setTextInteractionFlags(Qt::TextBrowserInteraction);
    connect(label, &QLabel::linkActivated, this, &AbstractSettingsPage::linkActivated);
    layout()->addWidget(label);

    layout()->addStretch();
}

void RegistrationSettingsPage::saveSettings()
{
    QVector<RegistrationColumns::Column> columns;
    for (const RegistrationColumns::Column column : RegistrationColumns::allColumns) {
        if (m_column[column]->isChecked()) {
            columns.append(column);
        }
    }
    m_tournamentSettings->setRegistrationColumns(columns);
    m_tournamentSettings->setRegistrationListHeaderHidden(m_headerHidden->isChecked());
}

void RegistrationSettingsPage::drawOverviewPageSelected(bool state)
{
    m_drawOverviewPageSelected = state;
}

void RegistrationSettingsPage::drawColumnShown(bool state)
{
    if (! state
        || m_drawOverviewPageSelected
        || QMessageBox::question(this,
               tr("Spalte „Auslosung einblenden“"),
               tr("Soll die „Übersicht Auslosung“-Seite auch gleich zu Beginn eingeblendet "
                  "werden?"))
           != QMessageBox::Yes) {

        return;

    } else {
        Q_EMIT showDrawOverviewPage();
    }
}
