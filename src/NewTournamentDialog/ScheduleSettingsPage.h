// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SCHEDULESETTINGSPAGE_H
#define SCHEDULESETTINGSPAGE_H

// Local includes
#include "shared/AbstractSettingsPage.h"

// Local classes

class TournamentSettings;
class ScheduleSettingsWidget;

namespace TournamentTemplate
{
struct Settings;
}

class ScheduleSettingsPage : public AbstractSettingsPage
{
    Q_OBJECT

public:
    explicit ScheduleSettingsPage(const TournamentTemplate::Settings &tournamentTemplate,
                                  TournamentSettings *tournamentSettings,
                                  QWidget *parent = nullptr);
    void saveSettings() override;

private: // Variables
    TournamentSettings *m_tournamentSettings;
    ScheduleSettingsWidget *m_settings;

};

#endif // SCHEDULESETTINGSPAGE_H
