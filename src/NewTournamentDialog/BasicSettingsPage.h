// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef BASICSETTINGSPAGE_H
#define BASICSETTINGSPAGE_H

// Local includes
#include "shared/AbstractSettingsPage.h"
#include "shared/Tournament.h"

// Local classes

class TournamentSettings;
class DrawModeWidget;

namespace TournamentTemplate
{
struct Settings;
}

// Qt classes
class QLabel;
class QRadioButton;
class QSpinBox;

class BasicSettingsPage : public AbstractSettingsPage
{
    Q_OBJECT

public:
    explicit BasicSettingsPage(const TournamentTemplate::Settings &tournamentTemplate,
                               TournamentSettings *tournamentSettings,
                               QWidget *parent = nullptr);
    void saveSettings() override;
    DrawModeWidget *drawModeWidget() const;

Q_SIGNALS:
    void tournamentModeSelected(Tournament::Mode mode);

private: // Variables
    TournamentSettings *m_tournamentSettings;
    QRadioButton *m_fixedPairs;
    QRadioButton *m_singlePlayers;
    QSpinBox *m_boogersPerRound;
    QSpinBox *m_boogerScore;
    DrawModeWidget *m_drawModeWidget;

};

#endif // BASICSETTINGSPAGE_H
