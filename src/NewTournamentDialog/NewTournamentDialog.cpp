// SPDX-FileCopyrightText: 2010-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "NewTournamentDialog.h"
#include "BasicSettingsPage.h"
#include "RegistrationSettingsPage.h"
#include "DrawSettingsPage.h"
#include "ScoreSettingsPage.h"
#include "RankingSettingsPage.h"
#include "OptionalPagesPage.h"
#include "MarkersSettingsPage.h"
#include "ScheduleSettingsPage.h"
#include "DrawModeWidget.h"

#include "Database/Database.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/Settings.h"
#include "SharedObjects/TournamentSettings.h"

#include "shared/TournamentTemplate.h"
#include "shared/StackedWidgetList.h"
#include "shared/AbstractSettingsPage.h"
#include "shared/OptionalPages.h"
#include "shared/TournamentTemplateHelper.h"
#include "shared/Booking.h"

#include "version.h"

// Qt includes
#include <QDateTime>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QCheckBox>
#include <QApplication>
#include <QMessageBox>
#include <QDir>
#include <QDebug>
#include <QTimer>
#include <QStyle>

NewTournamentDialog::NewTournamentDialog(QWidget *parent, SharedObjects *sharedObjects)
    : FileDialog(parent, sharedObjects->database()),
      m_sharedObjects(sharedObjects),
      m_db(sharedObjects->database()),
      m_settings(sharedObjects->settings()),
      m_tournamentSettings(sharedObjects->tournamentSettings())
{
    setTitle(tr("Neues Turnier starten"));
    setOkButtonText(tr("Turnier starten"));
    setTargetFileBaseName(QLocale::system().toString(QDateTime::currentDateTime(),
                                                     m_settings->dbFileNameTemplate()));
    setTargetFileExtension(QStringLiteral("mtdb"));
    setTargetFileDescription(tr("Muckturnier-Datenbanken"));
    setFileDialogTitle(tr("Bitte einen Dateinamen für die Turnierdatenbank auswählen"));

    const TournamentTemplate::Settings tournamentTemplate = m_settings->tournamentTemplate();

    m_stackedWidgetList = new StackedWidgetList;
    m_stackedWidgetList->setNavigationVisible(false);
    layout()->addWidget(m_stackedWidgetList);

    // Basic settings
    auto *basicSettingsPage = new BasicSettingsPage(tournamentTemplate, m_tournamentSettings);
    connect(basicSettingsPage, &AbstractSettingsPage::linkActivated,
            this, &NewTournamentDialog::processActivatedLink);
    m_stackedWidgetList->addWidget(basicSettingsPage);

    // Registration list columns settings
    auto *registrationSettingsPage
        = new RegistrationSettingsPage(tournamentTemplate, m_tournamentSettings);
    registrationSettingsPage->drawOverviewPageSelected(
        tournamentTemplate.optionalPages.contains(OptionalPages::Page::DrawOverview));
    connect(registrationSettingsPage, &AbstractSettingsPage::linkActivated,
            this, &NewTournamentDialog::processActivatedLink);
    m_stackedWidgetList->addWidget(registrationSettingsPage);

    // Markers "settings"
    auto *markersSettingsPage = new MarkersSettingsPage(m_settings);
    connect(basicSettingsPage, &BasicSettingsPage::tournamentModeSelected,
            markersSettingsPage, &MarkersSettingsPage::tournamentModeSelected);
    m_stackedWidgetList->addWidget(markersSettingsPage);

    // Schedule settings
    auto *scheduleSettingsPage = new ScheduleSettingsPage(tournamentTemplate, m_tournamentSettings);
    connect(scheduleSettingsPage, &AbstractSettingsPage::linkActivated,
            this, &NewTournamentDialog::processActivatedLink);
    m_stackedWidgetList->addWidget(scheduleSettingsPage);

    // Draw settings
    m_drawSettingsPage = new DrawSettingsPage(tournamentTemplate, m_tournamentSettings);
    connect(m_drawSettingsPage, &AbstractSettingsPage::linkActivated,
            this, &NewTournamentDialog::processActivatedLink);
    m_stackedWidgetList->addWidget(m_drawSettingsPage);

    // Score settings
    auto *scoreSettingsPage = new ScoreSettingsPage(tournamentTemplate, m_tournamentSettings);
    m_stackedWidgetList->addWidget(scoreSettingsPage);
    connect(basicSettingsPage->drawModeWidget(), &DrawModeWidget::drawModeChanged,
            scoreSettingsPage->drawModeWidget(), &DrawModeWidget::setDrawMode);
    connect(scoreSettingsPage->drawModeWidget(), &DrawModeWidget::drawModeChanged,
            basicSettingsPage->drawModeWidget(), &DrawModeWidget::setDrawMode);

    // Ranking settings
    auto *rankingSettingsPage = new RankingSettingsPage(tournamentTemplate, m_tournamentSettings);
    m_stackedWidgetList->addWidget(rankingSettingsPage);

    // Optional Pages
    m_optionalPagesPage = new OptionalPagesPage(tournamentTemplate, m_tournamentSettings);
    connect(m_optionalPagesPage, &OptionalPagesPage::drawOverviewPageSelected,
            registrationSettingsPage, &RegistrationSettingsPage::drawOverviewPageSelected);
    connect(registrationSettingsPage, &RegistrationSettingsPage::showDrawOverviewPage,
            m_optionalPagesPage, &OptionalPagesPage::showDrawOverviewPage);
    m_stackedWidgetList->addWidget(m_optionalPagesPage);

    // "Extended options" and "Save template"
    auto *saveTemplateLayout = new QHBoxLayout;
    layout()->addLayout(saveTemplateLayout);

    m_showAllOptions = new QCheckBox(tr("Alle Optionen anzeigen"));
    saveTemplateLayout->addWidget(m_showAllOptions);

    // We don't want to use showAllOptions() whilst constructing, as this
    // 1. would cause an unneeded settings write and 2. we don't have a valid geometry yet
    if (m_settings->newTournamentShowExtendedOptions()) {
        m_showAllOptions->setChecked(true);
        m_stackedWidgetList->setNavigationVisible(true);
        m_stackedWidgetList->setCurrentIndex(0);
    }

    connect(m_showAllOptions, &QCheckBox::toggled, this, &NewTournamentDialog::showAllOptions);

    saveTemplateLayout->addStretch();

    m_saveTemplate = new QCheckBox(tr("Als Vorlage für neue Turniere speichern"));
    saveTemplateLayout->addWidget(m_saveTemplate);
}

void NewTournamentDialog::accept()
{
    const QString lastDbPath = m_db->dbFile();

    QString fileName = getTargetFileName(m_settings->lastDbPath());
    if (fileName.isEmpty()) {
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    setEnabled(false);
    QApplication::processEvents();

    Q_EMIT updateStatus(tr("Lege neue Turnierdatenbank an …"));

    if (! m_db->create(fileName)) {
        Q_EMIT updateStatus(tr("Anlegen der neuen Turnierdatenbank fehlgeschlagen"));
        QApplication::restoreOverrideCursor();
        setEnabled(true);
        QMessageBox::critical(this,
            tr("Neue Turnierdatenbank anlegen"),
            tr("<p><b>Anlegen der Datenbank ist fehlgeschlagen</b></p>"
               "<p>Das Anlegen der Turnierdatenbank „%1“ ist nicht möglich:</p><p>%2</p>").arg(
               QDir::toNativeSeparators(fileName).toHtmlEscaped(), m_db->errorMessage()),
            QMessageBox::Ok);
        if (! lastDbPath.isEmpty()) {
            Q_EMIT reopenLastOpenedDb(lastDbPath);
        }
        return;
    }

    // Save the new db's path (if enabled)
    if (m_settings->saveOnCloseEnabled(Settings::SaveLastDbPath)) {
        m_settings->saveLastDbPath(m_db->dbPath());
    }

    // Initialize the tournament settings
    m_tournamentSettings->initializeTournamentOpen();
    m_tournamentSettings->setMuckturnierVersion(QLatin1String(MT_VERSION));
    m_tournamentSettings->setSpecialMarkers(m_settings->specialMarkers());
    m_tournamentSettings->setSinglesManagement(m_settings->singlesManagementTemplate());
    m_tournamentSettings->setBookingSettings(Booking::Settings());

    // Add the settings from the pages
    for (int i = 0; i < m_stackedWidgetList->count(); i++) {
        qobject_cast<AbstractSettingsPage *>(m_stackedWidgetList->widget(i))->saveSettings();
    }

    // Adopt in the markers from the template
    if (! m_db->addMarkers(m_settings->markersTemplate(m_tournamentSettings->tournamentMode()))) {
        QApplication::restoreOverrideCursor();
        QDialog::reject();
        return;
    }

    // Save the settings to the database
    if (! m_db->saveSettings()) {
        QApplication::restoreOverrideCursor();
        QDialog::reject();
        return;
    }

    // Update the tournament template if requested
    if (m_saveTemplate->isChecked()) {
        m_settings->saveTournamentTemplate(
            TournamentTemplateHelper::assembleTemplate(m_tournamentSettings));
    }

    Q_EMIT updateStatus(tr("Turnier „%1“ gestartet.").arg(QDir::toNativeSeparators(fileName)));
    QApplication::restoreOverrideCursor();
    QDialog::accept();
}

void NewTournamentDialog::showAllOptions(bool checked)
{
    const auto oldGeometry = frameGeometry();

    m_stackedWidgetList->setNavigationVisible(checked);
    m_stackedWidgetList->setCurrentIndex(0);
    resizeToContents();

    m_settings->saveNewTournamentShowExtendedOptions(checked);

    // Keep the original top position while still centering the dialog
    QTimer::singleShot(0, this, [this, oldGeometry]
    {
        const auto newGeometry = frameGeometry();
        move(newGeometry.left() + (oldGeometry.width() - newGeometry.width()) / 2,
             oldGeometry.top());
    });
}

void NewTournamentDialog::processActivatedLink(const QString &anchor)
{
    if (anchor == QStringLiteral("#raiseDrawSettingsPage")) {
        m_showAllOptions->setChecked(true);
        m_stackedWidgetList->setCurrentWidget(m_drawSettingsPage);
    } else if (anchor == QStringLiteral("#raiseOptionalPages")) {
        m_stackedWidgetList->setCurrentWidget(m_optionalPagesPage);
    }
}
