// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef REGISTRATIONSETTINGSPAGE_H
#define REGISTRATIONSETTINGSPAGE_H

// Local includes
#include "shared/AbstractSettingsPage.h"
#include "shared/RegistrationColumns.h"

// Local classes

class TournamentSettings;

namespace TournamentTemplate
{
struct Settings;
}

// Qt classes
class QCheckBox;

class RegistrationSettingsPage : public AbstractSettingsPage
{
    Q_OBJECT

public:
    explicit RegistrationSettingsPage(const TournamentTemplate::Settings &tournamentTemplate,
                                      TournamentSettings *tournamentSettings,
                                      QWidget *parent = nullptr);
    void saveSettings() override;

Q_SIGNALS:
    void showDrawOverviewPage();

public Q_SLOTS:
    void drawOverviewPageSelected(bool state);

private Q_SLOTS:
    void drawColumnShown(bool state);

private: // Variables
    TournamentSettings *m_tournamentSettings;
    QHash<RegistrationColumns::Column, QCheckBox *> m_column;
    QCheckBox *m_headerHidden;
    bool m_drawOverviewPageSelected;

};

#endif // REGISTRATIONSETTINGSPAGE_H
