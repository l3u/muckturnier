// SPDX-FileCopyrightText: 2022-2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef MARKERSSETTINGSPAGE_H
#define MARKERSSETTINGSPAGE_H

// Local includes
#include "shared/AbstractSettingsPage.h"
#include "shared/Markers.h"
#include "shared/Tournament.h"

// Local classes
class Settings;
class MarkersDisplay;

class MarkersSettingsPage : public AbstractSettingsPage
{
    Q_OBJECT

public:
    explicit MarkersSettingsPage(Settings *settings, QWidget *parent = nullptr);
    void saveSettings() override;

public Q_SLOTS:
    void tournamentModeSelected(Tournament::Mode mode);

private: // Variables
    Settings *m_settings;
    QVector<Markers::Marker> m_markersTemplate;
    QHash<Markers::Type, int> m_specialMarkers;
    Markers::SinglesManagement m_singlesManagement;
    MarkersDisplay *m_display;

};

#endif // MARKERSSETTINGSPAGE_H
