// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "ScoreSettingsPage.h"
#include "DrawModeWidget.h"

#include "SharedObjects/TournamentSettings.h"

#include "shared/TournamentTemplate.h"

// Qt includes
#include <QLabel>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QGridLayout>
#include <QRadioButton>
#include <QCheckBox>
#include <QDebug>
#include <QFrame>

ScoreSettingsPage::ScoreSettingsPage(const TournamentTemplate::Settings &tournamentTemplate,
                                     TournamentSettings *tournamentSettings,
                                     QWidget *parent)
    : AbstractSettingsPage(tr("Ergebniseingabe"), parent),
      m_tournamentSettings(tournamentSettings)
{
    QLabel *inputSettingsLabel = new QLabel(tr("Die hier einstellbaren Optionen können auf der "
                                               "„Ergebnisse“-Seite beliebig verändert werden, auch "
                                               "während des Turniers."));
    inputSettingsLabel->setWordWrap(true);
    layout()->addWidget(inputSettingsLabel);

    // Score type

    QGroupBox *scoreTypeBox = new QGroupBox(tr("Spielstandzettel"));
    QGridLayout *scoreTypeLayout = new QGridLayout(scoreTypeBox);
    layout()->addWidget(scoreTypeBox);

    m_scoreTypeHorizontal = new QRadioButton(tr("Horizontal"));
    m_scoreTypeHorizontal->setChecked(tournamentTemplate.scoreType == Tournament::HorizontalScore);
    scoreTypeLayout->addWidget(m_scoreTypeHorizontal, 0, 0, Qt::AlignTop);

    scoreTypeLayout->addWidget(
        descriptionLabel(tr("Die Paare stehen oben und unten.\n"
                            "Die Bobbl werden von links nach rechts aufgeschrieben.")),
        0, 1);

    m_scoreTypeVertical = new QRadioButton(tr("Vertikal"));
    m_scoreTypeVertical->setChecked(tournamentTemplate.scoreType != Tournament::HorizontalScore);
    scoreTypeLayout->addWidget(m_scoreTypeVertical, 1, 0, Qt::AlignTop);

    scoreTypeLayout->addWidget(
        descriptionLabel(tr("Die Paare stehen links und rechts.\n"
                            "Die Bobbl werden von oben nach unten aufgeschrieben.")),
        1, 1);

    // Automatic pairs selection

    auto *autoSelectBox = new QGroupBox(tr("Automatische Paarauswahl"));
    QVBoxLayout *autoSelectBoxLayout = new QVBoxLayout(autoSelectBox);
    layout()->addWidget(autoSelectBox);

    m_drawModeWidget = new DrawModeWidget(DrawModeWidget::ScoreTexts);
    m_drawModeWidget->setDrawMode(tournamentTemplate.autoSelectPairs,
                                  tournamentTemplate.selectByLastRound);
    autoSelectBoxLayout->addWidget(m_drawModeWidget);

    // Automatic pairs/players selection for the next draw
    auto *autoSelectTableBox = new QGroupBox(tr("Automatische Auswahl für die nächste Auslosung"));
    auto *autoSelectTableLayout = new QVBoxLayout(autoSelectTableBox);
    layout()->addWidget(autoSelectTableBox);
    m_autoSelectTableForDraw = new QCheckBox(tr(
        "Automatisch alle Paare bzw. Spieler für die Auslosung der nächsten Runde\n"
        "auf der Anmeldungsseite auswählen, wenn ein Ergebnis eingegeben wurde"));
    autoSelectTableLayout->addWidget(m_autoSelectTableForDraw);
    m_autoSelectTableForDraw->setChecked(tournamentTemplate.autoSelectTableForDraw);

    layout()->addStretch();
}

DrawModeWidget *ScoreSettingsPage::drawModeWidget() const
{
    return m_drawModeWidget;
}

void ScoreSettingsPage::saveSettings()
{
    m_tournamentSettings->setScoreType(m_scoreTypeHorizontal->isChecked()
                                           ? Tournament::HorizontalScore
                                           : Tournament::VerticalScore);
    m_tournamentSettings->setAutoSelectPairs(m_drawModeWidget->autoSelectPairs());
    m_tournamentSettings->setSelectByLastRound(m_drawModeWidget->selectByLastRound());
    m_tournamentSettings->setAutoSelectTableForDraw(m_autoSelectTableForDraw->isChecked());
}
