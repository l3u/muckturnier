// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "DrawModeWidget.h"

#include "shared/DrawModeHelper.h"

// Qt includes
#include <QVBoxLayout>
#include <QRadioButton>
#include <QLabel>
#include <QMessageBox>

DrawModeWidget::DrawModeWidget(DrawModeWidget::TextMode textMode, QWidget *parent)
    : QWidget(parent)
{
    auto *layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSizeConstraint(QLayout::SetMinimumSize);

    QString pairs2RotateText;
    QString drawEachRoundText;
    QString ignoreDrawText;
    switch (textMode) {
    case TextMode::BasicTexts:
        pairs2RotateText = tr("Paar 1 bleibt sitzen, Paar 2 rutscht pro Runde einen Tisch weiter");
        drawEachRoundText = tr("Alle Plätze werden in jeder Runde neu ausgelost");
        ignoreDrawText = tr("Keine Auslosung bzw. automatische Paar- bzw. Tischauswahl");
        break;
    case TextMode::ScoreTexts:
        pairs2RotateText = tr("In der 1. Runde laut Auslosung (sofern vorhanden), ab dann laut\n"
                              "der letzten Runde (Paar 1 bleibt sitzen, Paar 2 rutscht weiter)");
        drawEachRoundText = tr("Auswahl laut Auslosung für die Runde (sofern vorhanden)");
        ignoreDrawText = tr("Keine automatische Paar- bzw. Tischauswahl");
        break;
    }

    m_pairs2Rotate = new QRadioButton(pairs2RotateText);
    connect(m_pairs2Rotate, &QRadioButton::clicked, this, &DrawModeWidget::emitDrawModeChanged);
    layout->addWidget(m_pairs2Rotate);

    m_drawEachRound = new QRadioButton(drawEachRoundText);
    connect(m_drawEachRound, &QRadioButton::clicked, this, &DrawModeWidget::emitDrawModeChanged);
    layout->addWidget(m_drawEachRound);

    m_ignoreDraw = new QRadioButton(ignoreDrawText);
    connect(m_ignoreDraw, &QRadioButton::clicked, this, &DrawModeWidget::emitDrawModeChanged);
    layout->addWidget(m_ignoreDraw);

    auto *showDescription = new QLabel(tr("<a href=\"#\">Ausführliche Beschreibung</a>"));
    showDescription->setTextInteractionFlags(Qt::TextBrowserInteraction);
    connect(showDescription, &QLabel::linkActivated, this, [this]
    {
        QMessageBox::information(this, tr("Auslosung bzw. automatische Paar- bzw. Tischauswahl"),
            tr("<p><i>Folgende Einstellung für die Auslosung bzw. die automatische Paar- bzw. "
               "Tischauswahl bei der Ergebniseingabe wurde gewählt:</i></p>")
               + DrawModeHelper::description(autoSelectPairs(), selectByLastRound()));
    });
    layout->addWidget(showDescription);
}

void DrawModeWidget::setDrawMode(bool autoSelectPairs, bool selectByLastRound)
{
    m_pairs2Rotate->setChecked(autoSelectPairs && selectByLastRound);
    m_drawEachRound->setChecked(autoSelectPairs && ! selectByLastRound);
    m_ignoreDraw->setChecked(! autoSelectPairs && ! selectByLastRound);
}

void DrawModeWidget::emitDrawModeChanged()
{
    Q_EMIT drawModeChanged(autoSelectPairs(), selectByLastRound());
}

bool DrawModeWidget::autoSelectPairs() const
{
    return ! m_ignoreDraw->isChecked();
}

bool DrawModeWidget::selectByLastRound() const
{
    return m_pairs2Rotate->isChecked();
}
