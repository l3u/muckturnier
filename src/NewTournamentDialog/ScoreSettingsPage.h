// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SCORESETTINGSPAGE_H
#define SCORESETTINGSPAGE_H

// Local includes
#include "shared/AbstractSettingsPage.h"

// Local classes

class TournamentSettings;
class DrawModeWidget;

namespace TournamentTemplate
{
struct Settings;
}

// Qt classes
class QRadioButton;
class QCheckBox;
class QLabel;

class ScoreSettingsPage : public AbstractSettingsPage
{
    Q_OBJECT

public:
    explicit ScoreSettingsPage(const TournamentTemplate::Settings &tournamentTemplate,
                               TournamentSettings *tournamentSettings,
                               QWidget *parent = nullptr);
    void saveSettings() override;
    DrawModeWidget *drawModeWidget() const;

private: // Variables
    TournamentSettings *m_tournamentSettings;
    QRadioButton *m_scoreTypeHorizontal;
    QRadioButton *m_scoreTypeVertical;
    DrawModeWidget *m_drawModeWidget;
    QLabel *m_selectDescription;
    QCheckBox *m_autoSelectTableForDraw;

};

#endif // SCORESETTINGSPAGE_H
