// SPDX-FileCopyrightText: 2010-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef NEWTOURNAMENTDIALOG_H
#define NEWTOURNAMENTDIALOG_H

// Local includes
#include "shared/FileDialog.h"

// Local classes
class SharedObjects;
class Database;
class Settings;
class TournamentSettings;
class StackedWidgetList;
class OptionalPagesPage;
class DrawSettingsPage;

// Qt classes
class QCheckBox;

class NewTournamentDialog : public FileDialog
{
    Q_OBJECT

public:
    explicit NewTournamentDialog(QWidget *parent, SharedObjects *sharedObjects);

Q_SIGNALS:
    void reopenLastOpenedDb(const QString &lastDbPath);

private Q_SLOTS:
    void accept() override;
    void showAllOptions(bool checked);
    void processActivatedLink(const QString &anchor);

private: // Variables
    SharedObjects *m_sharedObjects;
    Database *m_db;
    Settings *m_settings;
    TournamentSettings *m_tournamentSettings;
    StackedWidgetList *m_stackedWidgetList;
    QCheckBox *m_showAllOptions;
    QCheckBox *m_saveTemplate;

    DrawSettingsPage *m_drawSettingsPage;
    OptionalPagesPage *m_optionalPagesPage;

};

#endif // NEWTOURNAMENTDIALOG_H
