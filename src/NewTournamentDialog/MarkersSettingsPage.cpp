// SPDX-FileCopyrightText: 2022-2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "MarkersSettingsPage.h"

#include "SharedObjects/Settings.h"

#include "shared/Markers.h"
#include "shared/MarkersDisplay.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QLabel>
#include <QGroupBox>

MarkersSettingsPage::MarkersSettingsPage(Settings *settings, QWidget *parent)
    : AbstractSettingsPage(tr("Markierungen"), parent),
      m_settings(settings)
{
    auto *label = new QLabel(tr("Die hier verwendete Vorlage kann via „Extras“ → „Einstellungen“ → "
                                "„Vorlage Markierungen“ aus einer Datenbank erzeugt werden!"));
    label->setWordWrap(true);
    layout()->addWidget(label);

    m_markersTemplate = m_settings->markersTemplate(Tournament::FixedPairs);
    m_specialMarkers = m_settings->specialMarkers();
    m_singlesManagement = m_settings->singlesManagementTemplate();

    m_display = new MarkersDisplay(Tournament::FixedPairs, &m_markersTemplate,
                                   &m_specialMarkers, &m_singlesManagement);
    layout()->addWidget(m_display);
    m_display->refresh();

    layout()->addStretch();
}

void MarkersSettingsPage::tournamentModeSelected(Tournament::Mode mode)
{
    m_display->setTournamentMode(mode);
    m_markersTemplate = m_settings->markersTemplate(mode);
    m_display->setMarkersTemplate(&m_markersTemplate);
    m_display->refresh();
}

void MarkersSettingsPage::saveSettings()
{
    // The markers data is set by the NewTournamentDialog itself, as it can't be changed here
}
