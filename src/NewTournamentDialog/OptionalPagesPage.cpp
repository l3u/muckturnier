// SPDX-FileCopyrightText: 2020-2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "OptionalPagesPage.h"

#include "SharedObjects/TournamentSettings.h"

#include "shared/TournamentTemplate.h"
#include "shared/OptionalPages.h"

// Qt includes
#include <QLabel>
#include <QVBoxLayout>
#include <QCheckBox>
#include <QDebug>

OptionalPagesPage::OptionalPagesPage(const TournamentTemplate::Settings &tournamentTemplate,
                                     TournamentSettings *tournamentSettings,
                                     QWidget *parent)
    : AbstractSettingsPage(tr("Optionale Seiten"), parent),
      m_tournamentSettings(tournamentSettings)
{
    auto *inputSettingsLabel = new QLabel(tr("Folgende optional anzeigbare Seiten können bereits "
                                             "beim Starten eingeblendet werden (einblendbar über "
                                             "„Extras“):"));
    inputSettingsLabel->setWordWrap(true);
    layout()->addWidget(inputSettingsLabel);

    m_drawOverviewPage = new QCheckBox(tr("Übersicht Auslosung"));
    m_drawOverviewPage->setChecked(tournamentTemplate.optionalPages.contains(
                                       OptionalPages::DrawOverview));
    connect(m_drawOverviewPage, &QCheckBox::toggled,
            this, &OptionalPagesPage::drawOverviewPageSelected);
    layout()->addWidget(m_drawOverviewPage);

    m_schedulePage = new QCheckBox(tr("Turnier-Zeitplan"));
    m_schedulePage->setChecked(tournamentTemplate.optionalPages.contains(OptionalPages::Schedule));
    layout()->addWidget(m_schedulePage);

    layout()->addStretch();
}

void OptionalPagesPage::saveSettings()
{
    m_tournamentSettings->setOptionalPageShown(OptionalPages::DrawOverview,
                                               m_drawOverviewPage->isChecked());
    m_tournamentSettings->setOptionalPageShown(OptionalPages::Schedule,
                                               m_schedulePage->isChecked());
}

void OptionalPagesPage::showDrawOverviewPage()
{
    m_drawOverviewPage->setChecked(true);
}
