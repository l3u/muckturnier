// SPDX-FileCopyrightText: 2020-2022 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "RankingSettingsPage.h"

#include "SharedObjects/TournamentSettings.h"

#include "shared/TournamentTemplate.h"

// Qt includes
#include <QLabel>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QCheckBox>

RankingSettingsPage::RankingSettingsPage(const TournamentTemplate::Settings &tournamentTemplate,
                                         TournamentSettings *tournamentSettings,
                                         QWidget *parent)
    : AbstractSettingsPage(tr("Rangliste"), parent),
      m_tournamentSettings(tournamentSettings)
{
    QLabel *rankingSettingsLabel = new QLabel(tr("Die hier einstellbaren Optionen können auf der "
                                                 "„Rangliste“-Seite beliebig verändert werden, "
                                                 "auch während des Turniers."));
    rankingSettingsLabel->setWordWrap(true);
    layout()->addWidget(rankingSettingsLabel);

    QGroupBox *rankingCreationBox = new QGroupBox(tr("Ranglistenerstellung"));
    QVBoxLayout *rankingCreationLayout = new QVBoxLayout(rankingCreationBox);
    layout()->addWidget(rankingCreationBox);

    m_includeOpponentGoals = new QCheckBox(tr("Gegnerische geschossene Tore berücksichtigen"));
    m_includeOpponentGoals->setChecked(tournamentTemplate.includeOpponentGoals);
    rankingCreationLayout->addWidget(m_includeOpponentGoals);

    QGroupBox *rankingDisplayBox = new QGroupBox(tr("Ranglistendarstellung"));
    QVBoxLayout *rankingDisplayLayout = new QVBoxLayout(rankingDisplayBox);
    layout()->addWidget(rankingDisplayBox);

    m_grayOutUnimportantGoals = new QCheckBox(tr("Nicht entscheidende Tore ausgrauen"));
    m_grayOutUnimportantGoals->setChecked(tournamentTemplate.includeOpponentGoals);
    rankingDisplayLayout->addWidget(m_grayOutUnimportantGoals);

    layout()->addStretch();
}

void RankingSettingsPage::saveSettings()
{
    m_tournamentSettings->setIncludeOpponentGoals(m_includeOpponentGoals->isChecked());
    m_tournamentSettings->setGrayOutUnimportantGoals(m_grayOutUnimportantGoals->isChecked());
}
