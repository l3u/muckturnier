// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef DRAWSETTINGSPAGE_H
#define DRAWSETTINGSPAGE_H

// Local includes
#include "shared/AbstractSettingsPage.h"

// Local classes

class TournamentSettings;
class DrawSettingsWidget;

namespace TournamentTemplate
{
struct Settings;
}

class DrawSettingsPage : public AbstractSettingsPage
{
    Q_OBJECT

public:
    explicit DrawSettingsPage(const TournamentTemplate::Settings &tournamentTemplate,
                              TournamentSettings *tournamentSettings,
                              QWidget *parent = nullptr);
    void saveSettings() override;

private: // Variables
    TournamentSettings *m_tournamentSettings;
    DrawSettingsWidget *m_drawSettingsWidget;

};

#endif // DRAWSETTINGSPAGE_H
