// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef OPTIONALPAGESPAGE_H
#define OPTIONALPAGESPAGE_H

// Local includes
#include "shared/AbstractSettingsPage.h"

// Local classes

class TournamentSettings;

namespace TournamentTemplate
{
struct Settings;
}

// Qt classes
class QCheckBox;

class OptionalPagesPage : public AbstractSettingsPage
{
    Q_OBJECT

public:
    explicit OptionalPagesPage(const TournamentTemplate::Settings &tournamentTemplate,
                               TournamentSettings *tournamentSettings,
                               QWidget *parent = nullptr);
    void saveSettings() override;

Q_SIGNALS:
    void drawOverviewPageSelected(bool state);

public Q_SLOTS:
    void showDrawOverviewPage();

private: // Variables
    TournamentSettings *m_tournamentSettings;
    QCheckBox *m_drawOverviewPage;
    QCheckBox *m_schedulePage;

};

#endif // OPTIONALPAGESPAGE_H
