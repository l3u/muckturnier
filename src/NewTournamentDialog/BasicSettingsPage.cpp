// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "BasicSettingsPage.h"
#include "DrawModeWidget.h"

#include "SharedObjects/TournamentSettings.h"

#include "shared/TournamentTemplate.h"
#include "shared/DefaultValues.h"

// Qt includes
#include <QLabel>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QRadioButton>
#include <QSpinBox>

// C++ includes
#include <functional>

BasicSettingsPage::BasicSettingsPage(const TournamentTemplate::Settings &tournamentTemplate,
                                     TournamentSettings *tournamentSettings,
                                     QWidget *parent)
    : AbstractSettingsPage(tr("Grundeinstellungen"), parent),
      m_tournamentSettings(tournamentSettings)
{
    QLabel *label;
    QGroupBox *box;

    label = new QLabel(tr(
        "Turniertyp, Punkte pro Bobbl und Bobbl pro Runde müssen beim Erstellen des Turniers "
        "festgelegt werden und sind später nicht mehr veränderbar:"));
    label->setWordWrap(true);
    layout()->addWidget(label);

    // Tournament type

    box = new QGroupBox(tr("Turniertyp"));
    QGridLayout *tournamentTypeBoxLayout = new QGridLayout(box);
    tournamentTypeBoxLayout->setSizeConstraint(QLayout::SetMinimumSize);

    layout()->addWidget(box);

    m_fixedPairs = new QRadioButton(tr("Feste Paare"));
    m_fixedPairs->setChecked(tournamentTemplate.tournamentMode == Tournament::FixedPairs);
    connect(m_fixedPairs, &QRadioButton::toggled,
            this, [this](bool checked)
            {
                Q_EMIT tournamentModeSelected(checked ? Tournament::FixedPairs
                                                      : Tournament::SinglePlayers);
            });
    tournamentTypeBoxLayout->addWidget(m_fixedPairs, 0, 0, Qt::AlignTop);

    tournamentTypeBoxLayout->addWidget(
        descriptionLabel(tr("Es werden immer zwei Spieler als festes Paar angemeldet.\n"
                            "Die Paare spielen das ganze Turnier lang zusammen.")),
        0, 1);

    m_singlePlayers = new QRadioButton(tr("Einzelne Spieler"));
    m_singlePlayers->setChecked(! m_fixedPairs->isChecked());
    tournamentTypeBoxLayout->addWidget(m_singlePlayers, 1, 0, Qt::AlignTop);

    tournamentTypeBoxLayout->addWidget(
        descriptionLabel(tr("Es werden einzelne Spieler angemeldet.\n"
                            "Die Paare werden (ggf. pro Runde) ausgelost.")),
        1, 1);

    // Boogers and scores

    QGroupBox *boogersBox = new QGroupBox(tr("Bobbl und Punkte"));
    QHBoxLayout *boogersBoxLayout = new QHBoxLayout(boogersBox);
    layout()->addWidget(boogersBox);

    boogersBoxLayout->addWidget(new QLabel(tr("Punkte pro Bobbl:")));
    m_boogerScore = new QSpinBox;
    m_boogerScore->setMinimum(1);
    m_boogerScore->setMaximum(DefaultValues::maximumBoogerScore);
    m_boogerScore->setValue(tournamentTemplate.boogerScore);
    boogersBoxLayout->addWidget(m_boogerScore);

    boogersBoxLayout->addStretch();

    boogersBoxLayout->addWidget(new QLabel(tr("Bobbl pro Runde:")));
    m_boogersPerRound = new QSpinBox;
    m_boogersPerRound->setMinimum(1);
    m_boogersPerRound->setMaximum(DefaultValues::maximumBoogersPerRound);
    m_boogersPerRound->setValue(tournamentTemplate.boogersPerRound);
    boogersBoxLayout->addWidget(m_boogersPerRound);

    label = new QLabel(tr(
        "<p>Alle Einstellungen für die Auslosung bzw. automatische Paar- bzw. Tischauswahl können "
        "auch später noch auf der „Anmeldung“- und der „Ergebnisse“-Seite gewählt werden. Für die "
        "genauen Auslosungs-Einstellungen siehe <a href=\"#raiseDrawSettingsPage\">Auslosung"
        "</a>!</p>"));
    label->setWordWrap(true);
    label->setTextInteractionFlags(Qt::TextBrowserInteraction);
    connect(label, &QLabel::linkActivated, this, &AbstractSettingsPage::linkActivated);
    layout()->addWidget(label);

    box = new QGroupBox(tr("Modus Auslosung bzw. automatische Paar-/Tischauswahl"));
    QGridLayout *drawBoxLayout = new QGridLayout(box);
    layout()->addWidget(box);

    m_drawModeWidget = new DrawModeWidget(DrawModeWidget::BasicTexts);
    m_drawModeWidget->setDrawMode(tournamentTemplate.autoSelectPairs,
                                  tournamentTemplate.selectByLastRound);
    connect(m_fixedPairs, &QRadioButton::clicked,
            m_drawModeWidget, std::bind(&DrawModeWidget::setDrawMode, m_drawModeWidget,
                                        true, true));
    connect(m_fixedPairs, &QRadioButton::clicked, m_drawModeWidget,
            &DrawModeWidget::emitDrawModeChanged);
    connect(m_singlePlayers, &QRadioButton::clicked,
            m_drawModeWidget, std::bind(&DrawModeWidget::setDrawMode, m_drawModeWidget,
                                        true, false));
    connect(m_singlePlayers, &QRadioButton::clicked, m_drawModeWidget,
            &DrawModeWidget::emitDrawModeChanged);
    drawBoxLayout->addWidget(m_drawModeWidget);

    layout()->addStretch();
}

DrawModeWidget *BasicSettingsPage::drawModeWidget() const
{
    return m_drawModeWidget;
}

void BasicSettingsPage::saveSettings()
{
    m_tournamentSettings->setTournamentMode(m_fixedPairs->isChecked() ? Tournament::FixedPairs
                                                                      : Tournament::SinglePlayers);
    m_tournamentSettings->setBoogerScore(m_boogerScore->value());
    m_tournamentSettings->setBoogersPerRound(m_boogersPerRound->value());
}
