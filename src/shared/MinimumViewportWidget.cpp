// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "MinimumViewportWidget.h"

MinimumViewportWidget::MinimumViewportWidget(QWidget *parent) : QWidget(parent)
{
}

QSize MinimumViewportWidget::minimumViewport() const
{
    return m_minimumViewport.isValid() ? m_minimumViewport : sizeHint();
}

void MinimumViewportWidget::setMinimumViewport(const QSize &size)
{
    m_minimumViewport = size;
}
