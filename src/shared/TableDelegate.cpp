// SPDX-FileCopyrightText: 2010-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "Application.h"

#include "TableDelegate.h"

// Qt includes
#include <QPainter>
#include <QBrush>
#include <QDebug>
#include <QGuiApplication>
#include <QPalette>

static bool s_isInitialized = false;
static QColor s_topLineColor;
static QBrush s_alternateBackground;
static QBrush s_highlightBrush;
static QPen s_selectedPen;

TableDelegate::TableDelegate(QObject *parent, RowColorMode rowColorMode)
    : QStyledItemDelegate(parent),
      m_rowColorMode(rowColorMode)
{
    if (! s_isInitialized) {
        initialize();
    }

    connect(qobject_cast<Application *>(qApp), &Application::paletteChanged,
            this, &TableDelegate::initialize);
}

void TableDelegate::initialize()
{
    const bool darkMode = qApp->property("DARK_MODE").toBool();
    const QPalette palette;

    if (! darkMode) {
        s_topLineColor = palette.color(QPalette::Mid);
    } else {
        s_topLineColor = palette.color(QPalette::Light);
    }

#ifndef Q_OS_MACOS
    s_alternateBackground = QBrush(palette.color(QPalette::AlternateBase));
#else
    // If we're on macOS and in dark mode, QPalette::AlternateBase doesn't look nice.
    // We use a color derived from the default base in this case:
    if (! darkMode) {
        s_alternateBackground = QBrush(palette.color(QPalette::AlternateBase));
    } else {
        s_alternateBackground = QBrush(palette.color(QPalette::Base).lighter(140));
    }
#endif

    if (! darkMode) {
        s_highlightBrush = QBrush(palette.color(QPalette::Highlight).lighter(180));
    } else {
        s_highlightBrush = QBrush(palette.color(QPalette::Highlight).darker(330));
    }

    s_selectedPen = QPen(palette.color(QPalette::Highlight), 2);

    s_isInitialized = true;
}

void TableDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option,
                          const QModelIndex &index) const
{
    painter->save();
    const auto row = index.row();
    const auto column = index.column();

    // We work on a copy of the QStyleOptionViewItem, in case we have to alter it
    auto opt = option;

    if (column == m_firstVisibleColumn) {
        // Add two pixels to the width (which is the "selected" border with) so that we can move
        // the name two pixels to the right if the respective row is selected and we won't overlap
        // with the next column
        opt.rect.setWidth(option.rect.width() + 2);
    }

    // Draw the background, according to the set alternation mode or hovered rows

    if (row >= m_firstHoveredRow && row <= m_lastHoveredRow) {
        // We want to highlight the respective row
        painter->fillRect(opt.rect, s_highlightBrush);

    } else {
        // We check if we should draw the alternative background
        bool useAlternateBackground = false;

        switch (m_rowColorMode) {
        case RowColorMode::AlternatingRows:
            useAlternateBackground = row % 2 != 0;
            break;
        case RowColorMode::AlternatingNRows:
            useAlternateBackground = row / m_alternationStep % 2 != 0;
            break;
        case RowColorMode::CustomAlternating:
            useAlternateBackground = index.data(Display::DrawBackground).isValid();
        }

        if (useAlternateBackground) {
            painter->fillRect(opt.rect, s_alternateBackground);
        }
    }

    // Draw a frame if the row is selected

    if (row == m_selectedRow) {
        painter->setPen(s_selectedPen);

        if (column == 0) {
            // Left frame part
            painter->drawLine(QPoint(opt.rect.left() + 1, opt.rect.top() + 1),
                              QPoint(opt.rect.left() + 1, opt.rect.bottom()));
        }

        if (column == m_lastVisibleColumn) {
            // Right frame part
            painter->drawLine(QPoint(opt.rect.right(), opt.rect.top() + 1),
                              QPoint(opt.rect.right(), opt.rect.bottom()));
        }

        // Top frame part
        painter->drawLine(QPoint(opt.rect.left() + 1, opt.rect.top() + 1),
                          QPoint(opt.rect.right() - (column == m_lastVisibleColumn),
                                 opt.rect.top() + 1));

        // Bottom frame part
        painter->drawLine(QPoint(opt.rect.left() + 1, opt.rect.bottom()),
                          QPoint(opt.rect.right() - (column == m_lastVisibleColumn),
                                 opt.rect.bottom()));
    }

    // Possibly draw a top line

    if (row != m_selectedRow && row != m_selectedRow + 1) {
        bool doDrawTopLine = false;

        if (m_rowColorMode != RowColorMode::CustomAlternating
            && row != 0
            && row != m_selectedRow + 1
            && (m_rowColorMode == RowColorMode::AlternatingRows || row % m_alternationStep == 0)) {

            doDrawTopLine = true;

        } else if (m_rowColorMode == RowColorMode::CustomAlternating) {
            doDrawTopLine = index.data(Display::DrawTopLine).isValid();
        }

        if (doDrawTopLine) {
            painter->setPen(s_topLineColor);
            painter->drawLine(opt.rect.topLeft(), opt.rect.topRight());
        }
    }

    painter->restore();

    // Paint the text
    if (row == m_selectedRow && column == m_firstVisibleColumn) {
        // The row is selected and we draw the first visible column. So we move the text two pixels
        // to the right so that we have the same space between the frame and the text as we had
        // before between the widget border and the text. We compensate this by restoring the
        // original width (to which we added two pixels as a reserve above)
        opt.rect.setLeft(option.rect.left() + 2);
        opt.rect.setWidth(option.rect.width());
    }
    drawText(painter, opt, index);
}

void TableDelegate::drawText(QPainter *painter, const QStyleOptionViewItem &option,
                             const QModelIndex &index) const
{
    QStyledItemDelegate::paint(painter, option, index);
}

void TableDelegate::setAlternationStep(int rows)
{
    m_alternationStep = rows;
}

void TableDelegate::setFirstVisibleColumn(int column)
{
    m_firstVisibleColumn = column;
}

void TableDelegate::setLastVisibleColumn(int column)
{
    m_lastVisibleColumn = column;
}

void TableDelegate::setSelectedRow(int row)
{
    m_selectedRow = row;
}

void TableDelegate::setHoveredRow(int row)
{
    setHoveredRows(row, row);
}

void TableDelegate::setHoveredRows(int first, int last)
{
    m_firstHoveredRow = first;
    m_lastHoveredRow = last;
}
