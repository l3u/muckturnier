// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef NOTETYPE_H
#define NOTETYPE_H

namespace NoteType
{

enum Type {
    Shared,
    TableNumbers,
    DrawNotes,
    ScoreNotes1Booger,
    ScoreNotes,
    ScoreNotes3Boogers
};

}

#endif // NOTETYPE_H
