// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef NOTETYPE_H
#define NOTETYPE_H

namespace NoteType
{

enum Type {
    Shared,
    TableNumbers,
    DrawNotes,
    ScoreNotes,
    ScoreNotes3Boogers
};

}

#endif // NOTETYPE_H
