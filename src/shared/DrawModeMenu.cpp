// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "DrawModeMenu.h"

#include "SharedObjects/TournamentSettings.h"

// Qt includes
#include <QActionGroup>

DrawModeMenu::DrawModeMenu(TextMode textMode, TournamentSettings *tournamentSettings,
                           QWidget *parent)
    : QMenu(parent),
      m_tournamentSettings(tournamentSettings)
{
    QString menuTitle;
    QString drawEachRoundText;
    QString ignoreDrawText;
    switch (textMode) {
    case TextMode::DrawTexts:
        menuTitle = tr("Auslosungs-Modus");
        drawEachRoundText = tr("Jede Runde wird neu ausgelost");
        ignoreDrawText = tr("Keine Auslosung");
        break;
    case TextMode::SelectTexts:
        menuTitle = tr("Automatische Paar- bzw. Tisch-Auswahl");
        drawEachRoundText = tr("Auswahl laut Auslosung für die Runde");
        ignoreDrawText = tr("Keine automatische Auswahl");
        break;
    }

    setTitle(menuTitle);

    m_pairs2Rotate  = addAction(tr("Paar 1 bleibt sitzen, 2 rutscht weiter"));
    m_drawEachRound = addAction(drawEachRoundText);
    m_ignoreDraw    = addAction(ignoreDrawText);

    m_pairs2Rotate->setCheckable(true);
    m_drawEachRound->setCheckable(true);
    m_ignoreDraw->setCheckable(true);

    auto *group = new QActionGroup(this);
    group->addAction(m_pairs2Rotate);
    group->addAction(m_drawEachRound);
    group->addAction(m_ignoreDraw);

    connect(m_tournamentSettings, &TournamentSettings::drawModeChanged,
            this, &DrawModeMenu::selectMode);

    connect(m_pairs2Rotate, &QAction::triggered, this, &DrawModeMenu::modeSelected);
    connect(m_drawEachRound, &QAction::triggered, this, &DrawModeMenu::modeSelected);
    connect(m_ignoreDraw, &QAction::triggered, this, &DrawModeMenu::modeSelected);
}

void DrawModeMenu::selectMode()
{
    if (m_ownChange) {
        return;
    }

    m_pairs2Rotate->setChecked(
        m_tournamentSettings->autoSelectPairs() && m_tournamentSettings->selectByLastRound());
    m_drawEachRound->setChecked(
        m_tournamentSettings->autoSelectPairs() && ! m_tournamentSettings->selectByLastRound());
    m_ignoreDraw->setChecked(
        ! m_tournamentSettings->autoSelectPairs());
}

void DrawModeMenu::modeSelected()
{
    m_ownChange = true;
    if (m_pairs2Rotate->isChecked()) {
        m_tournamentSettings->setDrawModeParameters(true, true);
    } else if (m_drawEachRound->isChecked()) {
        m_tournamentSettings->setDrawModeParameters(true, false);
    } else if (m_ignoreDraw->isChecked()) {
        m_tournamentSettings->setDrawModeParameters(false, false);
    }
    m_ownChange = false;
    Q_EMIT drawModeChanged();
}
