// SPDX-FileCopyrightText: 2010-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "SearchWidget.h"
#include "SearchLineEdit.h"
#include "PhoneticSearchButton.h"
#include "SharedStyles.h"

// Qt includes
#include <QHBoxLayout>
#include <QLabel>
#include <QDebug>

SearchWidget::SearchWidget(const QString &searchId, SharedObjects *sharedObjects,
                           QWidget *parent) : QWidget(parent)
{
    QHBoxLayout *layout = new QHBoxLayout(this);

    m_searchLabel = new QLabel(tr("Suche:"));
    layout->addWidget(m_searchLabel);

    m_search = new SearchLineEdit;
    m_search->setClearButtonEnabled(true);
    connect(m_search, &QLineEdit::textChanged, this, &SearchWidget::processInput);
    layout->addWidget(m_search);

    m_phoneticSearch = new PhoneticSearchButton(searchId, sharedObjects);
    connect(m_phoneticSearch, &QToolButton::toggled,
            this, [this]
            {
                Q_EMIT searchChanged(m_search->text());
            });
    layout->addWidget(m_phoneticSearch);

    m_matches = new QLabel;
    m_matches->hide();
    layout->addWidget(m_matches);
}

void SearchWidget::updateMatches(int matches, int hiddenMatches)
{
    m_matches->show();

    QString text = matches == 0 ? tr("<span style=\"%1\">Keine Treffer</span>").arg(
                                     SharedStyles::redText)
                                : tr("%1 Treffer").arg(matches);
    if (hiddenMatches > 0) {
        text.append(tr(" – <span style=\"%1\">%2 ausgeblendet</span>").arg(
                       SharedStyles::redText, QString::number(hiddenMatches)));
    }

    m_matches->setText(text);
}

void SearchWidget::processInput(const QString &text)
{
    Q_EMIT searchChanged(text);
    if (text.isEmpty()) {
        m_matches->hide();
    }
}

void SearchWidget::clear()
{
    m_search->clear();
    m_savedSearch.clear();
}

void SearchWidget::saveSearch()
{
    if (! m_savedSearch.isEmpty()) {
        return;
    }

    m_savedSearch = m_search->text();
    m_search->clear();
}

void SearchWidget::restoreSearch()
{
    if (m_savedSearch.isEmpty()) {
        return;
    }

    m_search->setText(m_savedSearch);
    m_savedSearch.clear();
}

bool SearchWidget::phoneticSearch() const
{
    return m_phoneticSearch->isChecked();
}

QString SearchWidget::searchText() const
{
    return m_search->text();
}

void SearchWidget::setSearchText(const QString &text)
{
    m_search->setText(text);
}

QString SearchWidget::matchesText() const
{
    return m_matches->text();
}
