// SPDX-FileCopyrightText: 2010-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef TABLEDELEGATE_H
#define TABLEDELEGATE_H

// Qt includes
#include <QStyledItemDelegate>
#include <QPixmap>

// Qt classes
class QPainter;

class TableDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:
    enum RowColorMode {
        AlternatingRows,
        AlternatingNRows,
        CustomAlternating
    };

    enum Display {
        DrawBackground = Qt::UserRole,
        DrawTopLine
    };

    explicit TableDelegate(QObject *parent,
                           RowColorMode rowColorMode = RowColorMode::AlternatingRows);
    void setAlternationStep(int rows);

public Q_SLOTS:
    void setFirstVisibleColumn(int column);
    void setLastVisibleColumn(int column);
    void setSelectedRow(int row);
    void setHoveredRow(int row);
    void setHoveredRows(int first, int last);

protected:
    void paint(QPainter *painter, const QStyleOptionViewItem &option,
               const QModelIndex &index) const override;
    virtual void drawText(QPainter *painter, const QStyleOptionViewItem &option,
                          const QModelIndex &index) const;

private Q_SLOTS:
    void initialize();

private: // Variables
    const RowColorMode m_rowColorMode;
    int m_alternationStep = 2;
    int m_selectedRow = -1;
    int m_firstHoveredRow = -1;
    int m_lastHoveredRow = -1;
    int m_firstVisibleColumn = -1;
    int m_lastVisibleColumn = -1;

};

#endif // TABLEDELEGATE_H
