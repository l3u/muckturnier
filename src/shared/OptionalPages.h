// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef OPTIONALPAGES_H
#define OPTIONALPAGES_H

// Qt includes
#include <QObject>
#include <QHash>

namespace OptionalPages
{

enum Page {
    DrawOverview,
    Schedule,
    Booking
};

const QHash<Page, QString> pageTitle {
    { DrawOverview, QObject::tr("Übersicht Auslosung") },
    { Schedule,     QObject::tr("Turnier-Zeitplan") },
    { Booking,      QObject::tr("Voranmeldung") }
};

const QHash<Page, QString> pageId {
    { DrawOverview, QLatin1String("draw_overview") },
    { Schedule,     QLatin1String("schedule") },
    { Booking,      QLatin1String("booking") }
};

const QHash<QString, Page> pageIdMap {
    { QLatin1String("draw_overview"), DrawOverview },
    { QLatin1String("schedule"),      Schedule },
    { QLatin1String("booking"),       Booking }
};

const QString serverPageId = QLatin1String("server");
const QString clientPageId = QLatin1String("client");
const QString wlanCodeScannerPageId = QLatin1String("wlan_code_scanner");

}

#endif // OPTIONALPAGES_H
