// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "DrawModeHelper.h"

// Qt includes
#include <QObject>

QString DrawModeHelper::description(bool autoSelectPairs, bool selectByLastRound)
{
    if (! autoSelectPairs) {
        return QObject::tr(
            "<p>Es wird keine Auslosung eingegeben. Ggf. gespeicherte Auslosungen bleiben "
            "unberücksichtigt.</p>"
            "<p>Die Paar- bzw. Tischauswahl bei der Ergebniseingabe erfolgt manuell. Es wird "
            "nichts automatisch ausgewählt.</p>"
        );
    } else {
        if (selectByLastRound) {
            return QObject::tr(
                "<p>Es wird ggf. eine Auslosung für die 1. Runde eingegeben (optional).</p>"
                "<p>In der 1. Runde erfolgt die Paar- bzw. Tischauswahl bei der Ergebniseingabe "
                "automatisch, sofern es eine Auslosung gibt, ansonsten manuell.<br/>"
                "Für alle weiteren Runden ergibt sich die Paar- bzw. Tischauswahl aus den "
                "Ergebnissen der vorhergehenden Runde: Paar 1 beibt sitzen, Paar 2 rutscht jede "
                "Runde einen Tisch weiter.</p>"
                "<p>Auslosungen für andere Runden als Runde 1 bleiben unberücksichtigt.</p>"
            );
        } else {
            return QObject::tr(
                "<p>Es wird ggf. eine Auslosung für jede Runde eingegeben (optional).</p>"
                "<p>Sofern es eine passende Auslosung gibt, erfolgt die Paar- bzw. Tischauswahl "
                "pro Runde automatisch entsprechend der Auslosung für die jeweilige Runde, "
                "ansonsten manuell.</p>"
            );
        }
    }
}
