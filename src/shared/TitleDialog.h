// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef TITLEDIALOG_H
#define TITLEDIALOG_H

// Qt includes
#include <QDialog>
#include <QDialogButtonBox>
#include <QPushButton>

// Qt classes
class QVBoxLayout;
class QLabel;

class TitleDialog : public QDialog
{
    Q_OBJECT

public:
    enum DeleteMode {
        DeleteOnClose,
        NoDelete
    };

    void setTitle(const QString &title);
    void resizeToContents();

    void addButtonBox();
    void addButtonBox(QDialogButtonBox::StandardButtons buttons);
    void setOkButtonText(const QString &text);
    bool okButtonEnabled() const;
    void setButtonText(QDialogButtonBox::StandardButton button, const QString &text);

Q_SIGNALS:
    void updateStatus(const QString &text);

public Q_SLOTS:
    void setOkButtonEnabled(bool state);

protected:
    explicit TitleDialog(QWidget *parent, DeleteMode deleteMode = DeleteMode::DeleteOnClose);
    QVBoxLayout *mainLayout() const;
    QVBoxLayout *layout() const;
    QDialogButtonBox *buttonBox() const;

protected Q_SLOTS:
    void accept() override;

private: // Variables
    QVBoxLayout *m_mainLayout;
    QVBoxLayout *m_layout;
    QLabel *m_title;
    QDialogButtonBox *m_buttonBox;

};

#endif // TITLEDIALOG_H
