// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef PHONETICSEARCHBUTTON_H
#define PHONETICSEARCHBUTTON_H

// Local includes
#include "IconButton.h"

// Local classes
class SharedObjects;

class PhoneticSearchButton : public IconButton
{
    Q_OBJECT

public:
    explicit PhoneticSearchButton(const QString &searchId, SharedObjects *sharedObjects,
                                  const QString &header = QString(), QWidget *parent = nullptr);

    static const QString &description();

};

#endif // PHONETICSEARCHBUTTON_H
