// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "Draw.h"

namespace Draw
{

int indexForPair(int pair)
{
    return pair == 1 ? 0 : 2;
}

int indexForPlayer(int pair, int player)
{
    return indexForPair(pair) + player - 1;
}

int otherPair(int pair)
{
    return pair % 2 + 1;
}

int pairForIndex(int index)
{
    return index / 2 % 2 + 1;
}

int playerForIndex(int index)
{
    return index % 2 + 1;
}

} // namespace Draw

QDebug operator<<(QDebug debug, const Draw::Seat &seat)
{
    QDebugStateSaver saver(debug);

    if (seat.isEmpty()) {
        debug.nospace() << "Seat(not set)";
    } else {
        debug.nospace() << "Seat("
                        <<   "table: "  << seat.table
                        << ", pair: "   << seat.pair
                        << ", player: " << seat.player
                        << ')';
    }

    return debug;
}
