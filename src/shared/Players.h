// SPDX-FileCopyrightText: 2023-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef PLAYERS_H
#define PLAYERS_H

// Local includes
#include "Draw.h"

// Qt includes
#include <QString>
#include <QMap>

namespace Players
{

// For the draw, we need a QMap, because we need a defined order for the checksum

struct Data
{
    int id;
    QString name;
    int number;
    int marker;
    QMap<int, Draw::Seat> draw;
    int disqualified;
    QString bookingChecksum;
};

const Data noData = {
    0,                       // id
    QString(),               // name
    0,                       // number
    0,                       // marker
    QMap<int, Draw::Seat>(), // assignment
    0,                       // disqualified
    QString()                // bookingChecksum
};

struct DisplayData
{
    QString name;
    int number;
    bool disqualified;
    int id;
};

}

#endif // PLAYERS_H
