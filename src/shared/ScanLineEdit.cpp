// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "ScanLineEdit.h"

// Qt includes
#include <QAction>
#include <QStyle>

ScanLineEdit::ScanLineEdit(QWidget *parent) : QLineEdit(parent)
{
    m_blurAction = addAction(style()->standardIcon(QStyle::SP_DialogCancelButton),
                             QLineEdit::TrailingPosition);
    m_blurAction->setToolTip(tr("Scannen verhindern (Fokus entfernen)"));
    connect(m_blurAction, &QAction::triggered, this, &QLineEdit::clearFocus);
    m_blurAction->setVisible(false);

    connect(this, &QLineEdit::textChanged, this, &ScanLineEdit::updateBlurAction);
}

bool ScanLineEdit::readyToScan() const
{
    return hasFocus() && text().isEmpty();
}

void ScanLineEdit::focusInEvent(QFocusEvent *event)
{
    QLineEdit::focusInEvent(event);
    updateBlurAction();
    Q_EMIT focusChanged(true);
}

void ScanLineEdit::focusOutEvent(QFocusEvent *event)
{
    QLineEdit::focusOutEvent(event);
    updateBlurAction();
    Q_EMIT focusChanged(false);
}

void ScanLineEdit::setShowBlurAction(bool state)
{
    m_showBlurAction = state;
    updateBlurAction();
}

void ScanLineEdit::updateBlurAction()
{
    m_blurAction->setVisible(m_showBlurAction && hasFocus() && text().isEmpty());
}
