// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "FullScreenDialog.h"

// Qt includes
#include <QAction>
#include <QScreen>
#include <QWindow>

FullScreenDialog::FullScreenDialog(QWidget *parent)
    : QDialog(parent, Qt::WindowMaximizeButtonHint | Qt::WindowCloseButtonHint)
{
    setAttribute(Qt::WA_DeleteOnClose, true);

    m_toggleFullScreen = new QAction(tr("Vollbildanzeige"));
    m_toggleFullScreen->setCheckable(true);
    connect(m_toggleFullScreen, &QAction::toggled, this, &FullScreenDialog::fullScreenToggled);

    initializeGeometry();
}

void FullScreenDialog::initializeGeometry(QScreen *targetScreen)
{
    if (targetScreen != nullptr) {
        windowHandle()->setScreen(targetScreen);
    }

    setWindowState(Qt::WindowNoState);

    const auto screenSize = screen()->availableSize();
    const auto targetSize = screenSize * 0.75;
    setGeometry(QRect(QPoint((screenSize.width() - targetSize.width()) / 2,
                             (screenSize.height() - targetSize.height()) / 2)
                      + screen()->geometry().topLeft(),
                      targetSize));

    setWindowState(Qt::WindowActive);

    // This triggers a repaint so that the dialog will be shown correctly
    hide();
    show();
}

QAction *FullScreenDialog::fullScreenAction() const
{
    return m_toggleFullScreen;
}

void FullScreenDialog::fullScreenToggled(bool checked)
{
    if (checked) {
        cacheState();
        setWindowState(Qt::WindowFullScreen);
    } else {
        setWindowState(m_cachedWindowState);
        setGeometry(m_cachedGeometry);
    }
}

void FullScreenDialog::showFullScreen(QScreen *targetScreen)
{
    m_toggleFullScreen->setChecked(false);
    initializeGeometry(targetScreen);
    m_toggleFullScreen->setChecked(true);
}

void FullScreenDialog::cacheState()
{
    m_cachedWindowState = windowState();
    m_cachedGeometry = geometry();
}
