// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef DRAWMODEMENU_H
#define DRAWMODEMENU_H

// Qt includes
#include <QMenu>

// Local classes
class TournamentSettings;

// Qt classes
class QAction;

class DrawModeMenu : public QMenu
{
    Q_OBJECT

public:
    enum TextMode {
        DrawTexts,
        SelectTexts
    };

    explicit DrawModeMenu(TextMode textMode, TournamentSettings *tournamentSettings,
                          QWidget *parent);

Q_SIGNALS:
    void drawModeChanged();

private Q_SLOTS:
    void selectMode();
    void modeSelected();

private: // Variables
    TournamentSettings *m_tournamentSettings;
    QAction *m_pairs2Rotate;
    QAction *m_drawEachRound;
    QAction *m_ignoreDraw;
    bool m_ownChange = false;

};

#endif // DRAWMODEMENU_H
