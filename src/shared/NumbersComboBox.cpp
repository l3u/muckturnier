// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "NumbersComboBox.h"

NumbersComboBox::NumbersComboBox(QWidget *parent) : QComboBox(parent)
{
    setSizeAdjustPolicy(QComboBox::AdjustToContents);
    setMinimumContentsLength(2);

    connect(this, QOverload<int>::of(&QComboBox::currentIndexChanged),
            this, [this]
            {
                Q_EMIT currentNumberChanged(currentNumber());
            });
}

int NumbersComboBox::currentNumber() const
{
    return currentData().toInt();
}

bool NumbersComboBox::setCurrentNumber(int number)
{
    const auto index = findData(number);
    if (index != -1) {
        setCurrentIndex(index);
        return true;
    } else {
        return false;
    }
}
