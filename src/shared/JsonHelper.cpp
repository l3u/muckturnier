// SPDX-FileCopyrightText: 2019-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "JsonHelper.h"
#include "Logging.h"
#include "OptionalPages.h"
#include "Json.h"
#include "DefaultValues.h"

// Qt includes
#include <QString>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QDebug>

namespace JsonHelper
{

// =================================================================================================
// CAUTION: If any of the following values is changed, the server protocol version has to be bumped!
// =================================================================================================
// vvv Starting here vvv

static const QString s_noChecksumCheck = QStringLiteral("no checksum check");

// Singles management
static const QString s_singlesManagement_enabled         = QStringLiteral("enabled");
static const QString s_singlesManagement_singleMarker    = QStringLiteral("single marker");
static const QString s_singlesManagement_markSingles     = QStringLiteral("mark singles");
static const QString s_singlesManagement_conditionString = QStringLiteral("condition string");
static const QString s_singlesManagement_condition       = QStringLiteral("condition");
static const QString s_singlesManagement_assignedMarker  = QStringLiteral("assigned marker");

// Booking
static const QString s_booking_tournamentName = QStringLiteral("tournament name");
static const QString s_booking_securityKey    = QStringLiteral("security key");

// ^^^ until here ^^^
// =================================================================================================

static const QString s_draw_consecutive            = QStringLiteral("consecutive");
static const QString s_draw_tablesLimit            = QStringLiteral("tables limit");
static const QString s_draw_randomDraw             = QStringLiteral("random draw");
static const QString s_draw_windowDraw             = QStringLiteral("window draw");
static const QString s_draw_consecutiveDraw        = QStringLiteral("consecutive draw");
static const QString s_draw_avoidTeamMateCollision = QStringLiteral("avoid team mate collision");
static const QString s_draw_avoidOpponentCollision = QStringLiteral("avoid opponent collision");
static const QString s_draw_warnTeamMateCollision  = QStringLiteral("warn team mate collision");
static const QString s_draw_warnOpponentCollision  = QStringLiteral("warn opponent collision");

static const QString s_schedule_plannedRounds = QStringLiteral("planned rounds");
static const QString s_schedule_roundDuration = QStringLiteral("round duration");
static const QString s_schedule_breakDuration = QStringLiteral("break duration");

void setNoChecksumCheck(QJsonObject &json)
{
    json.insert(s_noChecksumCheck, true);
}

bool doChecksumCheck(const QJsonObject &json)
{
    bool check = true;
    if (json.contains(s_noChecksumCheck)) {
        check = ! json.value(s_noChecksumCheck).toBool();
    }
    return check;
}

// Singles management
// ==================

QJsonObject singlesManagementToJson(const Markers::SinglesManagement &data)
{
    return QJsonObject {
        { s_singlesManagement_enabled,         data.enabled },
        { s_singlesManagement_singleMarker,    data.singleMarker },
        { s_singlesManagement_markSingles,     data.markSingles },
        { s_singlesManagement_conditionString, data.conditionString },
        { s_singlesManagement_condition,       data.condition },
        { s_singlesManagement_assignedMarker,  data.assignedMarker }
    };
}

QString singlesManagementToString(const Markers::SinglesManagement &data)
{
    return QString::fromUtf8(
        QJsonDocument(singlesManagementToJson(data)).toJson(QJsonDocument::Compact));
}

Markers::SinglesManagement parseSinglesManagement(const QString &data)
{
    return parseSinglesManagement(Json::objectFromString(data));
}

Markers::SinglesManagement parseSinglesManagement(const QJsonObject &jsonObject)
{
    Markers::SinglesManagement data;
    QJsonValue value;

    value = jsonObject.value(s_singlesManagement_enabled);
    data.enabled = value.type() == QJsonValue::Bool
        ? value.toBool() : Markers::defaultSinglesManagement.enabled;

    value = jsonObject.value(s_singlesManagement_singleMarker);
    data.singleMarker = value.type() == QJsonValue::Double
        ? value.toInt() : Markers::defaultSinglesManagement.singleMarker;

    value = jsonObject.value(s_singlesManagement_markSingles);
    data.markSingles = value.type() == QJsonValue::Bool
        ? value.toBool() : Markers::defaultSinglesManagement.markSingles;

    value = jsonObject.value(s_singlesManagement_conditionString);
    data.conditionString = value.type() == QJsonValue::String
        ? value.toString() : Markers::defaultSinglesManagement.conditionString;

    value = jsonObject.value(s_singlesManagement_condition);
    data.condition = value.type() == QJsonValue::Double
        ? value.toInt() : Markers::defaultSinglesManagement.condition;

    value = jsonObject.value(s_singlesManagement_assignedMarker);
    data.assignedMarker = value.type() == QJsonValue::Double
        ? value.toInt() : Markers::defaultSinglesManagement.assignedMarker;

    return data;
}

// Registration columns
// ====================

QString registrationColumnsToString(const QVector<RegistrationColumns::Column> &columns)
{
    return Json::serialize(registrationColumnsToJson(columns));
}

QJsonArray registrationColumnsToJson(const QVector<RegistrationColumns::Column> &columns)
{
    QJsonArray data;
    for (const RegistrationColumns::Column column : RegistrationColumns::allColumns) {
        if (columns.contains(column)) {
            data.append(RegistrationColumns::columnId.value(column));
        }
    }
    return data;
}

QVector<RegistrationColumns::Column> parseRegistrationColumns(const QString &data)
{
    if (! data.isEmpty()) {
        return parseRegistrationColumns(Json::arrayFromString(data));
    } else {
        return RegistrationColumns::defaultColumns;
    }
}

QVector<RegistrationColumns::Column> parseRegistrationColumns(const QJsonArray &jsonArray)
{
    const auto knownIds = RegistrationColumns::columnId.values();
    QVector<RegistrationColumns::Column> columns;
    for (const auto &value : jsonArray) {
        const auto id = value.toString();
        if (knownIds.contains(id)) {
            columns.append(RegistrationColumns::columnId.key(id));
        } else {
            qCWarning(MuckturnierLog) << "JsonHelper::parseRegistrationColumns:"
                                      << "Skipping unknown column" << id;
        }
    }
    return columns;
}

// Draw settings
// =============

QString drawSettingsToString(const Draw::Settings &settings)
{
    return Json::serialize(drawSettingsToJson(settings));
}

QJsonObject drawSettingsToJson(const Draw::Settings &settings)
{
    return QJsonObject {
        { s_draw_consecutive,            settings.consecutive },
        { s_draw_tablesLimit,            settings.tablesLimit },
        { s_draw_randomDraw,             settings.randomDraw },
        { s_draw_windowDraw,             settings.windowDraw },
        { s_draw_consecutiveDraw,        settings.consecutiveDraw },
        { s_draw_avoidTeamMateCollision, settings.avoidTeamMateCollision },
        { s_draw_avoidOpponentCollision, settings.avoidOpponentCollision },
        { s_draw_warnTeamMateCollision,  settings.warnTeamMateCollision },
        { s_draw_warnOpponentCollision,  settings.warnOpponentCollision }
    };
}

Draw::Settings parseDrawSettings(int version, const QString &data)
{
    return parseDrawSettings(version, Json::objectFromString(data));
}

Draw::Settings parseDrawSettings(int version, const QJsonObject &jsonObject)
{
    if (version < 1 || version > Draw::settingsVersion) {
        qCWarning(MuckturnierLog) << "JsonHelper::parseDrawSettings: Can't parse draw settings "
                                     "version" << version << "-- falling back to defaults";
        return Draw::defaultSettings;
    }

    const bool consecutive = jsonObject.value(s_draw_consecutive).toBool();
    const int tablesLimit = jsonObject.value(s_draw_tablesLimit).toInt();
    const int randomDraw = jsonObject.value(s_draw_randomDraw).toInt();
    const int windowDraw = jsonObject.value(s_draw_windowDraw).toInt();
    const int consecutiveDraw = jsonObject.value(s_draw_consecutiveDraw).toInt();

    // First set the defaults of the version 2 draw settings
    bool avoidTeamMateCollision = false;
    bool avoidOpponentCollision = false;
    bool warnTeamMateCollision = true;
    bool warnOpponentCollision = true;

    // Then parse version 2 settings if present
    if (version == 1) {
        qCDebug(MuckturnierLog) << "JsonHelper::parseDrawSettings: Parsing version 1 settings, "
                                   "added defaults for new version 2 values";
    } else {
        avoidTeamMateCollision = jsonObject.value(s_draw_avoidTeamMateCollision).toBool();
        avoidOpponentCollision = jsonObject.value(s_draw_avoidOpponentCollision).toBool();
        warnTeamMateCollision = jsonObject.value(s_draw_warnTeamMateCollision).toBool();
        warnOpponentCollision = jsonObject.value(s_draw_warnOpponentCollision).toBool();
    }

    // Do a plausibility check if we're not drawing consecutively
    if (! consecutive && (
            (tablesLimit != -1 && (randomDraw > tablesLimit || consecutiveDraw > tablesLimit))
            || ((randomDraw != -1 && consecutiveDraw != -1) && (randomDraw >= consecutiveDraw))
            || ((consecutiveDraw == randomDraw + 1) && windowDraw != -1)
    )) {
        // This should only happen if the database was changed manually
        qCWarning(MuckturnierLog) << "JsonHelper::parseDrawSettings: Parsed settings failed the "
                                     "plausibility test (corrupt data?!) -- falling back to "
                                     "defaults";
        return Draw::defaultSettings;
    }

    return Draw::Settings {
        consecutive,
        tablesLimit,
        randomDraw,
        windowDraw,
        consecutiveDraw,
        avoidTeamMateCollision,
        avoidOpponentCollision,
        warnTeamMateCollision,
        warnOpponentCollision
    };
}

// Schedule settings
// =================

QString scheduleSettingsToString(const Schedule::Settings &settings)
{
    return Json::serialize(scheduleSettingsToJson(settings));
}

QJsonObject scheduleSettingsToJson(const Schedule::Settings &settings)
{
    return QJsonObject {
        { s_schedule_plannedRounds, settings.plannedRounds },
        { s_schedule_roundDuration, settings.roundDuration },
        { s_schedule_breakDuration, settings.breakDuration }
    };
}

Schedule::Settings parseScheduleSettings(const QString &data)
{
    return parseScheduleSettings(Json::objectFromString(data));
}

Schedule::Settings parseScheduleSettings(const QJsonObject &jsonObject)
{
    const int plannedRounds = jsonObject.value(s_schedule_plannedRounds).toInt();
    const int roundDuration = jsonObject.value(s_schedule_roundDuration).toInt();
    const int breakDuration = jsonObject.value(s_schedule_breakDuration).toInt();

    // Do a plausibility check
    if (   plannedRounds < 1 || plannedRounds > DefaultValues::maximumRounds
        || roundDuration < 1 || roundDuration > DefaultValues::maximumRoundDuration
        || breakDuration < 1 || breakDuration > DefaultValues::maximumBreakDuration) {

        // This should only happen if the database was changed manually
        qCWarning(MuckturnierLog) << "Schedule settings failed the plausibility test, using "
                                     "defaults!";
        return Schedule::defaultSettings;
    }

    return Schedule::Settings {
        plannedRounds,
        roundDuration,
        breakDuration
    };
}

// Optional pages
// ==============

QString optionalPagesToString(const QVector<OptionalPages::Page> &pages)
{
    return Json::serialize(optionalPagesToJson(pages));
}

QJsonArray optionalPagesToJson(const QVector<OptionalPages::Page> &pages)
{
    QJsonArray ids;
    for (const OptionalPages::Page page : pages) {
        ids.append(OptionalPages::pageId.value(page));
    }
    return ids;
}

QVector<OptionalPages::Page> parseOptionalPages(const QString &data)
{
    return parseOptionalPages(Json::arrayFromString(data));
}

QVector<OptionalPages::Page> parseOptionalPages(const QJsonArray &json)
{
    QVector<OptionalPages::Page> pages;
    for (const QJsonValue &value : json) {
        const QString id = value.toString();
        if (OptionalPages::pageIdMap.contains(id)) {
            pages.append(OptionalPages::pageIdMap.value(id));
        }
    }
    return pages;
}

// Booking settings
// ================

Booking::Settings parseBookingSettings(const QJsonObject &jsonObject)
{
    const auto tournamentName = jsonObject.value(s_booking_tournamentName).toString();
    const auto securityKey = jsonObject.value(s_booking_securityKey).toString();

    if (   (! tournamentName.isEmpty() && securityKey.isEmpty())
        || (tournamentName.isEmpty() && ! securityKey.isEmpty())) {

        qCWarning(MuckturnierLog) << "JsonHelper::parseBookingSettings: Invalid dataset!"
                                  << "Falling back to defaults";
        return Booking::Settings();
    }

    return Booking::Settings {
        tournamentName,
        securityKey
    };
}

QJsonObject bookingSettingsToJson(const Booking::Settings &settings)
{
    return QJsonObject {
        { s_booking_tournamentName, settings.tournamentName },
        { s_booking_securityKey,    settings.securityKey }
    };
}

}
