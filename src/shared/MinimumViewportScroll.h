// SPDX-FileCopyrightText: 2020 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef MINIMUMVIEWPORTSCROLL_H
#define MINIMUMVIEWPORTSCROLL_H

// Qt includes
#include <QScrollArea>

// Qt classes
class QResizeEvent;

class MinimumViewportScroll : public QScrollArea
{
    Q_OBJECT

public:
    explicit MinimumViewportScroll(Qt::Orientation orientation, QWidget *parent = nullptr);

protected:
    void resizeEvent(QResizeEvent *event) override;

private: // Variables
    Qt::Orientation m_orientation;

};

#endif // MINIMUMVIEWPORTSCROLL_H
