// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SCORES_H
#define SCORES_H

// Local includes
#include "Players.h"

#include <QVector>
#include <QString>

namespace Scores
{

struct Score
{
    QVector<int> tables;
    QVector<Players::DisplayData> pairsOrPlayers1;
    QVector<Players::DisplayData> players2;
    QVector<QVector<int>> scores;
};

}

#endif // SCORES_H
