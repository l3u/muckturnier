// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef DRAW_H
#define DRAW_H

// Qt includes
#include <QMetaType>
#include <QDebug>

namespace Draw
{

constexpr int settingsVersion = 2;

struct Settings
{
    bool consecutive;
    int tablesLimit;
    int randomDraw;
    int windowDraw;
    int consecutiveDraw;
    bool avoidTeamMateCollision;
    bool avoidOpponentCollision;
    bool warnTeamMateCollision;
    bool warnOpponentCollision;
};

const auto defaultSettings = Settings {
    false, // consecutive
    -1,    // tablesLimit
    -1,    // randomDraw
    4,     // windowDraw
    -1,    // consecutiveDraw
    false, // avoidTeamMateCollision
    false, // avoidOpponentCollision
    true,  // warnTeamMateCollision
    true   // warnOpponentCollision
};

struct Seat
{
    int table;
    int pair;
    int player;

    bool isEmpty() const
    {
        return this->table == 0;
    }

    bool isValid() const
    {
        return this->table != 0 && this->pair != 0 && this->player != 0;
    }

    bool operator==(const Seat &other) const
    {
        return this->table == other.table && this->pair == other.pair;
    }

    bool operator!=(const Seat &other) const
    {
        return this->table != other.table || this->pair != other.pair;
    }
};

const auto EmptySeat = Seat { 0, 0, 0 };

int indexForPair(int pair);
int indexForPlayer(int pair, int player);
int otherPair(int pair);
int pairForIndex(int index);
int playerForIndex(int index);

}

QDebug operator<<(QDebug debug, const Draw::Seat &seat);

Q_DECLARE_METATYPE(Draw::Seat)

#endif // DRAW_H
