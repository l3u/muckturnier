// SPDX-FileCopyrightText: 2018-2020 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "TitleMenu.h"

// Qt includes
#include <QWidgetAction>
#include <QLabel>
#include <QResizeEvent>
#include <QApplication>

TitleMenu::TitleMenu(QWidget *parent) : QMenu(parent)
{
    setupTitle();
}

TitleMenu::TitleMenu(const QString &text, QWidget *parent) : QMenu(text, parent)
{
    setupTitle();
    setTitle(text);
}

void TitleMenu::setupTitle()
{
    m_titleAction = new QWidgetAction(this);
    m_title = new QLabel;
    m_title->setAlignment(Qt::AlignCenter);
    m_title->setMargin(4);
    m_titleAction->setDefaultWidget(m_title);
    addAction(m_titleAction);
    addSeparator();
}

void TitleMenu::setTitle(const QString &text)
{
    m_title->setText(text);
    QResizeEvent resizeEvent(m_title->size(), size());
    QApplication::sendEvent(this, &resizeEvent);
}

QString TitleMenu::title() const
{
    return m_title->text();
}

void TitleMenu::setTitleHidden(bool state)
{
    m_titleAction->setVisible(! state);
    m_title->setVisible(! state);
}
