// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef FULLSCREENDIALOG_H
#define FULLSCREENDIALOG_H

// Qt includes
#include <QDialog>

// Qt classes
class QAction;
class QScreen;

class FullScreenDialog : public QDialog
{
    Q_OBJECT

public:
    explicit FullScreenDialog(QWidget *parent);
    QAction *fullScreenAction() const;
    void initializeGeometry(QScreen *targetScreen = nullptr);

public Q_SLOTS:
    void showFullScreen(QScreen *targetScreen);

private Q_SLOTS:
    void fullScreenToggled(bool checked);

private: // Functions
    void cacheState();

private: // Variables
    QAction *m_toggleFullScreen;
    QFlags<Qt::WindowState> m_cachedWindowState;
    QRect m_cachedGeometry;

};

#endif // FULLSCREENDIALOG_H
