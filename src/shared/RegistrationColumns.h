// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef REGISTRATIONCOLUMNS_H
#define REGISTRATIONCOLUMNS_H

#include <QVector>
#include <QHash>
#include <QObject>

namespace RegistrationColumns
{

enum Column {
    Name,
    Number,
    Mark,
    Draw,
    Marker
};

const QVector<Column> allColumns {
    // Name is always shown
    Column::Number,
    Column::Mark,
    Column::Draw,
    Column::Marker
};

const QHash<Column, int> columnIndex { { Column::Name,   0 },
                                       { Column::Number, 1 },
                                       { Column::Mark,   2 },
                                       { Column::Draw,   3 },
                                       { Column::Marker, 4 } };

const QHash<Column, QString> checkBoxText {
    { Column::Number, QObject::tr("Nummer")     },
    { Column::Mark,   QObject::tr("Markieren")  },
    { Column::Draw,   QObject::tr("Auslosung")  },
    { Column::Marker, QObject::tr("Markierung") }
};

const QHash<Column, QString> columnId {
    { Column::Number, QStringLiteral("number") },
    { Column::Mark,   QStringLiteral("mark")   },
    { Column::Draw,   QStringLiteral("draw")   },
    { Column::Marker, QStringLiteral("marker") }
};

const QVector<Column> defaultColumns {
    Column::Mark,
    Column::Draw
};

}

#endif // REGISTRATIONCOLUMNS_H
