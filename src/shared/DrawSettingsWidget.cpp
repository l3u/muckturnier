// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "DrawSettingsWidget.h"
#include "DefaultValues.h"

// Qt includes
#include <QVBoxLayout>
#include <QRadioButton>
#include <QDebug>
#include <QLabel>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QCheckBox>
#include <QSpinBox>
#include <QCheckBox>
#include <QMessageBox>

// C++ includes
#include <functional>
#include <algorithm>

DrawSettingsWidget::DrawSettingsWidget(QWidget *parent) : QWidget(parent)
{
    setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

    auto *layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);

    auto *drawBox = new QGroupBox(tr("Auslosung für die 1. Runde"));
    auto *drawBoxLayout = new QVBoxLayout(drawBox);
    layout->addWidget(drawBox);

    m_consecutive = new QRadioButton(tr("Fortlaufend: Immer den ersten freien Platz vergeben"));
    drawBoxLayout->addWidget(m_consecutive);

    m_random = new QRadioButton(tr("Zufällig nach folgenden Regeln:"));
    drawBoxLayout->addWidget(m_random);

    auto *randomBox = new QGroupBox;
    auto *randomBoxWrapperLayout = new QHBoxLayout(randomBox);
    auto *randomBoxLayout = new QGridLayout;
    randomBoxWrapperLayout->addLayout(randomBoxLayout);
    randomBox->setEnabled(false);
    connect(m_random, &QRadioButton::toggled, randomBox, &QWidget::setEnabled);
    drawBoxLayout->addWidget(randomBox);

    // Random draw

    m_randomDrawSet = new QCheckBox(tr("Komplett zufällig bis incl. Tisch Nr."));

    m_randomDraw = new QSpinBox;
    m_randomDraw->setMinimum(1);
    m_randomDraw->setMaximum(DefaultValues::maximumDrawTables);
    m_randomDraw->setEnabled(false);

    connect(m_randomDrawSet, &QCheckBox::toggled,
            this, std::bind(&DrawSettingsWidget::optionStateChanged, this,
                            std::placeholders::_1, m_randomDraw));
    connect(m_randomDraw, QOverload<int>::of(&QSpinBox::valueChanged),
            this, &DrawSettingsWidget::adjustLimits);

    randomBoxLayout->addWidget(m_randomDrawSet, 1, 0);
    randomBoxLayout->addWidget(m_randomDraw,    1, 1);

    // Frame draw

    m_windowDrawSet = new QCheckBox(tr("Zufällig innerhalb eines Vergabefensters mit +"));
    m_windowDrawSet->setToolTip(tr(
        "<p>Das Vergabefenster wird durch den ersten Tisch mit einem freien Platz und die "
        "angegebene Anzahl an darauffolgenden Tischen definiert.</p>"
        "<p>Alle Plätze werden in diesem Bereich vergeben, bis der erste Tisch des Fensters voll "
        "besetzt ist. Dann rückt das Fenster zum nächsten Tisch mit einem freien Platz weiter."
        "</p>"));

    m_windowDraw = new QSpinBox;
    m_windowDraw->setMinimum(0);
    m_windowDraw->setMaximum(DefaultValues::maximumDrawTables);
    m_windowDraw->setEnabled(false);

    connect(m_windowDrawSet, &QCheckBox::toggled,
            this, std::bind(&DrawSettingsWidget::optionStateChanged, this,
                            std::placeholders::_1, m_windowDraw));

    randomBoxLayout->addWidget(m_windowDrawSet,             2, 0);
    randomBoxLayout->addWidget(m_windowDraw,                2, 1);
    randomBoxLayout->addWidget(new QLabel(tr("Tisch(en)")), 2, 2);

    // Consecutive draw

    m_consecutiveDrawSet = new QCheckBox(tr("Fortlaufende Vergabe ab Tisch Nr."));

    m_consecutiveDraw = new QSpinBox;
    m_consecutiveDraw->setMinimum(1);
    m_consecutiveDraw->setMaximum(DefaultValues::maximumDrawTables);
    m_consecutiveDraw->setEnabled(false);

    connect(m_consecutiveDrawSet, &QCheckBox::toggled,
            this, std::bind(&DrawSettingsWidget::optionStateChanged, this,
                            std::placeholders::_1, m_consecutiveDraw));
    connect(m_consecutiveDraw, QOverload<int>::of(&QSpinBox::valueChanged),
            this, &DrawSettingsWidget::adjustLimits);

    randomBoxLayout->addWidget(m_consecutiveDrawSet, 3, 0);
    randomBoxLayout->addWidget(m_consecutiveDraw,    3, 1);

    randomBoxWrapperLayout->addStretch();

    // Tables limit

    m_tablesLimitSet = new QCheckBox(tr("Maximale Tischanzahl:"));

    m_tablesLimit = new QSpinBox;
    m_tablesLimit->setMinimum(1);
    m_tablesLimit->setMaximum(DefaultValues::maximumDrawTables);
    m_tablesLimit->setEnabled(false);

    connect(m_tablesLimitSet, &QCheckBox::toggled,
            this, std::bind(&DrawSettingsWidget::optionStateChanged, this,
                            std::placeholders::_1, m_tablesLimit));
    connect(m_tablesLimit, QOverload<int>::of(&QSpinBox::valueChanged),
            this, &DrawSettingsWidget::adjustLimits);

    auto *tablesLimitLayout = new QHBoxLayout;
    drawBoxLayout->addLayout(tablesLimitLayout);
    tablesLimitLayout->addWidget(m_tablesLimitSet);
    tablesLimitLayout->addWidget(m_tablesLimit);
    tablesLimitLayout->addStretch();

    // Other rounds info

    auto *otherDrawBox = new QGroupBox(tr("Auslosung für weitere Runden"));
    auto *otherDrawBoxLayout = new QVBoxLayout(otherDrawBox);
    layout->addWidget(otherDrawBox);

    auto *otherDrawLabel = new QLabel(tr("Alle Plätze werden komplett zufällig vergeben, unter "
                                         "Berücksichtigung folgender Parameter:"));
    otherDrawLabel->setWordWrap(true);
    otherDrawBoxLayout->addWidget(otherDrawLabel);

    m_avoidTeamMateCollision = new QCheckBox(tr("Mehrfache Zulosung derselben Partner vermeiden "
                                                "(für Einzelspieler)"));
    otherDrawBoxLayout->addWidget(m_avoidTeamMateCollision);

    m_avoidOpponentCollision = new QCheckBox(tr("Mehrfache Zulosung derselben Gegner vermeiden"));
    otherDrawBoxLayout->addWidget(m_avoidOpponentCollision);

    auto *line = new QFrame;
    line->setFrameShape(QFrame::HLine);
    line->setProperty("isseparator", true);
    otherDrawBoxLayout->addWidget(line);

    m_warnTeamMateCollision = new QCheckBox(tr("Auf schon einmal zugeloste Partner hinweisen "
                                               "(für Einzelspieler)"));
    otherDrawBoxLayout->addWidget(m_warnTeamMateCollision);

    m_warnOpponentCollision = new QCheckBox(tr("Auf schon einmal zugeloste Gegner hinweisen"));
    otherDrawBoxLayout->addWidget(m_warnOpponentCollision);
}

void DrawSettingsWidget::optionStateChanged(bool state, QSpinBox *target)
{
    target->setEnabled(state);
    if (! m_adjusting) {
        adjustLimits();
    }
}

void DrawSettingsWidget::adjustLimits()
{
    if (m_updatingSettings) {
        return;
    }

    m_adjusting = true;

    const int tablesLimit = m_tablesLimitSet->isChecked() ? m_tablesLimit->value()
                                                          : DefaultValues::maximumDrawTables;
    m_consecutiveDraw->setMaximum(tablesLimit);

    if (m_consecutiveDrawSet->isChecked()) {
        if (m_consecutiveDraw->value() == 1) {
            m_randomDrawSet->setChecked(false);
            m_randomDrawSet->setEnabled(false);
        } else {
            m_randomDrawSet->setEnabled(true);
            m_randomDraw->setMaximum(m_consecutiveDraw->value() - 1);
        }
    } else {
        m_randomDraw->setMaximum(tablesLimit);
    }

    if (m_randomDrawSet->isChecked()) {
        if (m_randomDraw->value() == tablesLimit) {
            m_consecutiveDrawSet->setChecked(false);
            m_consecutiveDrawSet->setEnabled(false);
        } else {
            m_consecutiveDrawSet->setEnabled(true);
            m_consecutiveDraw->setMinimum(m_randomDraw->value() + 1);
        }
    } else {
        m_consecutiveDraw->setMinimum(1);
    }

    if (   (m_consecutiveDrawSet->isChecked() && m_consecutiveDraw->value() == 1)
        || (m_randomDrawSet->isChecked() && m_consecutiveDrawSet->isChecked()
            && m_consecutiveDraw->value() == m_randomDraw->value() + 1)) {

        m_windowDrawSet->setChecked(false);
        m_windowDrawSet->setEnabled(false);

    } else {
        m_windowDrawSet->setEnabled(true);
    }

    if (! m_tablesLimitSet->isChecked()) {
        const int random = m_randomDrawSet->isChecked() ? m_randomDraw->value() : 1;
        const int consecutive = m_consecutiveDrawSet->isChecked() ? m_consecutiveDraw->value() : 1;
        const int min = std::max(random, consecutive);
        if (m_tablesLimit->value() < min) {
            m_tablesLimit->setValue(min);
        }
    }

    m_adjusting = false;

    if (! m_consecutive->isChecked()
        && ! m_randomDrawSet->isChecked() && ! m_windowDrawSet->isChecked()) {

        QMessageBox::warning(this, tr("Zufällig Auslosung"),
            tr("Wenn weder eine komplett zufällige Auslosung, noch eine Auslosung in einem "
               "Vergabefenster definiert sind, dann erfolgt eine fortlaufende Platzvergabe."));
    }
}

void DrawSettingsWidget::updateSettings(const Draw::Settings &settings)
{
    m_updatingSettings = true;

    m_consecutive->setChecked(settings.consecutive);
    m_random->setChecked(! settings.consecutive);

    m_tablesLimit->setValue(settings.tablesLimit);
    m_tablesLimitSet->setChecked(settings.tablesLimit != -1);

    m_randomDraw->setValue(settings.randomDraw);
    m_randomDrawSet->setChecked(settings.randomDraw != -1);

    m_windowDraw->setValue(settings.windowDraw);
    m_windowDrawSet->setChecked(settings.windowDraw != -1);

    m_consecutiveDraw->setValue(settings.consecutiveDraw);
    m_consecutiveDrawSet->setChecked(settings.consecutiveDraw != -1);

    m_avoidTeamMateCollision->setChecked(settings.avoidTeamMateCollision);
    m_avoidOpponentCollision->setChecked(settings.avoidOpponentCollision);
    m_warnTeamMateCollision->setChecked(settings.warnTeamMateCollision);
    m_warnOpponentCollision->setChecked(settings.warnOpponentCollision);

    m_updatingSettings = false;
    adjustLimits();
}

Draw::Settings DrawSettingsWidget::settings() const
{
    const bool consecutive = m_consecutive->isChecked();
    const int tablesLimit = m_tablesLimitSet->isChecked() ? m_tablesLimit->value() : -1;
    const int randomDraw = m_randomDrawSet->isChecked() ? m_randomDraw->value() : -1;
    const int windowDraw = m_windowDrawSet->isChecked() ? m_windowDraw->value() : -1;
    const int consecutiveDraw = m_consecutiveDrawSet->isChecked() ? m_consecutiveDraw->value() : -1;
    const bool avoidTeamMateCollision = m_avoidTeamMateCollision->isChecked();
    const bool avoidOpponentCollision = m_avoidOpponentCollision->isChecked();
    const bool warnTeamMateCollision = m_warnTeamMateCollision->isChecked();
    const bool warnOpponentCollision = m_warnOpponentCollision->isChecked();

    return Draw::Settings {
        consecutive,
        tablesLimit,
        randomDraw,
        windowDraw,
        consecutiveDraw,
        avoidTeamMateCollision,
        avoidOpponentCollision,
        warnTeamMateCollision,
        warnOpponentCollision
    };
}
