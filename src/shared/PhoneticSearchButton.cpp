// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "PhoneticSearchButton.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/Settings.h"

// Qt includes
#include <QTimer>
#include <QDebug>

static const QString s_description = QObject::tr(
    "<p>Die phonetische Suche sucht nicht nach der eingegebenen Buchstabenfolge oder Variantionen "
    "davon, sondern nach deren Klang beim Sprechen.</p>"
    "<p>Sie ist viel Tippfehler-toleranter, liefert aber auch mehr falsche Ergebnisse.</p>");

PhoneticSearchButton::PhoneticSearchButton(const QString &searchId, SharedObjects *sharedObjects,
                                           const QString &header, QWidget *parent)
    : IconButton(sharedObjects, QStringLiteral("phonetic.png"), parent)
{
    setObjectName(searchId);
    setToolTip((header.isEmpty() ? tr("<p>Phonetische Suche (de)aktivieren</p>") : header)
               + s_description);

    Settings *settings = sharedObjects->settings();

    connect(this, &QAbstractButton::toggled, this, [this, settings]
    {
        settings->savePhoneticSearchState(objectName(), isChecked());
    });

    // We do this via a QTimer call so that slots connected to the toggled signal will be handled
    QTimer::singleShot(0, this, [this, settings]
    {
        setChecked(settings->phoneticSearchState(objectName()));
    });
}

const QString &PhoneticSearchButton::description()
{
    return s_description;
}
