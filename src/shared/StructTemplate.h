// SPDX-FileCopyrightText: 2020 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef STRUCTTEMPLATE_H
#define STRUCTTEMPLATE_H

namespace StructTemplate
{

template<typename T>
struct TSuccess
{
    T data;
    bool success;
};

}

#endif // STRUCTTEMPLATE_H
