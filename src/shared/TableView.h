// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef TABLEVIEW_H
#define TABLEVIEW_H

// Local includes
#include "shared/TableDelegate.h"

// Qt includes
#include <QTableView>

// Qt classes
class QEvent;
class QMouseEvent;

class TableView : public QTableView
{
    Q_OBJECT

public:
    explicit TableView(QWidget *parent = nullptr,
                       TableDelegate::RowColorMode rowColorMode = TableDelegate::AlternatingRows);
    void setItemDelegate(TableDelegate *delegate);
    void setTrackHovering(bool state);
    TableDelegate *tableDelegate() const;
    void setKeepHoveredRow(bool state);
    bool keepHoveredRow() const;

protected: // Functions
    void mouseMoveEvent(QMouseEvent *event) override;
    bool viewportEvent(QEvent *event) override;
    void setRowsPerDataset(int rows);
    void updateHoveredRow(const QModelIndex &index);
    int hoveredRow() const;

private: // Variables
    TableDelegate *m_tableDelegate;
    bool m_keepHoveredRow = false;
    int m_rowsPerDataset = 1;
    int m_hoveredRow = -1;
    int m_lastHoveredRow = -1;

};

#endif // TABLEVIEW_H
