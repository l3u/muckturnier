// SPDX-FileCopyrightText: 2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "ReadyToScanLabel.h"
#include "ScanLineEdit.h"

#include "shared/SharedStyles.h"

ReadyToScanLabel::ReadyToScanLabel(ScanLineEdit *scanLineEdit, QWidget *parent)
    : QLabel(parent),
      m_scanLineEdit(scanLineEdit)
{
    updateText();
    connect(m_scanLineEdit, &ScanLineEdit::focusChanged, this, &ReadyToScanLabel::updateText);
    connect(m_scanLineEdit, &QLineEdit::textChanged, this, &ReadyToScanLabel::updateText);
}

void ReadyToScanLabel::updateText()
{
    if (m_scanLineEdit->readyToScan()) {
        setText(tr("Bereit zum Scannen"));
        setStyleSheet(SharedStyles::greenText);
    } else {
        setText(tr("Nicht bereit zum Scannen"));
        setStyleSheet(QString());
    }
}
