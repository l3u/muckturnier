// SPDX-FileCopyrightText: 2019-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef JSONHELPER_H
#define JSONHELPER_H

// Local includes
#include "Markers.h"
#include "RegistrationColumns.h"
#include "Draw.h"
#include "OptionalPages.h"
#include "Schedule.h"
#include "Booking.h"

// Qt classes
class QJsonObject;
class QJsonArray;
class QString;

namespace JsonHelper
{

void setNoChecksumCheck(QJsonObject &json);
bool doChecksumCheck(const QJsonObject &json);

QString singlesManagementToString(const Markers::SinglesManagement &data);
QJsonObject singlesManagementToJson(const Markers::SinglesManagement &data);
Markers::SinglesManagement parseSinglesManagement(const QString &data);
Markers::SinglesManagement parseSinglesManagement(const QJsonObject &jsonObject);

QString registrationColumnsToString(const QVector<RegistrationColumns::Column> &columns);
QJsonArray registrationColumnsToJson(const QVector<RegistrationColumns::Column> &columns);
QVector<RegistrationColumns::Column> parseRegistrationColumns(const QString &data);
QVector<RegistrationColumns::Column> parseRegistrationColumns(const QJsonArray &jsonArray);

QString drawSettingsToString(const Draw::Settings &settings);
QJsonObject drawSettingsToJson(const Draw::Settings &settings);
Draw::Settings parseDrawSettings(int version, const QString &data);
Draw::Settings parseDrawSettings(int version, const QJsonObject &jsonObject);

QString scheduleSettingsToString(const Schedule::Settings &settings);
QJsonObject scheduleSettingsToJson(const Schedule::Settings &settings);
Schedule::Settings parseScheduleSettings(const QString &data);
Schedule::Settings parseScheduleSettings(const QJsonObject &jsonObject);

QString optionalPagesToString(const QVector<OptionalPages::Page> &pages);
QJsonArray optionalPagesToJson(const QVector<OptionalPages::Page> &pages);
QVector<OptionalPages::Page> parseOptionalPages(const QString &data);
QVector<OptionalPages::Page> parseOptionalPages(const QJsonArray &json);

Booking::Settings parseBookingSettings(const QJsonObject &jsonObject);
QJsonObject bookingSettingsToJson(const Booking::Settings &settings);

}

#endif // JSONHELPER_H
