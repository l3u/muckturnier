// SPDX-FileCopyrightText: 2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef READYTOSCANLABEL_H
#define READYTOSCANLABEL_H

// Qt includes
#include <QLabel>

// Local classes
class ScanLineEdit;

class ReadyToScanLabel : public QLabel
{
    Q_OBJECT

public:
    explicit ReadyToScanLabel(ScanLineEdit *scanLineEdit, QWidget *parent = nullptr);

public Q_SLOTS:
    void updateText();

private: // Variables
    ScanLineEdit *m_scanLineEdit;

};

#endif // READYTOSCANLABEL_H
