// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef STACKEDWIDGETLIST_H
#define STACKEDWIDGETLIST_H

// Qt includes
#include <QWidget>

// Local classes
#include "AbstractSettingsPage.h"

// Qt classes
class QListWidget;
class QStackedWidget;
class QListWidgetItem;

class StackedWidgetList : public QWidget
{
    Q_OBJECT

public:
    explicit StackedWidgetList(QWidget *parent = nullptr);
    void addWidget(QWidget *widget, const QString &title);
    void addWidget(AbstractSettingsPage *page);
    void setCurrentIndex(int index);
    void setCurrentWidget(QWidget *widget);
    int count() const;
    QWidget *widget(int index) const;
    void setNavigationVisible(bool state);
    int currentIndex() const;

private Q_SLOTS:
    void changeWidget(QListWidgetItem *item, QListWidgetItem *);

private: // Variables
    QListWidget *m_widgetsList;
    QStackedWidget *m_widgetsStack;

};

#endif // STACKEDWIDGETLIST_H
