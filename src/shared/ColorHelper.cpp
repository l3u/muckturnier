// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "ColorHelper.h"

namespace ColorHelper
{

QString colorToString(const QColor &color)
{
    return QString::number(color.rgb());
}

QColor colorFromString(const QString &string)
{
    return QColor::fromRgb(string.toUInt());
}

}
