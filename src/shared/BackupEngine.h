// SPDX-FileCopyrightText: 2019-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef BACKUPENGINE_H
#define BACKUPENGINE_H

// Qt includes
#include <QObject>
#include <QDateTime>

class BackupEngine : public QObject
{
    Q_OBJECT

public:
    explicit BackupEngine(const QString &fileName);
    void setFileName(const QString &fileName);
    bool createBackup();
    bool createBackup(const QString &suffix);
    const QString &path() const;
    const QString &backupFile() const;
    QString messageDate() const;
    const QString &baseName() const;
    const QString &originalBaseName() const;
    QString originalFileName() const;

private: // Variables
    QString m_originalFile;
    QString m_path;
    QString m_baseName;
    QString m_originalBaseName;
    QDateTime m_backupTime;
    QString m_backupFile;

};

#endif // BACKUPENGINE_H
