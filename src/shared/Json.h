// SPDX-FileCopyrightText: 2023-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef JSON_H
#define JSON_H

// Qt classes
class QJsonObject;
class QJsonArray;
class QString;
class QByteArray;

namespace Json
{

QString serialize(const QJsonObject &json);
QString serialize(const QJsonArray &json);
QJsonObject objectFromString(const QString &data);
QJsonArray arrayFromString(const QString &data);
QJsonObject objectFromByteArray(const QByteArray &data);

}

#endif // JSONHELPER_H
