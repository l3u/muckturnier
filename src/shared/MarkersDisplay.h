// SPDX-FileCopyrightText: 2022-2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef MARKERSDISPLAY_H
#define MARKERSDISPLAY_H

// Local includes
#include "shared/Markers.h"
#include "shared/Tournament.h"

// Qt includes
#include <QWidget>

// Qt classes
class QTableWidget;
class QTableWidgetItem;
class QLabel;
class QGroupBox;

class MarkersDisplay : public QWidget
{
    Q_OBJECT

public:
    explicit MarkersDisplay(Tournament::Mode tournamentMode,
                            const QVector<Markers::Marker> *markersTemplate,
                            const QHash<Markers::Type, int> *specialMarkers,
                            const Markers::SinglesManagement *singlesManagement,
                            QWidget *parent = nullptr);
    void setTournamentMode(Tournament::Mode mode);
    void setMarkersTemplate(const QVector<Markers::Marker> *markersTemplate);
    void refresh();

private: // Functions
    const Markers::Marker &findMarker(int id) const;
    QString markerName(int id) const;

private: // Variables
    Tournament::Mode m_tournamentMode;
    const QVector<Markers::Marker> *m_markersTemplate;
    const QHash<Markers::Type, int> *m_specialMarkers;
    const Markers::SinglesManagement *m_singlesManagement;

    QTableWidget *m_allMarkersList;
    QTableWidget *m_specialMarkersList;
    QLabel *m_noProcessSingles;
    QGroupBox *m_processSingles;
    QLabel *m_singlesMarker;
    QLabel *m_automaticMarking;
    QLabel *m_assignedMarker;

};

#endif // MARKERSDISPLAY_H
