// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "Logging.h"
#include "debugMode.h"

#ifdef DEBUG_MODE
// Enable all messages in debug mode
Q_LOGGING_CATEGORY(MuckturnierLog, "Muckturnier")
#else
// Only enable warnings otherwise
Q_LOGGING_CATEGORY(MuckturnierLog, "Muckturnier", QtWarningMsg)
#endif
