// SPDX-FileCopyrightText: 2020 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef URLS_H
#define URLS_H

// Qt includes
#include <QLatin1String>

namespace Urls
{

const QLatin1String homepage  ("https://muckturnier.org/");
const QLatin1String download  ("https://muckturnier.org/download/");
const QLatin1String readme    ("https://muckturnier.org/readme/");
const QLatin1String bugtracker("https://gitlab.com/l3u/muckturnier/-/issues");

}

#endif // URLS_H
