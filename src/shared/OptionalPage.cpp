// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "OptionalPage.h"

// Qt includes
#include <QVBoxLayout>

OptionalPage::OptionalPage(QWidget *parent) : QWidget(parent)
{
    m_layout = new QVBoxLayout(this);
}

QVBoxLayout *OptionalPage::layout() const
{
    return m_layout;
}

bool OptionalPage::closePage()
{
    Q_EMIT pageClosed();
    parent()->deleteLater();
    return true;
}
