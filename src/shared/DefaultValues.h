// SPDX-FileCopyrightText: 2010-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef DEFAULTVALUES_H
#define DEFAULTVALUES_H

// Qt includes
#include <QString>

namespace DefaultValues
{

constexpr int boogerScore     = 21;
constexpr int boogersPerRound = 2;

const QString boogerSymbol       = QStringLiteral("\u25CF");
const QString namesSeparator     = QStringLiteral(" / ");
const QString dbFileNameTemplate = QStringLiteral("'Muckturnier' yyyy");

constexpr int maximumBoogerScore     = 9999;
constexpr int maximumBoogersPerRound = 9999;
constexpr int maximumDrawRounds      = 99;
constexpr int maximumDrawTables      = 9999;
constexpr int drawTableWarning       = 300;
constexpr int maximumPlayersNumber   = 9999;
constexpr int maximumNotesTables     = 999;
constexpr int maximumNotesRounds     = 99;

constexpr int importSizeWarning       = 9600;
constexpr int importLineLengthWarning = 80;
constexpr int importLinesCountWarning = 300;

constexpr int serverPort     = 8810;
constexpr int connectTimeout = 6000;
constexpr int requestTimeout = 3000;

constexpr int discoverServerPort = 8811;
constexpr int discoverTimeout    = 500;
constexpr int discoverAttempts   = 5;

constexpr int wlanCodeScannerServerPort = 8812;
const QString wlanCodeScannerServerKey = QStringLiteral("content");
constexpr int maximumHttpFragmentSize = 8192;

constexpr int plannedRounds        = 5;
constexpr int maximumRounds        = 9999;
constexpr int roundDuration        = 40;
constexpr int maximumRoundDuration = 9999;
constexpr int breakDuration        = 5;
constexpr int maximumBreakDuration = 9999;

}

#endif // DEFAULTVALUES_H
