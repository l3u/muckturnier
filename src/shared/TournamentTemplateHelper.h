// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef TOURNAMENTTEMPLATEHELPER_H
#define TOURNAMENTTEMPLATEHELPER_H

// Local includes
#include "TournamentTemplate.h"

// Local classes
class TournamentSettings;

namespace TournamentTemplateHelper
{

TournamentTemplate::Settings assembleTemplate(TournamentSettings *tournamentSettings);

}

#endif // TOURNAMENTTEMPLATEHELPER_H
