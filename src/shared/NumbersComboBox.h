// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef NUMBERSCOMBOBOX_H
#define NUMBERSCOMBOBOX_H

// Qt includes
#include <QComboBox>

class NumbersComboBox : public QComboBox
{
    Q_OBJECT

public:
    explicit NumbersComboBox(QWidget *parent = nullptr);
    int currentNumber() const;
    bool setCurrentNumber(int number);

Q_SIGNALS:
    void currentNumberChanged(int number);

};

#endif // NUMBERSCOMBOBOX_H
