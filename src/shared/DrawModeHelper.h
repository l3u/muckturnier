// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef DRAWMODEHELPER_H
#define DRAWMODEHELPER_H

// Qt includes
#include <QString>

namespace DrawModeHelper
{

QString description(bool autoSelectPairs, bool selectByLastRound);

}

#endif // DRAWMODEHELPER_H
