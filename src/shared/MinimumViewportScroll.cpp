// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "MinimumViewportScroll.h"
#include "MinimumViewportWidget.h"

// Qt includes
#include <QResizeEvent>
#include <QDebug>
#include <QScrollBar>
#include <QLayout>

static constexpr int s_scrollbarSpace = 4;

MinimumViewportScroll::MinimumViewportScroll(Qt::Orientation orientation, QWidget *parent)
    : QScrollArea(parent),
      m_orientation(orientation)
{
}

void MinimumViewportScroll::resizeEvent(QResizeEvent *event)
{
    QScrollArea::resizeEvent(event);

    if (widget() == nullptr) {
        return;
    }

    bool scrollBarVisible;
    auto *setWidget = qobject_cast<MinimumViewportWidget *>(widget());

    switch (m_orientation) {

    case Qt::Horizontal:
        scrollBarVisible = verticalScrollBar()->isVisible();
        setWidget->layout()->setContentsMargins(0, 0, scrollBarVisible ? s_scrollbarSpace : 0, 0);
        setMinimumWidth(setWidget->minimumViewport().width()
                        + (scrollBarVisible ? verticalScrollBar()->width() : 0));
        break;

    case Qt::Vertical:
        scrollBarVisible = horizontalScrollBar()->isVisible();
        setWidget->layout()->setContentsMargins(0, 0, 0, scrollBarVisible ? s_scrollbarSpace : 0);
        setMinimumHeight(setWidget->minimumViewport().height()
                        + (scrollBarVisible ? horizontalScrollBar()->height() : 0));
        break;
    }
}
