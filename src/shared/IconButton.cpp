// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "Application.h"

#include "IconButton.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/ResourceFinder.h"
#include "SharedObjects/IconSizeEngine.h"

IconButton::IconButton(SharedObjects *sharedObjects, const QString &icon, QWidget *parent)
    : QToolButton(parent),
      m_resourceFinder(sharedObjects->resourceFinder()),
      m_icon(icon)
{
    setCheckable(true);
    setStyleSheet(QStringLiteral("QToolButton { padding: 2px; }"));
    setIconSize(sharedObjects->iconSizeEngine()->relativeSize(1.0));
    initializeIcon();
    connect(qobject_cast<Application *>(qApp), &Application::paletteChanged,
            this, &IconButton::initializeIcon);
}

void IconButton::initializeIcon()
{
    setIcon(QIcon(m_resourceFinder->find(m_icon)));
}
