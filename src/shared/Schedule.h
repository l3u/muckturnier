// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SCHEDULE_H
#define SCHEDULE_H

#include "DefaultValues.h"

namespace Schedule
{

constexpr int settingsVersion = 1;

struct Settings
{
    int plannedRounds;
    int roundDuration;
    int breakDuration;
};

const Settings defaultSettings {
    DefaultValues::plannedRounds,
    DefaultValues::roundDuration,
    DefaultValues::breakDuration
};

}

#endif // SCHEDULE_H
