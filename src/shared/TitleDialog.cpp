// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "TitleDialog.h"

// Qt includes
#include <QVBoxLayout>
#include <QLabel>
#include <QApplication>
#include <QTimer>

TitleDialog::TitleDialog(QWidget *parent, DeleteMode deleteMode)
    : QDialog(parent)
{
    setAttribute(Qt::WA_DeleteOnClose, deleteMode == DeleteMode::DeleteOnClose);

    m_mainLayout = new QVBoxLayout;
    setLayout(m_mainLayout);

    m_title = new QLabel;
    m_title->setAlignment(Qt::AlignHCenter);
    m_title->setStyleSheet(QStringLiteral("QLabel { font-weight: bold; font-size: %1pt }").arg(
        (int) double(m_title->font().pointSize()) * 1.2));
    m_title->hide();
    m_mainLayout->addWidget(m_title);

    m_title->setSizePolicy(QSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed));

    m_layout = new QVBoxLayout;
    m_mainLayout->addLayout(m_layout);
}

QVBoxLayout *TitleDialog::layout() const
{
    return m_layout;
}

QVBoxLayout *TitleDialog::mainLayout() const
{
    return m_mainLayout;
}

void TitleDialog::setTitle(const QString &title)
{
    setWindowTitle(tr("Muckturnier: %1").arg(title));
    m_title->setText(title);
    m_title->show();
}

void TitleDialog::resizeToContents()
{
    QApplication::processEvents();
    QTimer::singleShot(0, this, &QWidget::adjustSize);
}

void TitleDialog::addButtonBox()
{
    m_buttonBox = new QDialogButtonBox;
    mainLayout()->addWidget(m_buttonBox);
}

void TitleDialog::addButtonBox(QDialogButtonBox::StandardButtons buttons)
{
    m_buttonBox = new QDialogButtonBox(buttons);
    mainLayout()->addWidget(m_buttonBox);
    connect(m_buttonBox, &QDialogButtonBox::accepted, this, &TitleDialog::accept);
    connect(m_buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);
}

void TitleDialog::accept()
{
    QDialog::accept();
}

void TitleDialog::setOkButtonText(const QString &text)
{
    setButtonText(QDialogButtonBox::Ok, text);
}

void TitleDialog::setOkButtonEnabled(bool state)
{
    m_buttonBox->button(QDialogButtonBox::Ok)->setEnabled(state);
}
void TitleDialog::setButtonText(QDialogButtonBox::StandardButton button, const QString &text)
{
    m_buttonBox->button(button)->setText(text);
}

bool TitleDialog::okButtonEnabled() const
{
    return m_buttonBox->button(QDialogButtonBox::Ok)->isEnabled();
}

QDialogButtonBox *TitleDialog::buttonBox() const
{
    return m_buttonBox;
}
