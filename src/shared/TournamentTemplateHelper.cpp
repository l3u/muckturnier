// SPDX-FileCopyrightText: 2023-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "TournamentTemplateHelper.h"

#include "SharedObjects/TournamentSettings.h"

namespace TournamentTemplateHelper
{

TournamentTemplate::Settings assembleTemplate(TournamentSettings *tournamentSettings)
{
    return TournamentTemplate::Settings {
        tournamentSettings->tournamentMode(),
        tournamentSettings->scoreType(),
        tournamentSettings->boogerScore(),
        tournamentSettings->boogersPerRound(),
        tournamentSettings->registrationColumns(),
        tournamentSettings->registrationListHeaderHidden(),
        tournamentSettings->autoSelectPairs(),
        tournamentSettings->selectByLastRound(),
        tournamentSettings->autoSelectTableForDraw(),
        tournamentSettings->includeOpponentGoals(),
        tournamentSettings->grayOutUnimportantGoals(),
        tournamentSettings->optionalPages(),
        tournamentSettings->drawSettings(),
        tournamentSettings->scheduleSettings()
    };
}

}
