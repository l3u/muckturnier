// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SELECTIONPOPUP_H
#define SELECTIONPOPUP_H

// Qt includes
#include <QDialog>

// Qt classes
class QScrollArea;

class SelectionPopup : public QDialog
{
    Q_OBJECT

public:
    explicit SelectionPopup(QWidget *parent);
    void show(QPoint position = QPoint(-1, -1));

Q_SIGNALS:
    void aboutToShow();

protected:
    void setScrollWidget(QWidget *widget);
    QPushButton *textButton(const QString &text);

private Q_SLOTS:
    void initializeCss();
    void checkForScrollBar();

private: // Variables
    QScrollArea *m_scrollArea;

};

#endif // SELECTIONPOPUP_H
