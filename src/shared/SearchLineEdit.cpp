// SPDX-FileCopyrightText: 2010-2020 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "SearchLineEdit.h"

// Qt includes
#include <QKeyEvent>

SearchLineEdit::SearchLineEdit(QWidget *parent) : QLineEdit(parent)
{
}

void SearchLineEdit::keyPressEvent(QKeyEvent *event)
{
    QLineEdit::keyPressEvent(event);
    if (event->key() == Qt::Key_Escape) {
        clear();
    }
}
