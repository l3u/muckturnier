// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "AbstractSettingsPage.h"

// Qt includes
#include <QVBoxLayout>
#include <QGroupBox>
#include <QLabel>

AbstractSettingsPage::AbstractSettingsPage(const QString &pageTitle, QWidget *parent)
    : QWidget(parent),
      m_pageTitle(pageTitle)
{
    QVBoxLayout *mainLayout = new QVBoxLayout(this);
    mainLayout->setContentsMargins(0, 0, 0, 0);
    QGroupBox *groupBox = new QGroupBox(m_pageTitle);
    m_layout = new QVBoxLayout(groupBox);
    mainLayout->addWidget(groupBox);
}

QString AbstractSettingsPage::pageTitle() const
{
    return m_pageTitle;
}

QVBoxLayout *AbstractSettingsPage::layout() const
{
    return m_layout;
}

QLabel *AbstractSettingsPage::descriptionLabel(const QString &text) const
{
    QLabel *label = new QLabel(text);
    label->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
    label->setAlignment(Qt::AlignTop);
    return label;
}
