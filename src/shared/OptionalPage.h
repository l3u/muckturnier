// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef OPTIONALPAGE_H
#define OPTIONALPAGE_H

// Qt includes
#include <QWidget>

// Qt classes
class QVBoxLayout;

class OptionalPage : public QWidget
{
    Q_OBJECT

Q_SIGNALS:
    void pageClosed();

public Q_SLOTS:
    virtual bool closePage();

protected:
    explicit OptionalPage(QWidget *parent = nullptr);
    QVBoxLayout *layout() const;

private: // Variables
    QVBoxLayout *m_layout;

};

#endif // OPTIONALPAGE_H
