// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "TableView.h"

// Qt includes
#include <QEvent>
#include <QMouseEvent>
#include <QHeaderView>

TableView::TableView(QWidget *parent, TableDelegate::RowColorMode rowColorMode) : QTableView(parent)
{
    setItemDelegate(new TableDelegate(this, rowColorMode));

    setShowGrid(false);
    setWordWrap(false);
    setSelectionMode(QTableView::NoSelection);
    setFocusPolicy(Qt::NoFocus);

    verticalHeader()->hide();
    horizontalHeader()->setSectionsClickable(false);
}

void TableView::setItemDelegate(TableDelegate *delegate)
{
    m_tableDelegate = delegate;
    QTableView::setItemDelegate(delegate);
}

void TableView::setTrackHovering(bool state)
{
    setMouseTracking(state);
    if (! state) {
        updateHoveredRow(QModelIndex());
    }
}

void TableView::setKeepHoveredRow(bool state)
{
    m_keepHoveredRow = state;
    if (! state) {
        updateHoveredRow(QModelIndex());
    }
}

bool TableView::keepHoveredRow() const
{
    return m_keepHoveredRow;
}

void TableView::mouseMoveEvent(QMouseEvent *event)
{
    QTableView::mouseMoveEvent(event);
    updateHoveredRow(indexAt(event->pos()));
}

bool TableView::viewportEvent(QEvent *event)
{
    // We can't use QWidget::leaveEvent, because this one is not called when the mouse cursor hovers
    // the header. This function only handles the actual viewport itself, without the header.
    const auto result = QTableView::viewportEvent(event);
    if (event->type() == QEvent::Leave) {
        updateHoveredRow(QModelIndex());
    }
    return result;
}

TableDelegate *TableView::tableDelegate() const
{
    return m_tableDelegate;
}

void TableView::setRowsPerDataset(int rows)
{
    m_rowsPerDataset = rows;
}

void TableView::updateHoveredRow(const QModelIndex &index)
{
    if (m_keepHoveredRow || ! hasMouseTracking()) {
        return;
    }

    const auto row = index.isValid() ? (index.row() / m_rowsPerDataset) * m_rowsPerDataset
                                     : -1;

    if (row == m_hoveredRow) {
        return;
    }

    m_lastHoveredRow = m_hoveredRow;
    m_hoveredRow = row;

    if (row == -1) {
        m_tableDelegate->setHoveredRows(-1, -1);
    } else {
        m_tableDelegate->setHoveredRows(m_hoveredRow, m_hoveredRow + m_rowsPerDataset - 1);
    }

    if (m_lastHoveredRow != -1) {
        for (int row = m_lastHoveredRow; row < m_lastHoveredRow + m_rowsPerDataset; row++) {
            for (int column = 0; column < model()->columnCount(); column++) {
                update(model()->index(row, column));
            }
        }
    }

    if (m_hoveredRow != -1) {
        for (int row = m_hoveredRow; row < m_hoveredRow + m_rowsPerDataset; row++) {
            for (int column = 0; column < model()->columnCount(); column++) {
                update(model()->index(row, column));
            }
        }
    }
}

int TableView::hoveredRow() const
{
    return m_hoveredRow;
}
