// SPDX-FileCopyrightText: 2023-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "Json.h"

// Qt includes
#include <QString>
#include <QByteArray>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QDebug>

namespace Json
{

QString serialize(const QJsonObject &json)
{
    return QString::fromUtf8(QJsonDocument(json).toJson(QJsonDocument::Compact));
}

QString serialize(const QJsonArray &json)
{
    return QString::fromUtf8(QJsonDocument(json).toJson(QJsonDocument::Compact));
}

QJsonObject objectFromString(const QString &data)
{
    return QJsonDocument::fromJson(data.toUtf8()).object();
}

QJsonObject objectFromByteArray(const QByteArray &data)
{
    return QJsonDocument::fromJson(data).object();
}

QJsonArray arrayFromString(const QString &data)
{
    return QJsonDocument::fromJson(data.toUtf8()).array();
}

}
