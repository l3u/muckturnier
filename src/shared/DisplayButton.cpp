// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "DisplayButton.h"

// Qt includes
#include <QHBoxLayout>
#include <QPushButton>

DisplayButton::DisplayButton(const QString &text, QMenu *menu, QWidget *parent) : QWidget(parent)
{
    auto *layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);

    m_show = new QPushButton(tr("%1 anzeigen").arg(text));
    connect(m_show, &QPushButton::clicked, this, &DisplayButton::requestDisplay);
    layout->addWidget(m_show);

    m_remote = new QPushButton(text);
    m_remote->setMenu(menu);
    m_remote->hide();
    layout->addWidget(m_remote);
}

void DisplayButton::requestDisplay()
{
    m_show->hide();
    m_remote->show();
    Q_EMIT displayRequested();
}

void DisplayButton::displayClosed()
{
    m_show->show();
    m_remote->hide();
}
