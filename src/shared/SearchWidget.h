// SPDX-FileCopyrightText: 2010-2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SEARCHWIDGET_H
#define SEARCHWIDGET_H

// Qt includes
#include <QWidget>

// Local classes;
class SharedObjects;
class SearchLineEdit;
class PhoneticSearchButton;

// Qt classes
class QLabel;

class SearchWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SearchWidget(const QString &searchId, SharedObjects *sharedObjects,
                          QWidget *parent = nullptr);
    void updateMatches(int matches, int hiddenMatches = 0);
    void saveSearch();
    void restoreSearch();
    bool phoneticSearch() const;
    QString searchText() const;
    void setSearchText(const QString &text);
    QString matchesText() const;

Q_SIGNALS:
    void searchChanged(const QString &text);

public Q_SLOTS:
    void clear();

private Q_SLOTS:
    void processInput(const QString &text);

private: // Variables
    QLabel *m_searchLabel;
    SearchLineEdit *m_search;
    QLabel *m_matches;
    QString m_savedSearch;
    PhoneticSearchButton *m_phoneticSearch;

};

#endif // SEARCHWIDGET_H
