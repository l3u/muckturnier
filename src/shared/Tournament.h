// SPDX-FileCopyrightText: 2019-2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef TOURNAMENT_H
#define TOURNAMENT_H

#include <QString>
#include <QHash>

namespace Tournament
{

// Tournament Mode

enum Mode {
    FixedPairs,
    SinglePlayers
};

const QLatin1String fixedPairsMode   ("fixed pairs");
const QLatin1String singlePlayersMode("single players");
const QHash<Mode, QString> modeMap {
    { Mode::FixedPairs,    fixedPairsMode },
    { Mode::SinglePlayers, singlePlayersMode }
};

// Score type

enum ScoreType {
    HorizontalScore,
    VerticalScore
};

const QLatin1String horizontalScoreType("horizontal");
const QLatin1String verticalScoreType  ("vertical");
const QHash<ScoreType, QString> scoreTypeMap {
    { ScoreType::HorizontalScore, horizontalScoreType },
    { ScoreType::VerticalScore,   verticalScoreType }
};

}

#endif // TOURNAMENT_H
