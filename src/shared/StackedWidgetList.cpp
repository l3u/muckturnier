// SPDX-FileCopyrightText: 2020 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "StackedWidgetList.h"
#include "AbstractSettingsPage.h"

// Qt includes
#include <QDebug>
#include <QHBoxLayout>
#include <QListWidget>
#include <QStackedWidget>

StackedWidgetList::StackedWidgetList(QWidget *parent) : QWidget(parent)
{
    QHBoxLayout *layout = new QHBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);

    m_widgetsList = new QListWidget;
    connect(m_widgetsList, &QListWidget::currentItemChanged,
            this, &StackedWidgetList::changeWidget);
    layout->addWidget(m_widgetsList);

    m_widgetsStack = new QStackedWidget;
    layout->addWidget(m_widgetsStack);
}

void StackedWidgetList::addWidget(QWidget *widget, const QString &title)
{
    m_widgetsList->addItem(title);
    m_widgetsStack->addWidget(widget);

    const int width = m_widgetsList->sizeHintForColumn(0) + 4;
    m_widgetsList->setMinimumWidth(width);
    m_widgetsList->setMaximumWidth(width);
}

void StackedWidgetList::addWidget(AbstractSettingsPage *page)
{
    addWidget(page, page->pageTitle());
}

void StackedWidgetList::setCurrentIndex(int index)
{
    m_widgetsList->setCurrentRow(index);
    m_widgetsList->setFocus();
}

void StackedWidgetList::setCurrentWidget(QWidget *widget)
{
    m_widgetsList->setCurrentRow(m_widgetsStack->indexOf(widget));
    m_widgetsList->setFocus();
}

int StackedWidgetList::count() const
{
    return m_widgetsStack->count();
}

QWidget *StackedWidgetList::widget(int index) const
{
    return m_widgetsStack->widget(index);
}

void StackedWidgetList::changeWidget(QListWidgetItem *item, QListWidgetItem *)
{
    m_widgetsStack->setCurrentIndex(m_widgetsList->row(item));
}

void StackedWidgetList::setNavigationVisible(bool state)
{
    m_widgetsList->setVisible(state);
}

int StackedWidgetList::currentIndex() const
{
    return m_widgetsStack->currentIndex();
}
