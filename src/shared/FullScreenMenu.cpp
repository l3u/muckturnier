// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "FullScreenMenu.h"

// Qt includes
#include <QApplication>
#include <QMessageBox>
#include <QScreen>

// C++ includes
#include <functional>

FullScreenMenu::FullScreenMenu(QWidget *parent) : QMenu(tr("Vollbildanzeige auf …"), parent)
{
    connect(this, &QMenu::aboutToShow, this, &FullScreenMenu::updateFullScreenMenu);
}

void FullScreenMenu::updateFullScreenMenu()
{
    clear();
    const auto screens = qApp->screens();
    int index = 0;
    for (auto *screen : screens) {
        const auto name = screen->name();
        auto *action = addAction(name);
        connect(action, &QAction::triggered,
                this, std::bind(&FullScreenMenu::requestFullScreen, this, name, index));
        index++;
    }
}

void FullScreenMenu::requestFullScreen(const QString &name, int screenIndex)
{
    const auto screens = qApp->screens();
    if (screenIndex > screens.size() || name != screens.at(screenIndex)->name()) {
        QMessageBox::warning(qobject_cast<QWidget *>(parent()), tr("Vollbildanzeige"),
            tr("Bildschirm „%1“ wurde nicht gefunden!").arg(name));
        return;
    }

    Q_EMIT fullScreenRequested(screens.at(screenIndex));
}
