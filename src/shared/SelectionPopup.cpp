// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "Application.h"

#include "SelectionPopup.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QCursor>
#include <QScrollArea>
#include <QScrollBar>
#include <QApplication>
#include <QTimer>
#include <QScreen>
#include <QPushButton>
#include <QFontMetrics>

SelectionPopup::SelectionPopup(QWidget *parent) : QDialog(parent)
{
    setWindowFlags(Qt::FramelessWindowHint | Qt::Popup);

    initializeCss();
    connect(qobject_cast<Application *>(qApp), &Application::paletteChanged,
            this, &SelectionPopup::initializeCss);

    auto *layout = new QVBoxLayout(this);
    layout->setSpacing(0);
    layout->setContentsMargins(1, 1, 1, 1); // We need this for the QDialog's gray border

    m_scrollArea = new QScrollArea(this);
    m_scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    layout->addWidget(m_scrollArea);
}

void SelectionPopup::initializeCss()
{
    setStyleSheet(QStringLiteral(
        "QDialog { border: 1px solid %1; } "
        "QScrollArea { border: none; } "
        "QPushButton { border: none; border-radius: 4px; } "
        "QPushButton:hover { border: 1px solid palette(highlight); } "
    ).arg(qApp->property("DARK_MODE").toBool() ? QStringLiteral("palette(light)")
                                               : QStringLiteral("palette(mid)")));
}

QPushButton *SelectionPopup::textButton(const QString &text)
{
    // For watever reason, as soon as we apply the above style sheet, the height of a QPushButton
    // is reduced to the minimum that can display the text. Adding a padding clips the text.
    // We fix this here by setting a minimum size that depends on the text height:
    auto *button = new QPushButton(text);
    const QFontMetrics fontMetrics(button->font());
    const auto height = fontMetrics.height() * 0.3;
    const auto sizeHint = button->sizeHint();
    button->setMinimumHeight(sizeHint.height() + height);
    button->setMinimumWidth(sizeHint.width() + height);
    return button;
}

void SelectionPopup::setScrollWidget(QWidget *widget)
{
    widget->setStyleSheet(QStringLiteral("QWidget { background-color: palette(base); }"));
    m_scrollArea->setWidget(widget);
}

void SelectionPopup::show(QPoint position)
{
    Q_EMIT aboutToShow();

    if (position == QPoint(-1, -1)) {
        position = QCursor::pos();
    }

    const QRect availableGeometry = QGuiApplication::primaryScreen()->availableGeometry();
    const int availableHeight = availableGeometry.height() - position.y();
    const int availableWidth = availableGeometry.width() - position.x();

    const QSize widgetSize = m_scrollArea->widget()->size();
    const int widgetHeight = widgetSize.height() + 2; // Two pixels for the border
    const int widgetWidth = widgetSize.width(); // Who appararenly aren't necessary here

    if (availableWidth < widgetWidth) {
        if (availableGeometry.width() >= widgetWidth) {
            position.setX(position.x() - (widgetWidth - availableWidth));
        } else {
            position.setX(0);
        }
    }

    int targetHeight = -1;

    if (widgetHeight <= availableHeight) {
        targetHeight = widgetHeight;
    } else {
        if (availableGeometry.height() >= widgetHeight) {
            position.setY(position.y() - (widgetHeight - availableHeight));
            targetHeight = widgetHeight;
        } else {
            position.setY(0);
            targetHeight = availableGeometry.height();
        }
    }

    resize(m_scrollArea->widget()->width(), targetHeight);
    move(position);

    QDialog::show();
    QTimer::singleShot(0, this, &SelectionPopup::checkForScrollBar);
}

void SelectionPopup::checkForScrollBar()
{
    if (m_scrollArea->verticalScrollBar()->isVisible()) {
        resize(m_scrollArea->widget()->width() + m_scrollArea->verticalScrollBar()->width(),
               height());
    } else {
        resize(m_scrollArea->widget()->width(), height());
    }
}
