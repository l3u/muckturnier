// SPDX-FileCopyrightText: 2018-2020 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef TITLEMENU_H
#define TITLEMENU_H

// Qt includes
#include <QMenu>

// Qt classes
class QWidgetAction;
class QLabel;

class TitleMenu : public QMenu
{
    Q_OBJECT

public:
    explicit TitleMenu(QWidget *parent = nullptr);
    explicit TitleMenu(const QString &text, QWidget *parent = nullptr);
    void setTitle(const QString &text);
    QString title() const;
    void setTitleHidden(bool state);

private: // Functions
    void setupTitle();

private: // Variables
    QWidgetAction *m_titleAction;
    QLabel *m_title;

};

#endif // TITLEMENU_H
