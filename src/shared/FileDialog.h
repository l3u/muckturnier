// SPDX-FileCopyrightText: 2018-2022 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef FILEDIALOG_H
#define FILEDIALOG_H

// Local includes
#include "TitleDialog.h"

// Qt includes
#include <QDialogButtonBox>

// Local classes
class Database;

class FileDialog : public TitleDialog
{
    Q_OBJECT

public:
    QString getTargetFileName(const QString &directory);
    void setTargetFileBaseName(const QString &targetFileBaseName);
    QString targetFileBaseName() const;
    void setTargetFileExtension(const QString &targetFileExtension);
    void setTargetFileDescription(const QString &targetFileDescription);
    void setFileDialogTitle(const QString &fileDialogTitle);
    void successMessage(const QString &title, const QString &message);
    void openFile(const QString &fileName);

protected:
    explicit FileDialog(QWidget *parent, Database *db,
                        DeleteMode deleteMode = DeleteMode::DeleteOnClose);
    Database *m_db;

private: // Variables
    QString m_targetFileBaseName;
    QString m_targetFileExtension;
    QString m_targetFileDescription;
    QString m_targetFileWildcards;
    QString m_fileDialogTitle;

};

#endif // FILEDIALOG_H
