// SPDX-FileCopyrightText: 2022-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "MarkersDisplay.h"

#include "shared/TableDelegate.h"
#include "shared/SharedStyles.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QLabel>
#include <QTableWidget>
#include <QHeaderView>
#include <QTableWidgetItem>
#include <QGroupBox>

// C++ includes
#include <algorithm>

MarkersDisplay::MarkersDisplay(Tournament::Mode tournamentMode,
                               const QVector<Markers::Marker> *markersTemplate,
                               const QHash<Markers::Type, int> *specialMarkers,
                               const Markers::SinglesManagement *singlesManagement,
                               QWidget *parent)
    : QWidget(parent),
      m_tournamentMode(tournamentMode),
      m_markersTemplate(markersTemplate),
      m_specialMarkers(specialMarkers),
      m_singlesManagement(singlesManagement)

{
    auto *layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);

    // List of all markers

    layout->addWidget(new QLabel(tr("Automatisch angelegte Markierungen:")));

    m_allMarkersList = new QTableWidget;
    m_allMarkersList->verticalHeader()->hide();
    m_allMarkersList->setShowGrid(false);
    m_allMarkersList->setSelectionMode(QTableWidget::NoSelection);
    m_allMarkersList->setFocusPolicy(Qt::NoFocus);
    m_allMarkersList->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_allMarkersList->setItemDelegate(new TableDelegate(m_allMarkersList));
    m_allMarkersList->setColumnCount(2);
    m_allMarkersList->setRowCount(m_markersTemplate->count());
    m_allMarkersList->setHorizontalHeaderLabels({ tr("Name/Farbe"), tr("Sortierung") });

    layout->addWidget(m_allMarkersList);
    // Make sure the frameWidth() is correct()
    m_allMarkersList->ensurePolished();

    // Set the correct maximum height
    m_allMarkersList->setMaximumHeight(
        m_allMarkersList->horizontalHeader()->height()
        // The cast to int is necessary for Qt 6, because count() returns a sizeType, not int.
        // However this also works for Qt 5, where it simply does nothing and lets the int as-is.
        + std::min(5, static_cast<int>(m_markersTemplate->count())) * m_allMarkersList->rowHeight(0)
        + 2 * m_allMarkersList->frameWidth());

    // Special markers

    layout->addWidget(new QLabel(tr("Vorauswahl Markierung für:")));

    const int specialMarkerRows = Markers::specialMarkers.count();

    m_specialMarkersList = new QTableWidget;
    m_specialMarkersList->horizontalHeader()->hide();
    m_specialMarkersList->verticalHeader()->hide();
    m_specialMarkersList->setShowGrid(false);
    m_specialMarkersList->setSelectionMode(QTableWidget::NoSelection);
    m_specialMarkersList->setFocusPolicy(Qt::NoFocus);
    m_specialMarkersList->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_specialMarkersList->setItemDelegate(new TableDelegate(m_specialMarkersList));
    m_specialMarkersList->setColumnCount(2);
    m_specialMarkersList->setRowCount(specialMarkerRows);

    layout->addWidget(m_specialMarkersList);
    m_specialMarkersList->ensurePolished();

    m_specialMarkersList->setMaximumHeight(m_specialMarkersList->horizontalHeader()->height()
                                           + specialMarkerRows * m_specialMarkersList->rowHeight(0)
                                           + 2 * m_specialMarkersList->frameWidth());

    m_specialMarkersList->setItem(Markers::specialMarkers.indexOf(Markers::Default), 0,
                                  new QTableWidgetItem(tr("Neue Anmeldungen:")));
    m_specialMarkersList->setItem(Markers::specialMarkers.indexOf(Markers::Drawn), 0,
                                  new QTableWidgetItem(tr("Ausgeloste Paare/Spieler:")));
    m_specialMarkersList->setItem(Markers::specialMarkers.indexOf(Markers::Booked), 0,
                                  new QTableWidgetItem(tr("Voranmeldungen:")));
    m_specialMarkersList->resizeColumnsToContents();

    // Singles management

    m_noProcessSingles = new QLabel(tr("Allein gekommene Spieler werden nicht gesondert "
                                       "berücksichtigt"));
    m_noProcessSingles->setWordWrap(true);
    m_noProcessSingles->hide();
    layout->addWidget(m_noProcessSingles);

    m_processSingles = new QGroupBox(tr("Allein gekommene Spieler"));
    m_processSingles->setStyleSheet(SharedStyles::normalGroupBoxTitle);
    auto *processSingleslayout = new QVBoxLayout(m_processSingles);
    m_singlesMarker = new QLabel;
    processSingleslayout->addWidget(m_singlesMarker);
    m_automaticMarking = new QLabel;
    processSingleslayout->addWidget(m_automaticMarking);
    m_assignedMarker = new QLabel;
    processSingleslayout->addWidget(m_assignedMarker);
    layout->addWidget(m_processSingles);
}

const Markers::Marker &MarkersDisplay::findMarker(int id) const
{
    for (const auto &marker : *m_markersTemplate) {
        if (marker.id == id) {
            return marker;
        }
    }

    return findMarker(0);
}

QString MarkersDisplay::markerName(int id) const
{
    const auto &marker = findMarker(id);
    return id == 0 ? marker.name : tr("<span style=\"color: %1\">%2</span>").arg(
                                      marker.color.name(), marker.name.toHtmlEscaped());
}

void MarkersDisplay::setTournamentMode(Tournament::Mode mode)
{
    m_tournamentMode = mode;
}

void MarkersDisplay::setMarkersTemplate(const QVector<Markers::Marker> *markersTemplate)
{
    m_markersTemplate = markersTemplate;
}

void MarkersDisplay::refresh()
{
    // Update the list of all markers
    m_allMarkersList->setRowCount(m_markersTemplate->count());
    for (int i = 0; i < m_markersTemplate->count(); i++) {
        auto *item = new QTableWidgetItem;
        m_allMarkersList->setItem(i, 0, item);
        item->setText(m_markersTemplate->at(i).name.isEmpty() ? tr("(unmarkiert)")
                                                              : m_markersTemplate->at(i).name);
        item->setForeground(m_markersTemplate->at(i).color);
        m_allMarkersList->setItem(i, 1, new QTableWidgetItem(
            m_markersTemplate->at(i).sorting == Markers::NewBlock ? tr("Neuer Block")
                                                                  : tr("Fortlaufend")));
    }
    m_allMarkersList->resizeColumnsToContents();

    // Update the special markers list

    for (const auto type : Markers::specialMarkers) {
        auto *item = new QTableWidgetItem;
        const int id = m_specialMarkers->value(type);

        if (id == -1) {
            item->setText(tr("nicht gesetzt"));
            auto font = item->font();
            font.setItalic(true);
            item->setFont(font);
        } else {
            const auto &marker = findMarker(id);
            item->setText(marker.name.isEmpty() ? tr("(unmarkiert)") : marker.name);
            item->setForeground(marker.color);
        }

        m_specialMarkersList->setItem(Markers::specialMarkers.indexOf(type), 1, item);
    }

    m_specialMarkersList->resizeColumnsToContents();

    // Singles management

    if (m_singlesManagement->enabled) {
        m_noProcessSingles->hide();
        m_processSingles->show();
        m_singlesMarker->setText(tr("Markierung für „Allein da“: %1").arg(
                                    markerName(m_singlesManagement->singleMarker)));
        if (m_singlesManagement->markSingles) {
            m_automaticMarking->setText(
                tr("Automatisch setzen, wenn der Name „%1“ %2").arg(
                    m_singlesManagement->conditionString,
                    m_singlesManagement->condition == 0 ? tr("nicht enthält")
                                                        : tr("enthält")));
            m_automaticMarking->show();
        } else {
            m_automaticMarking->hide();
        }
        m_assignedMarker->setText(tr("Markierung nach Zuweisung: %1").arg(
                                     markerName(m_singlesManagement->assignedMarker)));
    } else {
        m_noProcessSingles->setVisible(m_tournamentMode == Tournament::FixedPairs);
        m_processSingles->hide();
    }
}
