// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef FULLSCREENMENU_H
#define FULLSCREENMENU_H

// Qt includes
#include <QMenu>

// Qt classes
class QScreen;

class FullScreenMenu : public QMenu
{
    Q_OBJECT

public:
    explicit FullScreenMenu(QWidget *parent);

Q_SIGNALS:
    void fullScreenRequested(QScreen *screen);

private Q_SLOTS:
    void updateFullScreenMenu();
    void requestFullScreen(const QString &name, int screenIndex);

};

#endif // FULLSCREENMENU_H
