// SPDX-FileCopyrightText: 2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef BOOKING_H
#define BOOKING_H

// Qt includes
#include <QString>
#include <QHash>
#include <QDateTime>

namespace Booking
{

// =================================================================================================
// CAUTION: If the settings version is bumped, the network protocol version also has to be!
// =================================================================================================
constexpr int settingsVersion = 2;

struct Settings
{
    QString tournamentName;
    QString securityKey;

    bool isValid() const
    {
        return ! this->tournamentName.isEmpty() && ! this->securityKey.isEmpty();
    }

    void clear()
    {
        this->tournamentName.clear();
        this->securityKey.clear();
    }
};

enum RevocationReason {
    UnknownRevocationReason,
    RegistrationDeleted,
    NewCodeExported
};

const QHash<RevocationReason, QString> revocationReasonMap {
    { RevocationReason::RegistrationDeleted, QStringLiteral("RegistrationDeleted") },
    { RevocationReason::NewCodeExported,     QStringLiteral("NewCodeExported") }
};

struct RevocationState
{
    QDateTime date;
    RevocationReason reason = RevocationReason::UnknownRevocationReason;

    bool isRevoked() const
    {
        return this->date.isValid();
    }
};

}

#endif // BOOKING_H
