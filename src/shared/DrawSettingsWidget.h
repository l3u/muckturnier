// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef DRAWSETTINGSWIDGET_H
#define DRAWSETTINGSWIDGET_H

// Local includes
#include "Draw.h"

// Qt includes
#include <QWidget>

// Qt classes
class QRadioButton;
class QSpinBox;
class QLabel;
class QCheckBox;

class DrawSettingsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit DrawSettingsWidget(QWidget *parent = nullptr);
    void updateSettings(const Draw::Settings &settings);
    Draw::Settings settings() const;

private Q_SLOTS:
    void optionStateChanged(bool state, QSpinBox *target);

private: // Functions
    void adjustLimits();

private: // Variables
    QRadioButton *m_consecutive;
    QRadioButton *m_random;
    QCheckBox *m_tablesLimitSet;
    QSpinBox *m_tablesLimit;
    QCheckBox *m_randomDrawSet;
    QSpinBox *m_randomDraw;
    QCheckBox *m_windowDrawSet;
    QSpinBox *m_windowDraw;
    QCheckBox *m_consecutiveDrawSet;
    QSpinBox *m_consecutiveDraw;
    QCheckBox *m_avoidTeamMateCollision;
    QCheckBox *m_avoidOpponentCollision;
    QCheckBox *m_warnTeamMateCollision;
    QCheckBox *m_warnOpponentCollision;

    bool m_adjusting = false;
    bool m_updatingSettings = false;

};

#endif // DRAWSETTINGSWIDGET_H
