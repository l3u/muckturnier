// SPDX-FileCopyrightText: 2020-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef MARKERS_H
#define MARKERS_H

#include <QString>
#include <QColor>
#include <QHash>

namespace Markers
{

// Combo boxes / types

enum Type {
    Default,
    Drawn,
    Booked,
    Single,
    Assigned
};

// Markers

enum MarkerSorting {
    NewBlock,
    Merged
};

struct Marker
{
    int id = -1;
    QString name;
    QColor color;
    MarkerSorting sorting;

    bool isValid() const
    {
        return this->id != -1;
    }

    bool operator==(const Marker &other) const
    {
        return    this->id      == other.id
               && this->name    == other.name
               && this->color   == other.color
               && this->sorting == other.sorting;
    }

    bool operator!=(const Marker &other) const
    {
        return    this->id      != other.id
               || this->name    != other.name
               || this->color   != other.color
               || this->sorting != other.sorting;
    }
};

const Marker unmarkedMarker {
    0,                      // id
    QString(),              // name
    QColor(0, 0, 0),        // color
    MarkerSorting::NewBlock // sorting
};

// Singles Management

constexpr int singlesManagementVersion = 1;

struct SinglesManagement
{
    bool enabled;
    int singleMarker;
    bool markSingles;
    QString conditionString;
    int condition;
    int assignedMarker;
};

const SinglesManagement defaultSinglesManagement {
    false,               // enabled
    0,                   // singleMarker
    true,                // markSingles
    QStringLiteral("/"), // conditionString
    0,                   // condition
    0                    // assignedMarker
};

const QVector<Markers::Type> specialMarkers {
    Markers::Default,
    Markers::Drawn,
    Markers::Booked
};

const QHash<Markers::Type, int> defaultSpecialMarkerValues {
    { Markers::Default,  0 },
    { Markers::Drawn,   -1 },
    { Markers::Booked,  -1 }
};

}

#endif // MARKERS_H
