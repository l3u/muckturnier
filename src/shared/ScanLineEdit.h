// SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SCANLINEEDIT_H
#define SCANLINEEDIT_H

// Qt includes
#include <QLineEdit>

// Qt classes
class QFocusEvent;

class ScanLineEdit : public QLineEdit
{
    Q_OBJECT

public:
    explicit ScanLineEdit(QWidget *parent = nullptr);
    bool readyToScan() const;

public Q_SLOTS:
    void setShowBlurAction(bool state);

Q_SIGNALS:
    void focusChanged(bool focused);

protected:
    void focusInEvent(QFocusEvent *event) override;
    void focusOutEvent(QFocusEvent *event) override;

private Q_SLOTS:
    void updateBlurAction();

private: // Variables
    QAction *m_blurAction;
    bool m_showBlurAction = false;

};

#endif // SCANLINEEDIT_H
