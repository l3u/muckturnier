// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SHAREDSTYLES_H
#define SHAREDSTYLES_H

// Qt includes
#include <QLatin1String>

namespace SharedStyles
{

const QLatin1String boldText  ("font-weight: bold;");
const QLatin1String redText   ("color: red;");
const QLatin1String greenText ("color: green;");
const QLatin1String headerLogo("background-color: #2f874c;");
const QLatin1String footerLogo("color: white; background-color: #309ac4;");

const QLatin1String normalGroupBoxTitle("QGroupBox { font-weight: normal; }");

const QLatin1String visibleVerticalSplitter(
    "QSplitter::handle {"
        "background-color: palette(Button);"
        "border: 1px solid palette(Mid);"
        "border-radius: 2px;"
        "margin: 4px 50px;"
    "}"
);

const QLatin1String visibleHorizontalSplitter(
    "QSplitter::handle {"
        "background-color: palette(Button);"
        "border: 1px solid palette(Mid);"
        "border-radius: 2px;"
        "margin: 50px 4px;"
    "}"
);

const QLatin1String hiddenSplitter(
    "QSplitter::handle {"
        "margin: 1px;"
        "background-color: transparent;"
    "}"
);

}

#endif // SHARED_STYLES_H
