// SPDX-FileCopyrightText: 2010-2020 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SEARCHLINEEDIT_H
#define SEARCHLINEEDIT_H

// Qt includes
#include <QLineEdit>

// Qt classes
class QKeyEvent;

class SearchLineEdit : public QLineEdit
{
    Q_OBJECT

public:
    explicit SearchLineEdit(QWidget *parent = nullptr);

protected:
    void keyPressEvent(QKeyEvent *event) override;

};

#endif // SEARCHLINEEDIT_H
