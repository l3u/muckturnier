// SPDX-FileCopyrightText: 2019-2025 Tobias Leupold <tl@stonemx.de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "BackupEngine.h"

// Qt includes
#include <QDebug>
#include <QFileInfo>
#include <QRegularExpression>
#include <QMessageBox>
#include <QDir>
#include <QPushButton>

const QString s_dateFormat = QLatin1String("yyyy-MM-dd_hh-mm-ss");

BackupEngine::BackupEngine(const QString &fileName) : QObject()
{
    setFileName(fileName);
}

void BackupEngine::setFileName(const QString &fileName)
{
    m_originalFile = fileName;
    QFileInfo fileInfo(fileName);
    m_path = fileInfo.path();
    m_baseName = fileInfo.completeBaseName();

    QRegularExpression re(QStringLiteral("^(.+)"                  // Basename
                                         "_(\\d{4}-\\d{2}-\\d{2}" // yyyy-MM-dd
                                         "_\\d{2}-\\d{2}-\\d{2})" // hh-mm-ss
                                         "_?\\d*$"));             // possible consecutive number
    QRegularExpressionMatch match = re.match(m_baseName);
    if (match.hasMatch()) {
        m_originalBaseName = match.captured(1);
        m_backupTime = QDateTime::fromString(match.captured(2), s_dateFormat);
    }
}

bool BackupEngine::createBackup()
{
    return createBackup(QDateTime::currentDateTime().toString(s_dateFormat));
}

bool BackupEngine::createBackup(const QString &suffix)
{
    // Be sure to create a non-existant file
    m_backupFile = m_baseName + QStringLiteral("_") + suffix;
    QString backupPath = QStringLiteral("%1/%2.mtdb").arg(m_path, m_backupFile);

    int consecutiveNumber = 0;
    while (QFileInfo::exists(backupPath)) {
        backupPath = QStringLiteral("%1/%2_%3.mtdb").arg(m_path, m_backupFile,
                                                         QString::number(++consecutiveNumber));
    }

    // Set m_backupFile to the correct file name (without path)
    if (consecutiveNumber > 0) {
        m_backupFile.append(QLatin1String("_") + QString::number(consecutiveNumber));
    }
    m_backupFile.append(QLatin1String(".mtdb"));

    // Check if we can actually write the file. Yes, we have to do it in this way, because
    // QFileInfo::isWritable does not work as expected on Windows (returns false for
    // "My Documents").
    QFile writeTest(backupPath);
    if (writeTest.open(QIODevice::WriteOnly)) {
        writeTest.close();
        writeTest.remove(); // This is save because we can be sure that backup does not exist yet
    } else {
        return false;
    }

    // Create the actual backup
    return QFile::copy(m_originalFile, backupPath);
}

const QString &BackupEngine::path() const
{
    return m_path;
}

const QString &BackupEngine::backupFile() const
{
    return m_backupFile;
}

QString BackupEngine::messageDate() const
{
    if (QDateTime::currentDateTime().toString(QStringLiteral("dd.MM.yyyy"))
        != m_backupTime.toString(QStringLiteral("dd.MM.yyyy"))) {

        return m_backupTime.toString(tr("'am' dd.MM.yyyy 'um' hh:mm:ss 'Uhr'"));
    } else {
        return m_backupTime.toString(tr("'um' hh:mm:ss 'Uhr'"));
    }
}

const QString &BackupEngine::baseName() const
{
    return m_baseName;
}

const QString &BackupEngine::originalBaseName() const
{
    return m_originalBaseName;
}

QString BackupEngine::originalFileName() const
{
    return QStringLiteral("%1/%2.mtdb").arg(m_path, m_originalBaseName);
}
