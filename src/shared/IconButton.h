// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef ICONBUTTON_H
#define ICONBUTTON_H

// Qt includes
#include <QToolButton>

// Local classes
class SharedObjects;
class ResourceFinder;

class IconButton : public QToolButton
{
    Q_OBJECT

public:
    explicit IconButton(SharedObjects *sharedObjects, const QString &icon,
                        QWidget *parent = nullptr);

private Q_SLOTS:
    void initializeIcon();

private: // Variables
    ResourceFinder *m_resourceFinder;
    const QString m_icon;

};

#endif // ICONBUTTON_H
