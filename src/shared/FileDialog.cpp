// SPDX-FileCopyrightText: 2018-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "FileDialog.h"

#include "Database/Database.h"

// Qt includes
#include <QDebug>
#include <QStandardPaths>
#include <QFileInfo>
#include <QFileDialog>
#include <QMessageBox>
#include <QDesktopServices>

FileDialog::FileDialog(QWidget *parent, Database *db, DeleteMode deleteMode)
    : TitleDialog(parent, deleteMode),
      m_db(db)
{
    addButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
}

void FileDialog::setTargetFileBaseName(const QString &targetFileBaseName)
{
    m_targetFileBaseName = targetFileBaseName;
}

QString FileDialog::targetFileBaseName() const
{
    return m_targetFileBaseName;
}

void FileDialog::setTargetFileExtension(const QString &targetFileExtension)
{
    const auto targetFileExtensions = targetFileExtension.split(QStringLiteral(" "));
    m_targetFileExtension = targetFileExtensions.last();
    m_targetFileWildcards.clear();
    for (const auto &extension : targetFileExtensions) {
        m_targetFileWildcards.append(QStringLiteral("*.%1 ").arg(extension));
    }
    m_targetFileWildcards = m_targetFileWildcards.trimmed();
}

void FileDialog::setTargetFileDescription(const QString &targetFileDescription)
{
    m_targetFileDescription = targetFileDescription;
}

void FileDialog::setFileDialogTitle(const QString &fileDialogTitle)
{
    m_fileDialogTitle = fileDialogTitle;
}

QString FileDialog::getTargetFileName(const QString &directory)
{
    QString defaultFileName = directory + QStringLiteral("/%1.%2").arg(m_targetFileBaseName,
                                                                       m_targetFileExtension);
    int number = 0;
    while (QFileInfo::exists(defaultFileName)) {
        number++;
        defaultFileName = directory + QStringLiteral("/%1_%2.%3").arg(m_targetFileBaseName,
                                                                      QString::number(number),
                                                                      m_targetFileExtension);
    }

    QString fileName = QFileDialog::getSaveFileName(this,
        m_fileDialogTitle,
        QDir::toNativeSeparators(defaultFileName),
        QStringLiteral("%1 (%2);;").arg(m_targetFileDescription, m_targetFileWildcards)
        + tr("Alle Dateien") + QStringLiteral(" (*)"));

    if (fileName.isEmpty()) {
        return QString();
    }

    // The user could have selected the currently open database

    if (fileName == m_db->dbFile()) {
        QMessageBox messageBox(this);
        messageBox.setIcon(QMessageBox::Warning);
        messageBox.setWindowTitle(windowTitle());
        messageBox.setText(tr("Die ausgewählte Datei ist die momentan geöffnete Turnierdatenbank. "
                              "Soll sie wirklich überschrieben werden? Alle Daten gehen dabei "
                              "verloren!"));
        messageBox.setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
        messageBox.setDefaultButton(QMessageBox::Cancel);

        if (messageBox.exec() == QMessageBox::Cancel) {
            return QString();
        }

        m_db->closeDatabaseConnection();
    }

    // The selected file could be a database opened by another instance
    const QString lockFile(m_db->checkForLockFile(fileName));
    if (! lockFile.isEmpty()) {
        QMessageBox::warning(this, windowTitle(),
            tr("<p><b>Datei kann nicht überschrieben werden</b></p>"
               "<p>Die Datei „%1“ ist eine Turnierdatenbank, die momentan von einer anderen "
               "Instanz von Muckturnier geöffnet ist. Sie kann deswegen jetzt nicht überschrieben "
               "werden. Bitte erst die entsprechende Instanz bzw. die Datenbank schließen.</p>"
               "<p>Sollte dies nicht der Fall sein (evtl. nach einen Crash, Stromausfall etc.), "
               "dann bitte die Lock-Datei „%2“ manuell löschen und das Überschreiben erneut "
               "anfragen.</p>").arg(QDir::toNativeSeparators(fileName).toHtmlEscaped(),
                                    QDir::toNativeSeparators(lockFile).toHtmlEscaped()));
        return QString();
    }

    // Check if we can write the file. If it exists, it will be deleted, but the user has been
    // asked by the QFileDialog if this is okay. And yes, we have to do it in this way, because
    // QFileInfo::isWritable does not work as expected on Windows (returns false for
    // "My Documents").
    QFile test(fileName);
    if (test.open(QIODevice::WriteOnly)) {
        test.close();
        test.remove();
    } else {
        QMessageBox::warning(this,
            windowTitle(),
            tr("Es bestehen keine Schreibrechte für die Datei „%1“. "
               "Bitte die Zugriffsrechte überprüfen!").arg(QDir::toNativeSeparators(fileName)),
            QMessageBox::Ok);
        return QString();
    }

    return fileName;
}

void FileDialog::successMessage(const QString &title, const QString &message)
{
    QMessageBox::information(this, title, message, QMessageBox::Ok);
}

void FileDialog::openFile(const QString &fileName)
{
    QDesktopServices::openUrl(QUrl::fromLocalFile(fileName));
}
