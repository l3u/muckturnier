// SPDX-FileCopyrightText: 2019-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef ABSTRACTSETTINGSPAGE_H
#define ABSTRACTSETTINGSPAGE_H

// Qt includes
#include <QWidget>

// Qt classes
class QVBoxLayout;
class QLabel;

class AbstractSettingsPage : public QWidget
{
    Q_OBJECT

public:
    QString pageTitle() const;
    virtual void saveSettings() = 0;

Q_SIGNALS:
    void linkActivated(const QString &anchor);

protected:
    explicit AbstractSettingsPage(const QString &pageTitle, QWidget *parent = nullptr);
    QVBoxLayout *layout() const;
    QLabel *descriptionLabel(const QString &text) const;

private: // Variables
    const QString m_pageTitle;
    QVBoxLayout *m_layout;

};

#endif // ABSTRACTSETTINGSPAGE_H
