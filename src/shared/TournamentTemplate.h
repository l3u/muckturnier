// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef TOURNAMENTTEMPLATE_H
#define TOURNAMENTTEMPLATE_H

#include "Tournament.h"
#include "RegistrationColumns.h"
#include "Draw.h"
#include "DefaultValues.h"
#include "OptionalPages.h"
#include "Schedule.h"

namespace TournamentTemplate
{

// This version number is only for changes in the Settings struct itself,
// not for the externally stored structs (which have an own version counter)
constexpr int version = 5;

struct Settings
{
    Tournament::Mode tournamentMode;
    Tournament::ScoreType scoreType;
    int boogerScore;
    int boogersPerRound;
    QVector<RegistrationColumns::Column> registrationColumns;
    bool registrationListHeaderHidden;
    bool autoSelectPairs;
    bool selectByLastRound;
    bool autoSelectTableForDraw;
    bool includeOpponentGoals;
    bool grayOutUnimportantGoals;
    QVector<OptionalPages::Page> optionalPages;

    // Externally stored structs
    Draw::Settings drawSettings;
    Schedule::Settings scheduleSettings;
};

const Settings defaultSettings {
    // Settings versioned here
    Tournament::FixedPairs,              // tournamentMode
    Tournament::HorizontalScore,         // scoreType
    DefaultValues::boogerScore,          // boogerScore
    DefaultValues::boogersPerRound,      // boogersPerRound
    RegistrationColumns::defaultColumns, // registrationColumns
    false,                               // registrationListHeaderHidden
    true,                                // autoSelectPairs
    true,                                // selectByLastRound
    false,                               // autoSelectTableForDraw
    true,                                // includeOpponentGoals
    true,                                // grayOutUnimportantGoals
    { },                                 // optionalPages

    // Externally stored structs
    Draw::defaultSettings,               // drawSettings
    Schedule::defaultSettings            // scheduleSettings
};

}

#endif // TOURNAMENTTEMPLATE_H
