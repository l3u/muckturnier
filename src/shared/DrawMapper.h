// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef DRAWMAPPER_H
#define DRAWMAPPER_H

// Local includes
#include "Draw.h"
#include "Players.h"

// Qt includes
#include <QVector>
#include <QString>

namespace DrawMapper
{

const auto emptyTable = QVector<QString> { QString(), QString(), QString(), QString() };

QVector<QVector<QString>> mapDraw(const QVector<Players::Data> playersData, int round);

}

#endif // DRAWMAPPER_H
