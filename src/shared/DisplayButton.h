// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef DISPLAYBUTTON_H
#define DISPLAYBUTTON_H

// Qt includes
#include <QWidget>

// Qt classes
class QPushButton;
class QMenu;

class DisplayButton : public QWidget
{
    Q_OBJECT

public:
    explicit DisplayButton(const QString &text, QMenu *menu, QWidget *parent = nullptr);

Q_SIGNALS:
    void displayRequested();

public Q_SLOTS:
    void displayClosed();

private Q_SLOTS:
    void requestDisplay();

private: // Variables
    QPushButton *m_show;
    QPushButton *m_remote;

};

#endif // DISPLAYBUTTON_H
