// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "DrawMapper.h"

// Qt includes
#include <QHash>
#include <QMap>

// C++ includes
#include <algorithm>

namespace DrawMapper
{

QVector<QVector<QString>> mapDraw(const QVector<Players::Data> playersData, int round)
{
    // This maps all drawn seats from a Players::Data QVector
    // into a complete representation of the draw. The format is:
    //
    // {
    //     Table 1 { "Pair 1 player 1", "Pair 1 player 2", "Pair 2 player 1", "Pair 2 player 2" }
    //     Table 2 ...
    // }
    //
    // Tables that are currently empty are added as { "", "", "", "" }

    // Create a list of all assigned seats
    QHash<int, QVector<QString>> draw;
    for (int i = 0; i < playersData.count(); i++) {
        const auto &playersDraw = playersData.at(i).draw;
        if (! playersDraw.contains(round)) {
            continue;
        }
        const auto &seat = playersDraw[round];
        if (! draw.contains(seat.table)) {
            draw.insert(seat.table, QVector<QString>(4));
        }
        draw[seat.table][Draw::indexForPair(seat.pair) + seat.player - 1] = playersData.at(i).name;
    }

    // Sort the list by table numbers and insert empty tables
    QVector<QVector<QString>> sortedDraw;
    const auto tables = draw.keys();
    const int maxTable = tables.count() > 0 ? *std::max_element(tables.begin(), tables.end()) : 0;
    for (int i = 1; i <= maxTable; i++) {
        if (draw.contains(i)) {
            sortedDraw.append(draw.value(i));
        } else {
            sortedDraw.append(QVector<QString>(4));
        }
    }

    return sortedDraw;
}

}
