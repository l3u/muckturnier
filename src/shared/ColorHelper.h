// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef COLORHELPER_H
#define COLORHELPER_H

// Qt includes
#include <QString>
#include <QColor>

namespace ColorHelper
{

QString colorToString(const QColor &color);
QColor colorFromString(const QString &string);

}

#endif // COLORHELPER_H
