// SPDX-FileCopyrightText: 2020 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef MINIMUMVIEWPORTWIDGET_H
#define MINIMUMVIEWPORTWIDGET_H

// Qt includes
#include <QWidget>

class MinimumViewportWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MinimumViewportWidget(QWidget *parent = nullptr);
    void setMinimumViewport(const QSize &size);
    QSize minimumViewport() const;

private: // Variables
    QSize m_minimumViewport;

};

#endif // MINIMUMVIEWPORTWIDGET_H
