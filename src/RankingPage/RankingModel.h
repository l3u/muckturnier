// SPDX-FileCopyrightText: 2023-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef RANKINGMODEL_H
#define RANKINGMODEL_H

// Local includes
#include "Database/Database.h"

// Qt includes
#include <QAbstractTableModel>

class RankingModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit RankingModel(QObject *parent, Database *database);
    int rowCount(const QModelIndex & = QModelIndex()) const override;
    int columnCount(const QModelIndex & = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;

    const QString &name(int row) const;
    int playersNumber(int row) const;
    const QMap<int, int> &multipleRanks() const;

    void setParameters(bool isPairMode, int boogersPerRound);
    void setGrayOutUnimportantGoals(bool state);

    void clear();
    void refresh(int upToRound);

private: // Functions
    QColor grayedOutColor(const QColor &color) const;

private: // Variables
    Database *m_db;
    const int m_columnCount;

    QColor m_red;
    QColor m_defaultGray;
    QColor m_redGray;

    bool m_isPairMode = false;
    int m_boogersPerRound = 0;
    bool m_grayOutUnimportantGoals = false;

    QVector<Database::RankData> m_ranking;
    int m_rowCount = 0;
    int m_allBoogers = 0;
    QHash<int, bool> m_background;
    QVector<bool> m_goalsImportant;
    QVector<bool> m_opponentGoalsImportant;
    QMap<int, int> m_multipleRanks;

};

#endif // RANKINGMODEL_H
