// SPDX-FileCopyrightText: 2023-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "RankingModel.h"

#include "shared/TableDelegate.h"

// Qt includes
#include <QDebug>
#include <QPalette>

RankingModel::RankingModel(QObject *parent, Database *database)
    : QAbstractTableModel(parent),
      m_db(database),
      m_columnCount(5)
{
    QPalette palette;
    const auto foreground = palette.color(QPalette::Text);
    m_red = QColor(Qt::red);
    m_defaultGray = grayedOutColor(foreground);
    m_redGray = grayedOutColor(m_red);
}

QColor RankingModel::grayedOutColor(const QColor &color) const
{
    QColor processedColor = color.toCmyk();
    processedColor.setCmyk((int) ((float) processedColor.cyan() / 2.8),
                           (int) ((float) processedColor.magenta() / 2.8),
                           (int) ((float) processedColor.yellow() / 2.8),
                           (int) ((float) processedColor.black() / 2.8));
    return processedColor;
}

int RankingModel::rowCount(const QModelIndex &) const
{
    return m_rowCount;
}

int RankingModel::columnCount(const QModelIndex &) const
{
    return m_columnCount;
}

QVariant RankingModel::data(const QModelIndex &index, int role) const
{
    if (! index.isValid()) {
        return QVariant();
    }

    const auto row = index.row();
    const auto column = index.column();
    if (row >= m_rowCount || column >= m_columnCount) {
        return QVariant();
    }
    const auto &data = m_ranking.at(row);

    if (role == Qt::DisplayRole) {
        if (column == 0) {
            return data.rank;
        } else if (column == 1) {
            return data.name;
        } else if (column == 2) {
            return data.boogers;
        } else if (column == 3) {
            return data.goals;
        } else if (column == 4) {
            return data.opponentGoals;
        }

    } else if (role == Qt::ForegroundRole) {
        if (data.playedBoogers == m_allBoogers) {
            if (   (column == 3 && ! m_goalsImportant.at(row))
                || (column == 4 && ! m_opponentGoalsImportant.at(row))) {

                return m_defaultGray;
            }

        } else {
            if (column == 3) {
                return m_goalsImportant.at(row) ? m_red : m_redGray;
            } else if (column == 4) {
                return m_opponentGoalsImportant.at(row) ? m_red : m_redGray;
            } else {
                return m_red;
            }
        }

    } else if (role == TableDelegate::DrawBackground && m_background.value(data.rank)) {
        return true;

    } else if (role == TableDelegate::DrawTopLine
               && row > 0 && data.rank != m_ranking.at(row - 1).rank) {

        return true;
    }

    return QVariant();
}

QVariant RankingModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole || orientation == Qt::Vertical) {
        return QVariant();
    }

    if (section == 0) {
        return tr("Platz");
    } else if (section == 1) {
        return m_isPairMode ? tr("Paar") : tr("Spieler");
    } else if (section == 2) {
        return tr("Bobbl");
    } else if (section == 3) {
        return tr("Tore");
    } else if (section == 4) {
        return tr("Gegn. Tore");
    }

    return QVariant();
}

void RankingModel::setParameters(bool isPairMode, int boogersPerRound)
{
    m_isPairMode = isPairMode;
    m_boogersPerRound = boogersPerRound;
    Q_EMIT headerDataChanged(Qt::Horizontal, 1, 1);
}

void RankingModel::setGrayOutUnimportantGoals(bool state)
{
    m_grayOutUnimportantGoals = state;
}

void RankingModel::clear()
{
    const auto lastRow = m_rowCount - 1;
    beginRemoveRows(QModelIndex(), 0, lastRow);
    m_ranking.clear();
    m_background.clear();
    m_multipleRanks.clear();
    endRemoveRows();
    Q_EMIT dataChanged(index(0, 0), index(lastRow, m_columnCount - 1), { Qt::DisplayRole });
}


void RankingModel::refresh(int upToRound)
{
    clear();

    const auto [ ranking, success ] = m_db->ranking(upToRound);
    if (! success) {
        return;
    }

    m_allBoogers = m_boogersPerRound * upToRound;
    m_rowCount = ranking.count();
    const auto lastRow = m_rowCount - 1;

    beginInsertRows(QModelIndex(), 0, lastRow);

    int background = false;
    int lastRank = -1;
    for (const auto &row : ranking) {
        if (row.rank != lastRank) {
            m_background[row.rank] = background;
            background = ! background;
            lastRank = row.rank;
        } else {
            m_multipleRanks[row.rank]++;
        }
    }

    for (const auto rank : m_multipleRanks.keys()) {
        m_multipleRanks[rank]++;
    }

    m_ranking = ranking;

    if (m_grayOutUnimportantGoals) {
        const auto [ goalsImportant, opponentGoalsImportant ] = m_db->goalsImportance(m_ranking);
        m_goalsImportant = goalsImportant;
        m_opponentGoalsImportant = opponentGoalsImportant;
    } else {
        m_goalsImportant.fill(true, m_rowCount);
        m_opponentGoalsImportant.fill(true, m_rowCount);
    }

    endInsertRows();
    Q_EMIT dataChanged(index(0, 0), index(lastRow, m_columnCount - 1), { Qt::DisplayRole });
}

const QString &RankingModel::name(int row) const
{
    Q_ASSERT(row < m_rowCount);
    return m_ranking.at(row).name;
}

int RankingModel::playersNumber(int row) const
{
    Q_ASSERT(row < m_rowCount);
    return m_ranking.at(row).number;
}

const QMap<int, int> &RankingModel::multipleRanks() const
{
    return m_multipleRanks;
}
