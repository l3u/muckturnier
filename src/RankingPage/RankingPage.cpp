// SPDX-FileCopyrightText: 2010-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "RankingPage.h"
#include "RankingModel.h"

#include "Database/Database.h"

#include "SharedObjects/SharedObjects.h"
#include "SharedObjects/SearchEngine.h"
#include "SharedObjects/Settings.h"
#include "SharedObjects/TournamentSettings.h"

#include "shared/SearchWidget.h"
#include "shared/TableView.h"
#include "shared/SharedStyles.h"

// Qt includes
#include <QDebug>
#include <QGroupBox>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QMenu>
#include <QPushButton>
#include <QComboBox>

RankingPage::RankingPage(QWidget *parent, SharedObjects *sharedObjects)
    : QWidget(parent),
      m_db(sharedObjects->database()),
      m_searchEngine(sharedObjects->searchEngine()),
      m_tournamentSettings(sharedObjects->tournamentSettings())
{
    QVBoxLayout *layout = new QVBoxLayout(this);

    m_noResultsLabel = new QLabel(tr("<i>Es liegen noch keine Ergebnisse vor</i>"));
    m_noResultsLabel->setAlignment(Qt::AlignCenter);
    m_noResultsLabel->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);
    layout->addWidget(m_noResultsLabel);

    m_settingsBox = new QGroupBox;
    QHBoxLayout *settingsBoxLayout = new QHBoxLayout(m_settingsBox);
    m_settingsBox->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Maximum);
    layout->addWidget(m_settingsBox);
    m_settingsBox->hide();

    m_upToRoundLabel = new QLabel(tr("Rangliste für Runde"));
    m_upToRoundLabel->setAlignment(Qt::AlignRight);
    m_upToRoundLabel->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    settingsBoxLayout->addWidget(m_upToRoundLabel);
    m_upToRoundLabel->hide();

    m_upToRound = new QComboBox;
    m_upToRound->setSizeAdjustPolicy(QComboBox::AdjustToContents);
    m_upToRound->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    connect(m_upToRound, QOverload<int>::of(&QComboBox::currentIndexChanged),
            this, [this](int round)
            {
                reload(round + 1);
            });
    settingsBoxLayout->addWidget(m_upToRound);
    m_upToRound->hide();

    settingsBoxLayout->addStretch();

    m_printButton = new QPushButton(tr("Rangliste drucken"));
    settingsBoxLayout->addWidget(m_printButton);
    m_printButton->setEnabled(false);
    connect(this, &RankingPage::enableExport, m_printButton, &QPushButton::setEnabled);
    connect(m_printButton, &QPushButton::clicked, this, &RankingPage::requestPrintRanking);

    QPushButton *settingsButton = new QPushButton(tr("Einstellungen"));
    QMenu *settingsMenu = new QMenu(this);

    m_includeOpponentGoals = settingsMenu->addAction(tr("Gegnerische Tore berücksichtigen"));
    m_includeOpponentGoals->setCheckable(true);
    connect(m_includeOpponentGoals, &QAction::toggled,
            this, &RankingPage::includeOpponentGoalsChanged);

    settingsMenu->addSeparator();

    m_grayOutUnimportantGoals = settingsMenu->addAction(tr("Nicht entscheidende Tore ausgrauen"));
    m_grayOutUnimportantGoals->setCheckable(true);
    connect(m_grayOutUnimportantGoals, &QAction::toggled,
            this, &RankingPage::grayOutUnimportantGoalsChanged);

    settingsButton->setMenu(settingsMenu);
    settingsBoxLayout->addWidget(settingsButton);

    m_rankingBox = new QGroupBox;
    QVBoxLayout *rankingBoxLayout = new QVBoxLayout(m_rankingBox);
    layout->addWidget(m_rankingBox);
    m_rankingBox->hide();

    m_isSearchLabel = new QLabel;
    m_isSearchLabel->setAlignment(Qt::AlignCenter);
    m_isSearchLabel->hide();
    rankingBoxLayout->addWidget(m_isSearchLabel);

    m_interimLabel = new QLabel;
    m_interimLabel->setWordWrap(true);
    rankingBoxLayout->addWidget(m_interimLabel);

    m_rankingModel = new RankingModel(this, m_db);
    m_rankingView = new TableView(this, TableDelegate::CustomAlternating);
    m_rankingView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_rankingView->setModel(m_rankingModel);
    rankingBoxLayout->addWidget(m_rankingView);

    m_multipleRanks = new QLabel;
    m_multipleRanks->setWordWrap(true);
    m_multipleRanks->hide();
    rankingBoxLayout->addWidget(m_multipleRanks);

    m_disqualifiedPlayers = new QLabel;
    m_disqualifiedPlayers->setWordWrap(true);
    m_disqualifiedPlayers->hide();
    rankingBoxLayout->addWidget(m_disqualifiedPlayers);

    m_searchWidget = new SearchWidget(QStringLiteral("ranking_page_search"), sharedObjects);
    connect(m_searchWidget, &SearchWidget::searchChanged, this, &RankingPage::updateSearch);
    rankingBoxLayout->addWidget(m_searchWidget);
}

void RankingPage::setupGui()
{
    m_includeOpponentGoals->blockSignals(true);
    m_grayOutUnimportantGoals->blockSignals(true);

    m_includeOpponentGoals->setChecked(m_tournamentSettings->includeOpponentGoals());
    m_includeOpponentGoals->setEnabled(m_db->isChangeable());

    m_grayOutUnimportantGoals->setChecked(m_tournamentSettings->grayOutUnimportantGoals());

    m_includeOpponentGoals->blockSignals(false);
    m_grayOutUnimportantGoals->blockSignals(false);

    m_rankingModel->setParameters(m_tournamentSettings->isPairMode(),
                                  m_tournamentSettings->boogersPerRound());
    m_rankingModel->setGrayOutUnimportantGoals(m_tournamentSettings->grayOutUnimportantGoals());

    m_rankingView->setColumnHidden(4, ! m_includeOpponentGoals->isChecked());
    m_rankingView->resizeColumnsToContents();

    reload();
}

void RankingPage::dataChanged()
{
    m_upToRound->blockSignals(true);
    m_upToRound->clear();
    for (int i = 1; i <= m_db->allRounds(); i++) {
        m_upToRound->addItem(QString::number(i));
    }
    m_upToRound->setCurrentIndex(m_db->allRounds() - 1);
    m_upToRound->blockSignals(false);

    if (m_upToRound->count() == 1) {
        m_upToRound->hide();
        m_upToRoundLabel->hide();
    } else {
        m_upToRound->show();
        m_upToRoundLabel->show();
    }

    reload(m_db->allRounds());
}

void RankingPage::includeOpponentGoalsChanged(bool state)
{
    m_tournamentSettings->setIncludeOpponentGoals(state);
    m_rankingView->setColumnHidden(4, ! state);
    reload(m_upToRound->currentIndex() + 1);
}

void RankingPage::reload(int upToRound)
{
    const bool localTournamentStarted = m_db->localTournamentStarted();
    m_noResultsLabel->setVisible(! localTournamentStarted);
    m_settingsBox->setVisible(localTournamentStarted);
    m_rankingBox->setVisible(localTournamentStarted);
    if (! localTournamentStarted) {
        return;
    }

    m_searchWidget->saveSearch();

    QVector<int> incompleteRounds;
    const auto roundsState = m_db->roundsState();
    const auto roundsStateKeys = roundsState.keys();
    for (const int round : roundsStateKeys) {
        if (! roundsState.value(round)) {
            incompleteRounds.append(round);
        }
    }
    const int missingRounds = incompleteRounds.count();

    QString interimText;
    if (missingRounds > 0) {
        Q_EMIT enableExport(false);
        interimText = tr("<table><tr><td><b>Achtung!</b></td><td>Das ist nur ein Zwischenergebnis. "
                         "Es fehlen noch Ergebnisse aus ");
        if (missingRounds == 1) {
            interimText += tr("Runde %1.").arg(QString::number(incompleteRounds.at(0)));
        } else if (missingRounds == 2) {
            interimText += tr("den Runden %1 und %2.").arg(QString::number(incompleteRounds.at(0)),
                                                           QString::number(incompleteRounds.at(1)));
        } else {
            interimText += tr("den Runden ");
            for (int i = 0; i < missingRounds - 1; i++) {
                interimText += tr("%1, ").arg(QString::number(incompleteRounds.at(i)));
            }
            interimText = interimText.left(interimText.size() - 2);
            interimText += tr(" und %1.").arg(QString::number(incompleteRounds.last()));
        }
        interimText += QStringLiteral(
            "<br/>%1 <span style=\"%2\">%3</span></td></tr></table>").arg(
            m_tournamentSettings->isPairMode()
                ? tr("Paare, von denen noch Spielstände fehlen, sind")
                : tr("Spieler, von denen noch Spielstände fehlen, sind"),
            SharedStyles::redText,
            tr("rot markiert"));
    } else {
        Q_EMIT enableExport(true);
    }

    if (upToRound < m_db->allRounds()) {
        if (m_db->roundsState()[upToRound]) {
            m_rankingBox->setTitle(tr("Zwischenstand: Rangliste nach Runde %1").arg(upToRound));
        } else {
            m_rankingBox->setTitle(tr("Zwischenstand: Rangliste im Verlauf von Runde %1").arg(
                                      upToRound));
        }
        interimText = tr("<table><tr><td><b>Achtung!</b></td><td>Das ist nicht die aktuelle "
                         "Rangliste! Um diese anzuzeigen <b>bitte Runde %1 auswählen!</b></td>"
                         "</tr></table>").arg(m_db->allRounds())
                      + interimText;
    } else {
        if (! m_db->roundsState()[m_db->allRounds()]) {
            m_rankingBox->setTitle(tr("Aktuelle Rangliste im Verlauf der %1. Runde").arg(
                                      QString::number(m_db->allRounds())));
        } else {
            if (m_db->allRounds() == 1) {
                if (m_db->isChangeable()) {
                    m_rankingBox->setTitle(tr("Aktuelle Rangliste nach einer Runde"));
                } else {
                    m_rankingBox->setTitle(tr("Rangliste nach einer Runde"));
                }
            } else {
                if (m_db->isChangeable()) {
                    m_rankingBox->setTitle(tr("Aktuelle Rangliste nach %1 Runden").arg(
                                           QString::number(m_db->allRounds())));
                } else {
                    m_rankingBox->setTitle(tr("Rangliste nach %1 Runden").arg(
                                           QString::number(m_db->allRounds())));
                }
            }
        }
    }

    m_interimLabel->setText(interimText);
    m_interimLabel->setVisible(! interimText.isEmpty());

    m_rankingModel->refresh(upToRound);

    // Shared ranks display

    const auto &multipleRanks = m_rankingModel->multipleRanks();
    if (multipleRanks.count() > 0) {
        QString multipleRanksText = tr("Mehrfach besetzte Ranglistenplätze: ");
        QMap<int, int>::const_iterator it;
        for (it = multipleRanks.constBegin(); it != multipleRanks.constEnd(); it++) {
            multipleRanksText += tr("<b>%1</b> (%2x), ").arg(
                                    QString::number(it.key()),
                                    QString::number(it.value()));
        }
        multipleRanksText.chop(2);
        m_multipleRanks->setText(multipleRanksText);
        m_multipleRanks->show();
    } else {
        m_multipleRanks->hide();
    }

    // Disqualified players display

    const auto [ disqualifiedPlayers, success2 ] = m_db->disqualifiedPlayers(upToRound);
    if (! success2) {
        return;
    }

    if (! disqualifiedPlayers.isEmpty()) {
        QString disqualifiedPlayersString;

        QMap<int, QVector<QString>>::const_iterator it;
        for (it = disqualifiedPlayers.constBegin(); it != disqualifiedPlayers.constEnd(); it++) {
            if (! disqualifiedPlayersString.isEmpty()) {
                disqualifiedPlayersString.append(tr("; "));
            }
            disqualifiedPlayersString.append(tr("Runde %1: ").arg(QString::number(it.key())));

            QString names;
            for (const auto &entry : it.value()) {
                if (! names.isEmpty()) {
                    names.append(tr(", "));
                }
                names.append(tr("„%1“").arg(entry.toHtmlEscaped()));
            }
            disqualifiedPlayersString.append(names);
        }

        m_disqualifiedPlayers->setText(tr("<b>Disqualifizierte %1</b>: %2").arg(
            m_tournamentSettings->isPairMode() ? tr("Paare") : tr("Spieler"),
            disqualifiedPlayersString));
        m_disqualifiedPlayers->show();

    } else {
        m_disqualifiedPlayers->hide();
    }

    m_rankingView->resizeColumnsToContents();
    m_searchWidget->restoreSearch();
}

void RankingPage::updateSearch(const QString &searchText)
{
    if (searchText.isEmpty()) {
        for (int row = 0; row < m_rankingModel->rowCount(); row++) {
            m_rankingView->showRow(row);
        }
        m_rankingView->resizeColumnsToContents();
        m_isSearchLabel->hide();
        return;
    }

    bool isNumber = false;
    const auto number = searchText.toInt(&isNumber);

    int matches = 0;

    if (isNumber && number > 0) {
        // We do a pure number search. Only check the players number
        for (int row = 0; row < m_rankingModel->rowCount(); row++) {
            m_rankingView->hideRow(row);
        }
        for (int row = 0; row < m_rankingModel->rowCount(); row++) {
            if (m_rankingModel->playersNumber(row) == number) {
                m_rankingView->showRow(row);
                matches = 1;
                break;
            }
        }

    } else {
        // We query the search engine
        const bool phoneticSearch = m_searchWidget->phoneticSearch();
        m_searchEngine->setSearchTerm(searchText);
        for (int row = 0; row < m_rankingModel->rowCount(); row++) {
            const bool rowMatches = m_searchEngine->checkMatch(m_rankingModel->name(row),
                                                               SearchEngine::DefaultSearch,
                                                               phoneticSearch);
            m_rankingView->setRowHidden(row, ! rowMatches);
            matches += rowMatches;
        }
    }

    m_searchWidget->updateMatches(matches);
    m_rankingView->resizeColumnsToContents();

    m_isSearchLabel->setText(tr("(Suchergebnis für „%1“)").arg(searchText.simplified()));
    m_isSearchLabel->show();
}

void RankingPage::resetSearch()
{
    m_searchWidget->clear();
}

void RankingPage::grayOutUnimportantGoalsChanged(bool state)
{
    m_rankingModel->setGrayOutUnimportantGoals(state);
    m_tournamentSettings->setGrayOutUnimportantGoals(state);
    reload(m_upToRound->currentIndex() + 1);
}
