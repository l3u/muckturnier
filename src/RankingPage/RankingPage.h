// SPDX-FileCopyrightText: 2010-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef RANKINGPAGE_H
#define RANKINGPAGE_H

// Qt includes
#include <QWidget>

// Qt classes
class QGroupBox;
class QLabel;
class QAction;
class QComboBox;
class QPushButton;

// Local classes
class SharedObjects;
class Database;
class SearchEngine;
class SearchWidget;
class TournamentSettings;
class RankingModel;
class TableView;

class RankingPage : public QWidget
{
    Q_OBJECT

public:
    explicit RankingPage(QWidget *parent, SharedObjects *sharedObjects);
    void setupGui();
    void reload(int upToRound = 0);

Q_SIGNALS:
    void statusUpdate(const QString &message);
    void scoreChanged();
    void enableExport(bool state);
    void requestPrintRanking();

public Q_SLOTS:
    void dataChanged();
    void resetSearch();

private Q_SLOTS:
    void updateSearch(const QString &searchText);
    void includeOpponentGoalsChanged(bool state);
    void grayOutUnimportantGoalsChanged(bool state);

private: // Variables
    Database *m_db;
    SearchEngine *m_searchEngine;
    TournamentSettings *m_tournamentSettings;

    QAction *m_includeOpponentGoals;
    QAction *m_grayOutUnimportantGoals;

    QGroupBox *m_settingsBox;
    QPushButton *m_printButton;
    QGroupBox *m_rankingBox;
    QLabel *m_interimLabel;
    QLabel *m_noResultsLabel;

    RankingModel *m_rankingModel;
    TableView *m_rankingView;

    SearchWidget *m_searchWidget;
    QLabel *m_isSearchLabel;
    QComboBox *m_upToRound;
    QLabel *m_upToRoundLabel;
    QLabel *m_multipleRanks;
    QLabel *m_disqualifiedPlayers;

};

#endif // RANKINGPAGE_H
