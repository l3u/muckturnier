// SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes

#include "AdjustDrawDialog.h"

#include "SharedObjects/TemporaryFileHelper.h"

#include "shared/DrawMapper.h"
#include "shared/TableDelegate.h"

// Qt includes

#include <QVBoxLayout>
#include <QDebug>
#include <QLabel>
#include <QTableWidget>
#include <QHeaderView>
#include <QTimer>
#include <QMessageBox>
#include <QPushButton>
#include <QTextStream>
#include <QDir>
#include <QDesktopServices>
#include <QUrl>
#include <QApplication>

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
#include <QTextCodec>
#endif

// C++ includes
#include <utility>

AdjustDrawDialog::AdjustDrawDialog(QWidget *parent, TemporaryFileHelper *tmpHelper,
                                   const QVector<Players::Data> &playersData, bool isPairMode,
                                   QString namesSeparator)
    : TitleDialog(parent),
      m_tmpHelper(tmpHelper)
{
    setTitle(tr("Auslosung bereinigen"));
    addButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Close);
    setOkButtonText(tr("Änderungen übernehmen"));
    buttonBox()->button(QDialogButtonBox::Close)->setFocus();
    connect(this, &QDialog::accepted,
            this, [this]
            {
                Q_EMIT adoptAdjustment(m_names, m_newSeats);
            });

    auto *print = new QPushButton(tr("Drucken (via HTML)"));
    connect(print, &QPushButton::clicked, this, &AdjustDrawDialog::printList);
    buttonBox()->addButton(print, QDialogButtonBox::HelpRole);

    QLabel *label;

    label = new QLabel(tr("Folgende Änderungen sind nötig, damit die Auslosung fortlaufend und "
                          "lückenlos ist, und alle Tische voll besetzt sind:"));
    label->setWordWrap(true);
    layout()->addWidget(label);

    const auto draw = DrawMapper::mapDraw(playersData, 1);
    m_tables = draw.count();
    auto adjustedDraw = draw;

    if (! isPairMode) {
        // Compile pairs leaving role intact
        for (int pair = 1; pair <= 2; pair++) {
            while (compilePairsSameRole(pair, adjustedDraw)) { }
        }

        // Compile pairs switching roles
        for (int pair = 1; pair <= 2; pair++) {
            while (compilePairsSwitchRole(pair, adjustedDraw)) { }
        }
    }

    // Compile opponent pairs leaving roles intact
    for (int pair = 1; pair <= 2; pair++) {
        while (compileOpponentPairs(pair, adjustedDraw)) { }
    }

    // Compile pairs switching role
    for (int pair = 1; pair <= 2; pair++) {
        while (compileSameTypePairs(pair, adjustedDraw)) { }
    }

    // Fill table gaps
    while (fillTableGaps(adjustedDraw)) { }

    // We need a name-index mapping for all names
    QVector<QString> allNames;
    for (const auto &data : playersData) {
        allNames.append(data.name);
    }

    // Compile all moved pairs/players
    QVector<Draw::Seat> oldSeats;
    QVector<QVector<QString>>::const_reverse_iterator it = draw.crbegin();
    while (it != draw.crend()) {
        for (const QString &name : *it) {
            if (! name.isEmpty() && m_moved.contains(name)) {
                // This is exclusively for round 1 draws. Also, each registration has to have a
                // draw set so this can be called. Thus, we can safely access the draw directly:
                const auto &oldSeat = playersData.at(allNames.indexOf(name)).draw[1];
                const auto &newSeat = m_moved[name];
                if (newSeat == oldSeat) {
                    continue;
                }
                m_names.append(name);
                oldSeats.append(oldSeat);
                m_newSeats.append(newSeat);
            }
        }
        it++;
    }

    auto displayNames = m_names;
    auto displayNewSeats = m_newSeats;

    if (! isPairMode) {
        QVector<int> toRemove;
        const int count = displayNames.count();
        for (int i = 0; i < count; i++) {
            if ((i + 1) < count && oldSeats.at(i) == oldSeats.at(i + 1)
                                && displayNewSeats.at(i) == displayNewSeats.at(i + 1)) {

                displayNames[i] = displayNames.at(i) + namesSeparator + displayNames.at(i + 1);
                toRemove.append(i + 1);
            }
        }

        int alreadyRemoved = 0;
        for (int i : std::as_const(toRemove)) {
            displayNames.remove(i - alreadyRemoved);
            oldSeats.remove(i - alreadyRemoved);
            displayNewSeats.remove(i - alreadyRemoved);
            alreadyRemoved++;
        }
    }

    m_changes = new QTableWidget;
    m_changes->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_changes->setItemDelegate(new TableDelegate(this));
    m_changes->verticalHeader()->hide();
    m_changes->setShowGrid(false);
    m_changes->setWordWrap(false);
    m_changes->setSelectionMode(QTableWidget::NoSelection);
    m_changes->setFocusPolicy(Qt::NoFocus);
    m_changes->setColumnCount(4);
    m_changes->setHorizontalHeaderLabels({ isPairMode ? tr("Paar") : tr("Spieler bzw. Paar"),
                                           tr("aktuell"), QString(), tr("korrigiert") });
    m_changes->setRowCount(displayNames.count());
    layout()->addWidget(m_changes);

    for (int i = 0; i < displayNames.count(); i++) {
        m_changes->setItem(i, 0, new QTableWidgetItem(displayNames.at(i)));

        QTableWidgetItem *item;

        item = new QTableWidgetItem(
            tr("Tisch %1 Paar %2").arg(QString::number(oldSeats.at(i).table),
                                       QString::number(oldSeats.at(i).pair)));
        item->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
        item->setForeground(Qt::red);
        m_changes->setItem(i, 1, item);

        item = new QTableWidgetItem(QStringLiteral("→"));
        item->setTextAlignment(Qt::AlignCenter | Qt::AlignVCenter);
        m_changes->setItem(i, 2, item);

        item = new QTableWidgetItem(
            tr("Tisch %1 Paar %2").arg(QString::number(displayNewSeats.at(i).table),
                                       QString::number(displayNewSeats.at(i).pair)));
        item->setForeground(Qt::darkGreen);
        m_changes->setItem(i, 3, item);
    }

    m_changes->resizeColumnsToContents();

    label = new QLabel(tr(
        "Zum Ausdrucken kann diese Liste in eine temporäre HTML-Datei exportiert, mit dem "
        "Standardwebbrowser geöffnet und dann gedruckt werden. Beim Schließen des Programms wird "
        "die Datei automatisch gelöscht."));
    label->setWordWrap(true);
    layout()->addWidget(label);

    QTimer::singleShot(0, this, &AdjustDrawDialog::resizeView);
}

void AdjustDrawDialog::resizeView()
{
    const auto parentGeometry = qobject_cast<QWidget *>(parent())->geometry();
    const auto dialogGeometry = geometry();
    const auto changesGeometry = m_changes->geometry();

    int targetWidth = dialogGeometry.width() - changesGeometry.width()
                      + m_changes->horizontalHeader()->length() + 4;
    int targetHeight = dialogGeometry.height() - changesGeometry.height()
                       + m_changes->rowHeight(0) * (m_changes->rowCount() + 1) + 4;

    if (parentGeometry.width() < targetWidth) {
        targetWidth = parentGeometry.width();
    }
    if (parentGeometry.height() < targetHeight) {
        targetHeight = parentGeometry.height();
    }

    setGeometry(parentGeometry.x() + (parentGeometry.width() - targetWidth) / 2,
                parentGeometry.y() + (parentGeometry.height() - targetHeight) / 2,
                targetWidth, targetHeight);
}

QVector<int> AdjustDrawDialog::findIncompletePairs(int pair, QVector<QVector<QString>> &draw) const
{
    QVector<int> incomplete;

    for (int i = 0; i < m_tables; i++) {
        if (! draw.at(i).at(Draw::indexForPlayer(pair, 1)).isEmpty()
            && draw.at(i).at(Draw::indexForPlayer(pair, 2)).isEmpty()) {
            incomplete.append(i);
        }
    }

    return incomplete;
}

bool AdjustDrawDialog::compilePairsSameRole(int pair, QVector<QVector<QString>> &draw)
{
    const auto incomplete = findIncompletePairs(pair, draw);
    if (incomplete.count() < 2) {
        return false;
    }

    const int sourceIndex = Draw::indexForPlayer(pair, 1);
    const int from = incomplete.last();
    const int to = incomplete.first();
    const auto player = draw[from][sourceIndex];
    draw[to][Draw::indexForPlayer(pair, 2)] = player;
    draw[from][sourceIndex].clear();

    m_moved[player] = { to + 1, pair, 2 };
    return true;
}

bool AdjustDrawDialog::compilePairsSwitchRole(int pair, QVector<QVector<QString>> &draw)
{
    const auto incompleteTarget = findIncompletePairs(pair, draw);
    if (incompleteTarget.isEmpty()) {
        return false;
    }

    const int otherPair = Draw::otherPair(pair);
    const auto incompleteSource = findIncompletePairs(otherPair, draw);
    if (incompleteSource.isEmpty()) {
        return false;
    }

    const int sourceIndex = Draw::indexForPlayer(otherPair, 1);
    const int from = incompleteSource.last();
    const int to = incompleteTarget.first();
    const auto player = draw[from][sourceIndex];
    draw[to][Draw::indexForPlayer(pair, 2)] = player;
    draw[from][sourceIndex].clear();

    m_moved[player] = { to + 1, pair, 2 };
    return true;
}

QVector<int> AdjustDrawDialog::findSinglePairs(int pair, QVector<QVector<QString>> &draw) const
{
    QVector<int> singles;

    for (int i = 0; i < m_tables; i++) {
        if ( ! draw.at(i).at(Draw::indexForPair(pair)).isEmpty()
            && draw.at(i).at(Draw::indexForPair(Draw::otherPair(pair))).isEmpty()) {

            singles.append(i);
        }
    }

    return singles;
}

bool AdjustDrawDialog::compileOpponentPairs(int pair, QVector<QVector<QString>> &draw)
{
    const auto singlePairs = findSinglePairs(pair, draw);
    if (singlePairs.isEmpty()) {
        return false;
    }

    const int otherPair = Draw::otherPair(pair);
    const auto otherSinglePairs = findSinglePairs(otherPair, draw);
    if (otherSinglePairs.isEmpty()) {
        return false;
    }

    const int from = otherSinglePairs.last();
    const int to = singlePairs.first();
    const int index1 = Draw::indexForPlayer(otherPair, 1);
    const int index2 = Draw::indexForPlayer(otherPair, 2);
    const auto name1 = draw[from][index1];
    const auto name2 = draw[from][index2];

    draw[to][index1] = name1;
    draw[to][index2] = name2;
    draw[from][index1].clear();
    draw[from][index2].clear();

    m_moved[name1] = { to + 1, otherPair, 1 };
    if (! name2.isEmpty()) {
        m_moved[name2] = { to + 1, otherPair, 2 };
    }

    return true;
}

bool AdjustDrawDialog::compileSameTypePairs(int pair, QVector<QVector<QString>> &draw)
{
    const auto singlePairs = findSinglePairs(pair, draw);
    if (singlePairs.count() < 2) {
        return false;
    }

    const int otherPair = Draw::otherPair(pair);
    const int from = singlePairs.last();
    const int to = singlePairs.first();
    const int sourceIndex1 = Draw::indexForPlayer(pair, 1);
    const int sourceIndex2 = Draw::indexForPlayer(pair, 2);
    const auto name1 = draw[from][sourceIndex1];
    const auto name2 = draw[from][sourceIndex2];

    draw[to][Draw::indexForPlayer(otherPair, 1)] = name1;
    draw[to][Draw::indexForPlayer(otherPair, 2)] = name2;
    draw[from][sourceIndex1].clear();
    draw[from][sourceIndex2].clear();

    m_moved[name1] = { to + 1, otherPair, 1 };
    if (! name2.isEmpty()) {
        m_moved[name2] = { to + 1, otherPair, 2 };
    }

    return true;
}

bool AdjustDrawDialog::fillTableGaps(QVector<QVector<QString>> &draw)
{
    int firstEmpty = -1;
    for (int i = 0; i < m_tables; i++) {
        if (draw.at(i) == DrawMapper::emptyTable) {
            firstEmpty = i;
            break;
        }
    }

    if (firstEmpty == -1) {
        return false;
    }

    int lastFull = -1;
    for (int i = 0; i < m_tables; i++) {
        if (draw.at(i) != DrawMapper::emptyTable) {
            lastFull = i;
        }
    }

    if (firstEmpty > lastFull) {
        return false;
    }

    draw[firstEmpty] = draw[lastFull];
    draw[lastFull] = DrawMapper::emptyTable;

    for (int i = 0; i < 4; i++) {
        m_moved[draw[firstEmpty].at(i)] = { firstEmpty + 1,
                                            Draw::pairForIndex(i),
                                            Draw::playerForIndex(i) };
    }
    return true;
}

void AdjustDrawDialog::listWasModified()
{
    // This can only happen via a network change
    QTimer::singleShot(0, this, [this]
    {
        QMessageBox::warning(this,
            tr("Anmeldungsliste wurde verändert"),
            tr("<p><b>Anmeldungsliste wurde verändert</b></p>"
               "<p>Ein anderer Netzwerkteilnehmer hat eine Änderung an der Anmeldeliste "
               "vorgenommen. Potenziell hat sich dadurch die Anzahl der Anmeldungen und/oder der "
               "Status der Auslosung verändert, so dass evtl. die vorgeschlagenen Änderungen nicht "
               "mehr aktuell bzw. durchführbar sind.</p>"
               "<p>Das Fenster wird jetzt geschlossen. Bitte zum Bereinigen der Auslosung den "
               "Dialog neu aufrufen!</p>"));
        reject();
    });
}

void AdjustDrawDialog::printList()
{
    QApplication::setOverrideCursor(Qt::WaitCursor);

    const auto [ result, fileName ] = m_tmpHelper->getFileName(tr("Bereinigung_Auslosung.html"));
    if (result != TemporaryFileHelper::FileCreated) {
        QApplication::restoreOverrideCursor();
        m_tmpHelper->displayErrorMessage(this, result);
        TitleDialog::accept();
        return;
    }

    QFile html(fileName);
    html.open(QIODevice::WriteOnly);

    QTextStream outStream(&html);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    outStream.setCodec(QTextCodec::codecForName("UTF-8"));
#endif

    outStream << "<!DOCTYPE html>\n"
              << "<html lang=\"de\">\n"
              << "<head>\n"
              << "<meta charset=\"utf-8\"/>\n"
              << "<style>\n"
              << "h1 { font-size: 140%; }\n"
              << "table { border-spacing: 0px; }\n"
              << "th, td { padding: 0.2em 0.3em 0.2em; text-align: left; }\n"
              << "thead td { font-weight: bold; }\n"
              << "tbody td { border-top: 1px solid black; }\n"
              << "</style>\n"
              << tr("<title>Nötige Änderungen an der Auslosung</title>\n")
              << "</head>\n"
              << "<body>\n"
              << tr("<h1>Nötige Änderungen an der Auslosung</h1>\n")
              << "<table>\n"
              << "<thead>\n"
              << "<tr>";

    const int columnCount = m_changes->columnCount();

    for (int col = 0; col < columnCount; col++) {
        outStream << QStringLiteral("<td>%1</td>").arg(
                                    m_changes->horizontalHeaderItem(col)->text());
    }

    outStream << "</tr>\n"
              << "</thead>\n"
              << "<tbody>\n";

    for (int row = 0; row < m_changes->rowCount(); row++) {
        outStream << "<tr>";
        for (int col = 0; col < columnCount; col++) {
            outStream << QStringLiteral("<td>%1</td>").arg(
                                        m_changes->item(row, col)->text().toHtmlEscaped());
        }
        outStream << "</tr>\n";
    }

    outStream << "</tbody>\n"
              << "</table>\n"
              << "</body>\n"
              << "</html>\n";

    html.close();

    QApplication::restoreOverrideCursor();

    if (! QDesktopServices::openUrl(QUrl::fromLocalFile(fileName))) {
        QMessageBox::warning(this, tr("Rangliste drucken"),
            tr("<p><b>Automatisches Öffnen fehlgeschlagen</b></p>"
               "<p>Die exportierte HTML-Datei</p>"
               "<p>%1</p>"
               "<p>konnte nicht automatisch mit dem Standardwebbrowser geöffnet werden. Bitte die "
               "Datei manuell öffnen und drucken.</p>").arg(
               QDir::toNativeSeparators(fileName).toHtmlEscaped()));
    }
}
