# SPDX-FileCopyrightText: 2020-2024 Tobias Leupold <tl at stonemx dot de>
#
# SPDX-License-Identifier: BSD-2-Clause

option(NINJA_COLORS "Use colored output for GCC/Clang when building with Ninja" ON)

if(CMAKE_GENERATOR STREQUAL "Ninja" AND NINJA_COLORS)
    if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
        message(STATUS "Enabling colored output for GCC invoked by Ninja")
        add_compile_options("-fdiagnostics-color=always")
    elseif(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
        message(STATUS "Enabling colored output for Clang invoked by Ninja")
        add_compile_options("-fcolor-diagnostics")
    endif()
endif()
