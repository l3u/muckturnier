# SPDX-FileCopyrightText: 2024 Tobias Leupold <tl@stonemx.de>
#
# SPDX-License-Identifier: BSD-2-Clause

# DESCVAR is the parent scope's variable the date will be stored in.
# DATEFORMAT is passed to git's --date option, e.g. "iso-strict" or "format:%d.%m.%Y".
# A third argument can be passed if we want to get the last commit date of a specific file.

function(git_get_last_commit_date DESCVAR DATEFORMAT)
    find_package(Git QUIET)
    if (NOT GIT_FOUND)
        message(WARNING "git_get_last_commit_date: Could not find package git!")
        set(${DESCVAR} "-NOTFOUND" PARENT_SCOPE)
        return()
    endif()

    execute_process(COMMAND
        "${GIT_EXECUTABLE}" --no-pager log -1 --format=%ad --date=${DATEFORMAT} -- ${ARGV2}
        WORKING_DIRECTORY "${BASE_DIR}"
        RESULT_VARIABLE git_result
        OUTPUT_VARIABLE git_output
        ERROR_VARIABLE  git_error
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )

    if (git_result EQUAL 0)
        set(${DESCVAR} "${git_output}" PARENT_SCOPE)
    else()
        message(WARNING "git_get_last_commit_date: Error invoking git: ${git_error}")
        set(${DESCVAR} "-NOTFOUND" PARENT_SCOPE)
    endif()
endfunction()
