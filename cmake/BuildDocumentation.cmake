# SPDX-FileCopyrightText: 2017-2024 Tobias Leupold <tl@stonemx.de>
#
# SPDX-License-Identifier: BSD-2-Clause

if (EXISTS "${BASE_DIR}/.git")
    find_package(Git QUIET)
endif()

set(RSTFILE "${BASE_DIR}/doc/Readme.rst")
set(CSSFILE "${BASE_DIR}/doc/Readme.css")

if (GIT_FOUND)
    # Get the last change date for the readme file
    include(${CMAKE_CURRENT_LIST_DIR}/LastCommitDate.cmake)
    git_get_last_commit_date(LASTCHANGE "format:%d.%m.%Y" ${RSTFILE})

    # Get the current git description string
    include(${CMAKE_CURRENT_LIST_DIR}/GitDescription.cmake)
    git_get_description(VERSION GIT_ARGS)

    # Set the respective values in a copy of the readme sources we will be compiling
    message(STATUS "Setting the Readme's date to ${LASTCHANGE} and version info to ${VERSION}")
    configure_file(${RSTFILE} ${CMAKE_BINARY_DIR}/Readme.rst)
    set(RSTFILE "Readme.rst")
else()
    message(STATUS "Using the released RST Readme")
endif()

# On some machines, minimal.css is not found without an absolute path.
# We thus try to provide an absolute path.

# First, we try to find minimal.css in /usr/share, where e.g. Debian puts it
set(MINIMALCSS "/usr/share/docutils/writers/html5_polyglot/minimal.css")

# Then, we check if it's in the same directory as the code itself, like on Gentoo
if (NOT EXISTS "${MINIMALCSS}")
    execute_process(COMMAND python3 -c "import docutils; print(docutils.__path__[0])"
                    OUTPUT_VARIABLE DOCUTILSROOT
                    OUTPUT_STRIP_TRAILING_WHITESPACE)
    set(MINIMALCSS "${DOCUTILSROOT}/writers/html5_polyglot/minimal.css")
endif()

# If it's still not found, we simply reference the file without a full path
# ... and continue with fingers crossed ;-)
if (NOT EXISTS "${MINIMALCSS}")
    set(MINIMALCSS "minimal.css")
endif()

message(STATUS "Converting the Readme from RST to HTML")
execute_process(COMMAND ${RST2HTML5} --stylesheet=${MINIMALCSS},${CSSFILE}
                        "${RSTFILE}" "share/muckturnier/Readme.html"
                WORKING_DIRECTORY ${CMAKE_BINARY_DIR})
