# SPDX-FileCopyrightText: 2012-2018 Johannes Zarl-Zierl <johannes@zarl-zierl.at>
# SPDX-FileCopyrightText: 2024 Tobias Leupold <tl@stonemx.de>
#
# SPDX-License-Identifier: BSD-3-Clause

if (NOT DEFINED BASE_DIR)
    message(FATAL_ERROR "UpdateVersion.cmake: BASE_DIR not set. "
                        "Please supply base working directory!")
endif()

# Version header locations
set(RELEASE_DATE_H_RELEASE   ${BASE_DIR}/src/releaseDate.h)
set(RELEASE_DATE_H_IN        ${BASE_DIR}/src/releaseDate.h.in)
set(RELEASE_DATE_H_CURRENT   ${CMAKE_BINARY_DIR}/releaseDate.h.current)
set(RELEASE_DATE_H_GENERATED ${CMAKE_BINARY_DIR}/releaseDate.h)

if (EXISTS ${RELEASE_DATE_H_RELEASE})
    # A ready to use releaseDate.h has been found, so we're compiling a release tarball
    message(STATUS "Using the released releaseDate.h")
else()
    # We're not working on a release tarball and have to generate a releaseDate.h
    if (EXISTS "${BASE_DIR}/.git" AND EXISTS "${RELEASE_DATE_H_IN}")
        include(${CMAKE_CURRENT_LIST_DIR}/LastCommitDate.cmake)
        git_get_last_commit_date(RELEASE_DATE "iso-strict")
        if (NOT RELEASE_DATE)
            set(RELEASE_DATE "unknown")
        endif()

        message(STATUS "Updating release date information to ${RELEASE_DATE}")
        # Write the release date info to a temporary file
        configure_file(${RELEASE_DATE_H_IN} ${RELEASE_DATE_H_CURRENT})
        # Update the real header if the info changed
        execute_process(COMMAND ${CMAKE_COMMAND} -E copy_if_different
                                ${RELEASE_DATE_H_CURRENT} ${RELEASE_DATE_H_GENERATED})
        # Make sure the info doesn't get stale
        file(REMOVE ${RELEASE_DATE_H_CURRENT})

    else()
        message(SEND_ERROR "The generated file \"releaseDate.h\" does not exist!")
        message(AUTHOR_WARNING "When creating a release tarball, be sure to include a "
                               "releaseDate.h file including the correct release date!")
    endif()
endif()
