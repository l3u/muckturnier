.. SPDX-FileCopyrightText: 2024-2025 Tobias Leupold <tl@stonemx.de>

   SPDX-License-Identifier: CC-BY-SA-4.0

Version 2 (v3.9.0)
==================

The datasets to be encoded use an ASCII subset, leaving out some characters that may cause problems
when hardware barcode scanners are used. Technically, any 2D barcode that is able to encode a
sufficient amount of ASCII/alphanumeric chars can be used to generate booking codes, as long as the
scanner intended to be used can decode the respective barcode.

At least, 34 chars are needed for a dataset and at most, it can consume 2622 chars (see below). In
real life, a full pair's name including first and last names takes around 35 chars. Depending on the
tournament name chosen, one would expect the datasets to be around 80 chars long.

QR codes can store up to 4296 alphanumeric chars, Aztec up to 3067. Data Matrix with at most 2335
and PDF417 with at most 1850 alphanumeric chars would still be way sufficient for what one would
expect to be needed, however, they would not be able to store the techical maximum length of a
booking code dataset. Codablock F with a maximum of 2725 alphanumeric chars would cover all cases
but is not very common.

Most (if not all) 2D barcode scanners can read QR codes by default. Also, the storage capacity for
alphanumeric chars of QR codes is way bigger than the maximum dataset length, even at a high
redundancy level. With Project Nayuki providing the outstanding QR Code Generator library
`qrcodegen <https://www.nayuki.io/page/qr-code-generator-library>`_, QR codes were the obvious
choice for encoding booking datasets in Muckturnier.

All strings to be used are encoded in UTF-8. Before being stored in the code, a string is escaped so
that only printable ASCII characters (with some exceptions, see below) are actually used. This way,
the whole Unicode range is usable and storable. However, non-ASCII characters "cost" at least 6
chars: We need at least two bytes, represented by two times a ``%`` followed by two chars. But we
try to cheap out as much escaping as possible by swapping some characters (see below).


Dataset definition
------------------

A dataset consists of the following parts:

- The header identifying the code: ``MTBC``, "MuckTurnier Booking Code" (4 chars)

- The version number, represented as ASCII-encoded base-36 (2 chars)

- The length of the escaped tournament name, represented as ASCII-encoded base-36 (2 chars)

- The length of the escaped pair or player name, represented as ASCII-encoded base-36 (2 chars)

- The escaped tournament name (maximum length: 1295 chars)

- The escaped pair or player name (maximum length: 1295 chars)

- The checksum: a Base64-encoded MD5 hash without trailing ``=`` chars (22 chars).

Consequently, a dataset is at least 34 and at most 2622 chars long, with a realistic length of
around 80 chars.


Escaping
--------

For the ideas behind this serialization protocol cf. the "Escaping considerations" comment in
``BookingPage/BookingEngine.cpp``.

The actual escaping is done via URI/URL style percent encoding, with some special settings. It is
done by ``QByteArray::toPercentEncoding()``, after decoding the strings to bytes via
``QString::toUtf8()``.

All printable ASCII characters (including spaces) are allowed and remain unescaped, except for:

- ``_`` (used to replace spaces after escaping)

- ``%`` (used for the escaping itself)

- ````` and ``^`` (could be interpreted as a combining character during input, e.g. ``^e`` would
  become ``ê`` or ```e`` would become ``è``)

- ``\`` (may be interpreted as an escaping character during input)

To cheap out as much escaping as possible (it's expensive, cf. above), we swap non-ASCII chars
likely to be found in names with ASCII chars unlikely to be found in names before doing the percent
encoding. When unescaping, those chars are simply swapped again to get the original string.

The following swapping is done:

- ``ä`` with ``!``

- ``ö`` with ``"``

- ``ü`` with ``#``

- ``Ä`` with ``$``

- ``Ö`` with ``(``

- ``Ü`` with ``)``

- ``ß`` with ``*``

- ``é`` with ``,``

- ``è`` with ``:``

- ``ê`` with ``;``

- ``ë`` with ``<``

- ``á`` with ``=``

- ``à`` with ``>``

- ``â`` with ``?``

- ``ó`` with ``@``

- ``ò`` with ``[``

- ``ñ`` with ``]``

- ``É`` with ``{``

- ``È`` with ``|``

- ``Ê`` with ``}``

- ``’`` (a typographically correct apostrophe) with ``~``

The swapped string is then percent-encoded. We explicitely do not encode
``!"#$()*,:;<=>?@[]{|}~&'+-./`` and spaces, But we do explicitely encode the underscore ``_``.

Some hardware barcode scanners simply ignore spaces and skip them whilst entering the data. We thus
replace all spaces with underscores after having done the percent-encoding. Before decoding and
un-swapping a scanned string, all ``_`` chars are swapped back to spaces.

All escaped strings must not exceed 1295 characters, as we encode their lengths using two chars
representing a base-36 encoded int (and the maximal number that can be encoded in this case is
1295, represented by ``zz``).


Checksum calculation
--------------------

The checksum is calculated from the original non-escaped UTF-8 strings. The data is joined into one
string separated by one space character each time in the following order:

- The tournament name

- The pair or player name

- The security key

Then, the string is decoded to bytes using ``QString::toUtf8()``, and it's MD5 sum is calculated.
This is done using ``QCryptographicHash(QCryptographicHash::Md5)``. The ASCII hex representation
(two chars per byte) of the checksum is stored in the database (via ``QByteArray::toHex()``). For
the code, we use the Base64 representation without trailing ``=`` chars. This saves us 10 chars
compared to the ASCII hex representation (via
``QByteArray::toBase64(QByteArray::Base64Encoding | QByteArray::OmitTrailingEquals)``).

The checksum serves two purposes: Checking the data integrity and preventing people from creating
their own booking code and claiming they did book when they didn't.


Security considerations
-----------------------

A booking code is only considered valid if the checksum contained in the code is equal to the hash
calculated from the contained data plus the secret security key from the database. If the checksums
differ, a big warning is displayed after having processed the code.

MD5 has been broken long time ago in a cryptographic context. But we only have a very limited range
of data input, so we can virtually exclude a collision, no matter what we put in. Also, we can
exclude the use of rainbow tables because of the security key being a part of the input data.

The security key is by default initialized as a 20 char completely random printable ASCII range
string, excluding only spaces, quotes and the backslash (so that we won't get any problem if we back
up the key to a text file). Assumed the user leaves the security key as-is, there are more than
1.5e+39 possibilities for the key. That is a number with 40 decimal places.

Even if you have a valid QR code and start a brute force attack to calculate the key by finding the
right MD5 sum, you won't be finished until the tournament is long over (if ever). Generating a valid
booking code without knowing the security key is virtually impossible if the key is long enough, and
the default 20 chars should be way more than enough.


Differences to version 1 (v3.7.0)
=================================

In version 1, both underscores (``_``) and spaces were encoded as-is. This lead to problems with
some hardware barcode scanners (e.g. the Sunmi Blink) which ignore spaces and simply skip them
whilst entering the dataset.

With version 2, all spaces are replaced with underscores after percent-encoding to work around this
issue.

As the ``_`` was used to represent ``É``, the following changes have been done to "free" it for the
new use:

- ``É`` is now represented by ``{`` (instead of ``_``)

- ``È`` is now represented by ``|`` (instead of ``{``)

- ``Ê`` is now represented by ``}`` (instead of ``|``)

- ``Á`` has been removed from the swapping list
