.. SPDX-FileCopyrightText: 2017-2025 Tobias Leupold <tl@stonemx.de>

   SPDX-License-Identifier: CC-BY-SA-4.0

DB_VERSION 14 (v3.9)
====================

* Moved the booking settings from the serialized string representation to a JSON object, like it's
  done with all other sub-settings

* Added the new "revoked_bookings" table

* New create statement:

  .. code-block::

    PRAGMA application_id = 1299538795;
    PRAGMA user_version = (the currently used DB_VERSION);

    CREATE TABLE config(
        key TEXT,
        value TEXT
    );

    CREATE TABLE players(
        id INTEGER PRIMARY KEY,
        name TEXT,
        marker INTEGER,
        disqualified INTEGER,
        number INTEGER,
        booking_checksum TEXT
    );

    CREATE TABLE markers(
        id INTEGER PRIMARY KEY,
        name TEXT,
        color INTEGER,
        sorting INTEGER,
        sequence INTEGER
    );

    CREATE TABLE draw(
        round INTEGER,
        pair_or_player INTEGER,
        table_number INTEGER,
        pair_number INTEGER,
        player_number INTEGER
    );

    CREATE TABLE assignment(
        round INTEGER,
        pair_or_player INTEGER,
        table_number INTEGER,
        pair_number INTEGER,
        player_number INTEGER
    );

    CREATE TABLE score(
        round INTEGER,
        table_number INTEGER,
        booger INTEGER,
        pair_or_player INTEGER,
        score INTEGER
    );

    CREATE TABLE schedule(
        round INTEGER PRIMARY KEY,
        start TEXT,
        end TEXT
    );

    CREATE TABLE revoked_bookings(
        checksum TEXT PRIMARY KEY,
        revoked TEXT,
        reason TEXT
    );

    CREATE TRIGGER cleanup_draw AFTER DELETE ON players
    FOR EACH ROW
    BEGIN
        DELETE FROM draw WHERE pair_or_player = OLD.id;
    END;

    CREATE TRIGGER fix_player2_draw AFTER DELETE ON draw
    FOR EACH ROW WHEN OLD.player_number = 1
    BEGIN
        UPDATE draw SET player_number = 1
        WHERE round = OLD.round
        AND table_number = OLD.table_number
        AND pair_number = OLD.pair_number
        AND player_number = 2;
    END;


DB_VERSION 13 (v3.8)
====================

* Added an own "draw" table

* Reordered the columns of the "assignment" table to match the new "draw" table

* Updated the "draw cleanup" triggers to work on the new "draw" table

* New create statement:

  .. code-block::

    PRAGMA application_id = 1299538795;
    PRAGMA user_version = (the currently used DB_VERSION);

    CREATE TABLE config(
        key TEXT,
        value TEXT
    );

    CREATE TABLE players(
        id INTEGER PRIMARY KEY,
        name TEXT,
        marker INTEGER,
        disqualified INTEGER,
        number INTEGER,
        booking_checksum TEXT
    );

    CREATE TABLE markers(
        id INTEGER PRIMARY KEY,
        name TEXT,
        color INTEGER,
        sorting INTEGER,
        sequence INTEGER
    );

    CREATE TABLE draw(
        round INTEGER,
        pair_or_player INTEGER,
        table_number INTEGER,
        pair_number INTEGER,
        player_number INTEGER
    );

    CREATE TABLE assignment(
        round INTEGER,
        pair_or_player INTEGER,
        table_number INTEGER,
        pair_number INTEGER,
        player_number INTEGER
    );

    CREATE TABLE score(
        round INTEGER,
        table_number INTEGER,
        booger INTEGER,
        pair_or_player INTEGER,
        score INTEGER
    );

    CREATE TABLE schedule(
        round INTEGER PRIMARY KEY,
        start TEXT,
        end TEXT
    );

    CREATE TRIGGER cleanup_draw AFTER DELETE ON players
    FOR EACH ROW
    BEGIN
        DELETE FROM draw WHERE pair_or_player = OLD.id;
    END;

    CREATE TRIGGER fix_player2_draw AFTER DELETE ON draw
    FOR EACH ROW WHEN OLD.player_number = 1
    BEGIN
        UPDATE draw SET player_number = 1
        WHERE round = OLD.round
        AND table_number = OLD.table_number
        AND pair_number = OLD.pair_number
        AND player_number = 2;
    END;


DB_VERSION 12 (v3.7)
====================

* Added the ``number`` column to the players table.

* Added the ``booking_checksum`` column to the players table.

* Added the new ``fix_player2_draw`` trigger that moves "player 2" assignments in the preassignment
  to "player 1" if the current player 1's draw is deleted (also if the player itself is deleted).

* New create statement:

  .. code-block::

    PRAGMA application_id = 1299538795;
    PRAGMA user_version = (the currently used DB_VERSION);

    CREATE TABLE config(
        key TEXT,
        value TEXT
    );

    CREATE TABLE players(
        id INTEGER PRIMARY KEY,
        name TEXT,
        marker INTEGER,
        disqualified INTEGER,
        number INTEGER,
        booking_checksum TEXT
    );

    CREATE TABLE markers(
        id INTEGER PRIMARY KEY,
        name TEXT,
        color INTEGER,
        sorting INTEGER,
        sequence INTEGER
    );

    CREATE TABLE assignment(
        round INTEGER,
        pair_or_player INTEGER,
        pair_number INTEGER,
        player_number INTEGER,
        table_number INTEGER
    );

    CREATE TABLE score(
        round INTEGER,
        table_number INTEGER,
        booger INTEGER,
        pair_or_player INTEGER,
        score INTEGER
    );

    CREATE TABLE schedule(
        round INTEGER PRIMARY KEY,
        start TEXT,
        end TEXT
    );

    CREATE TRIGGER cleanup_preassignment AFTER DELETE ON players
    FOR EACH ROW
    BEGIN
        DELETE FROM assignment WHERE pair_or_player = OLD.id;
    END;

    CREATE TRIGGER fix_player2_draw AFTER DELETE ON assignment
    FOR EACH ROW WHEN OLD.round = 0 AND OLD.player_number = 1
    BEGIN
        UPDATE assignment SET player_number = 1
        WHERE round = 0
        AND table_number = OLD.table_number
        AND pair_number = OLD.pair_number
        AND player_number = 2;
    END;


DB_VERSION 11 (v3.6)
====================

* Added the new ``schedule`` table.

* New create statement:

  .. code-block::

    PRAGMA application_id = 1299538795;
    PRAGMA user_version = (the currently used DB_VERSION);

    CREATE TABLE config(
        key TEXT,
        value TEXT
    );

    CREATE TABLE players(
        id INTEGER PRIMARY KEY,
        name TEXT,
        marker INTEGER,
        disqualified INTEGER
    );

    CREATE TABLE markers(
        id INTEGER PRIMARY KEY,
        name TEXT,
        color INTEGER,
        sorting INTEGER,
        sequence INTEGER
    );

    CREATE TABLE assignment(
        round INTEGER,
        pair_or_player INTEGER,
        pair_number INTEGER,
        player_number INTEGER,
        table_number INTEGER
    );

    CREATE TABLE score(
        round INTEGER,
        table_number INTEGER,
        booger INTEGER,
        pair_or_player INTEGER,
        score INTEGER
    );

    CREATE TABLE schedule(
        round INTEGER PRIMARY KEY,
        start TEXT,
        end TEXT
    );

    CREATE TRIGGER cleanup_preassignment AFTER DELETE ON players
    FOR EACH ROW
    BEGIN
        DELETE FROM assignment WHERE pair_or_player = OLD.id;
    END;


DB_VERSION 10 (v3.5)
====================

* Added a ``disqualified`` column to the ``players`` table.

* New create statement:

  .. code-block::

    PRAGMA application_id = 1299538795;
    PRAGMA user_version = (the currently used DB_VERSION);

    CREATE TABLE config(
        key TEXT,
        value TEXT
    );

    CREATE TABLE players(
        id INTEGER PRIMARY KEY,
        name TEXT,
        marker INTEGER,
        disqualified INTEGER
    );

    CREATE TABLE markers(
        id INTEGER PRIMARY KEY,
        name TEXT,
        color INTEGER,
        sorting INTEGER,
        sequence INTEGER
    );

    CREATE TABLE assignment(
        round INTEGER,
        pair_or_player INTEGER,
        pair_number INTEGER,
        player_number INTEGER,
        table_number INTEGER
    );

    CREATE TABLE score(
        round INTEGER,
        table_number INTEGER,
        booger INTEGER,
        pair_or_player INTEGER,
        score INTEGER
    );

    CREATE TRIGGER cleanup_preassignment AFTER DELETE ON players
    FOR EACH ROW
    BEGIN
        DELETE FROM assignment WHERE pair_or_player = OLD.id;
    END;


DB_VERSION 9 (v3.4)
===================

* Introduced the ``SETTINGS_VERSION`` config key, along with a ``TournamentSettings`` revision
  counter. Using this, it's now possible to view newer databases only introducing
  downwards-compatible settings changes for new features with older Muckturnier versions.

* The available config keys are now:

  - ``DB_IDENTIFIER`` (string)
  - ``DB_VERSION`` (int)
  - ``SETTINGS`` (JSON data as string)
  - ``SETTINGS_VERSION`` (int)


DB_VERSION 8 (v3.3)
===================

* We now set the application_id and user_version header fields using :code:`PRAGMA` calls when
  creating a new database. The application_id is set to ``1299538795`` (= ``0x4d75636b`` =
  ``Muck``), and the ``user_version`` is set to the ``DB_VERSION`` config value.

* Added the ``DB_IDENTIFIER`` config key so that identifying a tournament database becomes easier.

* The tournament settings are now stored as a JSON object, using the new ``SETTINGS`` key.

* Added a trigger to cleanup the preassignment if a pair/player is deleted

* New create statement:

  .. code-block::

    PRAGMA application_id = 1299538795;
    PRAGMA user_version = (the currently used DB_VERSION);

    CREATE TABLE config(
        key TEXT,
        value TEXT
    );

    CREATE TABLE players(
        id INTEGER PRIMARY KEY,
        name TEXT,
        marker INTEGER
    );

    CREATE TABLE markers(
        id INTEGER PRIMARY KEY,
        name TEXT,
        color INTEGER,
        sorting INTEGER,
        sequence INTEGER
    );

    CREATE TABLE assignment(
        round INTEGER,
        pair_or_player INTEGER,
        pair_number INTEGER,
        player_number INTEGER,
        table_number INTEGER
    );

    CREATE TABLE score(
        round INTEGER,
        table_number INTEGER,
        booger INTEGER,
        pair_or_player INTEGER,
        score INTEGER
    );

    CREATE TRIGGER cleanup_preassignment AFTER DELETE ON players
    FOR EACH ROW
    BEGIN
        DELETE FROM assignment WHERE pair_or_player = OLD.id;
    END;

* The available config keys are now:

  - ``DB_IDENTIFIER`` (string)
  - ``DB_VERSION`` (int)
  - ``SETTINGS`` (JSON data as string)


DB_VERSION 7 (v0.7.7)
=====================

* Added the new ``markers`` table along with a respective column in the ``players`` table.

* New create statement:

  .. code-block::

    CREATE TABLE config(
        key TEXT,
        value TEXT
    );

    CREATE TABLE players(
        id INTEGER PRIMARY KEY,
        name TEXT,
        marker INTEGER
    );

    CREATE TABLE markers(
        id INTEGER PRIMARY KEY,
        name TEXT,
        color INTEGER,
        sorting INTEGER,
        sequence INTEGER
    );

    CREATE TABLE assignment(
        round INTEGER,
        pair_or_player INTEGER,
        pair_number INTEGER,
        player_number INTEGER,
        table_number INTEGER
    );

    CREATE TABLE score(
        round INTEGER,
        table_number INTEGER,
        booger INTEGER,
        pair_or_player INTEGER,
        score INTEGER
    );


DB_VERSION 6 (v0.7.5)
=====================

* Added the display options ``NO_TABLE_NUMBERS``, ``AUTO_SELECT_PAIRS`` and
  ``INCLUDE_OPPONENT_GOALS`` to the ``config`` table. All are boolean values represented by a
  literal ``0`` or ``1`` string.

* The available config keys are now:

  - ``MUCKTURNIER_VERSION`` (string)
  - ``DB_VERSION`` (int)
  - ``TOURNAMENT_MODE`` (``FIXED_PAIRS|SINGLE_PLAYERS``)
  - ``BOOGER_SCORE`` (int)
  - ``BOOGERS_PER_ROUND`` (int)
  - ``NO_TABLE_NUMBERS`` (``0|1``)
  - ``AUTO_SELECT_PAIRS`` (``0|1``)
  - ``INCLUDE_OPPONENT_GOALS`` (``0|1``)


DB_VERSION 5 (v0.7.4)
=====================

* Renamed the config key ``GAMES_PER_ROUND`` to ``BOOGERS_PER_ROUND``.

* Renamed the ``score`` table's ``game`` column to ``booger``.

* New create statement:

  .. code-block::

    CREATE TABLE config(
        key TEXT,
        value TEXT
    );

    CREATE TABLE players(
        id INTEGER PRIMARY KEY,
        name TEXT
    );

    CREATE TABLE assignment(
        round INTEGER,
        pair_or_player INTEGER,
        pair_number INTEGER,
        player_number INTEGER,
        table_number INTEGER
    );

    CREATE TABLE score(
        round INTEGER,
        table_number INTEGER,
        booger INTEGER,
        pair_or_player INTEGER,
        score INTEGER
    );

* The available config keys are now:

  - ``DB_VERSION`` (int)
  - ``MUCKTURNIER_VERSION`` (string)
  - ``TOURNAMENT_MODE`` (``FIXED_PAIRS|SINGLE_PLAYERS``)
  - ``BOOGER_SCORE`` (int)
  - ``BOOGERS_PER_ROUND`` (int)


DB_VERSION 4 (v0.7.3)
=====================

* Added the ``player_number`` column to ``assignment``.

* New create statement:

  .. code-block::

    CREATE TABLE config(
        key TEXT,
        value TEXT
    );

    CREATE TABLE players(
        id INTEGER PRIMARY KEY,
        name TEXT
    );

    CREATE TABLE assignment(
        round INTEGER,
        pair_or_player INTEGER,
        pair_number INTEGER,
        player_number INTEGER,
        table_number INTEGER
    );

    CREATE TABLE score(
        round INTEGER,
        table_number INTEGER,
        game INTEGER,
        pair_or_player INTEGER,
        score INTEGER
    );


DB_VERSION 3 (v0.5.1)
=====================

* Renamed the ``pairs`` table to ``players``.

* Renamed ``pair`` in ``assignment`` and ``score`` to ``pair_or_player``.

* New create statement:

  .. code-block::

    CREATE TABLE config(
        key TEXT,
        value TEXT
    );

    CREATE TABLE players(
        id INTEGER PRIMARY KEY,
        name TEXT
    );

    CREATE TABLE assignment(
        round INTEGER,
        pair_or_player INTEGER,
        pair_number INTEGER,
        table_number INTEGER
    );

    CREATE TABLE score(
        round INTEGER,
        table_number INTEGER,
        game INTEGER,
        pair_or_player INTEGER,
        score INTEGER
    );


DB_VERSION 2 (v0.5)
===================

* No bump to the database revision has been done, although this would have been obligatory because:

* The following config keys have been renamed:

  - ``DB_MUCKTURNIER_VERSION`` to ``MUCKTURNIER_VERSION``
  - ``DB_DB_VERSION`` to ``DB_VERSION``

* The possible values for ``TOURNAMENT_MODE`` have been changed from lower to upper case:

  - ``fixed_pairs`` to ``FIXED_PAIRS``
  - ``single_players`` to ``SINGLE_PLAYERS``

* The available config keys are now:

  - ``DB_VERSION`` (int)
  - ``MUCKTURNIER_VERSION`` (string)
  - ``TOURNAMENT_MODE`` (``FIXED_PAIRS|SINGLE_PLAYERS``)
  - ``BOOGER_SCORE`` (int)
  - ``GAMES_PER_ROUND`` (int)


Somewhat DB_VERSION 2 (called "DB_DB_VERSION", v0.4.1)
======================================================

* Added the following config keys:

  - ``DB_MUCKTURNIER_VERSION`` (string)
    Holds the Muckturnier version the database has been created with.
  - ``DB_DB_VERSION`` (int)
    Introduced a database scheme revision number, starting with revision 2.

* The available config keys are now:

  - ``DB_DB_VERSION`` (int)
  - ``DB_MUCKTURNIER_VERSION`` (string)
  - ``TOURNAMENT_MODE`` (``fixed_pairs|single_players``)
  - ``BOOGER_SCORE`` (int)
  - ``GAMES_PER_ROUND`` (int)


Pre-DB_VERSION (v0.4)
=====================

* Added the config key ``GAMES_PER_ROUND`` (int), holding the number of boogers to play in one
  round. Previously, that number has been hard-coded to 2.

* The available config keys are now:

  - ``TOURNAMENT_MODE`` (``fixed_pairs|single_players``)
  - ``BOOGER_SCORE`` (int)
  - ``GAMES_PER_ROUND`` (int)


Pre-DB_VERSION (v0.3)
=====================

* Added a ``config`` table with the following keys:

  - | ``TOURNAMENT_MODE`` (``fixed_pairs|single_players``)
    | Holds the tournament mode (fixed pairs or single players).
  - | ``BOOGER_SCORE`` (int)
    | Holds the winning score for a booger.

* New create statement:

  .. code-block::

    CREATE TABLE config(
        key TEXT,
        value TEXT
    );

    CREATE TABLE pairs(
        id INTEGER PRIMARY KEY,
        name TEXT
    );

    CREATE TABLE assignment(
        round INTEGER,
        pair INTEGER,
        pair_number INTEGER,
        table_number INTEGER
    );

    CREATE TABLE score(
        round INTEGER,
        table_number INTEGER,
        game INTEGER,
        pair INTEGER,
        score INTEGER
    );


Pre-DB_VERSION (v0.1)
=====================

* First database layout to be published.

* Create statement:

  .. code-block::

    CREATE TABLE pairs(
        id INTEGER PRIMARY KEY,
        name TEXT
    );

    CREATE TABLE assignment(
        pair INTEGER,
        pair_number INTEGER,
        table_number INTEGER
    );

    CREATE TABLE score(
        round INTEGER,
        table_number INTEGER,
        game INTEGER,
        pair INTEGER,
        score INTEGER
    );
