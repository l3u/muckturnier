.. SPDX-FileCopyrightText: 2010-2025 Tobias Leupold <tl@stonemx.de>

   SPDX-License-Identifier: CC-BY-SA-4.0

.. section-numbering::

.. |nnbsp| unicode:: 0x202F .. narrow no-break space
   :trim:

.. |br| raw:: html

   <br>

.. |contact_email| raw:: html

    tl at stonemx dot de

.. meta::
   :viewport: width=device-width, initial-scale=1

===================
Muckturnier: readme
===================

:Autor: Tobias Leupold
:Homepage: https://muckturnier.org/
:Kontakt: |contact_email|
:Datum: @LASTCHANGE@
:Programmversion: @VERSION@

.. contents:: Inhaltsverzeichnis

Beschreibung des Programms
==========================

Was ist Muckturnier?
~~~~~~~~~~~~~~~~~~~~

Muckturnier ist die Komplettlösung für die Vorbereitung, Durchführung und Auswertung eines Muckturniers. Der Funktionsumfang reicht vom Erfassen von Voranmeldungen und dem Erstellen von Tischnummernaufklebern, Auslosungszetteln und Spielstandblöcken über das Anmelden der Paare bzw. Spieler vor Ort (wenn gewünscht auch incl. einer Auslosung direkt beim Anmelden), der zeitlichen Planung und Überwachung des Turnierverlaufs bis zur Ergebniseingabe und Auswertung. Eine aktuelle Rangliste steht jederzeit zur Verfügung.

Man kann so viele oder wenige Features benutzen, wie man will. Vom bloßen Eingeben von Ergebnissen zum Erleichtern der Auswertung bis hin zum Nutzen aller Vorteile, die eine digitale Voranmeldung und Auslosung mit sich bringen, ist alles möglich. Dabei ist es egal, ob es um ein kleines Muckturnier oder um eine Großveranstaltung geht. Es wurden mithilfe des Programms schon Muckturniere mit vier Tischen und welche mit weit über 200 Teilnehmern erfolgreich durchgeführt.

Es werden verschiedene Turniertypen unterstützt: Es können sowohl feste Paare als auch einzelne Spieler angemeldet werden. Die Paare, die gegeneinander antreten, können in jeder Runde ausgelost, oder nach einem festen Schema für den Turnierablauf zugeordnet werden. Die Anzahl der Runden, Bobbl pro Runde und Punkte pro Bobbl sind frei wählbar (vgl. `Regeln für das Turnier`_).

Insbesondere für große Turniere besteht die Möglichkeit, mehrere Rechner (per LAN oder WLAN) miteinander zu vernetzen. So können dann sowohl die `Anmeldung <register_multiple_>`_ als auch die `Auswertung <analyze_multiple_>`_ an mehreren Computern durchgeführt werden. Das beschleunigt die Anmeldung erheblich und bietet die Möglichkeit, Eingabefehler zu finden.

.. _register_multiple: `Anmeldung mit mehr als einem Rechner`_
.. _analyze_multiple: `Auswertung mit mehr als einem Rechner`_

Begriffsdefinitionen
~~~~~~~~~~~~~~~~~~~~

Um Unklarheiten zu beseitigen vorab ein paar Definitionen:

**Runde**
    Eine *Runde* ist ein Turnierabschnitt, der aus mehreren (oft zwei) *Bobbln* besteht. I. |nnbsp| d. |nnbsp| R. wird vor dem Turnierbeginn die zu spielende Anzahl an Runden festgelegt. Es kann eine Zeitbegrenzung pro Runde festgelegt werden (nach Ablauf der Zeit können Bobbl abgebrochen werden, vgl. `Abgebrochene Bobbl`_ und `Zeitbegrenzung`_).

**Bobbl**
    Der Bobbl hat zwei Bedeutungen: Zum einen steht er für einen Turnierabschnitt, der aus mehreren *Spielen* besteht. Den Bobbl hat das Team gewonnen, welches zuerst eine definierte Punktezahl (i. |nnbsp| d. |nnbsp| R. 21) erreicht. Diese wird normalerweise nicht mehr aufgeschrieben (es zählt das Gewinnen des Bobbls). |br|
    Der Verlierer *bekommt* den Bobbl (das ist die andere Bedeutung), einen dicken Punkt, der hinter den letzten Punktestand der Verlierer gemalt wird.

**Brille**
    Wird ein Bobbl zu Null gewonnen, wird oftmals zur Verdeutlichung ein doppelter Bobbl beim Verlierer gemalt, der mittels Steg und Bügeln zur *Brille* ausgestaltet wird. Eine Brille wird wie ein normaler Bobbl gezählt (ein doppeltes Werten einer Brille ist bei einem Muckturnier nicht üblich und wird daher nicht unterstützt).

**Spiel**
    Ein Spiel wird angesagt („Wenz“, „Schneidermuck“ etc.) und besteht, wenn mit dem üblichen kurzen Blatt gespielt wird, aus sechs Stichen. Es muss eine definierte Anzahl an Punkten (jede Karte hat einen definierten Punktwert) erreicht werden, um das Spiel zu gewinnen. |br|
    Aufgeschrieben werden nicht die durch die Stiche erreichten Punkte, sondern ein für das Spiel definierter Punktwert, der für gewöhnlich die Art und Weise der Ansage („Schneider“ oder „Schwarz“ angesagt) und den Ausgang des Spiels („Schneider“ oder „Schwarz“ gewonnen) berücksichtigt.

**Geschossene Tore**
    Die *geschossenen Tore* bezeichnen die Punkte, die das jeweilige Team in seinen *verlorenen Bobbln* erreicht hat. Diese Punkte werden bei der Auswertung berücksichtigt: Wenn mehrere Teams gleich viele Bobbl gewonnen haben, wird nach geschossenen Toren sortiert. Je mehr, desto besser: Viele Tore heißen viele Spiele gewonnen, also gut gespielt in den verlorenen Bobbln.

**Gegnerische geschossene Tore**
    Die *gegnerischen geschossenen Tore* bezeichnen die Punkte (also „geschossenen Tore“), die die Gegner jeweils in den eigenen gewonnenen Bobbln erreicht haben. Sind sowohl die gewonnenen Bobbl, als auch die geschossenen Tore gleich, dann können diese Punkte zum Aufstellen der Rangliste herangezogen werden: Je weniger Tore die Gegner geschossen haben, desto weniger Spiele haben sie gewonnen und desto besser hat das jeweilige Team in den gewonnenen Bobbln gespielt.

Regeln für das Turnier
~~~~~~~~~~~~~~~~~~~~~~

Grundsätzliche Regeln
---------------------

Die genauen Regeln („Wenz über Muck“, „Muck über Wenz“, mit oder ohne Geier, doppelt nach Einwerfen oder nicht etc.) sind für die Auswertung nicht von Belang, solang Bobbl bis zu einer definierten Punktzahl gespielt werden.

Das Turnier in mehrere Runden mit einer definierten Anzahl Bobbl pro Runde einzuteilen ist üblich, aber nicht zwingend nötig: Es kann auch nur eine Runde (dann also das ganze Turnier) mit entsprechend vielen Bobbln gespielt werden. Prinzipiell wäre auch nur ein einziger, großer Bobbl möglich.

Punktwertung
------------

Die Punktwertung ist unabhängig vom Turniertyp:

* Gespielt wird pro Runde eine definierte Anzahl Bobbl (evtl. auch nur eine große Runde).

* Jeder Bobbl wird bis zu einer definierten Zahl von Punkten gespielt. Gewonnen hat das Paar, das zuerst die nötige Punktezahl erreicht bzw. überschreitet. Das Verliererpaar *bekommt* den Bobbl.

* Der gewonnene Bobbl wird immer nur mit der für den Bobbl definierten Punktezahl gewertet – auch dann, wenn durch das letzte Spiel eigentlich mehr Punkte erreicht worden wären.

* „Brillen“ (doppelte Bobbl, wenn ein Bobbl zu Null gewonnen wird) werden nicht doppelt, sondern wie ein normaler Bobbl gewertet.

* Die in den verlorenen Bobbln erreichten Punkte bekommt das Verliererteam (resp. jeweils die Spieler) als „geschossene Tore“ (vgl. `Begriffsdefinitionen`_) gutgeschrieben.

Die vom Preisschafkopf/Schafkopfrennen bekannten Auswertungsverfahren „Plus“ und „Plus-Minus“ resp. das bloße Aufschreiben von Punkten eignen sich für das Auswerten eines Muckturniers nicht. Alle berücksichtigen nur die Ergebnisse der gespielten Spiele, nicht aber das Gewinnen von Bobbln. Aber genau darum geht es ja gerade beim Mucken!

Ein ausführlicher Artikel mit Rechenbeispielen zu „Plus“ und „Plus-Minus“ kann auf der `Muckturnier-Homepage <https://muckturnier.org/material/>`_ unter `Bobbl und Tore vs. Plus und Plus-Minus <https://muckturnier.org/news/2020/bobbl-und-tore-vs.-plus-und-plus-minus/>`_ nachgelesen werden.

Ranglistenerstellung
--------------------

Zu den Begrifflichkeiten „Bobbl“, „geschossene Tore“ und „gegnerische geschossene Tore“ siehe auch vgl. `Begriffsdefinitionen`_.

* Die Rangliste wird nach gewonnenen Bobbln aufgestellt (je mehr, desto besser).

* Bei gleich vielen gewonnenen Bobbln entscheiden die „geschossenen Tore“ über die Platzierung (je mehr, desto besser).

* Bei Gleichstand von Bobbln und Toren können weiterhin (optional) auch noch die „gegnerischen geschossenen Tore“ zur Unterscheidung der Platzierung herangezogen werden (je weniger, desto besser).

Auslosung und Turniermodus
--------------------------

Es kann sowohl ein Turnier mit festen Paaren als auch mit einzelnen Spielern ausgewertet werden.

Es kann eine Auslosung eingegeben werden, über die eine automatische Paar- und Tischauswahl (am einfachsten über die Tischnumer) erfolgen kann, und/oder die Auswahl kann aufgrund der bisher eingegebenen Ergebnisse erfolgen, vgl. `Automatische Auswahl`_.

Wenn nach dem Schema „Paar 1 bleibt sitzen, Paar 2 rückt einen Tisch weiter“ gespielt wird, dann kann für das ganze Turnier eine automatische Auswahl erfolgen und die Eingabe der Ergebnisse ist sehr schnell durchführbar.

Sofern die Auslosung vor dem Turnier bekannt ist und auch eingegeben (resp. mittels des Programms erzeugt) wurde, ist die automatische Auswahl bereits ab der 1. Runde möglich, ansonsten ab der 2., wenn die Auslosung über die Eingabe der Ergebnisse der 1. Runde klar ist.

Gibt es Beschränkungen?
~~~~~~~~~~~~~~~~~~~~~~~

Beschränkungen des Programms an sich
------------------------------------

Das Programm enthält keine gewollten Beschränkungen. Im Realeinsatz ist Muckturnier noch nie an irgendwelche Grenzen gestoßen.

Die Punkte pro Bobbl und die Anzahl der Bobbl pro Runde sind auf maximal 9 |nnbsp| 999 eingestellt (das Bedienelement braucht einen Maximalwert). Ich kann mir nicht vorstellen, dass jemand jemals mehr brauchen könnte, aber auch in dem Fall könnte man den Wert durch das Editieren der Datei :code:`shared/DefaultValues.h` (die alle hart kodierten Werte enthält) beliebig im Rahmen der Grenzen der Datentypen erhöhen. Natürlich muss Muckturnier danach `neu kompiliert <compile_>`_ werden.

.. _compile: `Übersetzen aus den Quelltexten`_

Nur der Vollständigkeit halber: Die verwendeten Datentypen setzen faktisch Grenzen. Allerdings wird man diese mit sehr großer Wahrscheinlichkeit niemals erreichen. Für Punktzahlen, Runden, IDs etc. wird beispielsweise der Datentyp :code:`int` benutzt. Typischerweise wird dieser mit 4 Bytes (32 Bits) gespeichert und hat dann einen maximalen positiven Wert von 2 |nnbsp| 147 |nnbsp| 483 |nnbsp| 647.

Folgerichtig gibt es de facto keine relevanten Beschränkungen.

Beschränkungen der zugrundeliegenden Datenbank
----------------------------------------------

Muckturnier benutzt eine `SQLite <http://sqlite.org/>`_\ -Datenbank. Die Grenzen von SQLite (bezüglich der maximalen Zeilen pro Tabelle etc.) wird man noch viel unwahrscheinlicher erreichen, als die durch die Datentypen gesetzten.

Abgesehen davon ist mir bis dato nur eine einzige womöglich praxisrelevante Einschränkung bekannt: Eine SQLite-Datenbank mit Schreibzugriff auf einer Windows-Netzwerkfreigabe bzw. einem Samba-/CIFS-Share funktioniert nicht richtig. Das liegt weder am Muckturnier-Programm, noch an SQLite selbst; es liegt an dem Dateisystem-Protokoll, das Windows-Netzwerkfreigaben bzw. Samba-Shares nutzen. |br|
Das Anlegen einer Datenbank bzw. das Öffnen einer Datenbank mit Schreibzugriff auf einem solchen Netzwerklaufwerk ist deswegen nicht möglich.

Benutzung von Muckturnier
=========================

Generelles zur Benutzung
~~~~~~~~~~~~~~~~~~~~~~~~

Oberfläche und Dialoge
----------------------

Ist noch kein Turnier geöffnet, werden nur die Knöpfe zum Drucken von Zetteln sowie dem Anlegen bzw. Öffnen eines Turniers angezeigt (alternativ kann man das auch über „Datei“ → „Zettel drucken“, „Datei“ → „Neues Turnier starten“ bzw. „Turnier öffnen“ machen). Sobald ein Turnier geöffnet ist bzw. ein neues angelegt wurde, wird die „Paare“- bzw. „Spieler“-Seite angezeigt. Wenn die nötige Anzahl an Paaren bzw. Spielern angemeldet wurde, wird die „Ergebnisse“-Seite, und nach der Eingabe des ersten Ergebnisses schließlich die „Rangliste“-Seite angezeigt.

Standardmäßig werden die Seiten als Tabs (also hintereinander) angezeigt. Sie können aber auch (durch Ziehen mit der Maus) neben- oder übereinander angezeigt werden, sofern das Fenster groß genug ist. Weiterhin können alle Seiten aus dem Hauptfenster herausgezogen, und dann als eigene Fenster angezeigt werden. Das ist z. |nnbsp| B. nützlich, wenn man die Rangliste live über einen Beamer (etwa auf einem virtuellen zweiten Desktop) anzeigen will o. |nnbsp| Ä.

Speichern
---------

Mit jeder Dateneingabe oder -änderung wird die Datenbank aktualisiert. Man muss also den jeweils aktuellen Zustand der Datenbank nicht extra speichern.

Jeder Schreibvorgang ist „atomar“, d. |nnbsp| h., dass ein Datensatz entweder vollständig oder gar nicht gespeichert wird. Es sollte also auch bei einem Stromausfall (Netzteilstecker wird versehentlich aus dem Notebook gezogen, Akku leer) nicht zu Inkonsistenzen in der Datenbank kommen.

Trotzdem ist natürlich jeder selbst für seine Datensicherung verantwortlich (vgl. auch `Backup der Datenbank anlegen und wiederherstellen`_)!

Zettel drucken
~~~~~~~~~~~~~~

Mit dem „Zettel drucken“-Dialog kann man sowohl Tischnummernaufkleber, als auch Auslosungszettel und Spielstandblöckchen erstellen. Für die Spielstandzettel wird eine Vorlage für zwei und für drei Bobbl pro Runde mitgeliefert, jeweils in der „horizontalen“ Variante (Paare von oben nach unten, Bobbl von links nach rechts).

Eine genaue Beschreibung der jeweiligen Vorlage lässt sich über den Link „Hinweise und Tips zu dieser Vorlage“ anzeigen. Der Dialog selber sollte eigentlich selbsterklärend sein.

Alle Zettel werden als PDF-Datei in einem temporären Verzeichnis erstellt, das automatisch gelöscht wird, sobald das Programm beendet wird. Nach dem Erstellen wird jede Datei automatisch mit dem Standard-PDF-Viewer geöffnet. Mit dem können sie dann ausgedruckt werden.

Die Außen- und Innenabstände sind so voreingestellt, wie ich sie mit meinem Drucker wählen musste, damit alle Zettel exakt in der Mitte ausgegeben werden. Abhängig vom benutzen Drucker (insbesondere vom bedruckbaren Bereich) müssen diese Werte ggf. etwas angepasst werden. Das muss man einfach ausprobieren, und dann auf einem Probeausdruck messen, ob alles passt. Die Einstellungen für die Abstände können gespeichert werden und werden dann automatisch beim nächsten Mal wiederhergestellt (die Abstandswerte werden nur gespeichert, wenn das explizit angefragt wird).

Alle übrigen Einstellungen werden beim Schließen des Dialogs automatisch gespeichert und beim nächsten Aufrufen wiederhergestellt, sofern das nicht in den Einstellungen unter „Beim Beenden speichern“ deaktiviert ist (in diesem Fall werden immer die Voreinstellungen gesetzt).

Neues Turnier starten
~~~~~~~~~~~~~~~~~~~~~

Durch Klicken auf den großen Splash-Screen-Button links oder „Datei“ → „Neues Turnier starten“ kann eine neue Turnierdatenbank angelegt werden.

Alle Einstellungen können als Vorlage für neue Turniere gspeichert werden, damit sie beim nächsten Mal nicht wieder erst ausgewählt werden müssen. Weiterhin kann man auch die Einstellungen des laufenden Turniers als Vorlage übernehmen, das geht via „Extras“ → „Einstellungen“ auf der Seite „Vorlage Turniereinstellungen“. Die Vorlage für Markierungen muss immer aus einem laufenden Turnier übernommen werden.

Grundeinstellungen
------------------

Per Voreinstellung zeigt der „Neues Turnier starten“-Dialog nur die „Grundeinstellungen“-Seite. Hier werden Einstellungen festgelegt, die während des Turniers nicht mehr verändert werden können. Namentlich sind das: Der Turniertyp (feste Paare oder einzelne Spieler) und die Punkte pro Bobbl sowie die Bobbl pro Runde.

Zusätzlich kann der Auslosungs-Modus gewählt werden; diese Einstellung kann auch später noch angepasst werden. Vgl. hierzu auch `Auslosung für die 1. Runde`_ und `Auslosung weiterer Runden`_.

Alle Optionen
-------------

Wenn die Auswahlbox „Alle Optionen anzeigen“ angehakt ist, wird links eine Liste weiterer Seiten neben der „Grundeinstellungen“-Seite eingeblendet. Hier können Voreinstellungen für Werte und Optionen eingestellt werden, die auch noch während des Turniers verändert werden können.

Wo die Werte verändert werden können, und was die Einstellungen bedeuten, ist jeweils auf der Seite erklärt.

Turnierinformationen
--------------------

Über „Datei“ → „Turnierinformationen“ wird für das laufende (offene) Turnier die gewählte Konfiguration angezeigt.

Anmeldung
~~~~~~~~~

Hier können die Paare bzw. Spieler gespeichert werden. Es wird jeweils ein Name vergeben. Bei einzelnen Spielern sollte dies sinnvollerweise einfach der Name des Spielers sein, bei Paaren die Namen der Paarmitglieder. Übersichtlich ist z. |nnbsp| B. „Nachname Vorname / Nachname Vorname“. Das erleichtert die Suche, wenn später auf einem Spielstandzettel z. |nnbsp| B. nur noch „Vorname / Vorname“ steht. Der Name kann frei gewählt werden.

Ändern kann man einen Paar- oder Spielernamen jederzeit (auch, wenn bereits Spielstände eingegeben wurden), indem man ihn in der Liste doppelt anklickt (alternativ auch per Rechtsklick über das Kontextmenü). Gespeichert wird die Änderung durch das Drücken der Return- bzw. Enter-Taste.

Wenn mit dem Turnier noch nicht begonnen wurde (also noch keine Spielergebnisse gespeichert wurden), können Paare oder Spieler gelöscht werden. Hierzu einfach das Paar oder den Spieler mit der rechten Maustaste anklicken und im Kontextmenü „[Paar|Spieler] löschen“ auswählen. Liegen bereits Spielergebnisse vor, ist nur noch das Ändern des Paar- bzw. Spielernamens möglich.

Paar- bzw. Spielernummern
-------------------------

Optional ist es möglich, einem Paar bzw. Spieler eine (eindeutige) Nummer zuzuordnen. Hierür kann die optionale Spalte „Nummer“ (über das „Einstellungen“-Menü auf der Anmeldeseite) eingeblendet werden.

Eine Nummer kann über das Kontextmenü oder per Klick auf „…“ in der jeweiligen Zeile zugeordnet (bzw. gelöscht) werden. Es wird immer die erste freie Nummer, also die höchste vorhandene plus 1 vorausgewählt. Es kann auch eine höhere Nummer bzw. eine „Lücke“ neu vergeben werden.

Eine Nummer kann bei allen Suchen (auch bei der Paar- bzw. Spielerauswahl) benutzt werden. Wenn nach einer reinen Zahl gesucht wird, die größer als 0 ist, dann bekommt man entweder exakt einen Treffer (nämlich das Paar bzw. den Spieler mit dieser Nummer) oder keinen (sofern es keine Anmeldung mit dieser Nummer gibt).

Paare bzw. Spieler markieren
----------------------------

Per Voreinstellung wird beim Anlegen einer neuen Datenbank (sofern keine eigene Vorlage definiert wurde) ein vernünftiger Satz an Markierungen erstellt, der sich in der Praxis schon vielfach bewährt hat. Diese Markierungen können beliebig geändert und auch gelöscht werden, und es können beliebig viele eigene Markierungen hinzugefügt werden.

Wird der „Standard“-Satz Markierungen benutzt, dann werden beim Berücksichtigen `allein gekommener Spieler <alleingekommene_>`_ bzw. von `Voranmeldungen <voranmeldungen_>`_ gleich die passenden Markierungen ausgewählt.

.. _alleingekommene: `Allein gekommene Spieler berücksichtigen`_

.. _voranmeldungen: `Voranmeldungen berücksichtigen`_

Für jede Markierung kann eine Farbe und ein Name vergeben werden. Bei der Sortierung kann zwischen „neuer Block“ und „fortlaufend“ gewählt werden. Ein neuer Block gruppiert alle entsprechend markierten Namen, mit einer „fortlaufenden“ Markierung markierte Namen werden mit in den vorhergehenden Block einsortiert. Die Reihenfolge der Markierungen bestimmt die Reihenfolge der Anzeige.

Über das Kontextmenü der Paar-/Spielerliste können einzelne Paare/Spieler oder die ganze Liste markiert werden, bzw. es können Markierungen entfernt oder auch entsprechend markierte Einträge gelöscht werden.

Abgesehen vom Markieren allein gekommener Spieler oder von Voranmeldungen kann man auch z. |nnbsp| B. Paare/Spieler, die das Startgeld noch nicht bezahlt haben markieren, solche, bei denen die Auslosung noch fehlt etc.

Voranmeldungen berücksichtigen
------------------------------

Eine Markierung kann für abwesende Anmeldungen benutzt werden. So markierte Anmeldungen werden ignoriert, und entsprechend nicht beim Zählen der Anmeldungen bzw. der Tische und bei der Entscheidung ob oder ob nicht das Turnier gestartet werden kann berücksichtigt.

Gedacht ist das für Voranmeldungen: Die stehen bereits in der Liste, sind ja aber noch nicht da.

Vor dem Turnierstart können alle als abwesend markierte Anmeldungen (die also doch nicht gekommen sind) über ihre Markierung) auf ein Mal gelöscht werden.

Allein gekommene Spieler berücksichtigen
----------------------------------------

Bei Feste-Paare-Turnieren können allein gekommene Spieler gesondert behandelt werden. Es muss eine Markierung als „Allein da“ definiert werden. Diese kann bei der Anmeldung auch automatisch gestezt werden, z. |nnbsp| B. dann, wenn der Name keinen Schrägstrich enthält.

Allein gekommene Spieler können per Kontextmenü zu einem Paar zusammengeführt werden. Es wird immer angezeigt, wie viele Paare und/oder Spieler noch fehlen, damit das Turnier gestartet werden kann.

Voranmeldung mit Anmeldungscodes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ein fortgeschrittenes Feature zum Thema `Voranmeldungen berücksichtigen`_ ist die Voranmeldungs-Seite. Wenn die Markierung für Voranmeldungen aktiviert und ausgewählt ist, dann kann man die über „Zusatzfunktionen“ → „Voranmeldung“ einblendbare Voranmeldungs-Seite nutzen.

Einstellungen
-------------

Es müssen zwei Einstellungen vorgenommen werden, bevor man die Seite nutzen kann: Ein Turniername und ein Sicherheitsschlüssel. Beides sind frei wählbare Zeichenketten.

Für den Turniernamen sollte man eine eindeutige Bezeichnung wählen, um Verwechslungen auszuschließen (z. |nnbsp| B. „JU Leupoldsgrün 2024“). Der Sicherheitsschlüssel wird automatisch zufällig generiert, und sollte einfach so belassen werden.

Sobald die Einstellungen gespeichert sind, wird ein Backup in eine Textdatei angeboten (UTF-8-kodiert). Ein solches Backup kann auch später noch automatisch oder auch manuell erzeugt werden. Der Grund ist folgender: Wenn man Anmeldungscodes generiert und verschickt hat, und löscht bzw. verändert die Einstellungen oder löscht die Datenbank, dann können die Einstellungen (auch in einer neu angelegten Turnierdatenbank) wiederhergestellt werden. Wenn nicht sowohl der Turniername als auch der Sicherheitsschlüssel exakt identisch sind, schlägt die Prüfung aller damit erzeugten Codes fehl. Den Turniernamen kann man aus einem Code extrahieren, nicht aber den Sicherheitsschlüssel.

Es bietet sich also an, sicherheitshalber ein Backup zu machen, bevor man Codes erzeugt.

Anmeldungscodes generieren
--------------------------

Sobald die Einstellungen festgelegt sind, kann man Anmeldungscodes generieren. Dazu benötigt man vorangemeldete Anmeldungen. Diese können entweder direkt auf der Voranmeldungsseite eingetragen werden, oder auf der Anmeldungsseite registiert und manuell als „Vorangemeldet“ markiert werden (resp. über die Einstellung für die automatische Markierung für neue Anmeldungen). Beim Anmelden über die Voranmeldungsseite wird die Anmeldung an die Anmeldungsseite weitergegeben und die „Vorangemeldet“-Markierung gesetzt. Die Suche nach Duplikaten etc. ist also dieselbe wie bei der Eingabe auf der Anmeldungsseite.

In der Combobox „Voranmeldung“ sind alle Voranmeldungen aufgelistet, und es kann ausgewählt werden, welcher Datensatz angezeigt werden soll. Der QR-Code des Datensatzes enthält die relevanten Anmeldedaten, und kann z. |nnbsp| B. in einer Bestätigungs-E-Mail verschickt werden.

Die Daten enthalten u. |nnbsp| a. eine Prüfsumme, zu deren Berechnung auch der Sicherheitsschlüssel benutzt wird. Da dieser geheim ist, ist praktisch ausgeschlossen, dass jemand seinen „eigenen“ gültigen Anmeldungscode generiert und behauptet, er wäre vorangemeldet. Die technischen Details über die in den QR-Codes enthaltenen Daten und das benutzte Serialisierungsprotokoll können Interessierte in ``doc/BookingCodeProtocol.rst`` nachlesen.

Voranmeldungen anmelden
-----------------------

Hierfür benötigt man entweder einen Hardware-QR-Code-Scanner (die sind tatsächlich relativ günstig), einen Smartphone-Barcode-Scanner, der sich mit dem `WLAN-Code-Scanner-Server`_ unterhalten kann oder eine Softwarelösung, mit der man QR-Codes scannen kann, die dann automatisch eingetippt werden (z. |nnbsp| B. mit einer Webcam, `ZBarCam <https://zbar.sourceforge.net/>`_ und `xvkbd <http://t-sato.in.coocan.jp/xvkbd/>`_ o. |nnbsp| Ä.).

Wichtig: Bei einem Hardware-Scanner auf das richtige Tastaturlayout des Scanners achten! Voreinstellung ist normalerweise US-Amerikanisch. Da wären dann z. |nnbsp| B. ``z`` mit ``y`` vertauscht, ``/`` mit ``-`` etc. wenn man das normale deutsche Layout auf seinem Computer benutzt – und es kommt nicht das raus, was in dem Code gespeichert wurde (was dann auch zum Fehlschlagen der Checksummenprüfung führt).

Damit der Scan automatisch direkt verarbeitet wird, muss der Scanner nach der Eingabe des Datensatzes automatisch auf Return „drücken“. Das ist normalerweise die Voreinstellung. I. |nnbsp| d. |nnbsp| R. kann man das auch einstellen: Die Einstellung heißt „Terminator Symbol“ o. |nnbsp| Ä. und muss dann auf ``CR+LF`` stehen.

Sind Einstellungen für die Voranmeldung festgelegt, dann wird auf der Anmeldungsseite ein Label angezeigt, das darüber informiert, ob gerade gescannt werden kann, oder nicht. Gescannt werden kann dann, wenn das Eingabefeld für den Paar- bzw. Spielernamen fokussiert ist, also den Cursor enthält, und sonst noch nichts eingegeben wurde.

Eine Eingbe, die potenziell ein Anmeldungscode ist, wird nicht als neue Registierung, sondern eben als Anmeldungscode-Eingabe gewertet. Identifiziert wird so ein Code über den Header des Protokolls. Alles, was mit ``MTBC`` („MuckTurnier Booking Code“, in Großbuchstaben) anfängt, wird als Anmeldungscode betrachtet (es ist also im Voranmeldungsmodus nicht möglich, ein Paar bzw. einen Spieler anzumelden, dessen Name mit „MTBC“ in Großbuchstaben beginnt, allerdings ist das ja auch eher selten am Anfang eines Namens zu finden ;-).

Ist alles okay und gibt es die im Code gespeicherte Voranmeldung, dann wird angeboten, sie als „gekommen“ zu markieren. Hierfür wird die Markierung für neue Anmeldungen benutzt, oder – wenn das Berücksichtigen allein gekommener Spieler aktiviert ist und die Bedingung zutrifft – die Markierung für allein gekommene Spieler.

Wenn es keine passende Voranmeldung gibt, dann wird vorgeschlagen, das Paar bzw. den Spieler neu anzumelden.

Sobald ein Anmeldungscode exportiert wird, wird die Prüfsumme beim jeweiligen Paar bzw. Spieler in der Datenbank gespeichert. Über die Prüfsumme kann eine Voranmeldung auch dann identifiziert werden, wenn nach dem Export des Codes der Name geändert wurde. Es ist also nicht nötig nach einer Änderung einer Voranmeldung den Code neu zu verschicken, sofern sie nicht gelöscht und neu angelegt wurde.

Mit Anmeldungscodes geht die Anmeldung sehr schnell. Ein Spieler des Paars bzw. der Spieler zeigt den Code vor (z. |nnbsp| B. einfach auf seinem Handy) vor, scannen – „Ja“ – fertig.

WLAN-Code-Scanner-Server
------------------------

Die WLAN-Code-Scanner-Server-Seite kann über „Netzwerk“ → „WLAN-Code-Scanner nutzen“ eingeblendet werden.

Mit dem WLAN-Code-Scanner-Server ist es möglich, eine passende Smartphone-App anstatt eines Hardware-Barcode-Scanners zum Scannen von Anmeldungscodes zu benutzen. Hierfür muss die App die gescannten Daten per HTTP-Request an einen Server weitergeben können (und natürlich müssen der Rechner, auf dem das Programm läuft, und das Smartphone im selbem Netzwerk sein und sich erreichen können).

Eine sehr zu empfehlende kompatible App ist `Binary Eye <https://github.com/markusfisch/BinaryEye>`_, ein kostenlos erhältlicher Open-Source-Barcode-Scanner für Android (gibt es auf `F-Droid <https://f-droid.org/de/packages/de.markusfisch.android.binaryeye/>`_ und auch im `Google Play Store <https://play.google.com/store/apps/details?id=de.markusfisch.android.binaryeye&hl=de>`_).

Der Server kann ``GET``- und ``POST``-Anfragen verarbeiten. Bei Verwendung von ``POST`` können die Daten entweder mit dem Content-Type ``application/x-www-form-urlencoded`` oder als ``application/json`` formatiert werden. Der Parameter für den Datensatz heißt per Voreinstellung ``content``, der Name kann aber bei den Netzwerkeinstellungen („Extras“ → „Einstellungen“ → „Netzwerk“) beliebig definiert werden.

Die in der App anzugebende URL hängt davon ab, wie die Daten weitergegeben werden. Beispiel: Der Server läuft unter der IP-Adresse 192.168.0.1 und benutzt Port 8812:

Wenn die App die Daten per ``GET`` weitergibt, indem sie sie direkt an die Ziel-URL anhängt (bei Binary Eye wäre das Requesttyp „GET mit an die URL angehängtem Inhalt“), dann müsste man den Parameternamen explizit angeben. Die einzustellende URL wäre in diesem Fall dann ``http://192.168.0.1:8812/?content=``.

Werden via ``GET`` mehrere Parameter übergeben (bei Binary Eye „GET mit vollständigem Query-String“) oder ``POST`` benutzt, dann reicht normalerweise die Angabe der URL ohne weitere Parameter. Im genannten Beispiel wäre das dann ``http://192.168.0.1:8812/``.

Der WLAN-Code-Scanner-Server nutzt standardmäßig Port 8812 `TCP <https://de.wikipedia.org/wiki/Transmission_Control_Protocol>`_. Laut `IANA  <https://www.iana.org/>`_ ist dieser Port `frei <https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml?search=8812>`_ (Stand: 02/2024) und es wurde auch noch keine „Illegal Activitiy“ darauf verzeichnet. Die Chancen stehen also ganz gut, dass man ihn „einfach so“ nutzen kann. Das benutzte Betriebssystem muss natürlich zulassen, dass ein TCP-Server gestartet wird, und den Traffic des Servers nicht blockieren.

Falls ein anderer Prozess auf dem Rechner den Port 8812 TCP bereits benutzen sollte (oder einem der Port einfach nicht gefällt ;-), kann man die Voreinstellung über den „Einstellungen“-Dialog (Seite „Netzwerk“) beliebig zwischen 1024 und 65535 festlegen.

Empfangene Codes werden an die Anmeldungsseite weitergegeben und genau so behandelt, als würde ein Hardware-Barcode-Scanner zum Einsatz kommen (es muss das „Name(n)“-Eingabefeld fokussiert sein, damit der Code verarbeitet wird).

Auslosung für die 1. Runde
~~~~~~~~~~~~~~~~~~~~~~~~~~

Die Eingabe einer Auslosung ist optional, bringt aber vor allem für große Turniere organisatorische Vorteile.

Im Normalfall wird erst fertig angemeldet, und dann wird (per Zettel) ausgelost. Man muss entsprechend vorher wissen, wie viele Tische besetzt sind, damit keine Lücken in der Platzvergabe entstehen.

In diesem Fall ist die Auslosung zunächst nicht bekannt und ergibt sich erst aus der Eingabe der 1. Runde. Bei sehr vielen Teilnehmern dauert der zusätzliche Auslosungsschritt sehr lang, und die Tatsache, dass man nicht weiß, wer wo sitzt, birgt organisatorische Unwägbarkeiten. Es kann natürlich eine „Zettel-Auslosung“ auch manuell übertragen werden, aber das dauert dann noch länger. Deswegen bietet das Programm ein Konzept zum Auslosen für die 1. Runde direkt bei der Anmeldung.

Sinnvollerweise blendet man auf der „Anmeldung“-Seite (sofern noch nicht geschehen) zunächst die entsprechende Spalte über den „Einstellungen“-Knopf und „Angezeigte Spalten“ ein. Eine Auslosung kann man durch einen Klick auf diese Spalte (oder optional über das Kontextmenü) eingeben. Es wird eine Auslosung voreingestellt, die Werte können aber frei verändert werden.

Eine Übersicht über alle bisher zugelosten Plätze und die Belegung der Tische kann über „Zusatzfunktionen“ → „Übersicht Auslosung“ eingeblendet werden.

Die Parameter für die Auslosung der 1. Runde sowie die für die Auslosung weiterer Runden können entweder direkt beim Erstellen einer Turnierdatenbank voreingestellt (wenn alle Optionen eingeblendet sind unter dem Punkt „Auslosung“) bzw. über die Anmeldungsseite („Einstellungen“ → „Paramter Auslosung“) angepasst werden.

Es stehen verschiedene Optionen zur Auswahl:

Fortlaufende Platzvergabe
-------------------------

Ist die Option „Fortlaufend“ gewählt, dann findet streng genommen keine Auslosung statt. Es wird immer der erste freie Platz voreingestellt. Es entstehen keine Lücken, und die Auslosung ist zwangläufig fortlaufend und – die passende Anzahl an Anmeldungen natürlich vorausgesetzt – auch vollständig. Allerdings fehlt natürlich hierbei der Zufall.

Zufällige Platzvergabe
----------------------

Für die zufällige Vergabe der Plätze gibt es drei Optionen:

Komplett zufällige Vergabe
``````````````````````````

Die komplett zufällige Vergabe ist dann sinnvoll, wenn die Anzahl der Tische bekannt ist, oder es zumindest sicher ist, dass eine gewisse Anzahl an Tischen auf jeden Fall vergeben wird. Der einstellbare Tisch ist der letzte Tisch, bis zu dem alle Plätze komplett zufällig ausgelost werden.

Es werden immer zunächst alle Plätze vergeben, die durch diese Angabe definiert sind, sofern sie aktiviert ist.

Weiterhin kann eine komplette, lückenlose und fortlaufende Auslosung für alle Anmeldungen über das Kontextmenü erzeugt werden, sofern die passende Anzahl an Anmeldungen vorliegt.

„Fenster-Vergabe“ in einem Bereich von Tischen
``````````````````````````````````````````````

Diese Variante bietet sich an, wenn nicht klar ist, wie viele Tische besetzt sein werden, aber die Auslosung gleich bei der Anmeldung durchgeführt werden soll. Vor allem für sehr große Turniere ist das gut geeignet, da eine klassische „Zettel-Auslosung“ hier sehr lang dauert. Die „Fenster-Vergabe“ stellt einen gangbaren Kompromiss zwischen Zufälligkeit und Handhabbarkeit dar.

Das Vergabefenster definiert sich durch den ersten Tisch mit einem freien Platz und die eingestellten darauf folgenden Tische.

Zur Veranschaulichung ein Beispiel: Die komplett zufällige Vergabe ist deaktiviert, es liegen noch keine Auslosungen vor und der Bereich wird auf „+4 Tische“ eingestellt. Es werden so lange Plätze an den Tischen 1, 2, 3, 4 und 5 ausgelost, bis Tisch 1 voll besetzt ist. Dann rückt das Vergabefenster weiter, und es werden Plätze an den Tischen 2, 3, 4, 5 und 6 verlost. Wenn Tisch 2 voll besetzt ist dann an den Tischen 3, 4, 5, 6 und 7 etc.

Viel Zufälligkeit und nur wenige nötige Änderungen an der Auslosung bietet die Voreinstellung „+4“ (also die Vergabe in einem Fenster von 5 Tischen). Im Schnitt ist damit oft nur das Umsetzen von zwei Paaren bzw. Spielern vor dem Beginn des Turniers nötig. Dies kann ebenfalls computergestützt geschehen und ist unter `Auslosung bereinigen`_ beschrieben.

Eine statistische Betrachtung dieser Funktion ist in unter `Features: Auslosung bei der Anmeldung <https://muckturnier.org/features/auslosung-bei-der-anmeldung/#statistik>`_ auf muckturnier.org zu finden.

Ist die komplett zufällige Vergabe deaktiviert oder sind bereits alle komplett zufällig auszulosenden Plätze vergeben, werden die Plätze nach der Fenster-Vergabe ausgelost, sofern sie aktiviert ist.

Limit für die zufällige Vergabe
```````````````````````````````

Unabhängig von den anderen Einstellungen kann ein Limit der zufälligen Vergabe (komplett zufällig oder in einem Vergabefenster) eingestellt werden. Wenn alle Plätze, die für die zufällige Vergabe definiert wurden, vergeben sind, werden nur noch fortlaufende Plätze voreingestellt.

Die Einstellung „1“ für diese Option bewirkt dasselbe wie die „Fortlaufende Vergabe“.

Maximale Tischanzahl
--------------------

Ungeachtet der anderen Optionen kann eine maximale Tischanzahl eingestellt werden. Die einstellbaren Werte für die komplett zufällig Auslosung und das Limit für die zufällige Vergabe richten sich danach. Wenn die Fenster-Vergabe aktiviert ist, dann werden keine Tische oberhalb des Tisch-Limits ausgelost.

Ist die Option aktiviert und alle verfügbaren Plätze wurden vergeben, dann erfolgt nur noch eine sequenzielle (fortlaufende) Platzvegabe und es wird ein Warnhinweis angezeigt.

Außerdem wird, sofern eine maximale Tischanzahl gesetzt ist, bereits auf der Anmeldeseite ein Warnhinweis angezeigt, wenn mehr Anmeldungen als Plätze vorliegen.

Auslosung bereinigen
--------------------

Wenn durch die zufällige Vergabe von Plätzen Lücken und/oder unvollständige Paare entstanden sind, muss die Auslosung vor dem Beginn des Turniers bereinigt, also vollständig und fortlaufend gemacht, werden.

Das kann automatisch über „Zusatzfunktionen“ → „Auslosung bereinigen“ bzw. die Links auf der Anmelde- oder Auslosungs-Übersichts-Seite erfolgen. Die Funktion steht zur Verfügung, wenn die Anzahl der Anmeldungen für den gewählten Turniermodus korrekt ist (durch zwei teilbar für feste Paare, durch vier teilbar für einzelne Spieler), es keine abwesenden oder allein gekommenen Anmeldungen gibt und alle Anmeldungen eine Auslosung haben (also potenziell das Turnier begonnen werden kann).

Dabei wird die Variante berechnet, bei der möglichst wenige Paare/Spieler umgesetzt werden müssen. Immer vorausgesetzt, dass es nötig ist, wird dabei folgendes in folgender Reihenfolge gemacht:

Für Einzelspielerturniere zunächst:

- Unvollständige Paare mit Spielern vervollständigen, die die passende Paarnummer gezogen haben

- Unvollständige Paare mit Spielern vervollständigen, die eine andere Paarnummer gezogen haben

Für Feste-Paare- und Einzelspielerturniere dann:

- Unvollständige Tische mit Paaren der passenden Paarnummer vervollständigen

- Unvollständige Tische mit Paaren, die eine andere Paarnummer hatten, vervollständigen

- Lücken durch unbesetzte Tische schließen

Es werden also möglichst viele Paare/Spieler in ihrer bereits zugelosten Rolle „Paar 1“ bzw. „Paar 2“ belassen (damit sich möglichst wenige beschweren). Das Umsetzen/Auffüllen geht immer vom Ende der Auslosung aus, so dass bei einer quasi-sequenziellen „Verlosungsfenster“-Vergabe nur wenige Paare/Spieler umgesetzt werden müssen, und das die sind, die sich ganz am Ende angemeldet hatten.

Optional kann die Liste der nötigen Änderungen auch ausgedruckt werden. Hierzu wird sie (der Einfachheit halber zunächst) in eine HTML-Datei exportiert und mit dem Standard-Webbrowser geöffnet. Mit diesem kann sie dann gedruckt werden.

Auslosung weiterer Runden
~~~~~~~~~~~~~~~~~~~~~~~~~

Sofern pro Runde ausgelost wird (z. |nnbsp| B. bei Einzelspielerturnieren), kann nicht nur die Auslosung für die 1. Runde eingegeben werden, sondern auch die für folgende Runden.

Ab der 2. Runde ist die Anzahl der besetzten Tische bekannt, so dass dann eine komplett zufällige Auslosung über alle Tisch durchgeführt werden kann. Logischerweise ist die Eingabe einer Auslosung für Runde 2 oder später erst dann möglich, wenn die Anmeldung beendet wurde – entweder durch das Eingeben mindestens eines Spielstands, oder durch das explizite Beenden der Anmeldung im Netzwerkbetrieb.

Sinnvolles Vorgehen ist, dass man die Paare bzw. Spieler in dem Moment für die nächste Runde auslost, wo sie den Spielstand für die aktuelle Runde bringen. Hierfür kann die Anmeldungsliste nach den Paaren bzw. Spielern des entsprechenden Tischs gefiltert werden, so dass nur die passenden Paare oder Spieler angezeigt werden und dann direkt ausgelost werden können.

Hierfür kann man entweder über das Kontextmenü des entsprechenden Ergebnisses „Für die nächste Runde auslosen“ auswählen, oder generell automatisch nach der Ergebniseingabe die Anmeldungsliste nach den passenden Paaren bzw. Spielern filtern. Dies ist über die Option „Tisch automatisch für die Auslosung auswählen“ im „Einstellungen“-Menü möglich. Beide Optionen sind dann verfügbar, wenn die Einstellung für die Auslosung bzw. die automatische Paar- oder Tischauswahl eine Auslosung für Runde 2 oder später sinnvoll zulässt (also „Jede Runde wird neu ausgelost“ bzw. „Auswahl laut Auslosung für die Runde“).

Technisch betrachtet wird das Filtern über den speziellen Suchbegriff ``@EXACT_OR``, gefolgt von einem JSON-Array mit allen Namen, nach denen gesucht werden soll, realisiert. Eine Zeile wird dann angezeigt, wenn sie mit einem der angegebenen Namen exakt übereinstimmt. Wird in dieser Ansicht mehr als ein Name geändert, dann muss ggf. der Filter neu aufgerufen oder eine manuelle Suche eingegeben werden (die geänderte Zeile wird auch dann noch angezeigt, wenn sie durch eine Suche eigentlich ausgeblendet wäre, aber bei noch einer Änderung fehlt dann eine Zeile).

„Kollisionen“ vermeiden
-----------------------

In den Einstellungen können zwei Optionen zur Vermeidung von „Kollisionen“ aktiviert werden: „Mehrfache Zulosung derselben Partner vermeiden, wenn möglich“ und „Mehrfache Zulosung derselben Gegner vermeiden, wenn möglich“. Diese sind ja eigentlich selbsterklärend: Wenn sie aktiviert sind, dann werden bei der Auslosung nur die Plätze berücksichtigt, die eben so noch nicht ausgelost waren. Wenn es keine Möglichkeit mehr gibt, einen „kollisionsfreien“ Platz zuzulosen, dann wird aus allen verbliebenen Plätzen ausgelost.

Es können auch dann, wenn diese Optionen aktiv sind, immer alle freien Plätze zugelost werden. Ggf. wird aber ein Hinweis auf die „Kollision“ angezeigt (s. |nnbsp| u.).

Warnungen vor „Kollisionen“
---------------------------

In den Einstellungen für die Auslosung können zwei Optionen aktiviert werden: „Bei schon einmal zugelosten Partnern einen Hinweis anzeigen“ und „Bei schon einmal zugelosten Gegnern einen Hinweis anzeigen“. Erstere greift logischerweise nur bei Einzelspielerturnieren. Ist eine Hinweis-Option aktiviert, dann wird im Auslosungs-Popup ein entsprechender Hinweis angezeigt, wenn ein Partner und/oder ein Gegner bzw. Paar einem Spieler bzw. Paar bereits vorher zugelost wurde. Ungeachtet dessen ist die Auslosung des entsprechenden Platzes trotzdem möglich.

Zeitplan für das Turnier
~~~~~~~~~~~~~~~~~~~~~~~~

Die Planung für den zeitlichen Ablauf des Turniers ist vollkommen optional, aber man kann dadurch im Auge behalten, wie man „in der Zeit liegt“ – vor allem praktisch, wenn es tatsächlich eine Begrenzung für das Ende des Turniers gibt.

Die Turnier-Zeitplan-Seite kann über „Zusatzfunktionen“ → „Turnier-Zeitplan“ eingeblendet werden. Hier können die geplante Anzahl der Runden, die Minuten pro Runde und ggf. auch eine Pause dazwischen eingegeben werden. Mit der „Zeitplan-Vorschau“ wird ein Zeitplan generiert, und man kann sehen, wann das Turnier – geplanterweise – enden soll.

Sobald man eine Runde startet, wird der Zeitplan (dann letztmalig) neu generiert, und hinterher nicht mehr verändert (es sei denn, man setzt die Daten zurück). Die Box „Aktuelle Zeiten“ informiert ständig über alle relevanten Zeitmessungen und Zeitpunkte.

Optional kann beim Starten einer Runde auch automatisch eine zusätzliche Stoppuhr gestartet werden.

Sobald das letzte Ergebnis einer Runde eingegeben wurde, wird gefragt, ob auch die Zeitmessung für diese Runde beendet werden soll.

Info-Display
------------

Das Info-Display ist ein extra Fenster, das die momentane Uhrzeit, die gerade gespielte Runde bzw. Pause, sowie die verbleibende Zeit und den Start der nächsten Runde anzeigt.

Gedacht ist das Info-Display zur Vollbildanzeige auf einem zweiten Monitor, einem Fernseher, Beamer etc., so dass alle wissen, wie viel Zeit sie noch zum Spielen haben bzw. wann die nächste Runde losgeht. Die Vollbildanzeige kann im Info-Display über den Menüknopf umgeschaltet werden, oder auch über den Knopf auf der Zeitplan-Seite, mit dem man das Info-Display startet.

Die Größe und Position der Texte, die Ränder und alle Farben können mittels des Menü-Knopfs links oben individualisiert werden. Alle Einstellungen können gespeichert werden und werden dann beim nächsten Mal automatisch wieder angewandt. Diese Parameter sind rechnerspezifisch, nicht datenbankspezifisch und werden deswegen nicht in der gerade benutzen Datenbank abgelegt, sondern auf dem gerade benutzen Computer.

Strafpunkte
-----------

Ein womöglich interessantes Feature sind die „Strafpunkte“: Gedacht ist das als „Disziplinierungsmaßnahme“, wenn man mit einer Zeitbegrenzung arbeitet. Es passiert immer wieder, dass ein Paar kommt und sich beschwert, dass die Runde nun schon 10 Minuten läuft, aber das gegnerische Paar noch nicht gekommen ist. In diesem Fall könnte man dem „unschuldigen“ Paar Ausgleichs-Punkte gutschreiben, da ja schon Spielzeit verloren gegangen ist. Die Strafpunkte-Anzeige zeigt zu jeder Zeit (gleichmäßig verteilt auf die komplette Spielzeit pro Runde) an, wie viele es gerade wären.

Die Anzeige ist rein informativ. Automatisch passiert nichts. Wenn man das Feature nicht nutzen will, kann man die Anzeige einfach ignorieren.

Spielergebnisse eintragen
~~~~~~~~~~~~~~~~~~~~~~~~~

Das Layout der Eingabemaske unterstützt zwei Varianten von Spielstandzetteln: Einmal „Horizontal“, wo Paar 1 oben und Paar 2 unten steht und die Bobbl von links nach rechts aufgeschrieben werden und zusätzlich „Vertikal“, wo Paar 1 links und Paar 2 rechts steht und die Bobbl von oben nach unten aufgeschrieben werden.

Die Variante kann im „Neues Turnier starten“-Dialog voreingestellt, auf der „Ergebnisse“-Seite via „Optionen“ aber auch noch im laufenden Betrieb umgestellt werden.

Für die meiner Meinung nach übersichtlichere und gängigere horizontale Variante stelle ich einen Beispiel-Spielstandzettel auf der `Muckturnier-Homepage <https://muckturnier.org/material/>`_ zur Verfügung, u. |nnbsp| a. als PDF zum direkt ausfüllen und drucken.

Rundenauswahl
-------------

Zunächst wird die Runde ausgewählt, für die das Ergebnis eingetragen werden soll. Wenn eine Runde offen ist (also noch nicht alle Ergebnisse der entsprechenden Runde eingetragen wurden), dann wird automatisch diese Runde geöffnet (bzw. die erste offene Runde, falls mehrere Runden offen sind).

Alle bisher abgeschlossenen Runden sowie alle offenen Runden können über die Rundenauswahl angezeigt bzw. bearbeitet werden.

Paar- bzw. Spielerauswahl
-------------------------

Der Eingabedialog sieht die Eingabe von Runde, Tischnummer und zwei Paaren bzw. vier Spielern vor. Sollte ohne Tischnummern gespielt werden, kann die Tischauswahl auch ausgeblendet werden (über das Menü des „Einstellungen“-Knopfes). In diesem Fall wird eine Tischnummer automatisch intern gesetzt (für die Datenbank muss immer eine Tischnummer ausgewählt werden, auch dann, wenn sie nicht angezeigt wird) und die automatische Auswahl steht nicht zur Verfügung.

Die Auswahlboxen enthalten als wählbare Optionen alle noch nicht eingegebenen Paare bzw. Spieler und Tischnummern. Es können auch Teile der Namen bzw. des Namens eingeben werden. Damit werden die auswählbaren Optionen auf Übereinstimmungen eingeschränkt (das ist besonders bei vielen Spielern äußerst nützlich).

Automatische Auswahl
````````````````````

Wenn die Option „Automatische Paar- bzw. Tischauswahl“ → „Paar 1 bleibt sitzen, Paar 2 rutscht weiter“ oder „Auswahl laut Auslosung für die Runde“ im Menü unter dem „Einstellungen“-Knopf aktiviert ist, kann die Auswahl des Tischs und der Paare bzw. Spieler anhand der eingegebenen Auslosung und/oder anhand der Ergebnisse der letzten Runde erfolgen.

Die beiden Modi unterscheiden sich in der Art der Auslosung:

Bei „Paar 1 bleibt sitzen, Paar 2 rutscht weiter“ wird ggf. eine Auslosung für die 1. Runde eingegeben, ggf. `direkt bei der Anmeldung <Auslosung für die 1. Runde_>`_. Sofern vorhanden wird diese Auslosung für die Paar- bzw. Tischauswahl herangezogen. In allen späteren Runden werden die Plätze aus den Ergebnisen der letzten Runde berechnet, wobei Paar 1 seinen Platz behält und Paar 2 einen Tisch weiterrückt. Ggf. für weitere Runden eingegebene Auslosungen bleiben unberücksichtigt.

Bei „Auswahl laut Auslosung für die Runde“ werden die Paare bzw. Tische entsprechend der für die Runde eingegebenen Auslosung ausgewählt, sofern sie vorhanden ist.

Die automatische Auswahl kann jederzeit ausgetzt werden, indem der „Automatische Auswahl (de)aktivieren“-Knopf rechts aktiviert wird. Damit kann man von der Auswahl abweichende Ergebnisse eingeben und es werden keine anderen Auswahlen automatisch gesetzt.

Punkte und Bobbl eingeben
-------------------------

Unter der Paarauswahl gibt es für jeden Bobbl ein Eingabefeld. Das ausgewählte Paar bekommt den Bobbl (hat ihn also verloren; hier den Radio-Knopf anklicken) und es können die erreichten Punkte („geschossene Tore“) eingegeben werden. Das andere Paar bekommt automatisch die festgelegten Bobbl-Punkte, hier wird das Eingabefeld deaktiviert (außer, wenn der Bobbl `abgebrochen <aborted_boogers_>`_ wurde).

.. _aborted_boogers: `Abgebrochene Bobbl`_

Ein Rechtsklick auf einen der beiden Radio-Knöpfe bzw. Eingabefelder öffnet einen kleinen Eingabedialog, mittels welchem der Spielstand direkt durch einen Klick ausgewählt werden kann, ohne das Mausrad oder die Tastatur verwenden zu müssen.

Der Spielstand kann entweder mit dem „Ergebnis eintragen“-Knopf gespeichert werden, oder indem man in einem der beiden Eingabefelder im letzten Spiel die Return- bzw. Enter-Taste drückt.

Abgebrochene Bobbl
``````````````````

Wenn eine Zeitbegrenzung pro Runde vereinbart ist, kommt es vor, dass es unvollständige (abgebrochene) Spielstände gibt. Um ein solches Ergebnis einzugeben, wird einfach der letzte gespielte Bobbl als abgebrochen markiert (mit dem kleinen Knopf rechts oben). Daraufhin kann man zwei Spielstände eingeben, die jedem Paar als geschossene Tore gutgeschrieben werden. Beide Paare haben den Bobbl verloren. Alle folgenden Bobbl (sofern vorhanden) werden für beide Paare als „zu 0 verloren“ gewertet.

Wer mit Zeitbegrenzung spielt, will evtl. die über „Extras“ anzeigbare `Stoppuhr`_ verwenden. Diese ist ein Countdown-Timer. Wenn die Zeit abgelaufen ist, wird sie wieder hochgezählt, und die Zeitanzeige wechselt die Farbe von schwarz nach rot.

Eingetragene Ergebnisse
-----------------------

Unten werden alle bisher eingetragenen Spielergebnisse angezeigt. Per Kontextmenü (mit der rechten Maustaste oder mit einem Doppelklick einen Spielstand anklicken) kann man einen Spielstand löschen oder korrigieren.

Beim Korrigieren wird der Spielstand auch gelöscht, aber die eingegebenen Daten werden als ausgewählte Werte gesetzt. So kann man z. |nnbsp| B. einen falsch eingegebenen Punktestand schnell korrigieren, ohne die anderen Werte erneut eingeben zu müssen und den Tisch bzw. die Paare neu auszuwählen.

Paare bzw. Spieler disqualifizieren
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Auf der Anmeldeseite können Paare bzw. Spieler mittels des Kontextmenüs („Disqualifizieren“) vom Turnier ausgeschlossen werden. Dies ist möglich, sobald Ergebnisse eingegeben wurden (vorher kann man das entsprechende Paar ja auch einfach löschen).

Es kann eine Runde ausgewählt werden, ab der die Disqualifikation gelten soll. Ab dieser Runde taucht das ausgeschlossene Paar bzw. der ausgeschlossene Spieler nicht mehr in der Rangliste auf. Eine Eingabe von Ergebnissen ist aber trotzdem noch möglich, weil:

Vom Ablauf her ist vermutlich die einzige vernünftige Möglichkeit, das Turnier nach dem Ausschluss eines Paares oder Spielers normal weiterzuführen, folgende: Das disqualifizierte Paar bzw. der disqualifizierte Spieler wird durch ein Dummy-Paar bzw. einen Dummy-Spieler von der Turnierleitung ersetzt. Ansonsten würde ja die Anzahl der Mitspieler nicht mehr passen. So müssen sich die noch anstehenden Gegner normal ihre Bobbl und Tore erkarten.

Rangliste
~~~~~~~~~

Sobald das erste Ergebnis eingetragen wurde, ist die momentane Rangliste verfügbar. Wenn es sich um ein Zwischenergebnis handelt, also von einer oder mehreren Runden noch Spielergebnisse fehlen, wird dies entsprechend vermerkt.

Es kann mit der entsprechenden Checkbox eingestellt werden, ob die Rangliste – zusätzlich zu den gewonnenen Bobbln und den geschossenen Toren – auch nach den geschossenen Toren der Gegner in den gewonnenen Bobbln erstellt wird (vgl. auch `Regeln für das Turnier`_).

Standardmäßig wird die Rangliste für den aktuellen Turnierstand angezeigt. Es können aber auch Verlaufs-Ranglisten für frühere Runden erstellt werden, hierzu einfach die entsprechende Runde auswählen.

Import und Export von Turnierdaten
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Import einer Paar- bzw. Spielerliste
------------------------------------

Wenn das Turnier noch nicht begonnen hat (also noch kein Spielstand eingegeben wurde), kann eine Paar- bzw. Spielerliste importiert werden. Der zugehörige Menüpunkt ist „Datei“ → „Import und Export“ → „[Paar|Spieler]liste importieren“. Hierfür kann eine einfache, `UTF-8 <https://de.wikipedia.org/wiki/UTF-8>`_-kodierte Textdatei (wie sie auch von der `Exportfunktion <export_>`_ erzeugt wird) verwendet werden, die pro Zeile einen Paar- bzw. Spielernamen enthält. Leerzeichen am Anfang oder Ende, mehrere aufeinanderfolgende Leerzeichen sowie leere Zeilen werden automatisch entfernt.

.. _export: `Export der Paar- bzw. Spielerliste`_

Wird eine Datei geöffnet, die Zeichen außerhalb der ASCII-Kodierung enthält (Umlaute, Akzente, etc.), und mit der falschen Kodierung gespeichert wurde (z. |nnbsp| B. Windows-1252, ISO 8859-1, ISO 8859-15 etc.), dann wird der Import abgebrochen. |br|
Jeder vernünftige Editor (unter Linux was auch immer man nehmen will, unter Windows z. |nnbsp| B. `notepad++ <https://notepad-plus-plus.org/>`_) kann eine solche Datei mit der „falschen“ Kodierung öffnen, und mit der „richtigen“ speichern, so dass sie trotzdem importiert werden kann.

Es wird eine Vorschau des zu importierenden Datensatzes angezeigt. Gibt es (wenn bereits Paare/Spieler angemeldet sind oder auch innerhalb der Importdatei) Duplikate und/oder Namen, die zu Verwechslungen führen könnten, werden diese extra angezeigt und können vom Export ausgeschlossen bzw. mit einem Präfix ergänzt werden (um sie nach dem Import leicht finden und evtl. umbenennen zu können).

In keinem Fall werden bestehende Anmeldungen überschrieben oder gelöscht.

Export der Paar- bzw. Spielerliste
----------------------------------

Sobald mindestens ein Paar bzw. Spieler angemeldet wurde, kann eine Liste der Paare bzw. Spieler exportiert werden (um z. |nnbsp| B. eine Liste zum Abhaken aus einer bereits mit Voranmeldungen gefüllten Datenbank erstellen zu können).

Der zugehörige Menüpunkt ist „Datei“ → „Import und Export“ → „[Paar|Spieler]liste exportieren“. Es kann auch eine per `Markierungen <markierungen_>`_ ausgezeichnete Untermenge der Anmeldungsliste exportiert oder vom Export ausgeschlossen werden.

.. _markierungen: `Paare bzw. Spieler markieren`_

Die Liste wird als einfache Textdatei exportiert und ist `UTF-8 <https://de.wikipedia.org/wiki/UTF-8>`_-kodiert. Man kann dieses Format mit einem beliebigen Texteditor oder Textverarbeitungsprogramm öffnen und weiterverarbeiten.

Export von Turnierdaten nach Abschluss des Turniers
---------------------------------------------------

Sind alle Runden abgeschlossen (das Turnier also potenziell beendet), können via „Datei“ → „Import und Export“ → „Turnierdaten exportieren“ (alle) Daten des Turniers in ein von Muckturnier und der Datenbank unabhängiges Format exportiert werden.

Man kann auswählen, welche Datensätze exportiert werden, und wie diese formatiert sein sollen (sollen z. |nnbsp| B. Überschriften angezeigt werden, die Ergebnisse als einfache Zahl oder mit dem Bobbl-Symbol ausgegeben werden etc.).

Die Daten können als `HTML <https://de.wikipedia.org/wiki/Hypertext_Markup_Language>`_ oder `CSV <https://de.wikipedia.org/wiki/CSV_(Dateiformat)>`_ exportiert werden. Als Zeichensatz kommt jeweils `UTF-8 <https://de.wikipedia.org/wiki/UTF-8>`_ zum Einsatz.

Export als HTML
```````````````

Es wird eine als HTML5 formatierte, `UTF-8 <https://de.wikipedia.org/wiki/UTF-8>`_-kodierte Datei ausgegeben. Das Ergebnis kann programm- und plattformunabhängig mit einem Webbrowser angezeigt werden.

Wenn alle verfügbaren Daten exportiert werden, dann eignet sich dieses Format vor allem für das Archivieren von Turnierergebnissen. So können alle relevanten Daten eines Turniers in eine formatierte, auf absehbare Zeit für Menschen lesbare Form gebracht, und auch später noch eingesehen bzw. ausgedruckt werden.

Export als CSV/TSV
``````````````````

Es wird eine `UTF-8 <https://de.wikipedia.org/wiki/UTF-8>`_-kodierte Datei ausgegeben, die als Trennzeichen Tabulatoren enthält (CSV bzw. TSV, das „T“ impliziert, dass Tabulatoren als Trenner benutzt werden). Diese kann z. |nnbsp| B. mit einem Tabellenkalkulationsprogramm weiterverarbeitet werden.

Beim Öffnen darauf achten, dass als Zeichensatz „UTF-8“ und als Trennzeichen Tabulatoren eingestellt sind – formatbedingt enthält eine CSV/TSV-Datei keinerlei Meta-Informationen (welcher Zeichensatz oder welches Trennzeichen benutzt wurde etc.), deswegen muss man diese Informationen ggf. manuell angeben!

Netzwerkfunktionen
~~~~~~~~~~~~~~~~~~

Bei großen Turnieren hat es sich bewährt, sowohl die Anmeldung als auch die Auswertung an zwei oder mehr Rechnern durchzuführen. Mit mindestens zwei Computern vermeidet man bei der Anmeldung lange Schlangen, bei der Auswertung (die natürlich unabhängig voneinander eingegeben werden muss) kann man Eingabefehler finden, da diese zu unterschiedlichen Ranglisten führen. Das ist mittels der Netzwerkfunktionalität sehr einfach organisierbar.

Die Voraussetzung für die folgenden Funktionen ist, dass sich alle beteiligten Rechner in einem Netzwerk (LAN/WLAN) befinden. Dafür kann man z. |nnbsp| B. eine alte Fritz-Box oder – perfekt geeignet für ein Muckturnier – einen der vielen vergleichsweise günstigen mobilen Mini-Router (GL.iNet GL-AR150, TP-Link TL-WR802N o. |nnbsp| ä.) nehmen.

Eine Internetverbindung wird natürlich nicht benötigt. Die Rechner müssen sich nur im Netzwerk erreichen können (und nur um das an dieser Stelle gesagt zu haben: Nein, das Muckturnier-Programm kommuniziert nicht mit irgendwelchen Servern und es überträgt auch keine Daten irgendwohin, siehe Quellcode).

Technische Aspekte
------------------

Es werden für die Netzwerkkommunikation zwei Server benutzt. Einmal der „Discover“-Server, mit dessen Hilfe Clients einen Muckturnier-Server im Netzwerk automatisch finden können, und der eigentliche Muckturnier-Server, der sich um das Verteilen von Änderungen und auch den Ranglistenvergleich kümmert.

Der Muckturnier-Server nutzt standardmäßig Port 8810 `TCP <https://de.wikipedia.org/wiki/Transmission_Control_Protocol>`_, der Discover-Server Port 8811 `UDP <https://de.wikipedia.org/wiki/User_Datagram_Protocol>`_. Diese beiden Ports sind laut `IANA  <https://www.iana.org/>`_ `nicht vergeben <https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml?search=8810>`_ (Stand: 09/2019) und es wurde auch noch keine „Illegal Activitiy“ darauf verzeichnet. Die Chancen stehen also ganz gut, dass man sie „einfach so“ nutzen kann.

Falls ein anderer Prozess auf dem Rechner die Ports 8810 TCP und/oder 8811 UDP bereits benutzen sollte (oder einem die Ports einfach nicht gefallen ;-), können sie über den „Einstellungen“-Dialog (Seite „Netzwerk“) beliebig zwischen 1024 und 65535 eingestellt werden. Es können auch beide Server denselben Port nutzen, weil der eine TCP und der andere UDP verwendet, und es wegen des unterschiedlichen Protokolls auch dann keine Kollisionen gibt (wenngleich man das eigentlich nur dann macht, wenn der selbe Dienst über zwei Protokolle erreichbar ist. In diesem Fall sind es zwei verschiedene Dienste; technisch möglich ist es aber jedenfalls).

Wenn ein Programm unter Windows z. |nnbsp| B. einen TCP-Server startet, dann wird i. |nnbsp| d. |nnbsp| R. eine Sicherheitswarnung der „Firewall“ angezeigt. Natürlich darf die Netzwerkkommunikation des Muckturnier-Programms nicht geblockt werden. Der oder die verwendeten Ports dürfen natürlich ebenfalls nicht blockiert werden, weder auf der Server-, noch auf der Client-Seite.

In den meisten Fällen wird wohl ein klassisches IPv4-Klasse-C-Netzwerk zum Einsatz kommen (mit Adressen wie 192.168.0.10, 192.168.0.11 etc.), IPv6-Adressen können aber genauso benutzt werden.

Technisch gesehen ist das Muckturnier-Serverprotokoll so aufgebaut, dass die zu übermittelnden Daten erst in ein `JSON <https://de.wikipedia.org/wiki/JavaScript_Object_Notation>`_-Objekt verpackt, dann in einen String konvertiert, und schließlich in einem |QDataStream|_ per TCP/IP übertragen werden. Auf der anderen Seite wird dann wieder aus dem Stream der String geholt, in ein JSON-Objekt geparst und man kann auf die Daten zugreifen. Netterweise kümmern das zugrundeliegende Protokoll TCP und der ``QDataStream`` bereits auf der Übertragungsebene darum, dass alle Pakete vollständig und in der richtigen Reihenfolge übertragen werden.

.. |QDataStream| replace:: ``QDataStream``
.. _QDataStream: https://doc.qt.io/qt-5/qdatastream.html

Es gibt keine Authentifizierung und auch keine Absicherung der Kommunikation; man sollte darauf achten, ein geschlossenes Netzwerk aufzubauen, das nur aus vertrauenswürdigen Teilnehmern besteht. |br|
Einen irgendwie gearteten Angriff mittels manipulierter Anfragen kann ich mir zwar beim besten Willen nicht vorstellen – schlimmstensfalls sollte eine (manipulierte) Anfrage vom Server abgelehnt und/oder die Verbindung zum Client aufgrund nicht mehr synchroner Prüfsummen beendet werden. Aber man weiß ja nie ;-)

Muckturnier-Server
------------------

Sobald auf einem der beteiligten Rechner eine beschreibbare Datenbank geöffnet ist, kann dort per „Netzwerk“ →  „Als Server einrichten“ ein Muckturnier-Server gestartet werden, an dem sich dann beliebig viele andere Rechner als Clients anmelden können (per „Netzwerk“ → „Mit Server verbinden“).

Über das Netzwerk werden `Änderungen an der Anmeldungsliste verteilt <network_registration_>`_, und es ist ein `Ranglistenvergleich <network_ranking_>`_ möglich. Es wird jeweils TCP als Protokoll benutzt. Der Port kann frei gewählt werden (von 1024 bis 65535, voreingestellt ist 8810), darf aber natürlich noch von keinem anderen Programm belegt sein.

.. _network_registration: `Anmeldungsnetzwerk`_
.. _network_ranking: `Vergleich der eingegebenen Ergebnisse`_

Wird auf einem Rechner die Netzwerk-Client-Seite geöffnet, wird automatisch versucht, einen Muckturnier-Server im Netzwerk zu finden. Dieser Vorgang kann (wenn der Server z. |nnbsp| B. noch nicht gestartet war) mittels „Server suchen“ wiederholt werden. Wird der Server nicht gefunden, kann die IP-Adresse und der Port (die sog. „Socket-Adresse“) des Servers auch manuell eingegeben werden.

Damit sich ein Client verbinden kann, muss er ebenfalls eine beschreibbare Datenbank geöffnet haben. Damit eine Verbindung hergestellt werden kann, müssen die Turniereinstellungen identisch sein, ansonsten wird die Verbindung abgelehnt.

Wenn noch keine Ergebnisse eingegeben wurden, dann werden evtl. lokal eingegebene Namen gelöscht, und die komplette Anmeldungsliste des Servers übernonmmen. Gibt es bereits Ergebnisse, dann wird die lokale Anmeldungsliste mit der des Servers abgeglichen und Änderungen können übernommen (alternativ auch manuell korrigiert) werden. In jedem Fall werden lokale Markierungen verworfen und durch die auf dem Server ersetzt.

Discover-Server
---------------

Der Discover-Server wird beim Start des Muckturnier-Servers automatisch mitgestartet, mit der in den Einstellungen festgelegten Portnummer. Die Voreinstellung ist Port 8811, es wird UDP als Protokoll benutzt.

Der Betrieb des Servers ist nicht zwingend nötig. Er bringt aber den Komfort, dass die Socket-Adresse des Servers nicht manuell eingegeben werden muss: Wenn ein Client sich mit einem Server verbinden will, dann wird zunächst automatisch per UDP-Broadcast nach einem Server gesucht. Wenn der Server so eine Anfrage erhält, dann antwortet er mit seiner IP-Adresse und Portnummer, und die kann dann automatisch zum Verbinden benutzt werden.

Sollte das Starten fehlschlagen oder der genutzte Port (bzw. UDP-Kommunikation darüber) blockiert sein, funktioniert der Muckturnier-Server unabhängig davon trotzdem; die Clients müssen lediglich vor dem Verbinden die IP-Adresse und den Port des Servers manuell eingeben.

Anmeldungsnetzwerk
------------------

Alle Änderungen an der Paar- bzw. Spielerliste, die an einem der Rechner im Netzwerk vorgenommen werden (egal ob Server-Rechner oder Client-Rechner), werden an alle Beteiligten verteilt und angewandt. Namentlich das Anmelden neuer Paare/Spieler, das Löschen oder Umbenennen vorhandener und das Markieren bzw. Enfernen von Markierungen. Änderungen an verfügbaren Markierungen werden ebenso verteilt.

Alle Rechner, die sich an einem Muckturnier-Server angemeldet haben, haben immer denselben Datenbestand bezüglich Markierungen und der Anmeldungsliste.

Vergleich der eingegebenen Ergebnisse
-------------------------------------

Im Netzwerkbetrieb muss die Anmeldung explizit beendet werden, bevor Ergebnisse eingegeben werden können. Das kann der Server und auch jeder Client über „Netzwerk“ → „Anmeldung beenden“ (bzw. über den zugehörigen Link auf der Ergebnisse-Seite) machen.

Die eingegebenen Ergebnisse werden nicht synchronisiert. Der Sinn dahinter ist das Finden von Eingabefehlern: Wenn man die Ergebnisse unabhängig voneinander an mindestens zwei Rechnern eingibt, dann treten bei Tippfehlern, Zahlendrehern etc. Unterschiede auf. Würde man die Eingabe auf zwei Rechner aufteilen, hätten gleich zwei Benutzer die „Chance“, unbemerkte Falscheingaben zu machen.

Alle eingegebenen Ergebnisse werden fortlaufend verglichen. Am Server mit allen Clients und an jedem Client mit dem Server. Der Status (identisch – nicht identisch) wird neben den bisherigen Ergbnissen angezeigt. Die Anzeige wird bei jeder client- und serverseitigen Eingabe aktualisiert. Gibt es Unterschiede, dann können diese mittels des dann angezeigten Links in einem extra Dialog angezeigt und (durch den Abgleich mit dem jeweiligen Spielstandzettel) korrigiert werden.

Unterschiede zum Ein-Rechner-Betrieb
------------------------------------

Prinzipiell sollten die Netzwerkfunktion im Rahmen des Möglichen transparent funktionieren, d. |nnbsp| h. dass z. |nnbsp| B. durch eine Änderung eines anderen Netzwerkteilnehmers ein anderer nicht beeinträchtigt wird. Beispielsweise werden – wenn möglich – der Zustand einer Spielstandeingabe oder der Änderung eines Names nach dem Verarbeiten und Anwenden der Netzwerkänderung wiederhergestellt, und zwar so schnell, dass es der jeweilige Benutzer gar nicht merken dürfte.

Ein relevanter Unterschied ist aber, dass die Anmeldung explizit beendet werden muss („Netzwerk“ → „Anmeldung beenden“, vgl. auch `Vergleich der eingegebenen Ergebnisse`_). Das ist deswegen nötig, damit nicht an einem Rechner bereits Ergebnisse eingegeben werden (ab dann muss die Anzahl der Anmeldungen konstant bleiben), und an einem anderen ohne Ergebnisse noch ein Paar/Spieler gelöscht werden kann.

Im Ein-Rechner-Betrieb können Ergebnisse eingegeben werden, sobald eine korrekte Anzahl von Paaren (durch zwei teilbar) oder Spielern (durch vier teilbar) angemeldet ist. Wurde das erste Ergebnis eingegeben, dann können Namen noch verändert werden, aber es können keine Paare/Spieler mehr angemeldet oder gelöscht werden.

Ist die Anmeldung beendet, können auch dann Paare/Spieler nicht mehr angemeldet oder gelöscht werden, wenn der letzte eingegebene Spielstand gelöscht wurde. Sollten nach dem Beenden der Anmeldung noch Änderungen an der Anmeldungsliste abgesehen vom Editieren eines Namens oder der Änderung einer Markierung nötig sein (also Hinzufügen oder Löschen), dann bitte den Server schließen und neu starten und alle Clients neu anmelden. Danach sind – vorausgesetzt es sind noch keine Ergebnisse eingegeben oder alle eingegebenen Ergebnisse sind gelöscht worden – wieder alle Änderungen an der Anmeldungsliste möglich.

Sonstige Funktionen
~~~~~~~~~~~~~~~~~~~

Stoppuhr
--------

Über „Extras“ → „Stoppuhr“ kann ein Countdown-Timer angezeigt werden. Diese Funktion ist sinnvoll, wenn mit einer `Zeitbegrenzung`_ gespielt wird (vgl. auch `Abgebrochene Bobbl`_). Im Netzwerkbetrieb kann eine Client-Stoppuhr mit der des Servers synchronisiert werden, so dass alle dieselbe (Rest-)Zeit sehen.

Die Turnierdatenbank zurücksetzen
---------------------------------

Über „Extras“ → „Datenbank“ → „Datenbank zurücksetzen“ können alle eingegebenen Daten (Spieler und Ergebnisse) gelöscht werden. Das ist an sich nur für Testanwendungen, Debugging und Entwicklung gedacht.

Sollen auch die Einstellungen geändert werden (Punkte pro Bobbl, Bobbl pro Runde oder Turniermodus), dann kann einfach die bestehende Datenbank mit einer neuen überschrieben werden.

Backup der Datenbank anlegen und wiederherstellen
-------------------------------------------------

Per „Extras“ → „Datenbank“ → „Backup der Datenbank anlegen“ kann ein Backup des aktuellen Datenbank-Stands erzeugt werden. Es wird eine Datei mit dem selben Namen wie die geöffnete Datenbank angelegt, der zusätzlich den Zeitpunkt der Erstellung des Backups enthält.

Das Format für den Dateinamen ist „Datenbank_Jahr-Monat-Tag_Stunde-Minute-Sekunde“ (somit werden alle Backups automatisch nach dem Erstellungszeitpunkt sortiert), also beispielsweise ``Muckturnier_2018-07-25_14-48-14.mtdb`` für die Datenbank ``Muckturnier.mtdb``.

Der über „Extras“ → „Datenbank“ → „Backup wiederherstellen“ erreichbare Dialog listet alle verfügbaren Backups auf. Wenn ein Backup wiederhergestellt wird, wird die Datenbank damit überschrieben und neu geöffnet.

Das Ganze geht natürlich auch „zu Fuß“ (wenn auch nicht so bequem): Es werden nur Dateien kopiert bzw. umbenannt. Um ein Backup manuell zu erzeugen, kann man auch die Turnierdatenbank schließen, eine Kopie davon machen und sie dann wieder öffnen. Analog funktioniert das Wiederherstellen eines Backups.

Tips aus der Praxis
===================

Muckturnier wurde und wird schon seit vielen Jahren auf kleinen und großen Turnieren eingesetzt. Dabei haben sich einige Erkenntnisse ergeben, wie man sich das Leben leichter machen kann. Manche davon sind vielleicht nicht ganz offensichtlich und deswegen im folgenden aufgeführt.

Spielstandzettel
~~~~~~~~~~~~~~~~

Es sind prinzipiell zwei Varianten für Spielstandzettel denkbar: Eine „horizontale“, wo Paar 1 oben und Paar 2 unten steht und die Bobbl von links nach rechts geschrieben werden, und eine „vertikale“, wo Paar 1 links und Paar 2 rechts steht, und die Bobbl von oben nach unten aufgeschrieben werden.

Für beide Varianten bringt Muckturnier Vorlagen für zwei und drei Bobbl pro Runde mit. Mittels der `Zettel drucken`_-Funktion können sehr komfortabel Spielstandblöckchen erstellt werden. Der Aufbau der Zettel hat sich bewährt; es ist – anders als man denken möchte – gar nicht so einfach, intuitiv ausfüllbare Spielstandzettel zu entwerfen, bei denen die Spieler dann auch das Richtige an die richtige Stelle schreiben.

Die Eingabemaske für die Ergebnisse kann zwischen „horizontal“ und „vertikal“ umgestellt werden. Grundsätzlich gilt: Egal, wie die Zettel aussehen oder erstellt werden, sie müssen mit der Eingabemaske übereinstimmen. Weil wenn man erst überlegen muss, was wo eingegeben werden muss, dann sind Eingabefehler vorprogrammiert.

Abgesegen davon sollten Spielstandzettel unbedingt neben dem eigentlichen Spielstand alle verfügbaren Daten enthalten:

* Die Namen aller Spieler (im Zweifelsfall kann man das Geschmiere eh nur teilweise entziffern)
* Die richtige Zuordnung der Namen zu „Paar 1“ und „Paar 2“
* Die Tischnummer
* Die Rundennummer

Seit Version 3.7.0 stehen auch Spielernummern zur Verfügung. Wer die benutzen will, sollte sie natürlich sinnvollerweise auch auf die Zettel schreiben lassen.

Am besten macht man kleine Blöckchen mit Spielstandzetteln für alle geplanten Runden und alle verfügbaren Tische, auf denen die Runden- und Tischnummern bereits aufgedruckt bzw. vorgeschrieben sind. Je weniger die Leute machen müssen, desto weniger geht schief.

Zeitbegrenzung
~~~~~~~~~~~~~~

Man sollte auf jeden Fall eine Zeitbegrenzung pro Runde ausmachen. Zwei Bobbl bis 21 sind in 45 Minuten komfortabel zu karten (meistens dauert es gar nicht so lang), 40 sind okay, 35 sportlich aber auch noch machbar (je nach dem, ob man z. |nnbsp| B. einen „Pflichtmuck“ um einen Punkt spielt, wenn niemand etwas ansagt, oder das nächste Spiel doppelt zählt wird es etwas mehr oder weniger hektisch bei 35 Minuten).

So verhindert man, dass ein einzelnes oder wenige Trödel-Paare den kompletten Betrieb aufhalten.

Ist die Zeit abgelaufen, dann wird die Runde abgebrochen, und die in dem jeweiligen abgebrochenen Bobbl erreichten Punkte zählen als geschossene Tore für das jeweilige Paar. Das ist innerhalb der Wertung immer noch fair und transparent, da ja kein Paar den Bobbl gewonnen hat, aber die Punkte trotzdem zählen.

Damit macht man das Turnier und vor allem sein Ende zeitlich planbar und vermeidet genervte weil gelangweilte Karter.

Voranmeldungen
~~~~~~~~~~~~~~

Am besten schreibt man Voranmeldungen gleich in eine Datenbank. Damit wird dann, wie bei der eigentlichen Anmeldung auch, nach Duplikaten gesucht, so dass versehentliche Doppelanmeldungen vermieden werden. Der Einfachheit halber setzt man die automatische Markierung für neue Anmeldungen für den Zeitraum der Voranmeldung auf „Vorangemeldet“, dann muss man die Markierung nicht manuell setzen.

Alternativ kann man die Voranmeldungen auch in eine einfache, UTF-8-kodierte Textdatei schreiben und dann den `Import einer Paar- bzw. Spielerliste`_ nutzen. Hier gibt es dann natürlich keine automatische Suche nach Duplikaten. In diesem Fall bietet es sich an, nach dem Import zunächst die gesamte Liste als „Vorangemeldet“ zu markieren.

Für die richtige Anmeldung wird dann die automatische Markierung für neue Anmeldungen auf etwas anderes als „Vorangemeldet“ gestellt. Nicht vorangemeldete Paare, die „einfach so“ kommen, werden auch „einfach so“ eingetragen, bei solchen mit Voranmeldung wird die Markierung geändert bzw. entfernt.

Wenn es dann losgehen soll, sind alle vorangemeldeten aber nicht erschienenen Paare immer noch als „Vorangemeldet“ markiert und können über das „Ganze Liste“-Menü mit einem Klick gelöscht werden.

Weitere Markierungen (bisher nur ein Spieler da, Startgeld noch nicht bezahlt, allein gekommen etc.) und die Handhabung derselben bleiben der Kreativität der Turnierleitung überlassen ;-)

Noch einfacher wird es, wenn man die `Voranmeldung mit Anmeldungscodes`_ nutzt. Damit erspart man sich die Suche nach dem jeweiligen Paar oder Spieler, und die Markierungen werden automatisch geändert, so dass die Anmeldung mit einem „Piep“ und einem Klick erledigt ist.

Schnelle Eingabe der Ergebnisse
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Auslosung bei der Anmeldung nutzen
----------------------------------

Die `Auslosung für die 1. Runde`_ direkt bei der Anmeldung verursacht – je nach dem, wie die Parameter gewählt werden – deutlich weniger Arbeit und Durcheinander als die klassische Zettelauslosung. Weiterhin kann bereits in der 1. Runde die Paar- bzw. Spielerauswahl über die Tischnummer erfolgen, so dass sich das Heraussuchen der Paare bzw. Spieler erübrigt. Man bekommt damit mehr Überblick und hat weniger Arbeit.

Paare finden
------------

Die einfachste Auswahl der Paare bzw. Spieler geht immer über die Tischnummer – sofern man entweder die Auslosung mit dem Programm nutzt (vgl. `Auslosung für die 1. Runde`_ ff.), oder mit einem festen Schema („Paar 1 bleibt sitzen, Paar 2 rutscht weiter“) spielt – in diesem Fall ist ab der 2. Runde klar, wer wo sitzt, und die richtigen Paare können auch ohne eingegebene Auslosung über die Tischnummer ausgewählt werden.

Wenn man das Auslosungs-Feature nicht nutzen will, dann ist unabhängig vom Turniermodus ist zumindest die Eingabe der 1. Runde aufwändig, weil man die Paare auf jeden Fall heraussuchen muss. Das Heraussuchen eines Paares aus der ganzen Liste ist mitunter langwierig, weil oft die Namen nicht in der „richtigen“ (also angemeldeten) Reihenfolge auf den Spielstandzetteln stehen und/oder unvollständig  und/oder kaum lesbar sind.

Am besten benutzt man zum Auffinden des jeweiligen Paares die Suchfunktion in der Auswahlbox: Bereits das Eingeben der Anfangsbuchstaben der Nach- oder Vornamen schränkt die Auswahl sehr ein, bei jeweils zwei Buchstaben bleiben meistens nur doch zwei oder drei Möglichkeiten übrig. Es können auch Teile mitten aus einem Namen sein. Irgend ein Brocken bringt schon was :-)

Beispiel: Für das Paar „Meier Bernd / Müller Karl“ reicht es höchstwahrscheinlich aus, nach „me mü“ zu suchen. Groß- und Kleinschreibung müssen nicht „richtig“ eingegeben werden. Umlaute und Akzente können auch weggelassen oder ersetzt werden, so dass z. |nnbsp| B. auch die Suche nach „me mu“ oder „me mue“ zum gewünschten Ergebnis führt. „André“ findet man auch bei der Suche nach „andre“. Die Reihenfolge der Suchbegriffe ist ebenfalls egal: „mü me“ funktioniert genauso wie „me mü“.

Weiterhin kann man optional noch, mit dem Ohr-und-Schall-Symbol, die phonetische Suche aktivieren, bei der nicht nach der eingegebenen Zeichenfolge, sondern nach deren Klang gesucht wird. Diese ist noch deutlich toleranter als die Standardsuche: Den Namen „Raithel“ findet man damit z. |nnbsp| B. auch durch die Eingabe von „reidl“. Allerdings steigen durch die systembedingte Unschärfe auch die falsch-positiven (also angezeigte aber nicht gewollte/passende) Treffer an. Ob oder ob nicht die phonetische Such sinnvoll anzuwenden ist, muss man im Einzelfall einfach ausprobieren.

Punkte eingeben
---------------

Die Punkte kann man schnell (und ohne Tippfehler) über den Punkte-Dialog eingeben, wo man den jeweiligen Punktewert per Klick auswählen kann. Hierzu einfach den Bobbl oder das Eingabefeld mit der rechten Maustaste anklicken.

Anmeldung mit mehr als einem Rechner
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ab einer gewissen Größenordnung ist es sinnvoll, die Anmeldung an mindestens zwei Rechnern vorzunehmen. Mit zwei oder mehr Anmeldungscomputern verhindert man lange Schlangen.

Als Realeinsatz-Beispiel sei hier das Konzept für die Anmeldung beim Muckturnier auf dem Hofer Volksfest genannt. Es kommen drei Rechner zum Einsatz: Zwei für die Anmeldung und einer bei der Auslosung.

* Es werden die voreingestellten Markierungen benutzt: „Vorangemeldet“, „Allein da“ und „Auslosung fehlt“.

* Die Voranmeldungsliste wird am Server (z. |nnbsp| B. Anmeldungsrechner 1) als „Vorangemeldet“ markiert. Die Clients (Anmeldungsrechner 2 und Auslosungsrechner) verbinden sich und werden synchronisiert.

* Vor Ort angemeldete Paare  werden eingegeben und als „Auslosung fehlt“ markiert. Bei erschienenen vorangemeldeten Paaren wird die Markierung von „Vorangemeldet“ zu „Auslosung fehlt“ geändert. Allein gekommene Spieler werden angemeldet und als „Allein da“ markiert (was Anmeldungsrechner 2 dann auch sieht und weitere allein gekommene Spieler dann genauso zuordnen kann, wie Anmeldungsrechner 1).

* Die angemeldeten Paare gehen dann zur Auslosung. Damals mussten wir die Tische in der Reihenfolge des Erscheinens „zulosen“, damit keine Lücken entstehen, mittlerweile würde man die Auslosung `direkt bei der Anmeldung <Auslosung für die 1. Runde_>`_ machen. Am Auslosungsrechner wird überprüft, ob das jeweilige Paar korrekt angemeldet wurde (bezahlt hat, also die Markierung „Auslosung fehlt“ ist). Paar 1 bekommt die Spielstandblöckchen, Regeln, Stift und die Karten, Paar 2 nur die Tischnummer. Dann wird die Markierung entfernt.

* Alle anwesenden, korrekt angemeldeten und bereits ausgelosten Paare haben keine Markierung mehr. Vor dem Turnierbeginn werden einfach alle noch markierten Paare/Spieler gelöscht (sofern noch vorhanden).

Prinzipiell würde das auch ohne Netzwerk gehen: Man nimmt z. |nnbsp| B. zwei Rechner, beide haben alle Voranmeldungen. Beide markieren alle als „Vorangemeldet“. Beide entfernen bei den gekommenen Paaren die Markierung. Ist die Anmeldung beendet, dann exportiert Rechner 2 alle unmarkierten (also anwesenden) Paare. Rechner 1 löscht alle, die noch als „Vorangemeldet“ markiert sind und importiert die exportierte Liste von Rechner 2 (die z. |nnbsp| B. via USB-Stick übertragen wurde). Aber ganz sicher ist die netzwerkbasierte Lösung vorzuziehen.

Auswertung mit mehr als einem Rechner
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Wenn man an mindestens zwei Computern unabhängig voneinander alle Ergebnisse einträgt, kann man Eingabefehler finden.

Seit Version 3.5 werden alle eingegebenen Ergebnisse sowohl server- als auch clientseitig fortlaufend miteinander verglichen, so dass man zu jedem Zeitpunkt überprüfen kann, ob alle Ergebnisse gleich (und somit höchstwahrscheinlich richtig) eingegeben wurden.

Übersetzen aus den Quelltexten
==============================

Konfigurieren
~~~~~~~~~~~~~

Muckturnier ist in `C++ <https://de.wikipedia.org/wiki/C%2B%2B>`_ geschrieben. Es wird der C++17-Standard genutzt. Das Programm linkt gegen `Qt <http://www.qt.io/>`_ 6 oder 5.15. Es werden das QtSql-Modul für `SQLite <http://sqlite.org/>`_ 3, das QtNetwork- und das QtSvg-Modul benutzt.

Als Buildsystem kommt `CMake <http://cmake.org/>`_ (ab Version 3.10) zum Einsatz.

Folgende Schalter können gesetzt werden (via :code:`cmake ... -DSCHALTER=on/off`):

- ``QT6``: Gegen Qt 6 linken. Falls der Schalter deaktiviert wird, wird gegen Qt 5 gelinkt.

- ``DOCUMENTATION``: Die Readme von `reStructuredText <https://de.wikipedia.org/wiki/ReStructuredText>`_ nach HTML übersetzen. Benötigt die Python-`Docutils <http://docutils.sourceforge.net/>`_.

Weiterhin können natürlich Compiler, Generator etc. gesetzt werden (z. |nnbsp| B. `Ninja <https://ninja-build.org/>`_ und `Clang <http://clang.llvm.org/>`_), vgl. die `Dokumentation von CMake <https://cmake.org/documentation/>`_.

Kompilieren
~~~~~~~~~~~

Das Kompilieren funktioniert auf dem normalen Weg. Hier z. |nnbsp| B. ein Linux-Build mit GNU Make und GCC:

.. code::

    mkdir build
    cd build
    cmake ..
    make -j$(nproc)

Zum Entwickeln bietet es sich an, ein Debug-Build (:code:`cmake ... -DCMAKE_BUILD_TYPE=Debug ... ..`) zu erstellen. Das aktiviert sowohl Qt- als auch Muckturnier-seitig die Ausgabe vieler Infos, Warnungen etc.

Installieren
~~~~~~~~~~~~

Es ist nicht unbedingt eine Installation nötig. Das kompilierte Programm kann vor Ort ausgeführt werden. Das Buildsystem sorgt dafür, dass alle Ressourcendateien gefunden werden können.

Für eine systemweite Installation ist für Linux und Windows ist ein ``install``-Target definiert. Man könnte also, um beim obigen Beispiel zu bleiben, auf Linux das Programm nach dem Kompilieren mit

.. code::

    make install

installieren. Sofern verfügbar werden unter Linux mittels der `xdg-utils <https://www.freedesktop.org/wiki/Software/xdg-utils/>`_ dann auch die „Freedesktop Shared MIME Info“-Datei ``org.muckturnier.muckturnier.appdata.xml`` sowie die „Desktop Entry Specification“ ``org.muckturnier.muckturnier.desktop`` in der xdg-Datenbank angemeldet, damit ein Startmenüeintrag für Muckturnier erstellt und der Datenbank-Dateityp ``.mtdb`` mit dem Muckturnier-Programm verknüpft wird.

Unter Windows müssen bei einer manuellen Installation eine Verknüpfung mit dem Datenbank-Dateityp ``.mtdb`` sowie Startmenüeinträge manuell hinzugefügt werden.

Unter macOS kenne ich mich zu wenig aus, als dass ich eine Aussage darüber treffen könnte, ob das ``install``-Target sinnvoll einzusetzen ist, oder nicht. Hier wäre ich für „Insiderinformationen“ dankbar.

Einschränkungen von Windows- und macOS-Builds mit cmake
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Zwar sind auf Linux, Windows und macOS Builds mit cmake möglich, allerdings ist das Buildsystem auf Linux ausgelegt. Nur dort liefert cmake ein „komplettes“ Build.

Die Icons für das Programm sowie den Datenbank-Dateitypen bereite ich für das Windows-Binärpaket unter Linux vor. In die ausführbare Datei eingebettet wird das Icon nur vom qmake-Build (s. |nnbsp| u.), nicht von cmake.

Die Icons für macOS erstelle ich mittels des macOS-Build-Scripts. Auch hier werden bei einem cmake-Build keine Icons erzeugt.

Bauen der Binärpakete
=====================

Die Binärpakete baue ich nicht mit cmake, sondern mit `qmake <https://doc.qt.io/qt-5/qmake-manual.html>`_. Die qmake-.pro-Dateien dafür funktionieren aber nicht „einfach so“, es muss ein Quelltextpaket für das Build vorbereitet werden.

Zunächst baue ich unter Linux mit den Scripts unter ``res/release_tools`` ein entsprechendes Quell-Paket mit plattformspezifischen Daten (Icons etc.). Das eigentliche Build erstellt dann ein Script, jeweils für das jeweilige Betriebssystem. Hier werden dann noch weitere Aufgaben erledigt, z. |nnbsp| B. das Erstellen der `ICNS-Datei <https://en.wikipedia.org/wiki/Apple_Icon_Image_format>`_ für macOS.

Für Linux erstelle ich ein `AppImage <https://appimage.org/>`_ mittels `linuxdeploy <https://github.com/linuxdeploy/linuxdeploy/>`_.

Unter macOS ist das Build gleich selbst ein ``.app``-Verzeichnis. Das wird dann von dem Release-Script noch in ein ULMO- (also LZMA-komprimiertes) DMG-Image gepackt und ein Drag-and-Drop-Installer-Interface hinzugefügt.

Für Windows baue ich mit `NSIS <https://nsis.sourceforge.io/Main_Page>`_ einen Installer.

Die benutzen Scripts sind in ``res/release_tools`` sowie in ``res/appimage_build``, ``res/macos_build`` und ``res/windows_build`` zu finden.

Fehlermeldungen, Lob, Kritik, Mitmachen
=======================================

Der Quellcode des Muckturnier-Programms wird auf `GitLab <https://gitlab.com/l3u/muckturnier>`_ gehostet. Von dort kann er auch (anonym) über ``https://gitlab.com/l3u/muckturnier.git`` ausgecheckt werden.

Fehler, Feature Requests etc. können in den `Bugtracker <https://gitlab.com/l3u/muckturnier/-/issues>`_ eingetragen werden. Alternativ auch gerne einfache eine E-Mail an |contact_email| schreiben!

Schlussbemerkung
================

Wenn jemand Muckturnier auf einem Turnier eingesetzt hat, dann würde ich mich über Feedback freuen! Gerne würde ich das entsprechende Turnier auch als Referenz auf der Homepage mit auflisten, wenn ich darf.

Bisher hat Muckturnier immer fehlerfrei gerechnet und einwandfrei funktioniert. Der Autor übernimmt aber natürlich keinerlei Garantie für die Fehlerfreiheit, Verwendbarkeit, Funktionstüchtigkeit oder Verfügbarkeit des Programms (vgl. auch die für Muckturnier benutzte Lizenz `GPL <https://www.gnu.org/licenses/#GPL>`_), für aus Fehlberechnungen oder -eingaben resultierende Wirtshausschlägereien oder sonst irgendwas ;-)

Viel Spaß mit Muckturnier und natürlich auch beim Mucken!
